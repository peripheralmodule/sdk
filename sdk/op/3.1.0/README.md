1.文件介绍：
	1.1 SDK文件：ydhw.oppo.sdk.min.js
	1.2 SDK配置文件：config.js
	1.3 .d.ts提示文件：ydhw.interface.d.ts
	1.4 枚举和接口实现文件：ydhw.sdk.ts

2.引入步骤:
	2.1 拷贝ydhw.oppo.sdk.min.js放到发布根目录下
    2.2 将config.js拷贝到index.js文件的最上方，并修改ydhw.oppo.sdk.min.js加载路径和游戏参数配置戏参数配置
    2.3 接口的解释和参数请查看ydhw.interface.d.ts文件
	2.4 接口中用到的枚举和类ydhw.sdk.ts

3.记得添加以下合法域名：
	- 服务器地址：		   https://api.ylxyx.cn
	- 图片服务器地址1：	   https://ql.ylxyx.cn
	- 图片服务器地址2：	   https://tx.ylxyx.cn
	- 图片服务器地址3：	   https://ext.ylxyx.cn
	- CDN服务器地址：      https://ydhwimg.szvi-bo.com
	- CDN服务器地址：      https://txext.ylxyx.cn