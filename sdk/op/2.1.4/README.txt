该版本合并了SDK接口文件，更方便接入和维护了。
接口文件目录如下：
sdk_conf：  
    |- utils：
    |       |- yl_sdk.js          SDK文件
    |       |- yl_sdk_conf.js  SDK配置文件
    |
    |-.d.ts:   TS语言工程SDK提示文件
        |- qq.d.ts     QQ平台
        |- wx.d.ts     微信平台
        |- tt.d.ts     头条平台
        |- qg.d.ts     OPPO、VIVO、魅族共用，具体的接口请以文档为准


注意：
    1.因为SDK文件是共用的，但laya引入时有兼容问题，所以laya工程需要修改yl_sdk.js和yl_sdk_conf.js文件，具体请参考文档SDK配置项。