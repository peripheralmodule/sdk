/**
 * 积分墙列表
 */
cc.Class({
    extends: cc.Component,
    properties: {
        nodeContent:cc.Node,
        bItem:cc.Prefab,
    },

    showView() {
        this.node.active = true;
        if(!this.items) this.items = new Array();
        if(this.items.length == 0){
            if (this.inviteDatas) {
                this.getIntegralWall(this.inviteDatas);
            }else{
                var that = this
                qg.ylIntegralWall(function (data) {
                    console.log("获取积分墙列表："+ JSON.stringify(data))
                    that.inviteDatas = data;
                    that.getIntegralWall(data); 
                });
            }
        }else{
            this.showItemAnim();
        }
    },

    getIntegralWall(data) {
        //data.boardconfig instanceof Object 后台有个异常，
        //如果没配置积分墙，就会返回空对象,而不是空数组
        if (data && data.length > 0) {
            if (!cc.isValid(this.nodeContent)) return;
            this.loadBoardAwardIcon(0);
            let i = 0;
            data.forEach(element => {
                    i+=1;
                if (element.awardStatus) {
                    let box = cc.instantiate(this.bItem);
                    box.parent = this.nodeContent;
                    box.x = -box.width*1.5;
                    box.getComponent('ylInviteItem').updateCell(element);
                    this.items.push(box);
                }
            });
            this.showItemAnim();
        }
    },

    /**
     * 积分墙列表item的游戏ICON
     * @param index 列表ID
    **/
    loadBoardAwardIcon(index) {
        let self = this;
        if (index < this.inviteDatas.length) {
            let data = this.inviteDatas[index];
            if (!data.texture &&  data.icon && data.icon !== '') {
                cc.loader.load(data.icon, (err, texture) => {
                    if (!err) {
                        self.inviteDatas[index].texture = texture;
                        let evt = new cc.Event.EventCustom(window.Global.EVENT_LOAD_BOARD_ICON + data._id);
                        evt.setUserData(texture);
                        cc.director.dispatchEvent(evt);
                    }
                    index++;
                    self.loadBoardAwardIcon(index);
                });
            } else {
                index++;
                self.loadBoardAwardIcon(index);
            }
        }
    },

    showItemAnim(){
        let delayT = 0;
        this.items.forEach(element => {
            element.x = -element.width*1.5;
            element.stopAllActions();
            element.runAction(cc.sequence(
                cc.delayTime(delayT),
                cc.moveTo(0.2, 0, element.y)));
            delayT +=0.1;
        });
    },

    onClose(){
        this.node.active = false;
        if(this.items){
            this.items.forEach(element => {
                element.stopAllActions();
            });
        }
    },
});
