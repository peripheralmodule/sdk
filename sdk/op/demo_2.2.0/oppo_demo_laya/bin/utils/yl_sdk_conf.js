var exports = window;
exports.ylsdk_app_id = "30250869"; //游戏APPID
exports.ylsdk_version = "1.0.0";    //游戏版本号
exports.ylsdk_debug_model = true;   //是否debug模式 [true:打印日志,false:不打印日志]
exports.login_by_code = false;      //配置为true，则每次都会走平台登录
exports.web_test = false;            //web测试模式(该模式需将平台设置为 影流小游戏)
exports.ylsdk_pkg_name="ydhw.ccnbxl.nearme.gamecenter"; //游戏包名(OPPO、VIVO)

exports.side_min_num = 20;       //侧边栏列表item最小保留数(基于曝光策略)

exports.ylsdk_banner_ids = [	 //banner广告ID
    '166363'
]; 
exports.ylsdk_video_ids = [		//激励视频广告ID
    '166368'
];							  
exports.ylsdk_interstitial_ids = [	//插屏广告ID
    '166364'
];
exports.ylsdk_native_ids = [	//原生广告ID
    '166367'
];
                      

/****************微信公众平台配置以下合法域名************************/

// 服务器地址：		   https://api.ylxyx.cn
// 图片服务器地址1：	   https://ql.ylxyx.cn
// 图片服务器地址2：     https://tx.ylxyx.cn
// 图片服务器地址3：	   https://ext.ylxyx.cn
// CDN服务器地址：	   https://ydhwimg.szvi-bo.com

/*****************************************************************/
