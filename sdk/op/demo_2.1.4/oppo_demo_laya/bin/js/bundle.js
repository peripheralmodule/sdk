(function () {
	'use strict';

	var REG = Laya.ClassUtils.regClass;
	var ui;
	(function (ui) {
	    var test;
	    (function (test) {
	        class TestSceneUI extends Laya.Scene {
	            constructor() { super(); }
	            createChildren() {
	                super.createChildren();
	                this.loadScene("test/TestScene");
	            }
	        }
	        test.TestSceneUI = TestSceneUI;
	        REG("ui.test.TestSceneUI", TestSceneUI);
	    })(test = ui.test || (ui.test = {}));
	})(ui || (ui = {}));

	class GameUI extends ui.test.TestSceneUI {
	    constructor() {
	        super();
	        this.nativeData = null;
	        GameUI.instance = this;
	        Laya.MouseManager.multiTouchEnabled = false;
	    }
	    onEnable() {
	        this.custom = 0;
	        qg.ylGameLifeListen(function (type, res) {
	            if (type === 'onShow') {
	                console.warn("YLSDK GameUI-onShow--res:", res);
	            }
	            else if (type === 'onHide') {
	                console.warn("YLSDK GameUI-onHide");
	            }
	            else if (type === 'onError') {
	                console.warn("YLSDK GameUI-onError--res:", res);
	            }
	        });
	        qg.ylSetCurScene("MainScene");
	        qg.ylInitSDK(function () {
	            console.log("YLSDK ---ylInitSDK---初始化完成");
	            this.doSDKinterface(qg);
	        }.bind(this));
	        this.initUI();
	        qg.ylNativeAdCreate(function (data) {
	            console.log("HelloWorld ---ylNativeAdCreate--data:", JSON.stringify(data));
	            if (data) {
	                this.nativeData = data;
	            }
	        }.bind(this));
	    }
	    initUI() {
	        this.btnVideoOrShare.on(Laya.Event.CLICK, this, this.onShowShareOrVideo);
	        this.btnShare.on(Laya.Event.CLICK, this, this.onShowShare);
	        this.btnVideoUnlockCustoms.on(Laya.Event.CLICK, this, this.onVideoUnlockCustoms);
	        this.btnShowVideoAd.on(Laya.Event.CLICK, this, this.onShowVideoAd);
	        this.btnShowBannerAd.on(Laya.Event.CLICK, this, this.onShowBannerAd);
	        this.btnNativeAd.on(Laya.Event.CLICK, this, this.onShowNativeAd);
	        this.btnClickNativeAd.on(Laya.Event.CLICK, this, this.onClickNativeAd);
	        this.btnOver.on(Laya.Event.CLICK, this, this.onOver);
	        this.btnGetDeepTouch.on(Laya.Event.CLICK, this, this.onGetDeepTouch);
	        this.btnConsumePower.on(Laya.Event.CLICK, this, this.onConsumePower);
	        this.btnAddPower.on(Laya.Event.CLICK, this, this.onAddPower);
	        this.btnShowInterstitial.on(Laya.Event.CLICK, this, this.onShowInterstitial);
	        this.btnJump.on(Laya.Event.CLICK, this, this.onJump);
	    }
	    onShowVideoAd(e) {
	        qg.ylShowVideoAd(function () { }, this.custom);
	        console.log("HelloWorld ---onShowVideo---");
	    }
	    onShowBannerAd(e) {
	        qg.ylBannerAdCreate(true, function () {
	        }, true);
	        console.log("HelloWorld ---onShowBanner--");
	    }
	    onShowNativeAd(e) {
	        console.log("HelloWorld ---onShowNativeAd--nativeData:", this.nativeData);
	        if (this.nativeData && this.nativeData.length > 0) {
	            qg.ylNativeAdShow(this.nativeData[0].adId);
	        }
	    }
	    onClickNativeAd(e) {
	        console.log("HelloWorld ---onClickNativeAd--nativeData:", this.nativeData);
	        if (this.nativeData && this.nativeData.length > 0) {
	            qg.ylNativeAdClick(this.nativeData[0].adId);
	        }
	    }
	    onVideoUnlockCustoms(e) {
	        let unlock = qg.ylVideoUnlock(this.custom);
	        console.warn("HelloWorld-onVideoUnlockCustoms-是否解锁:", unlock);
	    }
	    onOver(e) {
	        this.custom += 1;
	        console.log("HelloWorld ---onOver--custom:", this.custom);
	        qg.ylOverGame(this.custom);
	    }
	    onGetDeepTouch(e) {
	        let info = qg.ylGetDeepTouch(this.custom);
	        console.warn("HelloWorld onGetDeepTouch-info: ", JSON.stringify(info));
	    }
	    onConsumePower(e) {
	        let power = qg.ylGetPower();
	        console.log("HelloWorld ---onConsumePower--power:", (power - 1));
	        qg.ylSetPower(power - 1);
	    }
	    onAddPower(e) {
	        let power = qg.ylGetPower();
	        console.log("HelloWorld ---onAddPower--power:", (power + 1));
	        qg.ylSetPower(power + 1);
	    }
	    onShowShareOrVideo(e) {
	        qg.ylShowShareOrVideo("bb", "aa", function (type) {
	            switch (type) {
	                case 0:
	                    console.warn("------策略-无");
	                    break;
	                case 1:
	                    console.warn("------策略-分享");
	                    break;
	                case 2:
	                    console.warn("------策略-视频");
	                    break;
	            }
	        }.bind(this));
	    }
	    onShowShare(e) {
	        console.log("HelloWorld ---onShowShare---");
	    }
	    onShowInterstitial(e) {
	        console.log("HelloWorld ---onShowInterstitial---");
	        qg.ylCreateInterstitialAd(true, function (type) {
	            let textType = "";
	            switch (type) {
	                case 0:
	                    textType = "创建或展示失败";
	                    break;
	                case 1:
	                    textType = "创建或展示成功";
	                    break;
	                case 2:
	                    textType = "关闭";
	                    break;
	            }
	            console.log("HelloWorld ---onShowInterstitial---", textType);
	        });
	    }
	    onJump(e) {
	        qg.ylSideBox(function (data) {
	            console.warn("获取侧边栏数据:", JSON.stringify(data));
	            if (data && data.length > 0) {
	                let item = data[0];
	                let jumpInfo = {
	                    _id: item._id,
	                    toAppid: item.toAppid,
	                    toUrl: item.toUrl,
	                    source: "测试",
	                };
	                qg.ylNavigateToMiniGame(jumpInfo, function () {
	                    console.warn("跳转返回-ylNavigateToMiniGame");
	                }.bind(this));
	            }
	        }.bind(this));
	    }
	    doSDKinterface(qg) {
	        qg.ylLog("---调用接口---");
	        qg.ylEventCount("start_game");
	        qg.ylSideBox(function (data) {
	            console.warn("获取侧边栏数据:", JSON.stringify(data));
	            if (data && data.length > 0) {
	                let item = data[0];
	                let jumpInfo = {
	                    _id: item._id,
	                    toAppid: item.toAppid,
	                    toUrl: item.toUrl,
	                    source: "测试",
	                };
	                qg.ylNavigateToMiniGame(jumpInfo, function () {
	                    console.warn("跳转返回-ylNavigateToMiniGame");
	                }.bind(this));
	            }
	        }.bind(this));
	        qg.ylGetCustom(function (data) {
	        }.bind(this));
	        qg.ylStatisticResult({ "total_score": 123, "rebirth_score": 123 }, function (status) {
	            if (status) {
	            }
	        }.bind(this));
	        let userInfo = qg.ylGetUserInfo();
	        console.log("---用户信息---userInfo:", JSON.stringify(userInfo));
	        let appid = qg.ylGetAppID();
	        console.log("------appid:", JSON.stringify(appid));
	        let inviteAccount = qg.ylGetInviteAccount();
	        console.log("------inviteAccount:", JSON.stringify(inviteAccount));
	        let switchInfo = qg.ylGetSwitchInfo();
	        console.log("------开关信息-switchInfo:", JSON.stringify(switchInfo));
	        qg.ylIntegralWall(function (data) {
	            qg.ylGetBoardAward(function (data) {
	                console.log("------领取积分墙奖励:", data);
	            }.bind(this));
	        }.bind(this));
	        qg.ylStatisticViedo(0, "212456", function (data) {
	            console.log("------视频播放统计-ylStatisticViedo:", JSON.stringify(data));
	        }.bind(this));
	        this.testStoreValue(qg);
	    }
	    testStoreValue(qg) {
	        this.test_sv_string(qg);
	        this.test_sv_list(qg);
	        this.test_sv_set(qg);
	        this.test_sv_hash(qg);
	        this.test_sv_radom(qg);
	    }
	    test_sv_string(qg) {
	        qg.ylStoreValue({
	            name: "testString",
	            cmd: "set",
	            args: "测试数据"
	        }, function (status) {
	            qg.ylStoreValue({
	                name: "testString",
	                cmd: "get"
	            }, function (status) {
	            }.bind(this));
	        }.bind(this));
	    }
	    test_sv_list(qg) {
	        qg.ylStoreValue({
	            name: "testList",
	            cmd: "add",
	            args: "0"
	        }, function (status) {
	        }.bind(this));
	        qg.ylStoreValue({
	            name: "testList",
	            cmd: "add",
	            args: "2"
	        }, function (status) {
	        }.bind(this));
	        qg.ylStoreValue({
	            name: "testList",
	            cmd: "set",
	            args: "0,3"
	        }, function (status) {
	            qg.ylStoreValue({
	                name: "testList",
	                cmd: "all"
	            }, function (status) {
	            }.bind(this));
	            qg.ylStoreValue({
	                name: "testList",
	                cmd: "get",
	                args: "0"
	            }, function (status) {
	            }.bind(this));
	            qg.ylStoreValue({
	                name: "testList",
	                cmd: "size"
	            }, function (status) {
	            }.bind(this));
	            qg.ylStoreValue({
	                name: "testList",
	                cmd: "poll",
	                args: "2"
	            }, function (status) {
	                qg.ylStoreValue({
	                    name: "testList",
	                    cmd: "size"
	                }, function (status) {
	                }.bind(this));
	                qg.ylStoreValue({
	                    name: "testList",
	                    cmd: "replace",
	                    args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
	                }, function (status) {
	                    qg.ylStoreValue({
	                        name: "testList",
	                        cmd: "all"
	                    }, function (status) {
	                    }.bind(this));
	                }.bind(this));
	            }.bind(this));
	        }.bind(this));
	    }
	    test_sv_set(qg) {
	        qg.ylStoreValue({
	            name: "testSet",
	            cmd: "add",
	            args: "12"
	        }, function (status) {
	        }.bind(this));
	        qg.ylStoreValue({
	            name: "testSet",
	            cmd: "add",
	            args: "10"
	        }, function (status) {
	            qg.ylStoreValue({
	                name: "testSet",
	                cmd: "exist",
	                args: "10"
	            }, function (status) {
	            }.bind(this));
	            qg.ylStoreValue({
	                name: "testSet",
	                cmd: "size"
	            }, function (status) {
	                qg.ylStoreValue({
	                    name: "testSet",
	                    cmd: "del",
	                    args: "10"
	                }, function (status) {
	                    qg.ylStoreValue({
	                        name: "testSet",
	                        cmd: "all"
	                    }, function (status) {
	                        qg.ylStoreValue({
	                            name: "testSet",
	                            cmd: "replace",
	                            args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
	                        }, function (status) {
	                            qg.ylStoreValue({
	                                name: "testSet",
	                                cmd: "all"
	                            }, function (status) {
	                            }.bind(this));
	                        }.bind(this));
	                    }.bind(this));
	                }.bind(this));
	            }.bind(this));
	        }.bind(this));
	    }
	    test_sv_hash(qg) {
	        qg.ylStoreValue({
	            name: "testHash",
	            cmd: "set",
	            args: "u_name,许"
	        }, function (status) {
	            qg.ylStoreValue({
	                name: "testHash",
	                cmd: "get",
	                args: "u_name"
	            }, function (status) {
	                qg.ylStoreValue({
	                    name: "testHash",
	                    cmd: "replace",
	                    args: "{\"u_name\":\"唐\",\"sex\":\"women\"}"
	                }, function (status) {
	                    qg.ylStoreValue({
	                        name: "testHash",
	                        cmd: "gets",
	                        args: "u_name,sex"
	                    }, function (status) {
	                    }.bind(this));
	                    qg.ylStoreValue({
	                        name: "testHash",
	                        cmd: "size",
	                    }, function (status) {
	                    }.bind(this));
	                    qg.ylStoreValue({
	                        name: "testHash",
	                        cmd: "values",
	                        args: "sex"
	                    }, function (status) {
	                    }.bind(this));
	                    qg.ylStoreValue({
	                        name: "testHash",
	                        cmd: "del",
	                        args: "u_name"
	                    }, function (status) {
	                        qg.ylStoreValue({
	                            name: "testHash",
	                            cmd: "all",
	                        }, function (status) {
	                        }.bind(this));
	                    }.bind(this));
	                }.bind(this));
	            }.bind(this));
	        }.bind(this));
	    }
	    test_sv_radom(qg) {
	        qg.ylStoreValue({
	            name: "testRandom"
	        }, function (status) {
	        }.bind(this));
	    }
	}

	class GameConfig {
	    constructor() {
	    }
	    static init() {
	        var reg = Laya.ClassUtils.regClass;
	        reg("script/GameUI.ts", GameUI);
	    }
	}
	GameConfig.width = 640;
	GameConfig.height = 1136;
	GameConfig.scaleMode = "fixedwidth";
	GameConfig.screenMode = "none";
	GameConfig.alignV = "top";
	GameConfig.alignH = "left";
	GameConfig.startScene = "test/TestScene.scene";
	GameConfig.sceneRoot = "";
	GameConfig.debug = false;
	GameConfig.stat = false;
	GameConfig.physicsDebug = false;
	GameConfig.exportSceneToJson = true;
	GameConfig.init();

	class Main {
	    constructor() {
	        if (window["Laya3D"])
	            Laya3D.init(GameConfig.width, GameConfig.height);
	        else
	            Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
	        Laya["Physics"] && Laya["Physics"].enable();
	        Laya["DebugPanel"] && Laya["DebugPanel"].enable();
	        Laya.stage.scaleMode = GameConfig.scaleMode;
	        Laya.stage.screenMode = GameConfig.screenMode;
	        Laya.stage.alignV = GameConfig.alignV;
	        Laya.stage.alignH = GameConfig.alignH;
	        Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
	        if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
	            Laya.enableDebugPanel();
	        if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
	            Laya["PhysicsDebugDraw"].enable();
	        if (GameConfig.stat)
	            Laya.Stat.show();
	        Laya.alertGlobalError = true;
	        Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
	    }
	    onVersionLoaded() {
	        Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
	    }
	    onConfigLoaded() {
	        GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
	    }
	}
	new Main();

}());
