cc.Class({
    extends: cc.Component,

    properties: {
        btnSideBox:cc.Node,             //侧边栏列表按钮
        btnBoardAwardList:cc.Node,      //积分墙列表按钮
        btnGridAndScrollList:cc.Node,   //浮动卖量列表按钮

        pbSideBox:cc.Prefab,            //侧边栏列表界面
        pbInvite:cc.Prefab,             //积分墙列表界面
        pbYlBoxGrid:cc.Prefab,          //卖量网格浮动条
        pbYlBoxScrollHori:cc.Prefab,    //卖量横向浮动条
        pbYlBoxScrollVerti:cc.Prefab,   //卖量纵向浮动条
        pbGetAward:cc.Prefab,           //获得奖励View
        pbFullBox:cc.Prefab,            //全屏卖量界面
        pbBox1:cc.Prefab,               //盒子1
        nodeFloatView:cc.Node,
    },
    onLoad: function () {
        let that = this;
        qg.ylSetCurScene("MainScene");//一定记得在每个场景都要设置当前场景
        qg.ylInitSDK(function(){
            console.log("YLSDK ---ylInitSDK---初始化完成");
            this.doSDKinterface();
        }.bind(this));
    },
    /**
    * 调用SDK接口
    **/
    doSDKinterface(){
        qg.ylLog("---调用接口---");
        qg.ylEventCount("start_game");
        qg.ylSideBox(function (data) {
        }.bind(this));
        qg.ylGetCustom(function(data){
        }.bind(this));
        qg.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status){
            if(status){
                //干点什么
            }
        }.bind(this));
        let userInfo = qg.ylGetUserInfo();
        console.log("---用户信息---userInfo:",JSON.stringify(userInfo));
        let appid = qg.ylGetAppID();
        console.log("------appid:",JSON.stringify(appid));
        let inviteAccount = qg.ylGetInviteAccount();
        console.log("------inviteAccount:",JSON.stringify(inviteAccount));
        let switchInfo  = qg.ylGetSwitchInfo();
        console.log("------开关信息-switchInfo:",JSON.stringify(switchInfo));
        qg.ylIntegralWall(function (data) {
                qg.ylGetBoardAward(function(data){
                    console.log("------领取积分墙奖励:",data);
            }.bind(this));
        }.bind(this));
        qg.ylStatisticViedo(0,"212456",function(data){
            console.log("------视频播放统计-ylStatisticViedo:",JSON.stringify(data));
        }.bind(this));
        this.testStoreValue();
    },
        //刷新侧边栏/积分墙按钮展示状态
    refreshBoardAndBoxBtnVisible(){
        qg.ylLog("---刷新开关状态---",data);
        let data = qg.ylGetSwitchInfo();
        this.btnSideBox.active = data.switchSidebox === 1;//[ 0:关、1:开 ]
        this.btnBoardAwardList.active = data.switchScoreboard === 1;
        this.btnGridAndScrollList.active = data.switchSidebox === 1;
    },

    //显示侧边栏列表对话框
    onBoxShow(){
        qg.ylEventCount("Click-SideBox");
        if(!this.nodeBox) this.nodeBox = cc.instantiate(this.pbSideBox);
        this.nodeBox.x = 0;
        this.nodeBox.y = 0;
        this.nodeBox.parent = this.node;
        this.nodeBox.getComponent('ylSideBox').showView();
    },

    //显示积分墙列表对话框
    onBoardAwardListShow(){
        qg.ylEventCount("Click-BoardAward");
        if(!this.nodeInvite) this.nodeInvite = cc.instantiate(this.pbInvite);
        this.nodeInvite.x = 0;
        this.nodeInvite.y = 0;
        this.nodeInvite.parent = this.node;
        this.nodeInvite.getComponent('ylInvite').showView();
    },
    //显示浮动卖量列表
    onShowGridAndScrollList(){
        //可以把卖量列表绑定在界面或对话框上面,可参考签到对话框效果
        if(!this.nodeYlBoxGrid){
            this.nodeYlBoxGrid = cc.instantiate(this.pbYlBoxGrid);
            this.nodeYlBoxGrid.x = 0;
            this.nodeYlBoxGrid.y = 0;
            this.nodeYlBoxGrid.parent = this.nodeFloatView;
        } 
        this.nodeYlBoxGrid.getComponent('ylBoxGrid').showView(0,300);
        //
        if(!this.nodeYlBoxScrollHori){
            this.nodeYlBoxScrollHori = cc.instantiate(this.pbYlBoxScrollHori);
            this.nodeYlBoxScrollHori.x = 0;
            this.nodeYlBoxScrollHori.y = 0;
            this.nodeYlBoxScrollHori.parent = this.nodeFloatView;
        } 
        this.nodeYlBoxScrollHori.getComponent('ylBoxScrollHori').showView(0,-894);
        //
        if(!this.nodeYlBoxScrollVerti){
            this.nodeYlBoxScrollVerti = cc.instantiate(this.pbYlBoxScrollVerti);
            this.nodeYlBoxScrollVerti.x = 0;
            this.nodeYlBoxScrollVerti.y = 0;
            this.nodeYlBoxScrollVerti.parent = this.nodeFloatView;
        } 
        this.nodeYlBoxScrollVerti.getComponent('ylBoxScrollVerti').showView(-276,-173);
        this.nodeFloatView.active = true;
    },
    onCloseFloatView(){
        this.nodeFloatView.active = false;
    },
    //显示获得奖励对话框
    onShowGetAward(evt){
        let data = evt.getUserData();
        if(!this.nodeGetAward) this.nodeGetAward = cc.instantiate(this.pbGetAward);
        this.nodeGetAward.x = 0;
        this.nodeGetAward.y = 0;
        this.nodeGetAward.zorder = 20;
        this.nodeGetAward.parent = this.node;
        this.nodeGetAward.getComponent('ylGetAward').showView(data);
    },
    //视频分享策略
    onShowShareOrVideo(){
        qg.ylShowShareOrVideo("bb","aa",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    break;
                case 2:
                    console.warn("------策略-视频");
                    break;
            }
        }.bind(this));
    },
    //显示全屏卖量界面
    onShowFullBox(){        
        if(!this.nodeFullBox) this.nodeFullBox = cc.instantiate(this.pbFullBox);
        this.nodeFullBox.x = 0;
        this.nodeFullBox.y = 0;
        this.nodeFullBox.zorder = 10;
        this.nodeFullBox.parent = this.node;
        this.nodeFullBox.getComponent('ylBoxFull').showView();
    },

    //积分墙跳转到目标小游戏
    inviteJumpToMinGame(evt){
        let jump = evt.getUserData();
        qg.ylNavigateToMiniGame(jump,
            function(success){
                if(success){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_REFRESH_SIDEBOX);
                    cc.director.dispatchEvent(evt);
                }else{
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_FULL_BOX);
                    cc.director.dispatchEvent(evt);
                }
        }.bind(this));
    },
    //测试自定义空间值
    testStoreValue(){
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    },
    test_sv_string(){
        //String
        qg.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    },
    test_sv_list(){
        //List
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 qg.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    qg.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    qg.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            qg.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    },
    test_sv_set(){
        //Set
        qg.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                qg.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        qg.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                qg.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        qg.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            qg.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    },
    test_sv_hash(){
        //litMap
        qg.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        qg.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                qg.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    },
    test_sv_radom(){
        //testRandom
        qg.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    },
});
