exports.ylsdk_app_id = "30235890"; //游戏APPID
exports.ylsdk_version = "1.0.0";   //游戏版本号
exports.ylsdk_debug_model = true;   //是否debug模式 [true:打印日志,false:不打印日志]
exports.login_by_code = false;      //配置为true，则每次都会走平台登录
exports.ylsdk_platform = 2, 		//平台[ 0:微信,1:QQ,2:OPPO,3:VIVO,4:字节跳动,5:百度,6:4399,7:趣头条,8:360,9:陌陌 ]
exports.ylsdk_pkg_name="ydhw.wfdzy.nearme.gamecenter"; //游戏包名

exports.side_min_num = 20;       //侧边栏列表item最小保留数(基于曝光策略)
                      
exports.ylsdk_banner_ids = [	//banner广告ID(微信、QQ、字节跳动)
]; 
exports.ylsdk_video_ids = [		//激励视频广告ID(微信、QQ、字节跳动)
];							  
exports.ylsdk_grid_ids = [		//格子广告ID(微信)
];
exports.ylsdk_interstitial_ids = [	//插屏广告ID(微信)
];
exports.ylsdk_template_id = [	 //模板分享ID(字节跳动)
];	
/****************微信公众平台配置以下合法域名************************/

// 服务器地址：		   https://api.ylxyx.cn

/*****************************************************************/
