/**
 * 网格卖量布局
 **/
cc.Class({
    extends: cc.Component,
    properties: {
        bItem:cc.Prefab,
        ndoeContent:cc.Node,
    },

    start () {
        cc.director.on(window.Global.EVENT_REFRESH_SIDEBOX, this.onRefreshSideBox.bind(this)); 
    },
    //刷新卖量列表
    onRefreshSideBox(){
        qg.ylSideBox(function (data) {
            this.boxDatas = data;
            this.getSideboxBack(data,false);                
        }.bind(this));
    },
    /**
    * 显示网格卖量布局
    *  @param _x x坐标
    *  @param _y y坐标
    */
    showView(_x,_y){
        this.node.x = _x;
        this.node.y = _y +this.node.height/2;
        if(!this.items) this.items = new Array();
        this.node.active = true;
        if(this.items.length > 0){
            this.dropItemAnim();
        }else{
            if(this.boxDatas){
                this.getSideboxBack(this.boxDatas,true);
            }else{
                var that = this
                qg.ylSideBox(function (data) {
                    that.boxDatas = data;
                    that.getSideboxBack(data,true);                
                }.bind(this));
            }
        }
    },

    getSideboxBack(data,runAnim) {
        this.ndoeContent.removeAllChildren();
        this.items = [];
        if (data && data.length > 0) {
            let count =  data.length > 6 ? 6 : data.length;
            this.loadGameIcon(0);
            let left = -238,space_left = 236,top=96,bottom = -123,space_top = 219;
            for(let i=0;i<count;i++){
                let x_i = i%3;
                let y_i = Math.floor(i/3);
                let element = data[i];
                let box = cc.instantiate(this.bItem);
                box.parent = this.ndoeContent;
                box.x = left + space_left*x_i;
                box.y = bottom + space_top*y_i;
                box.getComponent('ylBoxGridItem').setInfo(element,'MainView');
                this.items.push(box);
            }
            if(runAnim) this.dropItemAnim();
        }
    },

    //
    dropItemAnim(){
        let delayT = 0;
        this.items.forEach(element => {
            element.y = element.y + 800;
            element.stopAllActions();
            element.runAction(cc.sequence(cc.delayTime(delayT),cc.moveTo(0.3, element.x, element.y-800)));
            delayT +=0.2;
        });
    },

    /**
     * 加载侧边栏盒子item的游戏ICON
     * @param index 列表ID
    **/
    loadGameIcon(index) {
        let self = this;
        if (index < this.boxDatas.length) {
            let data = this.boxDatas[index];
            if (!data.texture && data.icon && data.icon !== '') {
                cc.loader.load(data.icon, (err, texture) => {
                    if (!err) {
                        self.boxDatas[index].texture = texture;
                        let evt = new cc.Event.EventCustom(window.Global.EVENT_LOAD_GRID_ICON + data._id);
                        evt.setUserData(texture);
                        cc.director.dispatchEvent(evt);
                    }
                    index++;
                    self.loadGameIcon(index);
                });
            } else {
                index++;
                self.loadGameIcon(index);
            }
        }
    },

    onClose(){
        this.node.active = false;
    },
});
