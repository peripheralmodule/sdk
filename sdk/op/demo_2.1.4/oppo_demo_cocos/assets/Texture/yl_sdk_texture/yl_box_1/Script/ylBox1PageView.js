
cc.Class({
    extends: cc.Component,

    properties: {
        preItem:cc.Prefab,
        pv:cc.PageView,
        nodeContent:cc.Node,
    },

    start () 
    {
        this.timeDt = 4
        this.index = 0
        this.step = 1
    },

    setData(datas)
    {
        // let pv = this.node.getComponent(cc.PageView)        
        for(let i = 0; i < datas.length; i++)
        {
            let item = datas[i];
            if(item.iconBanner && item.iconBanner != ""){
                let preItem = cc.instantiate(this.preItem)
                preItem.position = cc.v2(0, 0);
                preItem.active = true;
                let cellScript = preItem.getComponent("ylBox1PageViewItem")
                cellScript.setItemConf(item,this.scene_name)
                this.pv.addPage(preItem)
            }

        }

        this.totalCount = datas.length
    },

    onPageEvent (sender, eventType) 
    {
        // 翻页事件
        if (eventType !== cc.PageView.EventType.PAGE_TURNING) {
            return;
        }

        this.index = sender.getCurrentPageIndex()
    },
});
