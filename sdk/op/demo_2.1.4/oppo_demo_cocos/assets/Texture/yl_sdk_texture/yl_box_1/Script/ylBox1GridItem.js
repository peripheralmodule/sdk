cc.Class({
    extends: cc.Component,

    properties: {
        spIcon:cc.Sprite,
        lbName:cc.Label,
        nodeLight:cc.Node,
    },

    setItemConf(obj,_scene_name){
        this.cfg = obj;
        this.scene_name = _scene_name;
        this.loadImg(obj.icon);
        this.lbName.string = obj.title;
    },
    setTypeBarAndLocation(typeBar,location){
        this.typeBar = typeBar;
        this._location = location;
    },

    loadImg(url) {
        if (!url) {
            console.error('gameBtn数据有误 cfg: ' + JSON.stringify(this.cfg));
            return;
        }
        var self = this;
        if (!wx) {
            cc.loader.load(url, function (err, tex) {
                if (!err) {
                    var spFrame = new cc.SpriteFrame();
                    spFrame.setTexture(tex);
                    self.setRes(spFrame);
                } else {
                    console.error('拉取资源失败！');
                }
            });
        } else {
            var img = qg.createImage();
            img.onload = function() {
                var tex = new cc.Texture2D();
                tex.initWithElement(img);
                tex.url = url;
                tex.handleLoadedTexture();
                self.setRes(new cc.SpriteFrame(tex));
            }.bind(this);
            img.src = url;
        }
    },

    clickCallBack() {
        qg.ylNavigateToMiniGame(
            {
                _id: this._obj._id,
                toAppid:this._obj.toAppid,   
                toUrl:this._obj.toUrl,
                type:this._obj.type,
                showImage:this._obj.showImage,
                source:this.scene_name+"Grid",//从哪个模块导出的，该字段具体值由调用方自行定义
            },
            function(success){
                if(success){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_REFRESH_SIDEBOX);
                    cc.director.dispatchEvent(evt);
                }else{
                    //干点什么
                }
        }.bind(this));
    },
    // 改变纹理
    setRes(spriteFrame) {
        let size = this.spIcon.node.getContentSize();
        this.spIcon.spriteFrame = spriteFrame;
        // this.spIcon.spriteFrame = new cc.SpriteFrame(tex);
        this.spIcon.node.setContentSize(size);
    },

    startAnimLight(){
        this.nodeLight.getComponent(cc.Animation).play();
    },

    stopAnimLight(){
        this.nodeLight.getComponent(cc.Animation).stop();
    },
});
