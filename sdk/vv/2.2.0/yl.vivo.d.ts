declare namespace qg {
    /************************************************************* */
    /*****************       影流SDK-VIVO        ***************** */
    /************************************************************* */
  
       /**
       * 初始化SDK
       *
       * @param _callback   
       *  返回参数说明：
       *  date true:初始化成功、false:初始化失败、
       *      'config_success'：获取体力配置成功、'config_fail':获取体力配置失败(2.1.3版本接口增加的回调参数)
       *
       **/
      export function ylInitSDK(callback:Function):void;
      /**
       * 获取侧边栏列表
       *
       * @param _callback
       *
       *
       * 返回参数说明：
       * 参数                  类型                           说明
       * result            JSON数组            侧边栏数据，详细数据见下
       *  |
       *  result:
       *         参数              类型          说明
       *         _id             Integer     项目id
       *         type            Integer     该项目类型，0表示小程序，1表示图片
       *         title           String      标题
       *         icon            String      图标url
       *         toAppid         String      跳转小程序appid  (type=0时)
       *         toAppid         String      跳转小程序appid  (type=0时)
       *         toUrl           String      跳转小程序path  (type=0时)
       *         shieldIos       Integer     是否屏蔽ios，0不屏蔽，1屏蔽  (type=0时)
       *         bannerImage     String      仿banner图片
       *         rectangleImage  String      长方形图片
       *         frameImage      String      合成帧图(可用于做动态卖量图的帧资源)
       *         frame           Integer     帧数
       *         showImage       String      图片url  (type=1时)
       */
      export function ylSideBox(callback:Function):void;
  
      /**
       * 获取积分墙列表
       *
       * @param _callback
       *
       *
       * 参数返回说明:         
       *  参数名称       参数类型      说明     
       *  result  JSON数组    积分墙数据，详细数据见下 
       *   |
       *  result: 
       *       参数          类型          说明
       *       _id         Integer     项目id，前端按此值从小到大排序
       *       awardStatus String      奖励状态，1表示可领取，0表示已领取
       *       title       String      标题
       *       awardList   JSON数组     奖励数据，具体见下
       *       playTime    Integer     试玩多久得到奖励，单位秒
       *       type        Integer     0表示小程序
       *       icon        String      图标url
       *       desc        String      该项目的描述信息
       *       toAppid     String      跳转小程序appid(type=0时)
       *       toUrl       String      跳转小程序path(type=0时)
       *       shieldIos   Integer     是否屏蔽ios，0不屏蔽，1屏蔽(type=0时)
       *
       *       awardLis:
       *           参数      类型      说明
       *           type    String  奖励类型[gold:金币,diamond:钻石,spirit:体力,coupon:券] 
       *           value   String  奖励具体数值
       */
      export function ylIntegralWall(callback:Function):void;
  
      /**
       * 领取积分墙奖励
       * @param _id 积分墙ID
       * @param _callback
       *
       *  响应结果
       *   参数名称             参数类型               说明   
       *   award               JSON数组          奖励数据，具体见下
       *    |
       *   award:    对象         类型          说明
       *             type        String      奖励类型
       *             value       String      奖励具体数值
       *
      **/
      export function ylGetBoardAward(_id:any,callback:Function):void;
      /**
       * 结果统计
       * @param detail 用户该局游戏的技能及分数统计详情（每个游戏需要统计的数据不同)
       *               例如，泡泡龙大师：{"total_score": 123 , "rebirth_score": 123, "strategy": "1", "change_score": [123, 123...],"hammer_score": [123, 123, ...]}
       *               total_score 用户在游戏中一句的中分数
       *               rebirth_score 用户在一句中使用复活时的分数
       *               strategy 当前使用的策略，比如："1"
       *               change_score 换一换功能每一次使用时获得的分数
       *               hammer_score 使用锤子功能每一次使用时获得的分数
       * @param _callback
       *
       */
      export function ylStatisticResult(_detail:any,callback:Function):void;
      /**
       * 视频统计(打点统计）
       * @param _type 类型[0:显示视频,1:播放完成]
       * @param _adId 视频广告ID
       * @param _callback
       *
       */
      export function ylStatisticViedo(_type:any,_adId:any,callback:Function):void;
      /**
       * 获取视频分享策略
       * @param channel 渠道名
       * @param model   模块名
       * @param callback 回调函数 返回值:[0:无策略,1:分享,2:视频] 
      **/
      export function ylShowShareOrVideo(channel:String,module:String,callback:Function):void;
      /**
       * 使用视频分享策略
       * @param channel 渠道名
       * @param model   模块名
       * @param callback 回调函数 返回值:[0:无策略,1:分享,2:视频] 
      **/
      export function ylUseShareOrVideoStrategy(channel:String,module:String,callback:Function):void;
      /**
       * 事件统计
       * @param        参数         类型          说明 
                      eventName   String         事件名
       *
       */
      export function ylEventCount(eventName:any):void;
  
      /**
       * 获取游戏自定义配置
       * @param callback
       *
       * 返回参数说明：
       * 参数                类型           说明
       *  result     JSON数组  游戏配置列表，详细数据见下
       *       |
       *       result:
       *           参数      类型          说明
       *           _id     Integer     ID
       *           name    String      参数名(尽量用英文，用以前端区分不同功能)
       *           type    String      类型[1:值配置,2:开关配置]
       *           value   String      参数值，type=1时，值为CP后台配置的值，type=2时值为开关状态:["0":关闭，"1":打开]
       *           desc    String      参数描述
       */
      export function ylGetCustom(callback:Function):void;
  
  
      /**
       * 获取游戏APPID
      **/
      export function ylGetAppID ():any;
      /**
      * 获取邀请账号
      **/
      export function ylGetInviteAccount():any;
      /**
      * 获取用户SDK服务器账号信息
      **/
      export function ylGetUserInfo():any;
      /**
      * 获取开关信息
      **/
      export function ylGetSwitchInfo():any;
      /**
      * 日志输出
      * 
      * @param logMsg True  string  日志内容
      * @param logType  False String  日志类型(默认为'log'): ['log','debug','info','warn','error']
      *
      **/
      export function ylLog(logMsg:string,logType:string):void;
      
      /**
      * 自定义空间
      * @param info   对象 自定义空间存取参数(具体请查看API文档)
      *           ｜
      *           info:
      *              参数       类型          说明
      *              name      string        空间名称(需与后台配置的一致)
      *              cmd       string        空间操作指令
      *              args      string        操作的值
      *
      *   @param _callback 操作结果返回键听
      *
      *
      *   说明：
      *   name: 由后台配置，该名称决定查找后台配置其对应的类型，
      *         类型有：字符变量、字符数组、字符集合、字符散列、随机数
      *
      *   cmd:  根据name对应的类型，有不的操作指令
      *          字符变量：[
                              get:取值，返回后台配置的默认值或set的值、
                              set:赋值，返回后台是否操作成功 true false
                          ]
      *          字符数组：[
                              all:取值, 返回全部值的数组、
                              get:取值,返回 args 定义下标的值、
                              add:赋值,添加，返回后台是否操作成功 true false、
                              set:赋值,替换 args 为需要赋予的下标和值用逗号"," 分割，例如："2,text"，返回后台是否操作成功 true false、
                              replace:替换， args 为需要赋予的json字符数组、例如："[\"1\",\"2\",\"3\",\"4\",\"5\"]"，返回后台是否操作成功 true false
                              size:取值，返回该数组的值数量、
                              poll:取值，返回该数组的第一个值并从后台删除
                          ]
      *          字符集合：[
                              all：取值，返回全部值的数组
                              exist：取值，args 为需要判断的值，返回该值是否存在 true false
                              add：赋值，args 为需要添加的值，返回后台是否操作成功 true false
                              replace：替换，args 为需要赋予json字符数组，例如： "[\"1\",\"2\",\"3\",\"4\",\"5\"]"，返回后台是否操作成功 true false
                              size：取值操作 value 返回该集合的值数量
                              del：赋值， args 为需要删除的值，返回后台是否操作成功 true false
                          ]
      *          字符散列：[
                              all：取值，返回全部值的散列
                          get：取值，args 为需要获取的键，返回该键对应的值
                          gets：取值，args 为需要获取的键列表用逗号"," 分割 ，例如："key1,key2,key3"，返回全部值的数组
                          values：取值，返回全部值的数组
                          set：赋值，args 为需要赋予的键值用逗号"," 分割 ，例如："key,value"，返回后台是否操作成功 true false
                          replace：替换，args 为需要赋予json字符对象 例如："{\"key1\":\"value1\",\"key2\":\"value2\"}"，返回后台是否操作成功 true false
                          size：取值，返回该散列的值数量
                          del：赋值，args 为需要删除的键，返回后台是否操作成功 true false
                          ]
      *          随机数：无
          args：
      *           字符变量：[
                              get:无
                              set:需要赋予的值
                          ]
      *          字符数组：[
                              all:无
                              get:数组下标
                              add:需要添加的值、
                              set:需要赋予的下标和值用逗号"," 分割，例如："2,text"
                              replace:需要赋予的json字符数组、例如："[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                              size:无
                              poll:无
                          ]
      *          字符集合：[
                              all：无
                              exist：需要判断的值
                              add：需要添加的值
                              replace：需要赋予json字符数组，例如： "[\"1\",\"2\",\"3\",\"4\",\"5\"]"，
                              size：无
                              del：为需要删除的值，
                          ]
      *          字符散列：[
                          all：无
                          get：需要获取的键
                          gets：需要获取的键列表用逗号"," 分割 ，例如："key1,key2,key3"
                          values：无
                          set：需要赋予的键值用逗号"," 分割 ，例如："key,value"
                          replace：需要赋予json字符对象 例如："{\"key1\":\"value1\",\"key2\":\"value2\"}"
                          size：无
                          del：需要删除的键，
                          ]
      *          随机数：无
  
      *   callback 返回：
      *          字符变量：[
                              get:返回后台配置的默认值或set的值、
                              set:返回后台是否操作成功 true false
                          ]
      *          字符数组：[
                              all:返回全部值的数组、
                              get:返回 args 定义下标的值、
                              add:返回后台是否操作成功 true false、
                              set:返回后台是否操作成功 true false、
                              replace:返回后台是否操作成功 true false
                              size:返回该数组的值数量、
                              poll:返回该数组的第一个值并从后台删除
                          ]
      *          字符集合：[
                              all：返回全部值的数组
                              exist：返回该值是否存在 true false
                              add：返回后台是否操作成功 true false
                              replace：返回后台是否操作成功 true false
                              size：返回该集合的值数量
                              del：返回后台是否操作成功 true false
                          ]
      *          字符散列：[
                              all：返回全部值的散列
                              get：返回该键对应的值
                              gets：返回全部值的数组
                              values：返回全部值的数组
                              set：返回后台是否操作成功 true false
                              replace：返回后台是否操作成功 true false
                              size：返回该散列的值数量
                              del：返回后台是否操作成功 true false
                          ]
      *          随机数：返回同样长度的随机数
      *
      **/
      export function ylStoreValue(info:any,callback:Function):void;
    /**
     * 切换页面
     * SDK将根据云控banner定时刷新配置，和当前状态判断是否强制刷新banner
     * @param misToch 是否为误触banner: [true:是,false:否]默认：false
     */
    export function ylChangeView(misToch:Boolean):void;
    /**
     * 获取深度误触屏蔽开关状态
     *
     * @param customNum 关卡数
     * 
     * 响应结果:
     * {
     *   "deepTouch":  "1", // "0":误触开,"1":误触关,
     *   "customInfo":[
     *       {
     *       "_id": 1382,   //ID
     *       "name": "dawd",//参数名(后台配置的)
     *       "type": "2",  //类型[1:值配置,2:开关配置]
     *       "value": "0", //值，地区屏蔽状态type==1时value值为空串，type=2时value值为['0':关，'1':开]请CP做好判断
     *       "desc": ""    //参数描述
     *       }
     *   ]
     * }
     */
    export function ylGetDeepTouch(customNum):any;
    /**
     * 一局游戏结束
     * 
     * @param customNum 当前关卡数
     */
    export function ylOverGame(customNum:number):void;
    /**
     * 视频解锁
     * 
     * @param customNum 当前关卡数
     * 
     * 响应结果:
     *  unlockStatus  //是否可以视频解锁[true:是,false:否]
     */
    export function ylVideoUnlock(customNum:number):Boolean;
    /**
     * 获取体力相关信息
     *
     * @param callback 回调函数
     *
     * 响应结果:
     *   {
     *       "defaultPower": 5,     //初始体力
     *       "powerUpper": 5,       //体力上限
     *       "recoveryTime": 300,   //体力自动恢复时间
     *       "getPower": 1          //看视频获得体力值
     *   }
     */
    export function ylGetPowerInfo(callback:Function);
    /**
     * 体力变化监听
     * @param callback 回调函数
     * 
     * 回调返回: type[1:视频,2:定时自动恢复,3:调用ylSetPower]
     */
    export function ylOnPowerChange(callback:Function):void;
    /**
     * 设置体力
     * 
     * @param powerNum 当前体力值
     */
    export function ylSetPower(powerNum:number):void;
    /**
     * 获取体力
     * 
     * 响应结果:
     * powerNum  当前体力值
     */
    export function ylGetPower():number;
    /**
    * 创建banner广告(填充屏幕宽度)
    * @param show  是否显示: [true:是,false:否]默认：false
    * @param _callback 
    * @param misToch 是否为误触banner: [true:是,false:否]默认：false
    **/
    export function ylBannerAdCreate(show:Boolean, _callback:Function, misToch:Boolean):void;
    /**
    * 创建banner自定义style
    *
    * @param style banner的Style，具体传值请参考平台官方 
    * @param show  是否显示: [true:是,false:否]默认：false
    * @param _callback 
    * @param misToch 是否为误触banner: [true:是,false:否]默认：false
    **/
    export function ylBannerAdCreateByStyle(style:any, show:Boolean, _callback:Function, misToch:Boolean):void;
    /**
    * 显示banner广告
    **/
    export function ylBannerAdShow():void;
    /**
    * 隐藏 banner广告
    **/
    export function ylBannerAdHide():void;
    /**
      * 创建插屏广告
      *   @param show 是否展示 [true:展示,false:不展示]
      *   @param callback 回调函数 [type:0:创建或展示失败、1:创建或展示成功、2:关闭]
    **/
    export function ylCreateInterstitialAd(show:Boolean, callback:Function):void;
    /**
    * 显示插屏广告
    **/
    export function ylShowInterstitialAd():void;
    /**
    * 创建视频广告
    **/
    export function ylCreateVideoAd():void;
    /**
    *    播放视频广告
    * @param callback
    *   ---type:[
                  0：视频广告-播放失败,
                  1：视频广告-播放完成,
                  2：视频广告-播放取消,
                  3：视频广告-加载成功,
                  4：视频广告-显示
               ]
    *    
    * @param unlockCustomNum 解锁关卡
    *
    **/
    export function ylShowVideoAd(callback:Function,unlockCustomNum:number):void;
    /**
    *    播放视频广告
    *    
    *
    * @param info 参数对象
    *       参数           必选     类型       说明
    *      callBack        True    Function   接口回调
              ---type:[
                        0：视频广告-播放失败,
                        1：视频广告-播放完成,
                        2：视频广告-播放取消,
                        3：视频广告-加载成功,
                        4：视频广告-显示
                     ]
    *   
    *      unlockCustomNum False   Integer    需要视频解锁的关卡
    *      getPower        False   Boolean    看完视频是否获得体力
    *
    *
    **/
    export function ylShowVideoAd2(info:any):void;
    /**
    *    创建原生广告
    *    
    * @param callback
    *
    *    参数               类型      说明
    *    adId             string  广告标识，用来上报曝光与点击
    *    title            string  广告标题
    *    desc             string  广告描述
    *    icon             string  推广应用的 Icon 图标
    *    imgUrlList       Array   广告图片
    *    logoUrl          string  “广告”标签图片
    *    clickBtnTxt      string  点击按钮文本描述
    *    creativeType     number  获取广告类型，取值说明：  0：无   1：纯文字   2：图片   3：图文混合   4：视频   6. 640x320 大小图文混合   7. 320x210 大小图文单图   8. 320x210 大小图文多图
    *    interactionType  number  获取广告点击之后的交互类型，取值说明：   0：无   1：浏览类   2：下载类   3：浏览器（下载中间页广告）   4：打开应用首页   5：打开应用详情页
    *
    *
    **/
    export function ylNativeAdCreate(callback:Function):void;
    /**
    *    显示原生广告上报
    *    
    * @param _adId 广告标识，用来上报曝光与点击
    *
    **/
    export function ylNativeAdShow(_adId:String):void;
    /**
    *    点击原生广告上报
    *    
    * @param _adId  广告标识，用来上报曝光与点击
    *
    **/
    export function ylNativeAdClick(_adId:String):void;
    /***
       * 生命周期监听
       * 
       * @param _callback
       * 
       * 参数返回说明:         
       *  参数名称       参数类型          说明     
       *  type            String     'onShow'、'onHide'、'onError'三个状态
       *  res             any         onShow和onError为回调返回参数，onHide是为空
    */
    export function ylGameLifeListen(callback:Function):void;
    /**
     *  玩家玩过的关卡数
     * @param type 获取类型[1:最后一次玩的关卡，2:总玩过的最大关卡]
     **/
    export function ylGetPlayCustom(type:number):number;
    /**
     *  获取玩家玩游戏的局数
     * @param type 获取类型[1:今天玩的局数，2:总共玩的局数]
     **/
    export function ylGetPlayCount(type:number):number;
     /**
     *  获取玩家今天看视频的次数
     *
     *  
     **/
    export function ylGetWatchTvNum():number;
    /**
     * 获取流失统计列表
     * 
     * @param callback 
     */
    export function ylGetLayerList(callback:Function):void;
    /**
     * 流失统计埋点
     * 
     * @param layerPath 路径
     * @param callback 
     */
    export function ylStatisticLayer(layerPath:string):void;
    /**
     * 获取服务端信息
     * 
     * @param callback 
     */
    export function ylGetServerInfo(callback:Function):void;

  }