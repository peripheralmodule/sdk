SDK文件说明:
	1.yl.vivo.d.ts  接口提示文件。
	2.utils/yl_sdk_conf.js  SDK配置文件。
	3.yl_sdk.js 	SDK服务端公共接口文件(CP无需了解内容)。
	4.yl_sdk_vivo.js  SDK平台接口文件(CP无需了解内容)。

SDK配置：
	1.将utils目录复制到工程代码目录下
	2.在yl_sdk_conf.js中配置游戏相关信息
	3.配置以下域名为合法域名:
		1>服务器地址：		   https://api.ylxyx.cn
		2>图片服务器地址1：	   https://ql.ylxyx.cn
		3>图片服务器地址2：     https://tx.ylxyx.cn
		4>图片服务器地址3：	   https://ext.ylxyx.cn
	4.TS语言的工程需要复制yl.vivo.d.ts文件到工程的对应目录。