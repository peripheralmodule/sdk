import { ui } from "./../ui/layaMaxUI";
/**
 * 本示例采用非脚本的方式实现，而使用继承页面基类，实现页面逻辑。在IDE里面设置场景的Runtime属性即可和场景进行关联
 * 相比脚本方式，继承式页面类，可以直接使用页面定义的属性（通过IDE内var属性定义），比如this.tipLbll，this.scoreLbl，具有代码提示效果
 * 建议：如果是页面级的逻辑，需要频繁访问页面内多个元素，使用继承式写法，如果是独立小模块，功能单一，建议用脚本方式实现，比如子弹脚本。
 */
export default class GameUI extends ui.test.TestSceneUI {
    /**设置单例的引用方式，方便其他类引用 */
    static instance: GameUI;
    private custom:number;
    private nativeAdList:any = null;
    constructor() {
        super();
        GameUI.instance = this;
        //关闭多点触控，否则就无敌了
        Laya.MouseManager.multiTouchEnabled = false;
    }

    onEnable(): void {
        this.custom = 0;
        console.log("GameUI-platform:",window.navigator.platform);
        qg.ylGameLifeListen(function(type:string,res:any){
            if(type === 'onShow'){
                console.warn("YLSDK GameUI-onShow--res:",res);
            }else if(type === 'onHide'){
                console.warn("YLSDK GameUI-onHide");
            }else if(type === 'onError'){
                console.warn("YLSDK GameUI-onError--res:",res);
            }
        });
        qg.ylOnPowerChange(function(type){
            console.warn("YLSDK GameUI-ylOnPowerChange back[1:视频,2:定时自动恢复,3:ylSetPower]:",type);
        }.bind(this));
        qg.ylInitSDK(function(success:any){
            console.log("HelloWorld ---ylInitSDK---初始化完成");
            if(success === true){
                console.warn("HelloWorld-ylInitSDK-初始化成功");
                this.doSDKinterface();
            }else if(success == 'config_success' || success == 'config_fail'){
                console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
                qg.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("HelloWorld-ylGetPowerInfo:",data);
                });
            }else{
                console.warn("HelloWorld-ylInitSDK-初始化失败");
            }
        }.bind(this));
        this.initUI();
    }
    initUI():void{
        this.btnVideoOrShare.on(Laya.Event.CLICK,this,this.onShowShareOrVideo);
        this.btnVideoUnlockCustoms.on(Laya.Event.CLICK,this,this.onVideoUnlockCustoms);
        this.btnShowVideo.on(Laya.Event.CLICK,this,this.onShowVideo);
        this.btnShowBanner.on(Laya.Event.CLICK,this,this.onShowBanner);
        this.btnOver.on(Laya.Event.CLICK,this,this.onOver);
        this.btnGetDeepTouch.on(Laya.Event.CLICK,this,this.onGetDeepTouch);
        this.btnConsumePower.on(Laya.Event.CLICK,this,this.onConsumePower);
        this.btnAddPower.on(Laya.Event.CLICK,this,this.onAddPower);
        this.btnShowInterstitial.on(Laya.Event.CLICK,this,this.onShowInterstitial);
    }
    //显示视频广告
    onShowVideo(e:Laya.Event):void{
        // qg.ylBannerAdHide();
        // qg.ylShowVideoAd(function(){},this.custom);
        qg.ylShowVideoAd2({
            callBack:function(){}, 
            unlockCustomNum:this.custom,
            getPower:true
        });
        console.log("HelloWorld ---onShowVideo---");
    }
    //显示banner
    onShowBanner(e:Laya.Event):void{
        qg.ylBannerAdCreate(true,function(){
            
        },true);
        console.log("HelloWorld ---onShowBanner--");
    }
    //视频解锁关卡
    onVideoUnlockCustoms(e:Laya.Event):void{
        let unlock = qg.ylVideoUnlock(this.custom);
        console.warn("HelloWorld-onVideoUnlockCustoms-是否解锁:",unlock);
    }
    //游戏结束
    onOver(e:Laya.Event):void{
        this.custom +=1;
        console.log("HelloWorld ---onOver--custom:",this.custom);
        qg.ylOverGame(this.custom);
    }
    //获取深度误触开关
    onGetDeepTouch(e:Laya.Event):void{
       let info = qg.ylGetDeepTouch(this.custom);
       console.warn("HelloWorld onGetDeepTouch-info: ",JSON.stringify(info));
    }
    //消耗体力
    onConsumePower(e:Laya.Event):void{
       let power =  qg.ylGetPower();
       console.log("HelloWorld ---onConsumePower--power:",(power -1));
       qg.ylSetPower(power -1);
    }
    //视频分享策略
    onShowShareOrVideo(e:Laya.Event):void{
        qg.ylShowShareOrVideo("chanel_2","model_2",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    break;
                case 2:
                    console.warn("------策略-视频");
                    break;
            }
        }.bind(this));
    }
    //增加体力
    onAddPower(e:Laya.Event):void{
        let power =  qg.ylGetPower();
        console.log("HelloWorld ---onAddPower--power:",(power -1));
        qg.ylSetPower(power +1);
    }
    //原生广告-创建
    onCreateNativeAd():void{
        qg.ylNativeAdCreate(function(data){
            this.nativeAdList = data; 
            console.warn("HelloWorld ---onCreateNativeAd---ylNativeAdCreate:",JSON.stringify(data));
            
        }.bind(this));
    }
    //原生广告-显示上报
    onShowNativeAdReport():void{
        if(this.nativeAdList){
            let nativeAd = this.nativeAdList[0];
            console.log("HelloWorld --onShowNativeAdReport-nativeAd.adId:",nativeAd.adId.toString());
            qg.ylNativeAdShow(nativeAd.adId.toString());
        }else{
            console.log("HelloWorld --onShowNativeAdReport-nativeAdList is null");
        }
    }
    //原生广告-点击上报
    onClickNativeAdReport():void{
        if(this.nativeAdList){
            let nativeAd = this.nativeAdList[0];
            console.log("HelloWorld --onClickNativeAdReport-nativeAd.adId:",nativeAd.adId.toString());
            qg.ylNativeAdClick(nativeAd.adId.toString());
        }else{
            console.log("HelloWorld --onShowNativeAdReport-nativeAdList is null");
        }
    }
    // //加载原生广告
    // onLoadNativeAd():void{
    //     qg.ylNativeAdLoad(function(status){
    //         console.warn("HelloWorld ---onLoadNativeAd---ylNativeAdLoad:",status);
    //     });
    // }
    //展示插屏广告
    onShowInterstitial(e:Laya.Event):void{
        console.log("HelloWorld ---onShowInterstitial---");
        qg.ylCreateInterstitialAd(true,function(type){
            let textType = "";
            // [type:0:创建或展示失败、1:创建或展示成功、2:关闭]
            switch(type){
                case 0:
                    textType = "创建或展示失败";
                    break;
                case 1:
                    textType = "创建或展示成功";
                    break;
                case 2:
                    textType = "关闭";
                    break;
            }
            console.log("HelloWorld ---onShowInterstitial---",textType);
        });
    }
    //调用SDK接口
    doSDKinterface(){
        qg.ylLog("---调用接口---",'log');
        qg.ylEventCount("start_game");
        qg.ylSideBox(function (data:any) {
        }.bind(this));
        qg.ylGetCustom(function(data:any){
        }.bind(this));
        qg.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status){
            if(status){
                //干点什么
            }
        }.bind(this));
        let userInfo = qg.ylGetUserInfo();
        console.log("---用户信息---userInfo:",JSON.stringify(userInfo));
        let appid = qg.ylGetAppID();
        console.log("------appid:",JSON.stringify(appid));
        let inviteAccount = qg.ylGetInviteAccount();
        console.log("------inviteAccount:",JSON.stringify(inviteAccount));
        let switchInfo  = qg.ylGetSwitchInfo();
        console.log("------开关信息-switchInfo:",JSON.stringify(switchInfo));
        qg.ylIntegralWall(function (data:any) {
            qg.ylGetBoardAward(12,function(data:any){
                    console.log("------领取积分墙奖励:",data);
            }.bind(this));
        }.bind(this));
        qg.ylStatisticViedo(0,"212456",function(data:any){
            console.log("------视频播放统计-ylStatisticViedo:",JSON.stringify(data));
        }.bind(this));
        qg.ylGetLayerList(function(data){
            if(data && data.length >0){
                this.layerList = data;
                qg.ylStatisticLayer(this.layerList[this.layerList.length-1].layerPath);
            }
        }.bind(this));
        // this.testStoreValue(qg);
    }
    //测试自定义空间值
    testStoreValue(qg:any):void{
        this.test_sv_string(qg);//字符变量测试代码
        this.test_sv_list(qg);//字符数组测试代码
        this.test_sv_set(qg);//字符集合测试代码
        this.test_sv_hash(qg);//字符散列测试代码
        this.test_sv_radom(qg);//随机数测试代码
    }
    test_sv_string(qg:any):void{
        //String
        qg.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    }
    test_sv_list(qg:any):void{
        //List
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                qg.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    qg.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    qg.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            qg.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    }
    test_sv_set(qg:any):void{
        //Set
        qg.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                qg.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        qg.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                qg.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        qg.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            qg.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    }
    test_sv_hash(qg:any):void{
        //litMap
        qg.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        qg.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                qg.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    }
    test_sv_radom(qg:any):void{
        //testRandom
        qg.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    }
}