cc.Class({
    extends: cc.Component,

    properties: {
    },
    onLoad: function () {
        let that = this;
        this.nativeAdList = null;
        qg.ylOnPowerChange(function(type){
            console.warn("HelloWorld-ylOnPowerChange back[1:视频,2:定时自动恢复]:",type);
        }.bind(this));
        qg.ylInitSDK(function(success){
            console.log("HelloWorld-ylInitSDK:",success);
            if(success === true){
                console.warn("HelloWorld-ylInitSDK-初始化成功");
                this.doSDKinterface();
            }else if(success == 'config_success' || success == 'config_fail'){
                console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
                qg.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("HelloWorld-ylGetPowerInfo:",data);
                });
            }else{
                console.warn("HelloWorld-ylInitSDK-初始化失败");
            }
        }.bind(this));
        this.initVIVOonshow();
    },
    /**
    * 调用SDK接口
    **/
    doSDKinterface(){
        this.custom = 1;
        qg.ylLog("YLSDK ---调用接口---",'log');
        qg.ylSideBox(function (data) {
        }.bind(this));
        qg.ylGetCustom(function(data){
        }.bind(this));
        qg.ylIntegralWall(function(){}.bind(this));
        qg.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status){
            if(status){
                //干点什么
            }
        }.bind(this));
        let userInfo = qg.ylGetUserInfo();
        console.log("YLSDK ---用户信息---userInfo:",JSON.stringify(userInfo));
        let appid = qg.ylGetAppID();
        console.log("YLSDK ------appid:",JSON.stringify(appid));
        let inviteAccount = qg.ylGetInviteAccount();
        console.log("YLSDK ------inviteAccount:",JSON.stringify(inviteAccount));
        let switchInfo  = qg.ylGetSwitchInfo();
        console.log("YLSDK ------开关信息-switchInfo:",JSON.stringify(switchInfo));
        qg.ylStatisticViedo(0,"212456",function(data){
            console.log("YLSDK ------视频播放统计-ylStatisticViedo:",JSON.stringify(data));
        }.bind(this));
        qg.ylGetSignData(function(data){
            console.log("---ylGetSignData:",data);
        });
        //获取分享图列表
        qg.ylShareCard(function (shareInfo) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
                this.btnShare.active = true;
            }else{
                //获取失败
            }
        }.bind(this),'MenuScene');
        qg.ylStatisticShareCard(177);
        qg.ylEventCount("ylEventCount_TEST");
        qg.ylGetLayerList(function(data){
            if(data && data.length >0){
                this.layerList = data;
                qg.ylStatisticLayer(this.layerList[this.layerList.length-1].layerPath);
            }
        }.bind(this));
        // this.testStoreValue();
    },
    //视频分享策略
    onShowShareOrVideo(){
        qg.ylShowShareOrVideo("chanel_2","model_2",function(type){
            switch(type){
                case 0:
                    console.warn("YLSDK ------策略-无");
                    break;
                case 1:
                    console.warn("YLSDK ------策略-分享");
                    break;
                case 2:
                    console.warn("YLSDK ------策略-视频");
                    break;
            }
        }.bind(this));
    },
        //显示视频广告
    onShowVideo(){
        qg.ylBannerAdHide();

        // qg.ylCreateVideoAd();
        qg.ylShowVideoAd(function(status){
            switch(status){
                case 0:
                    console.warn("YLSDK ------视频广告-播放失败");
                    break;
                case 1:
                    console.warn("YLSDK ------视频广告-播放完成");
                    break;
                case 2:
                    console.warn("YLSDK ------视频广告-播放取消");
                    break;
            }
        },this.custom);
        console.log("HelloWorld ---onShowVideo---");
    },
    //显示banner
    onShowBanner(){
        qg.ylBannerAdCreate(true,function(status){
            
        },true);
        // qg.ylBannerAdCreateByStyle({
        //     left: 20,
        //     top: 100,
        //     width: 80,
        // }, false, function(status){
        //     if(status){
        //         qg.ylBannerAdShow();
        //     }
        // }, true);
        console.log("HelloWorld ---onShowBanner--");
    },
    onHideBanner(){
        console.log("HelloWorld ---onHideBanner--");
        qg.ylBannerAdHide();
    },
    //视频解锁关卡
    onVideoUnlockCustoms(){
        let unlock = qg.ylVideoUnlock(this.custom);
        console.warn("HelloWorld-onVideoUnlockCustoms-是否解锁:",unlock);
    },
    //游戏结束
    onOver(){
        this.custom +=1;
        console.log("HelloWorld ---onOver--custom:",this.custom);
        qg.ylOverGame(this.custom);
    },
    //获取深度误触开关
    onGetDeepTouch(){
       let info = qg.ylGetDeepTouch();
       console.warn("HelloWorld onGetDeepTouch-info: ",JSON.stringify(info));
    },
    //消耗体力
    onConsumePower(){
       let power =  qg.ylGetPower();
       console.log("HelloWorld ---onConsumePower--power:",(power -1));
       qg.ylSetPower(power -1);
    },
    //增加体力
    onAddPower(){
        let power =  qg.ylGetPower();
        console.log("HelloWorld ---onAddPower--power:",(power -1));
        qg.ylSetPower(power +1);
    },
    //切换界面,可调用强制刷新Banner接口
    onChangeView(){
        qg.ylChangeView(true);
    },
    //视频分享策略
    onShowShareOrVideo(){
        qg.ylShowShareOrVideo("bb","aa",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    break;
                case 2:
                    console.warn("------策略-视频");
                    break;
            }
        }.bind(this));
    },
    //展示插屏广告
    onShowInterstitial(){
        console.log("HelloWorld ---onShowInterstitial---");
        qg.ylCreateInterstitialAd(true,function(type){
            let textType = "";
            // [type:0:创建或展示失败、1:创建或展示成功、2:关闭]
            switch(type){
                case 0:
                    textType = "创建或展示失败";
                    break;
                case 1:
                    textType = "创建或展示成功";
                    // qg.ylShowInterstitialAd();
                    break;
                case 2:
                    textType = "关闭";
                    break;
            }
            console.log("HelloWorld ---onShowInterstitial---",textType);
        });
    },
    //原生广告-创建
    onCreateNativeAd(){
        qg.ylNativeAdCreate(function(data){
            this.nativeAdList = data; 
            console.warn("HelloWorld ---onCreateNativeAd---ylNativeAdCreate:",JSON.stringify(data));
            
        }.bind(this));
    },
    //原生广告-显示上报
    onShowNativeAdReport(){
        if(this.nativeAdList){
            let nativeAd = this.nativeAdList[0];
            console.log("HelloWorld --onShowNativeAdReport-nativeAd.adId:",nativeAd.adId.toString());
            qg.ylNativeAdShow(nativeAd.adId.toString());
        }else{
            console.log("HelloWorld --onShowNativeAdReport-nativeAdList is null");
        }
    },
    //原生广告-点击上报
    onClickNativeAdReport(){
        if(this.nativeAdList){
            let nativeAd = this.nativeAdList[0];
            console.log("HelloWorld --onClickNativeAdReport-nativeAd.adId:",nativeAd.adId.toString());
            qg.ylNativeAdClick(nativeAd.adId.toString());
        }else{
            console.log("HelloWorld --onShowNativeAdReport-nativeAdList is null");
        }
    },
    //加载原生广告
    onLoadNativeAd(){
        qg.ylNativeAdLoad(function(status){
            console.warn("HelloWorld ---onLoadNativeAd---ylNativeAdLoad:",status);
        });
    },
    //注册小游戏回到前台的事件监听
    initVIVOonshow(){
        console.log("HelloWorld ---注册小游戏回到前台的事件监听---")
        qg.onShow((res) => {
            console.log("HelloWorld ---小游戏回到前台的事件监听")
            //领取积分墙奖励,
            let a_id = 1;//这里的id是随便写的，项目中请使用积分墙列表 id
            qg.ylGetBoardAward(a_id,function(data){
                if(data){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_GET_AWARD);
                    evt.setUserData(data);
                    cc.director.dispatchEvent(evt);
                }
            });
        })
    },
    //测试自定义空间值
    testStoreValue(){
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    },
    test_sv_string(){
        //String
        qg.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    },
    test_sv_list(){
        //List
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 qg.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    qg.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    qg.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            qg.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    },
    test_sv_set(){
        //Set
        qg.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                qg.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        qg.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                qg.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        qg.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            qg.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    },
    test_sv_hash(){
        //litMap
        qg.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        qg.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                qg.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    },
    test_sv_radom(){
        //testRandom
        qg.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    },
});
