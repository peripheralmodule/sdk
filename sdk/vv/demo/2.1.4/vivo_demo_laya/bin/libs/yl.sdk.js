YLsdkLaya = (function () {
    var pc = { //平台号
        OPPO:2,
        VIVO:3,
        MZ:12   //魅族
    };
    var g = {
        ylsdk_app_id : "100001687", //游戏APPID
        ylsdk_version: "1.0.0",   //游戏版本号
        ylsdk_debug_model :true,   //是否debug模式 [true:打印日志,false:不打印日志]
        ylsdk_pkg_name:"com.szydhw.fkcc.vivominigame", //游戏包名
        ylsdk_platform:pc.VIVO, //平台[ 2:OPPO,3:VIVO,12:魅族 ]
        side_min_num :20,       //侧边栏列表item最小保留数(基于曝光策略)
        ylsdk_banner_ids : [	//banner广告ID
            '78a20654bf794f63a3d8104c6e4af30d'
        ],
        ylsdk_video_ids : [		//激励视频广告ID
            '70b33f51e34e4945af714a64df7c09d3'
        ],	
        ylsdk_interstitial_ids : [	//插屏广告ID
            'd1d5388a871e40379dc9b3c51eb1518b'
        ],					  
    };
    var sys = {};
    //系统信息
    var systemInfo = {
        getSystemInfoByMEIZU(){
            try {
                var A = qg.getSystemInfoSync();
                sys.brand = A.brand,    //  设备品牌   
                sys.model = A.model,    //  设备型号   
                sys.pixelRatio = A.pixelRatio,      //  设备像素比
                sys.screenWidth = A.screenWidth,    //  屏幕宽  
                sys.screenHeight = A.screenHeight,  //  屏幕高 
                sys.language = A.language,          //  系统语言
                sys.COREVersion = A.COREVersion,    //  版本号
                sys.platform = A.osType,      //  操作系统名称
                sys.system = A.system,        //  操作系统版本
                sys.platformVersion = A.platformVersion,    //  运行平台版本号
                console.log("YLSDK getSystemInfoByMEIZU:",JSON.stringify(sys));
            } catch(e) {
                console.log("YLSDK exception-meizu:",e);
            }
        },
        getSystemInfoByVIVO(){
            try {
                var A = qg.getSystemInfoSync();
                sys.brand = A.brand,    //  设备品牌   
                sys.model = A.model,    //  设备型号   
                sys.screenWidth = A.screenWidth,    //  屏幕宽  
                sys.screenHeight = A.screenHeight,  //  屏幕高 
                sys.language = A.language,          //  系统语言
                sys.manufacturer = A.manufacturer,  //  设备生产商
                sys.product = A.product,    //  设备代号
                sys.platform = A.osType,      //  操作系统名称
                sys.osVersionName = A.osVersionName,    //  操作系统版本名称
                sys.osVersionCode = A.osVersionCode,    //  操作系统版本号
                sys.platformVersionName = A.platformVersionName,    //  运行平台版本名称
                sys.platformVersionCode = A.platformVersionCode,    //  运行平台版本号
                sys.region = A.region,          //  系统地区
                sys.battery = A.battery,        //  当前电量，0.0 - 1.0 之间
                sys.wifiSignal = A.wifiSignal,  //  wifi信号强度，范围0 - 4
                console.log("YLSDK getSystemInfoByVIVO:",JSON.stringify(sys));
            } catch(e) {
                console.log("YLSDK exception-vivo:",JSON.stringify(e));
            }
        },
        getSystemInfoByOPPO(){
            try {
                var A = qg.getSystemInfoSync();
                sys.brand = A.brand,    //  手机品牌
                sys.model = A.model,    //  手机型号
                sys.screenWidth = A.screenWidth,    //  屏幕宽度
                sys.screenHeight = A.screenHeight,  //  屏幕高度
                sys.language = A.language,      //  当前环境设置的语言
                sys.pixelRatio = A.pixelRatio,  //  设备像素比
                sys.windowHeight = A.windowHeight,  //  可使用窗口高度
                sys.windowWidth = A.windowWidth,    //  可使用窗口宽度
                sys.platform = A.platform,  //  客户端平台
                sys.system = A.system,      //  操作系统版本
                sys.COREVersion = A.COREVersion,        //  版本号
                sys.platformVersion = A.platformVersion,//  平台版本号
                sys.notchHeight = A.notchHeight,        //  凹槽高度(刘海屏高度)
                console.log("YLSDK getSystemInfoByOPPO:",JSON.stringify(sys));
            } catch(e) {
                console.log("YLSDK exception-oppo:",JSON.stringify(e));
            }
        }
    };
    let platform_name = "无";
    if(g.ylsdk_platform === pc.OPPO){
        platform_name = "OPPO";
        systemInfo.getSystemInfoByOPPO();
    }else if(g.ylsdk_platform === pc.VIVO){
        systemInfo.getSystemInfoByVIVO();
        platform_name = "VIVO";
    }else if(g.ylsdk_platform === pc.MZ){
        sdk_data.platform_name = "魅族";
        systemInfo.getSystemInfoByVIVO();
    }
    if("" === g.ylsdk_app_id) console.error("YLSDK 请配置您的游戏APPID");
    if("" === g.ylsdk_version) console.error("YLSDK 请配置您的游戏版本号");
    console.warn("YLSDK 日志开关(本地)-",g.ylsdk_debug_model ? "打开" : "关闭");
    console.warn("YLSDK 当前设置平台请确认： ",platform_name);
    var p = YLsdkLaya.prototype;
    var self = this;
    function YLsdkLaya(){ };
    var request = function(options) { 
        //默认参数
        options.url = options.url || '',
        options.method = options.method || 'get',
        options.data = options.data || '',
        options.callback = options.callback || '';

        var http = new XMLHttpRequest();
        if (sdk_data.switchLog) {
            console.log("YLSDK Request--url:" + options.url+", menthod:"+options.method+", data:"+JSON.stringify(options.data));
        }

        //是否开启异步
        http.open(options.method, options.url);
        if(options.method === 'post')
            http.setRequestHeader("Content-Type", "application/json");
        if(options.needPass){
            if(!sdk_data.accountPass) console.error("YLSDK AccountPass is null !");
            http.setRequestHeader("AccountPass", sdk_data.accountPass);
        }else{            
            let ClientInfo = {
                              "brand": sys.brand || "", 
                              "model": sys.model || "",
                              "platform": sys.platform || "",
                              "version": sdk_data._SDKversion
                            };
            // console.log("YLSDK ClientInfo:",JSON.stringify(ClientInfo));
            ClientInfo = utils.base64_encode(JSON.stringify(ClientInfo))
            http.setRequestHeader("ClientInfo", ClientInfo);
        }

        http.onerror = function (e) {
            console.error("YLSDK ",e);
        };
        http.onabort = function (e) {
            console.exception(e);
        };
        http.onprogress = function (e) {

        };
        http.onreadystatechange = function () {
            if (this.status === 200) {
            } else {
                console.warn("YLSDK http request status:" + this.status);
            }
        };
        http.onload = function () {
            var back_data = this.responseText;

            if (options.callback && typeof options.callback === "function") {
                options.callback(back_data);
            }
        };

        if (options.data && options.data.length > 0 && sdk_data.switchLog) {
            console.log("YLSDK reqType:" + options.method + " url:" + options.url + " data:" + options.data);
        }
        try {
            if (options.data == undefined) {
                options.data = null;
            }
            http.send(JSON.stringify(options.data));
        } catch (e) {
            console.exception(options.data);
        }
    };

    //初始化SDK
    p.ylInitSDK = function(_callback){
        console.log("YLSDK 初始化SDK");
        // 登录
        nets.login(_callback);
    };
    //获取侧边栏列表
    p.ylSideBox = function(_callback){
        nets.SideBox(_callback);
    };
    //结果统计
    p.ylStatisticResult = function(_detail,_callback){
        let _data = {
            detail:_detail
        };
        nets.statisticResult(_data,_callback);
    };
    //视频统计(打点统计）
    p.ylStatisticViedo = function(_type,_adId,_callback){
        let _data = {
            scene:sdk_data.layer,
            adId:_adId,
            type:_type
        };
        nets.statisticViedo(_data,_callback);
    };
    //视频分享策略
    p.ylShowShareOrVideo = function(channel,module,cb){
        // console.warn("-----ylShowShareOrVideo:",channel,module);
        // console.warn("-----shareOrVideo:",sdk_data.moduleList);
        if(!channel){console.error("YLSDK ylShowShareOrVideo--channel is null"); return;}
        if(!module){console.error("YLSDK ylShowShareOrVideo--module is null"); return;}
        if(sdk_data.moduleList && sdk_data.moduleList.length >0){
            let hasModule = false;//默认无策略
            sdk_data.moduleList.forEach(function(element){  
                // console.warn("element.channel:",element.channel,element.module);
                if(element.channel == channel && element.module == module){
                    if(element && element.logicJson){
                        hasModule = true;
                        let sOrVvalue = element.logicJson;
                        // console.warn("element.channel:",element.channel,element.module);
                        if(sOrVvalue.pe && sOrVvalue.pe.length > 0){
                            let type = element.logicJson.pe.shift();
                            if(type == 1){
                                if(cb) cb(type);
                                if (sdk_data.switchLog) console.log("YLSDK 分享",JSON.stringify(sOrVvalue));
                            }else{
                                if(cb) cb(2);
                                if (sdk_data.switchLog) console.log("YLSDK 视频",JSON.stringify(sOrVvalue));
                            }
                        }else if(sOrVvalue.loop && 
                                sOrVvalue.loop.length > 0 &&
                                sOrVvalue.time && 
                                sOrVvalue.time > 0){

                            let type = sOrVvalue.loop[element.loopIndex];
                            element.loopIndex += 1;
                            if(element.loopIndex >= sOrVvalue.loop.length){
                                element.loopIndex = 0;
                                element.logicJson.time -= 1;
                            }
                            if(type == 1){
                                if(cb) cb(type);
                                if (sdk_data.switchLog) console.log("YLSDK 2-分享",JSON.stringify(sOrVvalue));
                            }else{
                                if(cb) cb(2);
                                if (sdk_data.switchLog) console.log("YLSDK 2-视频",JSON.stringify(sOrVvalue));
                            }

                        }else{
                            if(cb) cb(0);
                            if (sdk_data.switchLog) console.error("YLSDK 策略已经用完了");
                        }
                    }
                }
            });
            if(!hasModule){
                if(cb) cb(0);
                if (sdk_data.switchLog) console.error("YLSDK 没有视频分享策略1");
            }
        }else{
            if(cb) cb(0);
            if (sdk_data.switchLog) console.error("YLSDK 没有视频分享策略2");
        }
    };
    //自定义空间
    p.ylStoreValue = function(info,_callback){
            request({
                    method:"post",
                    url:sdk_data.domain_online+"/gamebase/store/value",
                    needPass:true,
                    data:info,
                    callback(res) {
                        let data = JSON.parse(res);
                        let log_tip = info.name + (info.cmd ? "-"+info.cmd : "")
                        utils.res_log("Response--store/value--"+log_tip+":",data.code,res);
                        if(data.code === 9002) p_f.loginPlatform();
                        if (data.code == 0 && data.result) {
                            if(_callback)_callback(data.result)
                        }else{
                            if(_callback)_callback(false)
                        }
                    }
                })
    };
    //事件统计
    p.ylEventCount = function(eventName){
        if(sdk_data.switchInfo.switchEvent=== 0){
            if (sdk_data.switchLog) console.warn('YLSDK 停用事件统计接口！');
            return;
        }
        let scene = sdk_data.layer ? sdk_data.layer : "default";
        sdk_data.statistics.events.push({ "event": eventName, "scene": scene, "time": (new Date().getTime())});
    };
    //获取游戏自定义配置
    p.ylGetCustom = function(_callback){
        if(sdk_data.custom){
            if(_callback){
                _callback(sdk_data.custom);
            }
        }else{
            request({
                method:"get",
                url:sdk_data.domain_online+"/gamebase/customconfig",
                needPass:true,
                callback(res) {
                    var _data = JSON.parse(res);
                    utils.res_log("Response--customconfig:",_data.code,res)
                    if (_data.code == 0) {
                        if(_data.result  && _data.result.length >0) sdk_data.custom = _data.result; 
                        if(_callback){
                            _callback(_data.result);
                        }
                    }else{
                        nets.customconfigFinal(_callback);
                    }
                }
            })
        }
    };
    //获取积分墙列表
    p.ylIntegralWall = function(_callback){
        nets.IntegralWall(_callback);
    };
    //领取积分墙奖励
    p.ylGetBoardAward = function(_callback){
        //获取本地积分id列表
        let integralIds = utils.getIntegralList();
        let list = null;
        if(integralIds && integralIds != ''){
            list = JSON.parse(integralIds);
        }
        if(list && list.length > 0) {
            let _id = list.pop();//取出最后一个元素
            request({
                method:"post",
                url:sdk_data.domain_online+"/gamebase/getboardaward",
                needPass:true,
                data:{
                    module:'scoreboard' //表示某个模块的签到，目前就填写scoreboard
                },
                callback(res) {
                    let data = JSON.parse(res);
                    utils.res_log("Response--getboardaward:",data.code,res)
                    if(data.code != 0){
                        data = false;
                        if(data.code === 9002) p_f.loginPlatform();
                    }else{
                        //将刚变动的数组保存到本地
                        let strList = list.length == 0 ? '' : JSON.stringify(list);
                        utils.saveIntegralList(strList);
                    }
                    if(_callback) _callback(data.result);
                }
            })
        }else{
            if(_callback) _callback(false);
        }
    };
    //设置当前场景，方便打点和退出游戏跟踪
    p.ylSetCurScene = function(sceneName){
        sdk_data.layer = sceneName;
    };

    //小游戏跳转
    p.ylNavigateToMiniGame = function(info,_cb) {
        if(!info._id) {console.error("YLSDK id is null !"); return;};
        if(!info.type) info.type = '0';
        let _scene = sdk_data.layer ? sdk_data.layer : "default";
        _scene += "_"+info.source; 
        if(''+info.type === '0'){
            if(!info.toUrl) {console.error("YLSDK toUrl is null !"); return;};
            let jumpInfo = {
                pkgName:info.toUrl,
                success() {
                    if (sdk_data.switchLog) console.log("YLSDK 小游戏跳转-跳转成功！");
                    //卖量统计
                    nets.ClickOut(info._id,info.toAppid,_scene,true);
                    utils.removeItemFrom(info.toAppid);
                    if(info.awardStatus){
                        //如果有奖励领取表示是积分墙item
                        let integralIds = utils.getIntegralList();
                        let list = [];
                        if(integralIds && integralIds != ''){
                            list = JSON.parse(integralIds);
                        }
                        list.push(info._id);
                        utils.saveIntegralList(JSON.stringify(list));
                    }
                    if(_cb)_cb(true);
                },
                fail() {
                    if (sdk_data.switchLog) console.log('YLSDK 小游戏跳转-跳转失败！');
                    //卖量统计
                    nets.ClickOut(info._id,info.toAppid,_scene,false);
                    if(_cb)_cb(false);
                }
            };
            if(info.extraData) jumpInfo.extraData = info.extraData;
            qg.navigateToMiniGame(jumpInfo);
        }else{
            if(!info.showImage) console.error("YLSDK showImage is null !");
            //卖量统计
            nets.ClickOut(info._id,info.toAppid,_scene,false);
            qg.previewImage({
                urls: [info.showImage],
            });
        }
        p.ylEventCount(_scene);
    };
    
    //获取游戏APPID
    p.ylGetAppID = function () {
        return g.ylsdk_app_id;
    };
    //获取邀请账号
    p.ylGetInviteAccount = function(){
        return sdk_data.invite_account;
    };
    //获取用户SDK服务器账号信息
    p.ylGetUserInfo = function(){
        return sdk_data.loginInfo;
    };
    //获取开关信息
    p.ylGetSwitchInfo = function(){
        return sdk_data.switchInfo;
    };
    //日志输出
    p.ylLog = function(logMsg,logType){

        if (!sdk_data.switchLog) return;

        logType = logType ? logType : 'log';
        if(logType ==='log'){
            console.log(logMsg);
        }else if(logType ==='info'){
            console.info(logMsg);
        }else if(logType ==='error'){
            console.error(logMsg);
        }else if(logType ==='warn'){
            console.warn(logMsg);
        }else if(logType ==='debug'){
            console.debug(logMsg);
        }
    };
    //获取玩家魅族平台账号信息
    p.ylGetUserMZinfo = function(){
        let userPlatformInfo = sdk_data.user_platform_info;
        if(!userPlatformInfo) userPlatformInfo = utils.getUserPlatformInfo();
        sdk_data.user_platform_info = (userPlatformInfo == '') ? null : JSON.parse(userPlatformInfo);
        return sdk_data.user_platform_info;
    };
    //切换页面
    p.ylChangeView = function(){
        if(!sdk_data.bannerAd){
            if (sdk_data.switchLog) console.warn("YLSDK ylChangeView-sdk_data.bannerAd is ",sdk_data.bannerAd);
            return;
        } 
        if(sdk_data.gameConfig && sdk_data.gameConfig.bannerTimeSpace && sdk_data.gameConfig.bannerTimeSpace.forceOnOff){
            let btSpace = sdk_data.gameConfig.bannerTimeSpace;
            let nowTime = new Date().getTime();
            let lastTime = sdk_data.bannerAd.lastTime || nowTime; //最后一次刷新banner
            let forceTimeSpace = btSpace.forceTimeSpace || sdk_data.defaultbTS; //强制刷新时间隔(秒)
            console.warn("YLSDK ylChangeView-forceTimeSpace:",(nowTime - lastTime)," >= ", forceTimeSpace*1000);
            if(nowTime - lastTime >= forceTimeSpace*1000){
                //强制刷新banner
                let isSmall = true;
                if(sdk_data.bannerAd.show_banner){
                    isSmall = sdk_data.gameConfig.bannerTimeSpace.isSmall || false;
                    p_f.createBannerAd(isSmall,true);
                }else{
                    sdk_data.bannerAd.lastTime = nowTime - (btSpace.timeSpace*1000+1000);//保证下次showBanner后可以立马刷新
                }
            }
        }
    };
    //获取深度误触屏蔽开关状态
    p.ylGetDeepTouch = function(customNum){
        let deepTouch = utils.getDeepTouch(customNum);
        if (sdk_data.switchLog) console.log("YLSDK ylGetDeepTouch-deepTouch,customNum:",deepTouch,customNum);
        let deepTouchInfo = {
            deepTouch:(deepTouch ? "1" : "0"),
        };
        let misTouchInfo = [];
        if(sdk_data.custom && sdk_data.custom.length >0){
            sdk_data.custom.forEach(element => {
                //如果是开关的话就返回
                if(parseInt(element.type) == 2){
                    if(!deepTouch){
                        element.value = "0";
                    }
                    misTouchInfo.push(element);
                }
            });
        }
        deepTouchInfo.customInfo = misTouchInfo;
        return deepTouchInfo;
    };
    //一局游戏结束
    p.ylOverGame = function(customNum){
        utils.addPlayNums(customNum);
    };
    //视频解锁
    p.ylVideoUnlock = function(customNum){
        return utils.getVideoUnlock(customNum);
    };
    //获取体力相关信息
    p.ylGetPowerInfo = function(_callback){
        if(sdk_data.gameConfig.isServerData){
            if(_callback) _callback(sdk_data.gameConfig.Power);
        }else{
            //获取游戏配置
            nets.getGameConfig(function(res){
                if (sdk_data.switchLog) console.log("YLSDK ylGetPowerInfo--res:",res);
                if(_callback) _callback(sdk_data.gameConfig.Power);
            });
        }
    };
    //体力变化监听
    //type[1:视频,2:定时自动恢复]
    p.ylOnPowerChange = function(_callback){
        sdk_data.powerChangeCB = _callback;
    };
    //设置体力
    p.ylSetPower = function(powerNum){
        sdk_data.userPowerNum = powerNum;
        if(sdk_data.gameConfig && sdk_data.gameConfig.Power && sdk_data.gameConfig.Power.powerUpper){
            //体力上限
            let powerUpper = sdk_data.gameConfig.Power.powerUpper;
            let recoveryTime = sdk_data.gameConfig.Power.recoveryTime;
            sdk_data.userPowerNum = (sdk_data.userPowerNum > powerUpper) ? powerUpper : sdk_data.userPowerNum;
            sdk_data.userPowerNum = (sdk_data.userPowerNum < 0) ? 0 : sdk_data.userPowerNum;
            if (sdk_data.switchLog) console.log("YLSDK ylSetPower-powerUpper,recoveryTime,userPowerNum,powerRtimer:",powerUpper,recoveryTime,sdk_data.userPowerNum,sdk_data.powerRtimer);
            if(sdk_data.userPowerNum < powerUpper){
                if(!sdk_data.powerRtimer && recoveryTime){
                    utils.powerRecoveryTimer(recoveryTime);
                }
            }else{
                if(sdk_data.powerRtimer){
                    clearTimeout(sdk_data.powerRtimer);
                    sdk_data.powerRtimer = null;
                }
            }
        }
    };
    //获取体力
    p.ylGetPower = function(){
        return sdk_data.userPowerNum;
    };
    //创建banner广告(填充屏幕宽度)
    p.ylBannerAdCreate = function(show,_callback){
        p_f.createBannerAd(false,show,_callback);
    };
    //创建banner广告(小的)
    p.ylBannerAdCreateSmall = function(show,_callback){
        p_f.createBannerAd(true,show,_callback);
    };
    //显示banner广告
    p.ylBannerAdShow = function(){
        p_f.showBannerAd();
    };
    //隐藏 banner广告
    p.ylBannerAdHide = function(){
        p_f.hideBannerAd();
    };
    //创建视频广告
    p.ylCreateVideoAd = function(){
        p_f.createVideoAd();
    };
    /**
        播放视频广告

        VIDEO_PLAY_FAIL:0    //视频广告-播放失败
        VIDEO_PLAY_FINISH:1  //视频广告-播放完成
        VIDEO_PLAY_CANCEL:2  //视频广告-播放取消
    **/
    p.ylShowVideoAd = function(cb,unlockCustoms){            
        p_f.showVideoAd(cb,unlockCustoms);
    };
    /**
     * 创建插屏广告
     * 
     * @param cb   回调函数 [type:0:创建或展示失败、1:创建或展示成功、2:关闭]
     * @param show 是否展示 [true:展示,false:不展示]
     */
    p.ylCreateInterstitialAd = function(cb,show){
        if(!qg.createInterstitialAd) return;
        if(g.ylsdk_platform === pc.VIVO && sys.platformVersionCode < 1031){
            if (sdk_data.switchLog) console.warn(`YLSDK VIVO平台版本号${sys.platformVersionCode}< 1031 无法展示插屏广告`);
            return ;
        }
        let ad_index = Math.floor(Math.random()*g.ylsdk_interstitial_ids.length);
        let advert = g.ylsdk_interstitial_ids[ad_index];
        sdk_data.interstitialAd = qg.createInterstitialAd({
            adUnitId: advert
        });
        sdk_data.interstitialAd.id_index = ad_index;
        sdk_data.interstitialAd.onLoad(() => {
            if(show){
                p.ylShowInterstitialAd(cb);
            }else{
                if(cb) cb(1);
            } 
            if (sdk_data.switchLog) console.log('YLSDK InterstitialAd.onLoad');
        });
        sdk_data.interstitialAd.onClose(res => {
            if (sdk_data.switchLog) console.log('YLSDK InterstitialAd.onClose');
            if(cb) cb(2);
            sdk_data.interstitialAd = null;
        });
        //视频加载失败
        sdk_data.interstitialAd.onError(err => {
            if (sdk_data.switchLog) console.warn('YLSDK InterstitialAd.onError:',err,sdk_data.interstitialAd.adUnitId);
            if(cb) cb(0);
        });
    };
    /**
     * 插屏广告
     * 
     * @param cb   回调函数 [type:0:展示失败、1:展示成功、2:关闭]
     */
    p.ylShowInterstitialAd = function(cb){
        if (sdk_data.interstitialAd) {
            let adShow = sdk_data.interstitialAd.show();
            adShow && adShow.then(() => {
                if(cb) cb(1);
            }).catch((err) => {
              if (sdk_data.switchLog) console.warn('YLSDK InterstitialAd.show - err:',err);
              if(cb) cb(0);
            })
            p.ylEventCount('showInterstitialAd');
        }
    };

    //工具方法
    var utils = {
        //注册定时器
        registerInterval:function(){
            utils.unRegisterInterval();
            if (sdk_data.switchLog) console.log("YLSDK 注册-事件上传-定时器");
            sdk_data.s_interval = setInterval(function(){
                utils.sendStatistics();
            }, sdk_data.s_commit_dt, null);
        },
        //注销定时器
        unRegisterInterval:function(){
            if (sdk_data.switchLog) console.log("YLSDK 注销-事件上传-定时器");
            if(sdk_data.s_interval) clearInterval(sdk_data.s_interval);
            sdk_data.s_interval = null;
        },
        //提交打点数据
        sendStatistics:function(){
            if(!sdk_data.statistics) return;
            if(!sdk_data.accountPass) return;
            if(sdk_data.statistics.events){
                let commitData = sdk_data.statistics.events;
                if (commitData.length !== 0) {
                    if (commitData.length >= sdk_data.s_max_post_l) {
                        let tempCommitData = [];
                        for (var i = 0; i < sdk_data.s_max_post_l; i++) {
                            tempCommitData.push(commitData.shift())
                        }
                        // 上传;
                        nets.statisticsEvents(tempCommitData);
                    }else {
                        // 上传;
                        nets.statisticsEvents(commitData, true);
                    }
                }
            }
            if(sdk_data.statistics.video && sdk_data.statistics.video.length > 0){
                nets.statisticViedo(sdk_data.statistics.video.pop());
            }
            // if(sdk_data.statistics.sharecard && sdk_data.statistics.sharecard.length > 0){
            //     nets.statisticShareCard(sdk_data.statistics.sharecard.pop());
            // }
            if(sdk_data.statistics.clickcount && sdk_data.statistics.clickcount.length > 0){
                nets.statisticsClickOut(sdk_data.statistics.clickcount.pop());
            }
            if(sdk_data.statistics.result && sdk_data.statistics.result.length > 0){
                nets.statisticResult(sdk_data.statistics.result.pop());
            }
            if(sdk_data.statistics.layer && sdk_data.statistics.layer.length > 0){
                nets.statisticsLayer(sdk_data.statistics.layer.pop());
            }
        },
        //获取本地打点数据缓存
        getLocalStatistics:function(){
            try {
                    var statistics = p_f.getLocalData('ylsdk_statistics' + g.ylsdk_app_id);
                    return ((!statistics || statistics.length === 0) ? {            
                        'events':[],
                        'video':[],
                        'result':[],
                        'sharecard':[],
                        'clickcount':[],
                        'layer':[]
                    } : JSON.parse(statistics));
            } catch (e) { }
        },
        //将打点数据缓存到本地
        saveStatisticsToLocal:function(){
            if (sdk_data.statistics.events.length > sdk_data.s_cache_ml) {
                for (var i = sdk_data.s_cache_ml; i < sdk_data.statistics.events.length; i++) {
                    sdk_data.statistics.events.shift();
                }
            }
            try {
                p_f.saveLocalData('ylsdk_statistics' + g.ylsdk_app_id, JSON.stringify(sdk_data.statistics));
            } catch (e) { }
        },
        // 清空本地本地打点数据缓存
        clearLocalStatistics:function() {
            try {
                p_f.saveLocalData('ylsdk_statistics' + g.ylsdk_app_id, '');
            } catch (e) { }
            sdk_data.statistics = {
                'events':[],
                'video':[],
                'result':[],
                'sharecard':[],
                'clickcount':[],
                'layer':[]
            };
        },
        //设置用户ID
        SetAccountID:function(accountId) {
            sdk_data._AccountId = accountId;
            utils.saveAccountId(accountId);
        },
        //设置accountPass
        SetAccountPass:function(accountPass) {
            sdk_data.accountPass = accountPass;
            utils.saveAccountPass(accountPass);
        },
        //设置accountPass本地缓存
        saveAccountPass:function(accountPass){
            p_f.saveLocalData('SDK_ACCOUNT_PASS'+g.ylsdk_app_id,""+accountPass)
            if (sdk_data.switchLog)
                 console.log("YLSDK 设置缓存AccountPass:",accountPass);
        },
        //获取accountPass缓存
        getAccountPass:function(){
            var accountPass = p_f.getLocalData('SDK_ACCOUNT_PASS'+g.ylsdk_app_id)
            if (sdk_data.switchLog)
                console.log("YLSDK 获取缓存AccountPass:",accountPass);
            return accountPass;
        },
        //设置account_id缓存
        saveAccountId:function(account_id){
            p_f.saveLocalData('SDK_ACCOUNT_ID'+g.ylsdk_app_id,""+account_id)
            if (sdk_data.switchLog)
                 console.log("YLSDK 设置缓存account_id:",account_id);
        },
        //获取account_id缓存
        getAccountId:function(){
            var accountId = p_f.getLocalData('SDK_ACCOUNT_ID'+g.ylsdk_app_id)
            if (sdk_data.switchLog)
                console.log("YLSDK 获取缓存account_id:",accountId);
            return accountId;
        },
        //设置卖量跳转记录
        saveJumpOutInfo:function(info){
            p_f.saveLocalData('SDK_JUMP_ICON'+g.ylsdk_app_id,info)
            if (sdk_data.switchLog)
                 console.log("YLSDK 设置卖量跳转记录:",info);
        },
        //获取卖量跳转记录
        getJumpOutInfo:function(){
            var info = p_f.getLocalData('SDK_JUMP_ICON'+g.ylsdk_app_id)
            if (sdk_data.switchLog)
                console.log("YLSDK 获取卖量跳转记录:",info);
            return info;
        },
        //隐藏已经跳转的卖量ITEM
        removeItemFrom(appid){
            let jumpInfo = utils.getJumpOutInfo() 
            let jumpOutInfo = (!jumpInfo || jumpInfo === '') ? false : JSON.parse(jumpInfo);
            //点击并确认跳转某个icon后，当日（可调整）隐藏对应appid的所有icon（开启内部标识的icon不受此功能影响）
            if(sdk_data.boxconfig && sdk_data.boxconfig.length >0 && g.side_min_num < sdk_data.boxconfig.length){
                for(let i=0;i<sdk_data.boxconfig.length;i++){
                    let item = sdk_data.boxconfig[i];
                    if(item.toAppid === appid && item.innerStatus == 0){
                        if (sdk_data.switchLog) console.error("YLSDK 隐藏对应appid的所有icon--id:",item._id);
                        sdk_data.boxconfig.splice(i,1);
                        //
                        let hasItem = false;
                        let date = new Date();
                        if(jumpOutInfo && jumpOutInfo.list && jumpOutInfo.list.length >0){
                            for(let j=0;j<jumpOutInfo.list.length;j++){
                                if(jumpOutInfo.list[j].appid == appid){
                                    hasItem = true;
                                }
                            }
                        }else{
                            jumpOutInfo = {list:[]};
                        }
                        if(!hasItem) jumpOutInfo.list.push({appid:appid});
                        jumpOutInfo.date = date.getTime();
                    } 
                }
                utils.saveJumpOutInfo(JSON.stringify(jumpOutInfo));
            }
        },
        //日志输出
        res_log(tips,code,res){
            if(code != 0){
                console.error("YLSDK ",tips+" "+res);
            }else if (sdk_data.switchLog){
                console.log("YLSDK ",tips+" "+res)
            } 
        },
        //设置积分墙列表ID本地缓存(待领取奖励)
        saveIntegralList:function(list){
            p_f.saveLocalData('SDK_INTEGRAL_IDS'+g.ylsdk_app_id,list)
            if (sdk_data.switchLog)
                 console.log("YLSDK 设置积分墙列表ID本地缓存(待领取奖励):",list);
        },
        /**
         * 获取积分墙列表ID本地缓存
         */
        getIntegralList:function(){
            var list = p_f.getLocalData('SDK_INTEGRAL_IDS'+g.ylsdk_app_id)
            if (sdk_data.switchLog)
                console.log("YLSDK 获取积分墙列表ID本地缓存:",list);
            return list;
        },
        //检测本地跳转列表是否是今天的缓存，不是则清除缓存(是保留当天的)
        checkJumpOutTime(){
            let date = new Date();
            let day = date.getDate();
            let month = date.getMonth();
            let year  = date.getFullYear();
            let jumpInfo = utils.getJumpOutInfo() 
            let jumpOutInfo = (!jumpInfo || jumpInfo === '') ? false : JSON.parse(jumpInfo);   
            if(jumpOutInfo && jumpOutInfo.date){
                let lastDate = new Date(jumpOutInfo.date);
                let day_2 = lastDate.getDate();
                let month_2 = lastDate.getMonth();
                let year_2  = lastDate.getFullYear();
                if(year != year_2|| month != month_2 || day_2 != day){
                    utils.saveJumpOutInfo('');
                }
            }
        },
        base64_encode (str) { // 编码，配合encodeURIComponent使用
            var c1, c2, c3;
            var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            var i = 0, len = str.length, strin = '';
            while (i < len) {
                c1 = str.charCodeAt(i++) & 0xff;
                if (i == len) {
                    strin += base64EncodeChars.charAt(c1 >> 2);
                    strin += base64EncodeChars.charAt((c1 & 0x3) << 4);
                    strin += "==";
                    break;
                }
                c2 = str.charCodeAt(i++);
                if (i == len) {
                    strin += base64EncodeChars.charAt(c1 >> 2);
                    strin += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
                    strin += base64EncodeChars.charAt((c2 & 0xF) << 2);
                    strin += "=";
                    break;
                }
                c3 = str.charCodeAt(i++);
                strin += base64EncodeChars.charAt(c1 >> 2);
                strin += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
                strin += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
                strin += base64EncodeChars.charAt(c3 & 0x3F)
            }
            return strin
        },
        //切出游戏打点
        statisticsPlayTime(){
            let curTime = new Date().getTime();
            if(!sdk_data.lastTime) sdk_data.lastTime = curTime;
            let timeSpace = curTime - sdk_data.lastTime;

            let tempCommitData = {
                "event": "play_time",
                "scene": (sdk_data.layer ? sdk_data.layer : "default"),
                "tp":timeSpace,
                "time": curTime
            }  
            nets.statisticsEvents([tempCommitData]);
        },
        //设置用户平台信息本地缓存
        saveUserPlatfromInfo:function(info){
            p_f.saveLocalData('SDK_USER_PLATFORM_INFO'+g.ylsdk_app_id,info)
            if (sdk_data.switchLog)
                 console.log("YLSDK 设置用户"+sdk_data.platform_name+"平台信息本地缓存:",info);
        },
        //获取用户平台信息本地缓存
        getUserPlatformInfo:function(){
            var info = p_f.getLocalData('SDK_USER_PLATFORM_INFO'+g.ylsdk_app_id)
            if (sdk_data.switchLog)
                console.log("YLSDK 获取用户"+sdk_data.platform_name+"平台信息本地缓存:",info);
            return info;
        },
        //解析游戏配置
        parseGameConfig(data,_callback){
            if(data.length>0){
                let gameConfig = sdk_data.gameConfig;
                data.forEach(element => {
                    let _value = null;
                    if(element.value) _value = JSON.parse(element.value);
                    if(element.code === "b_t_s"){
                        // banner定时刷新配置开关
                        gameConfig.bannerTimeSpace = {};
                        //是否定时刷新
                        gameConfig.bannerTimeSpace.onOff = (_value[0] === 1);
                        //定时刷新时间
                        gameConfig.bannerTimeSpace.timeSpace = _value[1] || 10;
                      if (_value[1] || _value[1] === 0){
                            if(g.ylsdk_platform === pc.VIVO && _value[1] < 10){
                                if (sdk_data.switchLog) console.warn("YLSDK banner定时刷新配置-VIVO平台定时刷新时间不能小于10秒");
                                gameConfig.bannerTimeSpace.timeSpace = 10;
                            }
                        }else{
                            if (sdk_data.switchLog) console.error("YLSDK banner定时刷新配置-定时刷新时间不存在");
                        } 

                        //切页面是否强制刷新开关
                        gameConfig.bannerTimeSpace.forceOnOff = (_value[2] === 1);
                        gameConfig.bannerTimeSpace.forceTimeSpace = _value[3] || 3;
                        if(!_value[3] && _value[3]!== 0){
                            if (sdk_data.switchLog) console.error("YLSDK banner定时刷新配置-强制刷新最小间隔时间不存在");
                        } 
                    }else if(element.code === "power"){
                        // 体力系统
                        gameConfig.Power = {};
                        //初始体力（num）
                        gameConfig.Power.defaultPower = _value[0] || 5;
                      if (!_value[0] && _value[0] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 体力系统-初始体力不存在");
                        } 
                        //体力上限
                        gameConfig.Power.powerUpper = _value[1] || 5;
                      if (!_value[1] && _value[1] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 体力系统-体力上限不存在");
                        } 
                        //体力自动恢复时间
                        gameConfig.Power.recoveryTime = _value[2] || 300;
                      if (!_value[2] && _value[2] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 体力系统-体力自动恢复时间不存在");
                        } 
                        //看视频获得体力值
                        gameConfig.Power.getPower = _value[3] || 1;
                      if (!_value[3] && _value[3] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 体力系统-看视频获得体力值不存在");
                        } 
                    }else if(element.code === "v_u_c"){
                        // 视频解锁关卡
                        gameConfig.VideoUnlockCustoms = {};
                        //开关
                        gameConfig.VideoUnlockCustoms.onOff = (_value[0] === 1);
                        //总关卡数
                        gameConfig.VideoUnlockCustoms.openCustoms = _value[1] || 2;
                      if (!_value[1] && _value[1] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 视频解锁关卡-第几关开启值不存在");
                        } 
                        //每日关卡数
                        gameConfig.VideoUnlockCustoms.spaceCustoms = _value[2] || 0;
                      if (!_value[2] && _value[2] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 视频解锁关卡-每日关卡数不存在");
                        } 
                    }else if(element.code === "dp_s"){
                        // 深度屏蔽规则
                        gameConfig.depthShield = {};
                        //类型[0:关闭,1:按对局数,2:按关卡数]
                        gameConfig.depthShield._type = _value[0] || 0;
                        //总关卡数
                        gameConfig.depthShield.totalCustoms = _value[1] || 5;
                      if (!_value[1] && _value[1] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 深度屏蔽规则-总关卡数不存在");
                        }
                        //优质用户判断开关
                        gameConfig.depthShield.goodUserOnOff = (_value[2] === 1);
                      if (!_value[2] && _value[2] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 深度屏蔽规则-优质用户判断开关不存在");
                        }
                        //观看视频数量 
                        gameConfig.depthShield.watchTvNum = _value[3] || 2;
                      if (!_value[3] && _value[3] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 深度屏蔽规则-观看视频数量不存在");
                        }
                        //每日关卡数/对局数组 
                        gameConfig.depthShield.dayCustoms = _value[4] || 0;
                      if (!_value[4] && _value[4] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 深度屏蔽规则-关卡间隔数/对局数组不存在");
                        }
                    }else if(element.code === "m_t"){
                        // 定时开关误触
                        gameConfig.mistouchTimer = {};
                        //开关
                        gameConfig.mistouchTimer.onOff = (_value[0] === 1);
                        //开始时间
                        //时
                        gameConfig.mistouchTimer.startTime_h = _value[1] || 0;
                      if (!_value[1] && _value[1] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 定时开关误触-开始时间“时”不存在");
                        } 
                        //分
                        gameConfig.mistouchTimer.startTime_m = _value[2] || 0;
                      if (!_value[2] && _value[2] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 定时开关误触-开始时间“分”不存在");
                        }
                        //结束时间
                        //时
                        gameConfig.mistouchTimer.endTime_h = _value[3] || 23;
                      if (!_value[3] && _value[3] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 定时开关误触-结束时间“时”不存在");
                        }
                        //分
                        gameConfig.mistouchTimer.endTime_m = _value[4] || 59;
                      if (!_value[4] && _value[4] !== 0){
                            if (sdk_data.switchLog) console.error("YLSDK 定时开关误触-结束时间“分”不存在");
                        } 
                    }
                });
                sdk_data.gameConfig = gameConfig;
                sdk_data.gameConfig.isServerData = true;
            }
            utils.initTodayPlayNums();
            utils.initPower();
            if (sdk_data.switchLog) console.log("YLSDK sdk_data.gameConfig:",JSON.stringify(sdk_data.gameConfig));
            if(_callback) _callback('config_success');
        },
        //检测是否在误触的时间段内
        checkMistouchTimer(){
            if(sdk_data.gameConfig){
                if(sdk_data.gameConfig.mistouchTimer){
                    if (sdk_data.switchLog) console.log("YLSDK checkMistouchTimer-onOff: ",sdk_data.gameConfig.mistouchTimer.onOff);
                    if(sdk_data.gameConfig.mistouchTimer.onOff){
                        let mistouchTimer = sdk_data.gameConfig.mistouchTimer;
                        let startTime_h = mistouchTimer.startTime_h || 0;
                        let startTime_m = mistouchTimer.startTime_m || 0;
                        let endTime_h = mistouchTimer.endTime_h || 23;
                        let endTime_m = mistouchTimer.endTime_m || 59;

                        //当前时间
                        let nowDate = new Date();
                        let misNThours = nowDate.getHours();
                        let misNTminutes = nowDate.getMinutes();
                        if (sdk_data.switchLog) console.log(`YLSDK checkMistouchTimer- ${startTime_h}:${startTime_m} > ${misNThours}:${misNTminutes} < ${endTime_h}:${endTime_m}`);
                        let inTime =!((misNThours < startTime_h || (misNThours == startTime_h && misNTminutes <= startTime_m)) || (misNThours > endTime_h || (misNThours == endTime_h && misNTminutes >= endTime_m)));
                        if(inTime){
                            return false;
                        }else{
                            //不在时间范围里面
                            return true;
                        }
                    }else{
                        //开关是关的，表示不受时间范围控制
                        return true;
                    }
                }
            }
            return false;
        },
        //获取深度屏蔽结果
        // true:误触开,false:误触关
        getDeepTouch(customNum){
            if (sdk_data.switchLog) console.log("YLSDK switchTouchAll:",sdk_data.switchTouchAll);
            if(!(sdk_data.switchTouchAll == 1)) return false;
            if(utils.checkMistouchTimer()){
                if(sdk_data.gameConfig && sdk_data.gameConfig.depthShield){
                    let depthShield = sdk_data.gameConfig.depthShield;
                    let totalCustoms = depthShield.totalCustoms || 0;
                    if (sdk_data.switchLog) console.log("YLSDK getDeepTouch-depthShield._type:",depthShield._type);
                    if(depthShield._type !== 0){
                        //[0:关闭,1:按对局数,2:按关卡数]
                        if(depthShield._type == 1){
                            //按对局数
                            if (sdk_data.switchLog) console.log(`YLSDK getDeepTouch-toalNum > totalCustoms ? :${sdk_data.gameConfig.toalNum} > ${depthShield.totalCustoms}`);
                            if(!depthShield.totalCustoms || sdk_data.gameConfig.toalNum > depthShield.totalCustoms){
                                if (sdk_data.switchLog) console.log(`YLSDK getDeepTouch-toalByTodayNum > dayCustoms ? :${sdk_data.gameConfig.toalByTodayNum} > ${depthShield.dayCustoms[0]}`);
                                if(!depthShield.dayCustoms || sdk_data.gameConfig.toalByTodayNum > depthShield.dayCustoms[0]){
                                    return utils.checkGoodUser(depthShield);
                                }else{
                                    return false;
                                }
                            }else{
                                return false;
                            }
                        }else{
                            if (sdk_data.switchLog) console.log("YLSDK getDeepTouch-customNum:",customNum);
                            if (sdk_data.switchLog) console.log("YLSDK getDeepTouch-totalCustoms,SpaceCustom:",depthShield.totalCustoms,depthShield.dayCustoms[0]);
                            //按关卡数
                            //玩家传入的关卡数 customNum == totalCustoms 或者  totalCustoms 开始每间隔 depthShield.dayCustoms[0]关
                            if(customNum || customNum === 0){
                                if(customNum >= totalCustoms){
                                    // || sdk_data.gameConfig.maxCustom > depthShield.totalCustoms
                                    if(!depthShield.dayCustoms || depthShield.dayCustoms.length == 0){
                                        if (sdk_data.switchLog) console.log("YLSDK getDeepTouch-depthShield.dayCustoms is null or empty");
                                        return false;
                                    }
                                    if((customNum == totalCustoms) || ((customNum - totalCustoms) % (depthShield.dayCustoms[0]+1)) == 0){
                                        return utils.checkGoodUser(depthShield);
                                    }else{
                                        return false;
                                    }
                                }else{
                                    return false;
                                }
                            }else{
                               if (sdk_data.switchLog) console.log("YLSDK getDeepTouch-customNum is ",customNum);
                                return false;
                            }
                        }
                    }else{
                        return utils.checkGoodUser(depthShield);
                    }
                }
            }else{
                return false;
            }
        },
        //是否为优质用户
        isGoodUser(){
            let isGoods = false;
            if(sdk_data.gameConfig.depthShield.watchTvNum || sdk_data.gameConfig.depthShield.watchTvNum === 0){
                if (sdk_data.switchLog) console.log(`YLSDK isGoodUser-userWatchTvNum >= watchTvNum ? : ${sdk_data.gameConfig.watchTvNum} >= ${sdk_data.gameConfig.depthShield.watchTvNum}`);
                isGoods = (sdk_data.gameConfig.watchTvNum >= sdk_data.gameConfig.depthShield.watchTvNum);
            }else{
                isGoods = true;
            }
            if (sdk_data.switchLog) console.log("YLSDK isGoodUser:",isGoods);
            return isGoods;
        },
        //检测优质用户
        checkGoodUser(depthShield){
            if (sdk_data.switchLog) console.log("YLSDK checkGoodUser:",depthShield.goodUserOnOff);
            if(depthShield.goodUserOnOff){
                return !utils.isGoodUser();
            }else{
                return true;
            }
        },
        //初始化玩家今天玩的局数
        initTodayPlayNums(){
            let todayPlay = p_f.getLocalData('SDK_TODAY_PLAY_NUMS_ID_'+g.ylsdk_app_id);
            let totalPlay = p_f.getLocalData('SDK_TOTAL_PLAY_NUMS_ID_'+g.ylsdk_app_id);
            let watchTvNum = p_f.getLocalData('SDK_TODAY_WATCH_TV_NUMS_ID_'+g.ylsdk_app_id);
            let lastWatch = p_f.getLocalData('SDK_LAST_WATCH_TIME_ID_'+g.ylsdk_app_id);
            let lastPlay = p_f.getLocalData('SDK_LAST_PLAY_TIME_ID_'+g.ylsdk_app_id);
            let maxCustom = p_f.getLocalData('SDK_MAX_CUSTOM_ID_'+g.ylsdk_app_id);
            let lastCustom = p_f.getLocalData('SDK_LAST_CUSTOM_ID_'+g.ylsdk_app_id);
            let nowTime = new Date();
            todayPlay = (todayPlay == '') ? 0 : parseInt(todayPlay);
            totalPlay = (totalPlay == '') ? 0 : parseInt(totalPlay);
            watchTvNum = (watchTvNum == '') ? 0 : parseInt(watchTvNum);
            maxCustom = (maxCustom == '') ? 0 : parseInt(maxCustom);
            lastCustom = (lastCustom == '') ? 0 : parseInt(lastCustom);

            if (sdk_data.switchLog) console.log(`YLSDK initTodayPlayNums-nowTime:${nowTime.getTime()},lastWatch:${lastWatch},lastPlay:${lastPlay}`);

            if(lastWatch == ''){
                lastWatch = nowTime.getTime();
                p_f.saveLocalData('SDK_LAST_WATCH_TIME_ID_'+g.ylsdk_app_id,lastWatch);
            }else{
                lastWatch =  parseInt(lastWatch);
            }
            
            if(lastPlay == ''){
                lastPlay = nowTime.getTime();
                p_f.saveLocalData('SDK_LAST_PLAY_TIME_ID_'+g.ylsdk_app_id,lastPlay);
            }else{
                lastPlay = parseInt(lastPlay);
            }
            //判断最后一次看视频和玩游戏的时间是否是昨天
            //是昨天就清理数据
            let nowYear = nowTime.getFullYear();
            let nowMonth = nowTime.getMonth()+1;
            let nowDay = nowTime.getDate();

            let lwatch = new Date(lastWatch);
            let lwatchYear = lwatch.getFullYear();
            let lwatchMonth = lwatch.getMonth()+1;
            let lwatchDay = lwatch.getDate();

            let lplay = new Date(lastPlay);
            let lplayYear = lplay.getFullYear();
            let lplayMonth = lplay.getMonth()+1;
            let lplayDay = lplay.getDate();

            if (sdk_data.switchLog) console.log(`YLSDK initTodayPlayNums-now:${nowYear}-${nowMonth}-${nowDay} ,lwatch:${lwatchYear}-${lwatchMonth}-${lwatchDay} ,lplay:${lplayYear}-${lplayMonth}-${lplayDay}`);

            if(nowYear != lwatchYear || nowMonth != lwatchMonth || nowDay != lwatchDay){
                watchTvNum = 0;
                lastWatch = nowTime.getTime();
                p_f.saveLocalData('SDK_TODAY_WATCH_TV_NUMS_ID_'+g.ylsdk_app_id,sdk_data.gameConfig.watchTvNum);
                p_f.saveLocalData('SDK_LAST_WATCH_TIME_ID_'+g.ylsdk_app_id,lastWatch);
            }
            if(nowYear != lplayYear || nowMonth != lplayMonth || nowDay != lplayDay){
                todayPlay = 0;
                lastPlay = nowTime.getTime();
                p_f.saveLocalData('SDK_TODAY_PLAY_NUMS_ID_'+g.ylsdk_app_id,sdk_data.gameConfig.toalByTodayNum);
                p_f.saveLocalData('SDK_LAST_PLAY_TIME_ID_'+g.ylsdk_app_id,lastPlay);
            }

            sdk_data.gameConfig.toalByTodayNum = todayPlay;
            sdk_data.gameConfig.toalNum = totalPlay;
            sdk_data.gameConfig.watchTvNum = watchTvNum;
            sdk_data.gameConfig.maxCustom = maxCustom;
            sdk_data.gameConfig.lastCustom = lastCustom;
        },
        //更新玩游戏的局数
        addPlayNums(customNum){
            if(customNum){
                try{
                    customNum = parseInt(customNum);
                    sdk_data.gameConfig.lastCustom = customNum;
                    let maxCustom = p_f.getLocalData('SDK_MAX_CUSTOM_ID_'+g.ylsdk_app_id);
                    maxCustom = (maxCustom == '') ? 0 : parseInt(maxCustom);
                    if(customNum > maxCustom){
                        maxCustom = customNum;
                        p_f.saveLocalData('SDK_MAX_CUSTOM_ID_'+g.ylsdk_app_id,maxCustom);
                        sdk_data.gameConfig.maxCustom = maxCustom;
                    }
                    p_f.saveLocalData('SDK_LAST_CUSTOM_ID_'+g.ylsdk_app_id,customNum);
                    if (sdk_data.switchLog) console.log("YLSDK addPlayNums-customNum,maxCustom:",customNum,maxCustom);
                }catch(e){
                    console.error("YLSDK addPlayNums error e:",e);
                }
                
                //如果有关卡数就要保存最大关卡数
                //对局：总对局|当日对局数
                //关卡:总关卡|当日关卡
            }
            //更新今天玩的局数
            sdk_data.gameConfig.toalByTodayNum += 1;
            //更新总局数
            sdk_data.gameConfig.toalNum += 1;
            if (sdk_data.switchLog) console.log("YLSDK addPlayNums-toalNum,toalByTodayNum:",sdk_data.gameConfig.toalNum,sdk_data.gameConfig.toalByTodayNum);
            p_f.saveLocalData('SDK_TODAY_PLAY_NUMS_ID_'+g.ylsdk_app_id,sdk_data.gameConfig.toalByTodayNum);
            p_f.saveLocalData('SDK_TOTAL_PLAY_NUMS_ID_'+g.ylsdk_app_id,sdk_data.gameConfig.toalNum);
            p_f.saveLocalData('SDK_LAST_PLAY_TIME_ID_'+g.ylsdk_app_id,new Date().getTime());
        },
        addWatchTVnums(unlockCustomNum){
            sdk_data.gameConfig.watchTvNum += 1;
            p_f.saveLocalData('SDK_TODAY_WATCH_TV_NUMS_ID_'+g.ylsdk_app_id,sdk_data.gameConfig.watchTvNum);
            p_f.saveLocalData('SDK_LAST_WATCH_TIME_ID_'+g.ylsdk_app_id,new Date().getTime());
            
            if(unlockCustomNum){
                utils.addVideoUnlockCustoms(unlockCustomNum);
            }
        },
        getVideoUnlock(customNum){
            let unlock = false;
            if(sdk_data.gameConfig && sdk_data.gameConfig.VideoUnlockCustoms){
                let VULcustoms = sdk_data.gameConfig.VideoUnlockCustoms;
                let openCustoms = VULcustoms.openCustoms || 0;
                let spaceCustoms = VULcustoms.spaceCustoms || 0;
                if(VULcustoms.onOff){
                    if(customNum == openCustoms){
                        unlock = true;
                    }else if((customNum > openCustoms) && ((customNum - openCustoms) % (spaceCustoms+1)) == 0){
                        unlock = true;
                    }else{
                        //判断是否有看视频解锁过该关卡
                        let unlockNum = utils.getVideoUnlockCustoms();
                        if(unlockNum && unlockNum.length > 0){
                            unlockNum.forEach(element => {
                                if(customNum == element){
                                    unlock = true;
                                }
                            });
                        }
                    }
                }
                if (sdk_data.switchLog) console.log("YLSDK getVideoUnlock-customNum,openCustoms,spaceCustoms,unlock:",customNum,openCustoms,spaceCustoms,unlock);
            }
            return unlock;
        },
        //初始化体力
        initPower(){
            let defaultPower = 0;
            if (sdk_data.switchLog) console.log("YLSDK initPower-Power:",JSON.stringify(sdk_data.gameConfig.Power));
            if(sdk_data.gameConfig && sdk_data.gameConfig.Power && sdk_data.gameConfig.Power.defaultPower){
                defaultPower = sdk_data.gameConfig.Power.defaultPower || 0;
            }
            let powerNum = p_f.getLocalData('SDK_POWER_NUM_ID_'+g.ylsdk_app_id);
            powerNum = (powerNum == '') ? defaultPower : parseInt(powerNum);
            if (sdk_data.switchLog) console.log("YLSDK initPower-powerNum:",powerNum);
            p.ylSetPower(powerNum);
        },
        //保存体力到本地缓存
        savePowerLocal(){
            p_f.saveLocalData('SDK_POWER_NUM_ID_'+g.ylsdk_app_id,sdk_data.userPowerNum);
            if (sdk_data.switchLog) console.log("YLSDK savePowerLocal-powerNum:",sdk_data.userPowerNum);
        },
        //增加体力
        //type[1:视频,2:定时自动恢复]
        addPower(type){
            if(sdk_data.gameConfig &&sdk_data.gameConfig.Power){
                //要在后台配置了体力系统存在的情况下才有增加体力逻辑
                let power = p.ylGetPower();
                if(type == 1){
                    let getPower = sdk_data.gameConfig.Power.getPower;
                    if(getPower){
                        power += getPower;
                    }
                }else if(type == 2){
                    power += 1; //次自动恢复1点
                }
                if(sdk_data.powerChangeCB) sdk_data.powerChangeCB(type);
                p.ylSetPower(power);
            }
        },
        //体力恢复定时器
        powerRecoveryTimer(delay){
            if (sdk_data.switchLog) console.log("YLSDK powerRecoveryTimer-delay:",delay);
            if (delay > 0) { //大于0才刷新
                if(sdk_data.powerRtimer){
                    clearTimeout(sdk_data.powerRtimer);
                    sdk_data.powerRtimer = null;
                }
                sdk_data.powerRtimer = setTimeout(() => {
                    console.warn("YLSDK test-powerRecoveryTimer-----1");
                    p_f.removeBannerAd();
                    console.warn("YLSDK test-powerRecoveryTimer-----2");
                    utils.powerRecoveryTimer(delay);
                    console.warn("YLSDK test-powerRecoveryTimer-----3");
                    utils.addPower(2);
                    console.warn("YLSDK test-powerRecoveryTimer-----4");
                }, (delay * 1000));
            }
        },
        getVideoUnlockCustoms(){
            let sUnlockNum = p_f.getLocalData('SDK_WATCH_TV_UNLOCK_CUSTOMS_ID_'+g.ylsdk_app_id);
            if(!sUnlockNum || sUnlockNum == '0' || sUnlockNum == ''){
                sUnlockNum = [];
            }else{
                sUnlockNum = JSON.parse(sUnlockNum);
            }
            if (sdk_data.switchLog) console.log("YLSDK getVideoUnlockCustoms-sUnlockNum:",sUnlockNum);
            return sUnlockNum;
        },
        addVideoUnlockCustoms(unlockCustomNum){
            let unlockNum = utils.getVideoUnlockCustoms();
            unlockNum.push(unlockCustomNum);
            let sUnlockNum = JSON.stringify(unlockNum); 
            if (sdk_data.switchLog) console.log("YLSDK addVideoUnlockCustoms-unlockCustomNum:",unlockCustomNum,sUnlockNum);
            p_f.saveLocalData('SDK_WATCH_TV_UNLOCK_CUSTOMS_ID_'+g.ylsdk_app_id,sUnlockNum);
        },
    };
    //平台接口
    var p_f = {
        //设置本地缓存
        saveLocalData:function(_key,_value){
            localStorage.setItem(_key, _value);
        },
        //获取本地缓存
        getLocalData:function(_key){
            try {
                let localData = localStorage.getItem(_key);
                if ( localData == null || typeof localData === "undefined" || 
                     localData === "undefined" || localData === "") {
                    localData = '';
                }
                return localData;
            } catch (e) {
              console.error("YLSDK getLocalData error key:"+_key+", e:",e);
            }
            return '';
        },
        //平台登录
        loginPlatform:function(_callback){
            if(g.ylsdk_platform === pc.VIVO){
                p_f.loginVIVO(_callback);
            }else if (g.ylsdk_platform === pc.MZ){
                p_f.loginMZ(_callback);
            }else{
                p_f.loginOPPO(_callback);
            }
        },
        //OPPO登录
        loginOPPO(_callback){
            qg.login({
                success: (res) => {
                    let data = {
                        code:res.data.token,
                        needPass:false
                    };
                    if(sdk_data.loginInfo){
                        sdk_data.loginInfo.token=res.data.token || "";
                    }else{
                        sdk_data.loginInfo = {
                            token:res.data.token || "",
                        };
                    }
                    if (sdk_data.switchLog) console.log("YLSDK qg.login-登录成功:",JSON.stringify(res));
                    nets.loginSDKServer(data,_callback);
                },
                fail: (res) => {
                    if (sdk_data.switchLog) console.error("YLSDK qg.login-登录失败",JSON.stringify(res));
                    if(_callback) _callback(false);
                },
            });
        },
        //VIVO登录
        loginVIVO(_callback){
            if (sys.platformVersionCode && sys.platformVersionCode >= 1053) {
                qg.login().then((res) => {
                    if (res.data.token) {
                        let data = {
                            code:res.data.token,
                            needPass:false
                        };
                        if(sdk_data.loginInfo){
                            sdk_data.loginInfo.token=res.data.token || "";
                        }else{
                            sdk_data.loginInfo = {
                                token:res.data.token || "",
                            };
                        }
                        if (sdk_data.switchLog) console.log("YLSDK qg.login-登录成功:",JSON.stringify(res));
                        nets.loginSDKServer(data,_callback);
                    }
                }, (err) => {
                    if (sdk_data.switchLog) console.error("YLSDK qg.login-登录失败",JSON.stringify(res));
                    if(_callback) _callback(false);
                });
            }else{
                qg.authorize({
                    type: "token",
                    success: function (data) {
                        let _data = {
                            code:data.accessToken,
                            needPass:false
                        };
                        if(sdk_data.loginInfo){
                            sdk_data.loginInfo.token=data.accessToken || "";
                        }else{
                            sdk_data.loginInfo = {
                                token:data.accessToken || "",
                            };
                        }
                        if (sdk_data.switchLog) console.log("YLSDK qg.login-登录成功:",JSON.stringify(data));
                        nets.loginSDKServer(_data,_callback);
                    },
                    fail: function (data, code) {
                        if (sdk_data.switchLog) console.log("YLSDK qg.login-登录失败-data:",JSON.stringify(data),JSON.stringify(code));
                        if(_callback) _callback(false);
                    }
                })
            }
        },
        //魅族登录
        loginMZ(_callback){
            mz.login({
                success: (res) =>{
                    var _data = JSON.stringify(res);
                    console.warn("YLSDK login success data = " + _data);
                    if (res.token) {
                        let data = {
                            code:res.token,
                            needPass:false,
                            pkgName:sdk_data.ylsdk_pkg_name
                        };
                        if(sdk_data.loginInfo){
                            sdk_data.loginInfo.token=res.token || "";
                        }else{
                            sdk_data.loginInfo = {
                                token:res.token || "",
                            };
                        }

                        if (sdk_data.switchLog) console.log("YLSDK YLSDK loginMZ-登录成功:",JSON.stringify(res));
                        nets.loginSDKServer(data,_callback);

                        sdk_data.user_platform_info = res;
                        utils.saveUserPlatfromInfo(JSON.stringify(sdk_data.user_platform_info));
                    }
                },
                fail: (res) =>{
                    if(res.code == 4){
                        //用户取消登录（没输入帐号就退出）
                    }else if(res.code == 20){
                        //用户点击授权弹框的取消按钮，取消个人信息授权

                    }else{

                    }

                    var data = JSON.stringify(res);
                    console.warn("YLSDK login fail data = " + data);
                    p_f.getTokenMZ(_callback);
                },
                complete: () =>{
                    console.warn("YLSDK login complete");
                }
            });
        },
        //魅族token登录
        getTokenMZ(_callback){
            mz.getToken({
                success: (res) =>{
                    var _data = JSON.stringify(res);
                    console.log("YLSDK getToken success data = " + _data);

                    let codeORtoken = res.token;
                    let data = {
                        code:codeORtoken,
                        needPass:false,
                        pkgName:'anonymousCode'
                    };
                    let identity = "以游客身份";
                    if(sdk_data.loginInfo){
                        sdk_data.loginInfo.code=codeORtoken || "";
                        sdk_data.loginInfo.token=codeORtoken || "";
                        sdk_data.loginInfo.pkgName=sdk_data.ylsdk_pkg_name;
                    }else{
                        sdk_data.loginInfo = {
                            code:codeORtoken || "",
                            token:codeORtoken || "",
                            pkgName:sdk_data.ylsdk_pkg_name || "",
                        };
                    }
                    if (sdk_data.switchLog) console.log("YLSDK loginInfo:",JSON.stringify(sdk_data.loginInfo));
                    if (sdk_data.switchLog) console.log("YLSDK getTokenMZ-登录成功:",res);
                    nets.loginSDKServer(data,_callback);
                },
                fail: (res) =>{
                    var data = JSON.stringify(res);
                    console.log("YLSDK getToken fail data = " + data);
                    if(res.code == 4){
                        //用户取消登录（没输入帐号就退出）
                    }else{

                    }
                },
                complete: () =>{
                    console.log("YLSDK getToken complete");
                }
            });
        },
        //获取小游戏启动时的参数
        getLaunchOptionsSync:function(){
            let launchOptions = qg.getLaunchOptionsSync();
            if (sdk_data.switchLog)
                console.log("YLSDK qg.getLaunchOptionsSync：",JSON.stringify(launchOptions))
            return launchOptions;
        },
        createBannerAd(isSmall,show,_callback){
            if(!g.ylsdk_banner_ids || g.ylsdk_banner_ids.length == 0){
                console.error("YLSDK 请在配置文件中配置banner ID");
                return;
            }
            let ad_index = Math.floor(Math.random()*g.ylsdk_banner_ids.length);
            let _adId = g.ylsdk_banner_ids[ad_index];
            if(sdk_data.bannerAd){
                p_f.removeBannerAd();
            }
            //是否强制刷新banner
            let forceUpdate = (sdk_data.gameConfig && sdk_data.gameConfig.bannerTimeSpace && sdk_data.gameConfig.bannerTimeSpace.forceOnOff); 
            if(forceUpdate) sdk_data.gameConfig.bannerTimeSpace.isSmall = isSmall;
            if(g.ylsdk_platform === pc.VIVO){
                let ww = isSmall ? 300 : sys.screenWidth;
                let left = (sys.screenWidth - ww) / 2;
                if(sys.platformVersionCode < 1031){
                    if (sdk_data.switchLog) console.warn(`YLSDK VIVO平台版本号${sys.platformVersionCode}< 1031 无法展示banner广告`);
                    return ;
                }
                sdk_data.bannerAd = qg.createBannerAd({
                    posId: _adId,
                    style:{}
                });
                if (sdk_data.switchLog) 
                    console.log("YLSDK qg.createBannerAd:",_adId);
                sdk_data.bannerAd.onLoad(() => {
                    if (sdk_data.switchLog) console.log('YLSDK qg.createBannerAd---onLoad 广告加载成功-show:',show);
                    if(show){
                        p_f.showBannerAd();
                    } 
                });
                sdk_data.bannerAd.onError(err => {
                    if (sdk_data.switchLog) console.warn("YLSDK qg.createBannerAd---onError:",err);
                    sdk_data.bannerAd = null;
                    if(forceUpdate){
                        let delay = sdk_data.gameConfig.bannerTimeSpace.timeSpace || sdk_data.defaultbTS;
                        if (sdk_data.switchLog) console.warn(`YLSDK 将在${delay}秒后重新创建banner(调用ylBannerAdHide会取消重试)`);
                        p_f.delayUpdateBanner(delay,isSmall,show);
                    }
                });
                sdk_data.bannerAd.onSize(res => {
                    if(sdk_data.bannerAd){
                        sdk_data.bannerAd.style.top = sys.screenHeight-res.height;
                        sdk_data.bannerAd.style.left = (sys.screenWidth - res.width) / 2;
                        if (sdk_data.switchLog) 
                            console.log("YLSDK qg.createBannerAd---onResize:",sdk_data.bannerAd.style.realWidth, sdk_data.bannerAd.style.realHeight,sdk_data.bannerAd.style.top);
                    }
                });
            }else{
                if(g.ylsdk_platform === pc.OPPO && sys.platformVersion < 1051){
                    if (sdk_data.switchLog) console.warn(`YLSDK OPPO平台版本号${sys.platformVersion}< 1051 无法展示banner广告`);
                    return ;
                }
                if(g.ylsdk_platform === pc.MZ && sys.platformVersion < 1064){
                    if (sdk_data.switchLog) console.warn(`YLSDK 魅族平台版本号${sys.platformVersion}< 1064 无法展示banner广告`);
                    return ;
                }
                let ww = isSmall ? 300 : sys.screenWidth;
                let left = (sys.windowWidth - ww) / 2;
                sdk_data.bannerAd = qg.createBannerAd({
                    adUnitId: _adId,
                    style:{
                        left: left,
                        top: sys.windowHeight-sys.windowWidth*0.28695,
                        width: ww,
                    }
                });
                // if (sdk_data.switchLog) 
                //     console.log("YLSDK platform.createBannerAd:",_adId,sdk_data.bannerAd.style.realWidth, sdk_data.bannerAd.style.realHeight,sdk_data.bannerAd.style.top);
                sdk_data.bannerAd.onLoad(() => {
                    if (sdk_data.switchLog) console.log('YLSDK platform.createBannerAd---onLoad 广告加载成功');
                    if(show){
                        p_f.showBannerAd();
                    } 
                });
                sdk_data.bannerAd.onError(err => {
                    if (sdk_data.switchLog) console.warn("YLSDK platform.createBannerAd---onError:",err);
                    sdk_data.bannerAd = null;
                    if(forceUpdate){
                        let delay = sdk_data.gameConfig.bannerTimeSpace.timeSpace || sdk_data.defaultbTS;
                        if (sdk_data.switchLog) console.warn(`YLSDK 将在${delay}秒后重新创建banner(调用ylBannerAdHide会取消重试)`);
                        p_f.delayUpdateBanner(delay,isSmall,show);
                    }
    
                });
                sdk_data.bannerAd.onResize(res => {
                    if(sdk_data.bannerAd){
                        sdk_data.bannerAd.style.top = sys.windowHeight-res.height;
                        sdk_data.bannerAd.style.left = (sys.windowWidth - res.width) / 2;
                        if (sdk_data.switchLog) 
                            console.log("YLSDK platform.createBannerAd---onResize:",sdk_data.bannerAd.style.realWidth, sdk_data.bannerAd.style.realHeight,sdk_data.bannerAd.style.top);
                    }
                });
            }
        },
        //延迟刷新banner
        delayUpdateBanner(delay,isSmall,show){
            if (sdk_data.switchLog) console.log("YLSDK delayUpdateBanner-delay,isSmall,show:",delay,isSmall,show);
            if (delay > 0) { //大于0才刷新
                if(sdk_data.bannerTimer){
                    clearTimeout(sdk_data.bannerTimer);
                    sdk_data.bannerTimer = null;
                }
                sdk_data.bannerTimer = setTimeout(() => {
                    if (sdk_data.switchLog) console.log('定时重新刷新横幅广告,delay,show_banner:',delay,sdk_data.bannerAd.show_banner);
                    if(sdk_data.bannerAd.show_banner){
                        p_f.createBannerAd(isSmall,show);
                    }
                }, (delay * 1000));
            }
        },
        //显示banner
        showBannerAd(){            
            if (sdk_data.switchLog) console.log("YLSDK BannerAd.show:",sdk_data.bannerAd);
            let isSmall = true;
            let create = false;
            let forceUpdate = (sdk_data.gameConfig && sdk_data.gameConfig.bannerTimeSpace && sdk_data.gameConfig.bannerTimeSpace.forceOnOff);
            if(forceUpdate) isSmall = sdk_data.gameConfig.bannerTimeSpace.isSmall || false;
            if(sdk_data.bannerAd){
                let nowTime = new Date().getTime();
                let last = sdk_data.bannerAd.lastTime || nowTime;
                if(forceUpdate){
                    let timeSpace = forceUpdate.timeSpace || sdk_data.defaultbTS;//定时时间间隔(秒)
                    if((nowTime - last) >= timeSpace*1000){
                        //强制更新banner
                        create = true;
                        console.log("YLSDK showBannerAd-createBannerAd:",create);
                        p_f.createBannerAd(isSmall,true);
                    }
                }
                if(!create){
                    //强制更新就不show了
                    if(g.ylsdk_platform === pc.VIVO){
                        let adShow = sdk_data.bannerAd.show();
                        adShow && adShow.then(() => {
                            sdk_data.bannerAd.show_banner = true;
                          }).catch(err => {
                            if (sdk_data.switchLog) console.warn("YLSDK banner广告展示失败", err);
                          });
                    }else{
                        sdk_data.bannerAd.show();
                        sdk_data.bannerAd.show_banner = true;
                    }
                }
                sdk_data.bannerAd.lastTime = nowTime;
                p.ylEventCount('showBannerAd');
            }else{
                if (sdk_data.switchLog) console.warn("YLSDK not BannerAd");
            }
            //
            if(!create && forceUpdate){
                let delay = sdk_data.gameConfig.bannerTimeSpace.timeSpace || sdk_data.defaultbTS;//定时时间间隔(秒)
                p_f.delayUpdateBanner(delay,isSmall,true);
            }
        },
        //隐藏banner
        hideBannerAd(){
            if (sdk_data.switchLog) console.log("YLSDK BannerAd.hide");
            if(sdk_data.bannerAd){
                if(g.ylsdk_platform === pc.VIVO){
                    let adHide = sdk_data.bannerAd.hide();
                    adHide && adHide.then(() => {
                      }).catch(err => {
                        if (sdk_data.switchLog) console.warn("YLSDK banner广告隐藏失败", err);
                      });
                }else{
                    sdk_data.bannerAd.hide();
                }
                if(sdk_data.bannerTimer){
                    clearTimeout(sdk_data.bannerTimer);
                    sdk_data.bannerTimer = null;
                }
                sdk_data.bannerAd.show_banner = false;
            } 
        },
        //删除banner
        removeBannerAd(){
            if (sdk_data.switchLog) console.log("YLSDK BannerAd.destroy");
            if(sdk_data.bannerAd){
                if(sdk_data.bannerTimer){
                    clearTimeout(sdk_data.bannerTimer);
                    sdk_data.bannerTimer = null;
                }
                sdk_data.bannerAd.hide();
                if(g.ylsdk_platform === pc.VIVO){
                    let adDestroy = sdk_data.bannerAd.destroy();
                    adDestroy && adDestroy.then(() => {
                        if (sdk_data.switchLog) console.log("banner广告销毁成功");
                      }).catch(err => {
                        if (sdk_data.switchLog) console.warn("banner广告销毁失败", err);
                      });
                }else{
                    sdk_data.bannerAd.destroy();
                }
                sdk_data.bannerAd = null;
            }
        },
        /**
         * 创建video，单例创建一次就行了，显示的时候获取视频即可
         
            VIDEO_PLAY_FAIL:0    //视频广告-播放失败
            VIDEO_PLAY_FINISH:1  //视频广告-播放完成
            VIDEO_PLAY_CANCEL:2  //视频广告-播放取消
         */
        createVideoAd(){
            let ad_index = Math.floor(Math.random()*g.ylsdk_video_ids.length);
            let advert = g.ylsdk_video_ids[ad_index];
            if (!sdk_data.videoAd) {
                if(g.ylsdk_platform === pc.OPPO && sys.platformVersion < 1051){
                    if (sdk_data.switchLog) console.warn(`YLSDK OPPO平台版本号${sys.platformVersion}< 1051 无法展示激励视频广告`);
                    return ;
                }
                if(g.ylsdk_platform === pc.MZ && sys.platformVersion < 1064){
                    if (sdk_data.switchLog) console.warn(`YLSDK 魅族平台版本号${sys.platformVersion}< 1064 无法展示激励视频广告`);
                    return ;
                }
                if(g.ylsdk_platform === pc.VIVO && sys.platformVersionCode < 1041){
                    if (sdk_data.switchLog) console.warn(`YLSDK VIVO平台版本号${sys.platformVersionCode}< 1041 无法展示激励视频广告`);
                    return ;
                }
                if(!qg.createRewardedVideoAd) return;
                if(g.ylsdk_platform === pc.VIVO){
                    sdk_data.videoAd = qg.createRewardedVideoAd({ posId: advert});
                }else{
                    sdk_data.videoAd = qg.createRewardedVideoAd({ adUnitId: advert});
                }
                sdk_data.videoAd.adUnitId = advert;
                // //视频加载成功
                // sdk_data.videoAd.onLoad(() => {
                //     if (sdk_data.switchLog) console.log('YLSDK RewardedVideoAd.onLoad');
                //     // if (sdk_data.onVideoClose) sdk_data.onVideoClose(sdk_data.VIDEO_LOAD_SUCCESS);
                // });

                //点击关闭按钮
                sdk_data.videoAd.onClose(res => {
                    sdk_data.show_video = false;
                    if (res && res.isEnded || res === undefined) {
                        if (sdk_data.switchLog) console.log('YLSDK 视频播放完成',sdk_data.videoAd.adUnitId);
                        // 正常播放结束，调用游戏逻辑。
                        if (sdk_data.onVideoClose) sdk_data.onVideoClose(sdk_data.VIDEO_PLAY_FINISH);
                        utils.addWatchTVnums(sdk_data.unlockCustoms);
                        utils.addPower(1);
                        p.ylStatisticViedo(1,sdk_data.videoAd.adUnitId);
                    } else {
                        // 播放中途退出。
                        if (sdk_data.onVideoClose) sdk_data.onVideoClose(sdk_data.VIDEO_PLAY_CANCEL);
                        if (sdk_data.switchLog) console.warn('YLSDK 视频播放取消');
                    }
                    sdk_data.unlockCustoms = false;
                });

                //视频加载失败
                sdk_data.videoAd.onError(err => {
                    if (sdk_data.switchLog) console.warn('YLSDK RewardedVideoAd.onError:',err,sdk_data.videoAd.adUnitId);
                    if (sdk_data.onVideoClose) sdk_data.onVideoClose(sdk_data.VIDEO_PLAY_FAIL);
                    sdk_data.unlockCustoms = false;
                    sdk_data.show_video = false;
                });
            } else {
                if (sdk_data.videoAd.adUnitId === advert) return;
                sdk_data.videoAd.adUnitId = advert;
            }
        },
        /**
         * 显示video
         
            VIDEO_PLAY_FAIL:0    //视频广告-播放失败
            VIDEO_PLAY_FINISH:1  //视频广告-播放完成
            VIDEO_PLAY_CANCEL:2  //视频广告-播放取消
         */
        showVideoAd(cb,unlockCustoms){
            sdk_data.show_video = true;
            if (!sdk_data.videoAd) {
                p_f.createVideoAd();
            }
            p.ylStatisticViedo(0,sdk_data.videoAd.adUnitId);
            sdk_data.unlockCustoms = unlockCustoms;
            sdk_data.onVideoClose = cb;
            let bReturn = false;
            if(g.ylsdk_platform === pc.VIVO){
                let adLoad = sdk_data.videoAd.load()
                adLoad && adLoad.catch((err) => {
                    sdk_data.show_video = false;
                    if (!bReturn) {
                        if (sdk_data.switchLog) console.warn('YLSDK 视频加载失败啦！',sdk_data.videoAd.adUnitId);
                        bReturn = true;
                        if (sdk_data._timer) {
                            clearTimeout(sdk_data._timer);
                            sdk_data._timer = null;
                        }
                        if (cb) cb(sdk_data.VIDEO_PLAY_FAIL);
                    }
                });
                 //视频加载成功
                 sdk_data.videoAd.onLoad(() => {
                    if (sdk_data.switchLog) console.log('YLSDK RewardedVideoAd.onLoad');
                    if (sdk_data.onVideoClose) sdk_data.onVideoClose(sdk_data.VIDEO_LOAD_SUCCESS);
                    if (!bReturn) {
                        bReturn = true;
                        if (sdk_data._timer) {
                            clearTimeout(sdk_data._timer);
                            sdk_data._timer = null;
                        }
                        let adShow = sdk_data.videoAd.show();
                        adShow && adShow.catch(err => {
                            if (sdk_data.switchLog) console.warn('YLSDK 视频播放失败',sdk_data.videoAd.adUnitId);
                            if (cb) cb(sdk_data.VIDEO_PLAY_FAIL);
                            sdk_data.show_video = false;
                        })
                    }
                });
            }else{
                sdk_data.videoAd.load()
                .then(() => {
                    if (!bReturn) {
                        bReturn = true;
                        if (sdk_data._timer) {
                            clearTimeout(sdk_data._timer);
                            sdk_data._timer = null;
                        }
                        sdk_data.videoAd.show().then(() => {
                            if (sdk_data.switchLog) console.log('YLSDK 视频播放成功',sdk_data.videoAd.adUnitId);
                            // if (cb) cb(sdk_data.VIDEO_PLAY_SUCCESS);
                        }).catch(err => {
                            if (sdk_data.switchLog) console.warn('YLSDK 视频播放失败',sdk_data.videoAd.adUnitId);
                            if (cb) cb(sdk_data.VIDEO_PLAY_FAIL);
                            sdk_data.show_video = false;
                        })
                    }
                })
                .catch((err) => {
                    sdk_data.show_video = false;
                    if (!bReturn) {
                        if (sdk_data.switchLog) console.warn('YLSDK 视频加载失败啦！',sdk_data.videoAd.adUnitId);
                        bReturn = true;
                        if (sdk_data._timer) {
                            clearTimeout(sdk_data._timer);
                            sdk_data._timer = null;
                        }
                        if (cb) cb(sdk_data.VIDEO_PLAY_FAIL);
                    }
                });
                //视频加载成功
                sdk_data.videoAd.onLoad(() => {
                    if (sdk_data.switchLog) console.log('YLSDK RewardedVideoAd.onLoad');
                    // if (sdk_data.onVideoClose) sdk_data.onVideoClose(sdk_data.VIDEO_LOAD_SUCCESS);
                });
            }
            //
            if (!sdk_data._timer) {
                sdk_data._timer = setTimeout(() => {
                    sdk_data._timer = null;
                    sdk_data.show_video = false;
                    if (!bReturn) {
                        if (sdk_data.switchLog) console.warn('YLSDK 4秒内无视屏回调，自动回调加载失败',sdk_data.videoAd.adUnitId);
                        bReturn = true;
                        if (cb) cb(sdk_data.VIDEO_PLAY_FAIL);
                        
                    }
                }, 4000);
            }
            p.ylEventCount('showVideoAd');
        },
    };
    //服务端接口相关
    var nets = {
        //坚决获取自定义配置列表
        customconfigFinal:function(_callback){
            request({
                method:"get",
                url:sdk_data.domain_online+"/gamebase/customconfig/final?appid="+g.ylsdk_app_id+"&version="+g.ylsdk_version,
                needPass:false,
                callback(res) {
                    var _data = JSON.parse(res);
                    utils.res_log("Response--customconfig-final:",_data.code,res)
                    if(_data.code === 9002) p_f.loginPlatform();
                    if (_data.code == 0 && _data.result  && _data.result.length >0) {
                        sdk_data.custom = _data.result; 
                    }
                    if(_callback){
                        _callback(_data.result);
                    }
                }
            })
        },
        /**
         * 上传打点数据
         *
         * 返回参数说明：
         * 参数        类型           说明
         * code       Integer      code=0 表示成功 其他值表示出错
         * msg        String       code=0 时为空 其他为错误信息
         **/
        statisticsEvents:function(cData,isClear){
            let postData = { 
                "events": cData 
            };
            request({
                method:"post",
                url:sdk_data.domain_online+"/statistics/event",
                needPass:true,
                data:postData,
                callback(res) {
                    let _data = JSON.parse(res);
                    utils.res_log("Response--event:",_data.code,res);
                    if (_data.code === 0) {
                        if (isClear) {
                            utils.clearLocalStatistics();
                        }
                    }else {
                        for (var i = 0; i < cData.length; i++) {
                            sdk_data.statistics.events.unshift(cData.pop());
                        }
                    }
                    if(_data.code === 9002) p_f.loginPlatform();
                }
            });
        },
        //视频统计(打点统计）
        statisticViedo:function(_data,_callback){
            if(sdk_data.switchInfo.switchVideo=== 0){
                if (sdk_data.switchLog) console.warn('YLSDK 停用视频统计接口！');
                return;
            }
            request({
                method:"post",
                url:sdk_data.domain_online+"/statistics/video",
                needPass:true,
                data:_data,
                callback(res) {
                    let data = JSON.parse(res);
                    utils.res_log("Response--statistics-video:",data.code,res)
                    if(data.code != 0){
                        sdk_data.statistics.video.push(_data);
                    }
                    if(data.code === 9002) p_f.loginPlatform();
                    if(_callback){
                        _callback(res);
                    }
                }
            })
        },
        //结果统计(打点统计）
        statisticResult:function(_data,_callback){
            if(sdk_data.switchInfo.switchResult=== 0){
                if (sdk_data.switchLog) console.warn('YLSDK 停用结果统计接口！');
                return;
            }
            request({
                method:"post",
                url:sdk_data.domain_online+"/statistics/result",
                needPass:true,
                data:_data,
                callback(res) {
                    let data = JSON.parse(res);
                    utils.res_log("Response--result:",data.code,res)
                    if(data.code != 0){
                        sdk_data.statistics.result.push(_data);
                    }
                    if(data.code === 9002) p_f.loginPlatform();
                    if(_callback){
                        _callback(data.code === 0);
                    }
                }
            })
        },
        //SDK服务器登录
        loginSDKServer:function(data,_callback){
            nets.loginServer(data,function(res){
                let results = JSON.parse(res);
                if(results.code === 0){
                    sdk_data.switchLog = (results.result.switchLog === 1);
                    if(g.ylsdk_debug_model) console.warn("YLSDK 日志开关(云控)-",sdk_data.switchLog ? "打开" : "关闭")
                    //设置用户信息
                    sdk_data.loginInfo = {
                        accountId:results.result.accountId,
                        nickName:results.result.nickName || '',
                        avatarUrl:results.result.avatarUrl || '',
                        newPlayer:(results.result.newPlayer !== 0 ? 1 : 0),
                        openid:results.result.openid || "",
                        token:data.code || "",
                    };
                    // //保存基础数据
                    if(results.result.accountId) utils.SetAccountID(results.result.accountId);        
                    if(results.result.accountPass) utils.SetAccountPass(results.result.accountPass);
                    if(results.result.isLowVersion && results.result.isLowVersion === 1){
                        console.error("/*****************************************/");
                        console.error("**  YLSDK 您的SDK版本过低，请尽快升级SDK  **");
                        console.error("/*****************************************/");
                    }
                    //开关设置
                    sdk_data.switchInfo = {
                        switchTouch:results.result.switchTouch || 0,
                        switchPush:results.result.switchPush || 0,
                        switchLog:results.result.switchLog || 0,
                    }
                    sdk_data.switchTouchAll = results.result.switchTouch || 0;//原始误触总开关(不开放给CP，SDK自己判断用的)
                    //记录进入游戏的时间戳
                    if(sdk_data.need_statistics_on_show){
                        sdk_data.lastTime=new Date().getTime();
                        sdk_data.need_statistics_on_show = false;
                    }
                    sdk_data.switchInfo.switchLogin = (results.result.switchLogin === 0)? results.result.switchLogin : 1;   
                    sdk_data.switchInfo.switchJump = (results.result.switchJump === 0)? results.result.switchJump : 1;     
                    sdk_data.switchInfo.switchShare = (results.result.switchShare=== 0) ? results.result.switchShare : 1;  
                    sdk_data.switchInfo.switchEvent = (results.result.switchEvent=== 0) ? results.result.switchEvent : 1;     
                    sdk_data.switchInfo.switchLayer = (results.result.switchLayer === 0)? results.result.switchLayer : 1;    
                    sdk_data.switchInfo.switchResult = (results.result.switchResult=== 0) ? results.result.switchResult : 1; 
                    sdk_data.switchInfo.switchVideo = (results.result.switchVideo=== 0) ? results.result.switchVideo : 1;
                    if (sdk_data.switchLog) console.log("YLSDK switchInfo:",JSON.stringify(sdk_data.switchInfo));
                    //视频分享策略
                    if(results.result.moduleList && results.result.moduleList.length > 0){
                        sdk_data.moduleList = results.result.moduleList;
                        sdk_data.moduleList.forEach(function(element){  
                            element.loopIndex = 0;
                            if(element.logicJson) element.logicJson = JSON.parse(element.logicJson)
                        });
                        if (sdk_data.switchLog) console.warn("YLSDK share_or_video2:",sdk_data.moduleList);
                    }
                    // //获取侧边栏列表数据
                    // nets.SideBox();

                    //启动打点定时器
                    sdk_data.statistics = utils.getLocalStatistics();
                    utils.registerInterval();
                    if(_callback) _callback(true);
                }else{
                    if(sdk_data.login_count < 3){
                        utils.SetAccountPass('');
                        nets.login(_callback);
                    }else{
                        console.error("YLSDK SDK初始化失败 ");
                        if(_callback) _callback(false);
                    }
                }
                //获取游戏配置
                nets.getGameConfig();
            });
        },
        /**
         * 登录SDK服务器
         * @param l_data
         * @param _callback 
        **/
        loginServer:function(l_data,_callback){
            let extraData = {};
            let _data = {
                        platform:g.ylsdk_platform || '',//默认为微信平台，微信平台可以不传或传空串
                        appid:g.ylsdk_app_id,
                        version:g.ylsdk_version,
                        pkgName:g.ylsdk_pkg_name,//qq、oppo平台必选,其他平台可忽略
                        shareInfo:extraData,
                    };
            if(l_data.code) _data.code = l_data.code;
            if (sdk_data.switchLog)
                console.log("YLSDK 登录SDK服务器-请求:",JSON.stringify(_data));
            request({
                method:"post",
                url:sdk_data.domain_online+"/user/login",
                needPass:l_data.needPass,
                data:_data,
                callback(res) {
                    var _data = JSON.parse(res);
                    utils.res_log("Response--login:",_data.code,res);
                    if(_callback){
                        _callback(res);
                    }
                }
            })
        },
        //登录
        login:function(_callback){
            if(sdk_data.switchInfo.switchLogin=== 0){
                if (sdk_data.switchLog) console.warn('YLSDK 该游戏被禁止！');
                return;
            }
            sdk_data.login_count +=1;
            let accountPass = utils.getAccountPass();
            if(!accountPass || '' === accountPass){
                if (sdk_data.switchLog)
                    console.log("YLSDK 通过平台 token登录");
                //如果没有account_id缓存，则需要先登录微信，
                //拿到微信的登录凭证(code)，再通过code去登录SDK服务器
                p_f.loginPlatform(_callback);
            }else{
                sdk_data.accountPass = accountPass;
                if (sdk_data.switchLog)
                    console.log("YLSDK 通过accountPass登录");
                //如果已经有account_id缓存,说明已经登录过微信，
                //直接用account_id登录SDK服务器
                let data = {
                    needPass:true
                };
                nets.loginSDKServer(data,_callback);
            }
        },
        //获取侧边栏列表
        SideBox:function(_callback){
            if(sdk_data.boxconfig){
                if(_callback){
                    _callback(sdk_data.boxconfig);
                }
            }else{
                request({
                    method:"get",
                    url:sdk_data.domain_online+"/gamebase/sidebox",
                    needPass:true,
                    callback(res) {
                        var _data = JSON.parse(res);
                        utils.res_log("Response--sidebox:",_data.code,res);
                        if (_data.code == 0 && _data.result) {
                            utils.checkJumpOutTime();
                            sdk_data.boxconfig = _data.result;
                            //
                            let jumpInfo = utils.getJumpOutInfo(); 
                            let jumpOutInfo = (!jumpInfo || jumpInfo === '') ? false : JSON.parse(jumpInfo);
                            if(jumpOutInfo && jumpOutInfo.list &&  jumpOutInfo.list.length >0){
                                //
                                for(let j=0;j<jumpOutInfo.list.length;j++){
                                    let appid_1 = jumpOutInfo.list[j].appid;
                                    for(let i=0;i<sdk_data.boxconfig.length;i++){
                                        let appid_2 = sdk_data.boxconfig[i].toAppid;
                                        if(appid_2 == appid_1){
                                            if (sdk_data.switchLog)
                                                console.log("YLSDK 删除已经点过的卖量项:",sdk_data.boxconfig[i]._id,appid_2);
                                            sdk_data.boxconfig.splice(i,1);
                                        }
                                    }
                                }
                            }
                            if(_callback){
                                _callback(sdk_data.boxconfig);
                            }
                        }else{
                            nets.newestsideboxFinal(_callback);
                        }
                    }
                })
            }
        },
        //坚决获取侧边栏列表
        newestsideboxFinal:function(_callback){
            request({
                method:"get",
                url:sdk_data.domain_online+"/gamebase/sidebox/final?appid="+g.ylsdk_app_id+"&version="+g.ylsdk_version,
                needPass:false,
                callback(res) {
                    var _data = JSON.parse(res);
                    utils.res_log("Response--sidebox-final:",_data.code,res);
                    utils.checkJumpOutTime();
                    if (_data.code == 0 && _data.result) {
                        sdk_data.boxconfig = _data.result;
                        //
                        let jumpInfo = utils.getJumpOutInfo(); 
                        let jumpOutInfo = (!jumpInfo || jumpInfo === '') ? false : JSON.parse(jumpInfo);
                        if(jumpOutInfo && jumpOutInfo.list &&  jumpOutInfo.list.length >0){
                            //
                            for(let j=0;j<jumpOutInfo.list.length;j++){
                                let appid_1 = jumpOutInfo.list[j].appid;
                                for(let i=0;i<sdk_data.boxconfig.length;i++){
                                    let appid_2 = sdk_data.boxconfig[i].toAppid;
                                    if(appid_2 == appid_1){
                                        if (sdk_data.switchLog)
                                            console.log("YLSDK 删除已经点过的卖量项-final:",sdk_data.boxconfig[i]._id,appid_2);
                                        sdk_data.boxconfig.splice(i,1);
                                    }
                                }
                            }
                        }
                    }
                    if(_callback){
                        _callback(sdk_data.boxconfig);
                    }
                }
            })
        },
        //获取积分墙列表
        IntegralWall:function(_callback){
            if(sdk_data.boardconfig){
                if(_callback){
                    _callback(sdk_data.boardconfig);
                }
            }else{
                request({
                    method:"post",
                    url:sdk_data.domain_online+"/gamebase/scoreboard",
                    needPass:true,
                    data:{
                        module:'scoreboard' //表示某个模块的签到，目前就填写scoreboard
                    },
                    callback(res) {
                        var data = JSON.parse(res);
                        utils.res_log("Response--scoreboard:",data.code,res);
                        if (data.code == 0) {
                            if (data.result) {
                                sdk_data.boardconfig = data.result;
                            }
                            if(_callback){
                                _callback(data);
                            }
                        }else{
                            nets.newestscoreboardFinal(_callback);
                        }
                    }
                })
            }
        },
        //坚决获取积分墙列表
        newestscoreboardFinal:function(_callback){
            request({
                method:"get",
                url:sdk_data.domain_online+"/gamebase/scoreboard/final?appid="+g.ylsdk_app_id+"&version="+g.ylsdk_version,
                needPass:false,
                data:{
                    module:'scoreboard' //表示某个模块的签到，目前就填写scoreboard
                },
                callback(res) {
                    var data = JSON.parse(res);
                    utils.res_log("Response--scoreboard-final:",data.code,res);
                    if(data.code === 9002) p_f.loginPlatform();
                    if (data.code == 0 && data.result) {
                        if (data.result) {
                            sdk_data.boardconfig = data.result;
                        }
                    }
                    if(_callback){
                        _callback(data);
                    }
                }
            })
        },
        /**
         * 流失统计
         *
         * 返回参数说明：
         * 参数        类型           说明
         * code       Integer      code=0 表示成功 其他值表示出错
         * msg        String       code=0 时为空 其他为错误信息
         **/
        commitStaticsLayer:function(){
            if(!sdk_data._AccountId){ console.error("YLSDK yl_sdk-commitStaticsLayer : _AccountId is null !"); return; };
            if(!sdk_data.layer){ console.error("YLSDK yl_sdk-ccommitStaticsLayer : layer is null !"); return; };
            if(sdk_data.switchInfo.switchLayer=== 0){
                if (sdk_data.switchLog) console.warn('YLSDK 停用流失统计接口！');
                return;
            }
            let postData = { 
                layer:sdk_data.layer || 'default'
            };
            nets.statisticsLayer(postData);
        },
        //流失统计
        statisticsLayer(_data){
            request({
                method:"post",
                url:sdk_data.domain_online+"/statistics/layer",
                needPass:true,
                data:_data,
                callback(res) {
                    var data = JSON.parse(res);
                    utils.res_log("Response--layer:",data.code,res);
                    if(data.code != 0){
                        sdk_data.statistics.layer.push(_data);
                    }
                    if(data.code === 9002) p_f.loginPlatform();
                }
            });
        },
        /**
         * 卖量统计
         * @param iconId       Icon图片的ID
         * @param source       从哪个模块导出的，该字段具体值由调用方自行定义
         * @param targetAppId  导出游戏的appid
         * @param isClick      用户点击与否[true:点击,false:取消]
         * @param _callback
         *
         * 返回参数说明：
         * 参数      类型         说明
         * code    Integer    code=0 表示成功 其他值表示出错
         * msg     String     code=0 时为空 其他为错误信息
         */
        ClickOut:function (iconId,targetAppId,source,isClick,_callback) {
            if(!iconId){console.error('YLSDK iconId is null !'); return;}
            if(!targetAppId){console.error('YLSDK toAppId is null !'); return;}
            if(sdk_data.switchInfo.switchJump=== 0){
                if (sdk_data.switchLog) console.warn('YLSDK 停用卖量统计接口！');
                return;
            }
            let _data = {
                iconId:iconId,
                source:source,
                target:targetAppId,
                action:(isClick === true ? "enable" : "cancel"),
            };
            nets.statisticsClickOut(_data,_callback);
        },
        //卖量统计
        statisticsClickOut(_data,_callback){
            request({
                method:"post",
                url:sdk_data.domain_online+"/statistics/clickcount",
                needPass:true,
                data:_data,
                callback(res) {
                        let data = JSON.parse(res);
                        utils.res_log("Response--clickcount:",data.code,res);
                        if(data.code != 0){
                            sdk_data.statistics.clickcount.push(_data);
                        }
                        if(data.code === 9002) p_f.loginPlatform();
                    if(_callback){
                        _callback(res);
                    }
                }
            })
        },
        //获取游戏配置列表
        getGameConfig:function(_callback){
            request({
                method:"get",
                url:sdk_data.domain_online+"/gamebase/config",
                needPass:true,
                callback(res) {
                    let data = JSON.parse(res);
                    utils.res_log("Response--config:",data.code,res);
                    if(data.code === 0){
                        utils.parseGameConfig(data.result,_callback);
                    }else{
                        nets.getGameConfigFinal(_callback);
                    }
                }
            })
        },
        //获取游戏配置列表-强制
        getGameConfigFinal:function(_callback){
            request({
                method:"get",
                url:sdk_data.domain_online+"/gamebase/config/final",
                needPass:false,
                callback(res) {
                    let data = JSON.parse(res);
                    utils.res_log("Response--config-final:",data.code,res);
                    if(data.code === 0){
                        utils.parseGameConfig(data.result,_callback);
                    }else if(data.code === 9002){
                        p_f.loginPlatform();
                        if(_callback) _callback('config_fail');
                    }
                }
            })
        },
    };

    var sdk_data = {
        _SDKversion:"2.0.3", //SDK版本号
        domain_online : "https://api.ylxyx.cn/api/collect/v1",   //"http://test-api.ylxyx.cn/api/collect/v1",  //          //线上环境服务器地址
        _AccountId : "",     //SDK后台生成的用户账号
        login_count:0,       //登录次数，在登录异常的情况下，重试三次登录如果都失败，则放弃并提示用户
        accountPass:null,    //接口调用密钥
        switchInfo:{         //开关信息
            switchTouch: 0,         //误触开关开关，1打开，0关闭
            switchPush: 0,          //推送开关
            switchLog: 0,       //日志开关
            switchLogin:1,     //登录开关,此开关关闭将禁止登录(禁用游戏)
            switchJump:1,      //卖量统计开关
            switchShare:1,     //分享统计开关
            switchEvent:1,     //事件统计开关
            switchLayer:1,     //流失统计开关
            switchResult:1,    //结果统计开关
            switchVideo:1,     //视频统计开关
        },     
        gameConfig:{//游戏配置信息  
            bannerTimeSpace: {
                onOff: false,
                timeSpace: 3,
                forceOnOff: true,
                forceTimeSpace: 3
            },
            Power: {
                defaultPower: 5,
                powerUpper: 5,
                recoveryTime: 300,
                getPower: 1
            },
            VideoUnlockCustoms: {
                onOff: false,
                openCustoms: 5,
                spaceCustoms: 2
            },
            depthShield: {
                _type: 0,
                totalCustoms: 3,
                goodUserOnOff: true,
                watchTvNum: 1,
                dayCustoms: [3]
            },
            mistouchTimer: {
                onOff: false,
                startTime_h: 9,
                startTime_m: 30,
                endTime_h: 23,
                endTime_m: 59
            },
            toalByTodayNum: 0,
            toalNum: 0,
            watchTvNum: 0,
            maxCustom: 0,
            lastCustom: 0,
            isServerData:false,//游戏配置数据来源[true:服务器,false:本地]
        },
        powerRtimer:null,       //恢复体力定时器
        userPowerNum:0,
        switchLog:true,        //日志开关
        need_statistics_on_show:true, //是否需要记录切入的时间戳

         //初始化SDK完成事件KEY
        boxconfig : null,         //侧边盒子数据
        boardconfig: null,         //积分墙列表数据
        custom:null,            //自定义游戏配置
        invite_account:-1,      //邀请者account_id(从分享进入游戏时可获取)
        loginInfo:null,         //登录信息
        statistics:{
            'events':[],
            'video':[],
            'result':[],
            'sharecard':[],
            'clickcount':[],
            'layer':[]
        },                      //打点-缓存队列    
        s_commit_dt:30000,      //打点-每次上传时间间隔(毫秒) 
        s_cache_ml:10000,       //打点-最大本地存储条数
        s_max_post_l:50,        //打点-每次最大上传条数
        s_interval:null,        //定时器ID
        layer:null,             //当前场景
        VIDEO_PLAY_FAIL:0,      //视频广告-播放失败
        VIDEO_PLAY_FINISH:1,    //视频广告-播放完成
        VIDEO_PLAY_CANCEL:2,    //视频广告-播放取消
        defaultbTS:10,  //默认banner刷新时间间隔
    };
    (function() {
        sdk_data.switchLog = g.ylsdk_debug_model;
    })(),qg.onShow(function(res) {
        console.log("YLSDK qg.onShow");
        sdk_data.statistics = utils.getLocalStatistics();
        utils.registerInterval();
        let sysInfo = p_f.getLaunchOptionsSync();

        if(sdk_data.accountPass){
            sdk_data.lastTime=new Date().getTime();
        }else{
            sdk_data.need_statistics_on_show = true;
        }
    }),qg.onHide(function() {
       console.log("YLSDK qg.onHide");
       utils.unRegisterInterval();
       utils.saveStatisticsToLocal();
       nets.commitStaticsLayer();
       utils.statisticsPlayTime();
       utils.savePowerLocal();
    });
    return YLsdkLaya;
})();