cc.Class({
    extends: cc.Component,

    properties: {
    },
    onLoad: function () {
        let that = this;
        qg.ylSetCurScene("MainScene");//一定记得在每个场景都要设置当前场景
        qg.ylInitSDK(function(){
            console.log("YLSDK ---ylInitSDK---初始化完成");
            this.doSDKinterface();
        }.bind(this));
    },
    /**
    * 调用SDK接口
    **/
    doSDKinterface(){
        qg.ylLog("YLSDK ---调用接口---");
        qg.ylEventCount("start_game");
        qg.ylSideBox(function (data) {
        }.bind(this));
        qg.ylGetCustom(function(data){
        }.bind(this));
        qg.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status){
            if(status){
                //干点什么
            }
        }.bind(this));
        let userInfo = qg.ylGetUserInfo();
        console.log("YLSDK ---用户信息---userInfo:",JSON.stringify(userInfo));
        let appid = qg.ylGetAppID();
        console.log("YLSDK ------appid:",JSON.stringify(appid));
        let inviteAccount = qg.ylGetInviteAccount();
        console.log("YLSDK ------inviteAccount:",JSON.stringify(inviteAccount));
        let switchInfo  = qg.ylGetSwitchInfo();
        console.log("YLSDK ------开关信息-switchInfo:",JSON.stringify(switchInfo));
        qg.ylStatisticViedo(0,"212456",function(data){
            console.log("YLSDK ------视频播放统计-ylStatisticViedo:",JSON.stringify(data));
        }.bind(this));
        this.testStoreValue();
    },
    //视频分享策略
    onShowShareOrVideo(){
        qg.ylShowShareOrVideo("chanel_2","model_2",function(type){
            switch(type){
                case 0:
                    console.warn("YLSDK ------策略-无");
                    break;
                case 1:
                    console.warn("YLSDK ------策略-分享");
                    break;
                case 2:
                    console.warn("YLSDK ------策略-视频");
                    break;
            }
        }.bind(this));
    },
    //测试自定义空间值
    testStoreValue(){
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    },
    test_sv_string(){
        //String
        qg.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    },
    test_sv_list(){
        //List
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 qg.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    qg.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    qg.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            qg.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    },
    test_sv_set(){
        //Set
        qg.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        qg.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                qg.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        qg.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                qg.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        qg.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            qg.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    },
    test_sv_hash(){
        //litMap
        qg.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                qg.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        qg.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qg.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                qg.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    },
    test_sv_radom(){
        //testRandom
        qg.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    },
});
