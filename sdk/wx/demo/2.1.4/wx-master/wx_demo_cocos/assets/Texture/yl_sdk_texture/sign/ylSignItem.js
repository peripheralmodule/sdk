
cc.Class({
    extends: cc.Component,
    properties: {
        nodeHasSign:cc.Node,
        lbDay:cc.Label,
        lbAwardValue:cc.Label,
        spAwardType:cc.Sprite,
        sfAwardType:[cc.SpriteFrame],
    },

    initData(obj) {
        this._obj = obj;
        if(this._obj){
            //state:"accepted"表示已经领取，"close"表示不可领取，"open"表示可领取
            this.nodeHasSign.active = (this._obj.state == 'accepted');
            this.lbDay.string = "第"+this._obj.id+"天"
            //此处根据自己的需求做相应修改，仅供参考
            if(this._obj.award && this._obj.award.length >0){
                this.lbAwardValue.string = this._obj.award[0].value;
                let size = this.spAwardType.node.getContentSize();
                this.spAwardType.spriteFrame = this._obj.award[0].type == 'prop_3' ? this.sfAwardType[0] : this.sfAwardType[1];
                this.spAwardType.node.setContentSize(size);
            }
        }
    },

});
