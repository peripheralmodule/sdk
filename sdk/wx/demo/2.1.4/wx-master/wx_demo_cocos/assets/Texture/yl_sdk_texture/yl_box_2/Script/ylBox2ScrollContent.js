
cc.Class({
    extends: cc.Component,

    properties: {
        nodeContent:cc.Node,
        pbItem:cc.Prefab,
    },

    initData(data,sceneName){
        if(!this.items) this.items = new Array();
        if(!this.items || this.items.length ==0 || this.items.length > data.length){
            for(let i=0;i<data.length;i++){
                let data_item = data[i];
                if(data_item.frameImage && data_item.frameImage != ""){
                    let playerNum = 22220 + Math.floor(Math.random()*500000);
                    data_item.playerNum = playerNum;

                    let random = Math.floor(Math.random()*10);
                    let star = random < 9 ? 5 : 4;
                    data_item.star = star;

                    let item = cc.instantiate(this.pbItem);
                    item.parent = this.nodeContent;
                    item.getComponent('ylBox2ScrollItem').setItemConf(data_item,sceneName);
                    this.items.push(item);
                }
            }
        }
    },
});
