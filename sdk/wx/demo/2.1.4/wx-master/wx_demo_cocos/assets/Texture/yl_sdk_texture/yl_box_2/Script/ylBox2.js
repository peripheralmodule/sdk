
cc.Class({
    extends: cc.Component,
    properties: {
        nodeContent:cc.Node,
        pbSpTitle:cc.Prefab,
        pbPageView:cc.Prefab,
        pbGrid:cc.Prefab,
        pbScroll:cc.Prefab,
        spFrame:[cc.SpriteFrame],
    },

    start () {
        cc.director.on(window.Global.EVENT_REFRESH_SIDEBOX, this.onRefreshSideBox.bind(this));
        this.titleInfos = [
            {
                spriteFrame:this.spFrame[0],
                title:'热度正在飙升',
                titleColor:cc.color(191,33,48,255),
            },
            {
                spriteFrame:this.spFrame[1],
                title:'最新发布',
                titleColor:cc.color(40,107,64,255),
            },
            {
                spriteFrame:this.spFrame[2],
                title:'五花八门',
                titleColor:cc.color(229,100,78,255),
            },
        ]; 
        this.onRefreshSideBox();
    },
    //刷新卖量列表
    onRefreshSideBox(){
        wx.ylSideBox(function (data) {
            if (data && data.length > 0) {
                this.sideBoxList = data;
                this._loadGameIcon(0);
                this.initData(data);
            }                
        }.bind(this));
    },
    // 预载侧边盒子内游戏ICON
    _loadGameIcon(index) {
        if (index < this.sideBoxList.length) {
            let data = this.sideBoxList[index];
            if (data.icon && data.icon !== '') {
                cc.loader.load(data.icon, (err, texture) => {
                    if (!err) {
                        this.sideBoxList[index].texture = texture;
                        let evt = new cc.Event.EventCustom(window.Global.EVENT_LOAD_BOX_ICON + data._id, true);
                        evt.setUserData(texture);
                        cc.director.dispatchEvent(evt);
                    }
                    index++;
                    this._loadGameIcon(index);
                });
            } else {
                index++;
                this._loadGameIcon(index);
            }
        }
    },
    initData(data){     
        let count = data.length;
        if(count == 0) return;
        this.nodeContent.removeAllChildren();

        let pbSpTitle_1 = cc.instantiate(this.pbSpTitle);
        pbSpTitle_1.getComponent('ylBox2TitleItem').setItemConf(this.titleInfos[0]);
        pbSpTitle_1.parent= this.nodeContent;
        if(!this.nodeGrid_1)
            this.nodeGrid_1 = cc.instantiate(this.pbGrid);
        this.nodeGrid_1.parent= this.nodeContent;        
        this.nodeGrid_1.getComponent('ylBox2GridContent').initData(data,"grid_1",1);

        let pbSpTitle_2 = cc.instantiate(this.pbSpTitle);
        pbSpTitle_2.getComponent('ylBox2TitleItem').setItemConf(this.titleInfos[1]);
        pbSpTitle_2.parent= this.nodeContent;
        if(!this.nodeGrid_2)
            this.nodeGrid_2 = cc.instantiate(this.pbGrid);
        this.nodeGrid_2.parent= this.nodeContent;        
        this.nodeGrid_2.getComponent('ylBox2GridContent').initData(data,"grid_2",2);

        let nodePGV = cc.instantiate(this.pbPageView);
        nodePGV.getComponent('ylBox2PageView').setData(data,'pageview');
        nodePGV.parent= this.nodeContent;

        let pbSpTitle_3 = cc.instantiate(this.pbSpTitle);
        pbSpTitle_3.getComponent('ylBox2TitleItem').setItemConf(this.titleInfos[2]);
        pbSpTitle_3.parent= this.nodeContent;
        if(!this.nodeScroll)
            this.nodeScroll = cc.instantiate(this.pbScroll);
        this.nodeScroll.parent= this.nodeContent;
        this.nodeScroll.getComponent('ylBox2ScrollContent').initData(data,"scroll_3");
    },
    showView(){
        this.node.active = true;
    },
    onClickHome(){
        this.node.active = false;
    },
});
