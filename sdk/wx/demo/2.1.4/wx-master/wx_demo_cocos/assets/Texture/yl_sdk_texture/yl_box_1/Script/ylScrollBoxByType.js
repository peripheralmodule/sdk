
cc.Class({
    extends: cc.Component,
    properties: {
        sp_type:cc.Sprite,
        lb_type:cc.Label,
        lay_scroll:cc.Node,
        pfb_item:cc.Prefab,
    },
    initData(startIndex,count,datas,scene_name) {
        if (datas) {
            // let count = datas.length;
            //生成随机光动效
            let total = count > 2 ? Math.floor(count/2) : count;
            let showLightId = window.Global.getRandomList(total,count);
            for(let i=startIndex;i<count;i++){
                let box = cc.instantiate(this.pfb_item);
                box.parent = this.lay_scroll;
                let random = Math.floor(Math.random()*10);
                let isNew = (random == 1 || random == 3 || random == 5 || random == 7 || random == 9);
                let playerNum = Math.floor(Math.random()*1000000);
                box.getComponent('ylBox1ScrollItem').setItemConf(datas[i],playerNum,scene_name);
                if(showLightId.length > 0){
                    for(let j=0;j<showLightId.length;j++){
                        if(showLightId[j] == i){
                            box.getComponent('ylBox1ScrollItem').startAnimLight();
                            break;
                        }
                    }
                }
            }
        }
    },

    // update (dt) {},
});
