
cc.Class({
    extends: cc.Component,
    properties: {
        appIcon:cc.Sprite,
        nodeFriendPlay:cc.Node,
        lbFriendPlay:cc.Label,
        pbFriendPlay:cc.Prefab,
        sfHeads:[cc.SpriteFrame],
    },

    setInfo(obj,scene) {
        this._obj = obj;
        this._scene = scene;
        let name = window.Global.handleNameLen(this._obj.title,4);        
        if (this._obj.icon && this._obj.icon !== '') {
            if (this._obj.texture) {
                this.setTex(this._obj.texture);
            } else {
                cc.director.on(window.Global.EVENT_LOAD_FULL_GRID_BOX_ICON + this._obj._id, (evt) => {
                    this.setTex(evt.getUserData());
                }, this); 
            }
        }else {
           // console.log('url是空的：this._obj.icon = ' + this._obj.icon);
        }
        this.addFriendsPlay();
    },

    onJumpOut(){
        wx.ylNavigateToMiniProgram(
            {
                _id: this._obj._id,
                toAppid:this._obj.toAppid,   
                toUrl:this._obj.toUrl,
                type:this._obj.type,
                showImage:this._obj.showImage,
                source:'item_full_grid',//从哪个模块导出的，该字段具体值由调用方自行定义
            },
            function(success){
                if(success){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_REFRESH_SIDEBOX);
                    cc.director.dispatchEvent(evt);
                }else{
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_FULL_BOX);
                    cc.director.dispatchEvent(evt);
                }
        }.bind(this));
    },
    setTex(tex) {
        if (!cc.isValid(this.node)) return;
        let size = this.appIcon.node.getContentSize();
        this.appIcon.spriteFrame = new cc.SpriteFrame(tex);
        this.appIcon.node.setContentSize(size);
    },
    addFriendsPlay(){
        //此处的好友在玩的数量只是一个假的随机值
        let friendNum = Math.floor(Math.random()*9);
        this.lbFriendPlay.string = friendNum+"个好友在玩";
        if(friendNum == 0) return;
        let count = friendNum > 3 ? 3 : friendNum;
        let left_x = -179;
        let left_space = 81;
        let h_length = this.sfHeads.length;
        this.friendIconIds = [];
        for(let i = 0;i < count;i++){
            var friendPlay = cc.instantiate(this.pbFriendPlay);
            let sp = friendPlay.getChildByName('sp_icon');
            let h_id = this.randomFriendIconId(h_length);
            sp.getComponent(cc.Sprite).spriteFrame = this.sfHeads[h_id];
            friendPlay.parent = this.nodeFriendPlay;
            friendPlay.y = 0;
            friendPlay.x = left_x +left_space*i;
        }
    },
    randomFriendIconId(h_length){
        let h_id = Math.floor(Math.random()*h_length);
        let has_id = false;
        for(let i=0;i<this.friendIconIds.length;i++){
            if(h_id == this.friendIconIds[i]){
                has_id = true;
                break;
            }
        }
                
        if(has_id){
            return this.randomFriendIconId(h_length);
        }else{
            this.friendIconIds.push(h_id);
            return h_id;
        }
    },
});
