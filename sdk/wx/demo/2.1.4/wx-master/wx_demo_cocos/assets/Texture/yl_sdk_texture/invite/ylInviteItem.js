/**
 * 积分墙Item
 **/
cc.Class({
    extends: cc.Component,

    properties: {
        appIcon: cc.Sprite,
        lbTitle: cc.Label,
        spAward:cc.Sprite,
        lbAwardNum: cc.Label,
        lbBtnTex:cc.Label,
        btnItem:cc.Button,
        awardSF:[cc.SpriteFrame],//[0:金币,1:钻石,2:体力,3:券]虚拟币请根据自己的实际需求配置
    },

    updateCell (obj) {
        this._obj = obj;        
        let name = window.Global.handleNameLen(this._obj.title,5);
        this.lbTitle.string = name;
        if (this._obj.icon && this._obj.icon !== '') {
            if (this._obj.texture) {
                this.setTex(this._obj.texture);
            } else {
                cc.director.on(window.Global.EVENT_LOAD_BOARD_ICON + this._obj._id, (evt) => {
                    this.setTex(evt.getUserData());
                }, this); 
            }
        }else {
           // console.log('url是空的：this._obj.icon = ' + this._obj.icon);
        }
        //此处可自己根据自己的情况自行配置
        if(this._obj.awardList && this._obj.awardList.length >0){
            let award_1 = this._obj.awardList[0]
            if(award_1.value) this.lbAwardNum.string = award_1.value
            if(award_1.type){
                let tex = this.awardSF[this.getIcon(award_1.type)];
                let size = this.spAward.node.getContentSize();
                this.spAward.spriteFrame = tex;
                this.spAward.node.setContentSize(size);
            } 
        }
        this.refreshBtnStatus();
    },

    clickTryPlay(){
        if(this._obj.awardStatus == 0) return;
        //跳转小游戏
       let j_data =  {
                _id: this._obj._id,
                toAppid:this._obj.toAppid,   
                toUrl:this._obj.toUrl,
                type:this._obj.type,
                showImage:this._obj.showImage,
                source:'item_board',//从哪个模块导出的，该字段具体值由调用方自行定义
            };
        wx.ylNavigateToMiniProgram(j_data,
            function(success){
                if(success){
                    //领取积分墙奖励
                    wx.ylGetBoardAward(this._obj._id,function(data){
                        if(data){
                            let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_GET_AWARD);
                            evt.setUserData(data);
                            cc.director.dispatchEvent(evt);
                            this._obj.awardStatus = 0;
                            this.refreshBtnStatus();
                            //领取后，后面的逻辑请根据自己的情况处理
                        }
                    }.bind(this));
                }else{
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_FULL_BOX);
                    cc.director.dispatchEvent(evt);
                }
        }.bind(this));
    },
    refreshBtnStatus(){
        this.lbBtnTex.string = (this._obj.awardStatus == 1)?"试玩领取":"已经领取";
    },
    setTex(tex) {
        if (!cc.isValid(this.node)) return;
        let size = this.appIcon.node.getContentSize();
        this.appIcon.spriteFrame = new cc.SpriteFrame(tex);
        this.appIcon.node.setContentSize(size);
    },
    getIcon(type){        
        if(type == "gold"){
            return 0;
        }else if(type == "diamond"){
            return 1;
        }else if(type == "spirit"){
            return 2;
        }else{//(award.type == "coupon"){
            return 3;
        } 
    },
});
