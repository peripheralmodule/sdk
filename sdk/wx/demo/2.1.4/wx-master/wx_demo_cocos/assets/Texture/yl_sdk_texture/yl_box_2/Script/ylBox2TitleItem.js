
cc.Class({
    extends: cc.Component,
    properties: {
        spIcon:cc.Sprite,
        lbName:cc.Label,
    },
    setItemConf(info){
        let size = this.spIcon.node.getContentSize();
        this.spIcon.spriteFrame = info.spriteFrame;
        this.spIcon.node.setContentSize(size);
        this.lbName.string = info.title;
        this.lbName.node.color = info.titleColor;
    },
});
