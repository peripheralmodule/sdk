import { ui } from "./../ui/layaMaxUI";
/**
 * 本示例采用非脚本的方式实现，而使用继承页面基类，实现页面逻辑。在IDE里面设置场景的Runtime属性即可和场景进行关联
 * 相比脚本方式，继承式页面类，可以直接使用页面定义的属性（通过IDE内var属性定义），比如this.tipLbll，this.scoreLbl，具有代码提示效果
 * 建议：如果是页面级的逻辑，需要频繁访问页面内多个元素，使用继承式写法，如果是独立小模块，功能单一，建议用脚本方式实现，比如子弹脚本。
 */
export default class GameUI extends ui.test.TestSceneUI {
    /**设置单例的引用方式，方便其他类引用 */
    static instance: GameUI;
    private shareCount:number = 0;
    constructor() {
        super();
        GameUI.instance = this;
        //关闭多点触控，否则就无敌了
        Laya.MouseManager.multiTouchEnabled = false;
    }

    onEnable(): void {
        wx.ylSetCurScene("MainScene");//一定记得在每个场景都要设置当前场景
        wx.ylOnPowerChange(function(type){
            console.warn("HelloWorld-ylOnPowerChange back[1:视频,2:定时自动恢复]:",type);
        }.bind(this));
        wx.ylInitSDK(function(success:any){
            console.log("-----初始化SDK-----:",success);
            if(success === true){
                console.warn("HelloWorld-ylInitSDK-初始化成功");
                this.doSDKinterface();
                this.refreshBoardAndBoxBtnVisible();

                // let videoShare = wx.ylGetVideoShareLimit();
                // let moduleList = wx.ylGetModuleList();
                // console.log("HelloWorld-ylGetVideoShareLimit:",videoShare);
                // console.log("HelloWorld-ylGetModuleList:",moduleList);
            }else if(success == 'config_success' || success == 'config_fail'){
                console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
                wx.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("HelloWorld-ylGetPowerInfo:",data);
                });
            }else{
                console.warn("HelloWorld-ylInitSDK-初始化失败");
            }
        }.bind(this));
        
        // this.btnSideBox.on(Laya.Event.CLICK,this,this.onShowSideBox);
        // this.list();
        this.initEvent();
    }
    initEvent():void{
        this.btnShare.on(Laya.Event.CLICK,this,this.onShareClick);
        this.btnShowShareOrVideo.on(Laya.Event.CLICK,this,this.onShowShareOrVideo);
    }
    doSDKinterface():void{
        console.log("---调用接口---");
        wx.ylSideBox(function (data:any) {
        }.bind(this));
        wx.ylGetCustom(function(data:any){
        }.bind(this));
        wx.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status:any){
            if(status){
                //干点什么
            }
        }.bind(this));

        let userWXinfo = wx.ylGetUserWXInfo();//先获取本地缓存，没有在弹窗通过用户授权获取
        if(!userWXinfo){
             //是否收集用户信息按钮信息(具体配置请参考微信小游戏官方文档)
            wx.ylUserInfoButtonShow(
                {       
                  type: 'image',
                  image:'./images/sp_btn_start.png',  //图片地址
                  style: {
                    left: 100,
                    top: 360,
                    width: 150,
                    height: 40,
                    lineHeight: 40,
                    borderRadius: 4
                  }
                },
                function(data:any){
                    if(data){
                        userWXinfo = data;
                        wx.ylUserInfoButtonHide();
                    }
            }.bind(this));
        }
        let loginInfo = wx.ylGetUserInfo();//获取登录信息，包括微信oppid和code(code只有第一次登录才能取到)
        console.log("---登录信息-loginInfo:",JSON.stringify(loginInfo));
        // wx.ylBannerAdCreateSmall(true);

        var that = this;
        /**
         * 获取分享图列表
         */
        wx.ylShareCard(function (shareInfo:any) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
            }else{
                //获取失败
            }
        }.bind(this),null);
        wx.ylStatisticViedo(0,'adunit-6878a73e134f85e2',null);
        wx.ylStatisticShareCard(177);
        this.testStoreValue();
    }

    onShareClick(e:Laya.Event):void{
        console.error("-----分享点击-----");
        this.shareCount += 1;
        let _channel = (this.shareCount %2 ==0) ? 'bb' : 'cc';
        let _module = (this.shareCount %2 ==0) ? 'aa' : 'cc';
        let shareInfo = {
            channel:_channel,
            module:_module,
            scene:'MenuScene',
            inviteType:'award_id',
        };
        wx.ylShareAppMessage(function(sResult){
            if(sResult.sSuccess){
                let result = wx.ylRewardByVideoOrShare(false);
                let slLimit = wx.ylGetVSLimit();
                console.log("---------ylRewardByVideoOrShare-result:",result,slLimit);
            }
            console.log("---------分享结果-sResult:",sResult);
        },shareInfo);

    }
    //视频分享策略
    onShowShareOrVideo():void{
        wx.ylShowShareOrVideo("bb","aa",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    let _showTime = new Date().getTime()-3000;
                    console.warn("------策略-分享-_showTime:",_showTime);
                    let _channel ='cc';
                    let _module = 'cc';
                    let onShare = {
                        channel:_channel,
                        module:_module,
                        showTime: _showTime,
                    };
                    wx.ylGetSharingResults(onShare,function(result){
                        console.warn("-----ylGetSharingResults-result：",result);
                        if(result.sSuccess){
                            //分享成功
                        }else{
                            //分享失败
                            if(result.hasStrategy){
                                console.warn("-----分享失败话术：",result.trickJson);
                            }
                        }
                    });
                    break;
                case 2:
                    let result = wx.ylRewardByVideoOrShare(true);
                    console.warn("------策略-视频-result:",result);
                    break;
            }
        }.bind(this));
    }
    //测试自定义空间值
    testStoreValue():void{
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    }
    test_sv_string():void{
        //String
        wx.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                wx.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    }
    test_sv_list():void{
        //List
        wx.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        wx.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        wx.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                wx.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 wx.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        wx.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        wx.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    wx.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    wx.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            wx.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    }
    test_sv_set():void{
        //Set
        wx.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        wx.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                wx.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                wx.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        wx.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                wx.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        wx.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            wx.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    }
    test_sv_hash():void{
        //litMap
        wx.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                wx.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        wx.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            wx.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            wx.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            wx.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            wx.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                wx.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    }
    test_sv_radom():void{
        //testRandom
        wx.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    }
}