(function () {
    'use strict';

    var REG = Laya.ClassUtils.regClass;
    var ui;
    (function (ui) {
        var test;
        (function (test) {
            class TestSceneUI extends Laya.Scene {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.loadScene("test/TestScene");
                }
            }
            test.TestSceneUI = TestSceneUI;
            REG("ui.test.TestSceneUI", TestSceneUI);
        })(test = ui.test || (ui.test = {}));
    })(ui || (ui = {}));

    class GameUI extends ui.test.TestSceneUI {
        constructor() {
            super();
            this.shareCount = 0;
            GameUI.instance = this;
            Laya.MouseManager.multiTouchEnabled = false;
        }
        onEnable() {
            wx.ylSetCurScene("MainScene");
            wx.ylOnPowerChange(function (type) {
                console.warn("HelloWorld-ylOnPowerChange back[1:视频,2:定时自动恢复]:", type);
            }.bind(this));
            wx.ylInitSDK(function (success) {
                console.log("-----初始化SDK-----:", success);
                if (success === true) {
                    console.warn("HelloWorld-ylInitSDK-初始化成功");
                    this.doSDKinterface();
                    this.refreshBoardAndBoxBtnVisible();
                }
                else if (success == 'config_success' || success == 'config_fail') {
                    console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
                    wx.ylGetPowerInfo(function (data) {
                        console.log("HelloWorld-ylGetPowerInfo:", data);
                    });
                }
                else {
                    console.warn("HelloWorld-ylInitSDK-初始化失败");
                }
            }.bind(this));
            this.initEvent();
        }
        initEvent() {
            this.btnShare.on(Laya.Event.CLICK, this, this.onShareClick);
            this.btnShowShareOrVideo.on(Laya.Event.CLICK, this, this.onShowShareOrVideo);
        }
        doSDKinterface() {
            console.log("---调用接口---");
            wx.ylSideBox(function (data) {
            }.bind(this));
            wx.ylGetCustom(function (data) {
            }.bind(this));
            wx.ylStatisticResult({ "total_score": 123, "rebirth_score": 123 }, function (status) {
                if (status) {
                }
            }.bind(this));
            let userWXinfo = wx.ylGetUserWXInfo();
            if (!userWXinfo) {
                wx.ylUserInfoButtonShow({
                    type: 'image',
                    image: './images/sp_btn_start.png',
                    style: {
                        left: 100,
                        top: 360,
                        width: 150,
                        height: 40,
                        lineHeight: 40,
                        borderRadius: 4
                    }
                }, function (data) {
                    if (data) {
                        userWXinfo = data;
                        wx.ylUserInfoButtonHide();
                    }
                }.bind(this));
            }
            let loginInfo = wx.ylGetUserInfo();
            console.log("---登录信息-loginInfo:", JSON.stringify(loginInfo));
            var that = this;
            wx.ylShareCard(function (shareInfo) {
                if (shareInfo) {
                    console.log("----获取分享图列表:", JSON.stringify(shareInfo));
                }
                else {
                }
            }.bind(this), null);
            wx.ylStatisticViedo(0, 'adunit-6878a73e134f85e2', null);
            wx.ylStatisticShareCard(177);
            this.testStoreValue();
        }
        onShareClick(e) {
            console.error("-----分享点击-----");
            this.shareCount += 1;
            let _channel = (this.shareCount % 2 == 0) ? 'bb' : 'cc';
            let _module = (this.shareCount % 2 == 0) ? 'aa' : 'cc';
            let shareInfo = {
                channel: _channel,
                module: _module,
                scene: 'MenuScene',
                inviteType: 'award_id',
            };
            wx.ylShareAppMessage(function (sResult) {
                if (sResult.sSuccess) {
                    let result = wx.ylRewardByVideoOrShare(false);
                    let slLimit = wx.ylGetVSLimit();
                    console.log("---------ylRewardByVideoOrShare-result:", result, slLimit);
                }
                console.log("---------分享结果-sResult:", sResult);
            }, shareInfo);
        }
        onShowShareOrVideo() {
            wx.ylShowShareOrVideo("bb", "aa", function (type) {
                switch (type) {
                    case 0:
                        console.warn("------策略-无");
                        break;
                    case 1:
                        let _showTime = new Date().getTime() - 3000;
                        console.warn("------策略-分享-_showTime:", _showTime);
                        let _channel = 'cc';
                        let _module = 'cc';
                        let onShare = {
                            channel: _channel,
                            module: _module,
                            showTime: _showTime,
                        };
                        wx.ylGetSharingResults(onShare, function (result) {
                            console.warn("-----ylGetSharingResults-result：", result);
                            if (result.sSuccess) {
                            }
                            else {
                                if (result.hasStrategy) {
                                    console.warn("-----分享失败话术：", result.trickJson);
                                }
                            }
                        });
                        break;
                    case 2:
                        let result = wx.ylRewardByVideoOrShare(true);
                        console.warn("------策略-视频-result:", result);
                        break;
                }
            }.bind(this));
        }
        testStoreValue() {
            this.test_sv_string();
            this.test_sv_list();
            this.test_sv_set();
            this.test_sv_hash();
            this.test_sv_radom();
        }
        test_sv_string() {
            wx.ylStoreValue({
                name: "testString",
                cmd: "set",
                args: "测试数据"
            }, function (status) {
                wx.ylStoreValue({
                    name: "testString",
                    cmd: "get"
                }, function (status) {
                }.bind(this));
            }.bind(this));
        }
        test_sv_list() {
            wx.ylStoreValue({
                name: "testList",
                cmd: "add",
                args: "0"
            }, function (status) {
            }.bind(this));
            wx.ylStoreValue({
                name: "testList",
                cmd: "add",
                args: "2"
            }, function (status) {
            }.bind(this));
            wx.ylStoreValue({
                name: "testList",
                cmd: "set",
                args: "0,3"
            }, function (status) {
                wx.ylStoreValue({
                    name: "testList",
                    cmd: "all"
                }, function (status) {
                }.bind(this));
                wx.ylStoreValue({
                    name: "testList",
                    cmd: "get",
                    args: "0"
                }, function (status) {
                }.bind(this));
                wx.ylStoreValue({
                    name: "testList",
                    cmd: "size"
                }, function (status) {
                }.bind(this));
                wx.ylStoreValue({
                    name: "testList",
                    cmd: "poll",
                    args: "2"
                }, function (status) {
                    wx.ylStoreValue({
                        name: "testList",
                        cmd: "size"
                    }, function (status) {
                    }.bind(this));
                    wx.ylStoreValue({
                        name: "testList",
                        cmd: "replace",
                        args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                    }, function (status) {
                        wx.ylStoreValue({
                            name: "testList",
                            cmd: "all"
                        }, function (status) {
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_set() {
            wx.ylStoreValue({
                name: "testSet",
                cmd: "add",
                args: "12"
            }, function (status) {
            }.bind(this));
            wx.ylStoreValue({
                name: "testSet",
                cmd: "add",
                args: "10"
            }, function (status) {
                wx.ylStoreValue({
                    name: "testSet",
                    cmd: "exist",
                    args: "10"
                }, function (status) {
                }.bind(this));
                wx.ylStoreValue({
                    name: "testSet",
                    cmd: "size"
                }, function (status) {
                    wx.ylStoreValue({
                        name: "testSet",
                        cmd: "del",
                        args: "10"
                    }, function (status) {
                        wx.ylStoreValue({
                            name: "testSet",
                            cmd: "all"
                        }, function (status) {
                            wx.ylStoreValue({
                                name: "testSet",
                                cmd: "replace",
                                args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                            }, function (status) {
                                wx.ylStoreValue({
                                    name: "testSet",
                                    cmd: "all"
                                }, function (status) {
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_hash() {
            wx.ylStoreValue({
                name: "testHash",
                cmd: "set",
                args: "u_name,许"
            }, function (status) {
                wx.ylStoreValue({
                    name: "testHash",
                    cmd: "get",
                    args: "u_name"
                }, function (status) {
                    wx.ylStoreValue({
                        name: "testHash",
                        cmd: "replace",
                        args: "{\"u_name\":\"唐\",\"sex\":\"women\"}"
                    }, function (status) {
                        wx.ylStoreValue({
                            name: "testHash",
                            cmd: "gets",
                            args: "u_name,sex"
                        }, function (status) {
                        }.bind(this));
                        wx.ylStoreValue({
                            name: "testHash",
                            cmd: "size",
                        }, function (status) {
                        }.bind(this));
                        wx.ylStoreValue({
                            name: "testHash",
                            cmd: "values",
                            args: "sex"
                        }, function (status) {
                        }.bind(this));
                        wx.ylStoreValue({
                            name: "testHash",
                            cmd: "del",
                            args: "u_name"
                        }, function (status) {
                            wx.ylStoreValue({
                                name: "testHash",
                                cmd: "all",
                            }, function (status) {
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_radom() {
            wx.ylStoreValue({
                name: "testRandom"
            }, function (status) {
            }.bind(this));
        }
    }

    class GameConfig {
        constructor() {
        }
        static init() {
            var reg = Laya.ClassUtils.regClass;
            reg("script/GameUI.ts", GameUI);
        }
    }
    GameConfig.width = 640;
    GameConfig.height = 1136;
    GameConfig.scaleMode = "fixedwidth";
    GameConfig.screenMode = "none";
    GameConfig.alignV = "top";
    GameConfig.alignH = "left";
    GameConfig.startScene = "test/TestScene.scene";
    GameConfig.sceneRoot = "";
    GameConfig.debug = false;
    GameConfig.stat = false;
    GameConfig.physicsDebug = false;
    GameConfig.exportSceneToJson = true;
    GameConfig.init();

    class Main {
        constructor() {
            if (window["Laya3D"])
                Laya3D.init(GameConfig.width, GameConfig.height);
            else
                Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
            Laya["Physics"] && Laya["Physics"].enable();
            Laya["DebugPanel"] && Laya["DebugPanel"].enable();
            Laya.stage.scaleMode = GameConfig.scaleMode;
            Laya.stage.screenMode = GameConfig.screenMode;
            Laya.stage.alignV = GameConfig.alignV;
            Laya.stage.alignH = GameConfig.alignH;
            Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
            if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
                Laya.enableDebugPanel();
            if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
                Laya["PhysicsDebugDraw"].enable();
            if (GameConfig.stat)
                Laya.Stat.show();
            Laya.alertGlobalError = true;
            Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
        }
        onVersionLoaded() {
            Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
        }
        onConfigLoaded() {
            GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
        }
    }
    new Main();

}());
