
enum EM_RECORD_TYPE {
    None = 0,
    Recording = 1,
    Pasuse = 2,
}
/**
 * 字节跳动平台独有接口
 */
export class PlatformTT{
    private _status:number = 0;

    constructor() {
        this._status = 0;
    }

    /**
     * 开始录屏
     */
    onRecordStart():void{
        console.log("-------------onRecordStart--------this._status:",this._status);
        //duration:录屏的时长，单位 s，必须 >3 &&  <= 300s（5 分钟）
        let duration = 200;
        ydhw.RecorderStart(10,
            (result)=>{
                this._status = EM_RECORD_TYPE.Recording;
                console.log("PlatformTT -RecorderStart-onStart:",JSON.stringify(result),this._status);
            },
            (videoPath:string)=>{
                console.log("PlatformTT -RecorderStart-onStop:",videoPath);
            },
            (result:any)=>{
                this._status = EM_RECORD_TYPE.Recording;
                console.log("PlatformTT -RecorderResume:",result,this._status);
            },
            (result:any)=>{
                this._status = EM_RECORD_TYPE.Pasuse;
                console.log("PlatformTT -RecorderPause:",result,this._status);
            },
            (result:any)=>{
                console.log("PlatformTT -RecorderStart-onError:",JSON.stringify(result));
            }
        );
    }
    /**
     * 停止录屏
     */
    onRecordStop():void {
        console.log("PlatformTT -TT- 停止录屏");
        ydhw.RecorderStop();
    }
    /**
     * 继续录屏
     */
    onRecordResume():void{
        ydhw.RecorderResume();
    }

    /**
     * 暂停录屏
     */
    onRecordPasue():void{
        ydhw.RecorderPause();
    }

    /**
     * 获取当前录屏状态
     */
    getRecordStatus():IYDHW.TT.EM_RECORD_TYPE {
        return this._status;
    }

    /**
     *图片分享 (含模板)
     */
    onShareImage():void{
        ydhw.ShareImage('MainScene',()=>{

        },"测试文案");
    }

    /**
     * 视频分享(含模板)
     */
    onShareVideo():void{
        ydhw.ShareVideo('视频分享','描述测试','',(isOk) =>{
            console.log("PlatformTT -ShareVideo-isOk:",JSON.stringify(isOk));
        });
    }

    /**
     * Token分享(含模板)
     */
    onShareToken():void{
        ydhw.ShareToken((isOk) =>{
            console.log("PlatformTT -ShareToken-isOk:",JSON.stringify(isOk));
        });
    }

    /**
     * 模板分享
     */
    onShareTemplate():void{
        ydhw.ShareTemplate((isOk) =>{
            console.log("PlatformTT -ShareTemplate-isOk:",JSON.stringify(isOk));
        });
    }

    /**
     * 显示更多游戏
     */
    showMoreGame():void{
        ydhw.ShowMoreGamesModal((isOk) =>{
            console.log("PlatformTT -ShowMoreGamesModal-isOk:",JSON.stringify(isOk));
        },{
            //如下数据应该从侧边栏卖量列表获取
            iconId:1607290,
            souce:"MainScene",
            target:"ttfe8b883661a11109"
        });
        // ydhw.CreateMoreGamesButton("text","",{
        //     left: 20,
        //     top: 40,
        //     width: 150,
        //     height: 40,
        //     lineHeight: 40,
        //     backgroundColor: "#ff0000",
        //     textColor: "#ffffff",
        //     textAlign: "center",
        //     fontSize: 16,
        //     borderRadius: 4,
        //     borderWidth: 1,
        //     borderColor: "#ff0000",
        // },this,(isOk:boolean)=>{
        //     console.log("YDHW ---CreateMoreGamesButton-onMoreGame:"+isOk);
        // },()=>{
        //     console.log("YDHW ---CreateMoreGamesButton-onTip");
        // },{
        //     //如下数据应该从侧边栏卖量列表获取
        //     iconId:1607290,
        //     souce:"MainScene",
        //     target:"ttfe8b883661a11109"
        // });
    }

    /**
     * 获取胶囊按钮左侧位置
     */
    GetLeftTopBtnPosition():void{
        // let topBtnPosition = ydhw.GetLeftTopBtnPosition();
        // console.log("PlatformTT -GetLeftTopBtnPosition:",topBtnPosition);
    }
    /**
     * 获取平台用户信息
     */
    GetPlatformUserInfo():void{
        // ydhw.GetPlatformUserInfo(this, (userInfo) => {
        //     console.log("PlatformTT -获取平台用户信息:",JSON.stringify(userInfo));
        // });
    }



}