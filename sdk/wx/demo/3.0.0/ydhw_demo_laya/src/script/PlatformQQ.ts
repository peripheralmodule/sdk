import { YDHW } from "./../../libs/ydhw/ydhw.sdk"; 


export enum EM_APP_BOX_TYPE {
    /**
     * 盒子广告-创建成功
     */
    APPBOX_CREATE_SUCCESS = 0,   
    /**
     * 盒子广告-创建失败
     */
    APPBOX_CREATE_FAIL = 1,      
    /**
     * 盒子广告-关闭
     */
    APPBOX_CLOSE = 2,            
}

export class PlatformQQ{

    constructor() {
    }

    /**
     * 分享(邀请类模板)
     */
    ShareByTemplateId():void{
        let info = new YDHW.QQ.ShareTempletInfo();
        info.shareTemplateId = "EE558DDCEFB407FD811CC6C06181D6AF";
        info.shareTemplateData = { txt1: "老铁就等你了，快上车!", txt2: "应邀前往" };
        ydhw.ShareByTemplateId(info,this,
            (result: IYDHW.QQ.ITempShareResult) => {
                console.log("PlatformQQ -----ShareByTemplateId:", result);
            }
        );
    }

    /**
     * 添加彩签
     */
    AddColorSign():void{
        ydhw.AddColorSign(this, (success: boolean) => {
            console.log("PlatformQQ -----AddColorSign:", success);
        });
    }
    
    /**
     * 主动订阅
     */
    SubscribeAppMsg():void{
        let appMsgInfo = new YDHW.QQ.AppMsgInfo();
        appMsgInfo.subscribe = true;
        //注意：长期订阅不要传tmplIds参数，一次性订阅要传(请到QQ后台核对你的订阅消息类型)
        appMsgInfo.tmplIds = ["4dec1376c90128640a36879636ec6320"];
        ydhw.SubscribeAppMsg(appMsgInfo, this, (success: boolean) => {
            console.log("PlatformQQ -----SubscribeAppMsg:", success);
        });
    }
    
    /**
     * 创建-打开添加好友页面的按钮
     */
    CreateAddFriendButton():void{
        let fBtnInfo = new YDHW.QQ.AddFriendButtonInfo();
        fBtnInfo = {
            type: 'text',
            text: '添加好友',
            openId: "6A43B93B26F2562B65D317398B58C9FE",
            style: {
                left: 10,
                top: 276,
                width: 200,
                height: 40,
                backgroundColor: '#ff0000',
                borderColor: '#ff0000',
                borderWidth: 2,
                borderRadius: 5,
                color: '#ffffff',
                textAlign: 'center',
                fontSize: 16,
                lineHeight: 40,
            }
        };
        ydhw.CreateAddFriendButton(
            fBtnInfo, this,
            (success: boolean) => {
                console.log("PlatformQQ -----CreateAddFriendButton:", success);
                this.ShowAddFriendButton();
            }
        );
    }

    /**
     * 显示-打开添加好友页面的按钮
     */
    ShowAddFriendButton(){
        ydhw.ShowAddFriendButton();
    }

    /**
     * 隐藏-打开添加好友页面的按钮
     */
    HideAddFriendButton(){
        ydhw.HideAddFriendButton();
        this.DestroyAddFriendButton();
    }

    /**
     * 销毁-打开添加好友页面的按钮
     */
    DestroyAddFriendButton(){
        ydhw.DestroyAddFriendButton();
    }
    /**
     * 创建-盒子广告
     */
    CreateAppBox():void{
        ydhw.CreateAppBox(this, (status: IYDHW.QQ.EM_APP_BOX_TYPE) => {
            console.log("PlatformQQ -----CreateAppBox:", status);
            if(status == EM_APP_BOX_TYPE.APPBOX_CREATE_SUCCESS){
                this.ShowAppBox();
            }
        });
    }

    /**
     * 显示-盒子广告
     */
    ShowAppBox(){
        ydhw.ShowAppBox(this, () => {
            console.log("PlatformQQ ----ShowAppBox-onSuccess:");
        }, () => {
            console.log("PlatformQQ ----ShowAppBox-onError:");
        });
        // ydhw.CreateUserInfoButton({       
        //     type: 'image',
        //     image:'./images/sp_btn_start.png',  //图片地址
        //     style: {
        //         left: 100,
        //         top: 66,
        //         width: 150,
        //         height: 40,
        //         lineHeight: 40,
        //         borderRadius: 4
        //     }
        // },
        // this,(result: any):void =>{
        //     console.log("PlatformQQ -CreateUserInfoButton-onSuccess:",result);
        // },
        // (error: any)=>{
        //     console.log("PlatformQQ -CreateUserInfoButton-onError:",error);
        // });
    }

    /**
     * 创建-积木广告
     */
    CreateBlockAd():void{
        let blockInfo = new YDHW.QQ.BlockAdInfo();
        blockInfo = {
            style: {
                left: 50,
                top: 50,
            },
            size: 5,
            orientation: 'landscape'
        }
        ydhw.CreateBlockAd(blockInfo, this, (status: IYDHW.QQ.EM_BLOCK_TYPE) => {
            console.log("PlatformQQ ----CreateBlockAd-status:", status);
            this.ShowBlockAd();
        },(data:any) =>{
            console.log("PlatformQQ ----onResize-data:", JSON.stringify(data));
        });
    }

    /**
     * 显示-积木广告
     */
    ShowBlockAd(){
        ydhw.ShowBlockAd(this, () => {
            console.log("PlatformQQ ----ShowBlockAd");
        });
    }

    /**
     * 隐藏-积木广告
     */
    HideBlockAd(){
        ydhw.HideBlockAd();
        this.DestroyBlockAd();
    }

    /**
     * 销毁-积木广告
     */
    DestroyBlockAd(){
        ydhw.DestroyBlockAd();
    }

    /**
     * 分享
     */
    onShareAppMessage():void{
        ydhw.ShareAppMessage("MainScene","aa","bb","award_id",this,(result: any)=>{
            console.log("PlatformQQ --ShareAppMessage-back",JSON.stringify(result));
            this.getShareResult();
        },YDHW.QQ.EM_SHARE_APP_TYPE.QQ);
    }

    /**
     * 获取假分享策略结果
     */
    getShareResult(){
        let shareResultInfo = new YDHW.ShareAppInfo();
        shareResultInfo = {
            channel: "aa",
            module: "bb",
            showTime: new Date().getTime(),
            shareId: 1
        };
        ydhw.GetSharingResults(shareResultInfo,this,(shareBackInfo: IYDHW.IShareBackInfo):void=>{
            console.log("PlatformQQ -GetSharingResults:",JSON.stringify(shareBackInfo));
        });
    }
}