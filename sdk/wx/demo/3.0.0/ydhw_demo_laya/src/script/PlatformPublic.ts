import { YDHW } from "../../libs/ydhw/ydhw.sdk";

/**
 * 各平台公用接口
 */
export class PlatformPublic {

    private layerList: IYDHW.GameBase.ILayerInfo[];
    private currentLayerPath:string;    //当前流失路径(其实可以看做是当前关卡)
    private sLayerIndex:number = 0;
    private custom:number = 1; //关卡
    private lastTime:number = 0;//最后一次展示广告时间戳
    private sideBoxList:IYDHW.GameBase.ISideBoxResult[];
    constructor() {

    }
    /**
     * 初始化接口
     */
    init(){

        ydhw.OnHide(this,function(res){
            console.log("PlatformPublic -OnHide:"+JSON.stringify(res));
        });

        ydhw.OnShow(this,function(res){
            console.log("PlatformPublic -OnShow:"+JSON.stringify(res));
        });

        ydhw.OnError(this,function(res){
            console.log("PlatformPublic -OnError:"+JSON.stringify(res));
        });

        this.lastTime = new Date().getTime();//初始化时间
        ydhw.ListenOnPowerChanged(this,(powerInfo: IYDHW.GameBase.IPowerInfo) => {
            console.log("PlatformPublic -监听体力变化:",JSON.stringify(powerInfo));
        });

        /**
         * 记得先调用登录接口，很多接口都需要再登录成功后才能访问服务器
         */
        ydhw.Login(this, (isOk: boolean)=>{
            if(isOk){
                console.log("PlatformPublic -Login-isOk: " + isOk);
                ydhw.GetCustomConfig(this,(result: IYDHW.GameBase.ICustomConfigResult[]) => {
                    console.log("PlatformPublic -GetCustomConfig:",JSON.stringify(result));
                });
            
                ydhw.GetSideBox(this,(result: IYDHW.GameBase.ISideBoxResult[]) => {
                    this.sideBoxList = result;
                    console.log("PlatformPublic -GetSideBox:",JSON.stringify(result)); 
                });
    
                ydhw.ShareCard("",this,(result: IYDHW.GameBase.IShareCardResult[]) => {
                    console.log("PlatformPublic -ShareCard:",JSON.stringify(result));
                });
                let me = this;
                ydhw.GetLayerList((layerList: IYDHW.GameBase.ILayerInfo[])=> {
                    console.log("PlatformPublic -GetLayerList:",JSON.stringify(layerList));
                    me.layerList = layerList;
                    me.startGame();
                });
            
                ydhw.GetScoreBoardList(this,(result: IYDHW.GameBase.IScoreBoardResult[]) => {
                    console.log("PlatformPublic -GetScoreBoardList:",JSON.stringify(result)); 
                    ydhw.GetScoreBoardAward(54803,this,(result: IYDHW.GameBase.IGetBoardAwardResult) => {
                        console.log("PlatformPublic -GetScoreBoardAward:",JSON.stringify(result)); 
                    });
                });
    
                let address = ydhw.LoginAddress();
                let shareInfo = ydhw.ShareInfo();
                console.log("PlatformPublic -LoginAddress:",address,",ShareInfo:",JSON.stringify(shareInfo));                
    
                ydhw.GetServerInfo(this,(result) =>{
                    console.log("PlatformPublic -GetServerInfo-result:",JSON.stringify(result));
                });
                
                this.initGameInfo();
                this.testStatisticInterface();
                this.testVideoRewardLimitInfo();
                this.testCustomOrBout();
                this.startGame();
                this.testStoreValue();
            }
        });
    }

    /**
     * 获取开关信息
     */
    initGameInfo():void{
        let SwitchTouch = ydhw.SwitchTouch;
        let SwitchPush = ydhw.SwitchPush;
        let IsRealVersion = ydhw.IsRealVersion;
        let AppId = ydhw.AppId;
        let PkgName = ydhw.PkgName;
        let Version = ydhw.Version;
        let AccountId = ydhw.AccountId;
        let NickName = ydhw.NickName;
        let AvatarUrl = ydhw.AvatarUrl;
        let Code = ydhw.Code;
        let OpenID = ydhw.OpenID;
        let AccountPass = ydhw.AccountPass;
        let IsNewPlayer = ydhw.IsNewPlayer;
        let InviteAccount = ydhw.InviteAccount;
        let SceneId = ydhw.SceneId;

        console.log("PlatformPublic -标识后台返回的游戏版本号是否是真实的游戏版本号：",IsRealVersion);
        console.log("PlatformPublic -游戏AppId：",AppId);
        console.log("PlatformPublic -游戏包名：",PkgName);
        console.log("PlatformPublic -游戏版本：",Version);
        console.log("PlatformPublic -玩家账号：",AccountId);
        console.log("PlatformPublic -玩家昵称(需要平台授权)：",NickName);
        console.log("PlatformPublic -玩家头像(需要平台授权)：",AvatarUrl);
        console.log("PlatformPublic -平台登录的Code：",Code);
        console.log("PlatformPublic -平台登录OpenID：",OpenID);
        console.log("PlatformPublic -玩家登录凭证：",AccountPass);
        console.log("PlatformPublic -是否新用户：",IsNewPlayer);
        console.log("PlatformPublic -邀请人账号(从分享进入的时候才有)：",InviteAccount);
        console.log("PlatformPublic -SceneId：",SceneId);

        console.log("PlatformPublic -误触开关：",SwitchTouch);
        console.log("PlatformPublic -推送开关：",SwitchPush);

        let BannerAdUnitIdList = ydhw.BannerAdUnitIdList;
        console.log("PlatformPublic -BannerAdUnitIdList："+JSON.stringify(BannerAdUnitIdList));
    }

    /**
     * 接口测试-统计相关接口
     */
    testStatisticInterface(){
        ydhw.StatisticBanner(YDHW.EM_STATISTIC_TYPE.SHOW,"15926jnded313m55ms");
        
        ydhw.StatisticVideo(YDHW.EM_STATISTIC_TYPE.CREATE,'6sn234aoqf3737fmu9');
    
        ydhw.StatisticEvent("event","scene");

        let clickOutInfo = new YDHW.ClickOutRequest();
            clickOutInfo ={
                iconId: 1,
                souce: "导出模块",
                target: "导出游戏的appid",
                action: "enable"
            };
        ydhw.StatisticClickOut(clickOutInfo);

        let _shareInfo = new YDHW.StatistiicShareInfo(2,0);
        ydhw.StatisticShareCard(_shareInfo);
    }

    /**
     * 接口测试-视频/分享上限
     */
    testVideoRewardLimitInfo():void{
        let isCanRewardByVideoOrShare = ydhw.IsCanRewardByVideoOrShare(1);//[1:分享,2:视频]
        let ShareRewardLimit = ydhw.GetShareRewardLimit();
        let getVideoRewardLimit = ydhw.GetVideoRewardLimit();  
        
        console.log("PlatformPublic -IsCanRewardByVideoOrShare:",isCanRewardByVideoOrShare);
        console.log("PlatformPublic -GetShareRewardLimit:",ShareRewardLimit);
        console.log("PlatformPublic -GetVideoRewardLimit:",getVideoRewardLimit);
    }

    /**
     * 局数和关卡相关接口
     */
    testCustomOrBout():void{
        let boutCount = ydhw.GetTodayBoutCount();
        let totalBountCount = ydhw.GetTotalBoutCount();
        let lastBoutCount = ydhw.GetLastBountNumber();
        let maxBoutCount = ydhw.GetMaxBountNumber();
        let todayWatchVideoCount = ydhw.GetTodayWatchVideoCounter();
        console.log("PlatformPublic -GetTodayBoutCount:",boutCount);
        console.log("PlatformPublic -GetTotalBoutCount:",totalBountCount);
        console.log("PlatformPublic -GetLastBountNumber:",lastBoutCount);
        console.log("PlatformPublic -GetMaxBountNumber:",maxBoutCount);
        console.log("PlatformPublic -GetTodayWatchVideoCounter:",todayWatchVideoCount);
    }


    /**
     * 开始游戏
     */
    startGame():void{
        if(this.layerList && this.layerList.length >0){
            this.currentLayerPath = this.layerList[this.sLayerIndex].layerPath;
            console.log("PlatformPublic -StatisticLayer:",this.currentLayerPath,this.sLayerIndex);
            ydhw.StatisticLayer(this.currentLayerPath);
        }
    }
    
    /**
     * 游戏结束
     */
    onOver():void{ 
        this.startGame();
        ydhw.GameOver(this.custom);
        this.custom +=1;
        //结果统计
        this.sLayerIndex = ((this.sLayerIndex + 2) >= this.layerList.length) ? 0 : (this.sLayerIndex + 2); 
        let win = (this.sLayerIndex % 2 == 0);
        let resultInfo = new YDHW.StatisticResultInfo();
        resultInfo = {  
            source: 80, layerPath: this.currentLayerPath, hasWin: win,
            detail:{
                name: "test_name", type: "GameOver"
            }
        }; 
        ydhw.StatisticResult(resultInfo);
    }

    /**
     * 获取深度误触屏蔽开关
     */
    onDeepTouch():void{
        let deepTouch = ydhw.GetDeepTouchInfo(2);
        console.log("PlatformPublic -GetDeepTouchInfo:",deepTouch);
    }

    /**
     * 创建Banner广告
     */
    onCreateBanner():void{
        /** ************以下是各平台对Banner的特殊要求:*****************************************
         * 
         * 字节跳动：Banner 广告 最小宽度是 128（设备像素），最大宽度是 208（设备像素）
         * 魅族：使用广告api打包时最小平台版本需要填写1064以上
         * QQ:banner 广告组件的宽度（横屏情况下该宽度设置不能超过 1/2 屏幕宽度）
         * VIVO:show与hide最好间隔10秒，一次创建可多次隐藏，一天最多展示100次,两次创建间隔30秒都有失败的。
         * OPPO:可以在onResize监听里调整完了之后再展示Banner,避免有时候创建很久没onResize监听回调，导致有Banner移位现象。
         * 
         * **********************************************************************************/ 

        ydhw.CreateBannerAd(false,true,this,(isOk: boolean)=>{
            console.warn("PlatformPublic ------CreateBannerAd-isOk:",isOk);
            // ydhw.ShowBannerAd();
        });
        // ydhw.CreateSmallBannerAd(true,true,this,(isOk: boolean)=>{
    
        // });
        // ydhw.CreateCustomBannerAd(true,false,
        //     {
        //         top: 0,
        //         left: 0,
        //         width: 208,
        //         height:100
        //     },this,
        //     (isOk:boolean)=>{
        //         console.log("PlatformPublic -CreateCustomBannerAd-isOk:",isOk);
        //     }, 
        //     (result:any)=>{
        //         console.log("PlatformPublic -CreateCustomBannerAd-result:",result);
        //     });

            // ydhw.BannerAdChangeSize({
            //     top: 100,
            //     left: 50,
            //     width: 100,
            //     height:30
            // });
    }

    /**
     * 展示Banner广告
     * 
     */
    onShowBanner():void{
        
        ydhw.ShowBannerAd();//要在创建成功回调之后再调用展示(此处只做演示)        
        console.log("GameUI-onShowBanner---2:");
    }
    /**
     * 切换界面，可以强制刷新Banner
     */
    onSwitchView(){        
        ydhw.SwitchView(true);
    }

    /**
     * 
     */
    onHideBanner(e:Laya.Event):void{
        console.log("GameUI-onHideBanner");
        this.hideBanner();
    }

    /**
     * 改变Banner Style
     */
    onBannerAdChangeSize():void{
        ydhw.BannerAdChangeSize({
            top: 100,
            left: 50,
            width: 100,
            height:30
        });
    }
    /**
     * 创建插屏广告
     * 头条：间隔30秒创建一次
     * 
     */
    onCreateInterstitialAd(){
        console.log("GameUI-onCreateInterstitialAd");
        ydhw.CreateInterstitialAd(false,this,
            ()=>{//success
                console.log("PlatformPublic -CreateInterstitialAd-onSuccess");
                // ydhw.ShowInterstitialAd();
            },
            ()=>{//close
                console.log("PlatformPublic -CreateInterstitialAd-onClose");
            },
            ()=>{//fail
                console.log("PlatformPublic -CreateInterstitialAd-onFail");
            }
        );
    }

    /**
     * 显示插屏广告
     * @param e 
     */
    onShowInterstitial(e:Laya.Event):void{
        // console.log("GameUI-onShowInterstitial");
        // //一次创建多次展示
        // //1>支持：魅族
        // //2>不支持：头条,微信,QQ,OPPO
        ydhw.ShowInterstitialAd();//插屏广告每一次show完都要重新创建
        
    }

    /**
     * 增加体力
     * 
     * @param e 
     */
    onPowerAdd(e:Laya.Event):void{
        let p = ydhw.GetPower();
        console.log("PlatformPublic -GetPower:",p);   
        p += 1;
        ydhw.SetPower(p, 2);
    }


    /**
     * 消耗体力
     * 
     * @param e 
     */
    onPowerReduce(e:Laya.Event):void{        
        this.getPowerInfo();
        let p = ydhw.GetPower();
        console.log("PlatformPublic -GetPower:",p);   
        p -= 1;
        ydhw.SetPower(p, 2);
    }

    /**
     * 获取体力信息(后台配置的体力配置信息)
     */
    getPowerInfo(){
        let powerInfo = ydhw.GetPowerInfo();
        console.log("PlatformPublic -获取体力信息:",powerInfo);
    }

    /**
     * 视频分享策略
     * 
     */
    onShowShareOrVideo():void{
        //视频分享策略要ShowShareOrVideo(显示策略结果)和UseShareOrVideoStrategy(消耗策略)这两个接口配合使用

        //显示视频/分享策略-用于游戏展示"视频"或"分享"按钮
        ydhw.ShowShareOrVideo("bb","aa",this,(type: number) => {
            console.log("PlatformPublic -ShowShareOrVideo:",type);
        });
        
        //使用视频/分享策略-用于玩家点击"视频"或"分享"按钮后，消耗掉当前的策略。
        ydhw.UseShareOrVideoStrategy("bb","aa",this,(type: number) => {
            console.log("PlatformPublic -UseShareOrVideoStrategy:",type);
        });
    }
    
    /**
     * 显示激励视频
     * 
     * 
     */
    onShowVideo():void{
        /***********************各平台的注意事项*******************
         * VIVO:
         *      1>. 广告限制一分钟内只能请求一次，请不要频繁请求广告。上一条广告加载或者播放过程中，不能加载新的广告。
         *      2>. 第一次创建视频广告对象时，已自动加载一次广告，请勿重新加载                
         * 头条：
         *      1>在需要展示的地方创建后就显示，不要做预加载
         ********************************************************/
        this.hideBanner();
        // ydhw.CreateRewardVideoAd();
        ydhw.ShowRewardVideoAd(this.custom,true,this,(type)=>{
            console.log("PlatformPublic ---激励视频:",type);
        });
    }

    /**
     * 隐藏Banner广告
     */
    hideBanner():void{
        ydhw.HideBannerAd();
    }

    /**
     * 视频解锁关卡
     */
    onUnlockCustomByVideo():void{
        let result = ydhw.IsUnlockVideo(this.custom);
        console.log("PlatformPublic -视频解锁关卡:",this.custom,result);
    }

    /**
     * 切换界面,可调用强制刷新Banner接口
     */
    onChangeView():void{
        ydhw.SwitchView(true);
    }
    /**
     * 创建平台用户信息按钮
     * 支持平台:QQ,微信
     */
    CreateUserInfoButton(){
        ydhw.CreateUserInfoButton({       
            type: 'image',
            image:'./images/sp_btn_start.png',  //图片地址
            style: {
                left: 100,
                top: 66,
                width: 150,
                height: 40,
                lineHeight: 40,
                borderRadius: 4
            }
        },
        this,(result: any):void =>{
            console.log("PlatformPublic -CreateUserInfoButton-onSuccess:",result);
        },
        (error: any)=>{
            console.log("PlatformPublic -CreateUserInfoButton-onError:",error);
        });
    }
    /**
     * 隐藏平台用户信息按钮
     * 支持平台:QQ,微信
     */
    HideUserInfoButton(){
        console.log("PlatformPublic-HideUserInfoButton");
        ydhw.HideUserInfoButton();
    }

    /**
     * 获取平台用户信息
     * 支持平台:QQ,微信,头条
     * 注意:QQ平台如果没有授权过,第一次调用授权后不会有回调,第二次调用才可以拿到用户信息
     */
    GetUserInfo(){
        ydhw.AvatarUrl
        ydhw.GetUserInfo(this,(result) =>{
            console.log("PlatformPublic-GetUserInfo---result:"+JSON.stringify(result));
        },(error) =>{
            console.log("PlatformPublic-GetUserInfo---error:"+JSON.stringify(error));
        });
    }

    /**
     * 是否已经创建桌面图标
     * @param e 
     */
    HasShortcutInstalled():void{
        ydhw.HasShortcutInstalled(this,
            (result: any)=>{
                console.log("PlatformPublic ---HasShortcutInstalled-onSuccess-result:",result);
            },
            (error: any)=>{
                console.log("PlatformPublic ---HasShortcutInstalled-error:",error);
            },
            ()=>{
                console.log("PlatformPublic ---HasShortcutInstalled-onComplete");
            });
    }

    /**
     * 创建桌面图标
     * @param e 
     */
    InstallShortcut():void{
        ydhw.InstallShortcut(this,
            ()=>{
                console.log("PlatformPublic ---InstallShortcut-onSuccess");
                //这个回调只代表成功拉起创建桌面图标弹窗，并不代表成功创建桌面图标
            },
            (error: any)=>{
                console.log("PlatformPublic ---InstallShortcut-error:"+JSON.stringify(error));
            },
            ()=>{
                console.log("PlatformPublic ---InstallShortcut-onComplete");
            });
    }
    
    /**
     * 退出游戏
     * 支持平台：OPPO、VIVO、魅族
     * 
     * @param e 
     */
    Exit():void{
        ydhw.ExitGame();
    }

    /**
     * 震动-短时
     * 支持平台：OPPO、VIVO、魅族
     * 
     * @param e 
     */
    VibrateShort():void{
        ydhw.VibrateShort();
    }

    /**
     * 震动-长时
     * 支持平台：OPPO、VIVO、魅族
     * 
     * @param e 
     */
    VibrateLong():void{
        ydhw.VibrateLong();
    }
    /**
     * 获取侧边栏类别ID
     */
    getSideBoxList():IYDHW.GameBase.ISideBoxResult[]{
        
        return this.sideBoxList;
    }

    // StoreValue(){
    //     let storeValueRequest = new YDHW.StoreValueRequest("testHash","size");
    //     ydhw.StoreValue(storeValueRequest,this,(res)=>{
    //         console.log("PlatformPublic -StoreValue:" + JSON.stringify(res));
    //     }); 
    // }

    //测试自定义空间值
    testStoreValue(){
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    }

    test_sv_string(){
        //String
        ydhw.StoreValue(
            new YDHW.StoreValueRequest("testString","set","测试数据"),this,
            function(status){

                ydhw.StoreValue(
                    new YDHW.StoreValueRequest("testString","get"),
                    function(status){
                        
                });
        });
    }

    test_sv_list(){
        //List
        ydhw.StoreValue(
            new YDHW.StoreValueRequest("testList","add","0"),this,
            function(status){
                
        });
        ydhw.StoreValue(
            new YDHW.StoreValueRequest("testList","add","2"),this,
            function(status){
                
        });
        ydhw.StoreValue(
            new YDHW.StoreValueRequest("testList","set","0,3"),this,
            function(status){
                ydhw.StoreValue(
                    new YDHW.StoreValueRequest("testList","all"),this,
                    function(status){
                        
                });
                 ydhw.StoreValue(
                    new YDHW.StoreValueRequest("testList","get","0"),this,
                    function(status){
                
                    }
        );
        ydhw.StoreValue(
            new YDHW.StoreValueRequest("testList","size"),this,
            function(status){
                
        });
        ydhw.StoreValue(
            new YDHW.StoreValueRequest("testList","poll","2"),this,
            function(status){
                    ydhw.StoreValue(
                        new YDHW.StoreValueRequest("testList","size"),this,
                        function(status){
                            
                    });
                    ydhw.StoreValue(
                        new YDHW.StoreValueRequest("testList","replace","[\"1\",\"2\",\"3\",\"4\",\"5\"]"),this,
                        function(status){
                            ydhw.StoreValue(
                                new YDHW.StoreValueRequest("testList","all"),this,
                                function(status){
                                    
                            });
                    });
            });
        });
    }

    test_sv_set(){
        //Set
        ydhw.StoreValue(
            new YDHW.StoreValueRequest("testSet","add","12"),this,
            function(status){
                
        });
        ydhw.StoreValue(
            new YDHW.StoreValueRequest("testSet","add","10"),this,
            function(status){
                ydhw.StoreValue(
                    new YDHW.StoreValueRequest("testSet","exist","10"),this,
                    function(status){
                        
                });
                ydhw.StoreValue(
                    new YDHW.StoreValueRequest("testSet","size"),this,
                    function(status){
                        ydhw.StoreValue(
                            new YDHW.StoreValueRequest("testSet","del","10"),this,
                            function(status){
                                ydhw.StoreValue(
                                    new YDHW.StoreValueRequest("testSet","all","12"),this,
                                    function(status){
                                        ydhw.StoreValue(
                                            new YDHW.StoreValueRequest("testSet","replace","[\"1\",\"2\",\"3\",\"4\",\"5\"]"),this,
                                        function(status){
                                            ydhw.StoreValue(
                                                new YDHW.StoreValueRequest("testSet","all"),this,
                                                function(status){
                                                    
                                            });
                                            
                                    });
                                });
                                
                        });
                        
                });
                
        });
    }
    test_sv_hash(){
        //litMap
        ydhw.StoreValue(
            new YDHW.StoreValueRequest("testHash","set","u_name,许"),this,
            function(status){
                ydhw.StoreValue(
                    new YDHW.StoreValueRequest("testHash","get","u_name"),this,
                    function(status){
                        ydhw.StoreValue(
                            new YDHW.StoreValueRequest("testHash","replace","{\"u_name\":\"唐\",\"sex\":\"women\"}"),this,
                        function(status){
                            ydhw.StoreValue(
                                new YDHW.StoreValueRequest("testHash","gets","u_name,sex"),this,
                            function(status){
                                
                            });
                            ydhw.StoreValue(
                                new YDHW.StoreValueRequest("testHash","size"),this,
                            function(status){});
                            ydhw.StoreValue(
                                new YDHW.StoreValueRequest("testHash","values","u_name,sex"),this,
                            function(status){});
                            ydhw.StoreValue(
                                new YDHW.StoreValueRequest("testHash","del","u_name"),this,
                            function(status){
                                ydhw.StoreValue(
                                    new YDHW.StoreValueRequest("testHash","all"),this,
                                function(status){
                                    
                                });
                            });
                        });
                    });
        });
    }
    test_sv_radom(){
        //testRandom
        ydhw.StoreValue(
            new YDHW.StoreValueRequest("testRandom"),this,
            function(status){
        });
    }
}