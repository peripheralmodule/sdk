import { YDHW } from "./../../libs/ydhw/ydhw.sdk";

export enum EM_RECORD_STATUS {
    /**
     * 录制结束或无
     */
    E_NONE = 1,
    /**
     * 录制中
     */
    E_RECORDING = 2,    
    /**
     * 录制暂停事件
     */
    E_PAUSE = 3,
}

export enum EM_RECORD_EVENT {
    /**
     * 录制开始事件
     */
    E_START = "start",
    /**
     * 录制结束事件
     */
    E_STOP = "stop",
    /**
     * 录制暂停事件
     */
    E_PAUSE = "pause",
    /**
     * 录制恢复事件
     */
    E_RESUME = "resume",
    /**
     * 录制取消事件
     */
    E_ABORT = "abort",
    /**
     * 录制时间更新事件.在录制过程中触发该事件
     */
    E_TIME_UPDATE = "timeUpdate",
    /**
     * 错误事件。当录制和分享过程中发生错误时触发该事件
     */
    E_ERROR = "error"
}

/**
 * 微信平台独有接口
 */
export class PlatformWX {

    private recorderStatus:number = 1;
    private showRecorderBtn:boolean = false;
    private RecordChangeCB:Function = null;

    constructor() {
    }
    
    /**
     * 创建格子广告
     */
    CreateGridAd():void {
        let adStyle = new YDHW.AdStyle();
        adStyle = {
            top: 200,
            left: 100,
            width: 200,
            height: 100
        };
        ydhw.CreateGridAd(false,'white',8,adStyle); 
    }

    /**
     * 显示格子广告
     */
    ShowGridAd():void {
        ydhw.ShowGridAd();
    }

    /**
     * 隐藏格子广告
     */
    HideGridAd():void {
        ydhw.HideGridAd();
    }

    /**
     * 系统订阅
     */
    SubscribeSysMsg():void {
        ydhw.SubscribeSysMsg(['SYS_MSG_TYPE_INTERACTIVE'],
            (result: any) => {
                console.log("PlatformWX -SubscribeSysMsg-success:",result);
            },(error:any) =>{
                console.log("PlatformWX -SubscribeSysMsg-error:",error);
            }
        );
    }

    /**
     * 获取用户当前设置
     */
    GetSetting():void {
        ydhw.GetSetting(true,
            (result: any) => {
                console.log("PlatformWX -GetSetting-success:",result);
            },(error:any) =>{
                console.log("PlatformWX -GetSetting-error:",error);
            }
        );
    }

    /**
     * 显示用户信息
     */
    ShowUserInfoButton():void {
        // ydhw.ShowUserInfoButton(
        //     {       
        //         type: 'image',
        //         image:'./images/sp_btn_start.png',  //图片地址
        //         style: {
        //             left: 100,
        //             top: 66,
        //             width: 150,
        //             height: 40,
        //             lineHeight: 40,
        //             borderRadius: 4
        //         }
        //     },
        //     this,(isOk:boolean):void =>{
        //     console.log("PlatformWX -ShowUserInfoButton-isOk:",isOk);
            
        // });
        ydhw.CreateUserInfoButton({       
            type: 'image',
            image:'./images/sp_btn_start.png',  //图片地址
            style: {
                left: 100,
                top: 66,
                width: 150,
                height: 40,
                lineHeight: 40,
                borderRadius: 4
            }
        },
        this,(result: any):void =>{
            console.log("PlatformWX -ShowUserInfoButton-onSuccess:",result);
        },
        (error: any)=>{
            console.log("PlatformWX -ShowUserInfoButton-onError:",error);
        });
    }

    /**
     * 隐藏用户信息按钮
     */
    HideUserInfoButton():void{
        ydhw.HideUserInfoButton();
    }

    /**
     * 分享
     */
    onShareAppMessage():void{
        ydhw.ShareAppMessage("MainScene","aa","bb","award_id",this,(result: any)=>{
            console.log("PlatformWX --ShareAppMessage-back",JSON.stringify(result));
            this.getShareResult();
        });
    }

    /**
     * 获取假分享策略结果
     */
    getShareResult(){
        let shareResultInfo = new YDHW.ShareAppInfo();
        shareResultInfo = {
            channel: "aa",
            module: "bb",
            showTime: new Date().getTime(),
            shareId: 1
        };
        ydhw.GetSharingResults(shareResultInfo,this,(shareBackInfo: IYDHW.IShareBackInfo):void=>{
            console.log("PlatformWX -GetSharingResults:",JSON.stringify(shareBackInfo));
        });
    }
    
    /**
     * 小游戏跳转
     */
    NavigateToMiniProgram(id: number, toAppId: string, toUrl: string, source: string): void {
        ydhw.NavigateToMiniProgram(id,toAppId,toUrl,source,this,(isOk:boolean)=>{
            console.log("PlatformWX -NavigateToMiniProgram-isOk:",isOk);
        });
    }

    /**
     * 获取胶囊按钮左侧位置
     */
    GetLeftTopBtnPosition():void{
        // let topBtnPosition = ydhw.GetLeftTopBtnPosition();
        // console.log("PlatformWX -GetLeftTopBtnPosition:",topBtnPosition);
    }
    /**
     * 录屏状态变化监听设置
     */
    SetRecordChangeCB(callBack: (res: any) => void){
        this.RecordChangeCB = callBack;
    }

    /**
     * 获取当前录屏状态
     */
    GetRecordStatus():number{
        return this.recorderStatus;
    }

    /**
     * 录制游戏-开始或停止
     */
    GameRecorderStartOrStop():void{
        let isVolumeSupported = this.IsVolumeSupported();
        let isFrameSupported = this.IsFrameSupported();
        let isSoundSupported = this.IsSoundSupported();
        console.log("PlatformWX -isVolumeSupported: "+isVolumeSupported+" ,isFrameSupported: "+isFrameSupported+" ,isSoundSupported："+isSoundSupported);

        if(EM_RECORD_STATUS.E_NONE == this.recorderStatus){
            let recordInfo = new YDHW.WX.GameRecorderInfo();
            recordInfo.fps = 26;
            recordInfo.duration = 20;
            recordInfo.gop = 15;
            recordInfo.hookBgm = true;
           ydhw.GameRecorderStart(recordInfo,this,
            (res: any)=>{//onStart
                this.recorderStatus = EM_RECORD_STATUS.E_RECORDING;
                if(this.RecordChangeCB) this.RecordChangeCB(this.recorderStatus);
                console.log("PlatformWX -GameRecorderStart-onStart:",JSON.stringify(res));
            },(res: any)=>{//onStop
                this.recorderStatus = EM_RECORD_STATUS.E_NONE;
                if(this.RecordChangeCB) this.RecordChangeCB(this.recorderStatus);
                console.log("PlatformWX -GameRecorderStart-onStop:",JSON.stringify(res));
            },(error: any)=>{//onError
                this.recorderStatus = EM_RECORD_STATUS.E_NONE;
                if(this.RecordChangeCB) this.RecordChangeCB(this.recorderStatus);
                console.log("PlatformWX -GameRecorderStart-onError:",JSON.stringify(error));
            },(res: any)=>{//onPause
                this.recorderStatus = EM_RECORD_STATUS.E_PAUSE;
                if(this.RecordChangeCB) this.RecordChangeCB(this.recorderStatus);
                console.log("PlatformWX -GameRecorderStart-onPause:",JSON.stringify(res));
            },(res: any)=>{//onResume
                this.recorderStatus = EM_RECORD_STATUS.E_RECORDING;
                if(this.RecordChangeCB) this.RecordChangeCB(this.recorderStatus);
                console.log("PlatformWX -GameRecorderStart-onResume:",JSON.stringify(res));
            },(res: any)=>{//onAbort
                this.recorderStatus = EM_RECORD_STATUS.E_NONE;
                if(this.RecordChangeCB) this.RecordChangeCB(this.recorderStatus);
                console.log("PlatformWX -GameRecorderStart-onAbort:",JSON.stringify(res));
            },(res: any)=>{//onTimeUpdate
                console.log("PlatformWX -GameRecorderStart-onTimeUpdate:",JSON.stringify(res));
            }); 
        }else{
            ydhw.GameRecorderStop();
        }
    }
    
    /**
     * 录制游戏-暂停或继续
     */
    GameRecorderPauseOrResume():void{        
        if(EM_RECORD_STATUS.E_RECORDING == this.recorderStatus){
           ydhw.GameRecorderPause(); 
        }else{
            ydhw.GameRecorderResume();
        }
    }

    /**
     * 放弃录制游戏画面。此时已经录制的内容会被丢弃
     */
    GameRecorderAbort():void{
        ydhw.GameRecorderAbort();
    }

    /**
     * 创建游戏对局回放分享按钮
     */
    CreateGameRecorderShareButton():void{
        // let shareBtnInfo = {
        //     style:{
        //         left:100,
        //         top:100,
        //         height:60,
        //         iconMarginRight:8,
        //         fontSize:30,
        //         color:"#ffffff",
        //         paddingLeft:10,
        //         paddingRight:10,
        //     },
        //     // icon:"",
        //     // image:"",
        //     // text:"",
        //     share:{
        //         query:"a=1&b=2",
        //         // title:{
        //         //     template:"default.score",
        //         //     data:{
        //         //         score:50000
        //         //     }
        //         // },
        //         // button:{
        //         //     template:"default.enter"
        //         // },
        //         bgm:"",//"./sound/bg.mp3",
        //         timeRange:[[1000, 3000], [4000, 5000]]
        //         // volume:1,
        //         // atempo:1,
        //         // audioMix:false,
        //     }
        // };
        let shareStyle = new YDHW.WX.GameRecorderBtnStyle(100,100,60,8,30,"#ffffff",10,10);
        // let shareData = new YDHW.WX.GameRecorderTitleData(5000);
        // let shareTitle = new YDHW.WX.GameRecorderTitle("default.score",shareData);
        let shareInfo = new YDHW.WX.GameRecorderShareInfo("a=1&b=2","");
        let shareBtnInfo = new YDHW.WX.GameRecorderBtnInfo(shareStyle,shareInfo);
        ydhw.CreateGameRecorderShareButton(shareBtnInfo);
    }

    /**
     * 监听游戏对局回放分享按钮的点击事件
     */
    GameRecorderShareButtonOnTap():void{
        ydhw.GameRecorderShareButtonOnTap(this,(res: any) => {
            console.log("PlatformWX -GameRecorderShareButtonOnTap:",JSON.stringify(res));
        });
        // ydhw.GameRecorderShareButtonOffTap(this,(res: any) => {
        //     console.log("PlatformWX -GameRecorderShareButtonOffTap:",JSON.stringify(res));
        // });
    }

    /**
     * 取消监听录制事件。
     */
    GameRecorderOff():void{
        ydhw.GameRecorderOff(EM_RECORD_EVENT.E_START,(res:any)=>{
            console.log("PlatformWX -GameRecorderOff-E_START:",JSON.stringify(res));
        });
        ydhw.GameRecorderOff(EM_RECORD_EVENT.E_STOP,(res:any)=>{
            console.log("PlatformWX -GameRecorderOff-E_STOP:",JSON.stringify(res));
        });
        ydhw.GameRecorderOff(EM_RECORD_EVENT.E_PAUSE,(res:any)=>{
            console.log("PlatformWX -GameRecorderOff-E_PAUSE:",JSON.stringify(res));
        });
        ydhw.GameRecorderOff(EM_RECORD_EVENT.E_RESUME,(res:any)=>{
            console.log("PlatformWX -GameRecorderOff-E_RESUME:",JSON.stringify(res));
        });
        ydhw.GameRecorderOff(EM_RECORD_EVENT.E_ABORT,(res:any)=>{
            console.log("PlatformWX -GameRecorderOff-E_ABORT:",JSON.stringify(res));
        });
        ydhw.GameRecorderOff(EM_RECORD_EVENT.E_ERROR,(res:any)=>{
            console.log("PlatformWX -GameRecorderOff-E_ERROR:",JSON.stringify(res));
        });
        ydhw.GameRecorderOff(EM_RECORD_EVENT.E_TIME_UPDATE,(res:any)=>{
            console.log("PlatformWX -GameRecorderOff-E_TIME_UPDATE:",JSON.stringify(res));
        });
    }

    /**
     * 获取是否支持调节录制视频的播放速率
     */
    IsAtempoSupported():void{
        ydhw.IsAtempoSupported();
    }

    /**
     * 获取是否支持录制游戏画面
     */
    IsFrameSupported():void{
        ydhw.IsFrameSupported();
    }

    /**
     * 获取是否在录制游戏画面的同时支持录制游戏音频的信息
     */
    IsSoundSupported():void{
        ydhw.IsSoundSupported();
    }

    /**
     * 获取是否支持调节录制视频的音量
     */
    IsVolumeSupported():void{
        ydhw.IsVolumeSupported();
    }
    /**
     * 显示/隐藏游戏对局回放分享按钮
     */
    GameRecorderShareButtonShowOrHide(caller: any, method: (isShow: boolean) => void):void{
        if(this.showRecorderBtn){
            ydhw.GameRecorderShareButtonHide();
        }else{
            ydhw.GameRecorderShareButtonShow();
        }
         
        this.showRecorderBtn = !this.showRecorderBtn;
        caller && method && method.call(caller, this.showRecorderBtn);
    }
    /**
     * 设置 wx.shareMessageToFriend 接口 query 字段的值
     */
    SetMessageToFriendQuery():void{
        let shareMessageToFriendScene = 20;
        let success = ydhw.SetMessageToFriendQuery(shareMessageToFriendScene);
        console.log("PlatformWX -SetMessageToFriendQuery-success:",success);
    }
    /**
     * 监听主域接收 wx.shareMessageToFriend 接口的成功失败通知
     */
    OnShareMessageToFriend():void{
        ydhw.OnShareMessageToFriend(this,(res)=>{
            console.log("PlatformWX -OnShareMessageToFriend：",JSON.stringify(res));
        });
    }
    /**
     * 绑定朋友圈分享参数
     */
    OnShareTimeline():void{
        let timeLineInfo = new YDHW.WX.OnShareTimelineInfo("","分享朋友圈测试","a=1&b=2");
        ydhw.OnShareTimeline(timeLineInfo,this,()=>{
            console.log("PlatformWX -OnShareTimeline:",JSON.stringify(timeLineInfo));
        });
    }
    /**
     * 取消绑定分享参数
     */
    OffShareTimeline():void{
        ydhw.OffShareTimeline();
    }

    // /**
    //  * 测试代码段-CP请忽略
    //  * 邀请好友(部分逻辑在子域)
    //  */
    // TestSendFriend(){
    //     //发送分享到好友代码段
    //     let openDataContext = wx.getOpenDataContext()
    //     openDataContext.postMessage({
    //       key: 'shareToFriend'
    //     })
    //     //上传分数代码段
    //     let openDataContext = wx.getOpenDataContext()
    //     openDataContext.postMessage({
    //       key: 'setCurMaxScore',
    //       message: 300
    //     })
    //     //获取好友上传数据代码段
    //     let openDataContext = wx.getOpenDataContext()
    //     openDataContext.postMessage({
    //       key: 'reqAllFriendData',
    //       message: { shareTicket: 'score', keyList:'keyList'}
    //     })
    // }
    // /**
    //  * 测试代码段-CP请忽略
    //  * 在线好友邀请-启动服务(部分逻辑在子域)
    //  */
    // TEST_startServer() {
    //     const m = wx.getGameServerManager()
    //     m.login().then(() => {

    //       console.warn('PlatformWX -游戏服务登录成功')
    //       m.startStateService({
    //         userState: 'inviteFrends',
    //         success: () => {
    //           console.log('PlatformWX -startStateService-success')
    //         },
    //         fail: res => {
    //           console.error('PlatformWX -startStateService-fail', res)
    //         }
    //       })

    //     }).catch(err => {
    //       console.warn('游戏服务登录失败', err)
    //     });
    //     m.onInvite(res => {
    //       console.log('该玩家接受了来自用户', res.openId, '的邀请')
    //       console.log('本次邀请的附加信息', res.data)
    //     })
    //   }
    //   /**
    //  * 测试代码段-CP请忽略
    //  * 在线好友邀请-邀请(部分逻辑在子域)
    //  */
    // TEST_sendFriendOnLine() {
    //   wx.postMessage({
    //     key: 'inviteFrends'
    //   })
    // }
}