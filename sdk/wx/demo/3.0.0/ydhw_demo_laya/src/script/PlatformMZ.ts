import { YDHW } from "./../../libs/ydhw/ydhw.sdk";

/**
 * 魅族平台独有接口
 */
export class PlatformMZ {

    constructor() {
    }

    /**
     * 获取网络类型
     * 
     * @param e 
     */
    GetNetworkType():void{
        ydhw.GetNetworkType(this,(type:any)=>{
            console.log("PlatformMZ --GetNetworkType-type:"+JSON.stringify(type));
        });
    }

    /**
     * 监听网络状态变化事件
     * 
     * @param e 
     */
    OnNetworkStatusChange():void{
        ydhw.OnNetworkStatusChange(this,(type:string,isConnected: boolean)=>{
            console.log("PlatformMZ --OnNetworkStatusChange-type:"+JSON.stringify(type)+",isConnected:"+isConnected);
        });
    }
}