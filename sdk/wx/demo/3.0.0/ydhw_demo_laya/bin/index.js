//  //------------------------字节跳动------------------------------------- 
//   var YDHW_CONFIG = {
//     appid: "tt711307aeee39fe2d",
//     version: '1.0.0',
//     banner_ad_unit_id_list: ['15926jnded313m55ms'],//Banner广告
//     interstitial_ad_unit_id_list: ['b5a2vkpi85b7f203df'],//插屏广告
//     spread_ad_unit_id_list: [],//开屏广告
//     native_ad_unit_id_list: [],//原生广告
//     video_ad_unit_id_list: ['6sn234aoqf3737fmu9'],//视频广告
//     grid_ad_unit_id_list: ['adunit-8b60da8bb5f96948'],//格子广告
//     tt_template_id_list: ['of6b158hdol2r5berq'],//TT 分享素材模板ID列表
//     interstitialAd_first_show_wait_time: 10,//插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
//     interstitialAd_show_time_interval: 10,//插屏广告-两次展示之间时间间隔（秒）
//     side_box_count: 20,//侧边栏列表item最小保留数(基于曝光策略)
//     pkg_name: "",  //包名OPPO、VOVP、魅族平台需要

//     //--------------以下配置CP无需理会------------
//     project: '',
// 	platform: 'tt',
//     env: 'dev',//online or dev
//     debug: true,
//     platformCode:4,
//     inspector: false,
//     engine: 'laya',
//     res_version: '20200310',
//     appkey: "30248858",
//     resource_url: 'http://127.0.0.1:3100',
//     scene_white_list: [1005, 1006, 1037, 1035],
// };
  
// window.YDHW_CONFIG = YDHW_CONFIG;
// // loadLib("tt/ydhw.tt.sdk.js");
// loadLib("tt/ydhw.tt.sdk.min.js");


// //------------------------微信-------------------------------------
// var YDHW_CONFIG = {
//     appid: "wxd42b44dbd686d382",
//     version: '1.0.0',
//     banner_ad_unit_id_list: ['adunit-723102dee1e8ddc3'],//Banner广告
//     interstitial_ad_unit_id_list: ['adunit-fe07eedeb0218f5c'],//插屏广告
//     spread_ad_unit_id_list: [],//开屏广告
//     native_ad_unit_id_list: [],//原生广告
//     video_ad_unit_id_list: ['adunit-546eabc2b53c133e'],//视频广告
//     grid_ad_unit_id_list: ['adunit-8b60da8bb5f96948'],//格子广告
//     tt_template_id_list: [],//TT 分享素材模板ID列表
//     interstitialAd_first_show_wait_time: 10,//插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
//     interstitialAd_show_time_interval: 10,//插屏广告-两次展示之间时间间隔（秒）
//     side_box_count: 20,//侧边栏列表item最小保留数(基于曝光策略)
//     pkg_name: "",  //包名OPPO、VOVP、魅族平台需要

//     //--------------以下配置CP无需理会------------
//     project: '',
// 	platform: 'wx',
//     platformCode:0,
//     env: 'dev',//online or dev
//     debug: true,
//     inspector: false,
//     engine: 'laya',
//     res_version: '20200310',
//     appkey: "30248858",
//     resource_url: 'http://127.0.0.1:3100',
//     scene_white_list: [1005, 1006, 1037, 1035],
// };

// window.YDHW_CONFIG = YDHW_CONFIG;
// // loadLib("wx/ydhw.wx.sdk.min.js");
// loadLib("wx/ydhw.wx.sdk.js");
  
//------------------------QQ-------------------------------------
var YDHW_CONFIG = {
    appid: "1109612091",
    version: '1.0.0',
    banner_ad_unit_id_list: ['acfa9a46408185a6ac7ac9b82521efb8'],//Banner广告
    interstitial_ad_unit_id_list: ['36bf954e345166c6b8b39c90522026d2'],//插屏广告
    spread_ad_unit_id_list: [],//开屏广告
    native_ad_unit_id_list: [],//原生广告
    video_ad_unit_id_list: ['afeb45aa98ec48e01d8e64030545ced6'],//视频广告
    grid_ad_unit_id_list: ['adunit-8b60da8bb5f96948'],//格子广告
    appbox_ad_unit_id_list: ['d26d39be40c18d6e56cb862d120da29c'],//appBox广告[QQ]
    block_ad_unit_id_list:['f59bd03b36d3b2ab9f5d3bea650cbcee'],//积木广告[QQ]
    tt_template_id_list: [],//TT 分享素材模板ID列表
    interstitialAd_first_show_wait_time: 10,//插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
    interstitialAd_show_time_interval: 10,//插屏广告-两次展示之间时间间隔（秒）
    side_box_count: 20,//侧边栏列表item最小保留数(基于曝光策略)
    pkg_name: "",  //包名OPPO、VOVP、魅族平台需要

    //--------------以下配置CP无需理会------------
    project: '',
	platform: 'qq',
    platformCode:1,
    env: 'dev',//online or dev
    debug: true,
    inspector: false,
    engine: 'laya',
    res_version: '20200310',
    appkey: "30248858",
    resource_url: 'http://127.0.0.1:3100',
    scene_white_list: [1005, 1006, 1037, 1035],
};
window.YDHW_CONFIG = YDHW_CONFIG;
// loadLib("qq/ydhw.qq.sdk.min.js");
loadLib("qq/ydhw.qq.sdk.js");


// //------------------------OPPO-------------------------------------
// var YDHW_CONFIG = {
//     appid: "30250869",
//     version: '1.0.0',
//     banner_ad_unit_id_list: ['166363'],//Banner广告
//     interstitial_ad_unit_id_list: [],//插屏广告
//     spread_ad_unit_id_list: [],//开屏广告
//     native_ad_unit_id_list: ['166367'],//原生广告[OPPO,VIVO]
//     video_ad_unit_id_list: ['166368'],//视频广告
//     grid_ad_unit_id_list: [],//格子广告
//     appbox_ad_unit_id_list: [],//appBox广告[QQ]
//     block_ad_unit_id_list:[],//积木广告[QQ]
//     tt_template_id_list: [],//TT 分享素材模板ID列表
//     interstitialAd_first_show_wait_time: 10,//插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
//     interstitialAd_show_time_interval: 10,//插屏广告-两次展示之间时间间隔（秒）
//     side_box_count: 20,//侧边栏列表item最小保留数(基于曝光策略)
//     pkg_name: "ydhw.ccnbxl.nearme.gamecenter",  //包名OPPO、VOVP、魅族平台需要

//     //--------------以下配置CP无需理会------------
//     project: '',
// 	platform: 'oppo',
//     platformCode:2,
//     env: 'dev',//online or dev
//     debug: true,
//     inspector: false,
//     engine: 'laya',
//     res_version: '20200310',
//     appkey: "30248858",
//     resource_url: 'http://127.0.0.1:3100',
//     scene_white_list: [1005, 1006, 1037, 1035],
// };

// window.YDHW_CONFIG = YDHW_CONFIG;
// loadLib("oppo/ydhw.oppo.sdk.min.js");
// // loadLib("oppo/ydhw.oppo.sdk.js");

// //------------------------VIVO-------------------------------------
// var YDHW_CONFIG = {
//     appid: "100001687",
//     version: '1.0.0',
//     banner_ad_unit_id_list: ['87b40276059048e7b86629c6a49b4187'],//Banner广告
//     interstitial_ad_unit_id_list: ['4bd36ddbc30e41ca9f9b23335643f8e9'],//插屏广告
//     spread_ad_unit_id_list: [],//开屏广告
//     native_ad_unit_id_list: ['9303d25cb65c4cdd858b43c6bb1a5345'],//原生广告[OPPO,VIVO]
//     video_ad_unit_id_list: ['065c483cddd148e5b89bbaad68fded5c'],//视频广告
//     grid_ad_unit_id_list: [],//格子广告
//     appbox_ad_unit_id_list: [],//appBox广告[QQ]
//     block_ad_unit_id_list:[],//积木广告[QQ]
//     tt_template_id_list: [],//TT 分享素材模板ID列表
//     interstitialAd_first_show_wait_time: 10,//插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
//     interstitialAd_show_time_interval: 10,//插屏广告-两次展示之间时间间隔（秒）
//     side_box_count: 20,//侧边栏列表item最小保留数(基于曝光策略)
//     pkg_name: "com.szydhw.fkcc.vivominigame",  //包名OPPO、VOVP、魅族平台需要

//     //--------------以下配置CP无需理会------------
//     project: '',
// 	platform: 'vivo',
//     platformCode:3,
//     env: 'dev',//online or dev
//     debug: true,
//     inspector: false,
//     engine: 'laya',
//     res_version: '20200310',
//     appkey: "30248858",
//     resource_url: 'http://127.0.0.1:3100',
//     scene_white_list: [1005, 1006, 1037, 1035],
// };

// window.YDHW_CONFIG = YDHW_CONFIG;
// // loadLib("vivo/ydhw.vivo.sdk.min.js");
// loadLib("vivo/ydhw.vivo.sdk.js");


// //------------------------魅族-------------------------------------
// var YDHW_CONFIG = {
//     appid: "19868920660",
//     version: '1.0.0',
//     banner_ad_unit_id_list: ['AiVo8Ijb'],//Banner广告
//     interstitial_ad_unit_id_list: ['xViWNvuK'],//插屏广告
//     spread_ad_unit_id_list: [],//开屏广告
//     native_ad_unit_id_list: [],//原生广告[OPPO,VIVO]
//     video_ad_unit_id_list: ['ByykrF4B'],//视频广告
//     grid_ad_unit_id_list: [],//格子广告
//     appbox_ad_unit_id_list: [],//appBox广告[QQ]
//     block_ad_unit_id_list:[],//积木广告[QQ]
//     tt_template_id_list: [],//TT 分享素材模板ID列表
//     interstitialAd_first_show_wait_time: 10,//插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
//     interstitialAd_show_time_interval: 10,//插屏广告-两次展示之间时间间隔（秒）
//     side_box_count: 20,//侧边栏列表item最小保留数(基于曝光策略)
//     pkg_name: "com.szylhy.zwjsdzz.mzgame",  //包名OPPO、VOVP、魅族平台需要

//     //--------------以下配置CP无需理会------------
//     project: '',
//     platform: 'meizu',
//     platformCode:12,
//     env: 'dev',//online or dev
//     debug: true,
//     inspector: false,
//     engine: 'laya',
//     res_version: '20200310',
//     appkey: "30248858",
//     resource_url: 'http://127.0.0.1:3100',
//     scene_white_list: [1005, 1006, 1037, 1035],

//     resolution_width:640,   //屏幕适配-设计分布率-宽
//     resolution_height:1136, //屏幕适配-设计分布率-高
//     scale_type:0,// 屏幕适配-缩放类型[0:等比缩放，显示所有内容,1:等比缩放，全屏显示，可能显示不全,2:非等比缩放，全屏显示，显示所有内容，可能会拉伸]
// };

// window.YDHW_CONFIG = YDHW_CONFIG;
// // loadLib("mz/ydhw.mz.sdk.min.js");
// loadLib("mz/ydhw.mz.sdk.js");

/**
 * 设置LayaNative屏幕方向，可设置以下值
 * landscape           横屏
 * portrait            竖屏
 * sensor_landscape    横屏(双方向)
 * sensor_portrait     竖屏(双方向)
 */
window.screenOrientation = "sensor_landscape";

//-----libs-begin-----
loadLib("libs/laya.core.js")
loadLib("libs/laya.ui.js")
loadLib("libs/laya.physics.js")
//-----libs-end-------
loadLib("js/bundle.js");
