const Utils = require("wxUtils")

class SubRenderer {
	constructor() {
		// 所有好友的数据;
		console.log("SubRenderer constructor");
		this.allFriendData = null;
		if (wx.getSharedCanvas == null) return;
		// 初始化模块;
		this.init();
	}

	/**
	 * 初始化;
	 */
	init() {
		// 排行榜模块;
		this.reqAllFriendData('user_id,level', 'level');
	}

	/**
	 * 消息监听;
	 */
	listen() {
		wx.onMessage(msg => {
			console.warn("wx.onMessage reqRankListData = ", msg);
			if (wx.getSharedCanvas == null) return;

			switch (msg.key) {
					// 获取自己的数据;
        case "shareToFriend"://"oxJAv5cyKbGj-6FnCKCaDMa59N7I"
          wx.shareMessageToFriend({
            openId: "oxJAv5cyKbGj-6FnCKCaDMa59N7I",
            title:"老铁，就等你了",
            success:(res)=>{
              console.warn("RANK --shareMessageToFriend-success", JSON.stringify(res));
            },
            fail: (res) => {
              console.warn("RANK --shareMessageToFriend-fail", JSON.stringify(res));
             },
            complete: (res) => { 
              console.warn("RANK --shareMessageToFriend-complete", JSON.stringify(res));
            },
          })
					break;
				case "setCurMaxScore":
          wx.setUserCloudStorage({
            KVDataList: [{ key: 'score', value: 5000 }],
            success: res => {
              console.log("RANK ---setUserCloudStorage:" + JSON.stringify(res));
            },
          });
					break;
					// 查看群排行;
				case "reqAllFriendData":
					var configData = msg.message;
					this.reqAllFriendData(
						configData.shareTicket,
						configData.keyList,
						data => {
              console.log("RANK ---data:" + JSON.stringify(data));
						}
					);
					break;
			}
		});
	}


	/**
	 * 取出所有好友数据
	 * @param {参数列表} keyList 
	 * @param {回调创建排行UI界面} callFunc 
	 * @param {是否强制更新数据} isUpdate 
	 */
	reqAllFriendData(keyList, sortList, callFunc, isUpdate) {
		const _keyList = Utils.get_keyList(keyList);
		const _sortList = Utils.get_keyList(sortList);
		// // 有数据的话就不再次请求;
		// if (this.allFriendData && !isUpdate) {
		// 	if (callFunc) {
		// 		callFunc(this.allFriendData);
		// 	}
		// 	return;
		// }

		this.allFriendData = null;
		wx.getFriendCloudStorage({
			keyList: _keyList,
			success: res => {
				console.log("reqAllFriendData success", JSON.stringify(res));
				this.allFriendData = Utils.data_sort(res.data, _sortList);
				if (callFunc) {
					callFunc(this.allFriendData);
				}
			},
			fail: res => {
				console.log("reqAllFriendData fail", JSON.stringify(res));
			},
		});
		// wx.shareMessageToFriend(Object object)
	}

}

const sub = new SubRenderer();
sub.listen();