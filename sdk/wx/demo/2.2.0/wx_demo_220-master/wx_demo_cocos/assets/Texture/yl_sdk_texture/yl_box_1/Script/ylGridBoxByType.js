
cc.Class({
    extends: cc.Component,
    properties: {
        node_bg:cc.Node,
        sp_type:cc.Sprite,
        lb_type:cc.Label,
        lay_grid:cc.Node,
        pfb_item:cc.Prefab,
        type_icon:[cc.SpriteFrame],
    },
    initData(startIndex,endIndex,datas,typeInfo,scene_name) {
        this.sp_type.spriteFrame = this.type_icon[typeInfo.typeIconId];
        this.lb_type.string = typeInfo.typeName;
        if (datas) {
            let count = endIndex - startIndex;
            let total = count > 2 ? Math.floor(count/2) : count;
            let showLightId = window.Global.getRandomList(total,count);
            for(let i=startIndex;i<endIndex;i++){
                let box = cc.instantiate(this.pfb_item);
                box.parent = this.lay_grid;
                let random = Math.floor(Math.random()*10);
                let isNew = (random == 1 || random == 3 || random == 5 || random == 7 || random == 9);
                box.getComponent('ylBox1GridItem').setItemConf(datas[i],isNew,scene_name);
                if(showLightId.length > 0){
                    for(let j=0;j<showLightId.length;j++){
                        if((startIndex + showLightId[j]) == i){
                            box.getComponent('ylBox1GridItem').startAnimLight();
                            break;
                        }
                    }
                }
            }
        }
    },
});
