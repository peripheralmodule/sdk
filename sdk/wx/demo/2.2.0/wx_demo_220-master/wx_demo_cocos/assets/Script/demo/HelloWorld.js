cc.Class({
    extends: cc.Component,

    properties: {
        label:cc.Label,
        btnShare:cc.Node,               //分享按钮
        btnSideBox:cc.Node,             //侧边栏列表按钮
        btnBoardAwardList:cc.Node,      //积分墙列表按钮
        btnGridAndScrollList:cc.Node,   //浮动卖量列表按钮
        btnSign:cc.Node,                //签到按钮

        pbSideBox:cc.Prefab,            //侧边栏列表界面
        pbInvite:cc.Prefab,             //积分墙列表界面
        pbYlBoxGrid:cc.Prefab,          //卖量网格浮动条
        pbYlBoxScrollHori:cc.Prefab,    //卖量横向浮动条
        pbYlBoxScrollVerti:cc.Prefab,   //卖量纵向浮动条
        pbYlGameOver:cc.Prefab,         //游戏结束界面
        pbSign:cc.Prefab,               //签到界面
        pbGetAward:cc.Prefab,           //获得奖励View
        pbFullBox:cc.Prefab,            //全屏卖量界面
        pbBreakEgg:cc.Prefab,           //砸金蛋
        pbOpenBox:cc.Prefab,            //开宝箱
        pbShakeTree:cc.Prefab,          //摇钱树
        preRank:cc.Prefab,              //排行榜
        pbBox1:cc.Prefab,               //盒子1
        nodeFloatView:cc.Node,          //浮动卖量View
        lbGridAd:cc.Label,              //格子广告按钮文字
        pbFullBox2:cc.Prefab,               //全屏卖量
    },
    onLoad: function () {
        console.log("-----onLoad-start");
        this.shareCount = 0;
        this.initEvent();
        let self = this;
        wx.ylGameLifeListen(function(type,res){
            if(type === 'onShow'){
                console.warn("YLSDK HelloWorld-onShow--res:",res);
                wx.ylGetBoardAward(function(data){
                if(data){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_GET_AWARD);
                    evt.setUserData(data);
                    cc.director.dispatchEvent(evt);
                }
            });
            }else if(type === 'onHide'){
                console.warn("YLSDK HelloWorld-onHide");
            }else if(type === 'onError'){
                console.warn("YLSDK HelloWorld-onError--res:",res);
            }
        });
        wx.ylOnPowerChange(function(type){
            let power =  wx.ylGetPower();
            console.warn("HelloWorld-ylOnPowerChange back[1:视频,2:定时自动恢复,3:调用ylSetPower]:",type,power);
            let textType = (type == 1) ? "看视频" : "定时";
            wx.showToast({
                  title: textType+'获得体力',
                  icon: 'success',
                  duration: 2000
                });
        }.bind(this));
        wx.ylInitSDK(function(success){
            console.log("HelloWorld-ylInitSDK:",success);
            if(success === true){
                console.warn("HelloWorld-ylInitSDK-初始化成功");
                self.doSDKinterface();
                self.refreshBoardAndBoxBtnVisible();
            }else if(success == 'config_success' || success == 'config_fail'){
                console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
                wx.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("HelloWorld-ylGetPowerInfo:",data);
                });
            }else{
                console.warn("HelloWorld-ylInitSDK-初始化失败");
            }
        });
        this.custom = 0;
        // this.onFullBox2Show();

        let btnInfo = wx.ylGetLeftTopBtnPosition();
        wx.ylLog("----btnInfo:"+JSON.stringify(btnInfo),'log');
        this.btnSideBox.width = btnInfo.width;
        this.btnSideBox.height = btnInfo.height;
        this.btnSideBox.y = btnInfo.y;
        this.btnSideBox.x = btnInfo.x+this.btnSideBox.width/2;
        console.log("-----onLoad-end");
    },

    /**
     * 初始化事件
     **/
    initEvent(){
        cc.director.on('INVITE_JUMP_TO_MIN_GAME', this.inviteJumpToMinGame.bind(this));
        cc.director.on(window.Global.EVENT_SHOW_GET_AWARD, this.onShowGetAward.bind(this)); 
        cc.director.on(window.Global.EVENT_SHOW_TIP, this.showTip.bind(this));
        cc.director.on(window.Global.EVENT_SHOW_FULL_BOX, this.onShowFullBox.bind(this)); 
    },

    /**
    * 调用SDK接口
    **/
    doSDKinterface(){
        wx.ylLog("---调用接口---",'log');
        wx.ylSideBox(function (data) {
        }.bind(this));
        wx.ylGetCustom(function(data){
            // if(!this.nodeBox1) this.nodeBox1 = cc.instantiate(this.pbBox1);
            // this.nodeBox1.x = 0;
            // this.nodeBox1.y = 0;
            // this.nodeBox1.parent = this.node;
            // this.nodeBox1.active = true;
        }.bind(this));
        wx.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status){
            if(status){
                //干点什么
            }
        }.bind(this));
        wx.ylIntegralWall(function(){}.bind(this));
        wx.ylGetPowerInfo(function(data){
            console.log("HelloWorld ylGetPowerInfo--data:",data);
        });
        let userWXinfo = wx.ylGetUserWXInfo();//先获取本地缓存，没有在弹窗通过用户授权获取
        // if(!userWXinfo){
        //     //是否收集用户信息按钮信息(具体配置请参考微信小游戏官方文档)
        //     wx.ylUserInfoButtonShow(
        //         {       
        //           type: 'image',
        //           image:'./images/sp_btn_start.png',  //图片地址
        //           style: {
        //             left: 100,
        //             top: 66,
        //             width: 150,
        //             height: 40,
        //             lineHeight: 40,
        //             borderRadius: 4
        //           }
        //         },
        //         function(data){
        //             console.warn("------ylUserInfoButtonShow-data:",data);
        //             if(data){
        //                 userWXinfo = data;
        //                 wx.ylUserInfoButtonHide();
        //             }
        //     }.bind(this));
        // }
        let loginInfo = wx.ylGetUserInfo();//获取登录信息，包括微信oppid和code(code只有第一次登录才能取到)
        console.log("---登录信息-loginInfo:",JSON.stringify(loginInfo));


        var that = this;
        /**
         * 获取分享图列表
         */
        wx.ylShareCard(function (shareInfo) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
                this.btnShare.active = true;
            }else{
                //获取失败
            }
        }.bind(this));
        // wx.ylStatisticViedo(1);
        wx.ylStatisticShareCard(177);
        wx.ylGetLayerList(function(data){
            if(data && data.length >0){
                this.layerList = data;
                wx.ylStatisticLayer(this.layerList[this.layerList.length-1].layerPath,function(data){                
                });
            }
        }.bind(this));
        this.testStoreValue();
        let appid = wx.ylGetAppID();
        console.log("---ylGetAppID:",appid);
        wx.ylGetSignData(function(data){
            console.log("---ylGetSignData:",data);
        });
        wx.ylIntegralWall(function(data){

        });
        let inviteAccount = wx.ylGetInviteAccount();
        console.log("---ylGetInviteAccount:",inviteAccount);
    },

    //刷新侧边栏/积分墙按钮展示状态
    refreshBoardAndBoxBtnVisible(){
        let data = wx.ylGetSwitchInfo();
        console.log("---刷新开关状态---",data);
        // this.btnSideBox.active = data.switchPush === 1;//[ 0:关、1:开 ]
        // this.btnGridAndScrollList.active = data.switchPush === 1;
    },

    //显示全屏卖量
    onFullBox2Show(){
        wx.ylEventCount("Click-SideBox","MainScene");
        if(!this.nodeFullBox2) this.nodeFullBox2 = cc.instantiate(this.pbFullBox2);
        this.nodeFullBox2.x = 0;
        this.nodeFullBox2.y = 0;
        this.nodeFullBox2.parent = this.node;
        this.nodeFullBox2.getComponent('ylBox2').showView();
    },
    //显示侧边栏列表对话框
    onBoxShow(){
        wx.ylEventCount("Click-SideBox","MainScene");
        if(!this.nodeBox) this.nodeBox = cc.instantiate(this.pbSideBox);
        this.nodeBox.x = 0;
        this.nodeBox.y = 0;
        this.nodeBox.parent = this.node;
        this.nodeBox.getComponent('ylSideBox').showView();
    },

    //显示积分墙列表对话框
    onBoardAwardListShow(){
        wx.ylEventCount("Click-BoardAward","MainScene");
        if(!this.nodeInvite) this.nodeInvite = cc.instantiate(this.pbInvite);
        this.nodeInvite.x = 0;
        this.nodeInvite.y = 0;
        this.nodeInvite.parent = this.node;
        this.nodeInvite.getComponent('ylInvite').showView();
    },
    //显示浮动卖量列表
    onShowGridAndScrollList(){
        //可以把卖量列表绑定在界面或对话框上面,可参考签到对话框效果
        if(!this.nodeYlBoxGrid){
            this.nodeYlBoxGrid = cc.instantiate(this.pbYlBoxGrid);
            this.nodeYlBoxGrid.x = 0;
            this.nodeYlBoxGrid.y = 0;
            this.nodeYlBoxGrid.parent = this.nodeFloatView;
        } 
        this.nodeYlBoxGrid.getComponent('ylBoxGrid').showView(0,300);
        //
        if(!this.nodeYlBoxScrollHori){
            this.nodeYlBoxScrollHori = cc.instantiate(this.pbYlBoxScrollHori);
            this.nodeYlBoxScrollHori.x = 0;
            this.nodeYlBoxScrollHori.y = 0;
            this.nodeYlBoxScrollHori.parent = this.nodeFloatView;
        } 
        this.nodeYlBoxScrollHori.getComponent('ylBoxScrollHori').showView(0,-894);
        //
        if(!this.nodeYlBoxScrollVerti){
            this.nodeYlBoxScrollVerti = cc.instantiate(this.pbYlBoxScrollVerti);
            this.nodeYlBoxScrollVerti.x = 0;
            this.nodeYlBoxScrollVerti.y = 0;
            this.nodeYlBoxScrollVerti.parent = this.nodeFloatView;
        } 
        this.nodeYlBoxScrollVerti.getComponent('ylBoxScrollVerti').showView(-276,-173);
        this.nodeFloatView.active = true;
    },
    onCloseFloatView(){
        this.nodeFloatView.active = false;
    },
    //显示获得奖励对话框
    onShowGetAward(evt){
        let data = evt.getUserData();
        if(!this.nodeGetAward) this.nodeGetAward = cc.instantiate(this.pbGetAward);
        this.nodeGetAward.x = 0;
        this.nodeGetAward.y = 0;
        this.nodeGetAward.zorder = 20;
        this.nodeGetAward.parent = this.node;
        this.nodeGetAward.getComponent('ylGetAward').showView(data);
    },
    showTip(tip){

    },
    //显示全屏卖量界面
    onShowFullBox(){        
        if(!this.nodeFullBox) this.nodeFullBox = cc.instantiate(this.pbFullBox);
        this.nodeFullBox.x = 0;
        this.nodeFullBox.y = 0;
        this.nodeFullBox.zorder = 10;
        this.nodeFullBox.parent = this.node;
        this.nodeFullBox.getComponent('ylBoxFull').showView();
        wx.ylBannerAdHide();
    },
    //显示签到对话框 
    onShowSign(){
        if(!this.nodeSign) this.nodeSign = cc.instantiate(this.pbSign);
        this.nodeSign.parent = this.node;
        this.nodeSign.getComponent('ylSign').showView();
    },

    //分享对话框
    onClickShare(){
        wx.ylEventCount("Click-Share","MainScene");
        let shareInfo = {
            channel:'bb',
            module:'cc',
            scene:'MenuScene',
            inviteType:'award_id',
        };
        wx.ylShareAppMessage(function(sResult){
            if(sResult.sSuccess){
                let result = wx.ylRewardByVideoOrShare(false);
                let slLimit = wx.ylGetVSLimit();
                console.log("---------onClickShare-result:",result,slLimit);
            }
            console.log("---onClickShare------分享结果-sResult:",sResult);
        },shareInfo);
    },
    //砸金蛋
    onClickBreakEgg(){
        if(!this.nodeBreakEgg) this.nodeBreakEgg = cc.instantiate(this.pbBreakEgg);
        this.nodeBreakEgg.parent = this.node;
        this.nodeBreakEgg.getComponent('ylBreakEgg').showView();
    },
    //开宝箱
    onOpenBox(){
        if(!this.nodeOpenBox) this.nodeOpenBox = cc.instantiate(this.pbOpenBox);
        this.nodeOpenBox.parent = this.node;
        this.nodeOpenBox.getComponent('ylOpenBox').showView();
    }, 
    //摇钱树
    onShakeTree(){
        if(!this.nodeShakeTree) this.nodeShakeTree = cc.instantiate(this.pbShakeTree);
        this.nodeShakeTree.parent = this.node;
        this.nodeShakeTree.getComponent('ylShakeTree').showView();
    }, 
    //显示获胜结算对话框
    onShowOverWin(){
        this.onShowGameOver(true);  
    },
    //视频分享策略
    onShowShareOrVideo(){
        wx.ylShowShareOrVideo("bb","aa",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    this.shareCount += 1;
                    let _channel = (this.shareCount %2 ==0) ? 'bb' : 'cc';
                    let _module = (this.shareCount %2 ==0) ? 'aa' : 'cc';
                    console.warn("------策略-分享");
                    let shareInfo = {
                        channel:_channel,
                        module:_module,
                        scene:'MenuScene',
                        inviteType:'award_id',
                    };
                    wx.ylShareAppMessage(function(sResult){
                        if(sResult.sSuccess){
                            let result = wx.ylRewardByVideoOrShare(false);
                            let slLimit = wx.ylGetVSLimit();
                            console.log("---------ylRewardByVideoOrShare-result:",result,slLimit);
                        }
                        console.log("---------分享结果-sResult:",sResult);
                    },shareInfo);

                    // let onShare = {
                    //     channel:_channel,
                    //     module:_module,
                    //     showTime: this.showTime,
                    // };
                    // wx.ylGetSharingResults(onShare,function(result){
                    //     console.warn("-----ylGetSharingResults-result：",result);
                    //     if(result.sSuccess){
                    //         //分享成功
                    //     }else{
                    //         //分享失败
                    //         if(result.hasStrategy){
                    //             console.warn("-----分享失败话术：",result.trickJson);
                    //         }
                    //     }
                    // });
                    break;
                case 2:
                    let result = wx.ylRewardByVideoOrShare(true);
                    console.warn("------策略-视频-result:",result);
                    break;
            }
        }.bind(this));
    },
    //显示失败结算对话框
    onShowOverFail(){
        this.onShowGameOver(false);  
    },
    //显示视频广告
    onShowVideo(){
        wx.ylBannerAdHide();
        // wx.ylShowVideoAd(function(){},this.custom);
        wx.ylShowVideoAd2({
            callBack:function(status){
                console.log("----showVideo:"+status);
                switch (status){
                    case 1:
                        //视频广告-播放完成
                        break;
                    case 2:
                        //视频广告-播放取消
                        break;
                    case 0:
                        //视频广告-播放失败
                        break;
                }
            },
            unlockCustomNum:this.custom,
            getPower:true
        });
    },
    //显示banner
    onShowBanner(){
        // wx.ylBannerAdCreate(true,function(){            
        // });
        wx.ylBannerAdCreateByStyle({
            left: 20,
            top: 100,
            width: 80,
        }, true, function(res){}, true);
    },
    onHideBanner(){
        wx.ylBannerAdHide();
    },
    //视频解锁关卡
    onVideoUnlockCustoms(){
        let unlock = wx.ylVideoUnlock(this.custom);
        console.warn("HelloWorld-onVideoUnlockCustoms-是否解锁:",unlock);
    },
    onShowGameOver(isWin){
        let obj = {
                hasWin:isWin,                    
                custom:2,
                golds:1000,
                extra:[
                    {
                        id:0,
                        url:"https://ydhwimg.szvi-bo.com/wx535481e2c3ff79bc/sidebox/11568198322531.jpg",
                        name:"道具补充",
                        getType:"video"
                    },
                    {
                        id:1,
                        url:"https://ydhwimg.szvi-bo.com/wx535481e2c3ff79bc/sidebox/11569753241448.png",
                        name:"钻石奖励",
                        getType:"video"
                    },
                    {
                        id:2,
                        url:"https://ydhwimg.szvi-bo.com/wx535481e2c3ff79bc/sidebox/21568971486805.png",
                        name:"全体回复",
                        getType:"share"
                    }
                ]
            };
        if(!this.nodeYlGameOver) this.nodeYlGameOver = cc.instantiate(this.pbYlGameOver);
        this.nodeYlGameOver.x = 0;
        this.nodeYlGameOver.y = 0;
        this.nodeYlGameOver.parent = this.node;
        this.nodeYlGameOver.getComponent('ylGameOver').showView(obj);//结束对话框
    },
    //切换界面,可调用强制刷新Banner接口
    onChangeView(){
        wx.ylChangeView();
    },
    onShowGridAd(){
        if(this.gridAdIsShow){
            wx.ylHideGridAd();
            this.lbGridAd.string = '格子广告';
            this.gridAdIsShow = false;
        }else{
            var sys = wx.getSystemInfoSync();
            let width  = sys.screenWidth*0.8;
            let adInfo = {
                    show:true,
                    adTheme: 'white',
                    gridCount: 8,
                    style: {
                        left: (sys.screenWidth-width)/2,
                        top: sys.screenHeight/4,
                        width: width,
                        opacity: 0.8
                    }
            }
            wx.ylCreateGridAd(adInfo);
            this.gridAdIsShow = true;
            this.lbGridAd.string = '关闭广告';
        }
    },
    onShowInterstitialAd(){
        wx.ylCreateInterstitialAd(true);
    },
    //积分墙跳转到目标小游戏
    inviteJumpToMinGame(evt){
        let jump = evt.getUserData();
        wx.ylNavigateToMiniProgram(jump,
            function(success){
                if(success){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_REFRESH_SIDEBOX);
                    cc.director.dispatchEvent(evt);
                }else{
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_FULL_BOX);
                    cc.director.dispatchEvent(evt);
                }
        }.bind(this));
    },

    onShowRank(){
        if(!this.nodeRank){
            this.nodeRank = cc.instantiate(this.preRank);
            this.nodeRank.parent = this.node;
        }
        this.nodeRank.getComponent('nodeRank').onShow();
        this.uploadScore();
    },
    //游戏结束
    onOver(){
        this.custom +=1;
        wx.ylOverGame(this.custom);

        let lastCustom = wx.ylGetPlayCustom(1);
        let maxCustom = wx.ylGetPlayCustom(2);
        let watchTvNum = wx.ylGetWatchTvNum();
        let todayPCount = wx.ylGetPlayCount(1);
        let totalPCount = wx.ylGetPlayCount(2);
        console.log("YLSDK onOver-lastCustom,maxCustom,watchTvNum,todayPCount,totalPCount:",lastCustom,maxCustom,watchTvNum,todayPCount,totalPCount);
    },
    //获取深度误触开关
    onGetDeepTouch(){
       let info = wx.ylGetDeepTouch(this.custom);
       console.log("onGetDeepTouch-info: ",JSON.stringify(info));
    },
    //消耗体力
    onConsumePower(){
       let power =  wx.ylGetPower();
       wx.ylSetPower(power -1);
    },
    onAddPower(){
        let power =  wx.ylGetPower();
        wx.ylSetPower(power +1);
    },
    //上传排行榜数据
    uploadScore(){
        wx.setUserCloudStorage({
            KVDataList:[{key:'h_score',value:"200"},{key:'hscore',value:"300"}],
            success(res) {
                console.log("YLSDK setUserCloudStorage-成功！");
            },
            fail(res) {
                console.log("YLSDK setUserCloudStorage-失败！");
            },
        });
    },
    //调起客户端小程序订阅消息界面
    onSubscribeMsg(){
        let tmplIds = [
        'X0kQTtxZAxl44ZU-X6djgJ2jzlbmmonIrd5K8WN-Ei4',
        'Y7w-jbIAo5BGNWAn-uLFBv3L1PrGFn2U24oLgwWgWV8'
        ];
        wx.ylSubscribeMsg(tmplIds,function(status){
            if(status){
                //订阅成功
            }else{
                //订阅失败
            }
        });
    },
    //调起小游戏系统订阅消息界面
    ylSubscribeSysMsg(){
        let msgTypeList = ['SYS_MSG_TYPE_INTERACTIVE'];
        wx.ylSubscribeSysMsg(msgTypeList,function(status){
            if(status){
                //订阅成功
            }else{
                //订阅失败
            }
        });
    },
    _strMapToObj(strMap){
        let obj= Object.create(null);
        for (let[k,v] of strMap) {
          obj[k] = v;
        }
        return obj;
    },
    //测试自定义空间值
    testStoreValue(){
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    },
    test_sv_string(){
        //String
        wx.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                wx.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get",
                        // args:"测试数据"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    },
    test_sv_list(){
        //List
        wx.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        wx.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        wx.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                wx.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all",
                        // args:""
                    },
                    function(status){
                        
                }.bind(this));
                 wx.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        wx.ylStoreValue(
            {
                name:"testList",
                cmd:"size",
                // args:""
            },
            function(status){
                
        }.bind(this));
        wx.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    wx.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size",
                            // args:""
                        },
                        function(status){
                            
                    }.bind(this));
                    wx.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            wx.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all",
                                    // args:""
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    },
    test_sv_set(){
        //Set
        wx.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        wx.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                wx.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                wx.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size",
                        // args:""
                    },
                    function(status){
                        wx.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                wx.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all",
                                        // args:""
                                    },
                                    function(status){
                                        wx.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            wx.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all",
                                                    // args:""
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    },
    test_sv_hash(){
        //litMap
        wx.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                wx.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        wx.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            wx.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            wx.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                                // args:"u_name,sex"
                            },function(status){}.bind(this));
                            wx.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"u_name,sex"
                            },function(status){}.bind(this));
                            wx.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                wx.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                    // args:""
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    },
    test_sv_radom(){
        //testRandom
        wx.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    },
});
