

----- 影流Cocos SDK 示例demo JS版本-----

示例目录说明：
 1.SDK接口文件：  			 build-templates/wechatgame/utils/yl_sdk.js
 2.SDK配置文件：  			 build-templates/wechatgame/utils/yl_sdk_conf.js
 3.初始化等接口调用示例：	 	 assets/Script/demo/HelloWorld.js
 4.组件库目录：  	 assets/Script/lib (组件可以直接复用,也可以自行定义)
 		ylBoxGrid.js         方形头像卖量Item
 		ylBoxItem.js         圆形头像卖量Item
 		ylBoxScrollHori.js   浮动横向滚动卖量组件
 		ylBoxScrollVerti.js  浮动纵向滚动卖量组件
 		ylGameOver.js        游戏结束界面组件
 		ylOverExtraItem.js   游戏结束获取奖励Item组件
 		ylInvite.js          积分墙列表组件
 		ylInviteItem.js      积分墙列表Item组件
 		ylSideBox.js         侧边栏卖量组件
 		ylSideBoxItem.js     侧边栏卖量Item组件
 		ylBoxFull.js     	 全屏卖量Item组件
 		ylBoxFullGridItem.js     全屏卖量Item组件
 		ylBoxFullScrollitem.js   全屏卖量Item组件
 		ylSign.js     		 签到组件
 		ylSignItem.js        签到Item组件
 		ylGetAward.js        获得奖励组件
 		ylBreakEgg.js        砸金蛋组件
 		ylShakeTree.js       摇钱树组件
 		ylOpenBox.js         开宝箱组件

 注意：1.调用接口前，请先初始化SDK
	   2.编译DEMO后请记得在build\wechatgame\game.js文件中添加 require('utils/yl_sdk.js');

SDK接入文档连接：https://shimo.im/docs/PrhhCYWK3kjVwRJp