exports.ylsdk_app_id = "wxd42b44dbd686d382"; //游戏APPID
exports.ylsdk_version = "1.0.8"; 	//游戏版本号
exports.ylsdk_debug_model = true;   //是否debug模式 [true:打印日志,false:不打印日志]
exports.login_by_code = false;      //配置为true，则每次都会走平台登录
exports.ylsdk_pkg_name=""; 			//游戏包名(OPPO、VIVO)
exports.interstitial_first_show = 0; //插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
exports.interstitial_space = 0;	//插屏广告-两次展示之间时间间隔（秒）

exports.side_min_num = 20;		//侧边栏列表item最小保留数(基于曝光策略)
exports.ylsdk_banner_ids = [	//banner广告ID
	'adunit-723102dee1e8ddc3',
	'adunit-fd273f6bd569a89b'
]; 
exports.ylsdk_video_ids = [		//激励视频广告ID
	'adunit-546eabc2b53c133e',
	'adunit-30f1d3be7e328824'
];							  
exports.ylsdk_grid_ids = [		//格子广告ID
	'adunit-8b60da8bb5f96948'
];
exports.ylsdk_interstitial_ids = [	//插屏广告ID
	'adunit-fe07eedeb0218f5c'
];

/****************微信公众平台配置以下合法域名************************/

// 服务器地址：		   https://api.ylxyx.cn
// 图片服务器地址1：	   https://ql.ylxyx.cn
// 图片服务器地址2：     https://tx.ylxyx.cn
// 图片服务器地址3：	   https://ext.ylxyx.cn
// CDN服务器地址：	   https://ydhwimg.szvi-bo.com

/*****************************************************************/
