(function () {
	'use strict';

	var REG = Laya.ClassUtils.regClass;
	var ui;
	(function (ui) {
	    var test;
	    (function (test) {
	        class TestSceneUI extends Laya.Scene {
	            constructor() { super(); }
	            createChildren() {
	                super.createChildren();
	                this.loadScene("test/TestScene");
	            }
	        }
	        test.TestSceneUI = TestSceneUI;
	        REG("ui.test.TestSceneUI", TestSceneUI);
	    })(test = ui.test || (ui.test = {}));
	})(ui || (ui = {}));

	class GameUI extends ui.test.TestSceneUI {
	    constructor() {
	        super();
	        this.shareCount = 0;
	        this.custom = 0;
	        this.layerList = null;
	        this.layerId = 0;
	        this.custom = 0;
	        GameUI.instance = this;
	        Laya.MouseManager.multiTouchEnabled = false;
	    }
	    onEnable() {
	        wx.ylGameLifeListen(function (type, res) {
	            if (type === 'onShow') {
	                console.warn("GameUI-onShow--res:", res);
	            }
	            else if (type === 'onHide') {
	                console.warn("GameUI-onHide");
	            }
	            else if (type === 'onError') {
	                console.warn("GameUI-onError--res:", res);
	            }
	        });
	        wx.ylOnPowerChange(function (info) {
	            console.log("GameUI-ylOnPowerChange-info:", JSON.stringify(info));
	        });
	        wx.ylInitSDK(function (success) {
	            if (success === true) {
	                console.warn("GameUI-ylInitSDK-初始化成功");
	                this.doSDKinterface();
	            }
	            else if (success == 'config_success' || success == 'config_fail') {
	                console.warn("GameUI-ylInitSDK-获取游戏配置回调");
	                wx.ylGetPowerInfo(function (data) {
	                    console.log("GameUI-ylGetPowerInfo:", data);
	                });
	            }
	            else {
	                console.warn("GameUI-ylInitSDK-初始化失败");
	            }
	        }.bind(this));
	        this.initUI();
	    }
	    initUI() {
	        this.btnShowShareOrVideo.on(Laya.Event.CLICK, this, this.onShowShareOrVideo);
	        this.btnShowBanner.on(Laya.Event.CLICK, this, this.onShowBanner);
	        this.btnHideBanner.on(Laya.Event.CLICK, this, this.onHideBanner);
	        this.btnShowInterstitial.on(Laya.Event.CLICK, this, this.onShowInterstitial);
	        this.btnSubscribeSystemMessage.on(Laya.Event.CLICK, this, this.onSubscribeSystemMessage);
	        this.btnOver.on(Laya.Event.CLICK, this, this.onOver);
	        this.btnDeepTouch.on(Laya.Event.CLICK, this, this.onDeepTouch);
	        this.btnPowerAdd.on(Laya.Event.CLICK, this, this.onPowerAdd);
	        this.btnPowerReduce.on(Laya.Event.CLICK, this, this.onPowerReduce);
	    }
	    doSDKinterface() {
	        console.log("---调用接口---");
	        wx.ylSideBox(function (data) {
	        }.bind(this));
	        wx.ylGetCustom(function (data) {
	        }.bind(this));
	        wx.ylStatisticResult({ "total_score": 123, "rebirth_score": 123 }, function (status) {
	            if (status) {
	            }
	        }.bind(this));
	        let userWXinfo = wx.ylGetUserWXInfo();
	        if (!userWXinfo) {
	            wx.ylUserInfoButtonShow({
	                type: 'image',
	                image: './images/sp_btn_start.png',
	                style: {
	                    left: 100,
	                    top: 360,
	                    width: 150,
	                    height: 40,
	                    lineHeight: 40,
	                    borderRadius: 4
	                }
	            }, function (data) {
	                if (data) {
	                    userWXinfo = data;
	                    wx.ylUserInfoButtonHide();
	                }
	            }.bind(this));
	        }
	        let loginInfo = wx.ylGetUserInfo();
	        console.log("---登录信息-loginInfo:", JSON.stringify(loginInfo));
	        var that = this;
	        wx.ylShareCard(function (shareInfo) {
	            if (shareInfo) {
	                console.log("----获取分享图列表:", JSON.stringify(shareInfo));
	            }
	            else {
	            }
	        }.bind(this), null);
	        wx.ylStatisticViedo(0, 'adunit-6878a73e134f85e2', null);
	        wx.ylStatisticShareCard(177);
	        wx.ylGetSetting(function (res) {
	            console.log("YLSDK getSetting-res:", JSON.stringify(res));
	            if (res) {
	            }
	            else {
	            }
	        }, true);
	        wx.ylGetLayerList(function (data) {
	            if (data && data.length > 0) {
	                this.layerList = data;
	                wx.ylStatisticLayer(this.layerList[this.layerList.length - 1].layerPath);
	            }
	        }.bind(this));
	    }
	    onOver() {
	        this.custom = this.custom + 1;
	        wx.ylOverGame(this.custom);
	        let lastCustom = wx.ylGetPlayCustom(1);
	        let maxCustom = wx.ylGetPlayCustom(2);
	        let watchTvNum = wx.ylGetWatchTvNum();
	        let todayPCount = wx.ylGetPlayCount(1);
	        let totalPCount = wx.ylGetPlayCount(2);
	        console.log("YLSDK onOver-lastCustom,maxCustom,watchTvNum,todayPCount,totalPCount:", lastCustom, maxCustom, watchTvNum, todayPCount, totalPCount);
	        if (this.layerList && this.layerList.length > 0) {
	            let layer = this.layerList[this.layerId];
	            let _layerPath = layer.layerPath || 'default';
	            wx.ylStatisticLayer(_layerPath);
	            this.layerId += 1;
	            this.layerId = (this.layerId >= this.layerList.length) ? 0 : this.layerId;
	        }
	    }
	    onDeepTouch() {
	        let deepTouch = wx.ylGetDeepTouch(this.custom);
	        console.log("YLSDK onDeepTouch-deepTouch:", deepTouch);
	    }
	    onShareClick(e) {
	        console.error("-----分享点击-----");
	        let shareInfo = {
	            channel: 'aa',
	            module: 'bb',
	            scene: 'MenuScene',
	            inviteType: 'award_id',
	        };
	        wx.ylShareAppMessage(function (sResult) {
	            if (sResult.sSuccess) {
	                let result = wx.ylRewardByVideoOrShare(false);
	                let slLimit = wx.ylGetVSLimit();
	                console.log("---------onShareClick-result:", result, slLimit);
	            }
	            console.log("-onShareClick--------分享结果-sResult:", sResult);
	        }, shareInfo);
	    }
	    onShowBanner(e) {
	        console.log("GameUI-onShowBanner:");
	        wx.ylBannerAdCreateByStyle({
	            left: 20,
	            top: 100,
	            width: 310,
	        }, true, (res) => {
	        }, true, (res) => {
	            console.warn("GameUI-ylBannerAdCreateByStyle-onResize-back:", JSON.stringify(res));
	        });
	        wx.ylChangeBannerStyle({
	            left: 0,
	            top: 350,
	            height: 30,
	        });
	    }
	    onHideBanner(e) {
	        console.log("GameUI-onHideBanner");
	        wx.ylBannerAdHide();
	    }
	    onShowInterstitial(e) {
	        console.log("GameUI-onShowInterstitial");
	        wx.ylCreateInterstitialAd(true, function (type) {
	            switch (type) {
	                case 0:
	                    console.log("GameUI-激励视频-创建或展示失败");
	                    break;
	                case 1:
	                    console.log("GameUI-激励视频-创建或展示成功");
	                    break;
	                case 2:
	                    console.log("GameUI-激励视频-关闭");
	                    break;
	            }
	        });
	    }
	    onSubscribeSystemMessage(e) {
	        console.log("GameUI-onSubscribeSystemMessage");
	        let msgTypeList = ['SYS_MSG_TYPE_INTERACTIVE', 'SYS_MSG_TYPE_RANK'];
	        wx.ylSubscribeSysMsg(msgTypeList, function (res) {
	            if (res) {
	                console.log("YLSDK ylSubscribeSysMsg-success-res:", JSON.stringify(res));
	            }
	            else {
	            }
	        });
	    }
	    onPowerAdd(e) {
	        let power = wx.ylGetPower();
	        wx.ylSetPower(power + 1);
	    }
	    onPowerReduce(e) {
	        let power = wx.ylGetPower();
	        wx.ylSetPower(power - 1);
	    }
	    onShowShareOrVideo() {
	        wx.ylShowShareOrVideo("bb", "aa", function (type) {
	            switch (type) {
	                case 0:
	                    console.warn("------策略-无");
	                    break;
	                case 1:
	                    console.warn("------策略-分享");
	                    this.shareCount += 1;
	                    let _channel = (this.shareCount % 2 == 0) ? 'bb' : 'cc';
	                    let _module = (this.shareCount % 2 == 0) ? 'aa' : 'cc';
	                    let shareInfo = {
	                        channel: _channel,
	                        module: _module,
	                        scene: 'MenuScene',
	                        inviteType: 'award_id',
	                    };
	                    wx.ylShareAppMessage(function (sResult) {
	                        if (sResult.sSuccess) {
	                            let result = wx.ylRewardByVideoOrShare(false);
	                            let slLimit = wx.ylGetVSLimit();
	                            console.log("---------ylRewardByVideoOrShare-result:", result, slLimit);
	                        }
	                        console.log("---------分享结果-sResult:", sResult);
	                    }, shareInfo);
	                    break;
	                case 2:
	                    console.warn("------策略-视频");
	                    break;
	            }
	        }.bind(this));
	    }
	    testStoreValue() {
	        this.test_sv_string();
	        this.test_sv_list();
	        this.test_sv_set();
	        this.test_sv_hash();
	        this.test_sv_radom();
	    }
	    test_sv_string() {
	        wx.ylStoreValue({
	            name: "testString",
	            cmd: "set",
	            args: "测试数据"
	        }, function (status) {
	            wx.ylStoreValue({
	                name: "testString",
	                cmd: "get"
	            }, function (status) {
	            }.bind(this));
	        }.bind(this));
	    }
	    test_sv_list() {
	        wx.ylStoreValue({
	            name: "testList",
	            cmd: "add",
	            args: "0"
	        }, function (status) {
	        }.bind(this));
	        wx.ylStoreValue({
	            name: "testList",
	            cmd: "add",
	            args: "2"
	        }, function (status) {
	        }.bind(this));
	        wx.ylStoreValue({
	            name: "testList",
	            cmd: "set",
	            args: "0,3"
	        }, function (status) {
	            wx.ylStoreValue({
	                name: "testList",
	                cmd: "all"
	            }, function (status) {
	            }.bind(this));
	            wx.ylStoreValue({
	                name: "testList",
	                cmd: "get",
	                args: "0"
	            }, function (status) {
	            }.bind(this));
	            wx.ylStoreValue({
	                name: "testList",
	                cmd: "size"
	            }, function (status) {
	            }.bind(this));
	            wx.ylStoreValue({
	                name: "testList",
	                cmd: "poll",
	                args: "2"
	            }, function (status) {
	                wx.ylStoreValue({
	                    name: "testList",
	                    cmd: "size"
	                }, function (status) {
	                }.bind(this));
	                wx.ylStoreValue({
	                    name: "testList",
	                    cmd: "replace",
	                    args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
	                }, function (status) {
	                    wx.ylStoreValue({
	                        name: "testList",
	                        cmd: "all"
	                    }, function (status) {
	                    }.bind(this));
	                }.bind(this));
	            }.bind(this));
	        }.bind(this));
	    }
	    test_sv_set() {
	        wx.ylStoreValue({
	            name: "testSet",
	            cmd: "add",
	            args: "12"
	        }, function (status) {
	        }.bind(this));
	        wx.ylStoreValue({
	            name: "testSet",
	            cmd: "add",
	            args: "10"
	        }, function (status) {
	            wx.ylStoreValue({
	                name: "testSet",
	                cmd: "exist",
	                args: "10"
	            }, function (status) {
	            }.bind(this));
	            wx.ylStoreValue({
	                name: "testSet",
	                cmd: "size"
	            }, function (status) {
	                wx.ylStoreValue({
	                    name: "testSet",
	                    cmd: "del",
	                    args: "10"
	                }, function (status) {
	                    wx.ylStoreValue({
	                        name: "testSet",
	                        cmd: "all"
	                    }, function (status) {
	                        wx.ylStoreValue({
	                            name: "testSet",
	                            cmd: "replace",
	                            args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
	                        }, function (status) {
	                            wx.ylStoreValue({
	                                name: "testSet",
	                                cmd: "all"
	                            }, function (status) {
	                            }.bind(this));
	                        }.bind(this));
	                    }.bind(this));
	                }.bind(this));
	            }.bind(this));
	        }.bind(this));
	    }
	    test_sv_hash() {
	        wx.ylStoreValue({
	            name: "testHash",
	            cmd: "set",
	            args: "u_name,许"
	        }, function (status) {
	            wx.ylStoreValue({
	                name: "testHash",
	                cmd: "get",
	                args: "u_name"
	            }, function (status) {
	                wx.ylStoreValue({
	                    name: "testHash",
	                    cmd: "replace",
	                    args: "{\"u_name\":\"唐\",\"sex\":\"women\"}"
	                }, function (status) {
	                    wx.ylStoreValue({
	                        name: "testHash",
	                        cmd: "gets",
	                        args: "u_name,sex"
	                    }, function (status) {
	                    }.bind(this));
	                    wx.ylStoreValue({
	                        name: "testHash",
	                        cmd: "size",
	                    }, function (status) {
	                    }.bind(this));
	                    wx.ylStoreValue({
	                        name: "testHash",
	                        cmd: "values",
	                        args: "sex"
	                    }, function (status) {
	                    }.bind(this));
	                    wx.ylStoreValue({
	                        name: "testHash",
	                        cmd: "del",
	                        args: "u_name"
	                    }, function (status) {
	                        wx.ylStoreValue({
	                            name: "testHash",
	                            cmd: "all",
	                        }, function (status) {
	                        }.bind(this));
	                    }.bind(this));
	                }.bind(this));
	            }.bind(this));
	        }.bind(this));
	    }
	    test_sv_radom() {
	        wx.ylStoreValue({
	            name: "testRandom"
	        }, function (status) {
	        }.bind(this));
	    }
	}

	class GameConfig {
	    constructor() {
	    }
	    static init() {
	        var reg = Laya.ClassUtils.regClass;
	        reg("script/GameUI.ts", GameUI);
	    }
	}
	GameConfig.width = 640;
	GameConfig.height = 1136;
	GameConfig.scaleMode = "fixedwidth";
	GameConfig.screenMode = "none";
	GameConfig.alignV = "top";
	GameConfig.alignH = "left";
	GameConfig.startScene = "test/TestScene.scene";
	GameConfig.sceneRoot = "";
	GameConfig.debug = false;
	GameConfig.stat = false;
	GameConfig.physicsDebug = false;
	GameConfig.exportSceneToJson = true;
	GameConfig.init();

	class Main {
	    constructor() {
	        if (window["Laya3D"])
	            Laya3D.init(GameConfig.width, GameConfig.height);
	        else
	            Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
	        Laya["Physics"] && Laya["Physics"].enable();
	        Laya["DebugPanel"] && Laya["DebugPanel"].enable();
	        Laya.stage.scaleMode = GameConfig.scaleMode;
	        Laya.stage.screenMode = GameConfig.screenMode;
	        Laya.stage.alignV = GameConfig.alignV;
	        Laya.stage.alignH = GameConfig.alignH;
	        Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
	        if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
	            Laya.enableDebugPanel();
	        if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
	            Laya["PhysicsDebugDraw"].enable();
	        if (GameConfig.stat)
	            Laya.Stat.show();
	        Laya.alertGlobalError = true;
	        Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
	    }
	    onVersionLoaded() {
	        Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
	    }
	    onConfigLoaded() {
	        GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
	    }
	}
	new Main();

}());
