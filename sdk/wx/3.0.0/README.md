1.文件介绍：
	1.1 SDK文件：ydhw.wx.sdk.min.js
	1.2 SDK配置文件：config.js  (强调:config.js 必须在引擎支持之后如微信平台 必须在引入 weapp-adapter.js之后才可以
	1.3 .d.ts提示文件：ydhw.interface.d.ts
	1.4 枚举和接口实现文件：ydhw.sdk.ts

2.引入步骤:
	2.1 拷贝ydhw.wx.sdk.min.js放到发布根目录下
    2.2 将config.js拷贝到index.js文件的最上方，并修改ydhw.wx.sdk.min.js加载路径和游戏参数配置
    2.3 接口的解释和参数请查看ydhw.interface.d.ts文件
	2.4 接口中用到的枚举和类ydhw.sdk.ts

3.记得添加以下合法域名：
	- 服务器地址：		     https://api.ylxyx.cn
	- 图片服务器地址1：	 Https://cdn2.ydhaowan.com
	- 图片服务器地址2：	 https://cdn3.ydhaowan.com 
	- 图片服务器地址3：	 https://cdn4.ydhaowan.com
	- 图片服务器地址4：	 https:// cdn5.ydhaowan.com
	- CDN1(OSS客户端上传)：     Https://cdn1.ydhaowan.com   
	- CDN2(OSS客户端上传)：     https://cdn6.ydhaowan.com



4.针最近游戏在微信官方过审被拒绝,对因为使用SDK代码导致代码和线上游戏有重复的情况,可以使用以下方式重新提审:
	1>将ydhw.wx.sdk.min.js的内容拷贝到laya.core.js文件的头部。
	2>去掉index.js中loadLib("tt/ydhw.tt.sdk.min.js");
	3>一定要保证config.js放在loadLib("libs/laya.core.js")的前面
