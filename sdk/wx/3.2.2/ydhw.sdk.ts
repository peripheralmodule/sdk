//@namespace=YDHW
export namespace YDHW {

    /**
     * 平台ID
     * 0:微信,
     * 1:QQ,
     * 2:OPPO,
     * 3:VIVO,
     * 4:字节跳动,
     * 5:百度,
     * 6:4399平台,
     * 7:趣头条,
     * 8:360平台,
     * 9:陌陌,
     * 10:影流小游戏,
     * 12:魅族,
     * 13:UC平台
     * 15：快手
     * 
     * 接口ID
     * 10000:公共,
     * 20000:微信,
     * 30000:QQ,
     * 40000:OPPO,
     * 50000:VIVO,
     * 60000:字节跳动,
     * 70000:百度,
     * 80000:4399平台,
     * 90000:趣头条,
     * 100000:360平台,
     * 110000:陌陌,
     * 120000:影流小游戏,
     * 140000:魅族,
     * 150000:快手,
     * 
     */
    export enum YDHW_WECHAT_API {
        /**
         * 显示格子广告
         * 
       *  @param c               上下文
       *  @param Index           广告列表里的位置
       *  @param Style           广告样式  按官方的数据来
       *  @param onLoad          加载回调
       *  @param onError         失败回调
       *  @param isAlignCenter   是否居中对其
       *  @param count           显示加载数量
       *  @param adTheme         主题
       *  @param adIntervals     刷新间隔
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.ShowGridAd,c:any,Index: number, adid: string, Style: IYDHW.IGirdAdStyle, onLoad?:any,onError?:any,isAlignCenter?:any, count?:any, adTheme ?:any, adIntervals?:any): void;
         */
         ShowGridAd   = 20001,
        /**
         * 隐藏格子广告
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.HideGridAd,IsDesTroy, ...index): void;
         */
         HideGridAd = 20002,


        /**
         * 系统订阅消息
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.SubscribeSysMsg,msgTypeList: string[], onSuccess: (result: any) => void, onError: (error: any) => void): void;
         */
        SubscribeSysMsg = 20004,
        /**
         * 获取用户的当前设置
         * @param isSubcribe 是否同时获取用户订阅消息的订阅状态
         * @param onSuccess 
         * @param onError 
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.GetSetting,isSubcribe: boolean, onSuccess: (result: any) => void, onError: (error: any) => void): void;
         */
        GetSetting = 20005,/**获取用户的当前设置 */
        /**
         * 取消监听录制事件。当对应事件触发时，该回调函数不再执行
         * 
         * @param event 事件名
         * @param c 
         * @param callBack 
         *  ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderOff,event:IYDHW.Wechat.EM_RECORD_EVENT,c?: any, callBack?: (res:any) => void):void;
         */
        WXRecorderOff = 20006,
        /**
         * 注册监听录制事件的回调函数。当对应事件触发时，回调函数会被执行
         * 
         * @param event  事件名
         * @param c 
         * @param callBack 
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderOn,event:IYDHW.Wechat.EM_RECORD_EVENT,c?: any, callBack?: (res:any) => void):void;
         */
        WXRecorderOn = 20007,
        /**
         * 开始录制游戏画面
         * 
         * @param info 创建分享按钮传参
         * @param c 
         * @param onStart 录制事件监听-开始
         * @param onStop 录制事件监听-结束
         * @param onError 录制事件监听-错误
         * @param onPause 录制事件监听-暂停
         * @param onResume 录制事件监听-恢复
         * @param onAbort 录制事件监听-取消
         * @param onTimeUpdate  录制时间更新事件监听，在录制过程中触发该事件(如果没有回调可能是录屏)
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderStart,info:IYDHW.Wechat.IGameRecorderInfo,c: any, onStart: (res: any) => void, onStop: (res: any) => void, 
                onError?: (error: any) => void, onPause?: (res: any) => void, onResume?: (res: any) => void, onAbort?: (res: any) => void, onTimeUpdate?: (res: any) => void):void;
         */
        WXRecorderStart = 20008,
        /**
         * 结束录制游戏画面。结束录制后可以发起分享。
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderStop):void;
         */
        WXRecorderStop = 20009,
        /**
         * 暂停录制游戏画面。
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderPause):void;
         */
        WXRecorderPause = 20010,
        /**
         * 恢复录制游戏画面
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderResume):void;
         */
        WXRecorderResume = 20011,
        /**
         * 放弃录制游戏画面。此时已经录制的内容会被丢弃
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderAbort):void;
         */
        WXRecorderAbort = 20012,
        /**
         * 获取是否支持调节录制视频的播放速率
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.IsAtempoSupported):boolean;
         */
        IsAtempoSupported = 20013,
        /**
         * 获取是否支持录制游戏画面
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.IsFrameSupported):boolean;
         */
        IsFrameSupported = 20014,
        /**
         * 获取是否在录制游戏画面的同时支持录制游戏音频的信息
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.IsSoundSupported):boolean;
         */
        IsSoundSupported = 20015,
        /**
         * 获取是否支持调节录制视频的音量
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.IsVolumeSupported):boolean;
         */
        IsVolumeSupported = 20016,
        /**
         * 创建游戏对局回放分享按钮，返回一个单例对象
         * 按钮在被用户点击后会发起对最近一次录制完成的游戏对局回放的分享
         * @param btnInfo 按钮信息(后面补充类型说明)
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.CreateRecorderShareButton,btnInfo:IYDHW.Wechat.IGameRecorderBtnInfo):void;
         */
        CreateRecorderShareButton = 20017,
        /**
         * 隐藏游戏对局回放分享按钮
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.RecorderShareButtonHide):void;
         */
        RecorderShareButtonHide = 20018,
        /**
         * 显示游戏对局回放分享按钮
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.RecorderShareButtonShow):void;
         */
        RecorderShareButtonShow = 20019,
        /**
         * 监听游戏对局回放分享按钮的点击事件
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.RecorderShareButtonOnTap,c: any, m: (res: any) => void):void;
         */
        RecorderShareButtonOnTap = 20020,
        /**
         * 取消监听游戏对局回放分享按钮的点击事件
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.RecorderShareButtonOffTap,c: any, m: (res: any) => void):void;
         */
        RecorderShareButtonOffTap = 20021,
        /**
         * 设置 wx.shareMessageToFriend 接口 query 字段的值
         * 
         * @param shareMessageToFriendScene 需要传递的代表场景的数字，需要在 0 - 50 之间 (场景值CP自己在该范围定义)
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.SetMessageToFriendQuery,shareMessageToFriendScene:number):boolean;
         */
        SetMessageToFriendQuery = 20022,
        /**
        * 监听主域接收 wx.shareMessageToFriend 接口的成功失败通知
        * @param c 
        * @param m 
        * ydhw.Invoke(YDHW.YDHW_WECHAT_API.OnShareMessageToFriend,c: any, m: (res: any) => void):void;
        */
        OnShareMessageToFriend = 20023,
        /**
         * 绑定朋友圈分享参数
         * @param shareInfo 分享参数
         * @param c 
         * @param m 
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.OnShareTimeline,shareInfo:IYDHW.Wechat.IOnShareTimelineInfo,c: any, m: (res: any) => void):void;
         */
        OnShareTimeline = 20024,
        /**
         * 取消绑定分享参数
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.OffShareTimeline):void;
         */
        OffShareTimeline = 20025,


       /**
         * 显示原生广告
         * 
       *  @param c               上下文
       *  @param Index           广告列表里的位置
       *  @param Style           广告样式  按官方的数据来
       *  @param onLoad          加载回调
       *  @param onError         失败回调
       *  @param isAlignCenter   是否居中对其
       *  @param count           显示加载数量
       *  @param adTheme         主题
       *  @param adIntervals     刷新间隔
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.ShowCustomAd,c:any,Index: number, Style: IYDHW.IGirdAdStyle,onLoad,onError,isAlignCenter?:any, count?:any, adTheme ?:any, adIntervals?:any): void;
         */
        ShowCustomAd   = 20026,
        /**
         * 隐藏格子广告
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.HideCustomAd,): void;
         */
         HideCustomAd = 20027,
        /**
       * 一次性订阅模板
       * @param tmplIds 模板
       * @param allTmplIdNames  本地存储的订阅名称列表 一般 mgs1  mgs2  
       * @param onSuccess 成功
       * @param onError 失败
       * ydhw.Invoke(YDHW.YDHW_WECHAT_API.SubMsg,tmplIds: string[],allTmplIdNames:string[],onSuccess: (result: any) => void, onError: (error: any) => void): void;
       */
        SubMsg = 20030,

        /**
       * 取消支付
       * @param orderNo 订单编号 
       * @param onSuccess 成功
       * @param onError 失败
       * ydhw.Invoke(YDHW.YDHW_WECHAT_API.CancelPay,this,orderNo: string,onSuccess: (result: any) => void, onError: (error: any) => void): void;
       */
        CancelPay = 20031,
        /**
     * 游戏币赠送
     * @param amount 数量 
     * @param onSuccess 成功
     * @param onError 失败
     * ydhw.Invoke(YDHW.YDHW_WECHAT_API.GivinglGameCurrency,this,amount: string,onSuccess: (result: any) => void, onError: (error: any) => void): void;
     */
        GivinglGameCurrency = 20032,
        /**
       * 游戏币使用
       * @param amount 数量 
       * @param onSuccess 成功
       * @param onError 失败
       * ydhw.Invoke(YDHW.YDHW_WECHAT_API.GameCurrencyPay,this,amount: string,onSuccess: (result: any) => void, onError: (error: any) => void): void;
       */
        GameCurrencyPay = 20033,
        /**
     * 获取用户信息
     * @param onSuccess 成功
     * @param onError 失败
     * ydhw.Invoke(YDHW.YDHW_WECHAT_API.GetServerUserInfo,this,onSuccess: (result: any) => void, onError: (error: any) => void): void;
     */
        GetServerUserInfo = 20034,

        /**
         *发起米大师支付 
         * @param Object 支付参数
         * @param onSuccess 成功
         * ydhw.Invoke(YDHW.YDHW_WECHAT_API.RequestMidasPayment,this,Object:IYDHW.Wechat.IRequestMidasFriendPayment): void;
         */
        RequestMidasPayment = 20035,


    }

    export enum YDHW_QQ_API {
        /**
         * 分享(邀请类模板)
         * 
         * @param shareInfo 分享参数
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.ShareByTemplateId,shareInfo: IShareTempletInfo, caller: any, method: (result: ITempShareResult) => void): void;
         */
        ShareByTemplateId = 30001,

        /**
         * 添加彩签
         * 
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.AddColorSign,caller: any, method: (success: boolean) => void): void;
         */
        AddColorSign = 30002,

        /**
         * 主动订阅
         * 
         * @param appMsgInfo 订阅参数
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.SubscribeAppMsg,appMsgInfo:IAppMsgInfo,caller: any, method: (success: boolean) => void): void;
         */
        SubscribeAppMsg = 30003,


        /**
         * 创建-打开添加好友页面的按钮
         * 
         * @param btnInfo 
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.CreateAddFriendButton,btnInfo: IAddFriendButtonInfo,caller: any, method: (success: boolean) => void): void;
         */
        CreateAddFriendButton = 30004,


        /**
         * 显示-打开添加好友页面的按钮
         * 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.ShowAddFriendButton): void;
         */
        ShowAddFriendButton = 30005,


        /**
         * 隐藏-打开添加好友页面的按钮
         * 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.HideAddFriendButton): void;
         */
        HideAddFriendButton = 30006,


        /**
         * 销毁-打开添加好友页面的按钮
         * 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.DestroyAddFriendButton): void;
         */
        DestroyAddFriendButton = 30007,


        /**
         * 创建-盒子广告
         * 
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.CreateAppBox,caller: any, method: (status: EM_APP_BOX_TYPE) => void): void;
         */
        CreateAppBox = 30008,


        /**
         * 显示-盒子广告
         * @param caller 
         * @param onSuccess 
         * @param onError 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.ShowAppBox,caller:any ,onSuccess: () => void, onError: (error: any) => void): void;
         */
        ShowAppBox = 30009,


        /**
         * 创建-积木广告
         * 
         * @param adInfo 积木广告参数
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.CreateBlockAd,adInfo:IBlockAdInfo,caller: any, method: (status: EM_BLOCK_TYPE) => void,onResize?:(data:any) => void): void; 
         */
        CreateBlockAd = 30010,


        /**
         * 显示-积木广告
         * 
         * @param caller 
         * @param show 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.ShowBlockAd,caller: any, show: () => void): void;
         */
        ShowBlockAd = 30011,


        /**
         * 隐藏-积木广告
         * 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.HideBlockAd): void;
         */
        HideBlockAd = 30012,


        /**
         * 销毁-积木广告
         * 
         * ydhw.Invoke(YDHW.YDHW_QQ_API.DestroyBlockAd): void;
         */
        DestroyBlockAd = 30013,

    }

    export enum YDHW_TT_API {
        /**
         * 图片分享(含模板)
         * 
         * @param scene   场景名称 (从ShareCard接口获得)
         * @param onSuccess 
         * @param description 分享文案,不超过 28 个中文字符
         * ydhw.Invoke(YDHW.YDHW_TT_API.ShareImage,scene: string, onSuccess: (isOk: any) => void, description?: string);
         */
        ShareImage = 40001,

        /**
         * 视频分享(含模板)
         * 
         * @param title 分享标题,不超过 14 个中文字符
         * @param description 分享文案,不超过 28 个中文字符
         * @param query 查询字符串，必须是 key1=val1&key2=val2 的格式。从这条转发消息进入后，可通过 tt.getLaunchOptionSync() 或 tt.onShow() 获取启动参数中的 query。
         * @param onSuccess 
         * ydhw.Invoke(YDHW.YDHW_TT_API.ShareVideo,title: string, description: string, query: string, onSuccess: (isOk: any) => void): void;
         */
        ShareVideo = 40002,


        /**
         * 模板分享
         * 
         * @param onSuccess 
         * ydhw.Invoke(YDHW.YDHW_TT_API.ShareTemplate,onSuccess: (isOk: any) => void): void;
         */
        ShareTemplate = 40003,

        /**
         * Token分享(含模板)
         * 
         * @param onSuccess 
         * ydhw.Invoke(YDHW.YDHW_TT_API.ShareToken,onSuccess: (isOk: any) => void): void;
         */
        ShareToken = 40004,

        /**
         * 显示更多游戏
         * @param onEvtOpen 展示结果
         * @param sideInfo  卖量统计参数,从侧边栏列表获取(不需要做卖量打点的游戏不需要)
         * ydhw.Invoke(YDHW.YDHW_TT_API.ShowMoreGamesModal,onEvtOpen: (isOk: boolean) => void,sideInfo?:IYDHW.TT.ISideBoxInfo): void;
         */
        ShowMoreGamesModal = 40005,

        /**
         * 创建更多游戏按钮
         * @param type 按钮的类型，取值 image 或 text。image 对应图片按钮，text 对应文本按钮
         * @param imageUrl 按钮的背景图片，type 为 image 时必填。
         * @param style 按钮的样式
         * @param caller 
         * @param onMoreGame 
         * @param onTip 
         * @param sideInfo 卖量统计参数,从侧边栏列表获取(不需要做卖量打点的游戏不需要)
         * ydhw.Invoke(YDHW.YDHW_TT_API.CreateMoreGamesButton,type: string, imageUrl: string, style: IYDHW.TT.IMoreGamesButtonStyle, caller: any, onMoreGame: (isOk: boolean) => void, onTip: () => void,sideInfo?:IYDHW.TT.ISideBoxInfo): void;
         */
        CreateMoreGamesButton = 40006,

        /**
         *  开始录屏
         * 
         * @param duration 录屏的时长，单位 s，必须 >3 &&  <= 300s（5 分钟）
         * @param onStart  监听录屏开始事件
         * @param onStop   录屏停止监听,回调参数videoPath：视频保存地址
         * @param onResume 暂停录屏
         * @param onPause  继续录屏
         * @param onError  监听录屏错误事件
         * @param onInterruptionBegin 监听录屏中断开始
         * @param onInterruptionEnd 监听录屏中断结束
         * ydhw.Invoke(YDHW.YDHW_TT_API.RecorderStart,duration: number, onStart: (result: any) => void,onStop: (videoPath: string) => void, 
                        onResume?: (result: any) => void,onPause?: (result: any) => void,
                        onError?: (error: any) => void, onInterruptionBegin?: () => void, onInterruptionEnd?: () => void): void;
         */
        RecorderStart = 40007,

        /**
         *  停止录屏
         * 
         * ydhw.Invoke(YDHW.YDHW_TT_API.RecorderStop): void;
         */
        RecorderStop = 40008,

        /**
         * 暂停录屏
         * 
         * ydhw.Invoke(YDHW.YDHW_TT_API.RecorderPause): void;
         */
        RecorderPause = 40009,

        /**
         * 继续录屏
         * 
         * ydhw.Invoke(YDHW.YDHW_TT_API.RecorderResume): void;
         */
        RecorderResume = 40010,

        /**
         * 获取AccountToken
         * ydhw.Invoke(YDHW.YDHW_TT_API.GetAccountToken): void;
         */
        GetAccountToken = 40011,

        /**
         * 获取视频排行榜列表
         * ydhw.Invoke(YDHW.YDHW_TT_API.GetVideoInfo,this,videoTag,method:(res)=>void,numberTop?: number): void;
         */
        GetVideoInfo = 40012,

        /**
         * 视频跳转
         * ydhw.Invoke(YDHW.YDHW_TT_API.NavigateToVideoView,this,videoid,success:(res)=>void,fail:(res)=>void): void;
         */
        NavigateToVideoView = 40013

    }


    export enum YDHW_VIVO_API {
        /**
         * 判断用户是否通过桌面图标来启动应用
         * @param caller 
         * @param onSuccess 
         * @param onFail 
         * ydhw.Invoke(YDHW.YDHW_VIVO_API.IsStartupByShortcut,caller:any , onSuccess:(status:boolean)=>void,onFail?: (error: any) => void):void;
         */
        IsStartupByShortcut = 50001,

        /**
         * 上传用户分数
         * @param caller 
         * @param score  分数
         * @param onSuccess 
         * @param onFail 
         *  ydhw.Invoke(YDHW.YDHW_VIVO_API.RreportUserScore,caller:any,score:number,onSuccess:(status:boolean)=>void,onFail?: (error: any) => void):void;
         */
        RreportUserScore = 50002,

        /**
         * 请求用户排行榜信息
         * @param caller 
         * @param onSuccess 
         * @param onFail 
         *  ydhw.Invoke(YDHW.YDHW_VIVO_API.GetUserRankInfo,caller:any ,onSuccess:(status:boolean)=>void,onFail?: (error: any) => void):void;
         */
        GetUserRankInfo = 50003,

        /**
         * 获取游戏总排行榜
         * @param caller 
         * @param dimension dimension共有四种取值，'1','2','3','4'，分别代表日榜、周榜、月榜、永久榜。如果不传递该值，则默认返回永久榜。
         * @param onSuccess 
         * @param onFail 
         *  ydhw.Invoke(YDHW.YDHW_VIVO_API.GetUserRankList,caller:any,dimension:string,onSuccess:(status:boolean)=>void,onFail?: (error: any) => void):void;
         */
        GetUserRankList = 50004,

        /**
        * 分享
        * @param caller 
        * @param success
        * @param fail 
        * @param cancel 
        *  ydhw.Invoke(YDHW.YDHW_VIVO_API.VShare,caller:any,success:()=>void,fail:(erro)=>void,cancel:() => void):void;
        */
        VShare = 50005,

    }


    export enum YDHW_OPPO_API {
        /**创建互推盒子Banner
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_OPPO_API.CreateGameBannerAd,caller:any ,method:(isTrue:boolean)=>void):void;
         */
        CreateGameBannerAd = 60001,

        /**显示互推盒子Banner
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_OPPO_API.ShowGameBannerAd,caller:any ,method:(isTrue:boolean)=>void):void;
         */
        HideGameBannerAd = 60002,

        /**隐藏互推盒子Banner
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_OPPO_API.HideGameBannerAd,caller:any ,method:(isTrue:boolean)=>void):void;
         */
        ShowGameBannerAd = 60003,
        /**创建互推盒子9宫格
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_OPPO_API.CreateGamePortalAd,caller:any ,method:(isTrue:boolean)=>void):void;
         */
        CreateGamePortalAd = 60004,

        /**显示互推盒子9宫格
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_OPPO_API.ShowGamePortalAd,caller:any ,isShow?:(show:boolean)=>void,isClose?:(close:boolean)=>void):void;
         */
        ShowGamePortalAd = 60005,


        /**
         * 获取订单签名 传递需要签名的参数
         * ydhw.Invoke(YDHW.YDHW_OPPO_API.GetPaySign,caller:any,custom:IYDHW.IOPOrderInfo,method:(res)=>void):void;
         */
        GetOrderSign = 60006, //直接忽略 无需调用


        /**
         * 请求后台支付结果
         *  @param caller 上下文
         *  @param orderId 后台生成的支付订单编号
         * ydhw.Invoke(YDHW.YDHW_OPPO_API.GetPayResult,caller:any,orderId:string,method:(res:any)=>void):void;
         */
        GetPayResult = 60007, //直接忽略 无需调用


        /**
         * 请求服务器生成订单
         *  @param caller 上下文
         *  @param 
         * ydhw.Invoke(YDHW.YDHW_OPPO_API.RequestOrder,caller:any,method:(res)=>void):void;
         */
        RequestOrder = 60008, //直接忽略 无需调用

        /**
         * oppo统一下单
        * @param caller 上下文
         * @param custom 支付参数  appid token appVersion  engineVersion可默认为空 不为空则优先使用传入参数
         * @param res 请求返回的结果
         *  ydhw.Invoke(YDHW.YDHW_OPPO_API.OppoPlaceAnOrder,caller:any,custom:IYDHW.IOPOrderInfo,method:(res)=>void):void;
         */
        OppoPlaceAnOrder = 60009,  //直接忽略 无需调用


        /**
         * 获取订单签名 传递需要签名的参数
         * ydhw.Invoke(YDHW.YDHW_OPPO_API.GetPaySign,caller:any,custom:IYDHW.IOPOrderInfo,method:(res)=>void):void;
         */
        GetPaySign = 60011,   //直接忽略 无需调用

        /**
         * oppo支付  返回结果为True或False
         * @param caller 上下文
         * @param custom 支付参数  appid token 可默认为空 不为空则优先使用传入参数
         * ydhw.Invoke(YDHW.YDHW_OPPO_API.Pay,caller:any,custom:IYDHW.IOPPayInfo,method:(isTrue:boolean)=>void):void;
         */
        Pay = 60010,


    }

    export enum YDHW_MZ_API {
        /**
         * 获取网络类型
         * 
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_MZ_API.GetNetworkType,caller: any, method: (type: string) => void): void;
         */
        GetNetworkType = 140001,

        /**
         * 监听网络状态变化事件void;
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_MZ_API.OnNetworkStatusChange,caller: any, method: (type: string, isConnected: boolean) => void): void;
         */
        OnNetworkStatusChange = 140002,

    }

    export enum YDHW_KWG_API {


        /**
         * 开始游戏
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_KWG_API.readyGo,caller: any, method: () => void): void;
         */
        readyGo = 150001,

        shareToMsg = 150002,

        /**
        *  开始录屏
        * 
        * @param duration 录屏的时长，单位 s，必须 >3 &&  <= 300s（5 分钟）
        * @param onStart  监听录屏开始事件
        * @param onStop   录屏停止监听,回调参数videoPath：视频保存地址
        * @param onResume 暂停录屏
        * @param onPause  继续录屏
        * @param onError  监听录屏错误事件
        * @param onInterruptionBegin 监听录屏中断开始
        * @param onInterruptionEnd 监听录屏中断结束
        * ydhw.Invoke(YDHW.YDHW_KWG_API.RecorderStart,duration: number, onError?: (error: any) => void,
        */
        RecorderStart = 150003,

        /**
         *  停止录屏
         * 
         * ydhw.Invoke(YDHW.YDHW_KWG_API.RecorderStop): void;
         */
        RecorderStop = 150004,

        /**
         * 暂停录屏
         * 
         * ydhw.Invoke(YDHW.YDHW_KWG_API.RecorderPause): void;
         */
        RecorderPause = 150005,

        /**
         * 继续录屏
         * 
         * ydhw.Invoke(YDHW.YDHW_KWG_API.RecorderResume): void;
         */
        RecorderResume = 150006,


        /**
         *发布录屏
         ydhw.Invoke(YDHW.YDHW_KWG_API.publishVideo，C:any,CallBack?:(isSucess:boolean)=>void): void;
         */
        publishVideo = 150007


    }

    export enum YDHW_HuaWei_API {
        /**
         * 监听系统占用音频结束
         * ydhw.Invoke(YDHW.YDHW_HuaWei_API.OnAudioEnd,caller: any, method: () => void): void;
         */
        OnAudioEnd = 170001,
        /**
         *监听系统占用音频开始
        * ydhw.Invoke(YDHW.YDHW_HuaWei_API.onAudioBegin,caller: any, method: () => void): void;
        */
        onAudioBegin = 170002,
        /**
         * 获取玩家额外的信息
         * ydhw.Invoke(YDHW.YDHW_HuaWei_API.GetPlayerExtraInfo,caller: any, transactionId,method: (res) => void): void;
         */
        GetPlayerExtraInfo = 170003,
        /**
         * 上报进入和退出游戏的事件
         * ydhw.Invoke(YDHW.YDHW_HuaWei_API.SubmitPlayerEvent,caller: any,eventId: string, eventType: string,method: (res) => void): void;
         */
        SubmitPlayerEvent = 170004,

        /**
         * 获取支付订单信息
         * @param amount  金额
         * @param productDesc 产生描述
         * @param productName 产生名称
         *  ydhw.Invoke(YDHW.YDHW_HuaWei_API.GetPayOrder,caller: any,amount:string,productDesc:string,productName:string,method: (res) => void): void;
         */
        GetPayOrder = 170005,

        /**
         * 获取支付结果
         * @param requestId  订单信息
         *  ydhw.Invoke(YDHW.YDHW_HuaWei_API.GetPayOrderReq,caller: any,requestId:string,method: (res) => void): void;
         */
        GetPayOrderReq = 170006,


        /**
        * 支付
        * @param payInfo  支付信息
        *  @param caller  上限文
        *  @param sucess  支付成功回调
        *  @param faile  支付失败回调
        *  payInfo 信息为请求支付订单接口返回参数
        *  ydhw.Invoke(YDHW.YDHW_HuaWei_API.HBSPay,caller: any, payInfo:any,sucess: (res) => void  ,faile: (res) => void): void;
        */
        HBSPay = 170007

    }
    export enum YDHW_API {
        /**
         * 获取分享信息
         * 
         * ydhw.Invoke(YDHW.YDHW_API.ShareInfo): IYDHW.User.IShareInfo;
         */
        ShareInfo = 10001,
        /**
         * Banner统计
         * 
         * @param type  状态
         * @param adId 
         * 原接口名：StatisticBanner
         * ydhw.Invoke(YDHW.YDHW_API.StatBanner,type: IYDHW.Statistic.EM_STATISTIC_TYPE, adId: string): void;
         */
        StatBanner = 10002,//Statistic 的缩写 Stat
        /**
         *  激励视频统计
         * 
         * @param type SDK
         * @param adId 
         * 原接口名：StatisticVideo
         * ydhw.Invoke(YDHW.YDHW_API.StatVideo,type: IYDHW.Statistic.EM_STATISTIC_TYPE, adId: string): void;
         */
        StatVideo = 10003,
        /**
         * 插屏广告统计
         * 
         * @param type 
         * @param adId 
         * 
         * 原接口名：StatisticInterstitial
         * ydhw.Invoke(YDHW.YDHW_API.StatInterstitial,type: IYDHW.Statistic.EM_STATISTIC_TYPE, adId: string): void;
         */
        StatInterstitial = 10004,
        /**
         * 格子广告统计
         * @param type 
         * @param adId 
         * 原接口名：StatisticGrid
         * ydhw.Invoke(YDHW.YDHW_API.StatGrid,type: IYDHW.Statistic.EM_STATISTIC_TYPE, adId: string): void
         */
        StatGrid = 10005,
        /**
         * 盒子广告统计
         * @param type 
         * @param adId 
         * 原接口名：StatisticAppBox
         * ydhw.Invoke(YDHW.YDHW_API.StatAppBox,type: IYDHW.Statistic.EM_STATISTIC_TYPE, adId: string): void
         */
        StatAppBox = 10006,
        /**
         * 积木广告统计
         * @param type 
         * @param adId 
         * 原接口名：StatisticBlock
         * ydhw.Invoke(YDHW.YDHW_API.StatBlock,type: IYDHW.Statistic.EM_STATISTIC_TYPE, adId: string): void
         */
        StatBlock = 10007,
        /**
         * 原生广告统计
         * @param type 
         * @param adId 
         * 原接口名：StatisticNativeAd
         * ydhw.Invoke(YDHW.YDHW_API.StatNativeAd,type: IYDHW.Statistic.EM_STATISTIC_TYPE, adId: string): void
         */
        StatNativeAd = 10008,
        /**
         * 异常统计
         * @param ErrStack 
         * @param LogStr 
         * @param strDate 
         * 原接口名：StatisticErrorStack
         * ydhw.Invoke(YDHW.YDHW_API.StatErrorStack,ErrStack: any,LogStr: any,strDate:string):void
         */
        StatErrorStack = 10009,
        /**
         * 结果统计
         * @param details 对象或数组
         * 原接口名：StatisticResult
         * ydhw.Invoke(YDHW.YDHW_API.StatResult,details: any): void
         */
        StatResult = 10010,
        /**
         * 事件统计
         * 
         * @param event 事件名
         * @param scene 场景名
         * 原接口名：StatisticEvent
         * ydhw.Invoke(YDHW.YDHW_API.StatEvent,event: string, scene: string): void;
         */
        StatEvent = 10011,
        /**
         * 原接口名：StatisticDuration
         * ydhw.Invoke(YDHW.YDHW_API.StatDuration): void
         */
        StatDuration = 10012,
        /**
         *  卖量统计
         * 
         * @param request 
         * 原接口名：StatisticClickOut
         * ydhw.Invoke(YDHW.YDHW_API.StatClickOut,request: IYDHW.Statistic.IClickOutRequest);
         */
        StatClickOut = 10013,
        /**
         * 获取视频 or 分享的策略, type[1:分享， 2:视频]
         * 
         * @param channel 渠道
         * @param module 模块
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.ShowShareVideo,channel: string, module: string, caller: any, method: (type: number) => void): void;
         */
        ShowShareVideo = 10014,

        /**
         * 使用视频 or 分享的策略, type[1:分享， 2:视频](会消耗次数)
         * @param channel 渠道
         * @param module 模块
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.UseShareVideoStrategy,channel: string, module: string, caller: any, method: (type: number) => void): void;
         */
        UseShareVideoStrategy = 10015,
        /**
         * 切换页面（是否强制刷新banner）
         * 
         * @param isMisTouched 是否为误触banner
         * ydhw.Invoke(YDHW.YDHW_API.SwitchView,isMisTouched: boolean): void;
         */
        SwitchView = 10016,
        /**
         * 获取深度误触屏蔽开关状态
         * 
         * @param counter 当前关卡数(如果不是关卡游戏的话，不用传)
         * ydhw.Invoke(YDHW.YDHW_API.GetDeepTouchInfo,customNumber: number): IYDHW.IDeepTouchInfo;
         */
        GetDeepTouchInfo = 10017,
        /**
         * 获取自定义配置
         * 
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.GetCustomConfig,caller: any, method: (result: IYDHW.GameBase.ICustomConfigResult[]) => void): void
         */
        GetCustomConfig = 10018,
        /**
         * 游戏结束
         * 
         * @param index  当前关卡
         * ydhw.Invoke(YDHW.YDHW_API.GameOver,index: number): void;
         */
        GameOver = 10019,
        /**
         * 视频解锁 
         * 
         * @param index 当前关卡
         * ydhw.Invoke(YDHW.YDHW_API.IsUnlockVideo,index: number): boolean;
         */
        IsUnlockVideo = 10020,
        /**
         * 获取体力信息(后台配置的)
         * ydhw.Invoke(YDHW.YDHW_API.GetPowerInfo,): IYDHW.GameBase.IPowerSystemConfig;
         */
        GetPowerInfo = 10021,
        /**
         * 体力变化监听
         * 
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.ListenOnPowerChange,caller: any, method: (powerInfo: IYDHW.GameBase.IPowerInfo) => void): void;
         */
        ListenOnPowerChange = 10022,
        /**
         * 设置体力值
         * 
         * @param power 体力值
         * @param type 设置类型(CP手动修改请传0)
         * ydhw.Invoke(YDHW.YDHW_API.SetPower,power: number, type: EM_POWER_RECOVERY_TYPE): void;
         */
        SetPower = 10023,
        /**
         * 获取体力值
         * ydhw.Invoke(YDHW.YDHW_API.GetPower): number;
         */
        GetPower = 10024,
        /**
         * 获取卖量列表
         * 
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.GetSideBox,caller: any, method: (result: IYDHW.GameBase.ISideBoxResult[]) => void);
         */
        GetSideBox = 10025,
        /**
         * 获取积分墙列表
         * 
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.GetScoreBoardList,caller: any, method: (result: IYDHW.GameBase.IScoreBoardResult[]) => void): void;
         */
        GetScoreBoardList = 10026,
        /**
         * 领取积分墙奖励
         * 
         * @param id 积分墙ID，从GetScoreBoardList接口获取
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.GetScoreBoardAward,id: number, caller: any, method: (result: IYDHW.GameBase.IGetBoardAwardResult) => void): void;
         */
        GetScoreBoardAward = 10027,
        /**
         * 获取分享图列表
         * 
         * @param scene 场景名(传空串将获取所有场景的分享列表)
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.ShareCard,scene: string, caller: any, method: (result: IYDHW.GameBase.IShareCardResult[]) => void): void;
         */
        ShareCard = 10028,
        /**
         * 今天玩的局数
         * ydhw.Invoke(YDHW.YDHW_API.GetTodayBoutCount): number;
         */
        GetTodayBoutCount = 10029,
        /**
         * 总共玩的局数
         * ydhw.Invoke(YDHW.YDHW_API.GetTotalBoutCount): number;
         */
        GetTotalBoutCount = 10030,
        /**
         * 最后一次玩的关卡号码
         * ydhw.Invoke(YDHW.YDHW_API.GetLastBountNumber): number;
         */
        GetLastBountNumber = 10031,
        /**
         * 总玩过的最大关卡号码
         * ydhw.Invoke(YDHW.YDHW_API.GetMaxBountNumber): number;
         */
        GetMaxBountNumber = 10032,
        /*
         * 获取玩家今天看视频的次数
         ydhw.Invoke(YDHW.YDHW_API.GetTodayWatchVideoCounter): number;
         */
        GetTodayWatchVideoCounter = 10033,
        /**
         * 创建banner广告(填充屏幕宽度)
         * 
         * @param isMisTouch 是否为误触Banner
         * @param isShow 是否立马显示
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.CreateBannerAd,isMisTouch: boolean, isShow: boolean, caller: any, method: (isOk: boolean) => void): void;
         */
        CreateBannerAd = 10034,
        /**
         * 创建banner广告(小的)
         * 
         * @param isMisTouch 是否为误触Banner
         * @param isShow 是否立马显示
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.CreateSmallBannerAd,isMisTouch: boolean, isShow: boolean, caller: any, method: (isOk: boolean) => void): void;
         */
        CreateSmallBannerAd = 10035,
        /**
         * 创建banner自定义style
         * 
         * @param isMisTouch 是否为误触Banner
         * @param isShow 是否立马显示
         * @param style Style风格
         * @param caller 
         * @param method 
         * @param onResize 
         * ydhw.Invoke(YDHW.YDHW_API.CreateCustomBannerAd,isMisTouch: boolean, isShow: boolean, style: IYDHW.IAdStyle, caller: any, method: (isOk: boolean) => void, onResize: (result: any) => void): void;
         */
        CreateCustomBannerAd = 10036,
        /**
         * 改变Banner的Style
         * 
         * @param style 
         * ydhw.Invoke(YDHW.YDHW_API.BannerAdChangeSize,style: IYDHW.IAdStyle): void;
         */
        BannerAdChangeSize = 10037,
        /**
         * 显示banner广告
         * ydhw.Invoke(YDHW.YDHW_API.ShowBannerAd): void;
         */
        ShowBannerAd = 10038,
        /**
         * 隐藏 banner广告
         * ydhw.Invoke(YDHW.YDHW_API.HideBannerAd): void;
         */
        HideBannerAd = 10039,
        /**
         * 
         * @param style 
         * ydhw.Invoke(YDHW.YDHW_API.ChangeBannerStyle,style: IYDHW.IAdStyle): void;
         */
        ChangeBannerStyle = 10040,
        /**
         * 创建激励视频广告
         * @param index 可选参数 创建配置视频广告列表指定下标广告
         * ydhw.Invoke(YDHW.YDHW_API.CreateRewardVideoAd,index?:number): void;
         */
        CreateRewardVideoAd = 10041,
        /**
         * 播放视频广告
         * 
         * @param unlockCustomNumber 解锁关卡
         * @param isAddPower 看完视频是否增加体力
         * @param caller 
         * @param onClose 
         * @param index  可选参数 指定广告视频ID （配置内广告ID的下标）
         * ydhw.Invoke(YDHW.YDHW_API.ShowRewardVideoAd,unlockCustomNumber: number, isAddPower: boolean, caller: any, onClose: (type: EM_VIDEO_PLAY_TYPE) => void,index?:string): void;
         */
        ShowRewardVideoAd = 10042,
        /**
         * 分享图统计（打点统计）
         * @param info  分享参数
         * 原接口名：StatisticShareCard
         * ydhw.Invoke(YDHW.YDHW_API.StatShareCard,info: IYDHW.Statistic.IStatistiicShareInfo): void;
         */
        StatShareCard = 10043,
        StatShareCardInner = 10044,
        /**
         * 
         * 获取游戏流失路径列表
         * 
         * @param onReturn 流失路径信息列表
         * ydhw.Invoke(YDHW.YDHW_API.GetLayerList,onReturn: (layerList: IYDHW.GameBase.ILayerInfo[]) => void): void
         */
        GetLayerList = 10045,
        /**
          * 小游戏跳转
          * （以下参数从GetSideBox接口获取）
          * @param id 卖量ItemID
          * @param toAppId 跳转AppID
          * @param toUrl 跳转路径
          * @param source 从哪个模块导出的，该字段具体值由调用方自行定义
          * @param caller 
          * @param method 
          * ydhw.Invoke(YDHW.YDHW_API.NavigateToMiniProgram,id: number, toAppId: string, toUrl: string, source: string, caller: any, method: (isOk: boolean) => void): void;
          */
        NavigateToMiniProgram = 10046,
        /**
         * 获取平台用户信息
         * 支持平台:QQ,微信,头条
         *
         * @param caller 
         * @param onSuccess 
         * @param onError 
         * ydhw.Invoke(YDHW.YDHW_API.GetUserInfo,caller:any ,onSuccess: (result: any) => void, onError: (error: any) => void):void;
         */
        GetUserInfo = 10047,
        /**
         * 创建-用户信息按钮
         * 
         * @param btnInfo 具体格式查看官方文档(目前只支持微信/QQ)
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.CreateUserInfoButton,btnInfo:any,caller:any ,onSuccess: (result: any) => void, onError: (error: any) => void): void;
         */
        CreateUserInfoButton = 10048,
        /**
         * 显示-用户信息按钮
         * ydhw.Invoke(YDHW.YDHW_API.ShowUserInfoButton): void;
         */
        ShowUserInfoButton = 10049,
        /**
         * 隐藏-用户信息按钮
         * ydhw.Invoke(YDHW.YDHW_API.HideUserInfoButton): void;
         */
        HideUserInfoButton = 10050,
        /**
         * 销毁-用户信息按钮void;
         * ydhw.Invoke(YDHW.YDHW_API.DestroyUserInfoButton): void;
         */
        DestroyUserInfoButton = 10051,
        /**
         * 创建插屏广告
         * 
         * @param isShow 是否展示
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.CreateInterstitialAd,isShow: boolean, caller: any, onLoad: () => void, onClose: () => void, onError: () => void): void;
         */
        CreateInterstitialAd = 10052,

        /**
         * 显示插屏广告
         * ydhw.Invoke(YDHW.YDHW_API.ShowInterstitialAd): void;
         */
        ShowInterstitialAd = 10053,
        /**
         * 获取假分享策略结果
         * 
         * @param channel 渠道
         * @param module  模块
         * ydhw.Invoke(YDHW.YDHW_API.GetSharingResults,shareInfo: IYDHW.IShareAppInfo, caller: any, method: (shareBackInfo: IYDHW.IShareBackInfo) => void): void;
         */
        GetSharingResults = 10054,
        /**
         * 调用流失统计
         * 
         * @param layerPath 流失路径(从GetLayerList接口获取)
         * ydhw.Invoke(YDHW.YDHW_API.StatLayer,layerPath: string): void;
         */
        StatLayer = 10055,
        /**
         * 获取分享发奖次数上限
         * ydhw.Invoke(YDHW.YDHW_API.GetShareRewardLimit): number;
         */
        GetShareRewardLimit = 10056,
        /**
         * 获取视频发奖次数上限
         * ydhw.Invoke(YDHW.YDHW_API.GetVideoRewardLimit): number;
         */
        GetVideoRewardLimit = 10057,
        /**
         * 获取服务器信息
         * 
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.GetServerInfo,caller: any, method: (result: IYDHW.User.IMyInfoResult) => void): void;
         */
        GetServerInfo = 10058,
        /**
         * 退出游戏
         * 
         * ydhw.Invoke(YDHW.YDHW_API.ExitGame):void;
         */
        ExitGame = 10059,
        /**
         * 震动-短时
         * 
         * (触发较短时间，持续15ms)
         * ydhw.Invoke(YDHW.YDHW_API.VibrateShort): void;
         */
        VibrateShort = 10060,
        /**
         * 震动-长时
         * 
         * (触发较长时间震动，持续400ms)
         * ydhw.Invoke(YDHW.YDHW_API.VibrateLong): void;
         */
        VibrateLong = 10061,
        /** 
         * 创建桌面图标，每次创建都需要用户授权
         * 两次调用之间的间隔时间是120秒以上
         * 
         * @param caller 
         * @param onSuccess   存在
         * @param onFail      不存在或接口调用失败  若调用失败 回调参数 失败为原因
         * @param onComplete 
         * @param message 权限弹窗上的说明文字，用于向用户解释为什么要创建桌面图标(VIVO可传)
         * ydhw.Invoke(YDHW.YDHW_API.InstallShortcut,caller: any, onSuccess: () => void,  onFail?: (error: any) => void, onComplete?: () => void,message?:string): void;
         */
        InstallShortcut = 10062,


        /**
         * 是否已经创建桌面图标
         * @param caller 
         * @param onSuccess  vivo平台无回调参数 存在 直接在回调内执行逻辑即可 无需再对回调参数进行判断处理 ;   opppo 调用接口结果需要判断
         * @param onFail     不存在 若调用失败 回调参数 失败为原因
         * @param onComplete  (VIVO平台没有回调) 
         * ydhw.Invoke(YDHW.YDHW_API.HasShortcutInstalled,caller: any, onSuccess: (result: any) => void, onFail?: (error: any) => void, onComplete?: () => void): void; 
         */
        HasShortcutInstalled = 10066,


        /**
         * 原生广告创建
         * adIndex  列表固定位置广告
         * ydhw.Invoke(YDHW.YDHW_API.CreateNativeAd,caller: any,method: (args: any) => void,adIndex: number = -1): void;
         */
        CreateNativeAd = 10063,
        /**
         * 上报广告曝光，一个广告只有一次上报有效
         * 
         * @param nativeId 创建返回的adId
         * ydhw.Invoke(YDHW.YDHW_API.ShowNativeAd,nativeId: string): void;
         */
        ShowNativeAd = 10064,
        /**
         * 上报广告点击，一个广告只有一次上报有效
         * 
         * @param nativeId  创建返回的adId
         * 
         * ydhw.Invoke(YDHW.YDHW_API.ClickNativeAd,nativeId: string): void;
         */
        ClickNativeAd = 10065,

        /**
         * 分享信息
         * 
         * @param scene        当前场景
         * @param channel      渠道名
         * @param module       模块名
         * @param inviteType   邀请参数
         * @param caller       上下文
         * @param method       回调函数
         * @param target       回调函数
         * ydhw.Invoke(YDHW.YDHW_API.ShareAppMessage,scene: string, channel: string, module: string, inviteType: string, caller: any, method: (result: any) => void,target?:EM_SHARE_APP_TYPE): void;
         */
        ShareAppMessage = 10067,
        Hook = 10068,
        OnFrontend = 10069,
        OnBackend = 10070,
        /**
         * onShow监听回调
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.OnShow,caller: any, method: (res:any) => void): void;
         */
        OnShow = 10071,
        /**
         * onHide监听回调
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.OnHide,caller: any, method: (res:any) => void): void;
         */
        OnHide = 10072,
        /**
         * onError监听回调
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.OnError,caller: any, method: (error:any) => void): void;
         */
        OnError = 10073,
        GetObject = 10074,
        GetString = 10075,
        SetObject = 10076,
        SetString = 10077,
        Size = 10078,
        DeleteObject = 10079,
        /**
         * 自定以存储空间
         * 
         * @param storeInfo  操作参数
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.StoreValue,storeInfo: IYDHW.GameBase.IStoreValueRequest, caller?: any, method?: (result: IYDHW.GameBase.IStoreValueResult) => void): void;
         */
        StoreValue = 10080,
        /**
         * SDK登录-必接接口(记得先调用登录接口，很多接口都需要再登录成功后才能访问服务器)
         * 
         * @param caller 
         * @param method 
         * ydhw.Invoke(YDHW.YDHW_API.Login,caller: any, method: (isOk: boolean) => void): void;
         */
        Login = 10081,
        ScoreBoard = 10083,
        IsCanRewardByVideoOrShare = 10084,
        /**
         * 原生模板广告统计(微信)
         * @param type 
         * @param adId 
         * ydhw.Invoke(YDHW.YDHW_API.StatCustomAd,type: IYDHW.Statistic.EM_STATISTIC_TYPE, adId: string): void
         */
        StatCustomAd = 10085,

        /**
         * 积分墙跳转处理
         * @param type 
         * @param adId 
         *  ydhw.Invoke(YDHW.YDHW_API.OnScorecardJump,id: number, toAppId: string, toUrl: string, source: string, caller: any, method: (isOk: boolean) => void): void;
         */
        OnScorecardJump = 10086,
        /**
         * 获取右上角坐标
         * 支持平台:QQ,微信,头条
         *
         * @param caller 
         * @param callBack
         * ydhw.Invoke(YDHW.YDHW_API.GetCapsulePoint,caller:any ,callBack(res:any)):void;
         */
        GetCapsulePoint = 10087,

        /**
         * 获取系统信息
         * 支持平台:QQ,微信,头条
         *
         * @param caller 
         * @param callBack
         * ydhw.Invoke(YDHW.YDHW_API.GetSysIf,caller:any ,callBack(res:any)):void;
         */
        GetSysIf = 10088,
        /**
              * 获取配置广告数据
              *  ydhw.Invoke(YDHW.YDHW_API.GetAdvertisingBoard,caller:any ,callBack(res:any)):void;
              */
        GetAdvertisingBoard = 10089,
        /**
         * 事件打点
         * @param event 事件
         * @param info 属性
         * ydhw.Invoke(YDHW.YDHW_API.TkDot,event:string,info?:any):void;
         */
        TkDot = 10090,
        /**
         * 属性打点
         * @param info 用户属性
         * ydhw.Invoke(YDHW.YDHW_API.TkUserSet,info:any):void;
         */
        TkUserSet = 10091,
        /**
        * 初始化用户属性
        * @param info  
        *  ydhw.Invoke(YDHW.YDHW_API.TkUserSetOnce,info:any):void;
        */
        TkUserSetOnce = 10092,
        /**
     * 用户登录
    * @param acountid 用户id
     * ydhw.Invoke(YDHW.YDHW_API.TkLogin,acountid:any):void;
     */
        TkLogin = 10093,
        /**
    * 用户登录
     * @param eventname 事件名称
    * ydhw.Invoke(YDHW.YDHW_API.EventTime,acountid:any):void;
    */
        EventTime = 10094,


    }


    export enum EM_TP_AD {
        /**
         * 创建
         */
        CREATE = 0,
        /**
         * 加载成功
         */
        LOAD_SUCCESS = 1,//
        /**
         * 加载失败
         */
        LOAD_FAIL = 2,//
        /**
         * 点击(上报点击)
         */
        CLICK = 3,
        /**
         * 展示
         */
        SHOW = 4,
        /**
         * 关闭
         */
        CLOSE = 5,
        /**
         * 上报曝光
         */
        EXPOSURE = 6,
        /**
         * 播放取消
         */
        PLAY_CANCEL = 7,
        /**
         * 播放完成
         */
        PLAY_FINISH = 8,
    }

    export enum EM_SHARE_TYPE {
        /**
          * 无策略
          */
        None = 0,
        /**
         * 分享
         */
        Share = 1,
        /**
         * 视频
         */
        Video = 2,
    }

    export enum EM_POWER_RECOVERY_TYPE {
        /**
         * 初始化或其他
         */
        None = 0,
        /**
        * 看视频恢复体力
        */
        WatchVideo = 1,
        /**
         * 定时恢复体力
         */
        AutoRecovery = 2,
        /**
         * 倒计时
         */
        CountDown = 3,
    }

    export enum EM_VIDEO_PLAY_TYPE {
        /**
         * 视频广告-播放失败
         */
        VIDEO_PLAY_FAIL = 0,
        /**
         * 视频广告-播放完成
         */
        VIDEO_PLAY_FINISH = 1,
        /**
         * 视频广告-播放取消
         */
        VIDEO_PLAY_CANCEL = 2,
    }

    //@platform=wechat
    export namespace WX {

        export class OnShareTimelineInfo implements IYDHW.Wechat.IOnShareTimelineInfo {
            /**
             * 转发显示图片的链接，可以是网络图片路径或本地图片文件路径或相对代码包根目录的图片文件路径。（该图片用于分享到朋友圈的卡片以及从朋友圈转发到会话消息的卡片展示）
             */
            imageUrl: string;
            /**
             * 转发标题，不传则默认使用当前小游戏的昵称
             */
            title?: string;
            /**
             * 查询字符串，必须是 key1=val1&key2=val2 的格式。从这条转发消息进入后，可通过 wx.getLaunchOptionsSync() 或 wx.onShow() 获取启动参数中的 query。不传则默认使用当前页面query
             */
            query?: string;

            constructor(imageUrl: string, title?: string, query?: string) {
                if (imageUrl) this.imageUrl = imageUrl;
                if (title) this.title = title;
                if (query) this.query = query;
            }
        }

        export class GameRecorderInfo implements IYDHW.Wechat.IRecorderInfo {
            /**
             * 视频 fps
             * 默认：24
             */
            fps?: number;
            /**
             * 视频的时长限制，单位为秒（s）。最大值 7200，最小值 5，到达指定时长后不会再录入。但还需要手动调用 GameRecorder.stop() 来结束录制。
             * 默认：7200
             */
            duration?: number;
            /**
             * 视频比特率（kbps），默认值1000，最大值 3000，最小值 600
             * 默认：1000
             */
            bitrate?: number;
            /**
             * 视频关键帧间隔
             * 默认：12
             */
            gop?: number;
            /**
             * 是否录制游戏音效
             * 默认：true
             */
            hookBgm?: boolean;

            constructor(fps?: number, duration?: number, bitrate?: number, gop?: number, hookBgm?: boolean) {
                if (fps) this.fps = fps;
                if (duration) this.duration = duration;
                if (gop) this.gop = gop;
                if (hookBgm) this.hookBgm = hookBgm;
            }
        }

        /**
         * 创建分享按钮传参
         * 
         * 详情参考官网文档：
         * https://developers.weixin.qq.com/minigame/dev/api/game-recorder/wx.createGameRecorderShareButton.html
         */
        export class GameRecorderBtnInfo implements IYDHW.Wechat.IRecorderBtnInfo {
            /**
             * 按钮的样式
             */
            style: IYDHW.Wechat.IGameRecorderBtnStyle;
            /**
             * 图标的 url。支持 http/https 开头的网络资源和 wxfile:// 开头的本地资源。如果不设置则使用默认图标。
             */
            icon?: string;
            /**
             * 按钮的背景图片的 url。支持 http/https 开头的网络资源和 wxfile:// 开头的本地资源。如果不设置则使用默认图标
             */
            image?: string;
            /**
             * 按钮的文本
             */
            text?: string;
            /**
             * 对局回放的分享参数
             */
            share: IYDHW.Wechat.IGameRecorderShareInfo;

            constructor(style: IYDHW.Wechat.IGameRecorderBtnStyle, share: IYDHW.Wechat.IGameRecorderShareInfo, icon?: string, image?: string, text?: string) {
                if (style) this.style = style;
                if (share) this.share = share;
                if (icon) this.icon = icon;
                if (image) this.image = image;
                if (text) this.text = text;
            }
        }
        export class GameRecorderBtnStyle implements IYDHW.Wechat.IGameRecorderBtnStyle {
            /**
             * 左上角横坐标，单位 逻辑像素
             * 默认值:0
             */
            left?: number;
            /**
             * 左上角纵坐标，单位 逻辑像素
             * 默认值:0
             */
            top?: number;
            /**
             * 按钮的高度，最小 40 逻辑像素
             * 默认值:40
             */
            height?: number;
            /**
             * 图标和文本之间的距离，最小 8 逻辑像素
             * 默认值:8
             */
            iconMarginRight?: number;
            /**
             * 文本的字体大小。最小 17，最大 22。
             * 默认值:17
             */
            fontSize?: number;
            /**
             * 文本的颜色
             * 默认值:#ffffff
             */
            color?: string;
            /**
             * 按钮的左内边距，最小 16 逻辑像素
             * 默认值:16
             */
            paddingLeft?: number;
            /**
             * 按钮的右内边距，最小 16 逻辑像素。
             * 默认值:16
             */
            paddingRight?: number;
            constructor(left?: number, top?: number, height?: number, iconMarginRight?: number, fontSize?: number, color?: string, paddingLeft?: number, paddingRight?: number) {
                if (left) this.left = left;
                if (top) this.top = top;
                if (height) this.height = height;
                if (iconMarginRight) this.iconMarginRight = iconMarginRight;
                if (fontSize) this.fontSize = fontSize;
                if (color) this.color = color;
                if (paddingLeft) this.paddingLeft = paddingLeft;
                if (paddingRight) this.paddingRight = paddingRight;
            }
        }
        export class GameRecorderShareInfo implements IYDHW.Wechat.IGameRecorderShareInfo {
            /**
             * 分享的对局回放打开后跳转小游戏的 query。
             */
            query: string;
            /**
             * 对局回放的标题。对局回放标题不能随意设置，只能选择预设的文案模版和对应的参数。
             */
            title?: IYDHW.Wechat.IGameRecorderTitle;
            /**
             * 对局回放的按钮。只能选择预设的文案模版
             */
            button?: IYDHW.Wechat.IGameRecorderBotton;
            /**
             * 对局回放背景音乐的地址。必须是一个代码包文件路径或者 wxfile:// 文件路径，不支持 http/https 开头的 url。
             */
            bgm: string;
            /**
             * 对局回放的剪辑区间，是一个二维数组，单位 ms（毫秒）。[[1000, 3000], [4000, 5000]] 表示剪辑已录制对局回放的 1-3 秒和 4-5 秒最终合成为一个 3 秒的对局回放。对局回放剪辑后的总时长最多 60 秒，即 1 分钟。
             */
            timeRange: Array<number>;
            /**
             * 对局回放的音量大小，最小 0，最大 1
             */
            volume?: number;
            /**
             * 对局回放的播放速率，只能设置以下几个值：0.3，0.5，1，1.5，2，2.5，3。其中1表示原速播放，小于1表示减速播放，大于1表示加速播放。
             */
            atempo?: number;
            /**
             * 如果原始视频文件中有音频，是否与新传入的bgm混音，默认为false，表示不混音，只保留一个音轨，值为true时表示原始音频与传入的bgm混音。
             */
            audioMix?: boolean;
            constructor(query: string, bgm: string, title?: IYDHW.Wechat.IGameRecorderTitle, button?: IYDHW.Wechat.IGameRecorderBotton, timeRange?: Array<number>, volume?: number, atempo?: number, audioMix?: boolean) {
                if (query) this.query = query;
                if (bgm) this.bgm = bgm;
                if (title) this.title = title;
                if (button) this.button = button;
                if (timeRange) this.timeRange = timeRange;
                if (volume) this.volume = volume;
                if (atempo) this.atempo = atempo;
                if (audioMix) this.audioMix = audioMix;
            }
        }
        export class GameRecorderTitle implements IYDHW.Wechat.IGameRecorderTitle {
            /**
             * 对局回放的标题的模版,
             * 不传则为：${用户昵称} 在 ${游戏名称} 的游戏时刻
             * 
             *  值	                说明	
             * default.score	    模版格式为，${游戏名称}，本局得分：${score}，对应的 data 应该如 { score: 4500 }	
             * default.level	    模版格式为，${游戏名称}，当前关卡：第42关，对应的 data 应该如 { level: 23 }	
             * default.opponent	    模版格式为，${游戏名称}，本局对手：${opponent}，对应的 data 应该如 { opponent_openid: 'oC6J75Sh1_4K8Mf5b1mlgDkMPhoI' }	
             * default.cost	        模版格式为，${游戏名称}，本局耗时：${cost}秒，对应的 data 应该如 { cost_seconds: 123 }
             * 
             */
            template: string;
            /**
             * 对局回放的标题的模版参数
             */
            data: IYDHW.Wechat.IGameRecorderTitleData;
            constructor(template: string, data: IYDHW.Wechat.IGameRecorderTitleData) {
                if (template) this.template = template;
                if (data) this.data = data;
            }
        }
        export class GameRecorderBotton implements IYDHW.Wechat.IGameRecorderBotton {
            /**
             * 对局回放的按钮的模版
             * 默认值:enter 
             *      值	        说明	
             *      enter	    马上玩	
             *      challenge	去挑战	
             *      play	    玩一把
             * 
             */
            template: string;
            constructor(template: string) {
                if (template) this.template = template;
            }
        }
        /**
         * 该参数根据template选择的模板传对应参数
         */
        export class GameRecorderTitleData implements IYDHW.Wechat.IGameRecorderTitleData {
            /**
             * template值为default.score选该参数
             */
            score?: number;
            /**
             * template值为default.level选该参数
             */
            level?: number;
            /**
             * template值为default.opponent选该参数
             */
            opponent_openid?: string;
            /**
             * template值为default.cost选该参数
             */
            cost_seconds?: number;

            constructor(score?: number, level?: number, opponent_openid?: string, cost_seconds?: number) {
                if (score) this.score = score;
                if (level) this.level = level;
                if (opponent_openid) this.opponent_openid = opponent_openid;
                if (cost_seconds) this.cost_seconds = cost_seconds;
            }
        }

    }
    //@end=wechat

    //@platform=tt
    export namespace TT {
        export class TTSideBoxInfo implements IYDHW.TT.ISideBoxInfo {
            iconId: number; //卖量图片ID,从侧边栏列表获取
            souce: string;  //导出模块
            target: string; //导出游戏的appid
        }
    }
    //@end=tt

    //@platform=qq
    export namespace QQ {
        export class ShareTempletInfo implements IYDHW.QQ.IShareTempletInfo {
            shareTemplateId: string;         //分享模板ID
            shareTemplateData: SHareTemplateData;   //分享文案
            channel?: string;                //渠道
            module?: string;                 //模块
            inviteType?: IYDHW.QQ.EM_SHARE_APP_TYPE;  //邀请类型,可以通过qq.getLaunchOptionsSync() 或 qq.onShow() 获取启动参数中的 query获取到的参数中拿到from参数的值就是该参数的传值，CP可以通过该参数给玩家发奖。
            shareAppType?: string;           //转发目标类型,不设该属性默认拉起手q通讯录，详见官方文档
            entryDataHash?: string;          //监听用户点击页面内转发按钮的，只有带上该参数，才支持快速分享，详见官方文档
        }

        export class SHareTemplateData implements IYDHW.QQ.ISHareTemplateData {
            txt1: string;    //中间文案
            txt2: string;    //底部文案
        }

        export class AppMsgInfo implements IYDHW.QQ.IAppMsgInfo {
            tmplIds?: string[];   //需订阅的消息模板的id的集合，一次调用最多可订阅3条消息。
            subscribe: boolean;  //订阅(true)及取消订阅(false)
        }

        //增加好友按钮信息
        export class AddFriendButtonInfo implements IYDHW.QQ.IAddFriendButtonInfo {
            type: string;       //按钮的类型 ['text':可以设置背景色和文本的按钮 ,'image':只能设置背景贴图的按钮，背景贴图会直接拉伸到按钮的宽高]
            text?: string;      //按钮上的文本，仅当 type 为 text 时有效
            image?: string;      //按钮的背景图片，仅当 type 为 image 时有效
            openId: string;     //好友的openid
            style: FbStyle;    //按钮的样式
        }

        //按钮的样式
        export class FbStyle implements IYDHW.QQ.IFbStyle {
            left: number;               //左上角横坐标
            top: number;                //左上角纵坐标
            width: number;              //宽度
            height: number;             //高度
            backgroundColor: string;    //背景颜色。格式为 6位/8位 16进制数
            borderColor: string;        //边框颜色。格式为 6位/8位 16进制数
            borderWidth: number;        //边框宽度
            borderRadius: number;       //边框圆角
            color: string;              //文本的颜色。格式为 6位 16进制数。
            textAlign: string;          //文本的水平居中方式['left':居左,'center':居中,'right':居右]
            fontSize: number;           //字号
            lineHeight: number;         //文本的行高
        }

        //积木广告参数
        export class BlockAdInfo implements IYDHW.QQ.IBlockAdInfo {
            style: BlockStyle;      //积木广告组件的样式
            size: number;            //范围是1~5，积木广告的个数（展示以实际拉取广告数量为准）
            orientation: string;     //landscape 或者 vertical，积木广告横向展示或者竖向展示
        }

        //积木广告组件的样式
        export class BlockStyle implements IYDHW.QQ.IBlockStyle {
            left: number;     //积木广告组件的左上角横坐标
            top: number;      //积木广告组件的左上角纵坐标
        }
        export enum EM_SHARE_APP_TYPE {
            QQ = 'qq',                              //转发到手q通讯录
            QQ_FAST_SHARE = 'qqFastShare',          //快速转发至来源的聊天窗口
            QQ_FAST_SHARE_LIST = 'qqFastShareList', //快速转发列表
            QZONE = 'qzone',                        //转发到空间
            WECHARTFRIENDS = 'wechatFriends',       //转发到微信好友
            WECHATMOMENT = 'wechatMoment',          //转发到微信朋友圈
        }
    }
    //@end=qq

    export class ClickOutRequest implements IYDHW.Statistic.IClickOutRequest {
        iconId: number; //卖量图片ID,从侧边栏列表获取
        souce: string;  //导出模块
        target: string; //导出游戏的appid
        action: string; //用户点击与否['enable':点击,'cancel':取消]
    }

    export class ShareAppInfo implements IYDHW.IShareAppInfo {
        channel: string;   //渠道名称
        module: string;    //模块名称
        showTime: number;  //分享时长
        shareId: number;   //分享ID
    }

    export class StatisticResultInfo implements IYDHW.Statistic.IStatisticResultInfo {
        layerPath?: string;   //路径，从流失路径列表接口获取
        hasWin?: boolean;     //是否获胜[true:获胜,false:失败]
        source?: number;      //获得分数
        detail?: any;         //其他详情,CP自由发挥
    }

    /**
     * 广告Style
     */
    export class AdStyle implements IYDHW.IAdStyle {
        top: number;    //广告组件的左上角横坐标
        left: number;   //广告组件的左上角纵坐标
        width?: number;  //广告组件的宽度
        height?: number; //广告组件的高度
        fixed?: boolean; //原生模板广告组件是否固定屏幕位置（不跟随屏幕滚动）
    }

    export class SdkInfo implements IYDHW.Statistic.IApiInfo {
        name: string;  //接口名
        platform: string; //接口所属平台
        time: number;   //接口调用时间
        wholesaleId: number;//批次ID
        count: number; //调用次数
    }

    export class StatistiicShareInfo implements IYDHW.Statistic.IStatistiicShareInfo {
        /**
         * 分享图ID ,-1：没有分享图
         */
        sharecardId?: number;
        /**
         * 0:分享图,1:视频分享,2:口令分享(头条需要)
         *
         */
        sType?: number;
        /**
         * 1: 转发到手q通讯录,1:快速转发至来源的聊天窗口,2:快速转发列表,3:转发到空间,4:转发到微信好友,5:转发到微信朋友圈(QQ需要)
         */
        target?: number;
        /**
         * -1:分享-取消,0:分享-不确定,1:分享-确定 
         * (头条有确定回调,微信和QQ都没有)
         */
        real?: number;

        constructor(sharecardId?: number, sType?: number, target?: number, real?: number) {
            this.sharecardId = sharecardId || -1;
            if (sType != null) this.sType = sType;
            if (target != null) this.target = target;
            this.real = real || 0;
        }
    }

    export class StoreValueRequest implements IYDHW.GameBase.IStoreValueRequest {
        /**
         * 空间名称(后台配置)
         */
        name: string;
        /**
         * 指令(具体请查看文档)
         */
        cmd: string;
        /**
         * 存贮数据(具体请查看文档)
         */
        args: string;
        /**
         * 
         * @param name 空间名称(后台配置)
         * @param cmd 指令(具体请查看文档)
         * @param args 存贮数据(具体请查看文档)
         */
        constructor(name: string, cmd?: string, args?: string) {
            if (name) this.name = name;
            if (cmd) this.cmd = cmd;
            if (args) this.args = args;
        }
    }
    export class EditRequest implements IYDHW.User.IEditRequest {
        nickName: string;
        headimgurl: string;
        gender: number;
        language: string;
        province: string;
        city: string;
        country: string;
    }


}


export namespace EventModule {
    //Basic  基础事件
    //Global 全局事件
    //Scence 场景值
    //RimSys 周边系统

    /**当局事件 */
    export enum EM_Basic_Game {
        /**当据开始 */
        Start = "g_start",
        /**当据胜利 */
        Sucess = "g_win",
        Fail = "g_fail",
        Resur = "g_re",
        End = "g_end",
    }
    /**新手引导 */
    export enum EM_Basic_Guide {
        GAME_GUIDE = "guide",

    }
    /**合成 */
    export enum EM_Basic_Merge {
        GAME_MERGE = "merge",
        GAME_MERGE_New = "merge_new",
    }

    /**签到 */
    export enum EM_RimSys_Sigin {
        EnterInto = "sign_in",
        Get_G_Award = "g_win",  //普通
        Get_S_Award = "sign_get2",//特殊
    }
    /**任务 */
    export enum EM_RimSys_Task {
        EnterInto = "task_in",
        Get_G_Award = "task_get1",  //普通
        Get_S_Award = "task_get2",//特殊
    }
    /**排行榜 */
    export enum EM_RimSys_Rank {
        EnterInto = "rank_in",
    }

    /**Banner */
    export enum EM_Global_Banner {
        Load = "banner_load",
        Show = "banner_show",
        Click = "banner_click",
    }
    /**插屏 */
    export enum EM_Global_Interstitial {
        Jump = "interstitial_show",
        Click = "interstitial_click",  //普通
    }

    /**跳转 */
    export enum EM_Global_Jump {
        Jump = "out_show",
        Complete = "out_confirm"
    }

    /**分享 */
    export enum EM_Global_Share {
        Share = "share",
        ReShare = "fakeshare_re"
    }
    /**资产 */
    export enum EM_Global_Money {
        Coin_Get = "coin_get", //金币
        Coin_Use = "coin_use",
        Power_Get = "phy_get", //体力
        Power_Use = "phy_use",
    }

    /**登录 */
    export enum EM_Global_Login {
        Login = "login",
    }

    /**注册 */
    export enum EM_Global_register {
        Register = "reg",
    }

    /**加载 */
    export enum EM_Global_register {
        Loading = "loading",
    }

    /**主界面 */
    export enum EM_Global_EnterGame {
        Loading = "main",
    }

    /**场景 */
    export enum EM_Scence {
        //游戏主页
        MainHome = 1,
        //关卡
        Chapter = 2,
        //结算
        Result = 3,
        //排行榜
        Rank = 4,
        //转盘
        Turntable = 5,
        //签到
        Sign = 6,
        //任务
        Task = 7,
        //商店
        Shop = 8,
    }


}


