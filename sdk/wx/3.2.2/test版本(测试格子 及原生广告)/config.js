//请将以下配置拷贝到index.js文件中
//下面这段配置拷贝放到index.js最前面
var YDHW_CONFIG = {
    appid: "xxxxx",
    version: '1.0.0',
    banner_ad_unit_id_list: ['xxxxx'],                       //Banner广告    全平台支持（除快手）
    interstitial_ad_unit_id_list: ['xxxxx'],                  //插屏广告          全平台支持
    spread_ad_unit_id_list: ['xxxxx'],                       //开屏广告          支持 o，v
    native_ad_unit_id_list: ['xxxxx'],                       //原生广告           支持 O，V，H，M
    video_ad_unit_id_list: ['xxxxx'],                        //视频广告            全平台支持
    grid_ad_unit_id_list: ["xxxxxxx"],                     //微信  格子广告
    cunstom_ad_unit_id_list: [`xxxxxxxx`],             //微信  原生广告
    tt_template_id_list: ["xxxxx"],                          //TT 分享素材模板ID列表      支持T
    interstitialAd_first_show_wait_time: 10,           //插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
    interstitialAd_show_time_interval: 10,             //插屏广告-两次展示之间时间间隔（秒）             
    cunstom_ad_unit_id_list:[],                               //微信原生模板广告 //使用时 传递下标即可         支持W          
    side_box_count: 20,                                        //侧边栏列表item最小保留数(基于曝光策略)         所有平台
    pkg_name: "",                                                //包名OPPO、VOVP、魅族平台需要                   

    //--------------以下配置CP无需理会------------
    project: '',
    platform: 'web',
    env: 'dev',//online or dev
    debug: true,
    inspector: false,
    engine: 'laya',
    res_version: '20200310',
    appkey: "30248858",
    resource_url: 'http://127.0.0.1:3100',
    scene_white_list: [1005, 1006, 1037, 1035],
};

window.YDHW_CONFIG = YDHW_CONFIG;




//这个引入需要根据自己放置的SDK位置修改
loadLib("wx/ydhw.wx.sdk.min.js"); 