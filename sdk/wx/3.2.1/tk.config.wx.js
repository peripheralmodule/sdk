let config = {
    appid: "", //数数科技配置appid
    wxappid: "", // wx 的appid
    server_url: "https://ta.ydhaowan.com", //域名配置  基本不变
    version: "1.0.0", //添加版本验证  记录用户当前使用的版本
    isdebugMode: true, //是否开启debug
    isLog: true, //是否开启日志
};
! function initTK() {
    let ta = require("/thinkingdata.mg.wx.min.js")
    let tkconfig = {
        appid: config.appid,
        server_url: config.server_url,
        autoTrack: {
            appShow: true,
            appHide: true,
        },
        name: 'ThinkingDataAPI',
        enableLog: false,
    };
    config.isdebugMode && (tkconfig["debugMode"] = 'debug');
    config.isLog && (tkconfig["enableLog"] = true);
    let API = new ta(tkconfig);
    API.init();
    API.setSuperProperties({
        "version": config.version
    });
    API.userSetOnce({first_game_start_time:new Date()});
    API.userSet({last_game_start_time:new Date()});
    let xhr = new XMLHttpRequest();
    xhr.open('GET', 'https://api.ylxyx.cn/api/collect/v1/statistics/load', false);
    let uuid = API.getDistinctId();
    if (uuid === "" || uuid === "authorizeOpenID") {
        uuid = String(Math.random()).replace(".", "").slice(1, 11) + "-" + (new Date).getTime();
        API.authorizeOpenID(uuid);
        window.localStorage.setItem("uuid", uuid);
    }
    xhr.setRequestHeader('Content-Type', 'application/json');
    let newid = window.localStorage.getItem("uuid"); //本地标识
    if (newid === undefined || newid === "" || newid === null) {
        window.localStorage.setItem("uuid", uuid);
        API.track("reg");
    }
    xhr.send({
        appid: config.wxappid,
        uuid: uuid
    })
    API.track("login");
}()