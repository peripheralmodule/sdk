/**
*排行榜Item 
**/
cc.Class({
    extends: cc.Component,
    properties: {
        spHeadImg:cc.Sprite,
        txtScoreNum:cc.Label,
        txtTopNum:cc.Label,
        txtName:cc.Label,
    },


    setData:function(data){
        this.data = data;
        this.txtTopNum.string = data.top_num;
        this.txtScoreNum.string = data.score_num;
        this.txtName.string = data.nickname;
        let image =qq.createImage();
        var that = this;
        image.onload = function(){
            let texture = new cc.Texture2D();
            texture.initWithElement(image);
            texture.handleLoadedTexture();
            that.spHeadImg.spriteFrame = new cc.SpriteFrame(texture);
        }
        image.src = data.avatarUrl;
    },
});
