
cc.Class({
    extends: cc.Component,

    properties: {
        pbBreak:cc.ProgressBar,
        spBox:cc.Sprite,
        sfBoxs:[cc.SpriteFrame],
        nodeGolds:cc.Node,
        nodeAwards:cc.Node,
        lbAwards:cc.Label,
        spLight:cc.Sprite,
        sHit:{
            type:cc.AudioClip,
            default:null,
        },
        sAward:{
            type:cc.AudioClip,
            default:null,
        },
    },
    showView(){
        //奖励数据自己定义,此处只供参考
        this.awards = {id:1,award:[{type:"golds",value:100}],title:"金币"};
        this.node.active = true;
        this.initData();
        qq.ylBannerAdCreate(false);
    },
    initData(){
        this.isOpen = false;
        qq.ylBannerAdHide();
        this.nodeGolds.active = false;
        this.series_count = 0;
        this.nodeAwards.active = false;
        this.nodeAwards.y = 355;
        this.lbAwards.string = "+"+this.awards.award[0].value;
        this.spBox.spriteFrame = this.sfBoxs[0];
        this.count_show_banner = Math.floor(Math.random()*4)+0.25; //弹出广告的进度范围
        this.count_show_banner = this.count_show_banner > 0.8 ? 0.8 : this.count_show_banner;
        this.series_all = Math.floor(Math.random()*2)+2;//连续点击总数
        this.pbBreak.progress = 0;
    },
    update(dt){
        // if(this.node.active) 
        this.spLight.node.rotation += 1; 
    },
    onHit(){
        if(this.isOpen) return;
        Global.playSound(this.sHit);
        this.spBox.node.runAction(cc.sequence(cc.rotateTo(0.05,5),cc.rotateTo(0.05,-5),cc.rotateTo(0.05,0)));
        this.pbBreak.progress = (this.pbBreak.progress +0.02) >= 1 ? 1 : this.pbBreak.progress + 0.02;
        if(this.pbBreak.progress >= 0.2){
            this.spBox.spriteFrame = this.sfBoxs[1];
        }
        //
        let curTime = new Date().getTime();
        if(this.lastTime){
            let t_space = curTime - this.lastTime;
            if(t_space <= 250){
                this.series_count +=1;
            }else{
                this.series_count = 0;
            }
        }
        this.lastTime = curTime;
        //
        if(this.pbBreak.progress >= this.count_show_banner){
            if(this.series_count >= this.series_all || this.pbBreak.progress == 1){
                if(this.pbBreak.progress != 1) qq.ylBannerAdShow();
                this.spBox.spriteFrame = this.sfBoxs[2];
                //
                this.isOpen = true;
                console.log("---开奖---");
                this.nodeAwards.opacity = 0;
                this.nodeAwards.active = true;
                this.nodeAwards.runAction(cc.sequence(
                    cc.fadeIn(0.1),cc.delayTime(0.45),
                    cc.spawn(cc.moveTo(0.2,0,662),cc.fadeOut(0.25)),
                    cc.callFunc(function(){
                        this.onClose();
                    },this,0)));
                Global.playSound(this.sAward);
                this.nodeGolds.active = true;
                this.nodeGolds.getComponent(cc.Animation).play();
            }
        }
    },
    onClose(){
        qq.ylBannerAdHide();
        this.node.active = false;
    },
});
