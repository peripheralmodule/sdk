
cc.Class({
    extends: cc.Component,
    properties: {
        spIcon:cc.Sprite,
        lbName:cc.Label,
        spTypeIcon:cc.Sprite,
        sfType:[cc.SpriteFrame],
    },

    setInfo(obj,cb) {
        this._obj = obj;
        this._cb = cb;
        this.lbName.string = this._obj.name;
        let size = this.spTypeIcon.node.getContentSize();
        this.spTypeIcon.spriteFrame = (this._obj.getType == 'video') ? this.sfType[0] : this.sfType[1];
        this.spTypeIcon.node.setContentSize(size);
        this.loadImg(this._obj.url);
    },

    onClick(){
        console.log("onClick---id:",this._obj.id);
        //调用视频或分享，返回后发放奖励，同时对话框
        //此处的奖励是假数据，请以自己的数据为准
        let award=[{"type":"prop_3","value":1}]
        let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_GET_AWARD);
        evt.setUserData(award);
        cc.director.dispatchEvent(evt);
        if(this._cb) this._cb();
    },
    loadImg(url) {
        if (!url) {
            console.error('url数据有误: ' + JSON.stringify(this.cfg));
            return;
        }
        var self = this;
        if (!qq) {
            cc.loader.load(url, function (err, tex) {
                if (!err) {
                    var spFrame = new cc.SpriteFrame();
                    spFrame.setTexture(tex);
                    self.spIcon.spriteFrame = spFrame;
                } else {
                    console.error('拉取资源失败！');
                }
            });
        } else {
            var img = qq.createImage();
            img.onload = function() {
                var tex = new cc.Texture2D();
                tex.initWithElement(img);
                tex.url = url;
                tex.handleLoadedTexture();
                self.spIcon.spriteFrame = new cc.SpriteFrame(tex);
            }.bind(this);
            img.src = url;
        }
    },
});
