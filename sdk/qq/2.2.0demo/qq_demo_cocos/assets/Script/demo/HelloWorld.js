cc.Class({
    extends: cc.Component,

    properties: {
        label:cc.Label,
        btnShare:cc.Node,               //分享按钮

        pbYlGameOver:cc.Prefab,         //游戏结束界面
        pbGetAward:cc.Prefab,           //获得奖励View
        pbBreakEgg:cc.Prefab,           //砸金蛋
        pbOpenBox:cc.Prefab,            //开宝箱
        pbShakeTree:cc.Prefab,          //摇钱树
        preRank:cc.Prefab,              //排行榜
        btnHideAddFriend:cc.Node,
        btnDestroyFriend:cc.Node,
    },
    onLoad: function () {
        this.initEvent();
        this.initQQonshow();
        let self = this;
        this.layerId = 0;
        this.custom = 1;
        qq.ylOnPowerChange(function(type){
            console.warn("HelloWorld-ylOnPowerChange back[1:视频,2:定时自动恢复,3:ylSetPower]:",type);
        }.bind(this));
        qq.ylInitSDK(function(success){
            if(success === true){
                self.doSDKinterface();
            }
            if(success == 'config_success' || success == 'config_fail'){
                qq.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("HelloWorld ylGetPowerInfo-data",data);
                });
            }            
        });

        let btnInfo = qq.ylGetLeftTopBtnPosition();
        console.log("----btnInfo:",JSON.stringify(btnInfo));
    },

    /**
     * 初始化事件
     **/
    initEvent(){
        cc.director.on(window.Global.EVENT_SHOW_GET_AWARD, this.onShowGetAward.bind(this)); 
    },

    /**
    * 调用SDK接口
    **/
    doSDKinterface(){
        qq.ylLog("---调用接口---");
        qq.ylGetCustom(function(data){
            // if(!this.nodeBox1) this.nodeBox1 = cc.instantiate(this.pbBox1);
            // this.nodeBox1.x = 0;
            // this.nodeBox1.y = 0;
            // this.nodeBox1.parent = this.node;
            // this.nodeBox1.active = true;
            console.log("----自定义配置-data:",JSON.stringify(data));
        }.bind(this));
        qq.ylSideBox(function (data) {
        }.bind(this));

        qq.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status){
            if(status){
                //干点什么
            }
        }.bind(this));

        qq.ylGetUserQQInfo(function(userQQinfo){
            //先获取本地缓存，没有在弹窗通过用户授权获取
            console.warn("HelloWorld ylGetUserQQInfo-userQQinfo:",userQQinfo);
            if(!userQQinfo){
                console.warn("HelloWorld ylGetUserQQInfo----2:");
                 //是否收集用户信息按钮信息(具体配置请参考微信小游戏官方文档)
                qq.ylUserInfoButtonShow(
                    {       
                      type: 'image',
                      image:'./images/sp_btn_start.png',  //图片地址
                      style: {
                        left: 100,
                        top: 676,
                        width: 150,
                        height: 40,
                        lineHeight: 40,
                        borderRadius: 4
                      }
                    },
                    function(data){
                        if(data){
                            userQQinfo = data;
                            qq.ylUserInfoButtonHide();
                        }
                }.bind(this));
            }
        });
        /**
         * 获取分享图列表
         */
        qq.ylShareCard(function (shareInfo) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
                this.btnShare.active = true;
            }else{
                //获取失败
            }
        }.bind(this));
        qq.ylStatisticViedo(0,'adunit-6878a73e134f85e2');
        qq.ylStatisticShareCard(177);
        // this.testStoreValue();
        let appid = qq.ylGetAppID();
        console.log("HelloWorld ylGetAppID:",appid);
        let switchInfo = qq.ylGetSwitchInfo();
        console.log("HelloWorld ylGetSwitchInfo:",JSON.stringify(switchInfo));
        qq.ylEventCount("test-eventCount");
        qq.ylIntegralWall(function(){});
        // qq.ylGetBoardAward(123,function(){});
        qq.ylGetSignData(function(){});
        qq.ylFetchSign(1,function(){});
        let inviteAccount = qq.ylGetInviteAccount();
        console.log("HelloWorld ylGetInviteAccount:",inviteAccount);
        let userInfo = qq.ylGetUserInfo();
        console.log("HelloWorld ylGetUserInfo:",JSON.stringify(userInfo));
        qq.ylGetLayerList(function(data){
            if(data && data.length >0){
                this.layerList = data;
                qq.ylStatisticLayer(this.layerList[this.layerList.length-1].layerPath);
            }
        }.bind(this));
    },
    //显示获得奖励对话框
    onShowGetAward(evt){
        let data = evt.getUserData();
        if(!this.nodeGetAward) this.nodeGetAward = cc.instantiate(this.pbGetAward);
        this.nodeGetAward.x = 0;
        this.nodeGetAward.y = 0;
        this.nodeGetAward.zorder = 20;
        this.nodeGetAward.parent = this.node;
        this.nodeGetAward.getComponent('ylGetAward').showView(data);
    },
    showTip(tip){

    },
    onChangeView(){
        qq.ylChangeView(true);
    },

    //分享-默认模板
    onClickShare(){
        let shareInfo = {
            channel:'aa',
            module:'bb',
            scene:'MainScene',
            inviteType:'award_id',
        };
        qq.ylShareAppMessage(function(sResult){
            if(sResult.sSuccess){
                let result = qq.ylRewardByVideoOrShare(false);
                let slLimit = qq.ylGetVSLimit();
                console.log("--onClickShare-result:",result,slLimit);
            }
            console.log("-onClickShare--------分享结果-sResult:",sResult);
        },shareInfo);
    },

    //砸金蛋
    onClickBreakEgg(){
        if(!this.nodeBreakEgg) this.nodeBreakEgg = cc.instantiate(this.pbBreakEgg);
        this.nodeBreakEgg.parent = this.node;
        this.nodeBreakEgg.getComponent('ylBreakEgg').showView();
    },
    //开宝箱
    onOpenBox(){
        if(!this.nodeOpenBox) this.nodeOpenBox = cc.instantiate(this.pbOpenBox);
        this.nodeOpenBox.parent = this.node;
        this.nodeOpenBox.getComponent('ylOpenBox').showView();
    }, 
    //摇钱树
    onShakeTree(){
        if(!this.nodeShakeTree) this.nodeShakeTree = cc.instantiate(this.pbShakeTree);
        this.nodeShakeTree.parent = this.node;
        this.nodeShakeTree.getComponent('ylShakeTree').showView();
    }, 
    //显示获胜结算对话框
    onShowOverWin(){
        this.onShowGameOver(true);  
    },
    //视频分享策略
    onShowShareOrVideo(){
        qq.ylShowShareOrVideo("chanel_2","model_2",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    break;
                case 2:
                    console.warn("------策略-视频");
                    break;
            }
        }.bind(this));
    },
    //显示失败结算对话框
    onShowOverFail(){
        this.onShowGameOver(false);  
    },
    onShowGameOver(isWin){
        let obj = {
                hasWin:isWin,                    
                custom:2,
                golds:1000,
                extra:[
                    {id:0,url:"https://ydhwimg.szvi-bo.com/wx535481e2c3ff79bc/sidebox/11568198322531.jpg",name:"道具补充",getType:"video"},
                    {id:1,url:"https://ydhwimg.szvi-bo.com/wx535481e2c3ff79bc/sidebox/11569753241448.png",name:"钻石奖励",getType:"video"},
                    {id:2,url:"https://ydhwimg.szvi-bo.com/wx535481e2c3ff79bc/sidebox/21568971486805.png",name:"全体回复",getType:"share"}
                ]
            };
        if(!this.nodeYlGameOver) this.nodeYlGameOver = cc.instantiate(this.pbYlGameOver);
        this.nodeYlGameOver.x = 0;
        this.nodeYlGameOver.y = 0;
        this.nodeYlGameOver.parent = this.node;
        this.nodeYlGameOver.getComponent('ylGameOver').showView(obj);//结束对话框
    },
    //显示视频广告
    onShowVideoAd(){
        qq.ylBannerAdHide();
        // qq.ylCreateVideoAd();
        // qq.ylShowVideoAd(function(){},this.custom);
        qq.ylShowVideoAd2({
            callBack:function(){}, 
            unlockCustomNum:this.custom,
            getPower:true
        });
    },
    //显示Banner广告
    onShowBannerAd(){
        if(this.custom === 1){
            qq.ylBannerAdCreate(true,function(success){
                if(success){
                    //创建成功
                }else{
                    //创建失败
                }
            },false);
        }else if(this.custom === 2){
            qq.ylBannerAdCreateSmall(true,function(success){
                if(success){
                    //创建成功
                }else{
                    //创建失败
                }
            },true);
        }else{
            qq.ylBannerAdCreateByStyle({
                left: 20,
                top: 100,
                width: 310,
            }, true, function(res){
                
            }, true,function(res){
                console.warn("HelloWorld ylBannerAdCreateByStyle-onResize-back:",JSON.stringify(res));
                qq.ylChangeBannerStyle({
                    left: 0,
                    top:350,
                    height: 30,
                });
            });
        }
        this.custom +=1;
        this.custom = (this.custom > 3) ? 1 : this.custom;
        // qq.ylBannerAdShow();
    },
    //隐藏Banner广告
    onHideBannerAd(){
        qq.ylBannerAdHide();
    },
    onShowInterstitialAd(){
        console.log("onShowInterstitialAd custom:",this.custom);
        qq.ylCreateInterstitialAd(false,function(msg){
                    console.log("HelloWorld onShowInterstitialAd-msg:",msg);
                });
        qq.ylShowInterstitialAd(function(){});
        // switch(this.custom){
        //     case 0:
        //         qq.ylStatisticViedo(0,'afeb45aa98ec48e01d8e64030545ced6',function(){

        //         });
        //     break;
        //     case 1:
        //         qq.ylStatisticViedo(1,'afeb45aa98ec48e01d8e64030545ced6',function(){
                    
        //         });
        //     break;
        //     case 2:
        //         qq.ylStatisticBanner(0,function(){
                    
        //         });
        //     break;
        //     case 3:
        //         qq.ylStatisticBanner(1,function(){
                    
        //         });
        //     break;
        //     case 4:
        //         qq.ylCreateInterstitialAd(true,function(msg){
        //             console.log("HelloWorld onShowInterstitialAd-msg:",msg);
        //         });
        //     break;
        // }
        // this.custom += 1;
        // if(this.custom === 5) this.custom = 0;
    },
    //游戏结束
    onOver(){
        this.custom +=1;
        qq.ylOverGame(this.custom);
        
        if(this.layerList && this.layerList.length > 0){
            let layer = this.layerList[this.layerId];
            let _layerPath = layer.layerPath || 'default';
            qq.ylStatisticLayer(_layerPath,function(data){                
            });
            this.layerId +=1;
            this.layerId = (this.layerId >= this.layerList.length) ? 0 : this.layerId;
        }
    },
    //获取深度误触开关
    onGetDeepTouch(){
       let info = qq.ylGetDeepTouch(this.custom);
       console.log("onGetDeepTouch-info: ",JSON.stringify(info));
    },
    //消耗体力
    onConsumePower(){
       let power =  qq.ylGetPower();
       qq.ylSetPower(power -1);
    },
    onAddPower(){
        let power =  qq.ylGetPower();
        qq.ylSetPower(power +1);
    },
    onChangeView(){
        qq.ylChangeView();
    },
    onVideoUnlock(){
        let unlock = qq.ylVideoUnlock(this.custom);
        console.warn("HelloWorld-onVideoUnlockCustoms-是否解锁:",unlock);
    },
    //注册小游戏回到前台的事件监听
    initQQonshow(){
        var that = this;
        qq.ylLog("---注册小游戏回到前台的事件监听---",'log');
        qq.onShow((res) => {
            qq.ylLog("---小游戏回到前台的事件监听:",'log');
        })
    },
    //分享(邀请类模板)
    shareByTemplateId(){
        qq.ylShareByTemplateId(
            {
                shareTemplateId: "EE558DDCEFB407FD811CC6C06181D6AF", //模板ID请改成自己游戏的ID
                shareTemplateData: { txt1: "老铁就等你了，快上车!", txt2: "应邀前往" }//分享内容请CP根据需求定义
            },
            function(status){
            console.log("---------分享结果:",status);
        });
    },
    //添加彩签
    onColorSign(){
        qq.ylAddColorSign(function(status){
            if(status){
                //添加成功
            }else{
                //添加失败
            }
        });
    },
    //添加桌面图标
    onSaveAppToDesktop(){
        qq.ylSaveAppToDesktop(function(status){
            if(status){
                //添加成功
            }else{
                //添加失败
            }
        });
    },
    //订阅消息
    onSubscribeAppMsg(){
        qq.ylSubscribeAppMsg(true,function(status){
            if(status){
                //订阅成功
            }else{
                //订阅失败
            }
        });
    },
    //显示排行榜
    onShowRank(){
        if(!this.nodeRank){
            this.nodeRank = cc.instantiate(this.preRank);
            this.nodeRank.parent = this.node;
        }
        this.nodeRank.getComponent('nodeRank').onShow();
    },
    //创建盒子广告
    onCreateAppBox(){
        qq.ylCreateAppBox(false,function(res){
            // [type:0:创建或展示失败、1:创建或展示成功、2:关闭]
            switch(res){
                case 0:
                console.log("ylCreateAppBox-创建或展示失败");
                break;
                case 1:
                console.log("ylCreateAppBox-创建或展示成功");
                    qq.ylShowAppBox();
                break;
                case 2:
                console.log("ylCreateAppBox-关闭");
                break;
            }
        });
    },
    //显示盒子广告
    onShowAppBox(){
        qq.ylShowAppBox();
    },
    //测试自定义空间值
    testStoreValue(){
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    },
    test_sv_string(){
        //String
        qq.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                qq.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    },
    test_sv_list(){
        //List
        qq.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qq.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        qq.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                qq.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 qq.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qq.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        qq.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    qq.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    qq.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            qq.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    },
    test_sv_set(){
        //Set
        qq.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        qq.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                qq.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                qq.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        qq.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                qq.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        qq.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            qq.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    },
    test_sv_hash(){
        //litMap
        qq.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                qq.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        qq.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            qq.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qq.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            qq.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                            },
                            function(status){
                                
                            }.bind(this));
                            qq.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                qq.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    },
    test_sv_radom(){
        //testRandom
        qq.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    },
    createAddFriendButton(){
        qq.ylCreateAddFriendButton({
          type: 'text',
          text: '添加好友',
          openId:"6A43B93B26F2562B65D317398B58C9FE",
          style: {
            left: 10,
            top: 276,
            width: 200,
            height: 40,
            lineHeight: 40,
            backgroundColor: '#ff0000',
            color: '#ffffff',
            textAlign: 'center',
            fontSize: 16,
            borderRadius: 4
          }
        },
        true,
        function(){
            this.btnHideAddFriend.active = true;
            this.btnDestroyFriend.active = true;
        }.bind(this));
    },
    showAddFriendBtn(){
        qq.ylShowAddFriendButton();
        this.btnHideAddFriend.active = true;
        this.btnDestroyFriend.active = true;
    },
    hideAddFriendBtn(){
        qq.ylHideAddFriendButton();
        this.btnHideAddFriend.active = false;
        this.btnDestroyFriend.active = false;
    },
    destroyAddFriendBtn(){
        qq.ylDestroyAddFriendButton();
        this.btnHideAddFriend.active = false;
        this.btnDestroyFriend.active = false;
    },
    
});
