exports.ylsdk_app_id = "1109612091"; //游戏APPID
exports.ylsdk_version = "1.0.0"; 	//游戏版本号
exports.ylsdk_debug_model = true;   //是否debug模式 [true:打印日志,false:不打印日志]
exports.login_by_code = false;      //配置为true，则每次都会走平台登录

exports.side_min_num = 20;		//侧边栏列表item最小保留数(基于曝光策略)
exports.ylsdk_banner_ids = [	//banner广告ID
		'acfa9a46408185a6ac7ac9b82521efb8',
		'fd97e2bcedc11260e8796ba3c6defdfb',
		'42eb6853f9a04d779d052653accdedd2'
]; 
exports.ylsdk_video_ids = [		//激励视频广告ID
		'afeb45aa98ec48e01d8e64030545ced6'
];							  
exports.ylsdk_grid_ids = [		//格子广告ID(appBox)
	'd26d39be40c18d6e56cb862d120da29c'
];
exports.ylsdk_interstitial_ids = [	//插屏广告ID
	"36bf954e345166c6b8b39c90522026d2"
];
exports.ylsdk_block_ids = [	//积木广告ID
	"f59bd03b36d3b2ab9f5d3bea650cbcee"
];

exports.ylsdk_pkg_name=""; //游戏包名(OPPO、VIVO)

/****************微信公众平台配置以下合法域名************************/

// 服务器地址：		   https://api.ylxyx.cn
// 图片服务器地址1：	   https://ql.ylxyx.cn
// 图片服务器地址2：     https://tx.ylxyx.cn
// 图片服务器地址3：	   https://ext.ylxyx.cn

/*****************************************************************/


