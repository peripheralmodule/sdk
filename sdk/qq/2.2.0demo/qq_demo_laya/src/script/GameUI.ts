import { ui } from "./../ui/layaMaxUI";
/**
 * 本示例采用非脚本的方式实现，而使用继承页面基类，实现页面逻辑。在IDE里面设置场景的Runtime属性即可和场景进行关联
 * 相比脚本方式，继承式页面类，可以直接使用页面定义的属性（通过IDE内var属性定义），比如this.tipLbll，this.scoreLbl，具有代码提示效果
 * 建议：如果是页面级的逻辑，需要频繁访问页面内多个元素，使用继承式写法，如果是独立小模块，功能单一，建议用脚本方式实现，比如子弹脚本。
 */
export default class GameUI extends ui.test.TestSceneUI {
    /**设置单例的引用方式，方便其他类引用 */
    static instance: GameUI;
    private custom:number = 0;
    private layerList:Array<any> = null;
    private layerId:number = 0; //流失列表Index-测试用
    constructor() {
        super();
        GameUI.instance = this;
        //关闭多点触控，否则就无敌了
        Laya.MouseManager.multiTouchEnabled = false;
    }

    onEnable(): void {
        qq.ylInitSDK(function(success:any){
            if(success === true){
                this.doSDKinterface();
            }
            if(success == 'config_success' || success == 'config_fail'){
                qq.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("HelloWorld ylGetPowerInfo-data",data);
                });
            } 
        }.bind(this));
        this.initUI();
    }

    initUI():void{
        this.btnShare.on(Laya.Event.CLICK,this,this.onClickShare);
        this.btnShareByTemplateId.on(Laya.Event.CLICK,this,this.shareByTemplateId);
        this.btnVideoOrShare.on(Laya.Event.CLICK,this,this.onShowShareOrVideo);
        this.btnSaveAppToDesktop.on(Laya.Event.CLICK,this,this.onSaveAppToDesktop);
        this.btnSubscribeAppMsg.on(Laya.Event.CLICK,this,this.onSubscribeAppMsg);
        this.btnColorSign.on(Laya.Event.CLICK,this,this.onColorSign);
        this.btnOver.on(Laya.Event.CLICK,this,this.onOver);
        this.btnShowBanner.on(Laya.Event.CLICK,this,this.onShowBanner);
        this.btnHideBanner.on(Laya.Event.CLICK,this,this.onHideBanner);
    }

    doSDKinterface():void{
        console.log("---调用接口---");
        qq.ylSideBox(function (data:any) {
        }.bind(this));
        qq.ylGetCustom(function(data:any){
        }.bind(this));
        qq.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status:any){
            if(status){
                //干点什么
            }
        }.bind(this));

        let userQQinfo = qq.ylGetUserQQInfo();//先获取本地缓存，没有在弹窗通过用户授权获取
        // if(!userQQinfo){
        //     //  是否收集用户信息按钮信息(具体配置请参考微信小游戏官方文档)
        //     qq.ylUserInfoButtonShow(
        //        {       
        //          type: 'image',
        //          image:'./images/sp_btn_start.png',  //图片地址
        //          style: {
        //            left: 100,
        //            top: 260,
        //            width: 150,
        //            height: 40,
        //            lineHeight: 40,
        //            borderRadius: 4
        //          }
        //        },
        //        function(data:any){
        //            if(data){
        //                userQQinfo = data;
        //                qq.ylUserInfoButtonHide();
        //            }
        //     }.bind(this));
        // }
        let loginInfo = qq.ylGetUserInfo();//获取登录信息，包括微信oppid和code(code只有第一次登录才能取到)
        console.log("---登录信息-loginInfo:",JSON.stringify(loginInfo));
        var that = this;
        /**
         * 获取分享图列表
         */
        qq.ylShareCard(function (shareInfo:any) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
            }else{
                //获取失败
            }
        }.bind(this),null);
        qq.ylStatisticViedo(0,'adunit-6878a73e134f85e2',null);
        qq.ylStatisticShareCard(177);
        qq.ylGetLayerList(function(data){
            if(data && data.length >0){
                this.layerList = data;
                qq.ylStatisticLayer(this.layerList[this.layerList.length-1].layerPath);
            }
        }.bind(this));
        // this.testStoreValue();
    }
    //分享
    onClickShare():void{
        let shareInfo = {
            channel:"chanel_1",
            module:"module_1",
            scene:'MainScene',
            inviteType:'award_id',
            shareAppType:'wechatMoment',
            entryDataHash:'entryDataHash',
        };
        qq.ylShareAppMessage(function(status){
            console.log("---------分享结果:",status);
            let result = qq.ylRewardByVideoOrShare(false);
            let slLimit = qq.ylGetVSLimit();
            console.log("---------ylRewardByVideoOrShare-result:",result,slLimit);
        },shareInfo);
    }
    //分享(邀请类模板)
    shareByTemplateId():void{
        qq.ylShareByTemplateId(
            {
                shareTemplateId: "EE558DDCEFB407FD811CC6C06181D6AF", //模板ID请改成自己游戏的ID
                shareTemplateData: { txt1: "老铁就等你了，快上车!", txt2: "应邀前往" },//分享内容请CP根据需求定义
                channel:"chanel_1",
                module:"module_1",
                inviteType:'award_id',
                shareAppType:'wechatMoment',
                entryDataHash:'entryDataHash'
            },
            function(status){
            console.log("---------分享结果:",status);
        });
    }
    //添加彩签
    onColorSign():void{
        qq.ylAddColorSign(function(status){
            if(status){
                //添加成功
            }else{
                //添加失败
            }
        });
    }
    //添加桌面图标
    onSaveAppToDesktop():void{
        qq.ylSaveAppToDesktop(function(status){
            if(status){
                //添加成功
            }else{
                //添加失败
            }
        });
    }
    //视频分享策略
    onShowShareOrVideo():void{
        qq.ylShowShareOrVideo("chanel_2","model_2",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    break;
                case 2:
                    console.warn("------策略-视频");
                    break;
            }
        }.bind(this));
    }
    //订阅消息
    onSubscribeAppMsg():void{
        qq.ylSubscribeAppMsg(true,function(status){
            if(status){
                //订阅成功
            }else{
                //订阅失败
            }
        });
    }
    onShowBanner(e:Laya.Event):void{
        console.log("GameUI-onShowBanner:");
        // qq.ylBannerAdCreate(true,function(){            
        // });
        // qq.ylBannerAdCreateByStyle({
        //     left: 20,
        //     top: 100,
        //     width: 310,
        // }, true, function(res){
            
        // }, true,function(res){
        //     console.warn("GameUI-ylBannerAdCreateByStyle-onResize-back:",JSON.stringify(res));
        //     qq.ylChangeBannerStyle({
        //         left: 0,
        //         top:350,
        //         height: 30,
        //     });
        // });
        let _left = Laya.Browser.clientWidth/2;
        qq.ylCreateBlockAd({  
            size: 5,  
            orientation: 'landscape',  
            style: {left:_left, top:106},
            isShow:false,
            callback:function(res){
                console.log("GameUI ylCreateBlockAd-status:",JSON.stringify(res));
                // [0:创建,1:加载成功,2:展示,3:onResize回调,4:onError回调]
                switch (res.type){
                    case 0:
                        //创建
                        break;
                    case 1:
                        //加载成功
                        qq.ylShowBlockAd();
                        break;
                    case 2:
                        //展示
                        break;
                    case 3:
                        //onResize回调
                        break;
                    case 4:
                        //onError回调
                        break;
                }
            }
        });
        
    }
    onHideBanner(e:Laya.Event):void{
        qq.ylHideBlockAd();
    }
    //游戏结束
    onOver():void{
        this.custom +=1;
        qq.ylOverGame(this.custom);

        let lastCustom = qq.ylGetPlayCustom(1);
        let maxCustom = qq.ylGetPlayCustom(2);
        let watchTvNum = qq.ylGetWatchTvNum();
        let todayPCount = qq.ylGetPlayCount(1);
        let totalPCount = qq.ylGetPlayCount(2);
        console.log("YLSDK onOver-lastCustom,maxCustom,watchTvNum,todayPCount,totalPCount:",lastCustom,maxCustom,watchTvNum,todayPCount,totalPCount);
        if(this.layerList && this.layerList.length > 0){
            let layer = this.layerList[this.layerId];
            let _layerPath = layer.layerPath || 'default';
            qq.ylStatisticLayer(_layerPath);
            this.layerId +=1;
            this.layerId = (this.layerId >= this.layerList.length) ? 0 : this.layerId;
        }
    }
    //测试自定义空间值
    testStoreValue():void{
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    }
    test_sv_string():void{
        //String
        qq.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                qq.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    }
    test_sv_list():void{
        //List
        qq.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qq.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        qq.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                qq.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 qq.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        qq.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        qq.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    qq.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    qq.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            qq.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    }
    test_sv_set():void{
        //Set
        qq.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        qq.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                qq.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                qq.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        qq.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                qq.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        qq.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            qq.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    }
    test_sv_hash():void{
        //litMap
        qq.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                qq.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        qq.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            qq.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qq.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            qq.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            qq.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                qq.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    }
    test_sv_radom():void{
        //testRandom
        qq.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    }
}