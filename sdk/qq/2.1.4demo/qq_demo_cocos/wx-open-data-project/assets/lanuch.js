
cc.Class({
    extends: cc.Component,
    properties: {
        lay_h_score:cc.Node,
        spHeadImg:cc.Sprite,
        txtScoreNum:cc.Label,
        txtTopNum:cc.Label,
        txtName:cc.Label,
        preItem:cc.Prefab,
    },

    start () {
        this.t_key = 'h_score';//该key请根据自身的需求设置,若果有多个KEY请自行改装        
        qq.onMessage(data => {
            switch (data.message) {
                case 'Show':
                    this._getUserCloudStorage();
                    this._getFriendCloudStorage();
                    break;
            }
        });
    },

    onLoad(){
        this.getSelfInfo();
    },

    onDestroy(){
        this.allKmItemList=null;
        this.hScoreItemList=null;
        this.dataList=null;
    },

    //获取好友排行榜
    _getFriendCloudStorage(){
        qq.getFriendCloudStorage({
            keyList:[this.t_key],
            success:res=>{
                console.log("_getFriendCloudStorage-success:"+JSON.stringify(res));
                let _data = res.data;
                this.itemList = [];
                _data = this._sort(_data);
                var length = _data.length;
                this.lay_h_score.removeAllChildren();
                var that = this;
                
                for(var i=0;i<_data.length;i++){
                    var item = cc.instantiate(this.preItem);
                    let d_item = _data[i];
                    d_item.top_num = i+1;
                    if(this.selfInfo && d_item.avatarUrl == this.selfInfo.avatarUrl){
                        this.selfInfo.top_num = d_item.top_num;
                        this.txtTopNum.string = this.selfInfo.top_num;
                    }
                    item.getComponent('topItem').setData(_data[i]);
                    item.parent = this.lay_h_score;
                    this.itemList.push(item);
                }
            },
            fail:res=>{
                console.log("_getFriendCloudStorage-fail:"+JSON.stringify(res));
            }
        });
    },

    _sort(_data){
        for(var i=0;i<_data.length;i++){            
            var kvDataList = _data[i].KVDataList;
             _data[i].score_num=0;
            for(var j=0;j<kvDataList.length;j++){
                var data = kvDataList[j];
                if(data.key == this.t_key){
                    _data[i].score_num = Number(data.value);
                }
            }
        }
        function sequence_hs(a,b){
            let a_score = a.score_num;
            let b_score = b.score_num;
            if (a_score<b_score) {
              return 1;
            }else if(a_score>b_score){
              return -1
            }else{
             return 0;
            }    

        }
        _data.sort(sequence_hs);

        return _data;
    },

    //获取用户自己的数据记录
    _getUserCloudStorage(){
        let that = this;
        qq.getUserCloudStorage({
            keyList:[this.t_key],
            success:res=>{
                console.log("_getUserCloudStorage-success:"+JSON.stringify(res));
                if(res.KVDataList.length > 0){
                    that.selfInfo.score_num = res.KVDataList[0].value;
                    that.initMyRank(that.selfInfo);
                }
            },
            fail:res=>{
                console.log("_getUserCloudStorage-fail:"+JSON.stringify(res));
            }
        });
    },
    getSelfInfo(){
        let that = this;
        qq.getUserInfo({
          openIdList: ['selfOpenId'],
          lang: 'zh_CN',
          success: (res) => {
            that.selfInfo = res.data[0];
            that._getUserCloudStorage();
            that.initMyRank(that.selfInfo);
            console.log('-----getUserInfo-success', JSON.stringify(res))
          },
          fail: (res) => {
            reject(res)
          }
        });
    },
    initMyRank:function(data){
        this.data = data;
        if(data.top_num) this.txtTopNum.string = data.top_num;
        if(data.nickName) this.txtName.string = data.nickName;
        if(data.score_num) this.txtScoreNum.string = data.score_num;
        if(data.avatarUrl){
            let image =qq.createImage();
            var that = this;
            image.onload = function(){
                let texture = new cc.Texture2D();
                texture.initWithElement(image);
                texture.handleLoadedTexture();
                that.spHeadImg.spriteFrame = new cc.SpriteFrame(texture);
            }
            image.src = data.avatarUrl;
        }
    },
});
