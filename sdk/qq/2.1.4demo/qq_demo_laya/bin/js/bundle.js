(function () {
    'use strict';

    var REG = Laya.ClassUtils.regClass;
    var ui;
    (function (ui) {
        var test;
        (function (test) {
            class TestSceneUI extends Laya.Scene {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.loadScene("test/TestScene");
                }
            }
            test.TestSceneUI = TestSceneUI;
            REG("ui.test.TestSceneUI", TestSceneUI);
        })(test = ui.test || (ui.test = {}));
    })(ui || (ui = {}));

    class GameUI extends ui.test.TestSceneUI {
        constructor() {
            super();
            GameUI.instance = this;
            Laya.MouseManager.multiTouchEnabled = false;
        }
        onEnable() {
            qq.ylSetCurScene("MainScene");
            qq.ylOnPowerChange(function (type) {
                console.warn("HelloWorld-ylOnPowerChange back[1:视频,2:定时自动恢复]:", type);
            }.bind(this));
            qq.ylInitSDK(function (success) {
                if (success === true) {
                    this.doSDKinterface();
                }
                if (success == 'config_success' || success == 'config_fail') {
                    qq.ylGetPowerInfo(function (data) {
                        console.log("HelloWorld ylGetPowerInfo-data", data);
                    });
                }
            }.bind(this));
            this.initUI();
        }
        initUI() {
            this.btnShare.on(Laya.Event.CLICK, this, this.onClickShare);
            this.btnColorSign.on(Laya.Event.CLICK, this, this.onColorSign);
            this.btnShareByTemplateId.on(Laya.Event.CLICK, this, this.shareByTemplateId);
            this.btnSaveAppToDesktop.on(Laya.Event.CLICK, this, this.onSaveAppToDesktop);
            this.btnShowShareOrVideo.on(Laya.Event.CLICK, this, this.onShowShareOrVideo);
            this.btnSubscribeAppMsg.on(Laya.Event.CLICK, this, this.onSubscribeAppMsg);
        }
        doSDKinterface() {
            console.log("---调用接口---");
            qq.ylSideBox(function (data) {
            }.bind(this));
            qq.ylGetCustom(function (data) {
            }.bind(this));
            qq.ylStatisticResult({ "total_score": 123, "rebirth_score": 123 }, function (status) {
                if (status) {
                }
            }.bind(this));
            let userWXinfo = qq.ylGetUserWXInfo();
            if (!userWXinfo) {
            }
            let loginInfo = qq.ylGetUserInfo();
            console.log("---登录信息-loginInfo:", JSON.stringify(loginInfo));
            var that = this;
            qq.ylShareCard(function (shareInfo) {
                if (shareInfo) {
                    console.log("----获取分享图列表:", JSON.stringify(shareInfo));
                }
                else {
                }
            }.bind(this), 'MainScene');
            qq.ylStatisticViedo(0, 'adunit-6878a73e134f85e2', null);
            qq.ylStatisticShareCard(177);
            this.testStoreValue();
        }
        onClickShare() {
            let shareInfo = {
                channel: "chanel_1",
                module: "module_1",
                scene: 'MainScene',
                inviteType: 'award_id',
                shareAppType: 'wechatMoment',
                entryDataHash: 'entryDataHash',
            };
            qq.ylShareAppMessage(function (status) {
                console.log("---------分享结果:", status);
                let result = qq.ylRewardByVideoOrShare(false);
                let slLimit = qq.ylGetVSLimit();
                console.log("---------ylRewardByVideoOrShare-result:", result, slLimit);
            }, shareInfo);
        }
        shareByTemplateId() {
            qq.ylShareByTemplateId({
                shareTemplateId: "EE558DDCEFB407FD811CC6C06181D6AF",
                shareTemplateData: { txt1: "老铁就等你了，快上车!", txt2: "应邀前往" },
                channel: "chanel_1",
                module: "module_1",
                inviteType: 'award_id',
                shareAppType: 'wechatMoment',
                entryDataHash: 'entryDataHash'
            }, function (status) {
                console.log("---------分享结果:", status);
            });
        }
        onColorSign() {
            qq.ylAddColorSign(function (status) {
                if (status) {
                }
                else {
                }
            });
        }
        onSaveAppToDesktop() {
            qq.ylSaveAppToDesktop(function (status) {
                if (status) {
                }
                else {
                }
            });
        }
        onShowShareOrVideo() {
            qq.ylShowShareOrVideo("chanel_2", "model_2", function (type) {
                switch (type) {
                    case 0:
                        console.warn("------策略-无");
                        break;
                    case 1:
                        console.warn("------策略-分享");
                        break;
                    case 2:
                        console.warn("------策略-视频");
                        break;
                }
            }.bind(this));
        }
        onSubscribeAppMsg() {
            qq.ylSubscribeAppMsg(true, function (status) {
                if (status) {
                }
                else {
                }
            });
        }
        testStoreValue() {
            this.test_sv_string();
            this.test_sv_list();
            this.test_sv_set();
            this.test_sv_hash();
            this.test_sv_radom();
        }
        test_sv_string() {
            qq.ylStoreValue({
                name: "testString",
                cmd: "set",
                args: "测试数据"
            }, function (status) {
                qq.ylStoreValue({
                    name: "testString",
                    cmd: "get"
                }, function (status) {
                }.bind(this));
            }.bind(this));
        }
        test_sv_list() {
            qq.ylStoreValue({
                name: "testList",
                cmd: "add",
                args: "0"
            }, function (status) {
            }.bind(this));
            qq.ylStoreValue({
                name: "testList",
                cmd: "add",
                args: "2"
            }, function (status) {
            }.bind(this));
            qq.ylStoreValue({
                name: "testList",
                cmd: "set",
                args: "0,3"
            }, function (status) {
                qq.ylStoreValue({
                    name: "testList",
                    cmd: "all"
                }, function (status) {
                }.bind(this));
                qq.ylStoreValue({
                    name: "testList",
                    cmd: "get",
                    args: "0"
                }, function (status) {
                }.bind(this));
                qq.ylStoreValue({
                    name: "testList",
                    cmd: "size"
                }, function (status) {
                }.bind(this));
                qq.ylStoreValue({
                    name: "testList",
                    cmd: "poll",
                    args: "2"
                }, function (status) {
                    qq.ylStoreValue({
                        name: "testList",
                        cmd: "size"
                    }, function (status) {
                    }.bind(this));
                    qq.ylStoreValue({
                        name: "testList",
                        cmd: "replace",
                        args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                    }, function (status) {
                        qq.ylStoreValue({
                            name: "testList",
                            cmd: "all"
                        }, function (status) {
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_set() {
            qq.ylStoreValue({
                name: "testSet",
                cmd: "add",
                args: "12"
            }, function (status) {
            }.bind(this));
            qq.ylStoreValue({
                name: "testSet",
                cmd: "add",
                args: "10"
            }, function (status) {
                qq.ylStoreValue({
                    name: "testSet",
                    cmd: "exist",
                    args: "10"
                }, function (status) {
                }.bind(this));
                qq.ylStoreValue({
                    name: "testSet",
                    cmd: "size"
                }, function (status) {
                    qq.ylStoreValue({
                        name: "testSet",
                        cmd: "del",
                        args: "10"
                    }, function (status) {
                        qq.ylStoreValue({
                            name: "testSet",
                            cmd: "all"
                        }, function (status) {
                            qq.ylStoreValue({
                                name: "testSet",
                                cmd: "replace",
                                args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                            }, function (status) {
                                qq.ylStoreValue({
                                    name: "testSet",
                                    cmd: "all"
                                }, function (status) {
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_hash() {
            qq.ylStoreValue({
                name: "testHash",
                cmd: "set",
                args: "u_name,许"
            }, function (status) {
                qq.ylStoreValue({
                    name: "testHash",
                    cmd: "get",
                    args: "u_name"
                }, function (status) {
                    qq.ylStoreValue({
                        name: "testHash",
                        cmd: "replace",
                        args: "{\"u_name\":\"唐\",\"sex\":\"women\"}"
                    }, function (status) {
                        qq.ylStoreValue({
                            name: "testHash",
                            cmd: "gets",
                            args: "u_name,sex"
                        }, function (status) {
                        }.bind(this));
                        qq.ylStoreValue({
                            name: "testHash",
                            cmd: "size",
                        }, function (status) {
                        }.bind(this));
                        qq.ylStoreValue({
                            name: "testHash",
                            cmd: "values",
                            args: "sex"
                        }, function (status) {
                        }.bind(this));
                        qq.ylStoreValue({
                            name: "testHash",
                            cmd: "del",
                            args: "u_name"
                        }, function (status) {
                            qq.ylStoreValue({
                                name: "testHash",
                                cmd: "all",
                            }, function (status) {
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_radom() {
            qq.ylStoreValue({
                name: "testRandom"
            }, function (status) {
            }.bind(this));
        }
    }

    class GameConfig {
        constructor() {
        }
        static init() {
            var reg = Laya.ClassUtils.regClass;
            reg("script/GameUI.ts", GameUI);
        }
    }
    GameConfig.width = 640;
    GameConfig.height = 1136;
    GameConfig.scaleMode = "fixedwidth";
    GameConfig.screenMode = "none";
    GameConfig.alignV = "top";
    GameConfig.alignH = "left";
    GameConfig.startScene = "test/TestScene.scene";
    GameConfig.sceneRoot = "";
    GameConfig.debug = false;
    GameConfig.stat = false;
    GameConfig.physicsDebug = false;
    GameConfig.exportSceneToJson = true;
    GameConfig.init();

    class Main {
        constructor() {
            if (window["Laya3D"])
                Laya3D.init(GameConfig.width, GameConfig.height);
            else
                Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
            Laya["Physics"] && Laya["Physics"].enable();
            Laya["DebugPanel"] && Laya["DebugPanel"].enable();
            Laya.stage.scaleMode = GameConfig.scaleMode;
            Laya.stage.screenMode = GameConfig.screenMode;
            Laya.stage.alignV = GameConfig.alignV;
            Laya.stage.alignH = GameConfig.alignH;
            Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
            if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
                Laya.enableDebugPanel();
            if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
                Laya["PhysicsDebugDraw"].enable();
            if (GameConfig.stat)
                Laya.Stat.show();
            Laya.alertGlobalError = true;
            Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
        }
        onVersionLoaded() {
            Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
        }
        onConfigLoaded() {
            GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
        }
    }
    new Main();

}());
