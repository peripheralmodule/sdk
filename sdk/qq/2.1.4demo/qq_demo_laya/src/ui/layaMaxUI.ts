/**This class is automatically generated by LayaAirIDE, please do not make any modifications. */
import View=Laya.View;
import Dialog=Laya.Dialog;
import Scene=Laya.Scene;
var REG: Function = Laya.ClassUtils.regClass;
export module ui.test {
    export class TestSceneUI extends Laya.Scene {
		public btnShare:Laya.Button;
		public btnShareByTemplateId:Laya.Button;
		public btnColorSign:Laya.Button;
		public btnSaveAppToDesktop:Laya.Button;
		public btnShowShareOrVideo:Laya.Button;
		public btnSubscribeAppMsg:Laya.Button;
        constructor(){ super()}
        createChildren():void {
            super.createChildren();
            this.loadScene("test/TestScene");
        }
    }
    REG("ui.test.TestSceneUI",TestSceneUI);
}