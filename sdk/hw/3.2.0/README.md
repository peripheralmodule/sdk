1.文件介绍：
	1.1 SDK文件：ydhw.huawei.sdk.min.js
	1.2 SDK配置文件：config.js
	1.3 .d.ts提示文件：ydhw.interface.d.ts
	1.4 枚举和接口实现文件：ydhw.sdk.ts
	# 1.5 数数wx平台引用:thinkingdata.mg.wx.min.js
	# 1.6 数数科技配置引入：tk.config.wx.js

2.引入步骤:
	2.1 拷贝ydhw.huawei.sdk.min.js放到发布根目录下
    2.2 将config.js拷贝到index.js文件的最上方，并修改ydhw.huawei.sdk.min.js加载路径和游戏参数配置
    2.3 接口的解释和参数请查看ydhw.interface.d.ts文件
	2.4 接口中用到的枚举和类ydhw.sdk.ts
	//2.5 将thinkingdata.mg.wx.min.js和tk.config.wx.js放入libs目录下

3.记得添加以下合法域名：
    SDK域名         https://thirdwx.qlogo.cn
	服务器接口地址  https://api.ylxyx.cn
	七牛云CDN      https://cdn1.ydhaowan.com oss
	七牛云CDN      https://cdn2.ydhaowan.com 后台上传
	七牛云CDN      https://cdn3.ydhaowan.com 后台上传
	腾讯云CDN      https://cdn4.ydhaowan.com 后台上传
	腾讯云CDN      https://cdn5.ydhaowan.com 后台上传
	华为云CDN      https://cdn6.ydhaowan.com oss
	
	如果是oss上传的推荐使用cdn6

	https://ydhwimg.szvi-bo.com/demo/WaterParkYY/index.html 不推荐 
	https://cdn6.ydhaowan.com/demo/WaterParkYY/index.html 推荐
	
	游戏内后台 https://ydhwimg.szvi-bo.com换成https://cdn1.ydhaowan.com

	数数科技后台 https://receiver.ta.thinkingdata.cn



4.引用顺序:
	1>一定要保证config.js放在loadLib("libs/laya.core.js")的前面

