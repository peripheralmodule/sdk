!function () {
  var g = require("./yl_sdk_conf");
  var _sdk = require("./yl_sdk");
  var iKey = [
    'ylInitSDK','ylSetCurScene',
    'ylGetInviteAccount', 'ylGetUserInfo', 'ylGetSwitchInfo', 
    'ylLog', 'ylStatisticViedo', 'ylStatisticResult', 'ylStoreValue', 'ylGetAppID',
     'ylEventCount', 'ylGetCustom', 'ylShowShareOrVideo', 'ylChangeView',
    'ylGetDeepTouch', 'ylOverGame', 'ylVideoUnlock', 'ylGetPowerInfo', 'ylOnPowerChange',
    'ylSetPower', 'ylGetPower','ylSideBox', 'ylIntegralWall', 'ylGetBoardAward',
    'ylGetSignData', 'ylFetchSign', 
    'ylBannerAdCreateByStyle', 'ylBannerAdCreate', 'ylBannerAdShow','ylBannerAdHide','ylCreateVideoAd', 'ylShowVideoAd',
    'ylCreateInterstitialAd', 'ylShowInterstitialAd','ylStatisticShareCard', 'ylShareCard',
    'ylGetUserMZinfo','ylGetPlayCount','ylGetPlayCustom','ylGetWatchTvNum','ylGameLifeListen'
  ];

  var pf_data = {
      bannerAd:null,
      videoAd:null,
      interstitialAd:null,
      gridAd:null,
      bannerTimer:null,
      _timer:null,
      unlockCustomNum:false,
      userInfoBtn:null,
      onVideoClose:null,
      show_video:false,
      switchLog:g.ylsdk_debug_model,
      show_share:false,
      defaultbTS: 10,  //默认banner刷新时间间隔
      shareCallBack:null,
      onShare:null,
      hideTime:null,
      VIDEO_PLAY_FAIL: 0,      //视频广告-播放失败
      VIDEO_PLAY_FINISH: 1,    //视频广告-播放完成
      VIDEO_PLAY_CANCEL: 2,    //视频广告-播放取消
      layer:'default',
      lifeListen:null,  //平台的onShow/onHide/onError生命周期监听
  };

  var Interface = {
    /**
     * 初始化SDK
     **/
    ylInitSDK(_callback) {
      //登录
      _sdk.login(_callback);
    },
    /**
     *设置当前场景，方便打点和退出游戏跟踪
     *
     **/
    ylSetCurScene(sceneName) {
      pf_data.layer = sceneName;
      _sdk.setCurScene(sceneName);
    },
    //获取邀请账号
    ylGetInviteAccount() {
      return _sdk.getInviteAccount();
    },
    //获取用户SDK服务器账号信息
    ylGetUserInfo() {
      return _sdk.getUserInfo();
    },
    //获取开关信息
    ylGetSwitchInfo() {
      return _sdk.getSwitchInfo();
    },
    //日志输出
    ylLog(logMsg, logType) {
      _sdk.logs(logMsg, logType);
    },
    /**
     * 视频统计(打点统计）
     * @param _type 类型[0:显示视频,1:播放完成]
     * @param _adId 视频广告ID
     * @param _callback
     *
     * 返回参数说明:
     * 参数                   类型                  说明
     * code                 Integer         code=0 表示成功 其他值表示出错
     * msg                  String          code=0 时为空 其他为错误信息
     */
    ylStatisticViedo(_type, _adId, _callback){
      _sdk.tatisticViedo(_type, _adId, _callback);
    },
    /**
     * 结果统计
     * @param detail 用户该局游戏的技能及分数统计详情（每个游戏需要统计的数据不同)
     *               例如，泡泡龙大师：{"total_score": 123 , "rebirth_score": 123, "strategy": "1", "change_score": [123, 123...],"hammer_score": [123, 123, ...]}
     *               total_score 用户在游戏中一句的中分数
     *               rebirth_score 用户在一句中使用复活时的分数
     *               strategy 当前使用的策略，比如："1"
     *               change_score 换一换功能每一次使用时获得的分数
     *               hammer_score 使用锤子功能每一次使用时获得的分数
     * @param _callback
     *
     *  参数返回说明         
     *  参数名称    参数类型    说明    
     *  code    Integer code=0 表示成功 其他值表示出错 
     *  msg     String  code=0 时为空 其他为错误信息  
     */
    ylStatisticResult(_detail, _callback) {
      _sdk.statisticResult(_detail, _callback);
    },
    //自定义空间
    ylStoreValue(info, _callback) {
      _sdk.storeValue(info, _callback);
    },
    // 获取游戏APPID
    ylGetAppID() {
      return g.ylsdk_app_id;
    },
    /**
     * 事件统计
     * @param        参数         类型          说明 
     *              eventName   String         事件名
     *
     */
    ylEventCount(eventName) {
      _sdk.eventCount(eventName);
    },
    /**
     * 获取游戏自定义配置
     * @param _callback
     *
     * 返回参数说明：
     * 参数                类型           说明
     * code              Integer      code=0 表示成功 其他值表示出错
     * msg               String       code=0 时为空 其他为错误信息
       custom_config     JSON数组  游戏配置列表，详细数据见下
            |
            custom_config:
            参数           类型           说明
        param_name       String     参数名(尽量用英文，用以前端区分不同功能)
        param_value      String     参数值，前端获取该自定义数据，做不同动作
        param_describe   String     参数描述
        appid            String     游戏appid
     */
    ylGetCustom(_callback) {
      _sdk.getCustom(_callback);
    },
    /**
     *   视频分享策略
     *   channel 渠道
     *   model 模块
     **/
    ylShowShareOrVideo(channel, module, cb) {
      _sdk.showShareOrVideo(channel, module, cb);
    },
    //切换页面
    ylChangeView(misToch) {
      if (!pf_data.bannerAd) {
        if (pf_data.switchLog) console.warn("YLSDK ylChangeView-pf_data.bannerAd is ", pf_data.bannerAd);
        return;
      }
      let gConfig = _sdk.getGameConfig();
      if (gConfig && gConfig.bannerTimeSpace && gConfig.bannerTimeSpace.forceOnOff) {
        let btSpace = gConfig.bannerTimeSpace;
        let nowTime = new Date().getTime();
        let lastTime = pf_data.bannerAd.lastTime || nowTime; //最后一次刷新banner
        let forceTimeSpace = btSpace.forceTimeSpace || pf_data.defaultbTS; //强制刷新时间隔(秒)
        console.warn("YLSDK ylChangeView-forceTimeSpace:", (nowTime - lastTime), " >= ", forceTimeSpace * 1000);
        if (nowTime - lastTime >= forceTimeSpace * 1000) {
          //强制刷新banner
          let isSmall = true;
          if (pf_data.bannerAd.show_banner) {
            isSmall = pf_data.bannerAd.isSmall || false;
            p_f.createBannerAd(isSmall, true, null, pf_data.bannerAd._style, misToch);
          } else {
            pf_data.bannerAd.lastTime = nowTime - (btSpace.timeSpace * 1000 + 1000);//保证下次showBanner后可以立马刷新
          }
        }
      }
    },
    //获取深度误触屏蔽开关状态
    ylGetDeepTouch(customNum) {
      return _sdk.getDeepTouch(customNum);
    },
    //一局游戏结束
    ylOverGame(customNum) {
      _sdk.overGame(customNum);
    },
    //视频解锁
    ylVideoUnlock(customNum) {
      return _sdk.videoUnlock(customNum);
    },
    //获取体力相关信息
    ylGetPowerInfo(_callback) {
      _sdk.getPowerInfo(_callback);
    },
    //体力变化监听
    //type[1:视频,2:定时自动恢复]
    ylOnPowerChange(_callback) {
      _sdk.onPowerChange(_callback);
    },
    //设置体力
    ylSetPower(powerNum) {
      _sdk.setPower(powerNum);
    },
    //获取体力
    ylGetPower() {
      return _sdk.getPower();
    },
    /***
	   * 生命周期监听
	   * 
	   * @param _callback
	   * 
	   * 参数返回说明:         
	   *  参数名称       参数类型          说明     
	   *  type            String     'onShow'、'onHide'、'onError'三个状态
	   *  res             any         onShow和onError为回调返回参数，onHide是为空
    */
    ylGameLifeListen(_callback){
      pf_data.lifeListen = _callback;
    },
    /**
     * 获取侧边栏列表
     * @param version 游戏版本号
     * @param _callback
     *
     *
     * 返回参数说明：
     * 参数                  类型                           说明
     * code                 Integer            code=0 表示成功，其他值表示出错
     * msg                  String             code=0 时为空，其他为错误信息
     * boxconfig            JSON数组            侧边栏数据，详细数据见下
     *  |
     *  boxconfig：
     *            id                  Integer             项目id
     *     *<----type                 Integer             该项目类型，0表示小程序，1表示图片
     *     |     title                String              标题
     *     |     icon                 String              图标url
     *     |     status               Integer             该项目是否展示，0不展示，1展示
     *     |     data                 JSON                该项目进一步数据，具体见下
     *     |      |
     *     |      data：
     *     *-------->type=0 时
     *                       appid               String               跳转小程序appid
     *                       path                String               跳转小程序path
     *                       shield_ios          Integer              是否屏蔽ios，0不屏蔽，1屏蔽
     *               type=1 时
     *                       img                 String               图片url
     */
    ylSideBox(_callback) {
      _sdk.getSideBox(_callback);
    },
    /**
     * 获取积分墙列表
     * @param _callback
     *
     *
     * 参数返回说明:         
     *  参数名称       参数类型      说明    
     *  code         Integer     code=0 表示成功 其他值表示出错 
     *  msg          String      code=0 时为空 其他为错误信息  
     *  boardconfig  JSON数组    积分墙数据，详细数据见下 
     *   |
     *  boardconfig:  |  对象    |      类型    |   说明
                     ----------  | ----------- | -----------
     *                  id       |   Integer   |  项目id，前端按此值从小到大排序
     *                  status   |   String    |  奖励状态，open表示可领取，accepted表示已领取
     *                  title    |   String    |  标题
     *                  award    |   JSON数组  |   奖励数据，具体见下
     *                  play_time|   Integer   |  试玩多久得到奖励，单位秒
     *                  type     |   Integer   |  0表示小程序
     *                  icon     |   String    |  图标url
     *                  data     |   JSON      |  该项目进一步数据，具体见下
     *                  describe |   String    |  该项目的描述信息
     *
     *  award:           对象   |    类型     |  说明
     *              ----------  | -----------| -----------
     *                  type    |  String    |  奖励类型
     *                  value   |  String    |  奖励具体数值
     *
     *  data:(type=0 时)  对象     |    类型    |     说明
     *                ----------  | -----------| -----------
     *                  jump_appid|   String   |  跳转小程序appid
     *                  jump_path |   String   |  跳转小程序path
     *                  shield_ios|   Integer  |  是否屏蔽ios，0不屏蔽，1屏蔽
     */
    ylIntegralWall(_callback) {
      _sdk.getIntegralWall(_callback);
    },
    /**
     * 领取积分墙奖励
     * @param _id 积分墙ID
     * @param _callback
     *
     *  响应结果
     *   参数名称             参数类型               说明  
     *   code                Integer          code=0表示成功，其他值表示出错
     *   msg                 String           code=0 时为空 其他为错误信息  
     *   id                  Integer          项目id(IntegralWall接口中返回的boardconfig数组中的项目id，具体请看该接口文档说明)  
     *   status              String           奖励状态，open表示可领取，accepted表示已领取  
     *   award               JSON数组          奖励数据，具体见下
     *    |
     *   award:    对象         类型          说明
     *             type        String      奖励类型
     *             value       String      奖励具体数值
     *
    **/
    ylGetBoardAward(_id, _callback) {
      _sdk.getBoardAward(_id, _callback);
    },
    /**
     * 获取签到列表
     * @param _callback
     * 
     * 返回参数说明：
     * |参数               |类型         |说明
        ----------        | ----------- | -----------
        code              | Integer     | code=0表示成功 其他值表示出错
        msg               | String      | code=0时为空 其他为错误信息
        signconfig        | JSON数组     | 按目前7天一个周期的话，数组里面有7个json格式的值，每个具体值中id表明是第几天，比如1表示第1天，详细数据见下 
            |
            signconfig:
                |参数      |类型         |说明
                id        | Integer      | id表明是第几天，比如1表示第1天 
                state     | String       | 表示状态，"accepted"表示已经领取，"close"表示不可领取，"open"表示可领取 
                award     | JSON数组     | 表示具体是什么奖励，水果的奖励类型是gold（金币）和skin（皮肤），详细数据见下 
                    |
                    award:
                    |参数     |类型         |说明
                    type     | String      | 奖励类型 
                    value    | Integer     | 奖励具体数值 

     * 
     */
    ylGetSignData(_callback) {
      _sdk.getSignData(_callback);
    },
    /**
     * 领取签到奖励
     * @param _id  第几天，1第1天，2第二天，依此类推
     * @param _callback
     * 
     * 返回参数说明：
     * |参数               |类型         |说明
        ----------        | ----------- | -----------
        code              | Integer     | code=0表示成功 其他值表示出错
        msg               | String      | code=0时为空 其他为错误信息
        id            | Integer      | id表明是第几天，比如1表示第1天
        state    | String      | 表示状态，"accepted"表示已经领取，"close"表示不可领取，"open"表示可领取
        award | JSON数组      | 表示具体是什么奖励，水果的奖励类型是gold（金币）和skin（皮肤），详细数据见下
            |
            award:
              |参数    |类型            |说明
               type    String          奖励类型 
               value   Integer         奖励具体数值 
     * 
     */
    ylFetchSign(_id, _callback) {
      _sdk.fetchSign(_id, _callback);
    },
    /**
     * 获取分享图列表
     * @param _callback
     * @param scene 获取分享图的场景，该值需要与后台管理系统配置的值保持一致
     *
     *
     * 返回参数说明:
     * 参数                   类型                  说明
     * code                 Integer             code=0 表示成功 其他值表示出错
     * msg                  String              code=0 时为空 其他为错误信息
     * sharecard_config     JSON数组             分享卡片数据，详细数据见下
     *  |
     *  sharecard_config:
     *   title                String              分享文案
     *   img                  String              图片url
     *   id                   Integer             分享卡片id
     */
    ylShareCard(_callback, scene) {
      _sdk.shareCard(_callback, scene);
    },
    //获取玩家魅族平台账号信息
    ylGetUserMZinfo() {
      return p_f.getUserPlatFormInfo();
    },
    //获取玩家玩游戏的局数
    //@param type 获取类型[1:今天玩的局数，2:总共玩的局数]
    ylGetPlayCount(type) {
    	if(type == 1){
    		return p_f.getLD('SDK_TODAY_PLAY_NUMS_ID_' + g.ylsdk_app_id);
    	}else{
    		return p_f.getLD('SDK_TOTAL_PLAY_NUMS_ID_' + g.ylsdk_app_id);
    	}
    },
    //玩家玩过的关卡数
    //@param type 获取类型[1:最后一次玩的关卡，2:总玩过的最大关卡]
    ylGetPlayCustom(type) {
    	if(type == 1){
			return  p_f.getLD('SDK_LAST_CUSTOM_ID_' + g.ylsdk_app_id);
    	}else{
    		return p_f.getLD('SDK_MAX_CUSTOM_ID_' + g.ylsdk_app_id);
    	}
    },
    //获取玩家今天看视频的次数
    ylGetWatchTvNum() {
    	return p_f.getLD('SDK_TODAY_WATCH_TV_NUMS_ID_' + g.ylsdk_app_id);
    },
    //创建banner广告(填充屏幕宽度)
    ylBannerAdCreate(show, _callback, misToch) {
      p_f.createBannerAd(false, show, _callback, null, misToch);
    },
    //创建banner自定义style
    ylBannerAdCreateByStyle(style, show, _callback, misToch) {
      p_f.createBannerAd(false, show, _callback, style, misToch);
    },
    //显示banner广告
    ylBannerAdShow() {
      p_f.showBannerAd();
    },
    //隐藏 banner广告
    ylBannerAdHide() {
      p_f.hideBannerAd();
    },
    //创建视频广告
    ylCreateVideoAd() {
      p_f.createVideoAd();
    },
    /**
        播放视频广告

        VIDEO_PLAY_FAIL:0    //视频广告-播放失败
        VIDEO_PLAY_FINISH:1  //视频广告-播放完成
        VIDEO_PLAY_CANCEL:2  //视频广告-播放取消
    **/
    ylShowVideoAd(cb, unlockCustomNum) {
      p_f.showVideoAd(cb, unlockCustomNum);
    },
    /**
     *  分享图统计（打点统计）
     * @param share_card_id 分享图id
     **/
    ylStatisticShareCard(share_card_id) {
      _sdk.statisticShareCard(share_card_id);
    },
    //创建插屏广告
    // cb   回调函数 [type:0:创建或展示失败、1:创建或展示成功、2:关闭]
    // show 是否展示 [true:展示,false:不展示]
    ylCreateInterstitialAd(show, cb) {
      console.log("YLSDK ylCreateInterstitialAd-show:", show,sys.platformVersionCode);
        let ad_index = Math.floor(Math.random() * g.ylsdk_interstitial_ids.length);
        let advert = g.ylsdk_interstitial_ids[ad_index];
        if (p_f.hasFun(qg.createInsertAd,'qg.createInsertAd')) {
          pf_data.interstitialAd = qg.createInsertAd({
            adUnitId: advert
          });
          
          pf_data.interstitialAd.onLoad(() => {
            if (show) {
              Interface.ylShowInterstitialAd(cb);
            } else {
              if (cb) cb(1);
            }
            if (pf_data.switchLog) console.log('YLSDK InterstitialAd.onLoad');
          });
          if(sys.platformVersionCode >= 1064) pf_data.interstitialAd.load();
          pf_data.interstitialAd.onClose(res => {
            if (cb) cb(2);
            if (pf_data.switchLog) console.log('YLSDK InterstitialAd.onClose');
          });
          //视频加载失败
          pf_data.interstitialAd.onError(err => {
            if (pf_data.switchLog) console.warn('YLSDK InterstitialAd.onError:', JSON.stringify(err), pf_data.interstitialAd.adUnitId);
            if (cb) cb(0);
          });
        }
    },
    //显示插屏广告
    ylShowInterstitialAd(cb) {
      if (pf_data.interstitialAd) {
        pf_data.interstitialAd.show();
        Interface.ylEventCount('showInterstitialAd');
      }
    },
  };

  for (var L = 0; L < iKey.length; L++) !
    function (e, t) {
      Object.defineProperty(qg, e, {
        value: t,
        writable: false,
        enumerable: true,
        configurable: true
      })
    }(iKey[L], Interface[iKey[L]]);

  //平台接口
  var p_f = {
    // 获取本地缓存
    getLD(_key) {
      try {
        let localData = null;
        if(p_f.hasFun(localStorage.getItem,'localStorage.getItem')) localData = localStorage.getItem(_key);
        if (localData == null || typeof localData === "undefined" ||
          localData === "undefined" || localData === "") {
          localData = '';
        }
        return localData;
      } catch (e) {
        console.error("YLSDK getLocalData error key:" + _key + ", e:", e);
      }
      return '';
    },
    // 保存本地缓存
    setLD(_key,_value) {
        try {
            if(p_f.hasFun(localStorage.setItem,'localStorage.setItem')) localStorage.setItem(_key, _value);
        } catch (e) { 
            console.error("YLSDK setLocalData error key:" + _key + ", e:", e);
        }
    },
    //平台登录
    loginPF(sdkData,_callback) {
      mz.login({
        success: (res) => {
          var _data = JSON.stringify(res);
          console.warn("YLSDK login success data = " + _data);
          if (res.token) {
            let data = {
              code: res.token,
              needPass: false,
              pkgName: sys.pkg_name
            };
            if (sdkData.loginInfo) {
              sdkData.loginInfo.token = res.token || "";
            } else {
              sdkData.loginInfo = {
                token: res.token || "",
              };
            }
            _sdk.setLoginInfo(sdkData.loginInfo);
            if (pf_data.switchLog) console.log("YLSDK YLSDK loginMZ-登录成功:", JSON.stringify(res));
            _sdk.loginSDKServer(data, _callback);
            pf_data.user_mz_info = res;
            _sdk.setUserPlatFormInfo(pf_data.user_mz_info);
            p_f.getTokenMZ();
          }
        },
        fail: (res) => {
          if (res.code == 4) {
            //用户取消登录（没输入帐号就退出）
          } else if (res.code == 20) {
            //用户点击授权弹框的取消按钮，取消个人信息授权

          } else {

          }

          var data = JSON.stringify(res);
          console.warn("YLSDK login fail data = " + data);
          p_f.getIMEI(_callback);
        },
        complete: () => {
          console.warn("YLSDK login complete");
        }
      });
    },
    //获取token
    getTokenMZ() {
      mz.getToken({
        success: (res) => {
          var _data = JSON.stringify(res);
          console.log("YLSDK getToken success data = " + _data);
          pf_data.user_mz_info.token = res.token;
          pf_data.user_mz_info.uid = res.uid;
          _sdk.setUserPlatFormInfo(pf_data.user_mz_info);
        },
        fail: (res) => {
          var data = JSON.stringify(res);
          console.log("YLSDK getToken fail data = " + data);
          if (res.code == 4) {
            //用户取消登录（没输入帐号就退出）
          } else {
          }
        },
        complete: () => {
          console.log("YLSDK getToken complete");
        }
      });
    },
    //通过IMEI登录
    getIMEI(_callback) {
      mz.getIMEI({
        success: (res) => {
          console.log("YLSDK  mz.getIMEI:" + res.imei);
          let codeORtoken = res.imei;
          let data = {
            code: codeORtoken,
            needPass: false,
            pkgName: 'anonymousCode'
          };
          let identity = "以游客身份";
          if (pf_data.loginInfo) {
            pf_data.loginInfo.code = codeORtoken || "";
            pf_data.loginInfo.token = codeORtoken || "";
            pf_data.loginInfo.pkgName = sys.pkg_name;
          } else {
            pf_data.loginInfo = {
              code: codeORtoken || "",
              token: codeORtoken || "",
              pkgName: sys.pkg_name || "",
            };
          }
          if (pf_data.switchLog) console.log("YLSDK loginInfo:", JSON.stringify(pf_data.loginInfo));
          if (pf_data.switchLog) console.log("YLSDK getIMEI-登录成功:", JSON.stringify(res));
          _sdk.loginSDKServer(data, _callback);

          pf_data.user_mz_info = res;
          _sdk.setUserPlatFormInfo(pf_data.user_mz_info);
        },
        fail: () => {
          console.log("YLSDK  mz.getIMEI fail !");
          if (_callback) _callback(false);
        }
      });
    },
    /**
     * 设置本地缓存
     */
    saveLD(_key, _value) {
      try {
        if(p_f.hasFun(localStorage.setItem,'localStorage.setItem')) localStorage.setItem(_key, _value);
      } catch (e) {
        console.error("YLSDK saveLocalData error:", e);
      }
    },
    //是否存在该函数
    hasFun(func,sFunc){
      let has = true;
      if(func){
        has = true;
      }else{
        has = false;
        console.warn("YLSDK function "+sFunc+" is ",func)
      }
      // console.log("YLSDK function "+sFunc+" is ",has)
      return has;
    },
    /**
    * 获取小游戏启动时的参数
    **/
    getLOS() {
      let launchOptions = null;
      if(p_f.hasFun(qg.getLaunchOptionsSync,'qg.getLaunchOptionsSync')) launchOptions = qg.getLaunchOptionsSync();
      return launchOptions;
    },
    //删除banner
    removeBannerAd() {
      // if (pf_data.switchLog) console.log("YLSDK BannerAd.destroy");
      if (pf_data.bannerAd) {
        if (pf_data.bannerTimer) {
          clearTimeout(pf_data.bannerTimer);
          pf_data.bannerTimer = null;
        }
        pf_data.bannerAd.hide();
        // pf_data.bannerAd.destroy();
        pf_data.bannerAd = null;
      }
    },
    //创建banner广告
    createBannerAd(isSmall, show, _callback, _style, misToch) {
      console.log("YLSDK createBannerAd-isSmall, show, _callback, _style,misToch:", isSmall, show, _callback, JSON.stringify(_style), misToch);
      if (!g.ylsdk_banner_ids || g.ylsdk_banner_ids.length == 0) {
        console.error("YLSDK 请在配置文件中配置banner ID");
        return;
      }
      let ad_index = Math.floor(Math.random() * g.ylsdk_banner_ids.length);
      let _adId = g.ylsdk_banner_ids[ad_index];
      if (pf_data.bannerAd) {
        p_f.removeBannerAd();
      }
      let gConfig = _sdk.getGameConfig();
      let ww = isSmall ? 300 : sys.screenWidth;
      let left = (sys.screenWidth - ww) / 2;
      let _style_2 = {
        left: left,
        top: sys.screenHeight - sys.screenWidth * 0.28695,
        width: ww,
      };
      let bannerInfo = {
        adUnitId: _adId,
        style: _style_2
      };
      if (_style) {
        bannerInfo.style = _style;
      }
      if(p_f.hasFun(qg.createBannerAd,'qg.createBannerAd')){
        pf_data.bannerAd = qg.createBannerAd(bannerInfo);
        //是否定时刷新banner
        let onOff = (gConfig && gConfig.bannerTimeSpace && gConfig.bannerTimeSpace.onOff);
        if (onOff) pf_data.bannerAd.isSmall = isSmall;
        pf_data.bannerAd._style = _style;
        pf_data.bannerAd.onLoad(() => {
          if (pf_data.switchLog) console.log('YLSDK qg.createBannerAd---onLoad 广告加载成功');
          if (_callback) _callback(true);
          if (show) {
            p_f.showBannerAd();
          }
        });
        pf_data.bannerAd.onError(err => {
          if (pf_data.switchLog) console.warn("YLSDK qg.createBannerAd---onError:", err);
          pf_data.bannerAd = null;
          if (onOff) {
            let delay = gConfig.bannerTimeSpace.timeSpace || 10;//定时时间间隔
            if (pf_data.switchLog) console.log(`YLSDK 将在${delay}秒后重新创建banner(调用ylBannerAdHide会取消重试)`);
            p_f.delayUpdateBanner(delay, isSmall, show, _style);
            if (_callback) _callback(false);
          }
        });
        if(p_f.hasFun(pf_data.bannerAd.onResize,'pf_data.bannerAd.onResize')){
          pf_data.bannerAd.onResize(res => {
            if (pf_data.bannerAd) {
              if (_style) {
                  pf_data.bannerAd.style = _style;
                } else {
                  pf_data.bannerAd.style.top = sys.windowHeight - res.height;
                  pf_data.bannerAd.style.left = (sys.windowWidth - res.width) / 2;
                }
              if (pf_data.switchLog)
                console.log("YLSDK qg.createBannerAd---onResize:", pf_data.bannerAd.style.realWidth, pf_data.bannerAd.style.realHeight, pf_data.bannerAd.style.top);
            }
          });
        }
        pf_data.bannerAd.misToch = misToch || false;
      }
    },
    //延迟刷新banner
    delayUpdateBanner(delay, isSmall, show, _style) {
      if (pf_data.switchLog) console.log("YLSDK delayUpdateBanner-delay,isSmall,show:", delay, isSmall, show);
      if (delay > 0) { //大于0才刷新
        if (pf_data.bannerTimer) {
          clearTimeout(pf_data.bannerTimer);
          pf_data.bannerTimer = null;
        }
        pf_data.bannerTimer = setTimeout(() => {
          p_f.createBannerAd(isSmall, show,null, _style);
        }, (delay * 1000));
      }
    },
    //显示banner
    showBannerAd() {
      if (pf_data.switchLog) console.log("YLSDK BannerAd.show");
      let isSmall = true;
      let create = false;//是否走创建banner流程
      let onOff = false;//是否定时刷新banner
      let timeSpace = pf_data.defaultbTS;//定时时间间隔(秒)
      let gConfig = _sdk.getGameConfig();
      if (gConfig && gConfig.bannerTimeSpace) {
        onOff = gConfig.bannerTimeSpace.onOff || false;
        timeSpace = gConfig.bannerTimeSpace.timeSpace || pf_data.defaultbTS;
      }
      if (pf_data.bannerAd) {
        if (onOff) isSmall = pf_data.bannerAd.isSmall || false;
        let _style = pf_data.bannerAd._style || null;
        let nowTime = new Date().getTime();
        let last = pf_data.bannerAd.lastTime || nowTime;
        if (onOff) {
          if ((nowTime - last) >= timeSpace * 1000) {
            //到时间定时更新banner
            create = true;
            if (pf_data.switchLog) console.log("YLSDK showBannerAd-createBannerAd:", create);
            p_f.createBannerAd(isSmall, true, null, _style);
          }
        }
        if (!create) {
          //更新就不show了
          pf_data.bannerAd.show();
          pf_data.bannerAd.show_banner = true;
        }
        pf_data.bannerAd.lastTime = nowTime;
        Interface.ylEventCount('showBannerAd');
      } else {
        if (pf_data.switchLog) console.warn("YLSDK not BannerAd");
      }
      //
      if (!create && onOff) {
        p_f.delayUpdateBanner(timeSpace, isSmall, true);
      }
    },
    //隐藏banner
    hideBannerAd() {
      if (pf_data.switchLog) console.log("YLSDK BannerAd.hide");
      if (pf_data.bannerAd) {
        pf_data.bannerAd.hide();
        if (pf_data.bannerTimer) {
          clearTimeout(pf_data.bannerTimer);
          pf_data.bannerTimer = null;
        }
        pf_data.bannerAd.show_banner = false;
      }
    },
    //获取玩家微信平台账号信息
    getUserPlatFormInfo() {
      return _sdk.getUserPlatFormInfo();
    },
    /**
     * 创建video，单例创建一次就行了，显示的时候获取视频即可
     
        VIDEO_PLAY_FAIL:0    //视频广告-播放失败
        VIDEO_PLAY_FINISH:1  //视频广告-播放完成
        VIDEO_PLAY_CANCEL:2  //视频广告-播放取消
     */
    createVideoAd() {
        let ad_index = Math.floor(Math.random() * g.ylsdk_video_ids.length);
        let advert = g.ylsdk_video_ids[ad_index];
        if (!pf_data.videoAd) {
          if (p_f.hasFun(qg.createRewardedVideoAd,'qg.createRewardedVideoAd')) {
            pf_data.videoAd = qg.createRewardedVideoAd({ adUnitId: advert });
            pf_data.videoAd.ad_unit_id = advert;

            //点击关闭按钮
            pf_data.videoAd.onClose(res => {
              pf_data.show_video = false;
              if (res && res.isEnded || res === undefined) {
                if (pf_data.switchLog) console.log('YLSDK 视频播放完成');
                // 正常播放结束，调用游戏逻辑。
                if (pf_data.onVideoClose) pf_data.onVideoClose(pf_data.VIDEO_PLAY_FINISH);
                _sdk.addWatchTVnums(pf_data.unlockCustomNum);
                _sdk.addPower(1);
                if(pf_data.videoAd.ad_unit_id)
                    Interface.ylStatisticViedo(1, pf_data.videoAd.ad_unit_id);
              } else {
                // 播放中途退出。
                if (pf_data.onVideoClose) pf_data.onVideoClose(pf_data.VIDEO_PLAY_CANCEL);
                if (pf_data.switchLog) console.warn('YLSDK 视频播放取消');
              }
              pf_data.unlockCustomNum = null;
            });

            //视频加载失败
            pf_data.videoAd.onError(err => {
              if (pf_data.switchLog) console.warn('YLSDK RewardedVideoAd.onError:', err);
              if (pf_data.onVideoClose) pf_data.onVideoClose(pf_data.VIDEO_PLAY_FAIL);
              pf_data.unlockCustomNum = null;
              pf_data.show_video = false;
            });
          }
        }else{
          if (pf_data.videoAd.ad_unit_id === advert) return;
          pf_data.videoAd.ad_unit_id = advert;
        }
        console.warn('YLSDK ------ad_unit_id:', pf_data.videoAd.ad_unit_id);
    },
    /**
     * 显示video
     
        VIDEO_PLAY_FAIL:0    //视频广告-播放失败
        VIDEO_PLAY_FINISH:1  //视频广告-播放完成
        VIDEO_PLAY_CANCEL:2  //视频广告-播放取消
     */
    showVideoAd(cb, unlockCustomNum) {
      pf_data.show_video = true;
      if (!pf_data.videoAd) {
        p_f.createVideoAd();
      }
      if (!pf_data.videoAd) return;
      if(pf_data.videoAd.ad_unit_id)
          Interface.ylStatisticViedo(0, pf_data.videoAd.ad_unit_id);
      pf_data.unlockCustomNum = unlockCustomNum;
      pf_data.onVideoClose = cb;
      let bReturn = false;
      //视频加载成功
      pf_data.videoAd.onLoad(() => {
        if (pf_data.switchLog) console.log('YLSDK RewardedVideoAd.onLoad');
        if (!bReturn) {
          bReturn = true;
          if (pf_data._timer) {
            clearTimeout(pf_data._timer);
            pf_data._timer = null;
          }
          pf_data.videoAd.show().then(() => {
            if (pf_data.switchLog) console.log('YLSDK 视频播放成功');
          }).catch(err => {
            if (pf_data.switchLog) console.warn('YLSDK 视频播放失败');
            if (cb) cb(pf_data.VIDEO_PLAY_FAIL);
            pf_data.show_video = false;
            // if(pf_data.videoAd.destroy) pf_data.videoAd.destroy();
            // pf_data.videoAd = null;
          })
        }
      });
      if (pf_data.videoAd.load) pf_data.videoAd.load();
      if (!pf_data._timer) {
        pf_data._timer = setTimeout(() => {
          pf_data._timer = null;
          pf_data.show_video = false;
          if (!bReturn) {
            if (pf_data.switchLog) console.log('YLSDK 4秒内无视屏回调，自动回调加载失败');
            bReturn = true;
            if (cb) cb(pf_data.VIDEO_PLAY_FAIL);
          }
        }, 4000);
      }
      Interface.ylEventCount('showVideoAd');
    },
    //初始化平台转发按钮信息
    initShareMenu() {
      qg.showShareMenu({
        success: function (e) {
        },
        fail: function (e) {
        },
        complete: function (e) {
        },
      });
      qg.onShareAppMessage(() => {
        pf_data.show_share = true;
        let share_scene = _sdk.getShareScene();
        let shareCardConfig = _sdk.getShareConfig();
        let shareInfo = {};
        if (!pf_data.layer || !shareCardConfig) return shareInfo;
        let sharInfo = shareCardConfig.get(share_scene);
        if (sharInfo && sharInfo.length > 0) {
          let sareCard = sharInfo[0];
          let queryData = 'account_id=' + _sdk.getAccountId() + '&sharecard_id=' + sareCard.id + '&from=' + 'stage_invite';
          shareInfo.title = sareCard.title;
          shareInfo.imageUrl = sareCard.img;
          shareInfo.query = queryData;
        }
        Interface.ylEventCount('share_menu');
        // 用户点击了“转发”按钮
        return shareInfo;
      })
    },
    //设置日志开关状态
    setSwitchLog(_switch){
      pf_data.switchLog = _switch;
      console.log("YLSDK setSwitchLog:",_switch);
    },
  };
  //系统信息
  var sys = {};
  try {
      sys = qg.getSystemInfoSync();
      console.log("YLSDK getSystemInfoSync:", JSON.stringify(sys));
  } catch(e) {
      console.log("YLSDK sys--e:",e);
  }
  sys.pf_name = "MEIZU";
  sys.pf_num = 12;//平台[ 0:微信,1:QQ,2:OPPO,3:VIVO,4:字节跳动,5:百度,12:魅族,13:UC ]//暂未开放:6:4399,7:趣头条,8:360,9:陌陌
  sys.pf_func = p_f;

  console.warn("YLSDK 当前设置平台请确认： ",sys.pf_name);
  if ("" == g.ylsdk_app_id) console.error("YLSDK 请在配置文件中填写您的游戏APPID");
  if ("" == g.ylsdk_version) console.error("YLSDK 请在配置文件中填写您的游戏版本号");
  sys.pkg_name = g.ylsdk_pkg_name || '';
  sys.s_log = g.ylsdk_debug_model;

  if (g.ylsdk_debug_model) console.warn("YLSDK 是否一直通过"+sys.pf_name+" code(token)登录: ", g.login_by_code);
  console.warn("YLSDK 日志开关(本地)-", g.ylsdk_debug_model ? "打开" : "关闭");
  if ("" === g.ylsdk_pkg_name) console.error("YLSDK 请在配置文件中填写您的包名");
  g.ylsdk_app_id = g.ylsdk_app_id.replace(/\s/g, ""); //去除多余空格
  g.ylsdk_version = g.ylsdk_version.replace(/\s/g, "");
  sys.game_config = g;
  _sdk.init(sys);
  (function() {
  })(),qg.onShow(function(e) {
    console.log("YLSDK onShow--e:",e);
    if(pf_data.lifeListen) pf_data.lifeListen('onShow',e);
    // 平台onShow的时候注册定时器，并且取出本地缓存上传
    if (pf_data.switchLog) console.log("YLSDK qg.onShow");
    _sdk.getLocalStatistics();
    _sdk.registerInterval();
    //点击banner统计
    if (pf_data.bannerAd) console.log("YLSDK show_banner,misToch,show_share,show_video:", pf_data.bannerAd.show_banner, pf_data.bannerAd.misToch, pf_data.show_share, pf_data.show_video);
    if ((pf_data.bannerAd && pf_data.bannerAd.show_banner && pf_data.bannerAd.misToch) && !pf_data.show_share && !pf_data.show_video) {
      let nowTime = new Date().getTime();
      let bannerTSP = _sdk.getBannerTimeSpace();
      if (pf_data.hideTime && (nowTime - pf_data.hideTime) <= bannerTSP * 1000) {
        _sdk.statisticsBannerClick();
      }
    }
    //
    if (pf_data.onShare) {
      pf_data.show_share = false;
      var curTime = new Date().getTime();
      let timeSpace = curTime - pf_data.onShare.showTime;
      _sdk.getSharingResults(timeSpace, function (result) {
        if (pf_data.switchLog) console.warn("YLSDK 本次分享" + (result ? "成功" : "失败"));
        if (pf_data.shareCallBack) pf_data.shareCallBack(result);
        pf_data.shareCallBack = null;
      });
      pf_data.onShare = null;
    }

    _sdk.setLastTime();
  }),qg.onHide(function() {
    console.log("YLSDK onHide");
    if(pf_data.lifeListen) pf_data.lifeListen('onHide',null);
    if (pf_data.switchLog) console.log("YLSDK qg.onHide");
    pf_data.hideTime = new Date().getTime();
    _sdk.unRegisterInterval();
    _sdk.saveStatisticsToLocal();
    _sdk.commitStaticsLayer();
    _sdk.statisticsPlayTime();
    _sdk.savePowerLocal();
  }),qg.onError(function(e) {
    console.log("YLSDK onError--e:",e);
    if(pf_data.lifeListen) pf_data.lifeListen('onError',e);
    let err = JSON.stringify(e);
    if (!pf_data.ERROR_LOG || pf_data.ERROR_LOG !== err) {
      console.error("YLSDK onError:", err);
      pf_data.ERROR_LOG = err;
      // commitStaticsErrStack("", e)
    }
  });
}();

