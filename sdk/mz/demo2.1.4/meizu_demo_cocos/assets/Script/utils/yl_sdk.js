var pc = { //平台号
  WX: 0,
  QQ: 1,
  OPPO: 2,
  VIVO: 3,
  TT: 4,
  BD: 5,
  SSJJ: 6,
  QTT: 7,
  SLL: 8,
  MM: 9,
  MZ: 12,
  UC:13
};
var def_tj = {  //统计信息
  'events': [],
  'video': [],
  'result': [],
  'sharecard': [],
  'clickcount': [],
  'layer': []
};
var s_d = {
  _SDKversion: "2.2.0", //SDK版本号
  pf_num: 0,
  pf_name: '',
  platform: {},
  s_url: "https://api.ylxyx.cn/api/collect/v1",   //"http://test-api.ylxyx.cn/api/collect/v1",  //          //线上环境服务器地址
  _AccountId: "",     //SDK后台生成的用户账号
  login_count: 0,       //登录次数，在登录异常的情况下，重试三次登录如果都失败，则放弃并提示用户
  accountPass: null,    //接口调用密钥
  u_pt_info: null,   //用户平台信息
  bannerAd: null,       //banner广告
  videoAd: null,        //video广告
  recorder: null,       //录屏管理器
  recorder_status: 0,   //录屏状态[0:无,1:录屏中,2:暂停]
  switchInfo: {         //开关信息
    switchTouch: 0,         //误触开关开关，1打开，0关闭
    switchPush: 0,          //推送开关
    switchLogin: 1,     //登录开关这个开关关闭则不让登录了(禁止该游戏)
    switchJump: 1,      //卖量统计开关
    switchShare: 1,     //分享统计开关
    switchEvent: 1,     //事件统计开关
    switchLayer: 1,     //流失统计开关
    switchResult: 1,    //结果统计开关
    switchVideo: 1,     //视频统计开关
  },
  gConf: {       //游戏配置
    bannerTimeSpace: {
      onOff: false,
      timeSpace: 15,
      forceOnOff: true,
      forceTimeSpace: 3
    },
    Power: {
      defaultPower: 5,
      powerUpper: 5,
      recoveryTime: 300,
      getPower: 1
    },
    VideoUnlockCustoms: {
      onOff: false,
      openCustoms: 5,
      spaceCustoms: 2
    },
    depthShield: {
      _type: 0,
      totalCustoms: 3,
      goodUserOnOff: true,
      watchTvNum: 1,
      dayCustoms: [3]
    },
    mistouchTimer: {
      onOff: false,
      startTime_h: 9,
      startTime_m: 30,
      endTime_h: 23,
      endTime_m: 59
    },
    toalByTodayNum: 0,
    toalNum: 0,
    watchTvNum: 0,
    maxCustom: 0,
    lastCustom: 0,
    isServerData: false,//游戏配置数据来源[true:服务器,false:本地]
  },      //游戏配置信息     
  getGameConfig: false,
  pRtimer: null,  //回复体力定时器
  uPnum: 0,  //玩家体力数
  banner_time_space: 3,    //判定banner点击时长,单位秒(预计切入后台小于该时长认定为点击banner) 
  switchLog: false,        //日志开关
  need_tj_on_show: true,   //是否需要记录玩家切入的时间戳
  show_video: false,
  show_share: false,
  show_banner: false,
  //初始化SDK完成事件KEY
  boxconfig: null,         //侧边盒子数据
  boardconfig: null,         //积分墙列表数据
  custom: null,            //自定义游戏配置
  sStrategy: null,   //分享策略
  signConfig: null,        //签到列表
  invite_account: -1,      //邀请者account_id(从分享进入游戏时可获取)
  loginInfo: null,         //登录信息
  sharecard_config: null,  //分享卡片数据
  sharecard_scene: null,   //分享卡片场景
  is_debug: false,       //是否Debug模式 [true:打印日志,false:不打印日志]
  tj: def_tj,                     //打点-缓存队列
  s_commit_dt: 3000,      //打点-每次上传时间间隔(毫秒)
  s_cache_ml: 10000,       //打点-最大本地存储条数
  s_max_post_l: 50,        //打点-每次最大上传条数
  s_interval: null,        //定时器ID
  layer: null,             //当前场景
  defaultbTS: 10,  //默认banner刷新时间间隔
};
var sys = {};
var g = null;
var is_platform_phone = false;
var p_f = null;
function request(options) {
  //默认参数
  options.url = options.url || '',
    options.method = options.method || 'get',
    options.data = options.data || '',
    options.callback = options.callback || '';
  if (!is_platform_phone) options.async = options.async || true;

  var http = new XMLHttpRequest();
  utils.sdk_log("Request--url:" + options.url + ", menthod:" + options.method + ", data:" + JSON.stringify(options.data));
  //是否开启异步
  if (is_platform_phone) {
    http.open(options.method, options.url);
  } else {
    http.open(options.method, options.url, options.async);
  }
  if (options.method === 'post')
    http.setRequestHeader("Content-Type", "application/json");
  if (options.needPass) {
    if (!s_d.accountPass) console.error("YLSDK AccountPass is null !");
    http.setRequestHeader("AccountPass", s_d.accountPass);
  } else {
    let ClientInfo = {
      "brand": sys.brand || "",
      "model": sys.model || "",
      "platform": sys.platform || "",
      "version": s_d._SDKversion
    };
    if (sys.appName) ClientInfo.appName = sys.appName;
    // console.log("YLSDK ClientInfo:",JSON.stringify(ClientInfo));
    ClientInfo = utils.base64_encode(JSON.stringify(ClientInfo))
    http.setRequestHeader("ClientInfo", ClientInfo);
  }
  http.onerror = function (e) {
    console.error("YLSDK ", e);
  };
  http.onabort = function (e) {
    console.exception(e);
  };
  http.onprogress = function (e) {

  };
  http.onreadystatechange = function () {
    if (this.status === 200) {
    } else {
      console.warn("YLSDK http request status:" + this.status);
    }
  };
  http.onload = function () {
    var back_data = this.responseText;
    if (options.callback && typeof options.callback === "function") {
      options.callback(back_data);
    }
  };
  if (options.data && options.data.length > 0 && s_d.switchLog) {
    console.log("YLSDK reqType:" + options.method + " url:" + options.url + " data:" + options.data);
  }
  try {
    if (options.data == undefined) {
      options.data = null;
    }
    if (is_platform_phone) {
      http.send(JSON.stringify(options.data));
    } else {
      http.send(options.data);
    }
  } catch (e) {
    console.exception(options.data);
  }
}
var nets = {
  // 登录
  login(_callback) {
    s_d.login_count += 1;
    let accountPass = s_d.accountPass;
    if (s_d.switchInfo.switchLogin === 0) {
      utils.sdk_log("该游戏被禁止！", 'warn');
      return;
    }
    if (g.login_by_code || (!accountPass || '' === accountPass)) {
      utils.sdk_log("login by " + s_d.pf_name + " code/token");
      //如果没有account_id缓存，则需要先登录平台，
      //拿到平台的登录凭证(code)，再通过code去登录SDK服务器
      p_f.loginPF(s_d, _callback);
    } else {
      s_d.accountPass = accountPass;
      utils.sdk_log("login by accountPass");
      //如果已经有account_id缓存,说明已经登录过平台，
      //直接用account_id登录SDK服务器
      let data = {
        needPass: true
      };
      nets.loginSDKServer(data, _callback);
    }
  },
  // SDK服务器登录
  loginSDKServer(data, _callback) {
    nets.loginServer(data, function (res) {
      let results = JSON.parse(res);
      if (results.code === 0) {
        let _result = results.result;
        s_d.switchLog = (_result.switchLog === 1);
        p_f.setSwitchLog(s_d.switchLog);
        if (g.ylsdk_debug_model) console.warn("YLSDK switchLog(online)-", s_d.switchLog ? "oppen" : "close")
        //设置用户信息
        s_d.loginInfo = {
          accountId: _result.accountId,
          nickName: _result.nickName || '',
          avatarUrl: _result.avatarUrl || '',
          newPlayer: (_result.newPlayer !== 0 ? 1 : 0),
          openid: _result.openid || "",
          code: data.code || "",
          token: data.code || "",
          pkgName: data.pkgName || ""
        };
        utils.sdk_log("loginInfo:" + JSON.stringify(s_d.loginInfo));
        // //保存基础数据
        if (_result.accountId) utils.SetAccountID(_result.accountId);
        if (_result.accountPass) utils.SetAccountPass(_result.accountPass);
        if (_result.isLowVersion && _result.isLowVersion === 1) {
          console.warn("/*****************************************/");
          console.warn("**  YLSDK 您的SDK版本过低，请尽快升级SDK  **");
          console.warn("/*****************************************/");
        }

        //开关设置
        s_d.switchInfo = {
          switchTouch: _result.switchTouch || 0,
          switchPush: _result.switchPush || 0,
          switchLog: _result.switchLog || 0,
        }
        s_d.switchTouchAll = _result.switchTouch || 0;//原始误触总开关(不开放给CP，SDK自己判断用的)
        s_d.switchInfo.switchLogin = (_result.switchLogin === 0) ? _result.switchLogin : 1;
        s_d.switchInfo.switchJump = (_result.switchJump === 0) ? _result.switchJump : 1;
        s_d.switchInfo.switchShare = (_result.switchShare === 0) ? _result.switchShare : 1;
        s_d.switchInfo.switchEvent = (_result.switchEvent === 0) ? _result.switchEvent : 1;
        s_d.switchInfo.switchLayer = (_result.switchLayer === 0) ? _result.switchLayer : 1;
        s_d.switchInfo.switchResult = (_result.switchResult === 0) ? _result.switchResult : 1;
        s_d.switchInfo.switchVideo = (_result.switchVideo === 0) ? _result.switchVideo : 1;
        //记录进入游戏的时间戳
        if (s_d.need_tj_on_show) {
          s_d.lastTime = new Date().getTime();
          s_d.need_tj_on_show = false;
        }
        //视频分享策略
        if (_result.moduleList && _result.moduleList.length > 0) {
          s_d.moduleList = _result.moduleList;
          s_d.moduleList.forEach(function (element) {
            element.loopIndex = 0;
            if (element.logicJson) element.logicJson = JSON.parse(element.logicJson)
          });
          // utils.sdk_log("share_or_video2:"+s_d.moduleList);
        }
        utils.sdk_log("init SDK finish");
        //获取游戏配置
	      nets.getGameConfig(_callback);
        if (_callback) _callback(true);
      } else {
        if (s_d.login_count < 3) {
          utils.SetAccountPass('');
          nets.login(_callback);
        } else {
          utils.sdk_log("init SDK fail", 'warn');
          //获取游戏配置
	        nets.getGameConfig(_callback);
          if (_callback) _callback(false);
        }
      }
    });
  },
  /**
  * 登录SDK服务器
  * @param l_data
  * @param _callback 
  **/
  loginServer(l_data, _callback) {
    let extraData = {};
    if (!(s_d.pf_num === pc.OPPO || s_d.pf_num === pc.VIVO || s_d.pf_num === pc.MZ)) {
      let launchOptions = p_f.getLOS();
      utils.sdk_log("getLaunchOptionsSync：" + JSON.stringify(launchOptions));
      if (launchOptions.scene) {
        extraData.scene = launchOptions.scene;
      }
      if (launchOptions.query) {
        extraData.query = JSON.stringify(launchOptions.query);
        if (parseInt(launchOptions.scene) != 1005 &&
          parseInt(launchOptions.scene) != 1006 &&
          parseInt(launchOptions.scene) != 1037 &&
          parseInt(launchOptions.scene) != 1035 && extraData.query != "{}") {
          if (launchOptions.query.sharecard_id)
            extraData.sharecardId = launchOptions.query.sharecard_id;
          if (launchOptions.query.account_id) {

            extraData.accountId = launchOptions.query.account_id;
            s_d.invite_account = launchOptions.query.account_id;
          }
          if (launchOptions.query.from)
            extraData.from = launchOptions.query.from;
        }
      }
    }
    let _data = {
      platform: ""+s_d.pf_num,
      appid: g.ylsdk_app_id,
      version: g.ylsdk_version,
      shareInfo: extraData,
    };
    if (l_data.code) _data.code = l_data.code;
    _data.pkgName = l_data.pkgName;
    utils.sdk_log("loginServer-data:" + JSON.stringify(_data));
    request({
      method: "post",
      url: s_d.s_url + "/user/login",
      needPass: l_data.needPass,
      data: _data,
      callback(res) {
        var r_data = JSON.parse(res);
        utils.res_log("login:", r_data.code, res);
        if (_callback) {
          _callback(res);
        }
      }
    })
  },
  // 获取游戏配置列表
  getGameConfig(_callback) {
    request({
      method: "get",
      url: s_d.s_url + "/gamebase/config",
      needPass: true,
      callback(res) {
        let data = JSON.parse(res);
        utils.res_log("config:", data.code, res);
        if (data.code === 0) {
          utils.parseGameConfig(data.result, _callback);
        } else {
          nets.getGameConfigFinal(_callback);
        }
      }
    })
  },
  // 获取游戏配置列表
  getGameConfigFinal(_callback) {
    request({
      method: "get",
      url: s_d.s_url + "/gamebase/config/final?appid=" + g.ylsdk_app_id + "&version=" + g.ylsdk_version,
      needPass: false,
      callback(res) {
        let data = JSON.parse(res);
        utils.res_log("config-final:", data.code, res);
        if (data.code === 0) {
          utils.parseGameConfig(data.result, _callback);
        } else if (data.code === 9002) {
          p_f.loginPF(s_d);
          if (_callback) _callback('config_fail');
        }
      }
    })
  },
  // 视频统计(打点统计）
  viedoTJ(_data, _callback) {
    if (s_d.switchInfo.switchVideo === 0) {
      utils.sdk_log("停用视频统计接口！", 'warn');
      return;
    }
    request({
      method: "post",
      url: s_d.s_url + "/statistics/video",
      needPass: true,
      data: _data,
      callback(res) {
        let data = JSON.parse(res);
        utils.res_log("st-video:", data.code, res)
        if (data.code != 0) {
          s_d.tj.video.push(_data);
        }
        if (data.code === 9002) p_f.loginPF(s_d);
        if (_callback) {
          _callback(res);
        }
      }
    })
  },
  // 结果统计(打点统计）
  resultTJ(_data, _callback) {
    if (s_d.switchInfo.switchResult === 0) {
      utils.sdk_log("停用结果统计接口！", 'warn');
      return;
    }
    request({
      method: "post",
      url: s_d.s_url + "/statistics/result",
      needPass: true,
      data: _data,
      callback(res) {
        let data = JSON.parse(res);
        utils.res_log("st-result:", data.code, res)
        if (data.code != 0) {
          s_d.tj.result.push(_data);
        }
        if (data.code === 9002) p_f.loginPF();
        if (_callback) {
          _callback(data.code === 0);
        }
      }
    })
  },
  // 自定义空间
  storeValue(info, _callback) {
    request({
      method: "post",
      url: s_d.s_url + "/gamebase/store/value",
      needPass: true,
      data: info,
      callback(res) {
        let data = JSON.parse(res);
        let log_tip = info.name + (info.cmd ? "-" + info.cmd : "")
        utils.res_log("store/value--" + log_tip + ":", data.code, res);
        if (data.code === 9002) p_f.loginPF(s_d);
        if (data.code == 0 && data.result) {
          if (_callback) _callback(data.result)
        } else {
          if (_callback) _callback(false)
        }
      }
    })
  },
  //获取游戏自定义配置
  getCustom(_callback) {
    if (s_d.custom) {
      if (_callback) {
        _callback(s_d.custom);
      }
    } else {
      request({
        method: "get",
        url: s_d.s_url + "/gamebase/customconfig",
        needPass: true,
        callback(res) {
          var _data = JSON.parse(res);
          utils.res_log("customconfig:", _data.code, res)
          //
          if (_data.code == 0) {
            if (_data.result && _data.result.length > 0) {
              s_d.custom = _data.result;
              _data.result.forEach(element => {
                //如果有分享策略，缓存分享策略
                if (element.name == "sharing_strategy" &&
                  element.value &&
                  element.value != '') {

                  s_d.sStrategy = element;
                  s_d.sStrategy.value = JSON.parse(element.value);
                  s_d.sStrategy.strategy_id = 0;//策略id
                  s_d.sStrategy.time_id = 0;//time数组ID
                  s_d.sStrategy.count = 0;//执行遍数

                  utils.sdk_log("sharing_strategy:" + s_d.sStrategy);
                }
                //点击banner判定时长
                if (element.name == "banner_time_space" &&
                  element.value &&
                  element.value != '') {
                  s_d.banner_time_space = Number(element.value);

                  utils.sdk_log("banner_time_space:" + s_d.banner_time_space);
                }
              });
            }
            if (_callback) {
              _callback(_data.result);
            }
          } else {
            nets.customconfigFinal(_callback);
          }
        }
      })
    }
  },
  // 坚决获取自定义配置列表
  customconfigFinal(_callback) {
    request({
      method: "get",
      url: s_d.s_url + "/gamebase/customconfig/final?appid=" + g.ylsdk_app_id + "&version=" + g.ylsdk_version,
      needPass: false,
      callback(res) {
        var _data = JSON.parse(res);
        utils.res_log("customconfig-final:", _data.code, res)
        if (_data.code === 9002) p_f.loginPF();
        if (_data.code == 0 && _data.result && _data.result.length > 0) {
          s_d.custom = _data.result;
          _data.result.forEach(element => {
            //如果有分享策略，缓存分享策略
            if (element.name == "sharing_strategy" &&
              element.value &&
              element.value != '') {

              s_d.sStrategy = element;
              s_d.sStrategy.value = JSON.parse(element.value);
              s_d.sStrategy.strategy_id = 0;//策略id
              s_d.sStrategy.time_id = 0;//time数组ID
              s_d.sStrategy.count = 0;//执行遍数

              utils.sdk_log("sharing_strategy:" + s_d.sStrategy);
            }
          });
        }
        if (_callback) {
          _callback(_data.result);
        }
      }
    })
  },
  // 获取侧边栏列表
  SideBox(_callback) {
    if (s_d.boxconfig) {
      if (_callback) {
        _callback(s_d.boxconfig);
      }
    } else {
      request({
        method: "get",
        url: s_d.s_url + "/gamebase/sidebox",
        needPass: true,
        callback(res) {
          var _data = JSON.parse(res);
          utils.res_log("sidebox:", _data.code, res);
          if (_data.code === 0 && _data.result) {
            utils.checkJumpOutTime();
            s_d.boxconfig = _data.result;
            //
            let jumpInfo = utils.getJumpOutInfo();
            let jumpOutInfo = (!jumpInfo || jumpInfo === '') ? false : JSON.parse(jumpInfo);
            if (jumpOutInfo && jumpOutInfo.list && jumpOutInfo.list.length > 0) {
              //
              for (let j = 0; j < jumpOutInfo.list.length; j++) {
                let appid_1 = jumpOutInfo.list[j].appid;
                for (let i = 0; i < s_d.boxconfig.length; i++) {
                  let appid_2 = s_d.boxconfig[i].toAppid;
                  if (appid_2 == appid_1) {
                    utils.sdk_log("remove clicked item-_id:" + s_d.boxconfig[i]._id + ",appid:" + appid_2);
                    s_d.boxconfig.splice(i, 1);
                  }
                }
              }
            }
            if (_callback) {
              _callback(s_d.boxconfig);
            }
          } else {
            nets.newestsideboxFinal(_callback);
          }
        }
      })
    }
  },
  // 坚决获取侧边栏列表
  newestsideboxFinal(_callback) {
    request({
      method: "get",
      url: s_d.s_url + "/gamebase/sidebox/final?appid=" + g.ylsdk_app_id + "&version=" + g.ylsdk_version,
      needPass: false,
      callback(res) {
        var _data = JSON.parse(res);
        utils.res_log("sidebox-final:", _data.code, res);
        if (_data.code === 9002) p_f.loginPF();
        utils.checkJumpOutTime();
        if (_data.code === 0 && _data.result) {
          s_d.boxconfig = _data.result;
          //
          let jumpInfo = utils.getJumpOutInfo();
          let jumpOutInfo = (!jumpInfo || jumpInfo === '') ? false : JSON.parse(jumpInfo);
          if (jumpOutInfo && jumpOutInfo.list && jumpOutInfo.list.length > 0) {
            //
            for (let j = 0; j < jumpOutInfo.list.length; j++) {
              let appid_1 = jumpOutInfo.list[j].appid;
              for (let i = 0; i < s_d.boxconfig.length; i++) {
                let appid_2 = s_d.boxconfig[i].toAppid;
                if (appid_2 == appid_1) {
                  utils.sdk_log("remove clicked item-final-_id:" + s_d.boxconfig[i]._id + ",appid:" + appid_2);
                  s_d.boxconfig.splice(i, 1);
                }
              }
            }
          }
        }
        if (_callback) {
          _callback(s_d.boxconfig);
        }
      }
    })
  },
  // 获取积分墙列表
  IntegralWall(_callback) {
    request({
      method: "post",
      url: s_d.s_url + "/gamebase/scoreboard",
      needPass: true,
      data: {
        module: 'scoreboard' //表示某个模块的签到，目前就填写scoreboard
      },
      callback(res) {
        var data = JSON.parse(res);
        utils.res_log("scoreboard:", data.code, res);
        if (data.code == 0 && data.result) {
          if (data.result) {
            s_d.boardconfig = data.result;
          }
          if (_callback) {
            _callback(s_d.boardconfig);
          }
        } else {
          nets.newestscoreboardFinal(_callback);
        }
      }
    })
  },
  // 坚决获取积分墙列表
  newestscoreboardFinal(_callback) {
    request({
      method: "get",
      url: s_d.s_url + "/gamebase/scoreboard/final?appid=" + g.ylsdk_app_id + "&version=" + g.ylsdk_version + "&module=scoreboard",
      needPass: false,
      callback(res) {
        var data = JSON.parse(res);
        utils.res_log("scoreboard-final:", data.code, res);
        if (data.code === 9002) p_f.loginPF();
        if (data.code == 0 && data.result) {
          if (data.result) {
            s_d.boardconfig = data.result;
          }
        }
        if (_callback) {
          _callback(data);
        }
      }
    })
  },
  //领取积分墙奖励
  getBoardAward(_id, _callback) {
    if (!_id) {
      console.error("getBoardAward need _id");
      return;
    }
    request({
      method: "post",
      url: s_d.s_url + "/gamebase/getboardaward",
      needPass: true,
      data: {
        module: 'scoreboard', //表示某个模块的签到，目前就填写scoreboard
        awardId: _id
      },
      callback(res) {
        let data = JSON.parse(res);
        utils.res_log("getboardaward:", data.code, res)
        if (data.code != 0) {
          data = false;
          if (data.code === 9002) p_f.loginPF();
        }
        if (_callback) _callback(data.result.award);
      }
    })
  },
  // 获取签到列表
  getSignData(_callback) {
    if (s_d.signConfig) {
      if (_callback) {
        _callback(s_d.signConfig);
      }
    } else {
      request({
        method: "get",
        url: s_d.s_url + "/gamebase/getsigndata",
        needPass: true,
        data: {
          appid: g.ylsdk_app_id,
          account_id: s_d._AccountId,
          module: 'sign',
        },
        callback(res) {
          var _data = JSON.parse(res);
          utils.res_log("getsigndata:", _data.code, res)
          if (_data.code === 9002) p_f.loginPF();
          if (_data.code == 0 && _data.signconfig) {
            s_d.signConfig = _data;
          }
          if (_callback) {
            _callback(_data);
          }
        }
      })
    }
  },
  //领取签到奖励
  fetchSign(_id, _callback) {
    request({
      method: "get",
      url: s_d.s_url + "/fetchsign",
      needPass: true,
      data: {
        appid: g.ylsdk_app_id,
        account_id: s_d._AccountId,
        module: 'sign',
        id: _id,
      },
      callback(res) {
        var _data = JSON.parse(res);
        utils.res_log("fetchsign:", _data.code, res);
        if (_data.code === 9002) p_f.loginPF();
        if (_callback) {
          _callback(res);
        }
      }
    })
  },
  // 获取分享图列表
  shareCard(_callback, scene) {
    if (!s_d.sharecard_config) s_d.sharecard_config = new Map();
    utils.sdk_log("shareCard-scene:" + scene + ",layer:" + s_d.layer);
    s_d.share_scene = scene ? scene : s_d.layer;
    if (!s_d.share_scene) {
      utils.sdk_log("ylShareCard-need scene", 'warn');
      return;
    }
    if (s_d.sharecard_config.has(s_d.share_scene)) {
      if (_callback) {
        _callback(true);
      }
    } else {
      request({
        method: "post",
        url: s_d.s_url + "/gamebase/sharecard",
        needPass: true,
        data: {
          scene: s_d.share_scene
        },
        callback(res) {
          let data = JSON.parse(res);
          utils.res_log("sharecard:", data.code, res);
          //
          if (data.code == 0) {
            if (data.result && data.result.length > 0) {
              s_d.sharecard_config.set(s_d.share_scene, data.result)
              p_f.initShareMenu();
            }
            if (_callback) {
              if (s_d.sharecard_config.has(s_d.share_scene)) {
                _callback(s_d.sharecard_config.get(s_d.share_scene));
              } else {
                _callback(false);
              }
            }
          } else {
            nets.sharecardFinal(_callback, scene);
          }
        }
      })
    }
  },
  // 坚决获取分享图列表
  sharecardFinal(_callback, scene) {
    utils.sdk_log("sharecardFinal-scene:" + scene + ",layer:" + s_d.layer);
    s_d.share_scene = scene ? scene : s_d.layer;
    if (!s_d.share_scene) {
      utils.sdk_log("sharecardFinal-need scene", 'warn');
      return;
    }
    request({
      method: "get",
      url: s_d.s_url + "/gamebase/sharecard/final?appid=" + g.ylsdk_app_id + "&version=" + g.ylsdk_version + "&scene=" + s_d.share_scene,
      needPass: false,
      callback(res) {
        let data = JSON.parse(res);
        utils.res_log("sharecard-final:", data.code, res)
        if (data.code === 9002) p_f.loginPF();
        //
        if (data.code == 0 && data.result && data.result.length > 0) {
          s_d.sharecard_config.set(s_d.share_scene, data.result)
          p_f.initShareMenu();
        }
        if (_callback) {
          if (s_d.sharecard_config.has(s_d.share_scene)) {
            _callback(s_d.sharecard_config.get(s_d.share_scene));
          } else {
            _callback(false);
          }
        }
      }
    })
  },
  /**
   * 分享图统计(打点统计）
   *
   * 返回参数说明:
   * 参数                   类型                  说明
   * code                 Integer         code=0 表示成功 其他值表示出错
   * msg                  String          code=0 时为空 其他为错误信息
   */
  shareCardTJ(_data) {
    if (s_d.switchInfo.switchShare === 0) {
      utils.sdk_log("停用分享图统计接口！", 'warn');
      return;
    }
    request({
      method: "post",
      url: s_d.s_url + "/statistics/sharecard",
      needPass: true,
      data: _data,
      callback(res) {
        let data = JSON.parse(res);
        utils.res_log("st-sharecard:", data.code, res);
        if (data.code != 0) {
          s_d.tj.sharecard.push(_data);
        }
        if (data.code === 9002) p_f.loginPF();
      }
    })
  },
  /**
   * 卖量统计
   * @param iconId       Icon图片的ID
   * @param source       从哪个模块导出的，该字段具体值由调用方自行定义
   * @param targetAppId  导出游戏的appid
   * @param isClick      用户点击与否[true:点击,false:取消]
   * @param _callback
   *
   * 返回参数说明：
   * 参数      类型         说明
   * code    Integer    code=0 表示成功 其他值表示出错
   * msg     String     code=0 时为空 其他为错误信息
   */
  ClickOut(iconId, targetAppId, source, isClick, _callback) {
    if (!iconId) { console.error('YLSDK iconId is null !'); return; }
    if (!targetAppId) { console.error('YLSDK toAppId is null !'); return; }
    let _data = {
      iconId: iconId,
      source: source,
      target: targetAppId,
      action: (isClick === true ? "enable" : "cancel"),
    };
    nets.clickOutTJ(_data, _callback);
  },
  //卖量统计
  clickOutTJ(_data, _callback) {
    if (s_d.switchInfo.switchJump === 0) {
      utils.sdk_log("停用卖量统计接口！", 'warn');
      return;
    }
    request({
      method: "post",
      url: s_d.s_url + "/statistics/clickcount",
      needPass: true,
      data: _data,
      callback(res) {
        let data = JSON.parse(res);
        utils.res_log("st-clickcount:", data.code, res);
        if (data.code != 0) {
          s_d.tj.clickcount.push(_data);
        }
        if (data.code === 9002) p_f.loginPF();
        if (_callback) {
          _callback(res);
        }
      }
    })
  },
  /**
   * 流失统计
   *
   * 返回参数说明：
   * 参数        类型           说明
   * code       Integer      code=0 表示成功 其他值表示出错
   * msg        String       code=0 时为空 其他为错误信息
   **/
  commitTJlayer() {
    if (!s_d._AccountId) { console.error("YLSDK yl_sdk-commitTJlayer : _AccountId is null !"); return; };
    if (!s_d.layer) { console.warn("YLSDK yl_sdk-ccommitTJlayer : layer is null !"); };
    let postData = {
      layer: s_d.layer || 'default'
    };
    nets.layerTJ(postData);
  },
  // 流失统计
  layerTJ(_data) {
    if (s_d.switchInfo.switchLayer === 0) {
      utils.sdk_log("停用流失统计接口！", 'warn');
      return;
    }
    request({
      method: "post",
      url: s_d.s_url + "/statistics/layer",
      needPass: true,
      data: _data,
      callback(res) {
        var data = JSON.parse(res);
        utils.res_log("st-layer:", data.code, res);
        if (data.code != 0) {
          s_d.tj.layer.push(_data);
        }
        if (data.code === 9002) p_f.loginPF();
      }
    });
  },
  /**
   * 事件统计(打点统计）
   *
   * 返回参数说明：
   * 参数        类型           说明
   * code       Integer      code=0 表示成功 其他值表示出错
   * msg        String       code=0 时为空 其他为错误信息
   **/
  eventsTJ(cData, isClear) {
    let postData = {
      "events": cData
    };
    request({
      method: "post",
      url: s_d.s_url + "/statistics/event",
      needPass: true,
      data: postData,
      callback(res) {
        let _data = JSON.parse(res);
        utils.res_log("st-event:", _data.code, res);
        if (_data.code === 0) {
          if (isClear) {
            utils.clearTJEvents();
          }
        } else {
          for (var i = 0; i < cData.length; i++) {
            s_d.tj.events.unshift(cData.pop());
          }
        }
        if (_data.code === 9002) p_f.loginPF();
      }
    });
  },
  /**
   * 更新信息
   *      参数            必选     类型     说明
   *  @param user_info    True    JSON    用户信息，详细见下
   *
   *  user_info：   参数名称       参数类型       说明  
   *               nickName      String      用户昵称
   *               avatarUrl     String      用户头像url
   *               gender        String      性别
   *               language      String      语言
   *               city          String      市
   *               province      String      省
   *               country       String      国家
   *
   *
   *  参数返回说明         
   *  参数名称    参数类型    说明    
   *  code    Integer code=0 表示成功 其他值表示出错 
   *  msg     String  code=0 时为空 其他为错误信息  
  **/
  Edit(_user_info) {
    request({
      method: "post",
      url: s_d.s_url + "/user/info/edit",
      needPass: true,
      data: {
        nickName: _user_info.nickName,
        headimgurl: _user_info.avatarUrl,
        gender: _user_info.gender,
        language: _user_info.language,
        province: _user_info.province,
        city: _user_info.city,
        country: _user_info.country
      },
      callback(res) {
        let data = JSON.parse(res);
        utils.res_log("edit：", data.code, res);
        if (data.code === 9002) p_f.loginPF();
      }
    })
  },
  /**
   * 客户端错误日志
   *
   * 返回参数说明：
   * 参数        类型           说明
   * code       Integer      code=0 表示成功 其他值表示出错
   * msg        String       code=0 时为空 其他为错误信息
   **/
  commitStaticsErrStack(_errStack, _logStr) {
    let postData = {
      appid: g.ylsdk_app_id,
      version: g.ylsdk_version,
      language: sys.language,
      system: sys.system,
      model: sys.model,
      brand: sys.brand,
      platform: sys.platform,
      SDKVersion: s_d._SDKversion,
      resolution: sys.screenWidth + "x" + sys.screenHeight,
      window: sys.windowWidth + "x" + sys.windowHeight,
      ErrStack: _errStack,
      LogStr: _logStr,
    };
    request({
      method: "post",
      url: s_d.s_url + "/statistics/clientlog",
      needPass: false,
      data: postData,
      callback(res) {
        var data = JSON.parse(res);
        utils.res_log("st-clientlog:", data.code, res);
        if (data.code === 9002) p_f.loginPF();
      }
    });
  },
};
var utils = {
  /**
  * 获取accountPass缓存
  */
  getAccountPass() {
    var accountPass = p_f.getLD('SDK_ACCOUNT_PASS' + g.ylsdk_app_id)
    // utils.sdk_log("getAccountPass:"+accountPass);
    return accountPass;
  },
  /**
  * 设置用户ID
  * @param account_id 这个可以cp传入自己对这个用户的绑定ID
  **/
  SetAccountID(accountId) {
    s_d._AccountId = accountId;
    // console.warn("YLSDK SetAccountID:",accountId);
    utils.saveAccountId(accountId);
  },
  /**
  * 设置account_id缓存
  * @param account_id
  */
  saveAccountId(account_id) {
    utils.sdk_log("saveAccountId:" + account_id);
    p_f.saveLD('SDK_ACCOUNT_ID' + g.ylsdk_app_id, "" + account_id)
  },
  //设置accountPass
  SetAccountPass(accountPass) {
    s_d.accountPass = accountPass;
    utils.saveAccountPass(accountPass);
  },
  /**
  * 设置accountPass缓存
  * @param accountPass
  */
  saveAccountPass(accountPass) {
    p_f.saveLD('SDK_ACCOUNT_PASS' + g.ylsdk_app_id, "" + accountPass)
    utils.sdk_log("saveAccountPass:" + accountPass);
  },
  // 编码，配合encodeURIComponent使用
  base64_encode(str) {
    var c1, c2, c3;
    var base64EncodeChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    var i = 0, len = str.length, strin = '';
    while (i < len) {
      c1 = str.charCodeAt(i++) & 0xff;
      if (i == len) {
        strin += base64EncodeChars.charAt(c1 >> 2);
        strin += base64EncodeChars.charAt((c1 & 0x3) << 4);
        strin += "==";
        break;
      }
      c2 = str.charCodeAt(i++);
      if (i == len) {
        strin += base64EncodeChars.charAt(c1 >> 2);
        strin += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
        strin += base64EncodeChars.charAt((c2 & 0xF) << 2);
        strin += "=";
        break;
      }
      c3 = str.charCodeAt(i++);
      strin += base64EncodeChars.charAt(c1 >> 2);
      strin += base64EncodeChars.charAt(((c1 & 0x3) << 4) | ((c2 & 0xF0) >> 4));
      strin += base64EncodeChars.charAt(((c2 & 0xF) << 2) | ((c3 & 0xC0) >> 6));
      strin += base64EncodeChars.charAt(c3 & 0x3F)
    }
    return strin
  },
  //网络请求日志
  res_log(tips, code, res) {
    if (code != 0) {
      console.warn("YLSDK Response--", tips + " " + res);
    } else if (s_d.switchLog) {
      console.log("YLSDK Response--", tips + " " + res)
    }
  },
  //SDK 日志
  sdk_log(logMsg, logType) {
    utils.logs("YLSDK " + logMsg, logType || 'log');
  },
  //日志打印
  logs(logMsg, logType) {
    if (!s_d.switchLog) return;
    logType = logType ? logType : 'log';
    if (logType === 'log') {
      console.log(logMsg);
    } else if (logType === 'info') {
      console.info(logMsg);
    } else if (logType === 'error') {
      console.error(logMsg);
    } else if (logType === 'warn') {
      console.warn(logMsg);
    } else if (logType === 'debug') {
      console.debug(logMsg);
    }
  },
  //解析游戏配置
  parseGameConfig(data, _callback) {
    if (data.length > 0) {
      let gConf = s_d.gConf;
      data.forEach(element => {
        let _value = null;
        if (element.value) _value = JSON.parse(element.value);
        if (element.code === "b_t_s") {
          // banner定时刷新配置开关
          let bannerTimeSpace = {};
          //是否定时刷新
          bannerTimeSpace.onOff = (_value[0] === 1);
          //定时刷新时间
          bannerTimeSpace.timeSpace = _value[1] || 10;
          if (_value[1] || _value[1] === 0) {
            if (s_d.pf_num === pc.VIVO && _value[1] < 10) {
              bannerTimeSpace.timeSpace = 10;
            }
          }

          //切页面是否强制刷新开关
          bannerTimeSpace.forceOnOff = (_value[2] === 1);
          bannerTimeSpace.forceTimeSpace = _value[3] || 3;
          gConf.bannerTimeSpace = bannerTimeSpace;
        } else if (element.code === "power") {
          // 体力系统
          let _power = {};
          //初始体力（num）
          _power.defaultPower = _value[0] || 5;
          //体力上限
          _power.powerUpper = _value[1] || 5;
          //体力自动恢复时间
          _power.recoveryTime = _value[2] || 300;
          //看视频获得体力值
          _power.getPower = _value[3] || 1;
          //体力开关值 
          _power.onOff = (_value[4] === 1);
          gConf.Power = _power;
        } else if (element.code === "v_u_c") {
          // 视频解锁关卡
          let videoUnlockCustoms = {};
          //开关
          videoUnlockCustoms.onOff = (_value[0] === 1);
          //总关卡数
          videoUnlockCustoms.openCustoms = _value[1] || 2;
          //每日关卡数
          videoUnlockCustoms.spaceCustoms = _value[2] || 0;
          gConf.VideoUnlockCustoms = videoUnlockCustoms;
        } else if (element.code === "dp_s") {
          // 深度屏蔽规则
          let depthShield = {};
          //类型[0:关闭,1:按对局数,2:按关卡数]
          depthShield._type = _value[0] || 0;
          //总关卡数
          depthShield.totalCustoms = _value[1] || 5;
          //优质用户判断开关
          depthShield.goodUserOnOff = (_value[2] === 1);
          //观看视频数量 
          depthShield.watchTvNum = _value[3] || 2;
          //每日关卡数/对局数组 
          depthShield.dayCustoms = _value[4] || 0;
          gConf.depthShield = depthShield;
        } else if (element.code === "m_t") {
          // 定时开关误触
          let mistouchTimer = {};
          //开关
          mistouchTimer.onOff = (_value[0] === 1);
          //开始时间
          //时
          mistouchTimer.startTime_h = _value[1] || 0;
          //分
          mistouchTimer.startTime_m = _value[2] || 0;
          //结束时间
          //时
          mistouchTimer.endTime_h = _value[3] || 23;
          //分
          mistouchTimer.endTime_m = _value[4] || 0;
          gConf.mistouchTimer = mistouchTimer;
        }
      });
      s_d.gConf = gConf;
      s_d.gConf.isServerData = true;
    }
    utils.initTodayPlayNums();
    utils.initPower();
    utils.sdk_log("s_d.gameConfig:" + JSON.stringify(s_d.gConf));
    if (_callback) _callback('config_success');
  },
  //初始化玩家今天玩的局数
  initTodayPlayNums() {
    let todayPlay = p_f.getLD('SDK_TODAY_PLAY_NUMS_ID_' + g.ylsdk_app_id);
    let totalPlay = p_f.getLD('SDK_TOTAL_PLAY_NUMS_ID_' + g.ylsdk_app_id);
    let watchTvNum = p_f.getLD('SDK_TODAY_WATCH_TV_NUMS_ID_' + g.ylsdk_app_id);
    let lastWatch = p_f.getLD('SDK_LAST_WATCH_TIME_ID_' + g.ylsdk_app_id);
    let lastPlay = p_f.getLD('SDK_LAST_PLAY_TIME_ID_' + g.ylsdk_app_id);
    let maxCustom = p_f.getLD('SDK_MAX_CUSTOM_ID_' + g.ylsdk_app_id);
    let lastCustom = p_f.getLD('SDK_LAST_CUSTOM_ID_' + g.ylsdk_app_id);
    let nowTime = new Date();
    todayPlay = (todayPlay == '') ? 0 : parseInt(todayPlay);
    totalPlay = (totalPlay == '') ? 0 : parseInt(totalPlay);
    watchTvNum = (watchTvNum == '') ? 0 : parseInt(watchTvNum);
    maxCustom = (maxCustom == '') ? 0 : parseInt(maxCustom);
    lastCustom = (lastCustom == '') ? 0 : parseInt(lastCustom);

    utils.sdk_log(`initTodayPlayNums-nowTime:${nowTime.getTime()},lastWatch:${lastWatch},lastPlay:${lastPlay}`);

    if (lastWatch == '') {
      lastWatch = nowTime.getTime();
      p_f.saveLD('SDK_LAST_WATCH_TIME_ID_' + g.ylsdk_app_id, lastWatch);
    } else {
      lastWatch = parseInt(lastWatch);
    }

    if (lastPlay == '') {
      lastPlay = nowTime.getTime();
      p_f.saveLD('SDK_LAST_PLAY_TIME_ID_' + g.ylsdk_app_id, lastPlay);
    } else {
      lastPlay = parseInt(lastPlay);
    }
    //判断最后一次看视频和玩游戏的时间是否是昨天
    //是昨天就清理数据
    let nowYear = nowTime.getFullYear();
    let nowMonth = nowTime.getMonth() + 1;
    let nowDay = nowTime.getDate();

    let lwatch = new Date(lastWatch);
    let lwatchYear = lwatch.getFullYear();
    let lwatchMonth = lwatch.getMonth() + 1;
    let lwatchDay = lwatch.getDate();

    let lplay = new Date(lastPlay);
    let lplayYear = lplay.getFullYear();
    let lplayMonth = lplay.getMonth() + 1;
    let lplayDay = lplay.getDate();

    utils.sdk_log(`initTodayPlayNums-now:${nowYear}-${nowMonth}-${nowDay} ,lwatch:${lwatchYear}-${lwatchMonth}-${lwatchDay} ,lplay:${lplayYear}-${lplayMonth}-${lplayDay}`);

    if (nowYear != lwatchYear || nowMonth != lwatchMonth || nowDay != lwatchDay) {
      watchTvNum = 0;
      lastWatch = nowTime.getTime();
      p_f.saveLD('SDK_TODAY_WATCH_TV_NUMS_ID_' + g.ylsdk_app_id, watchTvNum);
      p_f.saveLD('SDK_LAST_WATCH_TIME_ID_' + g.ylsdk_app_id, lastWatch);
    }
    if (nowYear != lplayYear || nowMonth != lplayMonth || nowDay != lplayDay) {
      todayPlay = 0;
      lastPlay = nowTime.getTime();
      p_f.saveLD('SDK_TODAY_PLAY_NUMS_ID_' + g.ylsdk_app_id, todayPlay);
      p_f.saveLD('SDK_LAST_PLAY_TIME_ID_' + g.ylsdk_app_id, lastPlay);
    }

    s_d.gConf.toalByTodayNum = todayPlay;
    s_d.gConf.toalNum = totalPlay;
    s_d.gConf.watchTvNum = watchTvNum;
    s_d.gConf.maxCustom = maxCustom;
    s_d.gConf.lastCustom = lastCustom;
  },
  //初始化体力
  initPower() {
    if (!s_d.gConf.Power.onOff) {
      utils.sdk_log("initPower--onOff is off");
      return;
    }
    let defaultPower = 0;
    utils.sdk_log("initPower-Power:" + JSON.stringify(s_d.gConf.Power));
    if (s_d.gConf && s_d.gConf.Power && s_d.gConf.Power.defaultPower) {
      defaultPower = s_d.gConf.Power.defaultPower || 0;
    }
    let powerNum = p_f.getLD('SDK_POWER_NUM_ID_' + g.ylsdk_app_id);
    powerNum = (powerNum == '') ? defaultPower : parseInt(powerNum);
    utils.sdk_log("initPower-powerNum:" + powerNum);
    utils.setPower(powerNum);
  },
  //设置体力
  setPower(powerNum) {
    if (!s_d.gConf.Power.onOff) {
      utils.sdk_log("addPower--onOff is off");
      return;
    }
    s_d.uPnum = powerNum;
    if (s_d.gConf && s_d.gConf.Power && s_d.gConf.Power.powerUpper) {
      //体力上限
      let powerUpper = s_d.gConf.Power.powerUpper;
      let recoveryTime = s_d.gConf.Power.recoveryTime;
      s_d.uPnum = (s_d.uPnum > powerUpper) ? powerUpper : s_d.uPnum;
      s_d.uPnum = (s_d.uPnum < 0) ? 0 : s_d.uPnum;
      utils.sdk_log("setPower-powerUpper:" + powerUpper + ",recoveryTime:" + recoveryTime + ",userPowerNum:" + s_d.uPnum + ",powerRtimer:" + s_d.pRtimer);
      if (s_d.uPnum < powerUpper) {
        if (!s_d.pRtimer && recoveryTime) {
          utils.powerRecoveryTimer(recoveryTime);
        }
      } else {
        if (s_d.pRtimer) {
          clearTimeout(s_d.pRtimer);
          s_d.pRtimer = null;
        }
      }
    }
  },
  //体力恢复定时器
  powerRecoveryTimer(delay) {
    utils.sdk_log("powerRecoveryTimer-delay:" + delay);
    if (delay > 0) { //大于0才刷新
      if (s_d.pRtimer) {
        clearTimeout(s_d.pRtimer);
        s_d.pRtimer = null;
      }
      s_d.pRtimer = setTimeout(() => {
        // p_f.removeBannerAd();
        utils.powerRecoveryTimer(delay);
        utils.addPower(2);
      }, (delay * 1000));
    }
  },
  // 设置当前场景
  setCurScene(sceneName) {
    s_d.layer = sceneName;
    utils.sdk_log("setCurScene-sceneName:" + s_d.layer);
  },
  // 事件统计
  eventCount(eventName) {
    if (s_d.switchInfo.switchEvent === 0) {
      utils.sdk_log("停用事件统计接口！", 'warn');
      return;
    }
    let scene = s_d.layer ? s_d.layer : "default";
    s_d.tj.events.push({ "event": eventName, "scene": scene, "time": (new Date().getTime()) });
  },
  // 视频分享策略
  showShareOrVideo(channel, module, cb) {
    // console.warn("-----showShareOrVideo:",channel,module);
    // console.warn("-----shareOrVideo:",s_d.moduleList);
    let sTip = 'showShareOrVideo ';
    if (!channel) { console.error("YLSDK " + sTip + "--channel is null"); return; }
    if (!module) { console.error("YLSDK " + sTip + "--module is null"); return; }
    if (s_d.moduleList && s_d.moduleList.length > 0) {
      let hasModule = false;//默认无策略
      s_d.moduleList.forEach(function (element) {
        // console.warn("element.channel:",element.channel,element.module);
        if (element.channel == channel && element.module == module) {
          if (element && element.logicJson) {
            hasModule = true;
            let sOrVvalue = element.logicJson;
            // console.warn("element.channel:",element.channel,element.module);
            if (sOrVvalue.pe && sOrVvalue.pe.length > 0) {
              let type = element.logicJson.pe.shift();
              if (type == 1) {
                utils.sdk_log(sTip + "share " + JSON.stringify(sOrVvalue));
                if (cb) cb(type);
              } else {
                utils.sdk_log(sTip + "video " + JSON.stringify(sOrVvalue));
                if (cb) cb(2);
              }
            } else if (sOrVvalue.loop &&
              sOrVvalue.loop.length > 0 &&
              sOrVvalue.time &&
              sOrVvalue.time > 0) {

              let type = sOrVvalue.loop[element.loopIndex];
              element.loopIndex += 1;
              if (element.loopIndex >= sOrVvalue.loop.length) {
                element.loopIndex = 0;
                element.logicJson.time -= 1;
              }
              if (type == 1) {
                utils.sdk_log(sTip + "2-share " + JSON.stringify(sOrVvalue));
                if (cb) cb(type);
              } else {
                utils.sdk_log(sTip + "2-video " + JSON.stringify(sOrVvalue));
                if (cb) cb(2);
              }

            } else {
              utils.sdk_log(sTip + "no strategy", 'warn');
              if (cb) cb(0);
            }
          }
        }
      });
      if (!hasModule) {
        if (cb) cb(0);
        utils.sdk_log(sTip + "no strategy_1", 'warn');
      }
    } else {
      if (cb) cb(0);
      utils.sdk_log(sTip + "no strategy_2", 'warn');
    }
  },
  //获取深度误触屏蔽开关状态
  getDTinfo(customNum) {
    let deepTouch = utils.getDeepTouch(customNum);
    utils.sdk_log("getDeepTouchInfo-deepTouch:" + deepTouch + ",customNum:" + customNum);
    let deepTouchInfo = {
      deepTouch: (deepTouch ? "1" : "0"),
    };
    let misTouchInfo = [];
    if (s_d.custom && s_d.custom.length > 0) {
      s_d.custom.forEach(element => {
        //如果是开关的话就返回
        if (parseInt(element.type) == 2) {
          if (!deepTouch) {
            element.value = "0";
          }
          misTouchInfo.push(element);
        }
      });
    }
    deepTouchInfo.customInfo = misTouchInfo;
    return deepTouchInfo;
  },
  //获取深度误触屏蔽开关状态
  // true:误触开,false:误触关
  getDeepTouch(customNum) {
    if (!(s_d.switchTouchAll == 1)) return false;
    let dt = 'getDeepTouch-';
    if (utils.checkMistouchTimer()) {
      if (s_d.gConf && s_d.gConf.depthShield) {
        let gConf = s_d.gConf;
        let depthShield = gConf.depthShield;
        let totalCustoms = depthShield.totalCustoms || 0;
        utils.sdk_log(dt + "depthShield._type:" + depthShield._type);
        if (depthShield._type !== 0) {
          //[0:关闭,1:按对局数,2:按关卡数]
          if (depthShield._type == 1) {
            // utils.sdk_log(dt + "toalNum:" + gConf.toalNum + ",toalByTodayNum:" + gConf.toalByTodayNum);
            // utils.sdk_log(dt + "totalCustoms:" + depthShield.totalCustoms + ",dayCustoms:" + depthShield.dayCustoms[0]);
            //按对局数
            utils.sdk_log(`toalNum > totalCustoms: ${gConf.toalNum} > ${depthShield.totalCustoms}`);
            if (!depthShield.totalCustoms || gConf.toalNum > depthShield.totalCustoms) {
              utils.sdk_log(`toalByTodayNum > dayCustoms: ${gConf.toalByTodayNum} > ${depthShield.dayCustoms[0]}`);
              if (!depthShield.dayCustoms || gConf.toalByTodayNum > depthShield.dayCustoms[0]) {
                return utils.checkGoodUser(depthShield);
              } else {
                return false;
              }
            } else {
              return false;
            }
          } else {
            // utils.sdk_log("customNum:" + customNum);
            // utils.sdk_log(dt + "totalCustoms:" + depthShield.totalCustoms + ",dayCustoms:" + depthShield.dayCustoms[0]);
            //按关卡数
            //玩家传入的关卡数 customNum == totalCustoms 或者  totalCustoms 开始每间隔 depthShield.dayCustoms[0]关
            if (customNum || customNum === 0) {
              utils.sdk_log(`customNum >= totalCustoms: ${customNum} >= ${totalCustoms}`);
              if (customNum >= totalCustoms) {
                if (!depthShield.dayCustoms || depthShield.dayCustoms.length == 0) {
                  utils.sdk_log("depthShield.dayCustoms is null or empty");
                  return false;
                }
                utils.sdk_log(`${customNum} == ${totalCustoms} || ((${customNum} > ${totalCustoms}) && ((${customNum} - ${totalCustoms}) % (${depthShield.dayCustoms[0]} + 1)) == 0)`);
                if ((customNum == totalCustoms) || ((customNum > totalCustoms) && ((customNum - totalCustoms) % (depthShield.dayCustoms[0] + 1)) == 0)) {
                  return utils.checkGoodUser(depthShield);
                } else {
                  return false;
                }
              } else {
                return false;
              }
            } else {
              utils.sdk_log("customNum is " + customNum);
              return false;
            }
          }
        } else {
          return utils.checkGoodUser(depthShield);
        }
      }
    } else {
      return false;
    }
  },
  //检测是否在误触的时间段内
  checkMistouchTimer() {
    if (s_d.gConf) {
      if (s_d.gConf.mistouchTimer) {
        if (s_d.gConf.mistouchTimer.onOff) {
          let mistouchTimer = s_d.gConf.mistouchTimer;
          let startTime_h = mistouchTimer.startTime_h || 0;
          let startTime_m = mistouchTimer.startTime_m || 0;
          let endTime_h = mistouchTimer.endTime_h || 23;
          let endTime_m = mistouchTimer.endTime_m || 59;

          //当前时间
          let nowDate = new Date();
          let misNThours = nowDate.getHours();
          let misNTminutes = nowDate.getMinutes();
          utils.sdk_log(`checkMistouchTimer- ${startTime_h}:${startTime_m} > ${misNThours}:${misNTminutes} < ${endTime_h}:${endTime_m}`);
          let inTime = !((misNThours < startTime_h || (misNThours == startTime_h && misNTminutes <= startTime_m)) || (misNThours > endTime_h || (misNThours == endTime_h && misNTminutes >= endTime_m)));
          if (inTime) {
            return false;
          } else {
            //不在时间范围里面
            return true;
          }
        } else {
          //开关是关的
          return true;
        }
      }
    }
    return false;
  },
  //检测优质用户
  checkGoodUser(depthShield) {
    utils.sdk_log("checkGoodUser:" + depthShield.goodUserOnOff);
    if (depthShield.goodUserOnOff) {
      return !utils.isGoodUser(depthShield);
    } else {
      return true;
    }
  },
  //是否为优质用户
  isGoodUser(depthShield) {
    let isGoods = false;
    if (depthShield.watchTvNum || depthShield.watchTvNum === 0) {
      utils.sdk_log(`isGoodUser--${s_d.gConf.watchTvNum} > ${depthShield.watchTvNum}`);
      isGoods = (s_d.gConf.watchTvNum > depthShield.watchTvNum);
    } else {
      isGoods = true;
    }
    utils.sdk_log("isGoodUser:" + isGoods);
    return isGoods;
  },
  //更新玩游戏的局数
  addPlayNums(customNum) {
    if (customNum) {
      try {
        customNum = parseInt(customNum);
        s_d.gConf.lastCustom = customNum;
        let maxCustom = p_f.getLD('SDK_MAX_CUSTOM_ID_' + g.ylsdk_app_id);
        maxCustom = (maxCustom == '') ? 0 : parseInt(maxCustom);
        if (customNum > maxCustom) {
          maxCustom = customNum;
          p_f.saveLD('SDK_MAX_CUSTOM_ID_' + g.ylsdk_app_id, maxCustom);
          s_d.gConf.maxCustom = maxCustom;
        }
        p_f.saveLD('SDK_LAST_CUSTOM_ID_' + g.ylsdk_app_id, customNum);
        utils.sdk_log("addPlayNums-customNum:" + customNum + ",maxCustom:" + maxCustom);
      } catch (e) {
        console.error("YLSDK addPlayNums error e:", e);
      }

      //如果有关卡数就要保存最大关卡数
      //对局：总对局|当日对局数
      //关卡:总关卡|当日关卡
    }
    //更新今天玩的局数
    s_d.gConf.toalByTodayNum += 1;
    //更新总局数
    s_d.gConf.toalNum += 1;
    utils.sdk_log("addPlayNums-toalNum:" + s_d.gConf.toalNum + ",toalByTodayNum:" + s_d.gConf.toalByTodayNum);
    p_f.saveLD('SDK_TODAY_PLAY_NUMS_ID_' + g.ylsdk_app_id, s_d.gConf.toalByTodayNum);
    p_f.saveLD('SDK_TOTAL_PLAY_NUMS_ID_' + g.ylsdk_app_id, s_d.gConf.toalNum);
    p_f.saveLD('SDK_LAST_PLAY_TIME_ID_' + g.ylsdk_app_id, new Date().getTime());
  },
  //视频解锁
  getVideoUnlock(customNum) {
    let unlock = false;
    if (s_d.gConf && s_d.gConf.VideoUnlockCustoms) {
      let VULcustoms = s_d.gConf.VideoUnlockCustoms;
      let openCustoms = VULcustoms.openCustoms || 0;
      let spaceCustoms = VULcustoms.spaceCustoms || 0;
      if (VULcustoms.onOff) {
        if (customNum == openCustoms) {
          unlock = true;
        } else if ((customNum > openCustoms) && ((customNum - openCustoms) % (spaceCustoms + 1)) == 0) {
          unlock = true;
        } else {
          //判断是否有看视频解锁过该关卡
          let unlockNum = utils.getVideoUnlockCustoms();
          if (unlockNum && unlockNum.length > 0) {
            unlockNum.forEach(element => {
              if (customNum == element) {
                unlock = true;
              }
            });
          }
        }
      }
      utils.sdk_log("getVideoUnlock-customNum:" + customNum + ",openCustoms:" + openCustoms + ",spaceCustoms:" + spaceCustoms + ",unlock:" + unlock);
    }
    return unlock;
  },
  //获取看视频解锁的关卡
  getVideoUnlockCustoms() {
    let sUnlockNum = p_f.getLD('SDK_WATCH_TV_UNLOCK_CUSTOMS_ID_' + g.ylsdk_app_id);
    if (!sUnlockNum || sUnlockNum == '0' || sUnlockNum == '') {
      sUnlockNum = new Array();
    } else {
      sUnlockNum = JSON.parse(sUnlockNum);
    }
    utils.sdk_log("getVideoUnlockCustoms-sUnlockNum:" + sUnlockNum);
    return sUnlockNum;
  },
  // 获取体力相关信息
  getPowerInfo(_callback) {
    if (s_d.gConf.isServerData) {
      if (_callback) _callback(s_d.gConf.Power);
    } else {
      //获取游戏配置
      nets.getGameConfig(function (res) {
        utils.sdk_log("ylGetPowerInfo--res:" + res);
        if (_callback) _callback(s_d.gConf.Power);
      });
    }
  },
  //检测本地跳转列表是否是今天的缓存，不是则清除缓存(是保留当天的)
  checkJumpOutTime() {
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();
    let jumpInfo = utils.getJumpOutInfo()
    let jumpOutInfo = jumpInfo === '' ? false : JSON.parse(jumpInfo);
    if (jumpOutInfo && jumpOutInfo.date) {
      let lastDate = new Date(jumpOutInfo.date);
      let day_2 = lastDate.getDate();
      let month_2 = lastDate.getMonth();
      let year_2 = lastDate.getFullYear();
      if (year != year_2 || month != month_2 || day_2 != day) {
        utils.saveJumpOutInfo('');
      }
    }
  },
  // 获取卖量跳转记录
  getJumpOutInfo() {
    var info = p_f.getLD('SDK_JUMP_ICON' + g.ylsdk_app_id)
    utils.sdk_log("获取卖量跳转记录:" + info);
    return info;
  },
  //增加体力
  //type[1:视频,2:定时自动恢复]
  addPower(type) {
    if (!s_d.gConf.Power.onOff) {
      utils.sdk_log("addPower--onOff is off");
      return;
    }
    if (s_d.gConf && s_d.gConf.Power) {
      //要在后台配置了体力系统存在的情况下才有增加体力逻辑
      let power = yl_sdk.getPower();
      if (type == 1) {
        let getPower = s_d.gConf.Power.getPower;
        if (getPower) {
          power += getPower;
        }
      } else if (type == 2) {
        power += 1; //次自动恢复1点
      }
      if (s_d.powerChangeCB) s_d.powerChangeCB(type);
      utils.setPower(power);
    }
  },
  // 获取account_id缓存
  getAccountId() {
    var accountId = p_f.getLD('SDK_ACCOUNT_ID' + g.ylsdk_app_id)
    utils.sdk_log("getAccountId:" + accountId);
    return accountId;
  },
  //注册定时器
  registerInterval() {
    utils.unRegisterInterval();
    utils.sdk_log("registerInterval");
    s_d.s_interval = setInterval(function () {
      utils.sendStatistics();
    }, s_d.s_commit_dt, null);
  },
  //注销定时器
  unRegisterInterval() {
    utils.sdk_log("unRegisterInterval");
    if (s_d.s_interval) clearInterval(s_d.s_interval);
    s_d.s_interval = null;
  },
  //提交打点数据
  sendStatistics() {
    if (!s_d.tj) return;
    if (!s_d.accountPass) return;
    if (s_d.tj.events) {
      let commitData = s_d.tj.events;
      if (commitData.length !== 0) {
        if (commitData.length >= s_d.s_max_post_l) {
          let tempCommitData = [];
          for (var i = 0; i < s_d.s_max_post_l; i++) {
            tempCommitData.push(commitData.shift())
          }
          // 上传;
          nets.eventsTJ(tempCommitData);
        } else {
          // 上传;
          nets.eventsTJ(commitData, true);
        }
      }
    }
    if (s_d.tj.video && s_d.tj.video.length > 0) {
      nets.viedoTJ(s_d.tj.video.pop());
    }
    if (s_d.tj.sharecard && s_d.tj.sharecard.length > 0) {
      nets.shareCardTJ(s_d.tj.sharecard.pop());
    }
    if (s_d.tj.clickcount && s_d.tj.clickcount.length > 0) {
      nets.clickOutTJ(s_d.tj.clickcount.pop());
    }
    if (s_d.tj.result && s_d.tj.result.length > 0) {
      nets.resultTJ(s_d.tj.result.pop());
    }
    if (s_d.tj.layer && s_d.tj.layer.length > 0) {
      nets.layerTJ(s_d.tj.layer.pop());
    }
  },
  //将打点数据缓存到本地
  saveTJtoLocal() {
    if (s_d.tj.events && s_d.tj.events.length > s_d.s_cache_ml) {
      for (var i = s_d.s_cache_ml; i < s_d.tj.events.length; i++) {
        s_d.tj.events.shift();
      }
    }
    p_f.setLD('ylsdk_statistics' + g.ylsdk_app_id, JSON.stringify(s_d.tj));
  },
  //获取本地打点数据缓存
  getLocalTJ() {
    try {
      var statistics = p_f.getLD('ylsdk_statistics' + g.ylsdk_app_id);
      return ((!statistics || statistics.length === 0) ? def_tj : JSON.parse(statistics));
    } catch (e) { }
  },
  // 清空本地本地打点数据缓存
  clearAllTJ() {
    p_f.setLD('ylsdk_statistics' + g.ylsdk_app_id, '');
    s_d.tj = def_tj;
  },
  // 清空本地本地打点数据缓存
  clearTJEvents() {
    try {
      s_d.tj.events = [];
      p_f.setLD('ylsdk_statistics' + g.ylsdk_app_id, JSON.stringify(s_d.tj));
    } catch (e) { }
  },
  removeItemFrom(appid) {
    let jumpInfo = utils.getJumpOutInfo()
    let jumpOutInfo = (!jumpInfo || jumpInfo === '') ? false : JSON.parse(jumpInfo);
    //点击并确认跳转某个icon后，当日（可调整）隐藏对应appid的所有icon（开启内部标识的icon不受此功能影响）
    if (s_d.boxconfig && s_d.boxconfig.length > 0 && g.side_min_num < s_d.boxconfig.length) {
      for (let i = 0; i < s_d.boxconfig.length; i++) {
        let item = s_d.boxconfig[i];
        if (item.toAppid === appid && item.innerStatus == 0) {
          utils.sdk_log("remove clicked item icon--id:" + item._id);
          s_d.boxconfig.splice(i, 1);
          //
          let hasItem = false;
          let date = new Date();
          if (jumpOutInfo && jumpOutInfo.list && jumpOutInfo.list.length > 0) {
            for (let j = 0; j < jumpOutInfo.list.length; j++) {
              if (jumpOutInfo.list[j].appid == appid) {
                hasItem = true;
              }
            }
          } else {
            jumpOutInfo = { list: [] };
          }
          if (!hasItem) jumpOutInfo.list.push({ appid: appid });
          jumpOutInfo.date = date.getTime();
        }
      }
      utils.saveJumpOutInfo(JSON.stringify(jumpOutInfo));
    }
  },
  //保存体力到本地缓存
  savePowerLocal() {
    p_f.saveLD('SDK_POWER_NUM_ID_' + g.ylsdk_app_id, s_d.uPnum);
    utils.sdk_log("savePowerLocal-powerNum:" + s_d.uPnum);
  },
  //切出游戏打点
  PlayTimeTJ() {
    let curTime = new Date().getTime();
    if (!s_d.lastTime) s_d.lastTime = curTime;
    let timeSpace = curTime - s_d.lastTime;

    let tempCommitData = {
      "event": "play_time",
      "scene": (s_d.layer ? s_d.layer : "default"),
      "tp": timeSpace,
      "time": curTime
    }
    nets.eventsTJ([tempCommitData]);
  },
  /**
   * 获取假分享结果
   * 根据前端传过来的分享时长，通过判断分享策略对应的概率，给个假的分享结果
   * @param time_space 分享时长
   * @param _callback
   * 
   * 返回参数说明：
   * |参数               |类型         |说明
      ----------        | ----------- | -----------
      shareSuccess      | boolean     | 是否分享成功
   * 
   */
  getSharingResults(time_space, _callback) {
    let shareSuccess = false;
    if (s_d.sStrategy) {
      let strategyId = s_d.sStrategy.strategy_id;
      let strategy = s_d.sStrategy.value[strategyId];
      let timeId = s_d.sStrategy.time_id;
      utils.sdk_log("strategy:[" + strategyId + " - " + timeId + "],time_space:" + time_space, JSON.stringify(strategy));
      if (strategy.time && timeId < strategy.time.length) {
        let time_item = strategy.time[timeId];
        let item_prob = strategy.prob[timeId];
        if (time_item && time_item.length > 1) {
          for (let i = 1; i < time_item.length; i++) {
            if (time_space >= time_item[i - 1] && time_space <= time_item[i]) {
              let prob = item_prob[i - 1];
              shareSuccess = utils.randomProbability(prob);
            } else if (i == time_item.length - 1 && time_space > time_item[i]) {
              let prob = item_prob[i];
              shareSuccess = utils.randomProbability(prob);
            }
          }
        }
      }
      // s_d.sStrategy.time_id += 1;
      timeId += 1;
      //
      if (shareSuccess) {
        s_d.sStrategy.time_id = 0;
        s_d.sStrategy.count += 1;
      } else {
        if (timeId >= strategy.time.length) {
          s_d.sStrategy.time_id = 0;
          s_d.sStrategy.count += 1;
        } else {
          s_d.sStrategy.time_id = timeId;
        }
      }
      if (s_d.sStrategy.count >= strategy.num) {
        //寻找下一组
        s_d.sStrategy.count = 0;
        s_d.sStrategy.strategy_id = utils.getNextStrategy(strategyId);
      }
    }

    if (_callback) {
      _callback(shareSuccess);
    }
  },
  //banner点击统计
  bannerClickTJ() {
    let curTime = new Date().getTime();
    let tempCommitData = {
      "event": "banner_click",
      "scene": (s_d.layer ? s_d.layer : "default"),
      "time": curTime
    }
    nets.eventsTJ([tempCommitData]);
  },
  //随机概率
  randomProbability(pro) {
    utils.sdk_log("randomProbability---pro:" + pro);
    if (pro == 100) return true;
    if (pro == 0) return false;
    let probability = Math.floor(Math.random() * 100);
    utils.sdk_log("randomProbability---random pro:" + probability);
    return probability <= pro;
  },
  //获取下一条记录
  getNextStrategy(strategyId) {
    let nextStrategyId = strategyId + 1;
    utils.sdk_log("nextStrategyId:" + nextStrategyId);
    if (nextStrategyId >= s_d.sStrategy.value.length) {
      nextStrategyId = 0;
    }
    let strategy = s_d.sStrategy.value[nextStrategyId];
    if (strategy.num == 0) {
      utils.sdk_log("num:" + strategy.num);
      nextStrategyId = utils.getNextStrategy(nextStrategyId);
    }
    return nextStrategyId;
  },
  // 获取accountPass缓存
  getAccountPass() {
    var accountPass = p_f.getLD('SDK_ACCOUNT_PASS' + g.ylsdk_app_id)
    utils.sdk_log("获取缓存AccountPass:" + accountPass);
    return accountPass;
  },
  /**
   * 设置卖量跳转记录
   * @param info
   */
  saveJumpOutInfo(info) {
    p_f.saveLD('SDK_JUMP_ICON' + g.ylsdk_app_id, info)
    utils.sdk_log("saveJumpOutInfo:" + info);
  },

  /**
   * 设置用户平台信息本地缓存
   * @param info
   */
  savePTinfo(info) {
    p_f.saveLD('SDK_USER_PLATFORM_INFO' + g.ylsdk_app_id, info)
    utils.sdk_log("savePTinfo:" + info);
  },
  // 获取用户平台信息本地缓存
  getPTinfo() {
    var info = p_f.getLD('SDK_USER_PLATFORM_INFO' + g.ylsdk_app_id)
    utils.sdk_log("getPTinfo:" + info);
    return info;
  },
  addWatchTVnums(unlockCustomNum) {
    s_d.gConf.watchTvNum += 1;
    p_f.saveLD('SDK_TODAY_WATCH_TV_NUMS_ID_' + g.ylsdk_app_id, s_d.gConf.watchTvNum);
    p_f.saveLD('SDK_LAST_WATCH_TIME_ID_' + g.ylsdk_app_id, new Date().getTime());

    if (unlockCustomNum) {
      utils.addVideoUnlockCustoms(unlockCustomNum);
    }
  },
  // 增加看视频解锁的关卡
  addVideoUnlockCustoms(unlockCustomNum) {
    let unlockNum = utils.getVideoUnlockCustoms();
    let hasCustom = false;//是否已经解锁该关卡
    if (unlockNum && unlockNum.length > 0) {
      unlockNum.forEach(element => {
        if (element == unlockCustomNum) hasCustom = true;
      });
    }
    if (!hasCustom) unlockNum.push(unlockCustomNum);
    let sUnlockNum = JSON.stringify(unlockNum);
    utils.sdk_log("addVideoUnlockCustoms-unlockCustomNum:" + unlockCustomNum + ",sUnlockNum:" + sUnlockNum);
    p_f.saveLD('SDK_WATCH_TV_UNLOCK_CUSTOMS_ID_' + g.ylsdk_app_id, sUnlockNum);
  },
};
var yl_sdk = {
  init(_sys, ) {
    sys = _sys;
    s_d.pf_name = sys.pf_name;//平台名称
    s_d.pf_num = sys.pf_num;
    s_d.switchLog = sys.s_log;
    g = _sys.game_config;
    p_f = _sys.pf_func;
    is_platform_phone = (s_d.pf_num === pc.OPPO || s_d.pf_num === pc.VIVO || s_d.pf_num === pc.MZ || s_d.pf_num === pc.UC);
    s_d.accountPass = utils.getAccountPass();
  },
  login(_callback) {
    nets.login(_callback)
  },
  loginSDKServer(data, _callback) {
    nets.loginSDKServer(data, _callback);
  },
  setCurScene(sceneName) {
    utils.setCurScene(sceneName);
  },
  //获取邀请账号
  getInviteAccount() {
    return s_d.invite_account;
  },
  //获取用户SDK服务器账号信息
  getUserInfo() {
    return s_d.loginInfo;
  },
  //获取开关信息
  getSwitchInfo() {
    return s_d.switchInfo;
  },
  //日志输出
  logs(logMsg, logType) {
    utils.logs(logMsg, logType);
  },
  //视频统计(打点统计）
  tatisticViedo(_type, _adId, _callback) {
    let _data = {
      scene: s_d.layer || 'default',
      adId: _adId,
      type: _type
    };
    nets.viedoTJ(_data, _callback);
  },
  //结果统计
  statisticResult(_detail, _callback) {
    let _data = {
      detail: _detail
    };
    nets.resultTJ(_data, _callback);
  },
  //自定义空间
  storeValue(info, _callback) {
    nets.storeValue(info, _callback);
  },
  // 事件统计
  eventCount(eventName) {
    utils.eventCount(eventName);
  },
  // 获取游戏自定义配置
  getCustom(_callback) {
    nets.getCustom(_callback);
  },
  //获取侧边栏列表
  getSideBox(_callback) {
    nets.SideBox(_callback);
  },
  //获取积分墙列表
  getIntegralWall(_callback) {
    nets.IntegralWall(_callback);
  },
  //领取积分墙奖励
  getBoardAward(_id, _callback) {
    nets.getBoardAward(_id, _callback);
  },
  // 获取签到列表
  getSignData(_callback) {
    nets.getSignData(_callback);
  },
  //领取签到奖励
  fetchSign(_id, _callback) {
    nets.fetchSign(_id, _callback);
  },
  //获取分享图列表
  shareCard(_callback, scene) {
    nets.shareCard(_callback, scene);
  },
  // 视频分享策略
  showShareOrVideo(channel, module, cb) {
    utils.showShareOrVideo(channel, module, cb);
  },
  //获取深度误触屏蔽开关状态
  getDeepTouch(customNum) {
    return utils.getDTinfo(customNum);
  },
  //一局游戏结束
  overGame(customNum) {
    utils.addPlayNums(customNum);
  },
  //视频解锁
  videoUnlock(customNum) {
    return utils.getVideoUnlock(customNum);
  },
  //体力变化监听
  //type[1:视频,2:定时自动恢复]
  onPowerChange(_callback) {
    s_d.powerChangeCB = _callback;
  },
  //设置体力
  setPower(powerNum) {
    utils.setPower(powerNum);
  },
  //获取体力
  getPower() {
    return s_d.uPnum;
  },
  // 获取玩家微信平台账号信息
  getUserPlatFormInfo() {
    let userPFinfo = s_d.u_pt_info;
    if (!userPFinfo) {
      userPFinfo = utils.getPTinfo();
      userPFinfo = (userPFinfo == '') ? null : JSON.parse(userPFinfo);
    }
    s_d.u_pt_info = userPFinfo;
    return s_d.u_pt_info;
  },
  setUserPlatFormInfo(u_pt_info) {
    s_d.u_pt_info = u_pt_info;
    utils.savePTinfo(JSON.stringify(s_d.u_pt_info));
    nets.Edit(s_d.u_pt_info);
  },
  //获取游戏配置信息
  getGameConfig() {
    return s_d.gConf;
  },
  getShareConfig() {
    return s_d.sharecard_config;
  },
  // 分享图统计（打点统计）
  statisticShareCard(share_card_id) {
    let _data = {
      sharecardId: share_card_id
    };
    nets.shareCardTJ(_data);
  },
  //获取分享场景
  getShareScene() {
    let share_scene = s_d.share_scene ? s_d.share_scene : s_d.layer;
    share_scene = share_scene || 'default';
    return share_scene;
  },
  //设置分享场景
  setShareScene(scene) {
    s_d.share_scene = scene ? scene : s_d.layer;
    utils.sdk_log("setShareScene-scene:" + scene + ",layer:" + s_d.layer);
    if (!s_d.share_scene) {
      utils.sdk_log("setShareScene-need scene", 'warn');
      s_d.share_scene = 'default';
    }
    return s_d.share_scene;
  },
  setLastTime() {
    if (s_d.accountPass) {
      s_d.lastTime = new Date().getTime();
    } else {
      s_d.need_tj_on_show = true;
    }
  },
  getBannerTimeSpace() {
    return s_d.banner_time_space;
  },
  getLocalStatistics() {
    s_d.tj = utils.getLocalTJ();
  },
  addPower() {
    utils.addPower(1);
  },
  addWatchTVnums(unlockCustomNum) {
    return utils.addWatchTVnums(unlockCustomNum);
  },
  getAccountId() {
    return utils.getAccountId();
  },
  removeItemFrom(toAppid) {
    utils.removeItemFrom(toAppid);
  },
  ClickOut(iconId, targetAppId, source, isClick) {
    nets.ClickOut(iconId, targetAppId, source, isClick);
  },
  registerInterval() {
    utils.registerInterval();
  },
  unRegisterInterval() {
    utils.unRegisterInterval();
  },
  savePowerLocal() {
    utils.savePowerLocal();
  },
  statisticsPlayTime() {
    utils.PlayTimeTJ();
  },
  commitStaticsLayer() {
    nets.commitTJlayer();
  },
  saveStatisticsToLocal() {
    utils.saveTJtoLocal();
  },
  getSharingResults(time_space, _callback) {
    utils.getSharingResults(time_space, _callback);
  },
  statisticsBannerClick() {
    utils.bannerClickTJ();
  },
  getBoxConfig() {
    return s_d.boxconfig;
  },
  setLoginInfo(info) {
    s_d.loginInfo = info;
  },
  //获取体力相关信息
  getPowerInfo(_callback) {
    utils.getPowerInfo(_callback);
  },
}
module.exports = yl_sdk;
