/**
 * 积分墙Item
 **/
cc.Class({
    extends: cc.Component,

    properties: {
        appIcon: cc.Sprite,
        lbTitle: cc.Label,
        spAward:cc.Sprite,
        lbAwardNum: cc.Label,
        awardSF:[cc.SpriteFrame],
    },

    updateCell (obj) {
        this._obj = obj;        
        let name = window.Global.handleNameLen(this._obj.title,5);
        this.lbTitle.string = name;
        if (this._obj.icon && this._obj.icon !== '') {
            if (this._obj.texture) {
                this.setTex(this._obj.texture);
            } else {
                cc.director.on(window.Global.EVENT_LOAD_BOARD_ICON + this._obj._id, (evt) => {
                    this.setTex(evt.getUserData());
                }, this); 
            }
        }else {
           // console.log('url是空的：this._obj.icon = ' + this._obj.icon);
        }
        //此处可自己根据自己的情况自行定义
        if(this._obj.awardList && this._obj.awardList.length >0){
            let award_1 = this._obj.awardList[0]
            if(award_1.value) this.lbAwardNum.string = award_1.value
            if(award_1.type){
                let tex = (award_1.type == "gold") ? this.awardSF[1] : this.awardSF[0];
                let size = this.spAward.node.getContentSize();
                this.spAward.spriteFrame = tex;
                this.spAward.node.setContentSize(size);
            } 
        }
    },

    clickTryPlay(){
        //跳转小游戏
       let j_data =  {
                _id: this._obj._id,
                toAppid:this._obj.toAppid,   
                toUrl:this._obj.toUrl,
                type:this._obj.type,
                showImage:this._obj.showImage,
                source:'item_board',//从哪个模块导出的，该字段具体值由调用方自行定义
                awardStatus:this._obj.awardStatus
            };
        let j_event = new cc.Event.EventCustom('INVITE_JUMP_TO_MIN_GAME');
        j_event.setUserData(j_data);
        cc.director.dispatchEvent(j_event);
    },

    setTex(tex) {
        if (!cc.isValid(this.node)) return;
        let size = this.appIcon.node.getContentSize();
        this.appIcon.spriteFrame = new cc.SpriteFrame(tex);
        this.appIcon.node.setContentSize(size);
    },
});
