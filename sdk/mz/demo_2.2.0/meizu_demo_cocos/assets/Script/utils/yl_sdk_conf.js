exports.ylsdk_app_id = "19868920660"; //游戏APPID
exports.ylsdk_version = "1.0.0";    //游戏版本号
exports.ylsdk_debug_model = true;   //是否debug模式 [true:打印日志,false:不打印日志]
exports.login_by_code = true;      //配置为true，则每次都会走平台登录
exports.login_by_imei = true;	//是否每次通过游客(IMEI)登录
exports.ylsdk_pkg_name="com.szylhy.zwjsdzz.mzgame"; //游戏包名
exports.web_test = false;            //web测试模式(该模式需将平台设置为 影流小游戏)

exports.side_min_num = 20;       //侧边栏列表item最小保留数(基于曝光策略)

exports.ylsdk_banner_ids = [	 //banner广告ID
	'AiVo8Ijb'
]; 
exports.ylsdk_video_ids = [		//激励视频广告ID
	'ByykrF4B'
];							  
exports.ylsdk_interstitial_ids = [	//插屏广告ID
	'xViWNvuK'
];
                      

/****************微信公众平台配置以下合法域名************************/

// 服务器地址：		   https://api.ylxyx.cn
// 图片服务器地址1：	   https://ql.ylxyx.cn
// 图片服务器地址2：     https://tx.ylxyx.cn
// 图片服务器地址3：	   https://ext.ylxyx.cn

/*****************************************************************/
