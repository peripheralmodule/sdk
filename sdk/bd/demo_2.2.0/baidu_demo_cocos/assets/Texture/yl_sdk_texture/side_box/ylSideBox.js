/**
 * 侧边栏卖量列表
 */
cc.Class({
    extends: cc.Component,

    properties: {
        nodeContent:cc.Node,
        bItem:cc.Prefab,
    },
    
    start () {
        cc.director.on(window.Global.EVENT_REFRESH_SIDEBOX, this.onRefreshSideBox.bind(this)); 
    },
    //刷新卖量列表
    onRefreshSideBox(){
        swan.ylSideBox(function (data) {
            this.boxDatas = data;
            this.getSideboxBack(data,false);                
        }.bind(this));
    },

    showView () {
        this.node.active = true;
        if(!this.items) this.items = new Array();
        if(this.items.length > 0){
            this.showItemAnim();
        }else{
            if(this.boxDatas){
                this.getSideboxBack(this.boxDatas,true);
            }else{
                var that = this
                swan.ylSideBox(function (data) {
                    that.boxDatas = data;
                    that.getSideboxBack(data,true);                
                }.bind(this));
            }
        }
    },

    getSideboxBack(data,runAnim) {
        this.nodeContent.removeAllChildren();
        this.items = [];
        if (data && data.length > 0) {
            if (!cc.isValid(this.nodeContent)) return;
            this.loadGameIcon(0);
            let count =  data.length > 12 ? 12 : data.length;
            let left = -238,space_left = 236,top=96,bottom = -123,space_top = 219;
            for(let i=0;i<count;i++){
                let element = data[i];
                let box = cc.instantiate(this.bItem);
                box.parent = this.nodeContent;
                if(runAnim)  box.opacity = 0;
                box.getComponent('ylSideBoxItem').updateCell(element,'MainView');
                this.items.push(box);
            }
            if(runAnim) this.showItemAnim();
        }
    },

    /**
     * 加载侧边栏盒子item的游戏ICON
     * @param index 列表ID
    **/
    loadGameIcon(index) {
        let self = this;
        if (index < this.boxDatas.length) {
            let data = this.boxDatas[index];
            if (!data.texture && data.icon && data.icon !== '') {
                cc.loader.load(data.icon, (err, texture) => {
                    if (!err) {
                        self.boxDatas[index].texture = texture;
                        let evt = new cc.Event.EventCustom(window.Global.EVENT_LOAD_SIDE_BOX_ICON + data._id);
                        evt.setUserData(texture);
                        cc.director.dispatchEvent(evt);
                    }
                    index++;
                    self.loadGameIcon(index);
                });
            } else {
                index++;
                self.loadGameIcon(index);
            }
        }
    },

    showItemAnim(){
        let delayT = 0;
        this.items.forEach(element => {
            element.setScale(0);
            element.stopAllActions();
            element.runAction(cc.sequence(
                cc.delayTime(delayT),
                cc.callFunc(function () {
                    element.opacity = 255;
                }, this, 0),
                cc.scaleTo(0.2, 1, 1)));
            delayT +=0.1;
        });
    },

    onClose(){
        this.node.active = false;
        if(this.items){
            this.items.forEach(element => {
                element.stopAllActions();
            });
        }
    },
});
