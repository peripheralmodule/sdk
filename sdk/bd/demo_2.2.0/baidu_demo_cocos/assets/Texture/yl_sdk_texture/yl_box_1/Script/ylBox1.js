
cc.Class({
    extends: cc.Component,
    properties: {
        nodeContent:cc.Node,
        nodeNews:cc.Node,
        nodePGV:cc.Node,
        pfbGrid:cc.Prefab,
        pfbScroll:cc.Prefab,
        nodeCollect:cc.Node,
        nodeCollectMask:cc.Node,
    },
    //刷新卖量列表
    onRefreshSideBox(){
        swan.ylSideBox(function (data) {
            this.datas = data;
            this.initData();                
        }.bind(this));
    },

    start() { 
        let that = this;
        this.scene_name = "Box_";
        swan.ylSideBox(function (data) {
            if (data && data.length > 0) {
                that.datas = data;
                that.initData();
            }                
        }.bind(that));
        this.startAnimCollect();
        cc.director.on(window.Global.EVENT_REFRESH_SIDEBOX, this.onRefreshSideBox.bind(this)); 
    },

    initData(){
        this.nodeContent.removeAllChildren();
        if (this.datas && this.datas.length>0) {
            let gridTypeInfo = [
                {typeName:"爆款推荐",typeIconId:0},
                {typeName:"抖音最火",typeIconId:1},
            ];
            let count = this.datas.length;     
            let grid_1 = cc.instantiate(this.pfbGrid);  
            let endIndex = count >= 8 ? 8 : count;
            grid_1.getComponent('ylGridBoxByType').initData(0,endIndex,this.datas,gridTypeInfo[0],this.scene_name);
            grid_1.parent = this.nodeContent;
            if(count > 8){
                let grid_2 = cc.instantiate(this.pfbGrid);  
                grid_2.getComponent('ylGridBoxByType').initData(8,count,this.datas,gridTypeInfo[1],this.scene_name);
                grid_2.parent = this.nodeContent;
            }
            let scroll_1 = cc.instantiate(this.pfbScroll);  
            scroll_1.getComponent('ylScrollBoxByType').initData(0,count,this.datas,this.scene_name);
            scroll_1.parent = this.nodeContent;

            this.nodePGV.getComponent('ylBox1PageView').setData(this.datas);
            //
            if(this.datas.length>0){
                this.cfg = this.datas[0];
            }
        }else{
            this.nodeNews.active = false;
        }
    },

    clickCallBack() {
        this.nodeNews.active = false;
        let source = this.scene_name+"News";//从哪个模块导出的，该字段具体值由调用方自行定义

        swan.ylNavigateToMiniProgram(
            {
                _id: this._obj._id,
                toAppid:this._obj.toAppid,   
                toUrl:this._obj.toUrl,
                type:this._obj.type,
                showImage:this._obj.showImage,
                source:this.scene_name+"News",//从哪个模块导出的，该字段具体值由调用方自行定义
            },
            function(success){
                if(success){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_REFRESH_SIDEBOX);
                    cc.director.dispatchEvent(evt);
                }else{
                    //干点什么
                }
        }.bind(this));
    },
    startAnimCollect(){
        this.nodeCollect.getComponent(cc.Animation).play();
    },

    stopAnimCollect(){
        this.nodeCollect.getComponent(cc.Animation).stop();
    },
    onClickCollect(){
        let source = this.scene_name+"Collect";
        //事件统计
        swan.ylEventCount(source);
        this.nodeCollectMask.active = true;
    },
    onClickCollectMask(){
        this.nodeCollectMask.active = false;
    },
});
