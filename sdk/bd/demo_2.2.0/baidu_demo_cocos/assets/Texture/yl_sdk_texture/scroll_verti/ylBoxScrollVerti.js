/**
 * 垂直滚动卖量布局
 **/
cc.Class({
    extends: cc.Component,

    properties: {
        scroll:cc.ScrollView,
        nodeContent:cc.Node,
        bItem:cc.Prefab,
    },
    
    start () {
        this.moveStep = 1.5;
        this.endSpace = 100;
        cc.director.on(window.Global.EVENT_REFRESH_SIDEBOX, this.onRefreshSideBox.bind(this)); 
    },
    //刷新卖量列表
    onRefreshSideBox(){
        swan.ylSideBox(function (data) {
            this.boxDatas = data;
            this.getSideboxBack(data,false);                
        }.bind(this));
    },

    /**
    * 显示垂直滚动卖量布局
    *  @param _x x坐标
    *  @param _y y坐标
    */
    showView(_x,_y){
        this.node.x = _x;
        this.node.y = _y;// +this.node.height/2;
        if(!this.items) this.items = new Array();
        this.node.active = true;
        if(this.items.length > 0){
            this.dropItemAnim();
        }else{
            if(this.boxDatas){
                this.getSideboxBack(this.boxDatas,true);
            }else{
                var that = this
                swan.ylSideBox(function (data) {
                    that.boxDatas = data;
                    that.getSideboxBack(data,true);                
                }.bind(this));
            }
        }
    },

    getSideboxBack(data,runAnim) {
        this.nodeContent.removeAllChildren();
        this.items = [];
        if (data && data.length > 0) {
            this.loadGameIcon(0);            
            for(let i=0;i<data.length;i++){
                let element = data[i];
                let box = cc.instantiate(this.bItem);
                box.parent = this.nodeContent;
                box.getComponent('ylBoxVertiItem').setInfo(element,'MainView');
                this.items.push(box);
            }
            if(runAnim){
                this.dropItemAnim();
            }else{
                this.runScrollAnim();
            } 
        }
    },
    //
    dropItemAnim(){
        let delayT = 0;
        this.onShow = true;
        for(let i=0;i<this.items.length;i++){
            let element = this.items[i];
            let move_space = 800;
            element.x = element.x + move_space;
            element.stopAllActions();
            if(i == (this.items.length-1)){
                element.runAction(
                    cc.sequence(
                        cc.delayTime(delayT),
                        cc.moveTo(0.3,element.x-move_space,element.y),
                        cc.callFunc(function () {
                            this.runScrollAnim();
                        }, this, 0)));
            }else{
                element.runAction(
                    cc.sequence(cc.delayTime(delayT),
                        cc.moveTo(0.3, element.x-move_space, element.y)));
            }
            delayT +=0.1;
        }
    },

    runScrollAnim(){
        this.nodeContent.stopAllActions();
        let scrollHeight = this.scroll.node.height;
        this.nodeContent.runAction(cc.repeatForever( cc.sequence(cc.delayTime(0.010), cc.callFunc(function(){
            if(scrollHeight < this.nodeContent.height)
            {
                let minX = this.nodeContent.height-scrollHeight;
                if(this.nodeContent.y >= scrollHeight/2+minX+this.endSpace)
                {
                    this.moveStep = -Math.abs(this.moveStep);
                }
                else if(this.nodeContent.y <= scrollHeight/2-this.endSpace)
                {
                    this.moveStep = Math.abs(this.moveStep);
                }
                this.nodeContent.y += this.moveStep;
            }

        }.bind(this)))))
    },

    /**
     * 加载侧边栏盒子item的游戏ICON
     * @param index 列表ID
    **/
    loadGameIcon(index) {
        let self = this;
        if (index < this.boxDatas.length) {
            let data = this.boxDatas[index];
            if (!data.texture && data.icon && data.icon !== '') {
                cc.loader.load(data.icon, (err, texture) => {
                    if (!err) {
                        self.boxDatas[index].texture = texture;
                        let evt = new cc.Event.EventCustom(window.Global.EVENT_LOAD_SCROLL_VERTI_ICON + data._id);
                        evt.setUserData(texture);
                        cc.director.dispatchEvent(evt);
                    }
                    index++;
                    self.loadGameIcon(index);
                });
            } else {
                index++;
                self.loadGameIcon(index);
            }
        }
    },

    onClose(){
        this.onShow = false;
        this.node.active = false;
        if(this.items){
            this.items.forEach(element => {
                element.stopAllActions();
            });
        }
    },
});
