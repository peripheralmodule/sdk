
cc.Class({
    extends: cc.Component,

    properties: {
        spIcon:cc.Sprite,
        lbName:cc.Label,
        lbPlayNum:cc.Label,
    },

   setItemConf(obj,_scene_name,type){
        this.cfg = obj;
        let titles = obj.title.split(",");
        let title_1 = titles[0];
        let title_2 = titles.length >1 ? titles[1] : titles[0];        
        this.lbName.string = (type == 1) ? title_1 : title_2;
        
        this.scene_name = _scene_name;
        let url = (type == 1) ? obj.icon : obj.rectangleImage; //type:1:icon，2：bannerImage
        this.loadImg(url);
        // this.lbName.string = obj.title;
        this.lbPlayNum.string = obj.playerNum+"人在玩"
    },
    setTypeBarAndLocation(typeBar,location){
        this.typeBar = typeBar;
        this._location = location;
    },

    loadImg(url) {
        if(!url || url == ""){
            let sType =(type == 1) ? 'icon' : 'rectangleImage';
            console.error("---"+sType+" is null");
            return;
        }
        var img = swan.createImage();
        img.onload = function() {
            var tex = new cc.Texture2D();
            tex.initWithElement(img);
            tex.url = url;
            tex.handleLoadedTexture();
            this.setRes(new cc.SpriteFrame(tex));
        }.bind(this);
        img.src = url;
    },

    // 改变纹理
    setRes(spriteFrame) {
        if (!cc.isValid(this.node)) return;
        let size = this.spIcon.node.getContentSize();
        this.spIcon.spriteFrame = spriteFrame;
        this.spIcon.node.setContentSize(size);
    },

    clickCallBack() {
        swan.ylNavigateToMiniProgram(
            {
                _id: this.cfg._id,
                toAppid:this.cfg.toAppid,   
                toUrl:this.cfg.toUrl,
                type:this.cfg.type,  
                showImage:this.cfg.showImage, 
                source:this.scene_name+"_Grid",
            },
            function(success){
                if(success){
                    let evt = new cc.Event.EventCustom(window.eGlobal.EVENT_REFRESH_SIDEBOX);
                    cc.director.dispatchEvent(evt);
                }
        }.bind(this));

    },
});
