
cc.Class({
    extends: cc.Component,

    properties: {
        spIcon:cc.Sprite,
        lbName:cc.Label,
        lbPlayNum:cc.Label,
        nodeStars:[cc.Node],
    },

    setItemConf(obj,_scene_name){
        this.cfg = obj;
        this.scene_name = _scene_name;
        this.loadImg(obj.frameImage);
        let titles = obj.title.split(",");
        let title_ = titles.length > 2 ? titles[2] : "无名";
        this.lbName.string = title_;
        this.lbPlayNum.string = obj.playerNum+"人在玩"
        let s_count = obj.star ? obj.star : 5;
        s_count = s_count > this.nodeStars.length ? this.nodeStars.length : s_count;
        for(let i=0;i<s_count;i++){
            this.nodeStars[i].active = true;
        }
    },
    setTypeBarAndLocation(typeBar,location){
        this.typeBar = typeBar;
        this._location = location;
    },

    loadImg(url) {
        if (!url) {
            console.error('gameBtn数据有误 cfg: ' + JSON.stringify(this.cfg));
            return;
        }
        var self = this;
        if (!wx) {
            cc.loader.load(url, function (err, tex) {
                if (!err) {
                    var spFrame = new cc.SpriteFrame();
                    spFrame.setTexture(tex);
                    self.setRes(spFrame);
                } else {
                    console.error('拉取资源失败！');
                }
            });
        } else {
            var img = swan.createImage();
            img.onload = function() {
                var tex = new cc.Texture2D();
                tex.initWithElement(img);
                tex.url = url;
                tex.handleLoadedTexture();
                self.setRes(new cc.SpriteFrame(tex));
            }.bind(this);
            img.src = url;
        }
    },

    clickCallBack() {
        swan.ylNavigateToMiniProgram(
            {
                _id: this.cfg._id,
                toAppid:this.cfg.toAppid,   
                toUrl:this.cfg.toUrl,
                type:this.cfg.type,
                showImage:this.cfg.showImage,
                source:this.scene_name+"Grid",//从哪个模块导出的，该字段具体值由调用方自行定义
            },
            function(success){
                if(success){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_REFRESH_SIDEBOX);
                    cc.director.dispatchEvent(evt);
                }else{
                    //干点什么
                }
        }.bind(this));
    },
    // 改变纹理
    setRes(spriteFrame) {
        let size = this.spIcon.node.getContentSize();
        this.spIcon.spriteFrame = spriteFrame;
        this.spIcon.node.setContentSize(size);
    },
});
