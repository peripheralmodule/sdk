/**
 * 卖量(水平/垂直)滚动Item公用
 **/
cc.Class({
    extends: cc.Component,

    properties: {
        appIcon: cc.Sprite,
        labTitle: cc.Label,
    },

    setInfo(obj,scene) {
        this._obj = obj;
        this._scene = scene;
        let name = window.Global.handleNameLen(this._obj.title,4);        
        this.labTitle.string = name;
        if (this._obj.icon && this._obj.icon !== '') {
            if (this._obj.texture) {
                this.setTex(this._obj.texture);
            } else {
                cc.director.on(window.Global.EVENT_LOAD_GRID_ICON + this._obj._id, (evt) => {
                    this.setTex(evt.getUserData());
                }, this); 
            }
        }else {
           // console.log('url是空的：this._obj.icon = ' + this._obj.icon);
        }
    },

    onJumpOut(){
        swan.ylNavigateToMiniProgram(
            {
                _id: this._obj._id,
                toAppid:this._obj.toAppid,   
                toUrl:this._obj.toUrl,
                type:this._obj.type,  
                showImage:this._obj.showImage,              
                source:'item_board',//从哪个模块导出的，该字段具体值由调用方自行定义
            },
            function(success){
                if(success){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_REFRESH_SIDEBOX);
                    cc.director.dispatchEvent(evt);
                }else{
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_FULL_BOX);
                    cc.director.dispatchEvent(evt);
                }
        }.bind(this));
    },

    setTex(tex) {
        if (!cc.isValid(this.node)) return;
        let size = this.appIcon.node.getContentSize();
        this.appIcon.spriteFrame = new cc.SpriteFrame(tex);
        this.appIcon.node.setContentSize(size);
    },
});
