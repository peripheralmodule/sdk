
cc.Class({
    extends: cc.Component,

    properties: {
        scroll:cc.ScrollView,
        nodeScrollView:cc.Node,
        grid:cc.ScrollView,
        nodeGridView:cc.Node,
        pbScrollItem:cc.Prefab,
        pbGridItem:cc.Prefab,
    },

    start () {
        this.scrollMove = 1.5;
        this.gridMove = 1.5;
        this.endSpace = 100;
        cc.director.on(window.Global.EVENT_REFRESH_SIDEBOX, this.onRefreshSideBox.bind(this)); 
    },
    //刷新卖量列表
    onRefreshSideBox(){
        swan.ylSideBox(function (data) {
            this.boxDatas = data;
            this.getBoxDataBack(data,false);                
        }.bind(this));
    },

    showView(){
        if(!this.items_grid) this.items_grid = new Array();
        if(!this.items_scroll) this.items_scroll = new Array();
        this.node.active = true;
        if(this.items_scroll.length > 0){
            this.onShow = true;
            this.onScrollMoveLeft();
            this.onGridMoveTop();
        }else{
            if(this.boxDatas){
                this.getBoxDataBack(this.boxDatas,true);
            }else{
                var that = this
                swan.ylSideBox(function(data) {
                    that.boxDatas = data;
                    that.getBoxDataBack(data,true);                
                }.bind(this));
            }
        }
    },

    getBoxDataBack(data,runAnim) {
        this.nodeScrollView.removeAllChildren();
        this.nodeGridView.removeAllChildren();
        this.items_grid = [];
        this.items_scroll = [];
        if (data && data.length >0) {
            this.loadGameIcon(0);            
            for(let i=0;i<data.length;i++){
                let element = data[i];
                let item_scroll = cc.instantiate(this.pbScrollItem);
                item_scroll.parent = this.nodeScrollView;
                item_scroll.getComponent('ylBoxFullScrollitem').setInfo(element,'MainView');
                this.items_scroll.push(item_scroll);
                //
                let item_grid = cc.instantiate(this.pbGridItem);
                item_grid.parent = this.nodeGridView;
                item_grid.getComponent('ylBoxFullGridItem').setInfo(element,'MainView');
                this.items_grid.push(item_grid);
            }
            this.onShow = true;
            if(runAnim) this.onScrollMoveLeft();
            if(runAnim) this.onGridMoveTop();
        }
    },

    /**
     * 加载侧边栏盒子item的游戏ICON
     * @param index 列表ID
    **/
    loadGameIcon(index) {
        let self = this;
        if (index < this.boxDatas.length) {
            let data = this.boxDatas[index];
            if (!data.texture && data.icon && data.icon !== '') {
                cc.loader.load(data.icon, (err, texture) => {
                    if (!err) {
                        self.boxDatas[index].texture = texture;
                        let evt_scroll = new cc.Event.EventCustom(window.Global.EVENT_LOAD_FULL_SCROLL_BOX_ICON + data._id);
                        evt_scroll.setUserData(texture);
                        cc.director.dispatchEvent(evt_scroll);
                        //
                        let evt_grid = new cc.Event.EventCustom(window.Global.EVENT_LOAD_FULL_GRID_BOX_ICON + data._id);
                        evt_grid.setUserData(texture);
                        cc.director.dispatchEvent(evt_grid);
                    }
                    index++;
                    self.loadGameIcon(index);
                });
            } else {
                index++;
                self.loadGameIcon(index);
            }
        }
    },

    onScrollMoveLeft(){
        this.nodeScrollView.stopAllActions();
        let scrollWidth = this.scroll.node.width;
        this.nodeScrollView.runAction(cc.repeatForever( cc.sequence(cc.delayTime(0.010), cc.callFunc(function(){
            if(scrollWidth < this.nodeScrollView.width)
            {
                let minX = scrollWidth - this.nodeScrollView.width;
                if(this.nodeScrollView.x >= -scrollWidth/2+this.endSpace)
                {
                    this.scrollMove = -Math.abs(this.scrollMove);
                }
                else if(this.nodeScrollView.x <= minX - scrollWidth/2-this.endSpace)
                {
                    this.scrollMove = Math.abs(this.scrollMove);
                }

                this.nodeScrollView.x += this.scrollMove;
            }

        }.bind(this)))))
    },
    onGridMoveTop(){
        this.nodeGridView.stopAllActions();
        let scrollHeight = this.grid.node.height;
        this.nodeGridView.runAction(cc.repeatForever( cc.sequence(cc.delayTime(0.010), cc.callFunc(function(){
            if(scrollHeight < this.nodeGridView.height)
            {
                let minX = this.nodeGridView.height-scrollHeight;
                if(this.nodeGridView.y >= scrollHeight/2+minX+this.endSpace)
                {
                    this.gridMove = -Math.abs(this.gridMove);
                }
                else if(this.nodeGridView.y <= scrollHeight/2-this.endSpace)
                {
                    this.gridMove = Math.abs(this.gridMove);
                }
                this.nodeGridView.y += this.gridMove;
            }

        }.bind(this)))))
    },

    onBack(){
        this.onShow = false;
        this.node.active = false;
        if(this.items_scroll){
            this.items_scroll.forEach(element => {
                element.stopAllActions();
            });
        }
        if(this.items_grid){
            this.items_grid.forEach(element => {
                element.stopAllActions();
            });
        }
    },
});
