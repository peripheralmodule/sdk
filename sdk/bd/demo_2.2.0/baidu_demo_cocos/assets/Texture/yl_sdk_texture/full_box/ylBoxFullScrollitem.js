
cc.Class({
    extends: cc.Component,
    properties: {
        appIcon:cc.Sprite,
    },

    setInfo(obj,scene) {
        this._obj = obj;
        this._scene = scene;
        let name = window.Global.handleNameLen(this._obj.title,4);        
        if (this._obj.icon && this._obj.icon !== '') {
            if (this._obj.texture) {
                this.setTex(this._obj.texture);
            } else {
                cc.director.on(window.Global.EVENT_LOAD_FULL_SCROLL_BOX_ICON + this._obj._id, (evt) => {
                    this.setTex(evt.getUserData());
                }, this); 
            }
        }else {
           // console.log('url是空的：this._obj.icon = ' + this._obj.icon);
        }
    },

    onJumpOut(){
        swan.ylNavigateToMiniProgram(
            {
                _id: this._obj._id,
                toAppid:this._obj.toAppid,   
                toUrl:this._obj.toUrl,
                type:this._obj.type,
                showImage:this._obj.showImage,
                source:'item_full_scroll',//从哪个模块导出的，该字段具体值由调用方自行定义
            },
            function(success){
                if(success){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_REFRESH_SIDEBOX);
                    cc.director.dispatchEvent(evt);
                }else{
                    //干点什么
                }
        }.bind(this));
    },
    setTex(tex) {
        if (!cc.isValid(this.node)) return;
        let size = this.appIcon.node.getContentSize();
        this.appIcon.spriteFrame = new cc.SpriteFrame(tex);
        this.appIcon.node.setContentSize(size);
    },
});
