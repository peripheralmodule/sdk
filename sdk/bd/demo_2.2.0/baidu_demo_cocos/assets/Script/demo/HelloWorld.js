cc.Class({
    extends: cc.Component,

    properties: {
        label:cc.Label,
        btnShare:cc.Node,               //分享按钮
        btnSideBox:cc.Node,             //侧边栏列表按钮
        btnBoardAwardList:cc.Node,      //积分墙列表按钮
        btnGridAndScrollList:cc.Node,   //浮动卖量列表按钮
        btnSign:cc.Node,                //签到按钮

        pbSideBox:cc.Prefab,            //侧边栏列表界面
        pbInvite:cc.Prefab,             //积分墙列表界面
        pbYlBoxGrid:cc.Prefab,          //卖量网格浮动条
        pbYlBoxScrollHori:cc.Prefab,    //卖量横向浮动条
        pbYlBoxScrollVerti:cc.Prefab,   //卖量纵向浮动条
        pbYlGameOver:cc.Prefab,         //游戏结束界面
        pbSign:cc.Prefab,               //签到界面
        pbGetAward:cc.Prefab,           //获得奖励View
        pbFullBox:cc.Prefab,            //全屏卖量界面
        pbBreakEgg:cc.Prefab,           //砸金蛋
        pbOpenBox:cc.Prefab,            //开宝箱
        pbShakeTree:cc.Prefab,          //摇钱树
        preRank:cc.Prefab,              //排行榜
        pbBox1:cc.Prefab,               //盒子1
        nodeFloatView:cc.Node,          //浮动卖量View
        lbGridAd:cc.Label,              //格子广告按钮文字
        pbFullBox2:cc.Prefab,               //全屏卖量
    },
    onLoad: function () {
        console.log("-----onLoad-start");
        this.initEvent();
        let self = this;
        swan.ylSetCurScene("MainScene");//一定记得在每个场景都要设置当前场景
        swan.ylGameLifeListen(function(type,res){
            if(type === 'onShow'){
                console.warn("YLSDK HelloWorld-onShow--res:",res);
            }else if(type === 'onHide'){
                console.warn("YLSDK HelloWorld-onHide");
            }else if(type === 'onError'){
                console.warn("YLSDK HelloWorld-onError--res:",res);
            }
        });
        swan.ylOnPowerChange(function(type){
            console.warn("HelloWorld-ylOnPowerChange back[1:视频,2:定时自动恢复]:",type);
            let textType = (type == 1) ? "看视频" : "定时";
            swan.showToast({
                  title: textType+'获得体力',
                  icon: 'success',
                  duration: 2000
                });
        }.bind(this));
        swan.ylInitSDK(function(success){
            console.log("HelloWorld-ylInitSDK:",success);
            if(success === true){
                console.warn("HelloWorld-ylInitSDK-初始化成功");
                self.doSDKinterface();
            }else if(success == 'config_success' || success == 'config_fail'){
                console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
                swan.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("HelloWorld-ylGetPowerInfo:",data);
                });
            }else{
                console.warn("HelloWorld-ylInitSDK-初始化失败");
            }
        },1);
        this.custom = 0;
        // this.onFullBox2Show();

        // let btnInfo = swan.ylGetLeftTopBtnPosition();
        // console.log("----btnInfo:",JSON.stringify(btnInfo));
        // this.btnSideBox.width = btnInfo.width;
        // this.btnSideBox.height = btnInfo.height;
        // this.btnSideBox.y = btnInfo.y;
        // this.btnSideBox.x = btnInfo.x+this.btnSideBox.width/2;
        console.log("-----onLoad-end");
        let data = swan.ylGetSwitchInfo();
        console.log("---刷新开关状态---",data);
    },

    /**
     * 初始化事件
     **/
    initEvent(){
        cc.director.on('INVITE_JUMP_TO_MIN_GAME', this.inviteJumpToMinGame.bind(this));
        cc.director.on(window.Global.EVENT_SHOW_GET_AWARD, this.onShowGetAward.bind(this)); 
        cc.director.on(window.Global.EVENT_SHOW_TIP, this.showTip.bind(this));
        cc.director.on(window.Global.EVENT_SHOW_FULL_BOX, this.onShowFullBox.bind(this)); 
    },

    /**
    * 调用SDK接口
    **/
    doSDKinterface(){
        swan.ylLog("---调用接口---","log");
        swan.ylSideBox(function (data) {
        }.bind(this));
        swan.ylGetCustom(function(data){
            // if(!this.nodeBox1) this.nodeBox1 = cc.instantiate(this.pbBox1);
            // this.nodeBox1.x = 0;
            // this.nodeBox1.y = 0;
            // this.nodeBox1.parent = this.node;
            // this.nodeBox1.active = true;
        }.bind(this));
        swan.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status){
            if(status){
                //干点什么
            }
        }.bind(this));
        swan.ylIntegralWall(function(){}.bind(this));
        swan.ylGetPowerInfo(function(data){
            console.log("HelloWorld ylGetPowerInfo--data:",data);
        });
        // let userWXinfo = swan.ylGetUserWXInfo();//先获取本地缓存，没有在弹窗通过用户授权获取
        // if(!userWXinfo){
        //     //是否收集用户信息按钮信息(具体配置请参考微信小游戏官方文档)
        //     swan.ylUserInfoButtonShow(
        //         {       
        //           type: 'image',
        //           image:'./images/sp_btn_start.png',  //图片地址
        //           style: {
        //             left: 100,
        //             top: 66,
        //             width: 150,
        //             height: 40,
        //             lineHeight: 40,
        //             borderRadius: 4
        //           }
        //         },
        //         function(data){
        //             console.warn("------ylUserInfoButtonShow-data:",data);
        //             if(data){
        //                 userWXinfo = data;
        //                 swan.ylUserInfoButtonHide();
        //             }
        //     }.bind(this));
        // }
        let loginInfo = swan.ylGetUserInfo();//获取登录信息，包括微信oppid和code(code只有第一次登录才能取到)
        console.log("---登录信息-loginInfo:",JSON.stringify(loginInfo));
        // swan.ylBannerAdCreate(true);
        // swan.ylBannerAdCreateSmall(true);


        var that = this;
        /**
         * 获取分享图列表
         */
        swan.ylShareCard(function (shareInfo) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
                this.btnShare.active = true;
            }else{
                //获取失败
            }
        }.bind(this),'MenuScene');
        // swan.ylStatisticViedo(1);
        swan.ylStatisticShareCard(177);
        swan.ylGetLayerList(function(data){
            if(data && data.length >0){
                this.layerList = data;
                swan.ylStatisticLayer(this.layerList[this.layerList.length-1].layerPath);
            }
        }.bind(this));
        
        // this.testStoreValue();
        let appid = swan.ylGetAppID();
        console.log("---ylGetAppID:",appid);
        swan.ylGetSignData(function(data){
            console.log("---ylGetSignData:",data);
        });
        swan.ylIntegralWall(function(data){

        });
        let inviteAccount = swan.ylGetInviteAccount();
        console.log("---ylGetInviteAccount:",inviteAccount);
    },

    //显示全屏卖量
    onFullBox2Show(){
        swan.ylEventCount("Click-SideBox");
        if(!this.nodeFullBox2) this.nodeFullBox2 = cc.instantiate(this.pbFullBox2);
        this.nodeFullBox2.x = 0;
        this.nodeFullBox2.y = 0;
        this.nodeFullBox2.parent = this.node;
        this.nodeFullBox2.getComponent('ylBox2').showView();
    },
    //显示侧边栏列表对话框
    onBoxShow(){
        swan.ylEventCount("Click-SideBox");
        if(!this.nodeBox) this.nodeBox = cc.instantiate(this.pbSideBox);
        this.nodeBox.x = 0;
        this.nodeBox.y = 0;
        this.nodeBox.parent = this.node;
        this.nodeBox.getComponent('ylSideBox').showView();
    },

    //显示积分墙列表对话框
    onBoardAwardListShow(){
        swan.ylEventCount("Click-BoardAward");
        if(!this.nodeInvite) this.nodeInvite = cc.instantiate(this.pbInvite);
        this.nodeInvite.x = 0;
        this.nodeInvite.y = 0;
        this.nodeInvite.parent = this.node;
        this.nodeInvite.getComponent('ylInvite').showView();
    },
    //显示浮动卖量列表
    onShowGridAndScrollList(){
    },
    onCloseFloatView(){
        this.nodeFloatView.active = false;
    },
    //显示获得奖励对话框
    onShowGetAward(evt){
    },
    showTip(tip){

    },
    //显示全屏卖量界面
    onShowFullBox(){        
    },
    //显示签到对话框 
    onShowSign(){
    },

    //分享对话框
    onClickShare(){
        swan.ylEventCount("Click-Share");
        swan.ylShareAppMessage(function(status){
            console.log("---------分享结果:",status);
        },'MenuScene');
    },
    //砸金蛋
    onClickBreakEgg(){
    },
    //开宝箱
    onOpenBox(){
    }, 
    //摇钱树
    onShakeTree(){
    }, 
    //显示获胜结算对话框
    onShowOverWin(){
    },
    //视频分享策略
    onShowShareOrVideo(){
        swan.ylShowShareOrVideo("bb","aa",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    break;
                case 2:
                    console.warn("------策略-视频");
                    break;
            }
        }.bind(this));
    },
    //显示失败结算对话框
    onShowOverFail(){
    },
    //显示视频广告
    onShowVideo(){
    },
    //显示banner
    onShowBanner(){
    },
    onHideBanner(){
    },
    //视频解锁关卡
    onVideoUnlockCustoms(){
    },
    onShowGameOver(isWin){
    },
    //切换界面,可调用强制刷新Banner接口
    onChangeView(){
        swan.ylChangeView();
    },
    //积分墙跳转到目标小游戏
    inviteJumpToMinGame(evt){
    },

    onShowRank(){
    },
    //游戏结束
    onOver(){
        this.custom +=1;
        swan.ylOverGame(this.custom);
    },
    //获取深度误触开关
    onGetDeepTouch(){
       let info = swan.ylGetDeepTouch(this.custom);
       console.log("onGetDeepTouch-info: ",JSON.stringify(info));
    },
    //消耗体力
    onConsumePower(){
       let power =  swan.ylGetPower();
       swan.ylSetPower(power -1);
    },
    onAddPower(){
        let power =  swan.ylGetPower();
        swan.ylSetPower(power +1);
    },
    //上传排行榜数据
    uploadScore(){
    },
    _strMapToObj(strMap){
        let obj= Object.create(null);
        for (let[k,v] of strMap) {
          obj[k] = v;
        }
        return obj;
    },
    //测试自定义空间值
    testStoreValue(){
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    },
    test_sv_string(){
        //String
        swan.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                swan.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get",
                        // args:"测试数据"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    },
    test_sv_list(){
        //List
        swan.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        swan.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        swan.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                swan.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all",
                        // args:""
                    },
                    function(status){
                        
                }.bind(this));
                 swan.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        swan.ylStoreValue(
            {
                name:"testList",
                cmd:"size",
                // args:""
            },
            function(status){
                
        }.bind(this));
        swan.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    swan.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size",
                            // args:""
                        },
                        function(status){
                            
                    }.bind(this));
                    swan.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            swan.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all",
                                    // args:""
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    },
    test_sv_set(){
        //Set
        swan.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        swan.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                swan.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                swan.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size",
                        // args:""
                    },
                    function(status){
                        swan.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                swan.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all",
                                        // args:""
                                    },
                                    function(status){
                                        swan.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            swan.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all",
                                                    // args:""
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    },
    test_sv_hash(){
        //litMap
        swan.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                swan.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        swan.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            swan.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            swan.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                                // args:"u_name,sex"
                            },function(status){}.bind(this));
                            swan.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"u_name,sex"
                            },function(status){}.bind(this));
                            swan.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                swan.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                    // args:""
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    },
    test_sv_radom(){
        //testRandom
        swan.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    },
});
