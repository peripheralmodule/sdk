exports.ylsdk_app_id = "17008570"; //游戏APPID
exports.ylsdk_version = "2.0.0"; 	//游戏版本号
exports.ylsdk_debug_model = true;   //是否debug模式 [true:打印日志,false:不打印日志]
exports.login_by_code = false;      //配置为true，则每次都会走平台登录
exports.ylsdk_pkg_name=""; //游戏包名（OPPO、VIVO平台必传）
// exports.ylsdk_app_key = "OvqebhCr6TdDgt68hh5NQ8WBZTdKGxdz"; //百度平台需要传
exports.ylsdk_app_sid = "e3e02a8c";
exports.side_min_num = 20;		//侧边栏列表item最小保留数(基于曝光策略)
exports.ylsdk_banner_ids = [	//banner广告ID(暂不支持)
    // "e3e02a8c",
    "7099843",    
    "6580230",
    "6580229",
]; 
exports.ylsdk_video_ids = [		//激励视频广告ID(暂不支持)
    "6580232",
    "6580222",
    "6580147",
];							  

/****************配置以下合法域名************************/

// 服务器地址：		   https://api.ylxyx.cn
// 图片服务器地址1：	   https://ql.ylxyx.cn
// 图片服务器地址2：     https://tx.ylxyx.cn
// 图片服务器地址3：	   https://ext.ylxyx.cn
// CDN服务器地址：	   https://ydhwimg.szvi-bo.com

/*******************************************************/

