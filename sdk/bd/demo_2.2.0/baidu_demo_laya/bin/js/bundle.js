(function () {
    'use strict';

    var REG = Laya.ClassUtils.regClass;
    var ui;
    (function (ui) {
        var test;
        (function (test) {
            class TestSceneUI extends Laya.Scene {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.loadScene("test/TestScene");
                }
            }
            test.TestSceneUI = TestSceneUI;
            REG("ui.test.TestSceneUI", TestSceneUI);
        })(test = ui.test || (ui.test = {}));
    })(ui || (ui = {}));

    class GameUI extends ui.test.TestSceneUI {
        constructor() {
            super();
            this.shareCount = 0;
            this.layerList = null;
            this.sideList = null;
            this.layerId = 0;
            this.custom = 1;
            GameUI.instance = this;
            Laya.MouseManager.multiTouchEnabled = false;
        }
        onEnable() {
            swan.ylGameLifeListen(function (type, res) {
                if (type === 'onShow') {
                    console.warn("YLSDK GameUI-onShow--res:", res);
                }
                else if (type === 'onHide') {
                    console.warn("YLSDK GameUI-onHide");
                }
                else if (type === 'onError') {
                    console.warn("YLSDK GameUI-onError--res:", res);
                }
            });
            swan.ylInitSDK(function (success) {
                console.log("-----初始化SDK-----:", success);
                if (success === true) {
                    console.warn("GameUI-ylInitSDK-初始化成功");
                    this.doSDKinterface();
                }
                else if (success == 'config_success' || success == 'config_fail') {
                    console.warn("GameUI-ylInitSDK-获取游戏配置回调");
                    swan.ylGetPowerInfo(function (data) {
                        console.log("GameUI-ylGetPowerInfo:", data);
                    });
                }
                else {
                    console.warn("GameUI-ylInitSDK-初始化失败");
                }
            }.bind(this));
            this.initEvent();
        }
        initEvent() {
            this.btnVideoOrShare.on(Laya.Event.CLICK, this, this.onShowShareOrVideo);
            this.btnShowVideoAd.on(Laya.Event.CLICK, this, this.onShowVideoAd);
            this.btnShowBannerAd.on(Laya.Event.CLICK, this, this.onShowBannerAd);
            this.btnHideBannerAd.on(Laya.Event.CLICK, this, this.onHideBannerAd);
            this.btnShare.on(Laya.Event.CLICK, this, this.onShare);
            this.btnJumpOut.on(Laya.Event.CLICK, this, this.onJumpOut);
            this.btnExit.on(Laya.Event.CLICK, this, this.onExit);
        }
        doSDKinterface() {
            console.log("---调用接口---");
            swan.ylSideBox(function (data) {
                this.sideList = data;
                console.log("GameUI ylSideBox:", this.sideList);
            }.bind(this));
            swan.ylGetCustom(function (data) {
            }.bind(this));
            swan.ylStatisticResult({ "total_score": 123, "rebirth_score": 123 }, function (status) {
                if (status) {
                }
            }.bind(this));
            let loginInfo = swan.ylGetUserInfo();
            console.log("---登录信息-loginInfo:", JSON.stringify(loginInfo));
            var that = this;
            swan.ylShareCard(function (shareInfo) {
                if (shareInfo) {
                    console.log("----获取分享图列表:", JSON.stringify(shareInfo));
                }
                else {
                }
            }.bind(this), 'MainScene');
            swan.ylStatisticViedo(0, 'adunit-6878a73e134f85e2', null);
            swan.ylStatisticShareCard(177);
            swan.ylGetLayerList(function (data) {
                if (data && data.length > 0) {
                    this.layerList = data;
                    swan.ylStatisticLayer(this.layerList[this.layerList.length - 1].layerPath);
                }
            }.bind(this));
            swan.ylGetServerInfo(function (data) {
                console.log("GameUI -ylGetServerInfo-back:", JSON.stringify(data));
            });
        }
        onShowShareOrVideo(e) {
            swan.ylShowShareOrVideo("bb", "aa", function (type) {
                switch (type) {
                    case 0:
                        console.warn("------策略-无");
                        break;
                    case 1:
                        console.warn("------策略-分享");
                        this.shareCount += 1;
                        let _channel = (this.shareCount % 2 == 0) ? 'bb' : 'cc';
                        let _module = (this.shareCount % 2 == 0) ? 'aa' : 'cc';
                        let shareInfo = {
                            channel: _channel,
                            module: _module,
                            scene: 'MenuScene',
                            inviteType: 'award_id',
                        };
                        let onShare = {
                            channel: _channel,
                            module: _module,
                            showTime: new Date().getTime() - 3500,
                        };
                        swan.ylGetSharingResults(onShare, function (result) {
                            console.warn("-----ylGetSharingResults-result：", result);
                            if (result.sSuccess) {
                            }
                            else {
                                if (result.hasStrategy) {
                                    console.warn("-----分享失败话术：", result.trickJson);
                                }
                            }
                        });
                        break;
                    case 2:
                        console.warn("------策略-视频");
                        let result = swan.ylRewardByVideoOrShare(false);
                        let slLimit = swan.ylGetVSLimit();
                        console.warn("---------ylRewardByVideoOrShare-result:", result, slLimit);
                        break;
                }
            }.bind(this));
        }
        onShowVideoAd(e) {
            swan.ylBannerAdHide();
            swan.ylShowVideoAd({
                callBack: function (status) {
                    console.log("----showVideo:" + status);
                    switch (status) {
                        case 1:
                            break;
                        case 2:
                            break;
                        case 0:
                            break;
                    }
                },
                unlockCustomNum: this.custom,
                getPower: true
            });
        }
        onShowBannerAd(e) {
            console.log("GameUI-onShowBanner:");
            swan.ylBannerAdCreateByStyle({
                left: 20,
                top: 100,
                width: 310,
            }, true, (res) => {
            }, true, (res) => {
                console.warn("GameUI-ylBannerAdCreateByStyle-onResize-back:", JSON.stringify(res));
            });
            swan.ylChangeBannerStyle({
                left: 0,
                top: 350,
                height: 30,
            });
        }
        onHideBannerAd(e) {
            console.log("GameUI-onHideBanner");
            swan.ylBannerAdHide();
        }
        onShare(e) {
            this.shareCount += 1;
            let _channel = (this.shareCount % 2 == 0) ? 'bb' : 'cc';
            let _module = (this.shareCount % 2 == 0) ? 'aa' : 'cc';
            let shareInfo = {
                channel: _channel,
                module: _module,
                scene: 'MainScene',
                inviteType: 'award_id',
            };
            swan.ylShareAppMessage(function (sResult) {
                if (sResult.sSuccess) {
                    let result = swan.ylRewardByVideoOrShare(false);
                    let slLimit = swan.ylGetVSLimit();
                    console.log("---------ylRewardByVideoOrShare-result:", result, slLimit);
                }
                console.log("---------分享结果-sResult:", sResult);
            }, shareInfo);
        }
        onJumpOut(e) {
            if (this.sideList && this.sideList.length > 0) {
                let item = this.sideList[0];
                swan.ylNavigateToMiniProgram({
                    _id: item._id,
                    toAppid: item.toAppid,
                    toUrl: item.toUrl,
                    type: item.type,
                    showImage: item.showImage,
                    source: 'item_side_box',
                }, function (success) {
                }.bind(this));
            }
            else {
                console.log("GameUI onJumpOut-没有卖量 ");
            }
        }
        onExit(e) {
            swan.ylExit();
        }
        testStoreValue() {
            this.test_sv_string();
            this.test_sv_list();
            this.test_sv_set();
            this.test_sv_hash();
            this.test_sv_radom();
        }
        test_sv_string() {
            swan.ylStoreValue({
                name: "testString",
                cmd: "set",
                args: "测试数据"
            }, function (status) {
                swan.ylStoreValue({
                    name: "testString",
                    cmd: "get"
                }, function (status) {
                }.bind(this));
            }.bind(this));
        }
        test_sv_list() {
            swan.ylStoreValue({
                name: "testList",
                cmd: "add",
                args: "0"
            }, function (status) {
            }.bind(this));
            swan.ylStoreValue({
                name: "testList",
                cmd: "add",
                args: "2"
            }, function (status) {
            }.bind(this));
            swan.ylStoreValue({
                name: "testList",
                cmd: "set",
                args: "0,3"
            }, function (status) {
                swan.ylStoreValue({
                    name: "testList",
                    cmd: "all"
                }, function (status) {
                }.bind(this));
                swan.ylStoreValue({
                    name: "testList",
                    cmd: "get",
                    args: "0"
                }, function (status) {
                }.bind(this));
                swan.ylStoreValue({
                    name: "testList",
                    cmd: "size"
                }, function (status) {
                }.bind(this));
                swan.ylStoreValue({
                    name: "testList",
                    cmd: "poll",
                    args: "2"
                }, function (status) {
                    swan.ylStoreValue({
                        name: "testList",
                        cmd: "size"
                    }, function (status) {
                    }.bind(this));
                    swan.ylStoreValue({
                        name: "testList",
                        cmd: "replace",
                        args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                    }, function (status) {
                        swan.ylStoreValue({
                            name: "testList",
                            cmd: "all"
                        }, function (status) {
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_set() {
            swan.ylStoreValue({
                name: "testSet",
                cmd: "add",
                args: "12"
            }, function (status) {
            }.bind(this));
            swan.ylStoreValue({
                name: "testSet",
                cmd: "add",
                args: "10"
            }, function (status) {
                swan.ylStoreValue({
                    name: "testSet",
                    cmd: "exist",
                    args: "10"
                }, function (status) {
                }.bind(this));
                swan.ylStoreValue({
                    name: "testSet",
                    cmd: "size"
                }, function (status) {
                    swan.ylStoreValue({
                        name: "testSet",
                        cmd: "del",
                        args: "10"
                    }, function (status) {
                        swan.ylStoreValue({
                            name: "testSet",
                            cmd: "all"
                        }, function (status) {
                            swan.ylStoreValue({
                                name: "testSet",
                                cmd: "replace",
                                args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                            }, function (status) {
                                swan.ylStoreValue({
                                    name: "testSet",
                                    cmd: "all"
                                }, function (status) {
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_hash() {
            swan.ylStoreValue({
                name: "testHash",
                cmd: "set",
                args: "u_name,许"
            }, function (status) {
                swan.ylStoreValue({
                    name: "testHash",
                    cmd: "get",
                    args: "u_name"
                }, function (status) {
                    swan.ylStoreValue({
                        name: "testHash",
                        cmd: "replace",
                        args: "{\"u_name\":\"唐\",\"sex\":\"women\"}"
                    }, function (status) {
                        swan.ylStoreValue({
                            name: "testHash",
                            cmd: "gets",
                            args: "u_name,sex"
                        }, function (status) {
                        }.bind(this));
                        swan.ylStoreValue({
                            name: "testHash",
                            cmd: "size",
                        }, function (status) {
                        }.bind(this));
                        swan.ylStoreValue({
                            name: "testHash",
                            cmd: "values",
                            args: "sex"
                        }, function (status) {
                        }.bind(this));
                        swan.ylStoreValue({
                            name: "testHash",
                            cmd: "del",
                            args: "u_name"
                        }, function (status) {
                            swan.ylStoreValue({
                                name: "testHash",
                                cmd: "all",
                            }, function (status) {
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_radom() {
            swan.ylStoreValue({
                name: "testRandom"
            }, function (status) {
            }.bind(this));
        }
    }

    class GameConfig {
        constructor() {
        }
        static init() {
            var reg = Laya.ClassUtils.regClass;
            reg("script/GameUI.ts", GameUI);
        }
    }
    GameConfig.width = 640;
    GameConfig.height = 1136;
    GameConfig.scaleMode = "fixedwidth";
    GameConfig.screenMode = "none";
    GameConfig.alignV = "top";
    GameConfig.alignH = "left";
    GameConfig.startScene = "test/TestScene.scene";
    GameConfig.sceneRoot = "";
    GameConfig.debug = false;
    GameConfig.stat = false;
    GameConfig.physicsDebug = false;
    GameConfig.exportSceneToJson = true;
    GameConfig.init();

    class Main {
        constructor() {
            if (window["Laya3D"])
                Laya3D.init(GameConfig.width, GameConfig.height);
            else
                Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
            Laya["Physics"] && Laya["Physics"].enable();
            Laya["DebugPanel"] && Laya["DebugPanel"].enable();
            Laya.stage.scaleMode = GameConfig.scaleMode;
            Laya.stage.screenMode = GameConfig.screenMode;
            Laya.stage.alignV = GameConfig.alignV;
            Laya.stage.alignH = GameConfig.alignH;
            Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
            if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
                Laya.enableDebugPanel();
            if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
                Laya["PhysicsDebugDraw"].enable();
            if (GameConfig.stat)
                Laya.Stat.show();
            Laya.alertGlobalError = true;
            Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
        }
        onVersionLoaded() {
            Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
        }
        onConfigLoaded() {
            GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
        }
    }
    new Main();

}());
