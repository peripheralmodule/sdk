import { ui } from "./../ui/layaMaxUI";
/**
 * 本示例采用非脚本的方式实现，而使用继承页面基类，实现页面逻辑。在IDE里面设置场景的Runtime属性即可和场景进行关联
 * 相比脚本方式，继承式页面类，可以直接使用页面定义的属性（通过IDE内var属性定义），比如this.tipLbll，this.scoreLbl，具有代码提示效果
 * 建议：如果是页面级的逻辑，需要频繁访问页面内多个元素，使用继承式写法，如果是独立小模块，功能单一，建议用脚本方式实现，比如子弹脚本。
 */
export default class GameUI extends ui.test.TestSceneUI {
    /**设置单例的引用方式，方便其他类引用 */
    static instance: GameUI;
    private shareCount:number = 0;
    private layerList:Array<any> = null; //流失列表
    private sideList:Array<any> = null; //侧边栏列表
    private layerId:number = 0; //流失列表Index-测试用
    private custom:number = 1;
    constructor() {
        super();
        GameUI.instance = this;
        //关闭多点触控，否则就无敌了
        Laya.MouseManager.multiTouchEnabled = false;
    }

    onEnable(): void {
        swan.ylGameLifeListen(function(type,res){
            if(type === 'onShow'){
                console.warn("YLSDK GameUI-onShow--res:",res);
            }else if(type === 'onHide'){
                console.warn("YLSDK GameUI-onHide");
            }else if(type === 'onError'){
                console.warn("YLSDK GameUI-onError--res:",res);
            }
        });
        swan.ylInitSDK(function(success:any){
            console.log("-----初始化SDK-----:",success);
            if(success === true){
                console.warn("GameUI-ylInitSDK-初始化成功");
                this.doSDKinterface();
            }else if(success == 'config_success' || success == 'config_fail'){
                console.warn("GameUI-ylInitSDK-获取游戏配置回调");
                swan.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("GameUI-ylGetPowerInfo:",data);
                });
            }else{
                console.warn("GameUI-ylInitSDK-初始化失败");
            }
        }.bind(this));
        this.initEvent();
    }
    initEvent():void{
        this.btnVideoOrShare.on(Laya.Event.CLICK,this,this.onShowShareOrVideo);
        this.btnShowVideoAd.on(Laya.Event.CLICK,this,this.onShowVideoAd);
        this.btnShowBannerAd.on(Laya.Event.CLICK,this,this.onShowBannerAd);
        this.btnHideBannerAd.on(Laya.Event.CLICK,this,this.onHideBannerAd);
        this.btnShare.on(Laya.Event.CLICK,this,this.onShare);
        this.btnJumpOut.on(Laya.Event.CLICK,this,this.onJumpOut);
        this.btnExit.on(Laya.Event.CLICK,this,this.onExit);
    }

    doSDKinterface():void{
        console.log("---调用接口---");
        swan.ylSideBox(function (data:any) {
            this.sideList = data;
            console.log("GameUI ylSideBox:",this.sideList);
        }.bind(this));
        swan.ylGetCustom(function(data:any){
        }.bind(this));
        swan.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status:any){
            if(status){
                //干点什么
            }
        }.bind(this));

        let loginInfo = swan.ylGetUserInfo();//获取登录信息
        console.log("---登录信息-loginInfo:",JSON.stringify(loginInfo));

        var that = this;
        /**
         * 获取分享图列表
         */
        swan.ylShareCard(function (shareInfo:any) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
            }else{
                //获取失败
            }
        }.bind(this),'MainScene');
        swan.ylStatisticViedo(0,'adunit-6878a73e134f85e2',null);
        swan.ylStatisticShareCard(177);
        swan.ylGetLayerList(function(data){
            if(data && data.length >0){
                this.layerList = data;
                swan.ylStatisticLayer(this.layerList[this.layerList.length-1].layerPath);
            }
        }.bind(this));
        // this.testStoreValue();
        swan.ylGetServerInfo(function(data){
            console.log("GameUI -ylGetServerInfo-back:",JSON.stringify(data));
        });
    }
    //视频分享策略
    onShowShareOrVideo(e:Laya.Event):void{
        swan.ylShowShareOrVideo("bb","aa",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    this.shareCount += 1;
                    let _channel = (this.shareCount %2 ==0) ? 'bb' : 'cc';
                    let _module = (this.shareCount %2 ==0) ? 'aa' : 'cc';
                    let shareInfo = {
                        channel:_channel,
                        module:_module,
                        scene:'MenuScene',
                        inviteType:'award_id',
                    };
                    let onShare = {
                        channel:_channel,
                        module:_module,
                        showTime: new Date().getTime()-3500,
                    };
                    swan.ylGetSharingResults(onShare,function(result){
                        console.warn("-----ylGetSharingResults-result：",result);
                        if(result.sSuccess){
                            //分享成功
                        }else{
                            //分享失败
                            if(result.hasStrategy){
                                console.warn("-----分享失败话术：",result.trickJson);
                            }
                        }
                    });
                    break;
                case 2:
                    console.warn("------策略-视频");
                    let result = swan.ylRewardByVideoOrShare(false);
                    let slLimit = swan.ylGetVSLimit();
                    console.warn("---------ylRewardByVideoOrShare-result:",result,slLimit);
                    break;
            }
        }.bind(this));
    }
    //显示激励视频
    onShowVideoAd(e:Laya.Event):void{
        swan.ylBannerAdHide();
        swan.ylShowVideoAd({
            callBack:function(status){
                console.log("----showVideo:"+status);
                switch (status){
                    case 1:
                        //视频广告-播放完成
                        break;
                    case 2:
                        //视频广告-播放取消
                        break;
                    case 0:
                        //视频广告-播放失败
                        break;
                }
            },
            unlockCustomNum:this.custom,
            getPower:true
        });
    }
    //显示Banner广告
    onShowBannerAd(e:Laya.Event):void{
        console.log("GameUI-onShowBanner:");
        // swan.ylBannerAdCreate(true,function(){            
        // });
        swan.ylBannerAdCreateByStyle({
            left: 20,
            top: 100,
            width: 310,
        }, true, (res)=>{
            
        }, true,(res)=>{
            console.warn("GameUI-ylBannerAdCreateByStyle-onResize-back:",JSON.stringify(res));
        });
        swan.ylChangeBannerStyle({
            left: 0,
            top:350,
            height: 30,
        });
    }
    //隐藏Banner广告
    onHideBannerAd(e:Laya.Event):void{
        console.log("GameUI-onHideBanner");
        swan.ylBannerAdHide();
    }
    //分享
    onShare(e:Laya.Event):void{
        this.shareCount += 1;
        let _channel = (this.shareCount %2 ==0) ? 'bb' : 'cc';
        let _module = (this.shareCount %2 ==0) ? 'aa' : 'cc';
        let shareInfo = {
            channel:_channel,
            module:_module,
            scene:'MainScene',
            inviteType:'award_id',
        };
        swan.ylShareAppMessage(function(sResult){
            if(sResult.sSuccess){
                let result = swan.ylRewardByVideoOrShare(false);
                let slLimit = swan.ylGetVSLimit();
                console.log("---------ylRewardByVideoOrShare-result:",result,slLimit);
            }
            console.log("---------分享结果-sResult:",sResult);
        },shareInfo);
    }
    //卖量跳转
    onJumpOut(e:Laya.Event):void{
        if(this.sideList && this.sideList.length >0){
            let item = this.sideList[0];
            swan.ylNavigateToMiniProgram(
                {
                    _id: item._id,
                    toAppid:item.toAppid,   
                    toUrl:item.toUrl,
                    type:item.type,
                    showImage:item.showImage,
                    source:'item_side_box',//从哪个模块导出的，该字段具体值由调用方自行定义
                },
                function(success){
            }.bind(this));
        }else{
            console.log("GameUI onJumpOut-没有卖量 ");
        }
    }
    //退出
    onExit(e:Laya.Event):void{
        swan.ylExit();
    }
    //测试自定义空间值
    testStoreValue():void{
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    }
    test_sv_string():void{
        //String
        swan.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                swan.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    }
    test_sv_list():void{
        //List
        swan.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        swan.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        swan.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                swan.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 swan.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        swan.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        swan.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    swan.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    swan.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            swan.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    }
    test_sv_set():void{
        //Set
        swan.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        swan.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                swan.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                swan.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        swan.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                swan.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        swan.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            swan.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    }
    test_sv_hash():void{
        //litMap
        swan.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                swan.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        swan.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            swan.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            swan.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            swan.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            swan.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                swan.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    }
    test_sv_radom():void{
        //testRandom
        swan.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    }
}