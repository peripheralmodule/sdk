SDK文件说明:
	1.yl.swan.d.ts  接口提示文件。
	2.utils/yl_sdk_conf.js  SDK配置文件。
	3.utils/yl_sdk.js 	SDK服务端公共接口文件(CP无需了解内容)。
	4.utils/yl_sdk_baidu.js  SDK平台接口文件(CP无需了解内容)。

SDK配置：
	1.将utils目录复制到工程目录根目录下
	2.再工程的game.js文件中引入SDK：require('./utils/yl_sdk_baidu.js');
	3.在yl_sdk_conf.js中配置游戏相关信息
	4.配置以下域名为合法域名:
		1>服务器地址：		   https://api.ylxyx.cn
		2>图片服务器地址1：	   https://ql.ylxyx.cn
		3>图片服务器地址2：     https://tx.ylxyx.cn
		4>图片服务器地址3：	   https://ext.ylxyx.cn
	5.TS语言的工程需要复制yl.swan.d.ts到工程的相应的目录中。