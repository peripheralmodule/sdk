import { ui } from "./../ui/layaMaxUI";
/**
 * 本示例采用非脚本的方式实现，而使用继承页面基类，实现页面逻辑。在IDE里面设置场景的Runtime属性即可和场景进行关联
 * 相比脚本方式，继承式页面类，可以直接使用页面定义的属性（通过IDE内var属性定义），比如this.tipLbll，this.scoreLbl，具有代码提示效果
 * 建议：如果是页面级的逻辑，需要频繁访问页面内多个元素，使用继承式写法，如果是独立小模块，功能单一，建议用脚本方式实现，比如子弹脚本。
 */
export default class GameUI extends ui.test.TestSceneUI {
    /**设置单例的引用方式，方便其他类引用 */
    static instance: GameUI;
    private layerList:Array<any> = null;
    private layerId:number = 0; //流失列表Index-测试用
// uclink://minigame?action=launch&module=uc_minigame&appid=eee5aeb07b59443bada5effefe50a980&clientid=eee5aeb07b59443bada5effefe50a980&game_name=游戏名&game_url=小游戏页面服务在线地址&game_icon=http://xxxxx
    constructor() {
        super();
        GameUI.instance = this;
        //关闭多点触控，否则就无敌了
        Laya.MouseManager.multiTouchEnabled = false;
    }

    onEnable(): void {
        uc.ylInitSDK(function(success:Boolean){
            console.log("-----初始化SDK-----:",success);
            this.doSDKinterface();
        }.bind(this),2);
        this.initUI();
    }
    initUI():void{
        this.btnShareOrVideo.on(Laya.Event.CLICK,this,this.onShowShareOrVideo);
        this.btnShowBanner.on(Laya.Event.CLICK,this,this.onShowBanner);
        this.btnExit.on(Laya.Event.CLICK,this,this.onExit);
        this.btnShowVideo.on(Laya.Event.CLICK,this,this.onShowVideo);
        this.btnGetUserInfo.on(Laya.Event.CLICK,this,this.onGetUserInfo);
    }
    doSDKinterface():void{
        console.log("---调用接口---");
        uc.ylSideBox(function (data:any) {
        }.bind(this));
        uc.ylGetCustom(function(data:any){
        }.bind(this));
        uc.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status:any){
            if(status){
                //干点什么
            }
        }.bind(this));

        let loginInfo = uc.ylGetUserInfo();//获取登录信息，包括微信oppid和code(code只有第一次登录才能取到)
        console.log("---登录信息-loginInfo:",JSON.stringify(loginInfo));

        var that = this;
        /**
         * 获取分享图列表
         */
        uc.ylShareCard(function (shareInfo:any) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
            }else{
                //获取失败
            }
        }.bind(this),'MainScene');
        uc.ylStatisticViedo(0,'adunit-6878a73e134f85e2',null);
        uc.ylStatisticShareCard(177);
        uc.ylGetLayerList(function(data){
            if(data && data.length >0){
                this.layerList = data;
                uc.ylStatisticLayer(this.layerList[this.layerList.length-1].layerPath);
            }
        }.bind(this));
        // this.testStoreValue();
    }
    //视频分享策略
    onShowShareOrVideo(e:Laya.Event):void{
        uc.ylShowShareOrVideo("chanel_2","model_2",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    let onShare = {
                            channel:'bb',
                            module:'aa',
                            showTime: new Date().getTime()-3500,
                        };
                        uc.ylGetSharingResults(onShare,function(result){
                            console.warn("-----ylGetSharingResults-result：",result);
                            if(result.sSuccess){
                                //分享成功
                            }else{
                                //分享失败
                                if(result.hasStrategy){
                                        console.warn("-----分享失败话术：",result.trickJson);
                                    }
                                }
                        });
                    break;
                    case 2:
                        console.warn("------策略-视频");
                        let result = uc.ylRewardByVideoOrShare(false);
                        let slLimit = uc.ylGetVSLimit();
                        console.log("---------ylRewardByVideoOrShare-result:",result,slLimit);
                    break;
            }
        }.bind(this));
    }
    onShowBanner(e:Laya.Event):void{
        console.log("GameUI-onShowBanner:");
        // uc.ylBannerAdCreate(true,function(){            
        // },true);
        uc.ylBannerAdCreateByStyle({
            gravity:0,
            left: 20,
            top: 100,
            width: 310,
            height: 200,
        }, true, (res)=>{
            console.warn("GameUI-ylBannerAdCreateByStyle-back:",JSON.stringify(res));
        }, true,(res)=>{
            console.warn("GameUI-ylBannerAdCreateByStyle-onResize-back:",JSON.stringify(res));
        });
        // uc.ylChangeBannerStyle({
        //     left: 0,
        //     top:350,
        //     height: 30,
        // });
    }
    onHideBanner(e:Laya.Event):void{
        console.log("GameUI-onHideBanner");
        uc.ylBannerAdHide();
    }
    onExit(e:Laya.Event):void{
        uc.ylExit();
    }
    onShowVideo(e:Laya.Event):void{
        console.log("GameUI-onShowVideo");
        uc.ylShowVideoAd({
            callBack:function(res){
                let tvNum = uc.ylGetWatchTvNum();
                console.log("GameUI-onShowVideo-callBack-res:",JSON.stringify(res),tvNum);
            },
            unlockCustomNum:true,
            getPower:true
        });
    }
    onGetUserInfo(e:Laya.Event):void{
        uc.ylGetUserUCInfo(function(data){
            console.log("GameUI-ylGetUserUCInfo-data:",JSON.stringify(data))
        });
    }
    //测试自定义空间值
    testStoreValue():void{
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    }
    test_sv_string():void{
        //String
        uc.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                uc.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    }
    test_sv_list():void{
        //List
        uc.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        uc.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        uc.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                uc.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 uc.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        uc.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        uc.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    uc.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    uc.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            uc.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    }
    test_sv_set():void{
        //Set
        uc.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        uc.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                uc.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                uc.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        uc.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                uc.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        uc.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            uc.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    }
    test_sv_hash():void{
        //litMap
        uc.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                uc.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        uc.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            uc.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            uc.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            uc.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            uc.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                uc.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    }
    test_sv_radom():void{
        //testRandom
        uc.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    }
}