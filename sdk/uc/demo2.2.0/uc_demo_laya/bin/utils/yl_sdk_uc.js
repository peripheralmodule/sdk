"use strict";
var _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (e) {
    return typeof e
} : function (e) {
    return e && "function" == typeof Symbol && e.constructor === Symbol && e !== Symbol.prototype ? "symbol" : typeof e
};
! function () {
    for (var e, n, t = window/***require("./yl_sdk_conf")***/, g = window.yl_sdk/***require("./yl_sdk")***/, o = ["ylInitSDK", "ylGetInviteAccount", "ylGetUserInfo", "ylGetSwitchInfo", "ylLog", "ylStatisticViedo", "ylStatisticResult", "ylStoreValue", "ylGetAppID", "ylEventCount", "ylGetCustom", "ylShowShareOrVideo", "ylChangeView", "ylGetDeepTouch", "ylOverGame", "ylVideoUnlock", "ylGetPowerInfo", "ylOnPowerChange", "ylSetPower", "ylGetPower", "ylSideBox", "ylIntegralWall", "ylGetBoardAward", "ylGetSignData", "ylFetchSign", "ylGetPlayCount", "ylGetPlayCustom", "ylGetWatchTvNum", "ylGetUserUCInfo", "ylChangeBannerStyle", "ylBannerAdCreateByStyle", "ylBannerAdCreate", "ylBannerAdShow", "ylBannerAdHide", "ylCreateVideoAd", "ylShowVideoAd", "ylStatisticShareCard", "ylShareCard", "ylRewardByVideoOrShare", "ylGetVSLimit", "ylGetSharingResults", "ylGetLayerList", "ylStatisticLayer", "ylExit", "ylGetServerInfo"], f = {
            bannerAd: null,
            videoAd: null,
            gridAd: null,
            bannerTimer: null,
            _timer: null,
            unlockCustomNum: !1,
            userInfoBtn: null,
            onVideoClose: null,
            show_video: !1,
            switchLog: t.ylsdk_debug_model,
            show_share: !1,
            defaultbTS: 10,
            shareCallBack: null,
            onShare: null,
            hideTime: null,
            VIDEO_PLAY_FAIL: 0,
            VIDEO_PLAY_FINISH: 1,
            VIDEO_PLAY_CANCEL: 2,
            loginType: 0,
            isGuest: !1
        }, i = {
            LD_KEY_AP: "SDK_ACCOUNT_PASS_" + t.ylsdk_app_id,
            LD_KEY_TDPNI: "SDK_TODAY_PLAY_NUMS_ID_" + t.ylsdk_app_id,
            LD_KEY_TTPNI: "SDK_TOTAL_PLAY_NUMS_ID_" + t.ylsdk_app_id,
            LD_KEY_TDWTNI: "SDK_TODAY_WATCH_TV_NUMS_ID_" + t.ylsdk_app_id,
            LD_KEY_LWTI: "SDK_LAST_WATCH_TIME_ID_" + t.ylsdk_app_id,
            LD_KEY_LPTI: "SDK_LAST_PLAY_TIME_ID_" + t.ylsdk_app_id,
            LD_KEY_MCI: "SDK_MAX_CUSTOM_ID_" + t.ylsdk_app_id,
            LD_KEY_LCI: "SDK_LAST_CUSTOM_ID_" + t.ylsdk_app_id,
            LD_KEY_PNI: "SDK_POWER_NUM_ID_" + t.ylsdk_app_id,
            LD_KEY_WTUCI: "SDK_WATCH_TV_UNLOCK_CUSTOMS_ID_" + t.ylsdk_app_id,
            LD_KEY_JI: "SDK_JUMP_ICON_" + t.ylsdk_app_id,
            LD_KEY_AI: "SDK_ACCOUNT_ID_" + t.ylsdk_app_id,
            LD_KEY_STST: "SDK_STATISTICS_" + t.ylsdk_app_id,
            LD_KEY_UPI: "SDK_USER_PLATFORM_INFO_" + t.ylsdk_app_id,
            LD_KEY_VSFL: "VIDEO_SHARE_FALSE_LIMIT" + t.ylsdk_app_id,
            LD_KEY_SCL: "SDK_COUNTED_LIST" + t.ylsdk_app_id
        }, d = 0, u = 1, _ = 2, y = 4, a = 7, r = 8, S = {
            ylInitSDK: function (e, n) {
                f.loginType = n, g.login(e)
            },
            ylGetInviteAccount: function () {
                return g.getInviteAccount()
            },
            ylGetUserInfo: function () {
                return g.getUserInfo()
            },
            ylGetSwitchInfo: function () {
                return g.getSwitchInfo()
            },
            ylLog: function (e, n) {
                console.warn("-----ylLog:", e, n), g.logs(e, n)
            },
            ylStatisticViedo: function (e, n, o) {
                g.statisticViedo(e, n, o)
            },
            ylStatisticResult: function (e, n) {
                g.statisticResult(e, n)
            },
            ylStoreValue: function (e, n) {
                g.storeValue(e, n)
            },
            ylGetAppID: function () {
                return t.ylsdk_app_id
            },
            ylEventCount: function (e, n) {
                g.eventCount(e, n)
            },
            ylGetCustom: function (e) {
                g.getCustom(e)
            },
            ylShowShareOrVideo: function (e, n, o) {
                g.showShareOrVideo(e, n, o)
            },
            ylChangeView: function (e) {
                if (f.bannerAd) {
                    var n = g.getGameConfig();
                    if (n && n.bannerTimeSpace && n.bannerTimeSpace.forceOnOff) {
                        var o = n.bannerTimeSpace,
                            t = (new Date).getTime(),
                            i = f.bannerAd.lastTime || t,
                            a = o.forceTimeSpace || f.defaultbTS;
                        if (console.warn("YLSDK ylChangeView-forceTimeSpace:", t - i, " >= ", 1e3 * a), 1e3 * a <= t - i) {
                            var r = !0;
                            if (f.bannerAd.show_banner) {
                                r = f.bannerAd.isSmall || !1;
                                var l = f.bannerAd._onResize || null;
                                h.createBannerAd(r, !0, null, f.bannerAd._style, e, l)
                            } else f.bannerAd.lastTime = t - (1e3 * o.timeSpace + 1e3)
                        }
                    }
                } else f.switchLog && console.warn("YLSDK ylChangeView-pf_data.bannerAd is ", f.bannerAd)
            },
            ylGetDeepTouch: function (e) {
                return g.getDeepTouch(e)
            },
            ylOverGame: function (e) {
                g.overGame(e)
            },
            ylVideoUnlock: function (e) {
                return g.videoUnlock(e)
            },
            ylGetPowerInfo: function (e) {
                g.getPowerInfo(e)
            },
            ylOnPowerChange: function (e) {
                g.onPowerChange(e)
            },
            ylSetPower: function (e) {
                g.setPower(e)
            },
            ylGetPower: function () {
                return g.getPower()
            },
            ylSideBox: function (e) {
                g.getSideBox(e)
            },
            ylIntegralWall: function (e) {
                g.getIntegralWall(e)
            },
            ylGetBoardAward: function (e, n) {
                g.getBoardAward(e, n)
            },
            ylGetSignData: function (e) {
                g.getSignData(e)
            },
            ylFetchSign: function (e, n) {
                g.fetchSign(e, n)
            },
            ylGetPlayCount: function (e) {
                return 1 == e ? h.getLD(i.LD_KEY_TDPNI) : h.getLD(i.LD_KEY_TTPNI)
            },
            ylGetPlayCustom: function (e) {
                return 1 == e ? h.getLD(i.LD_KEY_LCI) : h.getLD(i.LD_KEY_MCI)
            },
            ylGetWatchTvNum: function () {
                return h.getLD(i.LD_KEY_TDWTNI)
            },
            ylShareCard: function (e, n) {
                g.shareCard(e, n)
            },
            ylBannerAdCreate: function (e, n, o) {
                h.createBannerAd(!1, e, n, null, o, null)
            },
            ylBannerAdCreateByStyle: function (e, n, o, t, i) {
                h.createBannerAd(!1, n, o, e, t, i)
            },
            ylChangeBannerStyle: function (e) {
                h.changeBannerStyle(e)
            },
            ylBannerAdShow: function () {
                h.showBannerAd()
            },
            ylBannerAdHide: function () {
                h.hideBannerAd()
            },
            ylGetUserUCInfo: function (e) {
                h.getUserPlatFormInfo(e)
            },
            ylCreateVideoAd: function () {
                h.createVideoAd()
            },
            ylShowVideoAd: function (e) {
                e ? h.showVideoAd(e.callBack, e.unlockCustomNum, e.getPower) : console.warn("YLSDK ylShowVideoAd2 缺少参数")
            },
            ylStatisticShareCard: function (e) {
                g.statisticShareCard(e)
            },
            ylShareAppMessage: function (e, n) {
                h.shareAppMessage_2(e, n)
            },
            ylNavigateToMiniProgram: function (e, n) {
                h.navigateToMiniProgram(e, n)
            },
            ylGetSharingResults: function (e, n) {
                var o = "YLSDK ylGetSharingResults not parameter ";
                e ? g.getSharingResults(e, n) : console.warn(o + "shareInfo")
            },
            ylRewardByVideoOrShare: function (e) {
                return g.rewardByVideoOrShare(e)
            },
            ylGetVSLimit: function () {
                return g.getVSLimit()
            },
            ylGetLayerList: function (e) {
                g.getLayerList(e)
            },
            ylStatisticLayer: function (e) {
                g.statisticLayer(e)
            },
            ylExit: function () {
                g.saveStatisticsToLocal(), g.statisticsPlayTime(), g.savePowerLocal(), uc.exit()
            },
            ylGetServerInfo: function (e) {
                g.getServerInfo(e)
            }
        }, l = 0; l < o.length; l++) n = S[e = o[l]], Object.defineProperty(uc, e, {
        value: n,
        writable: !1,
        enumerable: !0,
        configurable: !0
    });
    var h = {
            getLD: function (n) {
                try {
                    var e = null;
                    return h.hasFun(localStorage.getItem, "localStorage.getItem") && (e = localStorage.getItem(n)), null != e && void 0 !== e && "undefined" !== e && "" !== e || (e = ""), e
                } catch (e) {
                    console.error("YLSDK getLocalData error key:" + n + ", e:", e)
                }
                return ""
            },
            saveLD: function (n, e) {
                try {
                    h.hasFun(localStorage.setItem, "localStorage.setItem") && localStorage.setItem(n, e)
                } catch (e) {
                    console.error("YLSDK saveLocalData error key:" + n + ", e:", e)
                }
            },
            loginPF: function (t, i) {
                if (0 == f.loginType) h.loginByGuest(t, i);
                else {
                    var e = {
                        success: function (e) {
                            var n = e.code,
                                o = {
                                    code: n,
                                    needPass: !1,
                                    pkgName: A.pkg_name
                                };
                            t.loginInfo ? (t.loginInfo.code = n || "", t.loginInfo.token = n || "", t.loginInfo.pkgName = o.pkgName || "") : t.loginInfo = {
                                code: n || "",
                                token: n || "",
                                pkgName: o.pkgName || ""
                            }, g.setLoginInfo(t.loginInfo), f.switchLog && console.log("YLSDK loginInfo:", JSON.stringify(t.loginInfo)), f.switchLog && console.log("YLSDK platform.login-登录成功:", e), g.loginSDKServer(o, i)
                        },
                        fail: function (e) {
                            f.switchLog && console.error("YLSDK platform.login-登录失败", e), 2 == f.loginType ? h.loginForceTip(t, i) : h.loginByGuest(t, i)
                        }
                    };
                    console.warn("----login_info:", e), h.hasFun(uc.login, "uc.login") && uc.login(e)
                }
            },
            loginByGuest: function (t, i) {
                uc.getGuestInfo({
                    success: function (e) {
                        f.switchLog && console.log("YLSDK getGuestInfo-res:", JSON.stringify(e));
                        var n = e.guestid,
                            o = {
                                code: n,
                                needPass: !1,
                                pkgName: "anonymousCode"
                            };
                        t.loginInfo ? (t.loginInfo.code = n || "", t.loginInfo.token = n || "", t.loginInfo.pkgName = "anonymousCode") : t.loginInfo = {
                            code: n || "",
                            token: n || "",
                            pkgName: "anonymousCode"
                        }, g.setLoginInfo(t.loginInfo), f.isGuest = !0, f.switchLog && console.log("YLSDK getGuestInfo-loginInfo:", JSON.stringify(t.loginInfo)), f.switchLog && console.log("YLSDK getGuestInfo-登录成功:", JSON.stringify(o)), g.loginSDKServer(o, i)
                    },
                    fail: function () {
                        i && i(!1), f.switchLog && console.error("YLSDK platform.getGuestInfo-登录失败", res)
                    }
                })
            },
            loginForceTip: function (n, o) {
                uc.showModal({
                    title: "登录失败",
                    content: "是否重新登录？",
                    cancelText: "退出游戏",
                    success: function (e) {
                        e.confirm ? h.loginPF(n, o) : e.cancel && uc.exit()
                    }
                })
            },
            hasFun: function (e, n) {
                var o = !0;
                return e ? o = !0 : (o = !1, console.warn("YLSDK function " + n + " is ", e)), o
            },
            getLOS: function () {
                var e = null;
                return h.hasFun(uc.getLaunchOptionsSync, "uc.getLaunchOptionsSync") && (e = uc.getLaunchOptionsSync()), e
            },
            removeBannerAd: function () {
                f.bannerAd && (f.bannerTimer && (clearTimeout(f.bannerTimer), f.bannerTimer = null), f.bannerAd.hide(), f.bannerAd.destroy(), f.bannerAd = null)
            },
            createBannerAd: function (o, t, i, a, r, l) {
                if (console.log("YLSDK createBannerAd-isSmall, show, _callback, _style,misToch:", o, t, i, JSON.stringify(a), r), f.bannerAd && h.removeBannerAd(), h.hasFun(uc.createBannerAd, "uc.createBannerAd")) {
                    var s = g.getGameConfig(),
                        e = o ? 300 : A.windowWidth,
                        n = {
                            style: {
                                left: (A.windowWidth - e) / 2,
                                top: A.windowHeight - .28695 * A.windowWidth,
                                width: e
                            },
                            adIntervals: 31
                        };
                    a && (n.style = a), f.bannerAd = uc.createBannerAd(n), f.bannerAd._onResize = l, f.bannerAd.ad_id = "00000000", g.statisticsBanner({
                        type: d,
                        adId: f.bannerAd.ad_id
                    });
                    var c = s && s.bannerTimeSpace && s.bannerTimeSpace.onOff;
                    c && (f.bannerAd.isSmall = o), f.bannerAd._style = a, f.bannerAd.onLoad(function () {
                        f.switchLog && console.log("YLSDK uc.createBannerAd---onLoad 广告加载成功"), g.statisticsBanner({
                            type: u,
                            adId: f.bannerAd.ad_id
                        }), i && i(!0)
                    }), f.bannerAd.onError(function (e) {
                        if (f.switchLog && console.warn("YLSDK uc.createBannerAd---onError:", e), g.statisticsBanner({
                                type: _,
                                adId: f.bannerAd.ad_id
                            }), f.bannerAd = null, c) {
                            var n = s.bannerTimeSpace.timeSpace || 10;
                            f.switchLog && console.log("YLSDK 将在" + n + "秒后重新创建banner(调用ylBannerAdHide会取消重试)"), h.delayUpdateBanner(n, o, t, a, r, l), i && i(!1)
                        }
                    }), t && h.showBannerAd(), f.bannerAd.misToch = r || !1
                } else i && i(!1)
            },
            changeBannerStyle: function (e) {
                f.bannerAd && e && (e.gravity && (f.bannerAd.style.gravity = e.gravity), e.left && (f.bannerAd.style.left = e.left), e.top && (f.bannerAd.style.top = e.top), e.bottom && (f.bannerAd.style.bottom = e.bottom), e.right && (f.bannerAd.style.right = e.right), e.width && (f.bannerAd.style.width = e.width), e.height && (f.bannerAd.style.height = e.height), f.bannerAd._style = e)
            },
            delayUpdateBanner: function (e, n, o, t, i, a) {
                f.switchLog && console.log("YLSDK delayUpdateBanner-delay,isSmall,show:", e, n, o), 0 < e && (f.bannerTimer && (clearTimeout(f.bannerTimer), f.bannerTimer = null), f.bannerTimer = setTimeout(function () {
                    h.createBannerAd(n, o, null, t, i, a)
                }, 1e3 * e))
            },
            showBannerAd: function () {
                f.switchLog && console.log("YLSDK BannerAd.show");
                var e = !0,
                    n = !1,
                    o = !1,
                    t = !1,
                    i = null,
                    a = null,
                    r = f.defaultbTS,
                    l = g.getGameConfig();
                if (l && l.bannerTimeSpace && (o = l.bannerTimeSpace.onOff || !1, r = l.bannerTimeSpace.timeSpace || f.defaultbTS), f.bannerAd) {
                    o && (e = f.bannerAd.isSmall || !1), t = f.bannerAd.misToch, i = f.bannerAd._style, a = f.bannerAd._onResize;
                    var s = (new Date).getTime(),
                        c = f.bannerAd.lastTime || s;
                    o && 1e3 * r <= s - c && (n = !0, f.switchLog && console.log("YLSDK showBannerAd-createBannerAd:", n), h.createBannerAd(e, !0, null, i, t, a)), n || (f.bannerAd.show(), f.bannerAd.show_banner = !0, g.statisticsBanner({
                        type: y,
                        adId: f.bannerAd.ad_id || "00000000"
                    })), f.bannerAd.lastTime = s, S.ylEventCount("showBannerAd")
                } else f.switchLog && console.warn("YLSDK not BannerAd");
                !n && o && h.delayUpdateBanner(r, e, !0, i, t, a)
            },
            hideBannerAd: function () {
                f.switchLog && console.log("YLSDK BannerAd.hide"), f.bannerAd && (f.bannerAd.hide(), f.bannerTimer && (clearTimeout(f.bannerTimer), f.bannerTimer = null), f.bannerAd.show_banner = !1)
            },
            getUserPlatFormInfo: function (n) {
                var e = g.getUserPlatFormInfo();
                e ? n && n(e) : f.isGuest ? h.getGuestInfo(n) : h.getSetting(function (e) {
                    e && e.userInfo ? h.getUserInfo(n) : h.authorize("userInfo", function (e) {
                        e && e.userInfo && h.getUserInfo(n)
                    })
                })
            },
            getSetting: function (n) {
                uc.getSetting({
                    success: function (e) {
                        console.log("YLSDK uc.getSetting-success:", e), n && n(e)
                    },
                    fail: function (e) {
                        console.warn("YLSDK uc.getSetting-fail:", e), n && n(!1)
                    }
                })
            },
            getUserInfo: function (n) {
                uc.getUserInfo({
                    success: function (e) {
                        console.log("YLSDK uc.getUserInfo-success:", e), g.setUserPlatFormInfo(e), n && n(e)
                    },
                    fail: function (e) {
                        console.warn("YLSDK uc.getUserInfo-fail:", e), n && n(!1)
                    }
                })
            },
            getGuestInfo: function (n) {
                uc.getGuestInfo({
                    success: function (e) {
                        console.log("YLSDK uc.getGuestInfo-success:", e), n && n(e)
                    },
                    fail: function (e) {
                        console.warn("YLSDK uc.getGuestInfo-fail:", e), n && n(!1)
                    }
                })
            },
            authorize: function (e, n) {
                uc.authorize({
                    scope: e,
                    success: function (e) {
                        console.log("YLSDK uc.authorize-success:", e), n && n(e)
                    },
                    fail: function (e) {
                        console.warn("YLSDK uc.authorize-fail:", e), n && n(!1)
                    }
                })
            },
            createVideoAd: function () {
                Math.floor(Math.random() * t.ylsdk_video_ids.length);
                if (f.videoAd) {
                    if (f.videoAd.ad_unit_id === advert) return;
                    f.videoAd.ad_unit_id = advert
                } else h.hasFun(uc.createRewardVideoAd, "uc.createRewardVideoAd") && (f.videoAd = uc.createRewardVideoAd(), f.videoAd.ad_unit_id = "11111111", g.statisticViedo(d, f.videoAd.ad_unit_id), f.getPower = !1, f.videoAd.onLoad(function () {
                    console.log("YLSDK 激励视频 广告加载成功")
                }), f.videoAd.onClose(function (e) {
                    f.show_video = !1, e && e.isEnded ? (f.switchLog && console.log("YLSDK 视频播放完成"), f.onVideoClose && f.onVideoClose(f.VIDEO_PLAY_FINISH), f.onVideoClose = null, g.addWatchTVnums(f.unlockCustomNum), f.getPower && g.addPower(1), g.statisticViedo(r, f.videoAd.ad_unit_id)) : (f.onVideoClose && f.onVideoClose(f.VIDEO_PLAY_CANCEL), f.onVideoClose = null, f.switchLog && console.warn("YLSDK 视频播放取消"), g.statisticViedo(a, f.videoAd.ad_unit_id)), f.unlockCustomNum = null
                }), f.videoAd.onError(function (e) {
                    f.switchLog && console.warn("YLSDK RewardedVideoAd.onError:", e), g.statisticViedo(_, f.videoAd.ad_unit_id), f.onVideoClose && f.onVideoClose(f.VIDEO_PLAY_FAIL), f.onVideoClose = null, f.unlockCustomNum = null, f.show_video = !1
                }))
            },
            showVideoAd: function (e, n, o) {
                if (f.show_video = !0, f.videoAd || h.createVideoAd(), f.videoAd) {
                    f.getPower = o || !1, console.log("YLSDK ---pf_data.getPower:", f.getPower, o), f.unlockCustomNum = n, f.onVideoClose = e;
                    var t = !1;
                    f.videoAd.load().then(function () {
                        t || (t = !0, f._timer && (clearTimeout(f._timer), f._timer = null), g.statisticViedo(u, f.videoAd.ad_unit_id), f.videoAd.show().then(function () {
                            g.statisticViedo(y, f.videoAd.ad_unit_id), f.switchLog && console.log("YLSDK 视频播放成功", f.videoAd.adUnitId)
                        }).catch(function (e) {
                            f.switchLog && console.warn("YLSDK 视频播放失败", f.videoAd.adUnitId), f.onVideoClose && f.onVideoClose(f.VIDEO_PLAY_FAIL), f.onVideoClose = null, f.show_video = !1
                        }))
                    }).catch(function (e) {
                        f.show_video = !1, g.statisticViedo(_, f.videoAd.ad_unit_id), t || (f.switchLog && console.warn("YLSDK 视频加载失败啦！", f.videoAd.adUnitId), t = !0, f._timer && (clearTimeout(f._timer), f._timer = null), f.onVideoClose && f.onVideoClose(f.VIDEO_PLAY_FAIL), f.onVideoClose = null)
                    }), f._timer || (f._timer = setTimeout(function () {
                        f._timer = null, f.show_video = !1, !t && f.onVideoClose && (f.switchLog && console.log("YLSDK 4秒内无视屏回调，自动回调加载失败"), t = !0, f.onVideoClose && f.onVideoClose(f.VIDEO_PLAY_FAIL), f.onVideoClose = null)
                    }, 4e3)), S.ylEventCount("showVideoAd")
                }
            },
            shareAppMessage_2: function (e, n) {
                var o = null,
                    t = g.setShareScene(n),
                    i = g.getShareConfig();
                if (i && i.has(t) ? o = i.get(t) : f.switchLog && console.warn("YLSDK 当前场景没有分享图,请配置"), o && 0 < o.length) {
                    S.ylEventCount("Click-Share");
                    var a = o[Math.floor(Math.random() * o.length)],
                        r = "account_id=" + g.getAccountId() + "&sharecard_id=" + a.id + "&from=stage_invite";
                    h.hasFun(uc.shareAppMessage, "uc.shareAppMessage") && (uc.shareAppMessage({
                        title: a.title,
                        imageUrl: a.img,
                        query: r
                    }), f.show_share = !0, f.shareCallBack = e, f.onShare = {
                        shareId: a.id,
                        showTime: (new Date).getTime()
                    }, S.ylStatisticShareCard(a.id), S.ylEventCount("share"))
                }
            },
            initShareMenu: function () {
                uc.showShareMenu({
                    success: function () {},
                    fail: function () {},
                    complete: function () {}
                }), uc.onShareAppMessage(function () {
                    f.show_share = !0;
                    var e = g.getShareScene(),
                        n = g.getShareConfig(),
                        o = {},
                        t = null;
                    if (!n) return {
                        title: "快来加入我们吧",
                        imageUrl: ""
                    };
                    if (e && "" != e) n.has(e) ? t = n.get(e) : f.switchLog && console.warn("YLSDK 当前场景没有分享图,请配置");
                    else {
                        var i = !0,
                            a = !1,
                            r = void 0;
                        try {
                            for (var l, s = n.entries()[Symbol.iterator](); !(i = (l = s.next()).done); i = !0) {
                                var c = l.value;
                                if (c[1]) {
                                    t = c[1];
                                    break
                                }
                            }
                        } catch (e) {
                            a = !0, r = e
                        } finally {
                            try {
                                !i && s.return && s.return()
                            } finally {
                                if (a) throw r
                            }
                        }
                    }
                    if (t && 0 < t.length) {
                        var d = t[0],
                            u = "account_id=" + g.getAccountId() + "&sharecard_id=" + d.id + "&from=stage_invite";
                        o.title = d.title, o.imageUrl = d.img, o.query = u
                    }
                    return o
                })
            },
            navigateToMiniProgram: function (e, n) {
                if (e._id) {
                    e.type || (e.type = "0");
                    var o = e.source ? e.source : "default";
                    if (h.hasFun(uc.navigateToMiniProgram, "uc.navigateToMiniProgram"))
                        if ("" + e.type == "0") {
                            if (!e.toAppid) return void console.error("YLSDK toAppid is null !");
                            uc.navigateToMiniProgram({
                                appId: e.toAppid,
                                path: e.toUrl || "",
                                extraData: {
                                    appid: t.ylsdk_app_id
                                },
                                success: function () {
                                    f.switchLog && console.log("YLSDK 小游戏跳转-跳转成功！"), g.ClickOut(e._id, e.toAppid, o, !0), g.removeItemFrom(e.toAppid), n && n(!0)
                                },
                                fail: function () {
                                    f.switchLog && console.log("YLSDK 小游戏跳转-跳转失败！"), g.ClickOut(e._id, e.toAppid, o, !1), n && n(!1)
                                }
                            })
                        } else e.showImage || console.error("YLSDK showImage is null !"), g.ClickOut(e._id, e.toAppid, o, !1), uc.previewImage({
                            urls: [e.showImage]
                        })
                } else console.error("YLSDK id is null !")
            },
            setSwitchLog: function (e) {
                f.switchLog = e, console.log("YLSDK setSwitchLog:", e)
            }
        },
        A = {};
    console.log("YLSDK Type of sys---1:", void 0 === A ? "undefined" : _typeof(A));
    try {
        var s = uc.getSystemInfoSync();
        console.log("YLSDK getSystemInfoSync--sysInfo:", void 0 === s ? "undefined" : _typeof(s), s), s.constructor === String && (s = JSON.parse(s), console.log("YLSDK getSystemInfoSync--10")), console.log("YLSDK Type of sys---2:", void 0 === s ? "undefined" : _typeof(s)), console.log("YLSDK getSystemInfoSync:", JSON.stringify(s))
    } catch (e) {
        console.log("YLSDK sys--e:", e)
    }
    console.log("YLSDK Type of sys---3:", void 0 === A ? "undefined" : _typeof(A)), A.pf_num = 13, A.pf_name = "UC", A.pf_func = h, A.ld_k = i, console.warn("YLSDK 当前设置平台请确认： ", A.pf_name), "" == t.ylsdk_app_id && console.error("YLSDK 请在配置文件中填写您的游戏APPID"), "" == t.ylsdk_version && console.error("YLSDK 请在配置文件中填写您的游戏版本号"), A.pkg_name = t.ylsdk_pkg_name || "", A.s_log = t.ylsdk_debug_model, t.ylsdk_debug_model && console.warn("YLSDK 是否一直通过" + A.pf_name + " code(token)登录: ", t.login_by_code), console.warn("YLSDK 日志开关(本地)-", t.ylsdk_debug_model ? "打开" : "关闭"), t.ylsdk_app_id = t.ylsdk_app_id.replace(/\s/g, ""), t.ylsdk_version = t.ylsdk_version.replace(/\s/g, ""), A.game_config = t, g.init(A)
}();