(function () {
    'use strict';

    var REG = Laya.ClassUtils.regClass;
    var ui;
    (function (ui) {
        var test;
        (function (test) {
            class TestSceneUI extends Laya.Scene {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.loadScene("test/TestScene");
                }
            }
            test.TestSceneUI = TestSceneUI;
            REG("ui.test.TestSceneUI", TestSceneUI);
        })(test = ui.test || (ui.test = {}));
    })(ui || (ui = {}));

    class GameUI extends ui.test.TestSceneUI {
        constructor() {
            super();
            this.layerList = null;
            this.layerId = 0;
            GameUI.instance = this;
            Laya.MouseManager.multiTouchEnabled = false;
        }
        onEnable() {
            uc.ylInitSDK(function (success) {
                console.log("-----初始化SDK-----:", success);
                this.doSDKinterface();
            }.bind(this), 2);
            this.initUI();
        }
        initUI() {
            this.btnShareOrVideo.on(Laya.Event.CLICK, this, this.onShowShareOrVideo);
            this.btnShowBanner.on(Laya.Event.CLICK, this, this.onShowBanner);
            this.btnExit.on(Laya.Event.CLICK, this, this.onExit);
            this.btnShowVideo.on(Laya.Event.CLICK, this, this.onShowVideo);
            this.btnGetUserInfo.on(Laya.Event.CLICK, this, this.onGetUserInfo);
        }
        doSDKinterface() {
            console.log("---调用接口---");
            uc.ylSideBox(function (data) {
            }.bind(this));
            uc.ylGetCustom(function (data) {
            }.bind(this));
            uc.ylStatisticResult({ "total_score": 123, "rebirth_score": 123 }, function (status) {
                if (status) {
                }
            }.bind(this));
            let loginInfo = uc.ylGetUserInfo();
            console.log("---登录信息-loginInfo:", JSON.stringify(loginInfo));
            var that = this;
            uc.ylShareCard(function (shareInfo) {
                if (shareInfo) {
                    console.log("----获取分享图列表:", JSON.stringify(shareInfo));
                }
                else {
                }
            }.bind(this), 'MainScene');
            uc.ylStatisticViedo(0, 'adunit-6878a73e134f85e2', null);
            uc.ylStatisticShareCard(177);
            uc.ylGetLayerList(function (data) {
                if (data && data.length > 0) {
                    this.layerList = data;
                    uc.ylStatisticLayer(this.layerList[this.layerList.length - 1].layerPath);
                }
            }.bind(this));
        }
        onShowShareOrVideo(e) {
            uc.ylShowShareOrVideo("chanel_2", "model_2", function (type) {
                switch (type) {
                    case 0:
                        console.warn("------策略-无");
                        break;
                    case 1:
                        console.warn("------策略-分享");
                        let onShare = {
                            channel: 'bb',
                            module: 'aa',
                            showTime: new Date().getTime() - 3500,
                        };
                        uc.ylGetSharingResults(onShare, function (result) {
                            console.warn("-----ylGetSharingResults-result：", result);
                            if (result.sSuccess) {
                            }
                            else {
                                if (result.hasStrategy) {
                                    console.warn("-----分享失败话术：", result.trickJson);
                                }
                            }
                        });
                        break;
                    case 2:
                        console.warn("------策略-视频");
                        let result = uc.ylRewardByVideoOrShare(false);
                        let slLimit = uc.ylGetVSLimit();
                        console.log("---------ylRewardByVideoOrShare-result:", result, slLimit);
                        break;
                }
            }.bind(this));
        }
        onShowBanner(e) {
            console.log("GameUI-onShowBanner:");
            uc.ylBannerAdCreateByStyle({
                gravity: 0,
                left: 20,
                top: 100,
                width: 310,
                height: 200,
            }, true, (res) => {
                console.warn("GameUI-ylBannerAdCreateByStyle-back:", JSON.stringify(res));
            }, true, (res) => {
                console.warn("GameUI-ylBannerAdCreateByStyle-onResize-back:", JSON.stringify(res));
            });
        }
        onHideBanner(e) {
            console.log("GameUI-onHideBanner");
            uc.ylBannerAdHide();
        }
        onExit(e) {
            uc.ylExit();
        }
        onShowVideo(e) {
            console.log("GameUI-onShowVideo");
            uc.ylShowVideoAd({
                callBack: function (res) {
                    let tvNum = uc.ylGetWatchTvNum();
                    console.log("GameUI-onShowVideo-callBack-res:", JSON.stringify(res), tvNum);
                },
                unlockCustomNum: true,
                getPower: true
            });
        }
        onGetUserInfo(e) {
            uc.ylGetUserUCInfo(function (data) {
                console.log("GameUI-ylGetUserUCInfo-data:", JSON.stringify(data));
            });
        }
        testStoreValue() {
            this.test_sv_string();
            this.test_sv_list();
            this.test_sv_set();
            this.test_sv_hash();
            this.test_sv_radom();
        }
        test_sv_string() {
            uc.ylStoreValue({
                name: "testString",
                cmd: "set",
                args: "测试数据"
            }, function (status) {
                uc.ylStoreValue({
                    name: "testString",
                    cmd: "get"
                }, function (status) {
                }.bind(this));
            }.bind(this));
        }
        test_sv_list() {
            uc.ylStoreValue({
                name: "testList",
                cmd: "add",
                args: "0"
            }, function (status) {
            }.bind(this));
            uc.ylStoreValue({
                name: "testList",
                cmd: "add",
                args: "2"
            }, function (status) {
            }.bind(this));
            uc.ylStoreValue({
                name: "testList",
                cmd: "set",
                args: "0,3"
            }, function (status) {
                uc.ylStoreValue({
                    name: "testList",
                    cmd: "all"
                }, function (status) {
                }.bind(this));
                uc.ylStoreValue({
                    name: "testList",
                    cmd: "get",
                    args: "0"
                }, function (status) {
                }.bind(this));
                uc.ylStoreValue({
                    name: "testList",
                    cmd: "size"
                }, function (status) {
                }.bind(this));
                uc.ylStoreValue({
                    name: "testList",
                    cmd: "poll",
                    args: "2"
                }, function (status) {
                    uc.ylStoreValue({
                        name: "testList",
                        cmd: "size"
                    }, function (status) {
                    }.bind(this));
                    uc.ylStoreValue({
                        name: "testList",
                        cmd: "replace",
                        args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                    }, function (status) {
                        uc.ylStoreValue({
                            name: "testList",
                            cmd: "all"
                        }, function (status) {
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_set() {
            uc.ylStoreValue({
                name: "testSet",
                cmd: "add",
                args: "12"
            }, function (status) {
            }.bind(this));
            uc.ylStoreValue({
                name: "testSet",
                cmd: "add",
                args: "10"
            }, function (status) {
                uc.ylStoreValue({
                    name: "testSet",
                    cmd: "exist",
                    args: "10"
                }, function (status) {
                }.bind(this));
                uc.ylStoreValue({
                    name: "testSet",
                    cmd: "size"
                }, function (status) {
                    uc.ylStoreValue({
                        name: "testSet",
                        cmd: "del",
                        args: "10"
                    }, function (status) {
                        uc.ylStoreValue({
                            name: "testSet",
                            cmd: "all"
                        }, function (status) {
                            uc.ylStoreValue({
                                name: "testSet",
                                cmd: "replace",
                                args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                            }, function (status) {
                                uc.ylStoreValue({
                                    name: "testSet",
                                    cmd: "all"
                                }, function (status) {
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_hash() {
            uc.ylStoreValue({
                name: "testHash",
                cmd: "set",
                args: "u_name,许"
            }, function (status) {
                uc.ylStoreValue({
                    name: "testHash",
                    cmd: "get",
                    args: "u_name"
                }, function (status) {
                    uc.ylStoreValue({
                        name: "testHash",
                        cmd: "replace",
                        args: "{\"u_name\":\"唐\",\"sex\":\"women\"}"
                    }, function (status) {
                        uc.ylStoreValue({
                            name: "testHash",
                            cmd: "gets",
                            args: "u_name,sex"
                        }, function (status) {
                        }.bind(this));
                        uc.ylStoreValue({
                            name: "testHash",
                            cmd: "size",
                        }, function (status) {
                        }.bind(this));
                        uc.ylStoreValue({
                            name: "testHash",
                            cmd: "values",
                            args: "sex"
                        }, function (status) {
                        }.bind(this));
                        uc.ylStoreValue({
                            name: "testHash",
                            cmd: "del",
                            args: "u_name"
                        }, function (status) {
                            uc.ylStoreValue({
                                name: "testHash",
                                cmd: "all",
                            }, function (status) {
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_radom() {
            uc.ylStoreValue({
                name: "testRandom"
            }, function (status) {
            }.bind(this));
        }
    }

    class GameConfig {
        constructor() {
        }
        static init() {
            var reg = Laya.ClassUtils.regClass;
            reg("script/GameUI.ts", GameUI);
        }
    }
    GameConfig.width = 640;
    GameConfig.height = 1136;
    GameConfig.scaleMode = "fixedwidth";
    GameConfig.screenMode = "none";
    GameConfig.alignV = "top";
    GameConfig.alignH = "left";
    GameConfig.startScene = "test/TestScene.scene";
    GameConfig.sceneRoot = "";
    GameConfig.debug = false;
    GameConfig.stat = false;
    GameConfig.physicsDebug = false;
    GameConfig.exportSceneToJson = true;
    GameConfig.init();

    class Main {
        constructor() {
            if (window["Laya3D"])
                Laya3D.init(GameConfig.width, GameConfig.height);
            else
                Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
            Laya["Physics"] && Laya["Physics"].enable();
            Laya["DebugPanel"] && Laya["DebugPanel"].enable();
            Laya.stage.scaleMode = GameConfig.scaleMode;
            Laya.stage.screenMode = GameConfig.screenMode;
            Laya.stage.alignV = GameConfig.alignV;
            Laya.stage.alignH = GameConfig.alignH;
            Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
            if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
                Laya.enableDebugPanel();
            if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
                Laya["PhysicsDebugDraw"].enable();
            if (GameConfig.stat)
                Laya.Stat.show();
            Laya.alertGlobalError = true;
            Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
        }
        onVersionLoaded() {
            Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
        }
        onConfigLoaded() {
            GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
        }
    }
    new Main();

}());
