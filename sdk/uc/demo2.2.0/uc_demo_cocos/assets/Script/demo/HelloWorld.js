cc.Class({
    extends: cc.Component,

    properties: {
        label:cc.Label,
        btnShare:cc.Node,               //分享按钮
        btnSideBox:cc.Node,             //侧边栏列表按钮
        btnBoardAwardList:cc.Node,      //积分墙列表按钮
        btnGridAndScrollList:cc.Node,   //浮动卖量列表按钮
        btnSign:cc.Node,                //签到按钮

        pbSideBox:cc.Prefab,            //侧边栏列表界面
        pbInvite:cc.Prefab,             //积分墙列表界面
        pbYlBoxGrid:cc.Prefab,          //卖量网格浮动条
        pbYlBoxScrollHori:cc.Prefab,    //卖量横向浮动条
        pbYlBoxScrollVerti:cc.Prefab,   //卖量纵向浮动条
        pbYlGameOver:cc.Prefab,         //游戏结束界面
        pbSign:cc.Prefab,               //签到界面
        pbGetAward:cc.Prefab,           //获得奖励View
        pbFullBox:cc.Prefab,            //全屏卖量界面
        pbBreakEgg:cc.Prefab,           //砸金蛋
        pbOpenBox:cc.Prefab,            //开宝箱
        pbShakeTree:cc.Prefab,          //摇钱树
        preRank:cc.Prefab,              //排行榜
        pbBox1:cc.Prefab,               //盒子1
        nodeFloatView:cc.Node,          //浮动卖量View
        lbGridAd:cc.Label,              //格子广告按钮文字
        pbFullBox2:cc.Prefab,               //全屏卖量
    },
    onLoad: function () {
        console.log("-----onLoad-start");
        this.custom = 1;
        let self = this;
        uc.ylOnPowerChange(function(type){
            console.warn("HelloWorld-ylOnPowerChange back[1:视频,2:定时自动恢复]:",type);
            let textType = (type == 1) ? "看视频" : "定时";
            uc.showToast({
                  title: textType+'获得体力',
                  icon: 'success',
                  duration: 2000
                });
        }.bind(this));
        // uc.ylInitSDK(function(success){
        //     console.log("HelloWorld-ylInitSDK:",success);
        //     if(success === true){
        //         console.warn("HelloWorld-ylInitSDK-初始化成功");
        //         // self.doSDKinterface();
        //         // self.refreshBoardAndBoxBtnVisible();
        //     }else if(success == 'config_success' || success == 'config_fail'){
        //         console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
        //         uc.ylGetPowerInfo(function(data){
        //             //干点什么
        //             console.log("HelloWorld-ylGetPowerInfo:",data);
        //         });
        //     }else{
        //         console.warn("HelloWorld-ylInitSDK-初始化失败");
        //     }
        // },true);
    },
    onLogin(){
        uc.ylInitSDK(function(success){
            console.log("HelloWorld-ylInitSDK:",success);
            if(success === true){
                console.warn("HelloWorld-ylInitSDK-初始化成功");
                this.doSDKinterface();
            }else if(success == 'config_success' || success == 'config_fail'){
                console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
                uc.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("HelloWorld-ylGetPowerInfo:",data);
                });
            }else{
                console.warn("HelloWorld-ylInitSDK-初始化失败");
            }
        }.bind(this),false);
    },

    /**
    * 调用SDK接口
    **/
    doSDKinterface(){
        uc.ylLog("---调用接口---");
        uc.ylSideBox(function (data) {
        }.bind(this));
        uc.ylGetCustom(function(data){
        }.bind(this));
        uc.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status){
            if(status){
                //干点什么
            }
        }.bind(this));
        uc.ylIntegralWall(function(){}.bind(this));
        uc.ylGetPowerInfo(function(data){
            console.log("HelloWorld ylGetPowerInfo--data:",data);
        });
        let loginInfo = uc.ylGetUserInfo();//获取登录信息，包括微信oppid和code(code只有第一次登录才能取到)
        console.log("---登录信息-loginInfo:",JSON.stringify(loginInfo));


        var that = this;
        /**
         * 获取分享图列表
         */
        uc.ylShareCard(function (shareInfo) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
                this.btnShare.active = true;
            }else{
                //获取失败
            }
        }.bind(this),'MenuScene');
        // uc.ylStatisticViedo(1);
        uc.ylStatisticShareCard(177);
        this.testStoreValue();
        let appid = uc.ylGetAppID();
        console.log("---ylGetAppID:",appid);
        uc.ylGetSignData(function(data){
            console.log("---ylGetSignData:",data);
        });
        uc.ylIntegralWall(function(data){

        });
        let inviteAccount = uc.ylGetInviteAccount();
        console.log("---ylGetInviteAccount:",inviteAccount);
        let data = uc.ylGetSwitchInfo();
        uc.ylLog("---刷新开关状态---",data);
    },

    //刷新侧边栏/积分墙按钮展示状态
    refreshBoardAndBoxBtnVisible(){
        
        // this.btnSideBox.active = data.switchPush === 1;//[ 0:关、1:开 ]
        // this.btnGridAndScrollList.active = data.switchPush === 1;
    },

    //显示全屏卖量
    onFullBox2Show(){
    },
    //显示侧边栏列表对话框
    onBoxShow(){
    },

    //显示积分墙列表对话框
    onBoardAwardListShow(){
    },
    //显示浮动卖量列表
    onShowGridAndScrollList(){
    },
    onCloseFloatView(){
    },
    //显示获得奖励对话框
    onShowGetAward(evt){
    },
    showTip(tip){

    },
    //显示全屏卖量界面
    onShowFullBox(){        
    },
    //显示签到对话框 
    onShowSign(){
    },

    //分享对话框
    onClickShare(){
    },
    //砸金蛋
    onClickBreakEgg(){
    },
    //开宝箱
    onOpenBox(){
    }, 
    //摇钱树
    onShakeTree(){
    }, 
    //显示获胜结算对话框
    onShowOverWin(){
    },
    //视频分享策略
    onShowShareOrVideo(){
        uc.ylShowShareOrVideo("bb","aa",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    break;
                case 2:
                    console.warn("------策略-视频");
                    break;
            }
        }.bind(this));
    },
    //显示失败结算对话框
    onShowOverFail(){
    },
    //显示视频广告
    onShowVideo(){
    },
    //显示banner
    onShowBanner(){
    },
    onHideBanner(){
    },
    //视频解锁关卡
    onVideoUnlockCustoms(){
    },
    onShowGameOver(isWin){
    },
    //切换界面,可调用强制刷新Banner接口
    onChangeView(){
        uc.ylChangeView();
    },
    //积分墙跳转到目标小游戏
    inviteJumpToMinGame(evt){
    },

    //注册小游戏回到前台的事件监听
    initWxOnshow(){
    },
    onShowRank(){
    },
    //游戏结束
    onOver(){
        this.custom +=1;
        uc.ylOverGame(this.custom);
    },
    //获取深度误触开关
    onGetDeepTouch(){
       let info = uc.ylGetDeepTouch(this.custom);
       console.log("onGetDeepTouch-info: ",JSON.stringify(info));
    },
    //消耗体力
    onConsumePower(){
       let power =  uc.ylGetPower();
       uc.ylSetPower(power -1);
    },
    onAddPower(){
        let power =  uc.ylGetPower();
        uc.ylSetPower(power +1);
    },
    //上传排行榜数据
    uploadScore(){
    },
    _strMapToObj(strMap){
        let obj= Object.create(null);
        for (let[k,v] of strMap) {
          obj[k] = v;
        }
        return obj;
    },
    //测试自定义空间值
    testStoreValue(){
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    },
    test_sv_string(){
        //String
        uc.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                uc.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get",
                        // args:"测试数据"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    },
    test_sv_list(){
        //List
        uc.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        uc.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        uc.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                uc.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all",
                        // args:""
                    },
                    function(status){
                        
                }.bind(this));
                 uc.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        uc.ylStoreValue(
            {
                name:"testList",
                cmd:"size",
                // args:""
            },
            function(status){
                
        }.bind(this));
        uc.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    uc.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size",
                            // args:""
                        },
                        function(status){
                            
                    }.bind(this));
                    uc.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            uc.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all",
                                    // args:""
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    },
    test_sv_set(){
        //Set
        uc.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        uc.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                uc.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                uc.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size",
                        // args:""
                    },
                    function(status){
                        uc.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                uc.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all",
                                        // args:""
                                    },
                                    function(status){
                                        uc.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            uc.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all",
                                                    // args:""
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    },
    test_sv_hash(){
        //litMap
        uc.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                uc.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        uc.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            uc.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            uc.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                                // args:"u_name,sex"
                            },function(status){}.bind(this));
                            uc.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"u_name,sex"
                            },function(status){}.bind(this));
                            uc.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                uc.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                    // args:""
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    },
    test_sv_radom(){
        //testRandom
        uc.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    },
});
