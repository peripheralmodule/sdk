
cc.Class({
    extends: cc.Component,

    properties: {
        nodeContent:cc.Node,
        pbItem:cc.Prefab,
    },

    initData(data,sceneName,_type){
        if(!this.items) this.items = new Array();
        if(!this.items || this.items.length ==0 || this.items.length > data.length){
            for(let i=0;i<data.length;i++){
                let data_item = data[i];
                if((_type == 1 && data_item.icon && data_item.icon != "") || 
                    (_type == 2 && data_item.bannerImage && data_item.bannerImage != "")){

                    let random = Math.floor(Math.random()*10);
                    let playerNum = 16320 + Math.floor(Math.random()*600000);
                    data_item.playerNum = playerNum;

                    let item = cc.instantiate(this.pbItem);
                    item.parent = this.nodeContent;
                    item.getComponent('ylBox2GridItem').setItemConf(data_item,sceneName,_type);
                    this.items.push(item);
                }
            }
        }
    },
});
