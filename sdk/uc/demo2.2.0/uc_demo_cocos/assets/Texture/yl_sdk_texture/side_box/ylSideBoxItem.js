/**
 * 侧边栏卖量Item
 */
cc.Class({
    extends: cc.Component,

    properties: {
        appIcon: cc.Sprite,
        labTitle: cc.Label,
    },

    updateCell (obj,scene) {
        this._obj = obj;
        this._scene = scene;
        let name = window.Global.handleNameLen(this._obj.title,4);
        this.labTitle.string = name;
        if (this._obj.icon && this._obj.icon !== '') {
            if (this._obj.texture) {
                this.setTex(this._obj.texture);
            } else {
                cc.director.on(window.Global.EVENT_LOAD_SIDE_BOX_ICON + this._obj._id, (evt) => {
                    this.setTex(evt.getUserData());
                }, this); 
            }
        }else {
           // console.log('url是空的：this._obj.icon = ' + this._obj.icon);
        }
    },

    setTex(tex) {
        if (!cc.isValid(this.node)) return;
        let size = this.appIcon.node.getContentSize();
        this.appIcon.spriteFrame = new cc.SpriteFrame(tex);
        this.appIcon.node.setContentSize(size);
    },
    //跳转到目标小游戏 
    onJump(){
        if(this._obj.type == 0){
            wx.ylNavigateToMiniProgram(
                {
                    _id: this._obj._id,
                    toAppid:this._obj.toAppid,   
                    toUrl:this._obj.toUrl,
                    type:this._obj.type,
                    showImage:this._obj.showImage,
                    source:'item_side_box',//从哪个模块导出的，该字段具体值由调用方自行定义
                },
                function(success){
                    if(success){
                    let evt = new cc.Event.EventCustom(window.Global.EVENT_REFRESH_SIDEBOX);
                    cc.director.dispatchEvent(evt);
                    }else{
                        let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_FULL_BOX);
                        cc.director.dispatchEvent(evt);
                    }
            }.bind(this));
        }else{
            //展示二维码图片,玩家通过扫码进入游戏
        }
    },
});
