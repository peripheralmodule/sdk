
cc.Class({
    extends: cc.Component,

    properties: {
        display: cc.Sprite,
    },

    start () {
        this.tex = new cc.Texture2D();
        // 发消息给子域
        wx.postMessage({
            message:'Show'
        })
    },
    _updaetSubDomainCanvas () {
        if (!this.node.active || !this.tex) {
            return;
        }
        var openDataContext = wx.getOpenDataContext();
        var sharedCanvas = openDataContext.canvas;
        this.tex.initWithElement(sharedCanvas);
        this.tex.handleLoadedTexture();
        this.display.spriteFrame = new cc.SpriteFrame(this.tex);
    },
    update () {
        this._updaetSubDomainCanvas();
    },
    onShow(){
        this.node.active = true;
    },
    onClose(){
        this.node.active = false;
    },
});
