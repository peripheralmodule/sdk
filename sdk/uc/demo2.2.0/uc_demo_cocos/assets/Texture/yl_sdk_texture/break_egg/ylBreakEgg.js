
cc.Class({
    extends: cc.Component,

    properties: {
        lbTips:cc.Label,
        spEgg:cc.Sprite,
        pbBreak:cc.ProgressBar,
        nodeFailTip:cc.Node,
        nodeTips:cc.Node,
        nodeFinger:cc.Node,
        sfEgg:[cc.SpriteFrame],
        nodeEgg:cc.Node,
        nodeVideo:cc.Node,
        sHit:{
            type:cc.AudioClip,
            default:null,
        },
        sAward:{
            type:cc.AudioClip,
            default:null,
        },
        sFail:{
            type:cc.AudioClip,
            default:null,
        },
    },

    showView(){
        //奖励数据自己定义,此处只供参考
        this.awards = {id:1,award:[{type:"lotteryHammer",value:1}],title:"锤子"};
        this.node.active = true;
        this.nodeFailTip.active = false;
        this.initData();
        this.startCountDown();
        wx.ylCreateVideoAd();
    },
    initData(){
        this.count = 800; //倒计时
        this.isStart = false;
        this.showVideo = false;
        this.nodeTips.x = 0;
        this.nodeTips.y = 0;
        this.nodeTips.opacity = 0;
        this.nodeTips.active = false;
        this.nodeVideo.active = false;
        this.series_count = 0;
        this.spEgg.spriteFrame = this.sfEgg[0];
        this.count_show_banner = Math.floor(Math.random()*4)+1; //弹出广告的倒计时间范围
        this.series_all = Math.floor(Math.random()*2)+2;//连续点击总数
        this.pbBreak.progress = 0;
        this.changeCountDown(this.count);
        this.nodeFinger.active = true;
        this.animFinger = this.nodeFinger.getComponent(cc.Animation);
        this.animFinger.play();
        this.animEgg = this.nodeEgg.getComponent(cc.Animation);
    },
    startCountDown(){
        if(!this.isStart){
            this.isStart = true;
            this.countDownTime();
        }
    },
    countDownTime(){
        let that = this;
        this.schedule(function() {
            that.count -= 1;
            that.count = that.count < 0 ? 0 :that.count;
            that.changeCountDown();
            let pro = this.pbBreak.progress - 0.001;
            this.pbBreak.progress = pro < 0 ? 0 : pro;
            this.refreshEggPng();
            if(that.count == 0){
                that.onClose();
            }
         }, 0.01, (this.count-1), 0.01);
    },
    changeCountDown(_count){
        let t_1 = Math.floor(this.count/100);
        let t_2 = this.count%100;
        this.lbTips.string = "0"+t_1+":"+(t_2<10 ? "0"+t_2 : t_2);
    },
    //砸金蛋
    doBreak(){
        console.log("");
        let that = this;
        if(this.nodeFinger.active){
            this.animFinger.stop();
            this.nodeFinger.active = false;
        }
        this.animEgg.play();
        Global.playSound(this.sHit);
        this.pbBreak.progress = (this.pbBreak.progress +0.05) >= 1 ? 1 : this.pbBreak.progress + 0.05;
        this.refreshEggPng();
        this.checkVideo();
        if(this.nodeVideo.active && !this.showVideo){
            this.showVideo = true;
            wx.ylShowVideoAd(
                function(status){
                    console.log("----showVideo:"+status);
                    switch (status){
                        case 1:
                            //视频广告-播放完成
                            let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_GET_AWARD);
                            evt.setUserData(that.awards);
                            cc.director.dispatchEvent(evt);
                            Global.playSound(that.sAward);
                            that.showVideo = false;
                            break;
                        case 2:
                            //视频广告-播放取消
                            that.showTips();
                            that.showVideo = false;
                            break;
                        case 0:
                            //视频广告-播放失败
                            that.nodeFailTip.active = true;   
                            Global.playSound(that.sFail);             
                            that.initData();
                            break;
                    }
                }
            );
        }
    },
    checkVideo(){
        //进度条大于10%小于50%时，
        //有10%概率在下一次点击时出现视频标志。
        //大于50%时有90%概率出现视频标志。点击带有此标志的操作按钮时，拉起激励视频。
        //并tips提示：完整看完视频后有极大概率获得神秘奖励！渐现1s后向上移动消失。
        let num = Math.floor(Math.random()*100);
        if(this.pbBreak.progress >0.5){
            if(num <= 90){
                this.nodeVideo.active = true;
            }
        }else if(this.pbBreak.progress >0.1){
            if(num <= 10){
                this.nodeVideo.active = true;
            }
        }
    },
    showTips(){
        this.nodeTips.active = true;
        this.nodeTips.opacity = 0;
        this.nodeTips.runAction(cc.sequence(
            // cc.delayTime(1),
            cc.fadeIn(0.1),
            cc.delayTime(0.3),
            cc.spawn(cc.moveTo(2,0,500),cc.fadeOut(2)),
            cc.callFunc(function(){
                this.nodeTips.x = 0;
                this.nodeTips.y = 0;
                this.nodeTips.active = false;
                this.nodeTips.opacity = 0;
        },this,0)));
    },
    refreshEggPng(){
        let pro = this.pbBreak.progress;
        let png_index = 0;
        if(pro >0.9){
            png_index = 4;
        }else if(pro > 0.6){
            png_index = 3;
        }else if(pro > 0.4){
            png_index = 4;
        }else if(pro > 0.25){
            png_index = 1;
        }
        this.spEgg.spriteFrame = this.sfEgg[png_index];
    },
    onFailAgain(){
        this.nodeFailTip.active = false;
        this.startCountDown();
    },
    onFailCancel(){
        this.nodeFailTip.active = false;
        this.onClose();
    },
    onClose(){
        wx.ylBannerAdHide();
        this.node.active = false;
    },
});
