window.Global = {
    EVENT_SHOW_GET_AWARD:'EVENT_SHOW_GET_AWARD',    //领取奖励
    bannerAd:null,
    soundOnOff:true,

	/**
	* @param playerName 字符串
	* @param len 截取长度
	**/
    handleNameLen(playerName, len){
        var new_playerName = "";
        if (playerName && playerName.length > len) {
            new_playerName = playerName.substring(0, len);
            new_playerName += "..."
        }
        else {
            new_playerName = playerName;
        }
        return new_playerName;
    },

    playSound(soundClip){
        if(Global.soundOnOff){
            cc.audioEngine.playEffect(soundClip,false);
        }
    },
    //给定一个列表，获取随机列表
    getRandomList(total,count){
        let showLightId = new Array();
        if(count == 0 || total == 0) return showLightId;
        let showLightNum = Math.floor(Math.random()*total)+1;
        let indexId = new Array();
        for(let i=0;i<count;i++){
            indexId[i] = i;
        }
        for(let i=0;i<showLightNum;i++){
            let r_id = this.getRandomId(indexId);
            showLightId.push(r_id);
        }
        return showLightId;
    },
    //获取随机ID
    getRandomId(idLists){
        // console.log("----idLists.length:",idLists.length,idLists);
        let id = Math.floor(Math.random()*(idLists.length-1));
        let r_id = idLists[id];
        idLists.splice(id,1);
        return r_id;
    },
};
