
cc.Class({
    extends: cc.Component,

    properties: {
        nodeDays:[cc.Node],
        nodeGetAward:cc.Node,
        nodeGetDouble:cc.Node,
        nodeHasSign:cc.Node,
        spAwardType:cc.Sprite,
        sfAwardType:[cc.SpriteFrame],
        pbBoxScrollHori:cc.Prefab,
    },

    showView(data){
        this.node.active = true;
        this.openDay = -1;
        if(this.signDatas){
            this.initSignData(this.signDatas);
        }else if(data){
            this.signDatas = data;
            this.initSignData(data);                
        }else{
            var that = this
            tt.ylGetSignData(function (data) {
                that.signDatas = data;
                that.initSignData(data);                
            }.bind(this));
        }
        // if(!this.nodeBoxScrollHori) this.nodeBoxScrollHori = cc.instantiate(this.pbBoxScrollHori);
        // this.nodeBoxScrollHori.parent = this.node;
        // this.nodeBoxScrollHori.getComponent('ylBoxScrollHori').showView(0,749);
    },
    initSignData(data) {
        if (data.code != 0) {
            console.error('签到列表数据失败了! ', data.msg);
            return;
        }
        if (data.signconfig) {
            let count =  data.signconfig.length < this.nodeDays.length ? data.signconfig.length : this.nodeDays.length;
            for(let i=0;i<count;i++){
                let element = data.signconfig[i];
                if(element.state == 'open'){
                   this.openDay =  element.id;
                }
                this.nodeDays[i].getComponent('ylSignItem').initData(element);
            }
        }
        this.nodeGetDouble.active = (this.openDay != -1);
        this.nodeGetAward.active = (this.openDay != -1);
        this.nodeHasSign.active = (this.openDay == -1);
    },
    getAward(){
        let that = this;
        tt.ylFetchSign(this.openDay,function(res){
            let data = JSON.parse(res);
            //弹出获奖窗口
            if(data && data.code == 0 && data.state == 'accepted'){
                let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_GET_AWARD);
                evt.setUserData(data.award);
                cc.director.dispatchEvent(evt);
                that.onClose();
            }else{
                console.error("-----奖励领取失败-----")
            }
        });
    },
    getDoubleAward(){
        let that = this;
        tt.ylFetchSign(this.openDay,function(res){
            let data = JSON.parse(res);
            //弹出获奖窗口
            if(data && data.code == 0 && data.state == 'accepted'){
                let evt = new cc.Event.EventCustom(window.Global.EVENT_SHOW_GET_AWARD);
                evt.setUserData(data.award);
                cc.director.dispatchEvent(evt);
                that.onClose();
            }else{
                console.error("-----奖励领取失败-----")
            }
        });
    },
    onClose(){
        this.node.active = false;
        // if(this.nodeBoxScrollHori)
        //     this.nodeBoxScrollHori.getComponent('ylBoxScrollHori').onClose();
    },
});
