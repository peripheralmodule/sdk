import { ui } from "./../ui/layaMaxUI";
/**
 * 本示例采用非脚本的方式实现，而使用继承页面基类，实现页面逻辑。在IDE里面设置场景的Runtime属性即可和场景进行关联
 * 相比脚本方式，继承式页面类，可以直接使用页面定义的属性（通过IDE内var属性定义），比如this.tipLbll，this.scoreLbl，具有代码提示效果
 * 建议：如果是页面级的逻辑，需要频繁访问页面内多个元素，使用继承式写法，如果是独立小模块，功能单一，建议用脚本方式实现，比如子弹脚本。
 */
export default class GameUI extends ui.test.TestSceneUI {
    /**设置单例的引用方式，方便其他类引用 */
    static instance: GameUI;
    private layerList:Array<any> = null;
    private layerId:number = 0; //流失列表Index-测试用
    constructor() {
        super();
        GameUI.instance = this;
        //关闭多点触控，否则就无敌了
        Laya.MouseManager.multiTouchEnabled = false;
    }

    onEnable(): void {
        tt.ylSetCurScene("MainScene");//一定记得在每个场景都要设置当前场景
        tt.ylGameLifeListen(function(type,res){
            if(type === 'onShow'){
                console.warn("YLSDK HelloWorld-onShow--res:",res);
            }else if(type === 'onHide'){
                console.warn("YLSDK HelloWorld-onHide");
            }else if(type === 'onError'){
                console.warn("YLSDK HelloWorld-onError--res:",res);
            }
        });
        tt.ylInitSDK(function(success:any){
            if(success === true){
                console.warn("HelloWorld-ylInitSDK-初始化成功");
                this.doSDKinterface();
            }else if(success == 'config_success' || success == 'config_fail'){
                console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
                tt.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("HelloWorld-ylGetPowerInfo:",data);
                });
            }else{
                console.warn("HelloWorld-ylInitSDK-初始化失败");
            }     
        }.bind(this));
        this.initUI();
    }
    initUI():void{
        this.lbRecorderTip.text = "录屏-无";
		this.btnReStartOrStop.on(Laya.Event.CLICK,this,this.onReStartOrStop);
        this.btnReResumeOrPause.on(Laya.Event.CLICK,this,this.onReResumeOrPause);
        this.btnShareVideo.on(Laya.Event.CLICK,this,this.onShareVideo);
        this.btnShowShareOrVideo.on(Laya.Event.CLICK,this,this.onShowShareOrVideo);
    }
    //开始或停止录屏
    onReStartOrStop(e:Laya.Event):void{
        let _status = tt.ylGetRecorderStatus();
        console.log("-------------onRecordStartOrStop--------_status:",_status);
        if(_status == 0){
            //duration:录屏的时长，单位 s，必须 >3 &&  <= 300s（5 分钟）
            let duration = 200;
            tt.ylRecorderStart(duration,function(status){
                console.log("-------------录屏--------status:",status);
                switch(status){//录屏状态[0:无,1:录屏中,2:暂停]
                    case 0:
                        this.btnReResumeOrPause.active = false;
                        this.btnReStartOrStop.label = '录屏';
                        this.btnReResumeOrPause.label = '继续';
                        this.lbRecorderTip.text = '录屏-无';
                        let videoPath = tt.ylGetVideoPath();
                        console.log("---videoPath:",videoPath);
                    break;
                    case 1:
                        this.btnReResumeOrPause.active = true;
                        this.btnReStartOrStop.label = '停止';
                        this.btnReResumeOrPause.label = '暂停';
                        this.lbRecorderTip.text = '录屏-录屏中';
                    break;
                    case 2:
                        this.btnReStartOrStop.label = '停止';
                        this.btnReResumeOrPause.label = '继续';
                        this.lbRecorderTip.text = '录屏-暂停';
                    break;
                }
            }.bind(this));
        }else if(_status == 1){
            tt.ylRecorderStop();
        }
    }
    //暂停或继续录屏
    onReResumeOrPause(e:Laya.Event):void{
        let status = tt.ylGetRecorderStatus();
        console.log("-------------onRecordResumeOrPasue--------status:",status);
        if(status && status == 1){
            tt.ylPause();
        }else if(status && status == 2){
            tt.ylResume();
        }
    }
    //分享录屏
    onShareVideo(e:Laya.Event):void{
        let info = {
            title: "测试分享视频",
            desc: "测试描述",
            query: "",
            extra: {
                withVideoId:true,
                videoTopics: []
            },
      };
      tt.ylShareVideo(info,function(res){
          if(res){
             if(res.videoId) {
                console.log("-----ylShareVideo----videoId:",res.videoId);
                tt.ylNavigateToVideoView(res.videoId,function(status){
                    console.log("-----ylNavigateToVideoView----back:",status);
                });
             }
          }
          console.log("-----ylShareVideo----分享结果:",res);
      });
    }
    doSDKinterface():void{
        console.log("---调用接口---");
        tt.ylGetCustom(function(data:any){
        }.bind(this));
        tt.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status:any){
            if(status){
                //干点什么
            }
        }.bind(this));

        let userWXinfo = tt.ylGetUserTTinfo();//先获取本地缓存，没有在弹窗通过用户授权获取

        let loginInfo = tt.ylGetUserInfo();//获取登录信息，包括微信oppid和code(code只有第一次登录才能取到)
        console.log("---登录信息-loginInfo:",JSON.stringify(loginInfo));
        // tt.ylBannerAdCreateByStyle({
        //     top: 100,
        //     left: 20,
        //     width: 130,
        // },true,function(res){
        //     console.log("HelloWorld-ylBannerAdCreateByStyle-onResize-callback:",res);
        // },true);
        tt.ylBannerAdCreate(false, function(status){
            if(status){
                console.log("HelloWorld-ylBannerAdCreate-status:",status);
                //创建成功
                tt.ylBannerAdShow();
            }else{
                //创建失败
            }
        }, true);

        var that = this;
        /**
         * 获取分享图列表
         */
        tt.ylShareCard(function (shareInfo:any) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
            }else{
                //获取失败
            }
        }.bind(this),null);
        tt.ylStatisticViedo(0,'adunit-6878a73e134f85e2',null);
        tt.ylStatisticShareCard(177);
        tt.ylGetLayerList(function(data){
            if(data && data.length >0){
                this.layerList = data;
                tt.ylStatisticLayer(this.layerList[this.layerList.length-1].layerPath);
            }
        }.bind(this));
        // this.testStoreValue();
    }
    //视频分享策略
    onShowShareOrVideo():void{
        tt.ylShowShareOrVideo("chanel_2","model_2",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    let result = tt.ylRewardByVideoOrShare(false);
                    let slLimit = tt.ylGetVSLimit();
                    console.log("---------ylRewardByVideoOrShare-result:",result,slLimit);
                    break;
                case 2:
                    console.warn("------策略-视频");
                    let result_2 = tt.ylRewardByVideoOrShare(true);
                    let slLimit_2 = tt.ylGetVSLimit();
                    console.log("---------ylRewardByVideoOrShare-result:",result_2,slLimit_2);
                    break;
            }
        }.bind(this));
    }
    //测试自定义空间值
    testStoreValue():void{
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    }
    test_sv_string():void{
        //String
        tt.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    }
    test_sv_list():void{
        //List
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 tt.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    tt.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    tt.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            tt.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    }
    test_sv_set():void{
        //Set
        tt.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                tt.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        tt.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                tt.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        tt.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            tt.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    }
    test_sv_hash():void{
        //litMap
        tt.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        tt.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                tt.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    }
    test_sv_radom():void{
        //testRandom
        tt.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    }
}