(function () {
    'use strict';

    var REG = Laya.ClassUtils.regClass;
    var ui;
    (function (ui) {
        var test;
        (function (test) {
            class TestSceneUI extends Laya.Scene {
                constructor() { super(); }
                createChildren() {
                    super.createChildren();
                    this.loadScene("test/TestScene");
                }
            }
            test.TestSceneUI = TestSceneUI;
            REG("ui.test.TestSceneUI", TestSceneUI);
        })(test = ui.test || (ui.test = {}));
    })(ui || (ui = {}));

    class GameUI extends ui.test.TestSceneUI {
        constructor() {
            super();
            this.layerList = null;
            this.layerId = 0;
            GameUI.instance = this;
            Laya.MouseManager.multiTouchEnabled = false;
        }
        onEnable() {
            tt.ylSetCurScene("MainScene");
            tt.ylGameLifeListen(function (type, res) {
                if (type === 'onShow') {
                    console.warn("YLSDK HelloWorld-onShow--res:", res);
                }
                else if (type === 'onHide') {
                    console.warn("YLSDK HelloWorld-onHide");
                }
                else if (type === 'onError') {
                    console.warn("YLSDK HelloWorld-onError--res:", res);
                }
            });
            tt.ylInitSDK(function (success) {
                if (success === true) {
                    console.warn("HelloWorld-ylInitSDK-初始化成功");
                    this.doSDKinterface();
                }
                else if (success == 'config_success' || success == 'config_fail') {
                    console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
                    tt.ylGetPowerInfo(function (data) {
                        console.log("HelloWorld-ylGetPowerInfo:", data);
                    });
                }
                else {
                    console.warn("HelloWorld-ylInitSDK-初始化失败");
                }
            }.bind(this));
            this.initUI();
        }
        initUI() {
            this.lbRecorderTip.text = "录屏-无";
            this.btnReStartOrStop.on(Laya.Event.CLICK, this, this.onReStartOrStop);
            this.btnReResumeOrPause.on(Laya.Event.CLICK, this, this.onReResumeOrPause);
            this.btnShareVideo.on(Laya.Event.CLICK, this, this.onShareVideo);
            this.btnShowShareOrVideo.on(Laya.Event.CLICK, this, this.onShowShareOrVideo);
        }
        onReStartOrStop(e) {
            let _status = tt.ylGetRecorderStatus();
            console.log("-------------onRecordStartOrStop--------_status:", _status);
            if (_status == 0) {
                let duration = 200;
                tt.ylRecorderStart(duration, function (status) {
                    console.log("-------------录屏--------status:", status);
                    switch (status) {
                        case 0:
                            this.btnReResumeOrPause.active = false;
                            this.btnReStartOrStop.label = '录屏';
                            this.btnReResumeOrPause.label = '继续';
                            this.lbRecorderTip.text = '录屏-无';
                            let videoPath = tt.ylGetVideoPath();
                            console.log("---videoPath:", videoPath);
                            break;
                        case 1:
                            this.btnReResumeOrPause.active = true;
                            this.btnReStartOrStop.label = '停止';
                            this.btnReResumeOrPause.label = '暂停';
                            this.lbRecorderTip.text = '录屏-录屏中';
                            break;
                        case 2:
                            this.btnReStartOrStop.label = '停止';
                            this.btnReResumeOrPause.label = '继续';
                            this.lbRecorderTip.text = '录屏-暂停';
                            break;
                    }
                }.bind(this));
            }
            else if (_status == 1) {
                tt.ylRecorderStop();
            }
        }
        onReResumeOrPause(e) {
            let status = tt.ylGetRecorderStatus();
            console.log("-------------onRecordResumeOrPasue--------status:", status);
            if (status && status == 1) {
                tt.ylPause();
            }
            else if (status && status == 2) {
                tt.ylResume();
            }
        }
        onShareVideo(e) {
            let info = {
                title: "测试分享视频",
                desc: "测试描述",
                query: "",
                extra: {
                    withVideoId: true,
                    videoTopics: []
                },
            };
            tt.ylShareVideo(info, function (res) {
                if (res) {
                    if (res.videoId) {
                        console.log("-----ylShareVideo----videoId:", res.videoId);
                        tt.ylNavigateToVideoView(res.videoId, function (status) {
                            console.log("-----ylNavigateToVideoView----back:", status);
                        });
                    }
                }
                console.log("-----ylShareVideo----分享结果:", res);
            });
        }
        doSDKinterface() {
            console.log("---调用接口---");
            tt.ylGetCustom(function (data) {
            }.bind(this));
            tt.ylStatisticResult({ "total_score": 123, "rebirth_score": 123 }, function (status) {
                if (status) {
                }
            }.bind(this));
            let userWXinfo = tt.ylGetUserTTinfo();
            let loginInfo = tt.ylGetUserInfo();
            console.log("---登录信息-loginInfo:", JSON.stringify(loginInfo));
            tt.ylBannerAdCreate(false, function (status) {
                if (status) {
                    console.log("HelloWorld-ylBannerAdCreate-status:", status);
                    tt.ylBannerAdShow();
                }
                else {
                }
            }, true);
            var that = this;
            tt.ylShareCard(function (shareInfo) {
                if (shareInfo) {
                    console.log("----获取分享图列表:", JSON.stringify(shareInfo));
                }
                else {
                }
            }.bind(this), null);
            tt.ylStatisticViedo(0, 'adunit-6878a73e134f85e2', null);
            tt.ylStatisticShareCard(177);
            tt.ylGetLayerList(function (data) {
                if (data && data.length > 0) {
                    this.layerList = data;
                    tt.ylStatisticLayer(this.layerList[this.layerList.length - 1].layerPath, function (data) {
                    });
                }
            }.bind(this));
        }
        onShowShareOrVideo() {
            tt.ylShowShareOrVideo("chanel_2", "model_2", function (type) {
                switch (type) {
                    case 0:
                        console.warn("------策略-无");
                        break;
                    case 1:
                        console.warn("------策略-分享");
                        let result = tt.ylRewardByVideoOrShare(false);
                        let slLimit = tt.ylGetVSLimit();
                        console.log("---------ylRewardByVideoOrShare-result:", result, slLimit);
                        break;
                    case 2:
                        console.warn("------策略-视频");
                        let result_2 = tt.ylRewardByVideoOrShare(true);
                        let slLimit_2 = tt.ylGetVSLimit();
                        console.log("---------ylRewardByVideoOrShare-result:", result_2, slLimit_2);
                        break;
                }
            }.bind(this));
        }
        testStoreValue() {
            this.test_sv_string();
            this.test_sv_list();
            this.test_sv_set();
            this.test_sv_hash();
            this.test_sv_radom();
        }
        test_sv_string() {
            tt.ylStoreValue({
                name: "testString",
                cmd: "set",
                args: "测试数据"
            }, function (status) {
                tt.ylStoreValue({
                    name: "testString",
                    cmd: "get"
                }, function (status) {
                }.bind(this));
            }.bind(this));
        }
        test_sv_list() {
            tt.ylStoreValue({
                name: "testList",
                cmd: "add",
                args: "0"
            }, function (status) {
            }.bind(this));
            tt.ylStoreValue({
                name: "testList",
                cmd: "add",
                args: "2"
            }, function (status) {
            }.bind(this));
            tt.ylStoreValue({
                name: "testList",
                cmd: "set",
                args: "0,3"
            }, function (status) {
                tt.ylStoreValue({
                    name: "testList",
                    cmd: "all"
                }, function (status) {
                }.bind(this));
                tt.ylStoreValue({
                    name: "testList",
                    cmd: "get",
                    args: "0"
                }, function (status) {
                }.bind(this));
                tt.ylStoreValue({
                    name: "testList",
                    cmd: "size"
                }, function (status) {
                }.bind(this));
                tt.ylStoreValue({
                    name: "testList",
                    cmd: "poll",
                    args: "2"
                }, function (status) {
                    tt.ylStoreValue({
                        name: "testList",
                        cmd: "size"
                    }, function (status) {
                    }.bind(this));
                    tt.ylStoreValue({
                        name: "testList",
                        cmd: "replace",
                        args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                    }, function (status) {
                        tt.ylStoreValue({
                            name: "testList",
                            cmd: "all"
                        }, function (status) {
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_set() {
            tt.ylStoreValue({
                name: "testSet",
                cmd: "add",
                args: "12"
            }, function (status) {
            }.bind(this));
            tt.ylStoreValue({
                name: "testSet",
                cmd: "add",
                args: "10"
            }, function (status) {
                tt.ylStoreValue({
                    name: "testSet",
                    cmd: "exist",
                    args: "10"
                }, function (status) {
                }.bind(this));
                tt.ylStoreValue({
                    name: "testSet",
                    cmd: "size"
                }, function (status) {
                    tt.ylStoreValue({
                        name: "testSet",
                        cmd: "del",
                        args: "10"
                    }, function (status) {
                        tt.ylStoreValue({
                            name: "testSet",
                            cmd: "all"
                        }, function (status) {
                            tt.ylStoreValue({
                                name: "testSet",
                                cmd: "replace",
                                args: "[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                            }, function (status) {
                                tt.ylStoreValue({
                                    name: "testSet",
                                    cmd: "all"
                                }, function (status) {
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_hash() {
            tt.ylStoreValue({
                name: "testHash",
                cmd: "set",
                args: "u_name,许"
            }, function (status) {
                tt.ylStoreValue({
                    name: "testHash",
                    cmd: "get",
                    args: "u_name"
                }, function (status) {
                    tt.ylStoreValue({
                        name: "testHash",
                        cmd: "replace",
                        args: "{\"u_name\":\"唐\",\"sex\":\"women\"}"
                    }, function (status) {
                        tt.ylStoreValue({
                            name: "testHash",
                            cmd: "gets",
                            args: "u_name,sex"
                        }, function (status) {
                        }.bind(this));
                        tt.ylStoreValue({
                            name: "testHash",
                            cmd: "size",
                        }, function (status) {
                        }.bind(this));
                        tt.ylStoreValue({
                            name: "testHash",
                            cmd: "values",
                            args: "sex"
                        }, function (status) {
                        }.bind(this));
                        tt.ylStoreValue({
                            name: "testHash",
                            cmd: "del",
                            args: "u_name"
                        }, function (status) {
                            tt.ylStoreValue({
                                name: "testHash",
                                cmd: "all",
                            }, function (status) {
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
                }.bind(this));
            }.bind(this));
        }
        test_sv_radom() {
            tt.ylStoreValue({
                name: "testRandom"
            }, function (status) {
            }.bind(this));
        }
    }

    class GameConfig {
        constructor() {
        }
        static init() {
            var reg = Laya.ClassUtils.regClass;
            reg("script/GameUI.ts", GameUI);
        }
    }
    GameConfig.width = 640;
    GameConfig.height = 1136;
    GameConfig.scaleMode = "fixedwidth";
    GameConfig.screenMode = "none";
    GameConfig.alignV = "top";
    GameConfig.alignH = "left";
    GameConfig.startScene = "test/TestScene.scene";
    GameConfig.sceneRoot = "";
    GameConfig.debug = false;
    GameConfig.stat = false;
    GameConfig.physicsDebug = false;
    GameConfig.exportSceneToJson = true;
    GameConfig.init();

    class Main {
        constructor() {
            if (window["Laya3D"])
                Laya3D.init(GameConfig.width, GameConfig.height);
            else
                Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
            Laya["Physics"] && Laya["Physics"].enable();
            Laya["DebugPanel"] && Laya["DebugPanel"].enable();
            Laya.stage.scaleMode = GameConfig.scaleMode;
            Laya.stage.screenMode = GameConfig.screenMode;
            Laya.stage.alignV = GameConfig.alignV;
            Laya.stage.alignH = GameConfig.alignH;
            Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
            if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
                Laya.enableDebugPanel();
            if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
                Laya["PhysicsDebugDraw"].enable();
            if (GameConfig.stat)
                Laya.Stat.show();
            Laya.alertGlobalError = true;
            Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
        }
        onVersionLoaded() {
            Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
        }
        onConfigLoaded() {
            GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
        }
    }
    new Main();

}());
