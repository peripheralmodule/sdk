declare namespace tt {
  /************************************************************* */
  /*****************      影流SDK-字节跳动      ***************** */
  /************************************************************* */

     /**
     * 初始化SDK
     *
     * @param _callback   
     *  返回参数说明：
     *  date true:初始化成功、false:初始化失败、
     *      'config_success'：获取体力配置成功、'config_fail':获取体力配置失败(2.1.3版本接口增加的回调参数)
     *
     **/
    export function ylInitSDK(callback:Function):void;

    /**
     * 结果统计
     * @param detail 用户该局游戏的技能及分数统计详情（每个游戏需要统计的数据不同)
     *               例如，泡泡龙大师：{"total_score": 123 , "rebirth_score": 123, "strategy": "1", "change_score": [123, 123...],"hammer_score": [123, 123, ...]}
     *               total_score 用户在游戏中一句的中分数
     *               rebirth_score 用户在一句中使用复活时的分数
     *               strategy 当前使用的策略，比如："1"
     *               change_score 换一换功能每一次使用时获得的分数
     *               hammer_score 使用锤子功能每一次使用时获得的分数
     * @param _callback
     *
     */
    export function ylStatisticResult(_detail:any,callback:Function):void;
    /**
     * 视频统计(打点统计）
     * @param _type 类型[0:显示视频,1:播放完成]
     * @param _adId 视频广告ID
     * @param _callback
     *
     */
    export function ylStatisticViedo(_type:any,_adId:any,callback:Function):void;
    /**
     * 获取分享图列表
     * @param _callback
     * @param scene 获取分享图的场景，该值需要与策划在后台管理系统配置的值保持一致,如果不传默认拉取所有场景的分享图
     *
     *
     * 返回参数说明:
     * 参数                   类型                  说明
     * result     JSON数组             分享卡片数据，详细数据见下
     *  |
     *  result:
     *   参数      类型      说明
     *   title   String  分享文案
     *   img     String  图片url
     *   id      Integer 分享卡片id
     */
    export function ylShareCard(callback:Function,scene:string):void;
    
    /**
     * 分享图统计（打点统计）
     * @param {*} share_card_id 分享图id 
     * @param {*} sType [ 0:分享图,1:视频分享,2:口令分享 ]
     * @param {*} real  [-1:分享-取消,0:分享-不确定,1:分享-确定]
     */
    export function ylStatisticShareCard(share_card_id:number,sType:number,real:number):void;
    /**
       * 获取视频分享策略
       * @param channel 渠道名
       * @param model   模块名
       * @param callback 回调函数 返回值:[0:无策略,1:分享,2:视频] 
      **/
      export function ylShowShareOrVideo(channel:String,module:String,callback:Function):void;
      /**
       * 使用视频分享策略
       * @param channel 渠道名
       * @param model   模块名
       * @param callback 回调函数 返回值:[0:无策略,1:分享,2:视频] 
      **/
      export function ylUseShareOrVideoStrategy(channel:String,module:String,callback:Function):void;
    /**
     * 事件统计
     * @param        参数         类型          说明 
                    eventName   String         事件名
                    scene       String         所在场景名称
     *
     */
    export function ylEventCount(eventName:String,scene:String):void;

    /**
     * 获取游戏自定义配置
     * @param callback
     *
     * 返回参数说明：
     * 参数                类型           说明
     *  result     JSON数组  游戏配置列表，详细数据见下
     *       |
     *       result:
     *           参数      类型          说明
     *           _id     Integer     ID
     *           name    String      参数名(尽量用英文，用以前端区分不同功能)
     *           type    String      类型[1:值配置,2:开关配置]
     *           value   String      参数值，type=1时，值为CP后台配置的值，type=2时值为开关状态:["0":关闭，"1":打开]
     *           desc    String      参数描述
     */
    export function ylGetCustom(callback:Function):void;


    /**
     * 获取游戏APPID
    **/
    export function ylGetAppID ():any;
    /**
    * 获取邀请账号
    **/
    export function ylGetInviteAccount():any;
    /**
    * 获取用户SDK服务器账号信息
    **/
    export function ylGetUserInfo():any;
    /**
    * 获取开关信息
    **/
    export function ylGetSwitchInfo():any;
    /**
    * 获取玩家头条账号信息
    **/
    export function ylGetUserTTinfo():any;
    
    /**
    * 创建banner广告
    * @param show  是否显示: [true:是,false:否]默认：false
    * @param _callback 
    * @param misToch 是否为误触banner: [true:是,false:否]默认：false
    **/
    export function ylBannerAdCreate(show:Boolean, _callback:Function, misToch:Boolean):void;
    /**
    * 创建banner自定义style
    *
    * @param style banner的Style，具体传值请参考平台官方 
    * @param show  是否显示: [true:是,false:否]默认：false
    * @param _callback 
    * @param misToch 是否为误触banner: [true:是,false:否]默认：false
    * @param _onResize onResize回调，返回onResize返回参数
    **/
    export function ylBannerAdCreateByStyle(style:any, show:Boolean, _callback:Function, misToch:Boolean,_onResize:Function):void;
    /**
    * 修改Banner的style
    *
    * @param style banner的Style，具体传值请参考平台官方 
    **/
   export function ylChangeBannerStyle(style:any):void;
    /**
    * 显示banner广告
    **/
    export function ylBannerAdShow():void;
    /**
    * 隐藏 banner广告
    **/
    export function ylBannerAdHide():void;
    /**
    * 创建视频广告
    **/
    export function ylCreateVideoAd():void;
    /**
    *    播放视频广告
    *    
    *    VIDEO_PLAY_FAIL:0    //视频广告-播放失败
    *    VIDEO_PLAY_FINISH:1  //视频广告-播放完成
    *    VIDEO_PLAY_CANCEL:2  //视频广告-播放取消
    * @param callback
    * @param unlockCustomNum 解锁关卡
    *
    **/
    export function ylShowVideoAd(callback:Function,unlockCustomNum:number):void;
    /**
    *    播放视频广告
    *    
    *    VIDEO_PLAY_FAIL:0    //视频广告-播放失败
    *    VIDEO_PLAY_FINISH:1  //视频广告-播放完成
    *    VIDEO_PLAY_CANCEL:2  //视频广告-播放取消
    *
    * @param info 参数对象
    *       参数           必选     类型       说明
    *      callBack        True    Function   接口回调
    *      unlockCustomNum False   Integer    需要视频解锁的关卡
    *      getPower        False   Boolean    看完视频是否获得体力
    *
    *
    **/
    export function ylShowVideoAd2(info:any):void;
    /**
      * 创建插屏广告
      *   @param show 是否展示 [true:展示,false:不展示]
      *   @param callback 回调函数 [type:0:创建或展示失败、1:创建或展示成功、2:关闭]
    **/
    export function ylCreateInterstitialAd(show:Boolean, callback:Function):void;
    /**
    * 显示插屏广告
    **/
    export function ylShowInterstitialAd():void;
    /**
    * 日志输出 可以使用SDK开关统一控制是否展示日志
    * 
    * @param logMsg  日志信息 String
    * @param logType 日志类型 String ['log'、'info'、'error'、'warn'、'debug']
    **/
    export function ylLog(logMsg:string,logType:string):void;
    // /**
    // * 获取与头条胶囊按钮对齐的信息
    // **/
    // export function ylGetLeftTopBtnPosition():any;

    /**
    * 分享-图片
    * 
    * @param callback
    * @param scene  获取分享图的场景，该值需要与后台管理系统配置的值保持一致
    **/
    export function ylShareImage(callback:Function, scene:string):void;
    /**
    * 分享-视频
    * 
    * @param info	JSON对象	token参数,详解下表
    *           info:
    *                参数	 必选	    类型	说明
    *                title	False	String	分享标题,不超过 14 个中文字符
    *                desc	False	String	分享文案,不超过 28 个中文字符
    *                query	False	String	查询字符串，必须是 key1=val1&key2=val2 的格式。从这条转发消息进入后，可通过 tt.getLaunchOptionSync() 或 tt.onShow() 获取启动参数中的 query。
    *                extra	True	JSON对象	token参数,详解下表
    *                  |
    *                       参数	     类型	    说明
    *                   withVideoId	    boolean	    是否支持跳转到播放页， 1.40.0+支持
    *                   videoTopics	    array	    视频话题(只在抖音可用)
    *                   createChallenge	boolean	    是否分享为挑战视频 (头条支持)
    * 
    * @param callback
    **/
    export function ylShareVideo(info:any,callback:Function):void;
    /**
        * 跳转到分享的视频播放页面
        * 
        * @param 	
        *            参数	    必选	  类型	    说明
        *            videoId	True	String	  分享视频返回的ID
        * 
        * @param callback
    **/
    export function ylNavigateToVideoView(videoId:string,callback:Function):void;
    /**
    * 分享-Token(含模板)
    * 
    * @param info  JSON对象	token参数,详解下表
    *           info:
    *           参数	必选	类型	说明
    *            title	True	String	分享标题,不超过 14 个中文字符
    *            desc	True	String	分享文案,不超过 28 个中文字符
    *            query	False	String	查询字符串，必须是 key1=val1&key2=val2 的格式。从这条转发消息进入后，可通过 tt.getLaunchOptionSync() 或 tt.onShow() 获取启动参数中的 query。
    *
    * @param callback
    **/
    export function ylShareToken(info:any,callback:Function):void;
    /**
    * 分享-模板
    * 
    * @param callback
    **/
    export function ylShareTemplate(callback:Function):void;
    /**
    * 更多游戏弹窗
    * 
    * @param callback
    **/
    export function ylShowMoreGamesModal(callback:Function):void;
    /**
    * 创建更多游戏按钮
    * @param info   JSON对象	详情见下
    *           info:
    *               参数	 必选	  类型	        说明
    *                type	True	String	    按钮的类型，取值 image 或 text。image 对应图片按钮，text 对应文本按钮
    *                image	False	String	    按钮的背景图片，type 为 image 时必填。仅支持本地图片，目录包括代码包目录、临时文件目录和本地用户目录
    *                style	True	JSON对象	 按钮的样式(详见字节跳动t.createMoreGamesButton文档)
    *
    * @param callback
    **/
    export function ylcreateMoreGamesButton(info,callback:Function):void;
    /**
    * 获取录屏状态,录屏状态[0:无,1:录屏中,2:暂停]
    **/
    export function ylGetRecorderStatus():any;
    /**
    * 获取录屏资源保存地址
    **/
    export function ylGetVideoPath():any;
    /**
    * 开始录屏
    * 
    * @param duration:录屏的时长，单位 s，必须大于 3s，最大值 300s（5 分钟）
    * @param callback
    **/
    export function ylRecorderStart(duration:any,callback:Function):void;
    /**
    * 停止录屏
    **/
    export function ylRecorderStop():void;
    /**
    * 暂停录屏
    **/
    export function ylPause():void;
    /**
    * 继续录屏
    **/
    export function ylResume():void;

    /**
    * 自定义空间
    * @param info   对象 自定义空间存取参数(具体请查看API文档)
    *           ｜
    *           info:
    *              参数       类型          说明
    *              name      string        空间名称(需与后台配置的一致)
    *              cmd       string        空间操作指令
    *              args      string        操作的值
    *
    *   @param _callback 操作结果返回键听
    *
    *
    *   说明：
    *   name: 由后台配置，该名称决定查找后台配置其对应的类型，
    *         类型有：字符变量、字符数组、字符集合、字符散列、随机数
    *
    *   cmd:  根据name对应的类型，有不的操作指令
    *          字符变量：[
                            get:取值，返回后台配置的默认值或set的值、
                            set:赋值，返回后台是否操作成功 true false
                        ]
    *          字符数组：[
                            all:取值, 返回全部值的数组、
                            get:取值,返回 args 定义下标的值、
                            add:赋值,添加，返回后台是否操作成功 true false、
                            set:赋值,替换 args 为需要赋予的下标和值用逗号"," 分割，例如："2,text"，返回后台是否操作成功 true false、
                            replace:替换， args 为需要赋予的json字符数组、例如："[\"1\",\"2\",\"3\",\"4\",\"5\"]"，返回后台是否操作成功 true false
                            size:取值，返回该数组的值数量、
                            poll:取值，返回该数组的第一个值并从后台删除
                        ]
    *          字符集合：[
                            all：取值，返回全部值的数组
                            exist：取值，args 为需要判断的值，返回该值是否存在 true false
                            add：赋值，args 为需要添加的值，返回后台是否操作成功 true false
                            replace：替换，args 为需要赋予json字符数组，例如： "[\"1\",\"2\",\"3\",\"4\",\"5\"]"，返回后台是否操作成功 true false
                            size：取值操作 value 返回该集合的值数量
                            del：赋值， args 为需要删除的值，返回后台是否操作成功 true false
                        ]
    *          字符散列：[
                            all：取值，返回全部值的散列
                        get：取值，args 为需要获取的键，返回该键对应的值
                        gets：取值，args 为需要获取的键列表用逗号"," 分割 ，例如："key1,key2,key3"，返回全部值的数组
                        values：取值，返回全部值的数组
                        set：赋值，args 为需要赋予的键值用逗号"," 分割 ，例如："key,value"，返回后台是否操作成功 true false
                        replace：替换，args 为需要赋予json字符对象 例如："{\"key1\":\"value1\",\"key2\":\"value2\"}"，返回后台是否操作成功 true false
                        size：取值，返回该散列的值数量
                        del：赋值，args 为需要删除的键，返回后台是否操作成功 true false
                        ]
    *          随机数：无
        args：
    *           字符变量：[
                            get:无
                            set:需要赋予的值
                        ]
    *          字符数组：[
                            all:无
                            get:数组下标
                            add:需要添加的值、
                            set:需要赋予的下标和值用逗号"," 分割，例如："2,text"
                            replace:需要赋予的json字符数组、例如："[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                            size:无
                            poll:无
                        ]
    *          字符集合：[
                            all：无
                            exist：需要判断的值
                            add：需要添加的值
                            replace：需要赋予json字符数组，例如： "[\"1\",\"2\",\"3\",\"4\",\"5\"]"，
                            size：无
                            del：为需要删除的值，
                        ]
    *          字符散列：[
                        all：无
                        get：需要获取的键
                        gets：需要获取的键列表用逗号"," 分割 ，例如："key1,key2,key3"
                        values：无
                        set：需要赋予的键值用逗号"," 分割 ，例如："key,value"
                        replace：需要赋予json字符对象 例如："{\"key1\":\"value1\",\"key2\":\"value2\"}"
                        size：无
                        del：需要删除的键，
                        ]
    *          随机数：无

    *   callback 返回：
    *          字符变量：[
                            get:返回后台配置的默认值或set的值、
                            set:返回后台是否操作成功 true false
                        ]
    *          字符数组：[
                            all:返回全部值的数组、
                            get:返回 args 定义下标的值、
                            add:返回后台是否操作成功 true false、
                            set:返回后台是否操作成功 true false、
                            replace:返回后台是否操作成功 true false
                            size:返回该数组的值数量、
                            poll:返回该数组的第一个值并从后台删除
                        ]
    *          字符集合：[
                            all：返回全部值的数组
                            exist：返回该值是否存在 true false
                            add：返回后台是否操作成功 true false
                            replace：返回后台是否操作成功 true false
                            size：返回该集合的值数量
                            del：返回后台是否操作成功 true false
                        ]
    *          字符散列：[
                            all：返回全部值的散列
                            get：返回该键对应的值
                            gets：返回全部值的数组
                            values：返回全部值的数组
                            set：返回后台是否操作成功 true false
                            replace：返回后台是否操作成功 true false
                            size：返回该散列的值数量
                            del：返回后台是否操作成功 true false
                        ]
    *          随机数：返回同样长度的随机数
    *
    **/
    export function ylStoreValue(info:any,callback:Function):void;
    /**
     * 切换页面
     * SDK将根据云控banner定时刷新配置，和当前状态判断是否强制刷新banner
     * @param misToch 是否为误触banner: [true:是,false:否]默认：false
     */
    export function ylChangeView(misToch:Boolean):void;
    /**
     * 获取深度误触屏蔽开关状态
     *
     * @param customNum 关卡数
     * 
     * 响应结果:
     * {
     *   "deepTouch":  "1", // "0":误触开,"1":误触关,
     *   "customInfo":[
     *       {
     *       "_id": 1382,   //ID
     *       "name": "dawd",//参数名(后台配置的)
     *       "type": "2",  //类型[1:值配置,2:开关配置]
     *       "value": "0", //值，地区屏蔽状态type==1时value值为空串，type=2时value值为['0':关，'1':开]请CP做好判断
     *       "desc": ""    //参数描述
     *       }
     *   ]
     * }
     */
    export function ylGetDeepTouch(customNum):any;
    /**
     * 一局游戏结束
     * 
     * @param customNum 当前关卡数
     */
    export function ylOverGame(customNum:number):void;
    /**
     * 视频解锁
     * 
     * @param customNum 当前关卡数
     * 
     * 响应结果:
     *  unlockStatus  //是否可以视频解锁[true:是,false:否]
     */
    export function ylVideoUnlock(customNum:number):Boolean;
    /**
     * 获取体力相关信息
     *
     * @param callback 回调函数
     *
     * 响应结果:
     *   {
     *       "defaultPower": 5,     //初始体力
     *       "powerUpper": 5,       //体力上限
     *       "recoveryTime": 300,   //体力自动恢复时间
     *       "getPower": 1          //看视频获得体力值
     *   }
     */
    export function ylGetPowerInfo(callback:Function);
    /**
     * 体力变化监听
     * @param callback 回调函数
     * 
     * 回调返回: type[1:视频,2:定时自动恢复,3:调用ylSetPower]
     */
    export function ylOnPowerChange(callback:Function):void;
    /**
     * 设置体力
     * 
     * @param powerNum 当前体力值
     */
    export function ylSetPower(powerNum:number):void;
    /**
     * 获取体力
     * 
     * 响应结果:
     * powerNum  当前体力值
     */
    export function ylGetPower():number;
    /***
       * 生命周期监听
       * 
       * @param _callback
       * 
       * 参数返回说明:         
       *  参数名称       参数类型          说明     
       *  type            String     'onShow'、'onHide'、'onError'三个状态
       *  res             any         onShow和onError为回调返回参数，onHide是为空
    */
    export function ylGameLifeListen(callback:Function):void;
    /**
     *  玩家玩过的关卡数
     * @param type 获取类型[1:最后一次玩的关卡，2:总玩过的最大关卡]
     **/
    export function ylGetPlayCustom(type:number):number;
    /**
     *  获取玩家玩游戏的局数
     * @param type 获取类型[1:今天玩的局数，2:总共玩的局数]
     **/
    export function ylGetPlayCount(type:number):number;
     /**
     *  获取玩家今天看视频的次数
     *
     *  
     **/
    export function ylGetWatchTvNum():number;
        /***
      * 是否可以发放奖励
      * 如果可以,SDK将自动扣除一次发放奖励次数
      *
      * @param isVideo [true:视频，false:分享]
      *
      * 返回: [true:可以,false:不能(今天已达上限)]
    **/
    export function ylRewardByVideoOrShare(isVideo:Boolean):Boolean;
    /***
      * 获取分享/视频发奖次数
      *
      *
      * 返回参数：
      *          参数     类型     说明
      *          vsLimit  Object   获取分享/视频发奖次数信息对象
      *             |
      *             参数             类型      说明
      *             videoFalseLimit Integer 剩余视频发奖次数
      *             shareFalseLimit Integer 剩余分享发奖次数
      *             lastDate        Integer 最近重置时间
      *
    **/
    export function ylGetVSLimit():any;
    /**
     * 获取流失统计列表
     * 
     * @param callback 
     */
    export function ylGetLayerList(callback:Function):void;
    /**
     * 流失统计埋点
     * 
     * @param layerPath 路径
     */
    export function ylStatisticLayer(layerPath:string):void;
    /**
     * 获取服务端信息
     * 
     * @param callback 
     */
    export function ylGetServerInfo(callback:Function):void;
}