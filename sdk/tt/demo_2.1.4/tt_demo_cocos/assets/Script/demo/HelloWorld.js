cc.Class({
    extends: cc.Component,

    properties: {
        label:cc.Label,
        lbTime:cc.Label,
        btnShare:cc.Node,               //分享按钮
        btnSideBox:cc.Node,             //侧边栏列表按钮

        pbYlGameOver:cc.Prefab,         //游戏结束界面
        pbSign:cc.Prefab,               //签到界面
        pbGetAward:cc.Prefab,           //获得奖励View
        pbBreakEgg:cc.Prefab,           //砸金蛋
        pbOpenBox:cc.Prefab,            //开宝箱
        pbShakeTree:cc.Prefab,          //摇钱树
        lbRecordStartOrStop:cc.Label,   //开始或停止录屏
        lbRecordResumeOrPasue:cc.Label, //继续或暂停录屏
        lbRecordTips:cc.Label,          //录屏提示
        nodeRecordResumeOrPasue:cc.Node,   //继续或暂停录屏
        dtCount:0,
        Tcount:0,
    },
    onLoad: function () {
        this.initEvent();
        this.initWxOnshow();
        tt.ylSetCurScene("MainScene");//一定记得在每个场景都要设置当前场景
        tt.ylOnPowerChange(function(type){
            console.warn("HelloWorld-ylOnPowerChange back[1:视频,2:定时自动恢复]:",type);
        }.bind(this));
        tt.ylInitSDK(function(success){
            if(success === true){
                console.warn("HelloWorld-ylInitSDK-初始化成功");
                this.doSDKinterface();
            }else if(success == 'config_success' || success == 'config_fail'){
                console.warn("HelloWorld-ylInitSDK-获取游戏配置回调");
                tt.ylGetPowerInfo(function(data){
                    //干点什么
                    console.log("HelloWorld-ylGetPowerInfo:",data);
                });
            }else{
                console.warn("HelloWorld-ylInitSDK-初始化失败");
            }            
        }.bind(this));

        let btnInfo = tt.ylGetLeftTopBtnPosition();
        tt.ylLog("----btnInfo:",JSON.stringify(btnInfo));
        this.btnSideBox.width = btnInfo.width;
        this.btnSideBox.height = btnInfo.height;
        this.btnSideBox.y = btnInfo.y;
        this.btnSideBox.x = btnInfo.x+this.btnSideBox.width/2;
        this.nodeRecordResumeOrPasue.active = false;
        this.lbRecordTips.string = '录屏-无';
        this.lbRecordStartOrStop.string = '录屏';
        this.lbRecordResumeOrPasue.string = '继续';
        tt.ylEventCount("event-text");
    },

    /**
     * 初始化事件
     **/
    initEvent(){
        cc.director.on(window.Global.EVENT_SHOW_GET_AWARD, this.onShowGetAward.bind(this)); 
    },

    /**
    * 调用SDK接口
    **/
    doSDKinterface(){
        tt.ylLog("---调用接口---");
        let _width = 150; 
        var sys = tt.getSystemInfoSync();
        let _left = (sys.windowWidth - _width) / 2;
        let moreGameInfo =  {
              type: "image",
              image: "images/sp_btn_start.png",
              style: {
                left: _left,
                top: sys.windowHeight-250,
                width: _width,
                height: 40,
                lineHeight: 40,
                backgroundColor: "#ff0000",
                textColor: "#ffffff",
                textAlign: "center",
                fontSize: 16,
                borderRadius: 4,
                borderWidth: 1,
                borderColor: "#ff0000"
              },
         };
        // tt.ylcreateMoreGamesButton(moreGameInfo);
        tt.ylGetCustom(function(data){
        }.bind(this));
        tt.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status){
            if(status){
                //干点什么
            }
        }.bind(this));
        tt.ylGetUserTTinfo(function(data){
            console.log("----userTTinfo:",JSON.stringify(data));
        });//先获取本地缓存，没有在弹窗通过用户授权获取
        let loginInfo = tt.ylGetUserInfo();//获取登录信息，包括微信oppid和code(code只有第一次登录才能取到)
        console.log("---登录信息-loginInfo:",JSON.stringify(loginInfo));
        tt.ylBannerAdCreateByStyle({
            top: 100,
            left: 20,
            width: 130,
        },true,function(){
            
        },true);

        var that = this;
        /**
         * 获取分享图列表
         */
        tt.ylShareCard(function (shareInfo) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
                this.btnShare.active = true;
            }else{
                //获取失败
            }
        }.bind(this));
        tt.ylStatisticViedo(0,'adunit-6878a73e134f85e2');
        tt.ylStatisticShareCard(177);
        this.testStoreValue();
        let appid = tt.ylGetAppID();
        console.log("---ylGetAppID:",appid);
        tt.ylGetSignData(function(data){
            console.log("---ylGetSignData:",data);
        });
        tt.ylIntegralWall(function(data){

        });
        let inviteAccount = tt.ylGetInviteAccount();
        console.log("---ylGetInviteAccount:",inviteAccount);
        let data = tt.ylGetSwitchInfo();
        tt.ylLog("---刷新开关状态---",data);
    },

    //显示获得奖励对话框
    onShowGetAward(evt){
        let data = evt.getUserData();
        if(!this.nodeGetAward) this.nodeGetAward = cc.instantiate(this.pbGetAward);
        this.nodeGetAward.x = 0;
        this.nodeGetAward.y = 0;
        this.nodeGetAward.zorder = 20;
        this.nodeGetAward.parent = this.node;
        this.nodeGetAward.getComponent('ylGetAward').showView(data);
    },

    //砸金蛋
    onClickBreakEgg(){
        if(!this.nodeBreakEgg) this.nodeBreakEgg = cc.instantiate(this.pbBreakEgg);
        this.nodeBreakEgg.parent = this.node;
        this.nodeBreakEgg.getComponent('ylBreakEgg').showView();
    },
    //开宝箱
    onOpenBox(){
        if(!this.nodeOpenBox) this.nodeOpenBox = cc.instantiate(this.pbOpenBox);
        this.nodeOpenBox.parent = this.node;
        this.nodeOpenBox.getComponent('ylOpenBox').showView();
    }, 
    //摇钱树
    onShakeTree(){
        if(!this.nodeShakeTree) this.nodeShakeTree = cc.instantiate(this.pbShakeTree);
        this.nodeShakeTree.parent = this.node;
        this.nodeShakeTree.getComponent('ylShakeTree').showView();
    }, 
    //视频分享策略
    onShowShareOrVideo(){
        tt.ylShowShareOrVideo("chanel_2","model_2",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    break;
                case 2:
                    console.warn("------策略-视频");
                    break;
            }
        }.bind(this));
    },
    //显示获胜结算对话框
    onShowOverWin(){
        this.onShowGameOver(true);  
    },
    //显示失败结算对话框
    onShowOverFail(){
        this.onShowGameOver(false);  
    },
    onShowGameOver(isWin){
        let obj = {
                hasWin:isWin,                    
                custom:2,
                golds:1000,
                extra:[
                    {id:0,url:"https://ydhwimg.szvi-bo.com/wx535481e2c3ff79bc/sidebox/11568198322531.jpg",name:"道具补充",getType:"video"},
                    {id:1,url:"https://ydhwimg.szvi-bo.com/wx535481e2c3ff79bc/sidebox/11569753241448.png",name:"钻石奖励",getType:"video"},
                    {id:2,url:"https://ydhwimg.szvi-bo.com/wx535481e2c3ff79bc/sidebox/21568971486805.png",name:"全体回复",getType:"share"}
                ]
            };
        if(!this.nodeYlGameOver) this.nodeYlGameOver = cc.instantiate(this.pbYlGameOver);
        this.nodeYlGameOver.x = 0;
        this.nodeYlGameOver.y = 0;
        this.nodeYlGameOver.parent = this.node;
        this.nodeYlGameOver.getComponent('ylGameOver').showView(obj);//结束对话框
    },
    onShowInterstitialAd(){
        tt.ylCreateInterstitialAd(true);
    },

    //注册小游戏回到前台的事件监听
    initWxOnshow(){
        var that = this;
        tt.ylLog("---注册小游戏回到前台的事件监听---",'log')
        tt.onShow((res) => {
            tt.ylLog("---小游戏回到前台的事件监听:",'log')
        })
    },
    showMoreGame(){
        tt.ylShowMoreGamesModal();
    },
    update(dt){
        this.dtCount += dt;
        if(this.dtCount >= 1){
            this.Tcount += 1;
            this.dtCount = 0;
            this.lbTime.string = ""+this.Tcount;
        }
    },
    //展示视频
    onShowVideo(){
        tt.ylCreateVideoAd();
        tt.ylShowVideoAd(function(type){
            switch(type){
                case 0:
                console.warn("onShowVideo 视频广告-播放失败");
                break;
                case 1:
                console.warn("onShowVideo 视频广告-播放完成");
                break;
                case 2:
                console.warn("onShowVideo 视频广告-播放取消");
                break;
            }
        },2);
    },
    //显示banner
    onShowBanner(){
        tt.ylBannerAdCreate(false, function(status){
            if(status){
                console.log("HelloWorld-ylBannerAdCreate-status:",status);
                //创建成功
                tt.ylBannerAdShow();
            }else{
                //创建失败
            }
        }, true);
        // tt.ylBannerAdCreateByStyle({
        //     left: 20,
        //     top: 100,
        //     width: 80,
        // }, true, function(res){}, true);
    },
    onHideBanner(){
        tt.ylBannerAdHide();
    },
    //游戏结束
    onOver(){
        this.custom +=1;
        tt.ylOverGame(this.custom);
    },
    //获取深度误触开关
    onGetDeepTouch(){
       let info = tt.ylGetDeepTouch(this.custom);
       console.log("onGetDeepTouch-info: ",JSON.stringify(info));
    },
    //视频解锁关卡
    onVideoUnlockCustoms(){
        let unlock = tt.ylVideoUnlock(this.custom);
        console.warn("HelloWorld-onVideoUnlockCustoms-是否解锁:",unlock);
    },
    //消耗体力
    onConsumePower(){
       let power =  tt.ylGetPower();
       tt.ylSetPower(power -1);
    },
    onAddPower(){
        let power =  tt.ylGetPower();
        tt.ylSetPower(power +1);
    },
    //切换界面,可调用强制刷新Banner接口
    onChangeView(){
        tt.ylChangeView();
    },
    //显示插屏广告
    onShowInterstitialAd(){
        console.log("HelloWorld-onShowInterstitialAd");
        tt.ylCreateInterstitialAd(true,function(res){
            // [success:0:创建或展示失败、1:创建或展示成功、2:关闭]
            switch(res){
                case 0:
                console.log("ylCreateInterstitialAd-创建或展示失败");
                break;
                case 1:
                console.log("ylCreateInterstitialAd-创建或展示成功");
                    tt.ylShowInterstitialAd();
                break;
                case 2:
                console.log("ylCreateInterstitialAd-关闭");
                break;
            }
            console.warn("HelloWorld-ylCreateInterstitialAd-res:",res);
        });
    },

    //开始或停止录屏
    onRecordStartOrStop(){
        let _status = tt.ylGetRecorderStatus();
        console.log("-------------onRecordStartOrStop--------_status:",_status);
        if(_status == 0){
            //duration:录屏的时长，单位 s，必须 >3 &&  <= 300s（5 分钟）
            let duration = 200;
            tt.ylRecorderStart(duration,function(status){
                console.log("-------------录屏--------status:",status);
                switch(status){//录屏状态[0:无,1:录屏中,2:暂停]
                    case 0:
                        this.nodeRecordResumeOrPasue.active = false;
                        this.lbRecordStartOrStop.string = '录屏';
                        this.lbRecordResumeOrPasue.string = '继续';
                        this.lbRecordTips.string = '录屏-无';
                        let videoPath = tt.ylGetVideoPath();
                        console.log("---videoPath:",videoPath);
                    break;
                    case 1:
                        this.nodeRecordResumeOrPasue.active = true;
                        this.lbRecordStartOrStop.string = '停止';
                        this.lbRecordResumeOrPasue.string = '暂停';
                        this.lbRecordTips.string = '录屏-录屏中';
                    break;
                    case 2:
                        this.lbRecordStartOrStop.string = '停止';
                        this.lbRecordResumeOrPasue.string = '继续';
                        this.lbRecordTips.string = '录屏-暂停';
                    break;
                }
            }.bind(this));
        }else if(_status == 1){
            tt.ylRecorderStop();
        }
    },
    //继续或暂停录屏
    onRecordResumeOrPasue(){
        let status = tt.ylGetRecorderStatus();
        console.log("-------------onRecordResumeOrPasue--------status:",status);
        if(status && status == 1){
            tt.ylPause();
        }else if(status && status == 2){
            tt.ylResume();
        }
    },
    onShareVideo(){
        let info = {
              title: "测试分享视频",
              desc: "测试描述",
              query: "",
              extra: {
                videoTopics: ["话题1", "话题2"]
              },
        };
        tt.ylShareVideo(info,function(status){
            console.log("-----ylShareVideo----分享结果:",status);
        });
    },
    onShareImage(){
        tt.ylShareImage(function(status){
            console.log("-----ylShareImage----分享结果:",status);
        });
    },
    onShareToken(){
        let info = {
           title: "测试分享Token",
           desc: "测试描述",
           query: "",
        }
        tt.ylShareToken(info,function(status){
            console.log("-----ylShareToken----分享结果:",status);
        });
    },
    onShareTemplate(){
        tt.ylShareTemplate(function(status){
            console.log("-----ylShareTemplate----分享结果:",status);
        });
    },
    //测试自定义空间值
    testStoreValue(){
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    },
    test_sv_string(){
        //String
        tt.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    },
    test_sv_list(){
        //List
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 tt.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    tt.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    tt.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            tt.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    },
    test_sv_set(){
        //Set
        tt.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                tt.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        tt.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                tt.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        tt.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            tt.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    },
    test_sv_hash(){
        //litMap
        tt.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        tt.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                            },
                            function(status){
                                
                            }.bind(this));
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                tt.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    },
    test_sv_radom(){
        //testRandom
        tt.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    },
});
