/**
 * 游戏结束
 **/
cc.Class({
    extends: cc.Component,
    properties: {
        nodeTitle:cc.Node,
        nodeCustom:cc.Node,
        nodeLine:cc.Node,
        nodeGolds:cc.Node,
        nodeJiangli:cc.Node,
        nodeRefuse:cc.Node,
        ndoeText:cc.Node,
        nodeCover_1:cc.Node,
        nodeCover_2:cc.Node,
        nodeCover_3:cc.Node,
        nodeFrame_1:cc.Node,
        nodeFrame_2:cc.Node,
        nodeFrame_3:cc.Node,
        ndoeResult_1:cc.Node,
        ndoeResult_2:cc.Node,
        ndoeResult_3:cc.Node,
        nodeBg:cc.Node,
        lbCustom:cc.Label,
        lbGolds:cc.Label,
        btnContinue:cc.Node,
        sfTitle:[cc.SpriteFrame],
        sfArrow:[cc.SpriteFrame],
        spTitle:cc.Sprite,
        spArrow:[cc.Sprite],
    },

    showView(obj){
        this._obj = obj;
        this.nodeBg.opacity = 0;
        this.nodeBg.runAction(cc.sequence(cc.fadeTo(0.25,180),cc.callFunc(function(){
            this.runActonStep_1();
        },this,0)));
        this.setData();
    },
    setData(){
        this.lbCustom.string = "第 "+this._obj.custom+" 关";
        this.lbGolds.string = (this._obj.golds>=0 ? "+" : "-")+""+this._obj.golds
        let that = this;
        if(this._obj.extra.length>0){
            this.ndoeResult_1.getComponent('ylOverExtraItem').setInfo(this._obj.extra[0],function(){
                that.onContinue();
            });
        }
        if(this._obj.extra.length>1){
            this.ndoeResult_2.getComponent('ylOverExtraItem').setInfo(this._obj.extra[1],function(){
                that.onContinue();
            });
        }
        if(this._obj.extra.length>2){
            this.ndoeResult_3.getComponent('ylOverExtraItem').setInfo(this._obj.extra[2],function(){
                that.onContinue();
            });
        }
        let size = this.spTitle.node.getContentSize();
        this.spTitle.spriteFrame = this._obj.hasWin ? this.sfTitle[0] : this.sfTitle[1];
        this.spTitle.node.setContentSize(size);
        this.spArrow[0].spriteFrame = this._obj.hasWin ? this.sfArrow[0] : this.sfArrow[1];
        this.spArrow[1].spriteFrame = this._obj.hasWin ? this.sfArrow[0] : this.sfArrow[1];
    },

    //
    runActonStep_1(){
        this.node.opacity = 0;
        this.btnContinue.active = false;
        //
        if(this._obj.hasWin){
            this.ndoeResult_1.opacity = 0;
            this.ndoeResult_2.opacity = 0;
            this.ndoeResult_3.opacity = 0;
            this.nodeCover_1.opacity = 0;
            this.nodeCover_2.opacity = 0;
            this.nodeCover_3.opacity = 0;

            this.ndoeResult_1.active = true;
            this.ndoeResult_2.active = true;
            this.ndoeResult_3.active = true;
            this.nodeCover_1.active = true;
            this.nodeCover_2.active = true;
            this.nodeCover_3.active = true;
        }else{
            this.ndoeResult_1.active = false;
            this.ndoeResult_2.active = false;
            this.ndoeResult_3.active = false;
            this.nodeCover_1.active = false;
            this.nodeCover_2.active = false;
            this.nodeCover_3.active = false;
        }
        this.nodeJiangli.opacity = 0;
        this.nodeRefuse.opacity = 0;
        this.ndoeText.opacity = 0;
        //
        this.nodeTitle.opacity = 0;
        this.nodeLine.opacity = 0;
        this.nodeCustom.active = false;
        this.nodeGolds.active = false;
        this.node.active = true;

        //渐显背景
        this.node.runAction(cc.sequence(cc.fadeIn(0.1),cc.callFunc(function(){
            //缩放标题进入
            this.nodeTitle.opacity = 255;
            this.nodeTitle.y = -60;
            this.nodeTitle.setScale(1.5);
            this.nodeTitle.runAction(
                cc.sequence(
                    cc.spawn(cc.scaleTo(0.1,1,1),cc.moveTo(0.1,this.nodeTitle.x,0)),
                    cc.callFunc(function(){
                        this.nodeCustom.active = true;
                        this.nodeCustom.y = this.nodeTitle.y-142;
                        this.nodeLine.y = this.nodeTitle.y-142;
                        this.nodeGolds.y = this.nodeCustom.y-128;                        
                        //
                        let t_line = 0.25;
                        this.nodeLine.runAction(
                            cc.sequence(
                                cc.fadeIn(t_line),
                                cc.fadeOut(t_line),
                                cc.callFunc(function(){
                                    this.nodeGolds.active = true;
                                    let t_move = 0.15;
                                    let titleY = 
                                    this.nodeTitle.runAction(cc.moveTo(t_move, this.nodeTitle.x, 694));
                                    this.nodeCustom.runAction(
                                        cc.sequence(
                                            cc.delayTime(0.05),
                                            cc.moveTo(t_move, this.nodeCustom.x, 694-142)));
                                    this.nodeGolds.runAction(
                                        cc.sequence(
                                            cc.delayTime(0.1),
                                            cc.moveTo(t_move, this.nodeGolds.x, 694-142-128),
                                            cc.callFunc(function(){
                                                if(this._obj.hasWin){
                                                    this.runActionStep_2();
                                                }else{
                                                    this.btnContinue.active = true;
                                                }
                                            },this,0)));
                                },this,0)));
                    },this,0)));
        },this,0)));
    },
    runActionStep_2(){
        let time = 0.05;
        this.nodeJiangli.runAction(cc.fadeIn(time));
        this.nodeRefuse.runAction(cc.sequence(cc.fadeIn(time),cc.fadeOut(time)));
        this.ndoeText.runAction(cc.sequence(cc.fadeIn(time),cc.fadeOut(time),cc.callFunc(function(){
            this.runActionStep_3();
        },this,0)));
        this.ndoeResult_1.runAction(cc.sequence(cc.fadeIn(time),cc.fadeOut(time)));
        this.ndoeResult_2.runAction(cc.sequence(cc.fadeIn(time),cc.fadeOut(time)));
        this.ndoeResult_3.runAction(cc.sequence(cc.fadeIn(time),cc.fadeOut(time)));
    },
    runActionStep_3(){
        this.nodeCover_1.setScale(1.5);
        this.nodeCover_2.setScale(1.5);
        this.nodeCover_3.setScale(1.5);
        this.nodeCover_1.opacity = 50;
        let t_fade_in = 0.1;
        let time_2 = 0.05;
        let time_3 = 0.2;
        let time_4 = 0.05;
        let t_delay = 0.05;
        let scale_f = 1.2;
        this.nodeCover_1.runAction(
            cc.sequence(
                cc.spawn(cc.fadeIn(t_fade_in),cc.scaleTo(t_fade_in,1,1)),
                cc.callFunc(function(){
                    this.ndoeResult_1.runAction(cc.fadeIn(time_2));
                },this,0),
            cc.fadeOut(time_2),
            cc.callFunc(function(){
                this.nodeCover_2.opacity = 50;
                this.nodeFrame_1.opacity = 255;
                this.nodeFrame_1.setScale(1);
                this.nodeFrame_1.runAction(
                    cc.spawn(cc.scaleTo(time_4,scale_f,scale_f),cc.fadeOut(time_3)));
                this.nodeCover_2.runAction(
                    cc.sequence(
                        cc.delayTime(t_delay),
                        cc.spawn(cc.fadeIn(t_fade_in),cc.scaleTo(t_fade_in,1,1)),
                        cc.callFunc(function(){
                            this.ndoeResult_2.runAction(cc.fadeIn(time_2));
                        },this,0),
                        cc.fadeOut(time_2),
                        cc.callFunc(function(){
                            this.nodeCover_3.opacity = 50;
                            this.nodeFrame_2.opacity = 255;
                            this.nodeFrame_2.setScale(1);
                            this.nodeFrame_2.runAction(
                                cc.spawn(cc.scaleTo(time_4,scale_f,scale_f),cc.fadeOut(time_3)));
                            this.nodeCover_3.runAction(
                                cc.sequence(
                                    cc.delayTime(t_delay),
                                    cc.spawn(cc.fadeIn(t_fade_in),cc.scaleTo(t_fade_in,1,1)),
                                    cc.callFunc(function(){
                                        this.ndoeResult_3.runAction(cc.fadeIn(time_2));
                                    },this,0),
                                    cc.fadeOut(time_2),
                                    cc.callFunc(function(){
                                        this.nodeFrame_3.opacity = 255;
                                        this.nodeFrame_3.setScale(1);
                                        this.nodeFrame_3.runAction(
                                            cc.spawn(cc.scaleTo(time_4,scale_f,scale_f),cc.fadeOut(time_3)));
                                        let y_2 = this.ndoeText.y;
                                        let x_2 = this.ndoeText.x;
                                        this.ndoeText.setScale(0.6);
                                        this.ndoeText.runAction(
                                            cc.sequence(
                                                cc.delayTime(t_delay),
                                                cc.spawn(cc.moveTo(time_3,x_2,y_2),
                                                    cc.scaleTo(time_3,1,1),
                                                    cc.fadeIn(time_3))));
                                        let y = this.nodeRefuse.y;
                                        let x = this.nodeRefuse.x;
                                        this.ndoeText.y = y - 100;
                                        this.nodeRefuse.y = y - 100;
                                        this.nodeRefuse.setScale(0.6);
                                        this.nodeRefuse.runAction(
                                            cc.sequence(
                                                cc.delayTime(time_3+0.2),
                                                cc.spawn(
                                                    cc.moveTo(time_3,x,y),
                                                    cc.scaleTo(time_3,1,1),
                                                    cc.fadeIn(time_3))));
                                    },this,0)));
                        },this,0)));
            },this,0)));
    },

    //残忍拒绝
    onClose(){
        this.node.active = false;
        console.log("---残忍拒绝---");
    },
    //点击继续
    onContinue(){
        this.node.active = false;
        console.log("---点击继续---");
    },
});
