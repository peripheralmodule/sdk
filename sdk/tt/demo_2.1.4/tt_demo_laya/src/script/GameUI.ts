import { ui } from "./../ui/layaMaxUI";
/**
 * 本示例采用非脚本的方式实现，而使用继承页面基类，实现页面逻辑。在IDE里面设置场景的Runtime属性即可和场景进行关联
 * 相比脚本方式，继承式页面类，可以直接使用页面定义的属性（通过IDE内var属性定义），比如this.tipLbll，this.scoreLbl，具有代码提示效果
 * 建议：如果是页面级的逻辑，需要频繁访问页面内多个元素，使用继承式写法，如果是独立小模块，功能单一，建议用脚本方式实现，比如子弹脚本。
 */
export default class GameUI extends ui.test.TestSceneUI {
    /**设置单例的引用方式，方便其他类引用 */
    static instance: GameUI;

    constructor() {
        super();
        GameUI.instance = this;
        //关闭多点触控，否则就无敌了
        Laya.MouseManager.multiTouchEnabled = false;
    }

    onEnable(): void {
        tt.ylSetCurScene("MainScene");//一定记得在每个场景都要设置当前场景
        tt.ylInitSDK(function(success:Boolean){
            console.log("-----初始化SDK-----:",success);
            //this.doSDKinterface();
            this.testStoreValue();
        }.bind(this));
    }
    doSDKinterface():void{
        console.log("---调用接口---");
        tt.ylGetCustom(function(data:any){
        }.bind(this));
        tt.ylStatisticResult({"total_score": 123 , "rebirth_score": 123},function(status:any){
            if(status){
                //干点什么
            }
        }.bind(this));

        let userWXinfo = tt.ylGetUserTTinfo();//先获取本地缓存，没有在弹窗通过用户授权获取

        let loginInfo = tt.ylGetUserInfo();//获取登录信息，包括微信oppid和code(code只有第一次登录才能取到)
        console.log("---登录信息-loginInfo:",JSON.stringify(loginInfo));

        var that = this;
        /**
         * 获取分享图列表
         */
        tt.ylShareCard(function (shareInfo:any) {
            if(shareInfo){
                console.log("----获取分享图列表:",JSON.stringify(shareInfo));
                this.btnShare.active = true;
            }else{
                //获取失败
            }
        }.bind(this),'LoginScene');
        tt.ylStatisticViedo(0,'adunit-6878a73e134f85e2',null);
        tt.ylStatisticShareCard(177);
        tt.ylRecorderStart(20,function(){});  
        this.testStoreValue();
    }
    //视频分享策略
    onShowShareOrVideo():void{
        tt.ylShowShareOrVideo("chanel_2","model_2",function(type){
            switch(type){
                case 0:
                    console.warn("------策略-无");
                    break;
                case 1:
                    console.warn("------策略-分享");
                    break;
                case 2:
                    console.warn("------策略-视频");
                    break;
            }
        }.bind(this));
    }
    //测试自定义空间值
    testStoreValue():void{
        this.test_sv_string();//字符变量测试代码
        this.test_sv_list();//字符数组测试代码
        this.test_sv_set();//字符集合测试代码
        this.test_sv_hash();//字符散列测试代码
        this.test_sv_radom();//随机数测试代码
    }
    test_sv_string():void{
        //String
        tt.ylStoreValue(
            {
                name:"testString",
                cmd:"set",
                args:"测试数据"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testString",
                        cmd:"get"
                    },
                    function(status){
                        
                }.bind(this));
        }.bind(this));
    }
    test_sv_list():void{
        //List
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"add",
                args:"2"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"set",
                args:"0,3"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testList",
                        cmd:"all"
                    },
                    function(status){
                        
                }.bind(this));
                 tt.ylStoreValue(
            {
                name:"testList",
                cmd:"get",
                args:"0"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"size"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testList",
                cmd:"poll",
                args:"2"
            },
            function(status){
                    tt.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"size"
                        },
                        function(status){
                            
                    }.bind(this));
                    tt.ylStoreValue(
                        {
                            name:"testList",
                            cmd:"replace",
                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                        },
                        function(status){
                            tt.ylStoreValue(
                                {
                                    name:"testList",
                                    cmd:"all"
                                },
                                function(status){
                                    
                            }.bind(this));
                    }.bind(this));
            }.bind(this));
        }.bind(this));
    }
    test_sv_set():void{
        //Set
        tt.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"12"
            },
            function(status){
                
        }.bind(this));
        tt.ylStoreValue(
            {
                name:"testSet",
                cmd:"add",
                args:"10"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"exist",
                        args:"10"
                    },
                    function(status){
                        
                }.bind(this));
                tt.ylStoreValue(
                    {
                        name:"testSet",
                        cmd:"size"
                    },
                    function(status){
                        tt.ylStoreValue(
                            {
                                name:"testSet",
                                cmd:"del",
                                args:"10"
                            },
                            function(status){
                                tt.ylStoreValue(
                                    {
                                        name:"testSet",
                                        cmd:"all"
                                    },
                                    function(status){
                                        tt.ylStoreValue(
                                        {
                                            name:"testSet",
                                            cmd:"replace",
                                            args:"[\"1\",\"2\",\"3\",\"4\",\"5\"]"
                                        },
                                        function(status){
                                            tt.ylStoreValue(
                                                {
                                                    name:"testSet",
                                                    cmd:"all"
                                                },
                                                function(status){
                                                    
                                            }.bind(this));
                                            
                                    }.bind(this));
                                }.bind(this));
                                
                        }.bind(this));
                        
                }.bind(this));
                
        }.bind(this));
    }
    test_sv_hash():void{
        //litMap
        tt.ylStoreValue(
            {
                name:"testHash",
                cmd:"set",
                args:"u_name,许"
            },
            function(status){
                tt.ylStoreValue(
                    {
                        name:"testHash",
                        cmd:"get",
                        args:"u_name"
                    },
                    function(status){
                        tt.ylStoreValue(
                        {
                            name:"testHash",
                            cmd:"replace",
                            args:"{\"u_name\":\"唐\",\"sex\":\"women\"}"
                        },
                        function(status){
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"gets",
                                args:"u_name,sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"size",
                            },
                            function(status){
                                
                            }.bind(this));
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"values",
                                args:"sex"
                            },
                            function(status){
                                
                            }.bind(this));
                            tt.ylStoreValue(
                            {
                                name:"testHash",
                                cmd:"del",
                                args:"u_name"
                            },
                            function(status){
                                tt.ylStoreValue(
                                {
                                    name:"testHash",
                                    cmd:"all",
                                },
                                function(status){
                                    
                                }.bind(this));
                            }.bind(this));
                        }.bind(this));
                    }.bind(this));
        }.bind(this));
    }
    test_sv_radom():void{
        //testRandom
        tt.ylStoreValue(
            {
                name:"testRandom"
            },
            function(status){
        }.bind(this));
    }
}