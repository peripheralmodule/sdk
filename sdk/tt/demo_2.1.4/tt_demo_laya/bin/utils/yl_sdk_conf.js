exports.ylsdk_app_id = "tt711307aeee39fe2d"; //游戏APPID
exports.ylsdk_version = "1.0.0"; 	//游戏版本号
exports.ylsdk_debug_model = true;   //是否debug模式 [true:打印日志,false:不打印日志]
exports.login_by_code = false;     //配置为true，则每次都会走头条登录
exports.ylsdk_platform = 4; 		//平台[ 0:微信,1:QQ,2:OPPO,3:VIVO,4:字节跳动 ]//暂未开放:5:百度,6:4399,7:趣头条,8:360,9:陌陌
exports.ylsdk_pkg_name=""; //游戏包名(OPPO、VIVO)

exports.side_min_num = 20;		//侧边栏列表item最小保留数(基于曝光策略)
exports.ylsdk_banner_ids = [	//banner广告ID(微信、QQ、字节跳动)
	'15926jnded313m55ms'
]; 
exports.ylsdk_video_ids = [		//激励视频广告ID(微信、QQ、字节跳动)
	'6sn234aoqf3737fmu9'
];							  
exports.ylsdk_grid_ids = [		//格子广告ID(微信)
];
exports.ylsdk_interstitial_ids = [	//插屏广告ID(微信,头条)
	'b5a2vkpi85b7f203df'
];
exports.ylsdk_template_id = [	 //模板分享ID(字节跳动)
	'of6b158hdol2r5berq'
];	
/****************头条公众平台配置以下合法域名************************/

// 服务器地址：		   https://api.ylxyx.cn
// 图片服务器地址：	   https://ql.ylxyx.cn

/*****************************************************************/
