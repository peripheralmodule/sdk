import { ui } from "../../ui/layaMaxUI";
import DouyinRankItem from "./DouyinRankItem";
import EventConst from "./EventConst";
import EventMgr from "./EventMgr";
import RecordDataMgr from "./RecordDataMgr";
import SDKMgr from "./SDKMgr";
import UIConst from "./UIConst";



import Handler = Laya.Handler;
/**
 * 抖音排行榜
 */
export default class DouyinRankView extends ui.test.DouyinRankingViewUI {
    private switchHot: boolean = true;    //当前选择排行类型:true:热门,false:最新
    _lastY: number;
    onAwake(): void {
        let self = this;
        this.btn_back.on(Laya.Event.CLICK, this, this.onClose);
        this.btn_share.on(Laya.Event.CLICK, this, this.onShare);
        this.lb_hot.on(Laya.Event.CLICK, this, this.clickHot);
        this.lb_new.on(Laya.Event.CLICK, this, this.clickNew);

        EventMgr.on(EventConst.E_REFRESH_RECORD_RANK, this, this.refreshRank);


        self.list.vScrollBarSkin = "";// 使用但隐藏滚动条,开启列表滚动
        self.list.selectEnable = true;
        self.list.selectHandler = new Handler(this, self.onSelect);
        self.list.renderHandler = new Handler(this, self.updateItem);
        //默认最热门视频
        this.img_hot_bg.visible = true;
        this.img_new_bg.visible = false;
        this.img_hot_bg.skin = "douyin/img_t.png";
        this.img_new_bg.skin = "douyin/img_t.png";

        this.btn_back.visible = true;

        self.clickHot();
    }

    onClose() {
        EventMgr.off(EventConst.E_REFRESH_RECORD_RANK, this, this.refreshRank);
        Laya.Dialog.close(UIConst.DouyinRankView);
    }

    onShare() {
        let self = this;
        if (SDKMgr.ins.GetRecordPath()) {
            SDKMgr.ins.ShareRecord((res) => {
                if (res == 1) {
                    console.warn("分享成功，获得奖励！");
                } else if (res == 2) {
                    console.warn("时长未满3秒，无法分享！");
                } else {
                    console.warn("分享失败！");
                }
            });
        } else {
            console.warn("录屏地址不存在！");
        }
    }

    /**
     * 最热视频
     */
    clickHot() {
        let self = this;
        let type = 1;
        self.list.array = [];
        this.img_hot_bg.visible = type == 1;
        this.img_new_bg.visible = type != 1;
        this.switchHot = true;
        RecordDataMgr.ins.getDiggRankInfo().then((data: any[]) => {
            let list = data.concat();  //克隆一份。
            self.list.array = list;
        });
    }

    /**
     * 最新视频
     */
    clickNew() {
        let self = this;
        let type = 2;
        self.list.array = [];
        this.img_hot_bg.visible = type == 1;
        this.img_new_bg.visible = type != 1;
        this.switchHot = false;
        RecordDataMgr.ins.getLastRankInfo().then((data: any[]) => {
            let list = data.concat();  //克隆一份。
            self.list.array = list;
        });
    }

    /**
     * 刷新排行榜数据
     */
    refreshRank() {
        if (this.img_hot_bg.visible) {
            this.clickHot();
        } else {
            this.clickNew();
        }
    }

    private onSelect(index: number): void {
        if (index < 0) return;
        SDKMgr.ins.JumpToVideoView(this.list.selectedItem.alias_id, () => {
        });
        this.list.selectedIndex = -1;
    }

    private updateItem(cell: DouyinRankItem, index: number): void {
        cell.setData(cell.dataSource, index, this.switchHot);
    }
}