import EventConst from "./EventConst";
import EventMgr from "./EventMgr";
import { YDHW } from "../../../libs/ydhw/ydhw.sdk";
import RecordDataMgr from "./RecordDataMgr";

/**
 * 录屏状态
 */
enum EM_RECORD_TYPE {
    None = 0,
    Recording = 1,
    Pasuse = 2,
    Stoped = 3,
}

/**
 * SDK
 */
export default class SDKMgr {
    private static _ins: SDKMgr = null;

    private _userInfos: any[] = null;    //存储空间的用户信息
    private _userInfo: any = null;

    private _status: number = 0; //录屏状态
    private _recordVideoPath: string = null;  //录屏资源地址
    private _recordDurationEnough = false;  //录屏时长是否足够（>3秒）
    private _recordStartTime = new Date().getTime(); //开始录屏时间
    private _recordMinTime = 3000;   //最小录屏时长

    private shareListData: IYDHW.GameBase.IShareCardResult[] = [];

    public static get ins(): SDKMgr {
        if (!this._ins) {
            this._ins = new SDKMgr();
        }
        return this._ins;
    }


    /**
     * 开始录屏
     */
    RecordStart(): void {
        //duration:录屏的时长，单位 s，必须 >3 &&  <= 300s（5 分钟）
        let duration = 30;
        let self = this;
        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderStart, duration,
            (result) => {
                self._status = EM_RECORD_TYPE.Recording;
                EventMgr.event(EventConst.E_REFRESH_RECORD_STATUS);
                self._recordStartTime = new Date().getTime();
                self._recordDurationEnough = false;
            },
            (videoPath: string) => {
                let nowTime = new Date().getTime();
                let timeSpace = nowTime - self._recordStartTime;
                self._recordStartTime = nowTime;
                if (timeSpace > self._recordMinTime) {
                    self._recordVideoPath = videoPath;
                    self._recordDurationEnough = true;
                } else {
                    self._recordDurationEnough = false;
                }

                self._status = EM_RECORD_TYPE.Stoped;
                let recordInfo = {
                    video_path: videoPath,
                    time_space: timeSpace
                }
                EventMgr.event(EventConst.E_REFRESH_RECORD_STATUS,
                    JSON.stringify(recordInfo));
            },
            (result: any) => {
                self._status = EM_RECORD_TYPE.Recording;
                EventMgr.event(EventConst.E_REFRESH_RECORD_STATUS);
            },
            (result: any) => {
                self._status = EM_RECORD_TYPE.Pasuse;
                EventMgr.event(EventConst.E_REFRESH_RECORD_STATUS);
            },
            (result: any) => {
                console.warn("Recorder-onError:", JSON.stringify(result));
            }
        );
    }

    /**
     * 获取录屏地址
     */
    GetRecordPath(): string {
        return this._recordVideoPath;
    }

    /**
     * 停止录屏
     */
    RecordStop(): void {
        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderStop);
    }
    /**
     * 继续录屏
     */
    RecordResume(): void {
        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderResume);
    }

    /**
     * 暂停录屏
     */
    RecordPasue(): void {
        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderPause);
    }

    /**
     * 获取当前录屏状态
     */
    GetRecordStatus(): IYDHW.TT.EM_RECORD_TYPE {
        return this._status;
    }

    /**
     * 分享录屏 
     * callback(isOk:0:录屏失败，1：录屏成功，2：录屏时长不足)
     */
    ShareRecord(callback: Function): void {
        let shareInfo = null;
        let title = "分享标题";
        let description = "分享文案！";
        let self = this;
        if (!self._recordDurationEnough) {
            if (callback) callback(2);
            return;
        }
        if (this.shareListData.length > 0) {
            let index = Math.floor(Math.random() * this.shareListData.length);
            shareInfo = this.shareListData[index];
            description = shareInfo.title;
        }
        ydhw.Invoke(YDHW.YDHW_TT_API.ShareVideo, title, description, '',
            (isOk: any) => {
                let result = isOk ? 1 : 0;
                if (callback) callback(result);
                if (isOk && isOk.videoId) {
                    let videoId = isOk.videoId;
                    let nowTime = new Date().getTime();
                    RecordDataMgr.ins.updateRecordLast(videoId, nowTime);
                }
            });
    }

    /**
     * 跳转视频播放页
     * @param _videoId  视频ID
     */
    JumpToVideoView(_videoId: string, callback: Function) {
        console.log("JumpToVideoView-videoId:", _videoId);
        if (window.ydhw_tt) {
            window['tt'].navigateToVideoView({
                videoId: _videoId,
                success: (res) => {
                    //res结构： {errMsg: string }
                    console.warn("success:", JSON.stringify(res));
                    if (callback) callback(res);
                },
                fail: (err: any) => {
                    if (err.errCode === 1006) {
                        console.warn("something wrong with your network");
                    }
                    if (callback) callback(err);
                },
            });
        }
    }

    /**
     * 获取用户信息
     * @param callback 
     */
    GetUserInfo(callback?: Function) {
        let self = this;
        if (self._userInfo) {
            if (callback) callback(self._userInfo);
            return;
        }
        ydhw.Invoke(YDHW.YDHW_API.GetUserInfo, this,
            (result: any) => {//onSuccess
                console.log("GetUserInfo-onSuccess:" + JSON.stringify(result));
                if (result) {
                    self.userInfo = result;
                }
                if (callback) callback(result);
            },
            (error: any) => {//onError
                console.warn("GetUserInfo-onError:", error);
                if (callback) callback(false);
            }
        );
    }

    /**
     * 获取所有用户信息
     */
    GetUserInfoByStore(callback?: Function) {
        let self = this;
        let selectInfo = {
            "#sortField": "gender",
            "#sortType": "-1",
            "#limit": "100"
        };
        this.StoreValue("tableUser", "select",
            "" + JSON.stringify(selectInfo),
            (result: IYDHW.GameBase.IStoreValueResult) => {
                if (result && result.value && result.value.length > 0) {
                    self._userInfos = result.value;
                    SDKMgr.ins.SetUserInfoByStore(result, () => {
                    });
                }
                if (callback) callback();
            });
    }

    /**
     * 上传用户信息
     * @param result 
     * @param callback 
     */
    SetUserInfoByStore(result: any, callback?: Function) {
        let self = this;
        let args = "user_id_" + ydhw.AccountId + "," + JSON.stringify(result);
        this.StoreValue("tableUser", "update", args,
            (result: IYDHW.GameBase.IStoreValueResult) => {
                if (result && result.value && result.value.length > 0) {
                    self._userInfos = result.value;
                }
                if (callback) callback();
            });
    }

    /**
     *存储空间
     */
    StoreValue(name: string, cmd?: string, args?: string, callBack?: Function) {
        let storeInfo = new YDHW.StoreValueRequest(name, cmd, args);
        ydhw.Invoke(YDHW.YDHW_API.StoreValue, storeInfo, this,
            (result: IYDHW.GameBase.IStoreValueResult) => {
                // console.log("StoreValue-name:", name + ",cmd:", cmd + ",result:", JSON.stringify(result));
                if (callBack) callBack(result);
            });
    }

    public get userInfo(): any {
        return this._userInfo;
    }

    public set userInfo(user_info: any) {
        this._userInfo = user_info;
    }

}