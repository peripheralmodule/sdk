import { ui } from "../../ui/layaMaxUI";

// "moduleResolution": "node",

/**
 * 抖音排行榜子项
 */
export default class DouyinRankItem extends ui.test.DouyinRankItemUI {


    onAwake(): void {
    }

    public setData(data: any, index: number, switchHot: boolean): void {
        let _coverUrl = "";
        let _diggCount = 0;
        let _rankText = "";
        let _shareTiem = "";
        let _nickName = "未知用户";
        let _avatarUrl = data.avatarUrl;
        if (data.video_info) {
            _diggCount = data.video_info.digg_count || 0;
            _coverUrl = data.video_info.cover_url || "";
        }
        if (data.nickName) _nickName = data.nickName;
        try {
            if (!switchHot) {
                _shareTiem = data.share_time ? this.formatDate(new Date(data.share_time), "YY-MM-dd") : "";
            }
        } catch (e) { }
        _rankText = switchHot ? "" + _diggCount : _shareTiem;
        this.img_icon.skin = _coverUrl;
        var self = this, imgIcon = self.img_head;
        var mask = imgIcon.mask = new Laya.Sprite;
        this.drawRoundRect(mask.graphics, imgIcon.width, imgIcon.height, 20);
        _avatarUrl = (_avatarUrl && _avatarUrl != "") ? _avatarUrl : "wxlocal/share1.jpg";;
        this.img_head.skin = _avatarUrl;
        this.lb_name.text = _nickName;
        this.lb_rank.text = _rankText;
        let top = index + 1;
        this.lb_top_num.text = "" + top;
        let skinStr = "douyin/img_ns.png";
        switch (top) {
            case 1:
                skinStr = "douyin/img_hs.png";
                break;
            case 2:
                skinStr = "douyin/img_ls.png";
                break;
            case 3:
                skinStr = "douyin/img_ss.png";
                break;
        }
        this.img_top_img.skin = skinStr;
    }

    private formatDate(date: Date, fmt: string): string {
        let o = {
            "Y+": date.getFullYear(),                  //年份
            "M+": date.getMonth() + 1,                 //月份   
            "d+": date.getDate(),                    //日   
            "h+": date.getHours(),                   //小时   
            "m+": date.getMinutes(),                 //分   
            "s+": date.getSeconds(),                 //秒
            "S": date.getMilliseconds()             //毫秒   
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (let k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }

    /**
     * 画上圆角矩形
     * @param sprite 
     * @param width 矩形宽
     * @param height 矩形高
     * @param round 圆角半径
     */
    public drawRoundRect(graphics: Laya.Graphics, width: number, height: number, round: number): void {
        var paths = [
            ['moveTo', round, 0],
            ['lineTo', width - round, 0],
            ['arcTo', width, 0, width, round, round],
            ['lineTo', width, height - round],
            ['arcTo', width, height, width - round, height, round],
            ['lineTo', round, height],
            ['arcTo', 0, height, 0, height - round, round],
            ['lineTo', 0, round],
            ['arcTo', 0, 0, round, 0, round]
        ];
        graphics.drawPath(0, 0, paths, { fillStyle: "#0" });
    }
}