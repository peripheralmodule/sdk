import { YDHW } from "../../../libs/ydhw/ydhw.sdk";
import EventMgr from "./EventMgr";
import EventConst from "./EventConst";
import SDKMgr from "./SDKMgr";


/**
 * 录屏排行相关数据管理
 */
export default class RecordDataMgr {
    private static _ins: RecordDataMgr;
    private _myRecordIds: string[] = [];  //玩家自身的录屏分享ID列表
    private _recordDiggs: any[] = [];     //录屏排行榜-点赞(存储空间)
    private _recordLasts: any[] = [];     //录屏排行榜-最新(存储空间)
    private diggRefreshed: boolean = false;  //是否已刷新-热门
    private lastRefreshed: boolean = false;  //是否已刷新-最新

    private _userInfo: any = null;

    private constructor() { }
    public static get ins(): RecordDataMgr {
        if (!this._ins) {
            this._ins = new RecordDataMgr();
            this._ins.init();
        }
        return this._ins;
    }

    /**
     * 初始化
     */
    private init(): void {
        let self = this;
        this.getRecordIds().then(() => {
            return self.getRecordDigg();
        }).then(() => {
            return self.getRecordLast();
        }).then(() => {
            console.log("RecordDataMgr init complete");
        });
    }

    /**
     * 获取录屏排行-点赞数
     */
    getRecordDigg() {
        let self = this;
        let selectInfo = {
            "#sortField": "digg_count",
            "#sortType": "-1",
            "#limit": "100"
        };
        return new Promise(resolve => {
            let args = "" + JSON.stringify(selectInfo);
            self.StoreValue("tableRecordDigg", "select", args)
                .then((result: IYDHW.GameBase.IStoreValueResult) => {
                    if (result && result.value && result.value.length > 0) {
                        self._recordDiggs = result.value;
                        EventMgr.event(EventConst.E_REFRESH_RECORD_RANK);
                    }
                    resolve();
                });
        });
    }

    /**
     * 获取录屏排行-最新
     */
    getRecordLast() {
        let self = this;
        let selectInfo = {
            "#sortField": "share_time",
            "#sortType": "-1",
            "#limit": "100"
        };
        return new Promise(resolve => {
            let args = "" + JSON.stringify(selectInfo);
            self.StoreValue("tableRecordLast", "select", args)
                .then((result: IYDHW.GameBase.IStoreValueResult) => {
                    if (result && result.value && result.value.length > 0) {
                        self._recordLasts = result.value;
                        EventMgr.event(EventConst.E_REFRESH_RECORD_RANK);
                    }
                    resolve();
                });
        });
    }

    /**
     * 获取玩家自己的录屏Id列表
     */
    getRecordIds() {
        let self = this;
        let selectInfo = {
            "#key": "user_id_" + ydhw.AccountId,
            "#sortField": "#key",
            "#sortType": "-1",
            "#limit": "100"
        };
        return new Promise(resolve => {
            let args = "" + JSON.stringify(selectInfo);
            self.StoreValue("tableRecordId", "select", args)
                .then((result: IYDHW.GameBase.IStoreValueResult) => {
                    if (result && result.value && result.value.length > 0) {
                        let values = result.value[0];
                        if (values.record_id && values.record_id.length > 0) {
                            self._myRecordIds = values.record_id;
                        }
                    }
                    resolve();
                });
        });
    }

    /**
     * 更新玩家最新录屏数据
     */
    updateRecordLast(recordId: string, shareTime: number) {
        let self = this;
        self.diggRefreshed = false;
        self.lastRefreshed = false;
        self.getVideoInfo([recordId]).then((res: any) => {
            if (res && res.data && res.data.data && res.data.data.length > 0) {
                let result = res.data.data[0];
                let user_id = "user_id_" + window.ydhw.AccountId;
                let lastInfo = JSON.stringify(self.getLastInfo(result, shareTime));
                let args = user_id + "," + lastInfo;
                //上传数据到存储空间
                return self.StoreValue("tableRecordLast", "update", args);
            }
        }).then((res: any) => {
            //拉取最新排行榜,更新本地数据
            return self.getRecordLast();
        }).then(() => {
            self.addRecordIds(recordId);
        });
    }
    /**
     * 最近-录屏信息
     * @param result 
     * @param shareTime 
     */
    getLastInfo(result: any, shareTime: number): any {
        let userInfo = this._userInfo;//用户授权信息需要先获取        
        let lastInfo = {
            "nickName": (userInfo ? userInfo.nickName : "未知用户"),
            "avatarUrl": (userInfo ? userInfo.avatarUrl : ""),
            "gender": (userInfo ? userInfo.gender : 0),
            "city": (userInfo ? userInfo.city : ""),
            "province": (userInfo ? userInfo.province : ""),
            "country": (userInfo ? userInfo.country : "中国"),
            "language": (userInfo ? userInfo.language : ""),
            "alias_id": result.alias_id || "",
            "video_info": result.video_info,
            "share_time": shareTime
        }
        return lastInfo;
    }

    /**
     * 热门-录屏信息
     * @param recordInfo 
     */
    getDiggInfo(recordInfo: any) {
        let userInfo = this._userInfo;
        let diggInfo = {
            "nickName": (userInfo ? userInfo.nickName : "未知用户"),
            "avatarUrl": (userInfo ? userInfo.avatarUrl : ""),
            "gender": (userInfo ? userInfo.gender : 0),
            "city": (userInfo ? userInfo.city : ""),
            "province": (userInfo ? userInfo.province : ""),
            "country": (userInfo ? userInfo.country : "中国"),
            "language": (userInfo ? userInfo.language : ""),
            "alias_id": recordInfo.alias_id || "",
            "video_info": recordInfo.video_info,
            // "share_time":recordInfo
        }
        return diggInfo;
    }

    /**
     * 添加玩家录屏Id到存储空间
     */
    addRecordIds(recordId: string) {
        let self = this;
        this._myRecordIds.push(recordId);
        let recorderIds = { "record_id": this._myRecordIds };
        let args = "user_id_" + ydhw.AccountId + "," + JSON.stringify(recorderIds);
        self.StoreValue("tableRecordId", "update", args)
            .then((res: any) => {
                return self.getRecordIds();
            }).then(() => {
                self.updateRecordDigg();
            });
    }

    /**
     * 更新玩家最高点赞排行
     */
    updateRecordDigg() {
        let self = this;
        //修改本地缓存
        if (self._myRecordIds && self._myRecordIds.length > 0) {
            self.getVideoInfo(self._myRecordIds).then((res: any) => {
                if (res && res.data
                    && res.data.data
                    && res.data.data.length > 0) {

                    let list = res.data.data;
                    list.sort(self.compare2);
                    let recordInfo = list[0];
                    let user_id = "user_id_" + window.ydhw.AccountId;
                    let diggInfo = self.getDiggInfo(recordInfo);
                    let args = user_id + "," + JSON.stringify(diggInfo);
                    //上传数据到存储空间
                    self.StoreValue("tableRecordDigg", "update", args)
                        .then((result: any) => {
                            //拉取点赞排行榜,更新本地数据
                            self.getRecordDigg();
                        });
                }
            });
        }
    }

    /**
     * 比较排序
     * @param a 
     * @param b 
     */
    compare2(a, b) {
        let a_diggCount = 0;
        let b_diggCount = 0;
        if (a.video_info && a.video_info != "") {
            a_diggCount = a.video_info.digg_count;
        }
        if (b.video_info && b.video_info != "") {
            b_diggCount = b.video_info.digg_count;
        }
        if (a_diggCount > b_diggCount) {
            return 1;
        } else if (a_diggCount < b_diggCount) {
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * 获取排行榜展示信息-热门
     * @param callback 
     */
    public getDiggRankInfo() {
        let self = this;
        return new Promise(resolve => {
            if (self.diggRefreshed) {
                resolve(self._recordDiggs);
            } else {
                let _diggsIds: string[] = [];
                self._recordDiggs.forEach(element => {
                    if (element && element.alias_id) {
                        _diggsIds.push(element.alias_id);
                    }
                });
                if (_diggsIds.length > 0) {
                    self.getVideoInfo(_diggsIds).then((res: any) => {
                        if (res && res.data
                            && res.data.data
                            && res.data.data.length > 0) {

                            let list = res.data.data;
                            let rData = list.concat(); //拷贝                   
                            self._recordDiggs.forEach(element => {
                                for (let i = 0; i < rData.length; i++) {
                                    let item = rData[i];
                                    if (element.alias_id == item.alias_id &&
                                        (item.video_info instanceof Object)) {

                                        element.video_info = item.video_info;
                                        rData.splice(i, 1);
                                        break;
                                    }
                                }
                            });
                            self.diggRefreshed = true;
                            resolve(self._recordDiggs);
                        }
                    });
                } else {
                    resolve([]);
                }
            }
        })
    }
    /**
     * 获取排行榜展示信息-最新
     * @param callback 
     */
    public getLastRankInfo() {
        let self = this;
        return new Promise(resolve => {
            if (self.lastRefreshed) {
                resolve(self._recordLasts);
            } else {
                let _lastsIds: string[] = [];
                this._recordLasts.forEach(element => {
                    if (element && element.alias_id) {
                        _lastsIds.push(element.alias_id);
                    }
                });
                if (_lastsIds.length > 0) {
                    self.getVideoInfo(_lastsIds).then((res: any) => {
                        if (res
                            && res.data
                            && res.data.data
                            && res.data.data.length > 0) {

                            let list = res.data.data;
                            let rData = list.concat(); //拷贝                   
                            self._recordLasts.forEach(element => {
                                for (let i = 0; i < rData.length; i++) {
                                    let item = rData[i];
                                    if (element.alias_id == item.alias_id &&
                                        (item.video_info instanceof Object)) {

                                        element.video_info = item.video_info;
                                        rData.splice(i, 1);
                                        break;
                                    }
                                }
                            });
                            self.lastRefreshed = true;
                            resolve(self._recordLasts);
                        } else {
                            resolve([]);
                        }
                    });
                } else {
                    resolve([]);
                }
            }
        });
    }

    /**
     * 获取视频点赞数、封面图
     */
    getVideoInfo(_videoIds: string[]) {
        return new Promise(resolve => {
            if (window.ydhw_tt) {
                window['tt'].request({
                    url: "https://gate.snssdk.com/developer/api/get_video_info",
                    method: "POST",
                    data: {
                        alias_ids: _videoIds,
                    },
                    success: (res) => {
                        // 从res中获取所需视频信息（videoId数组索引与返回数据数组索引一一对应）
                        // console.log("getVideoInfo:" + JSON.stringify(res));
                        resolve(res);
                    },
                    fail(res) {
                        resolve(null);
                        console.log("get_video_info request fail", JSON.stringify(res));
                    }
                });
            } else {
                resolve(null);
            }
        });
    }

    /**
     *存储空间
     */
    StoreValue(name: string, cmd?: string, args?: string, callBack?: Function) {
        let storeInfo = new YDHW.StoreValueRequest(name, cmd, args);
        return new Promise(resolve => {
            ydhw.Invoke(YDHW.YDHW_API.StoreValue, storeInfo, this,
                (result: IYDHW.GameBase.IStoreValueResult) => {
                    // console.log("StoreValue-name:", name + ",cmd:", cmd + ",result:", JSON.stringify(result));
                    if (callBack) callBack(result);
                    resolve(result);
                });
        })
    }

    public set userInfo(user_info: any) {
        this._userInfo = user_info;
    }

}