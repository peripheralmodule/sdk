
let EventConst = {
    /** 刷新抖音录屏排行榜 */
    E_REFRESH_RECORD_RANK:"E_REFRESH_RECORD_RANK",
    /** 刷新录屏状态 */
    E_REFRESH_RECORD_STATUS:"E_REFRESH_RECORD_STATUS",
}

export default EventConst;