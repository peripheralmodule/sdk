import { ui } from "./../ui/layaMaxUI";
import { YDHW } from "../../libs/ydhw/ydhw.sdk";
import RecordDataMgr from "./douyin/RecordDataMgr";
import UIConst from "./douyin/UIConst";
import EventMgr from "./douyin/EventMgr";
import EventConst from "./douyin/EventConst";
import SDKMgr from "./douyin/SDKMgr";

export default class GameUI extends ui.test.TestRecordRankUI {

    static instance: GameUI;

    constructor() {
        super();
        GameUI.instance = this;
        Laya.MouseManager.multiTouchEnabled = false;
    }

    onEnable(): void {
        console.log("YDHW ---YDHW_CONFIG:"+window['YDHW_CONFIG'].platformCode);
        this.initUI();
        this.init();
    }

    init(){
        ydhw.Invoke(YDHW.YDHW_API.Login,this, (isOk: boolean)=>{
            if(isOk){
                console.log("PlatformPublic -Login-isOk: " + isOk);
                RecordDataMgr.ins;    
                this.TT_GetPlatformUserInfo(null);
            }
        });
    }

    /**
     * 初始化UI
     */
    initUI():void{
        this.initTTplatformUI();
        this.txt_platfrom_name.text = "头条平台";
        this.lb_tt_node.visible = true;
    }

    /**
     * 初始化平台独有UI-字节跳动
     */
    initTTplatformUI():void{
        this.lb_tt_node.visible = true;
        this.txt_record_type.text = "";
        this.btnTTrecordStart.on(Laya.Event.CLICK,this,this.TT_RecordStart);
        this.btnTTrecordStop.on(Laya.Event.CLICK,this,this.TT_RecordStop);
        this.btnTTrecordPasueOrResume.on(Laya.Event.CLICK,this,this.TT_RecordPasueOrResume);
        this.btnTTshareVideo.on(Laya.Event.CLICK,this,this.TT_ShareVideo);
        this.btnTTshareToken.on(Laya.Event.CLICK,this,this.TT_ShareToken);
        this.btnTTgetUserInfo.on(Laya.Event.CLICK,this,this.TT_GetPlatformUserInfo);
        this.TT_RefreshRecordStatus(null);
        EventMgr.on(EventConst.E_REFRESH_RECORD_STATUS,this,this.TT_RefreshRecordStatus);
    }


    /********************************************************************/
    /****                   字节跳动平台独有接口                     ******/
    /********************************************************************/

    /**
     * 开始或停止录屏
     * @param e 
     */
    TT_RecordStart(e:Laya.Event):void{
        SDKMgr.ins.RecordStart();
    }

    /**
     * 停止录屏
     * @param e 
     */
    TT_RecordStop(e:Laya.Event):void{
        console.log("YDHW 停止录屏");
        SDKMgr.ins.RecordStop();
    }

    /**
     * 继续录屏/暂停录屏
     * @param e 
     */
    TT_RecordPasueOrResume(e:Laya.Event):void{
        let status = SDKMgr.ins.GetRecordStatus();
        if(status === 2){
            SDKMgr.ins.RecordResume();
        }else{
            SDKMgr.ins.RecordPasue();
        }
    }

    TT_RefreshRecordStatus(info:any){
        let status = SDKMgr.ins.GetRecordStatus();
        let statusS = "录屏-无";
        let sPoR = "继续录屏";
        switch(status){
            case 0: 
                statusS = "录屏-无";
                sPoR = "继续录屏";
                break;
            case 1:
                statusS = "录屏中";
                sPoR = "暂停录屏";
                break;
            case 2:
                statusS = "录屏-暂停";
                sPoR = "继续录屏";
                break;
            case 3:
                statusS = "录屏-停止:"+info;
                sPoR = "继续录屏";
                break;
        }
        this.btnTTrecordPasueOrResume.label = sPoR;
        this.txt_record_type.text = statusS;
    }

    /**
     * 视频分享(含模板)
     * @param e 
     */
    TT_ShareVideo(e:Laya.Event):void{
        SDKMgr.ins.ShareRecord(()=>{

        });
    }

    /**
     * 录屏排行榜对话框
     * @param e 
     */
    TT_ShareToken(e:Laya.Event):void{
        Laya.Dialog.open(UIConst.DouyinRankView);
    }

    /**
     * 获取平台用户信息
     * @param e 
     */
    TT_GetPlatformUserInfo(e:Laya.Event):void{
        SDKMgr.ins.GetUserInfo(()=>{ });
    }

}