(function () {
	'use strict';

	var REG = Laya.ClassUtils.regClass;
	var ui;
	(function (ui) {
	    var test;
	    (function (test) {
	        class DouyinRankingViewUI extends Laya.Dialog {
	            constructor() { super(); }
	            createChildren() {
	                super.createChildren();
	                this.loadScene("test/DouyinRankingView");
	            }
	        }
	        test.DouyinRankingViewUI = DouyinRankingViewUI;
	        REG("ui.test.DouyinRankingViewUI", DouyinRankingViewUI);
	        class DouyinRankItemUI extends Laya.View {
	            constructor() { super(); }
	            createChildren() {
	                super.createChildren();
	                this.loadScene("test/DouyinRankItem");
	            }
	        }
	        test.DouyinRankItemUI = DouyinRankItemUI;
	        REG("ui.test.DouyinRankItemUI", DouyinRankItemUI);
	        class TestRecordRankUI extends Laya.Scene {
	            constructor() { super(); }
	            createChildren() {
	                super.createChildren();
	                this.loadScene("test/TestRecordRank");
	            }
	        }
	        test.TestRecordRankUI = TestRecordRankUI;
	        REG("ui.test.TestRecordRankUI", TestRecordRankUI);
	    })(test = ui.test || (ui.test = {}));
	})(ui || (ui = {}));

	let EventConst = {
	    E_REFRESH_RECORD_RANK: "E_REFRESH_RECORD_RANK",
	    E_REFRESH_RECORD_STATUS: "E_REFRESH_RECORD_STATUS",
	};

	var EventMgr = new Laya.EventDispatcher;

	var YDHW;
	(function (YDHW) {
	    let YDHW_WECHAT_API;
	    (function (YDHW_WECHAT_API) {
	        YDHW_WECHAT_API[YDHW_WECHAT_API["CreateGridAd"] = 20001] = "CreateGridAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["ShowGridAd"] = 20002] = "ShowGridAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["HideGridAd"] = 20003] = "HideGridAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["SubscribeSysMsg"] = 20004] = "SubscribeSysMsg";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["GetSetting"] = 20005] = "GetSetting";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderOff"] = 20006] = "WXRecorderOff";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderOn"] = 20007] = "WXRecorderOn";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderStart"] = 20008] = "WXRecorderStart";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderStop"] = 20009] = "WXRecorderStop";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderPause"] = 20010] = "WXRecorderPause";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderResume"] = 20011] = "WXRecorderResume";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderAbort"] = 20012] = "WXRecorderAbort";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["IsAtempoSupported"] = 20013] = "IsAtempoSupported";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["IsFrameSupported"] = 20014] = "IsFrameSupported";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["IsSoundSupported"] = 20015] = "IsSoundSupported";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["IsVolumeSupported"] = 20016] = "IsVolumeSupported";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["CreateRecorderShareButton"] = 20017] = "CreateRecorderShareButton";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["RecorderShareButtonHide"] = 20018] = "RecorderShareButtonHide";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["RecorderShareButtonShow"] = 20019] = "RecorderShareButtonShow";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["RecorderShareButtonOnTap"] = 20020] = "RecorderShareButtonOnTap";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["RecorderShareButtonOffTap"] = 20021] = "RecorderShareButtonOffTap";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["SetMessageToFriendQuery"] = 20022] = "SetMessageToFriendQuery";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["OnShareMessageToFriend"] = 20023] = "OnShareMessageToFriend";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["OnShareTimeline"] = 20024] = "OnShareTimeline";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["OffShareTimeline"] = 20025] = "OffShareTimeline";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["CreateCustomAd"] = 20026] = "CreateCustomAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["ShowCustomAd"] = 20027] = "ShowCustomAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["HideCustomAd"] = 20028] = "HideCustomAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["IsShowCustomAd"] = 20029] = "IsShowCustomAd";
	    })(YDHW_WECHAT_API = YDHW.YDHW_WECHAT_API || (YDHW.YDHW_WECHAT_API = {}));
	    let YDHW_QQ_API;
	    (function (YDHW_QQ_API) {
	        YDHW_QQ_API[YDHW_QQ_API["ShareByTemplateId"] = 30001] = "ShareByTemplateId";
	        YDHW_QQ_API[YDHW_QQ_API["AddColorSign"] = 30002] = "AddColorSign";
	        YDHW_QQ_API[YDHW_QQ_API["SubscribeAppMsg"] = 30003] = "SubscribeAppMsg";
	        YDHW_QQ_API[YDHW_QQ_API["CreateAddFriendButton"] = 30004] = "CreateAddFriendButton";
	        YDHW_QQ_API[YDHW_QQ_API["ShowAddFriendButton"] = 30005] = "ShowAddFriendButton";
	        YDHW_QQ_API[YDHW_QQ_API["HideAddFriendButton"] = 30006] = "HideAddFriendButton";
	        YDHW_QQ_API[YDHW_QQ_API["DestroyAddFriendButton"] = 30007] = "DestroyAddFriendButton";
	        YDHW_QQ_API[YDHW_QQ_API["CreateAppBox"] = 30008] = "CreateAppBox";
	        YDHW_QQ_API[YDHW_QQ_API["ShowAppBox"] = 30009] = "ShowAppBox";
	        YDHW_QQ_API[YDHW_QQ_API["CreateBlockAd"] = 30010] = "CreateBlockAd";
	        YDHW_QQ_API[YDHW_QQ_API["ShowBlockAd"] = 30011] = "ShowBlockAd";
	        YDHW_QQ_API[YDHW_QQ_API["HideBlockAd"] = 30012] = "HideBlockAd";
	        YDHW_QQ_API[YDHW_QQ_API["DestroyBlockAd"] = 30013] = "DestroyBlockAd";
	    })(YDHW_QQ_API = YDHW.YDHW_QQ_API || (YDHW.YDHW_QQ_API = {}));
	    let YDHW_TT_API;
	    (function (YDHW_TT_API) {
	        YDHW_TT_API[YDHW_TT_API["ShareImage"] = 40001] = "ShareImage";
	        YDHW_TT_API[YDHW_TT_API["ShareVideo"] = 40002] = "ShareVideo";
	        YDHW_TT_API[YDHW_TT_API["ShareTemplate"] = 40003] = "ShareTemplate";
	        YDHW_TT_API[YDHW_TT_API["ShareToken"] = 40004] = "ShareToken";
	        YDHW_TT_API[YDHW_TT_API["ShowMoreGamesModal"] = 40005] = "ShowMoreGamesModal";
	        YDHW_TT_API[YDHW_TT_API["CreateMoreGamesButton"] = 40006] = "CreateMoreGamesButton";
	        YDHW_TT_API[YDHW_TT_API["RecorderStart"] = 40007] = "RecorderStart";
	        YDHW_TT_API[YDHW_TT_API["RecorderStop"] = 40008] = "RecorderStop";
	        YDHW_TT_API[YDHW_TT_API["RecorderPause"] = 40009] = "RecorderPause";
	        YDHW_TT_API[YDHW_TT_API["RecorderResume"] = 40010] = "RecorderResume";
	    })(YDHW_TT_API = YDHW.YDHW_TT_API || (YDHW.YDHW_TT_API = {}));
	    let YDHW_OPPO_API;
	    (function (YDHW_OPPO_API) {
	    })(YDHW_OPPO_API = YDHW.YDHW_OPPO_API || (YDHW.YDHW_OPPO_API = {}));
	    let YDHW_VIVO_API;
	    (function (YDHW_VIVO_API) {
	        YDHW_VIVO_API[YDHW_VIVO_API["IsStartupByShortcut"] = 50001] = "IsStartupByShortcut";
	    })(YDHW_VIVO_API = YDHW.YDHW_VIVO_API || (YDHW.YDHW_VIVO_API = {}));
	    let YDHW_MZ_API;
	    (function (YDHW_MZ_API) {
	        YDHW_MZ_API[YDHW_MZ_API["GetNetworkType"] = 140001] = "GetNetworkType";
	        YDHW_MZ_API[YDHW_MZ_API["OnNetworkStatusChange"] = 140002] = "OnNetworkStatusChange";
	    })(YDHW_MZ_API = YDHW.YDHW_MZ_API || (YDHW.YDHW_MZ_API = {}));
	    let YDHW_API;
	    (function (YDHW_API) {
	        YDHW_API[YDHW_API["ShareInfo"] = 10001] = "ShareInfo";
	        YDHW_API[YDHW_API["StatBanner"] = 10002] = "StatBanner";
	        YDHW_API[YDHW_API["StatVideo"] = 10003] = "StatVideo";
	        YDHW_API[YDHW_API["StatInterstitial"] = 10004] = "StatInterstitial";
	        YDHW_API[YDHW_API["StatGrid"] = 10005] = "StatGrid";
	        YDHW_API[YDHW_API["StatAppBox"] = 10006] = "StatAppBox";
	        YDHW_API[YDHW_API["StatBlock"] = 10007] = "StatBlock";
	        YDHW_API[YDHW_API["StatNativeAd"] = 10008] = "StatNativeAd";
	        YDHW_API[YDHW_API["StatErrorStack"] = 10009] = "StatErrorStack";
	        YDHW_API[YDHW_API["StatResult"] = 10010] = "StatResult";
	        YDHW_API[YDHW_API["StatEvent"] = 10011] = "StatEvent";
	        YDHW_API[YDHW_API["StatDuration"] = 10012] = "StatDuration";
	        YDHW_API[YDHW_API["StatClickOut"] = 10013] = "StatClickOut";
	        YDHW_API[YDHW_API["ShowShareVideo"] = 10014] = "ShowShareVideo";
	        YDHW_API[YDHW_API["UseShareVideoStrategy"] = 10015] = "UseShareVideoStrategy";
	        YDHW_API[YDHW_API["SwitchView"] = 10016] = "SwitchView";
	        YDHW_API[YDHW_API["GetDeepTouchInfo"] = 10017] = "GetDeepTouchInfo";
	        YDHW_API[YDHW_API["GetCustomConfig"] = 10018] = "GetCustomConfig";
	        YDHW_API[YDHW_API["GameOver"] = 10019] = "GameOver";
	        YDHW_API[YDHW_API["IsUnlockVideo"] = 10020] = "IsUnlockVideo";
	        YDHW_API[YDHW_API["GetPowerInfo"] = 10021] = "GetPowerInfo";
	        YDHW_API[YDHW_API["ListenOnPowerChange"] = 10022] = "ListenOnPowerChange";
	        YDHW_API[YDHW_API["SetPower"] = 10023] = "SetPower";
	        YDHW_API[YDHW_API["GetPower"] = 10024] = "GetPower";
	        YDHW_API[YDHW_API["GetSideBox"] = 10025] = "GetSideBox";
	        YDHW_API[YDHW_API["GetScoreBoardList"] = 10026] = "GetScoreBoardList";
	        YDHW_API[YDHW_API["GetScoreBoardAward"] = 10027] = "GetScoreBoardAward";
	        YDHW_API[YDHW_API["ShareCard"] = 10028] = "ShareCard";
	        YDHW_API[YDHW_API["GetTodayBoutCount"] = 10029] = "GetTodayBoutCount";
	        YDHW_API[YDHW_API["GetTotalBoutCount"] = 10030] = "GetTotalBoutCount";
	        YDHW_API[YDHW_API["GetLastBountNumber"] = 10031] = "GetLastBountNumber";
	        YDHW_API[YDHW_API["GetMaxBountNumber"] = 10032] = "GetMaxBountNumber";
	        YDHW_API[YDHW_API["GetTodayWatchVideoCounter"] = 10033] = "GetTodayWatchVideoCounter";
	        YDHW_API[YDHW_API["CreateBannerAd"] = 10034] = "CreateBannerAd";
	        YDHW_API[YDHW_API["CreateSmallBannerAd"] = 10035] = "CreateSmallBannerAd";
	        YDHW_API[YDHW_API["CreateCustomBannerAd"] = 10036] = "CreateCustomBannerAd";
	        YDHW_API[YDHW_API["BannerAdChangeSize"] = 10037] = "BannerAdChangeSize";
	        YDHW_API[YDHW_API["ShowBannerAd"] = 10038] = "ShowBannerAd";
	        YDHW_API[YDHW_API["HideBannerAd"] = 10039] = "HideBannerAd";
	        YDHW_API[YDHW_API["ChangeBannerStyle"] = 10040] = "ChangeBannerStyle";
	        YDHW_API[YDHW_API["CreateRewardVideoAd"] = 10041] = "CreateRewardVideoAd";
	        YDHW_API[YDHW_API["ShowRewardVideoAd"] = 10042] = "ShowRewardVideoAd";
	        YDHW_API[YDHW_API["StatShareCard"] = 10043] = "StatShareCard";
	        YDHW_API[YDHW_API["StatShareCardInner"] = 10044] = "StatShareCardInner";
	        YDHW_API[YDHW_API["GetLayerList"] = 10045] = "GetLayerList";
	        YDHW_API[YDHW_API["NavigateToMiniProgram"] = 10046] = "NavigateToMiniProgram";
	        YDHW_API[YDHW_API["GetUserInfo"] = 10047] = "GetUserInfo";
	        YDHW_API[YDHW_API["CreateUserInfoButton"] = 10048] = "CreateUserInfoButton";
	        YDHW_API[YDHW_API["ShowUserInfoButton"] = 10049] = "ShowUserInfoButton";
	        YDHW_API[YDHW_API["HideUserInfoButton"] = 10050] = "HideUserInfoButton";
	        YDHW_API[YDHW_API["DestroyUserInfoButton"] = 10051] = "DestroyUserInfoButton";
	        YDHW_API[YDHW_API["CreateInterstitialAd"] = 10052] = "CreateInterstitialAd";
	        YDHW_API[YDHW_API["ShowInterstitialAd"] = 10053] = "ShowInterstitialAd";
	        YDHW_API[YDHW_API["GetSharingResults"] = 10054] = "GetSharingResults";
	        YDHW_API[YDHW_API["StatLayer"] = 10055] = "StatLayer";
	        YDHW_API[YDHW_API["GetShareRewardLimit"] = 10056] = "GetShareRewardLimit";
	        YDHW_API[YDHW_API["GetVideoRewardLimit"] = 10057] = "GetVideoRewardLimit";
	        YDHW_API[YDHW_API["GetServerInfo"] = 10058] = "GetServerInfo";
	        YDHW_API[YDHW_API["ExitGame"] = 10059] = "ExitGame";
	        YDHW_API[YDHW_API["VibrateShort"] = 10060] = "VibrateShort";
	        YDHW_API[YDHW_API["VibrateLong"] = 10061] = "VibrateLong";
	        YDHW_API[YDHW_API["InstallShortcut"] = 10062] = "InstallShortcut";
	        YDHW_API[YDHW_API["CreateNativeAd"] = 10063] = "CreateNativeAd";
	        YDHW_API[YDHW_API["ShowNativeAd"] = 10064] = "ShowNativeAd";
	        YDHW_API[YDHW_API["ClickNativeAd"] = 10065] = "ClickNativeAd";
	        YDHW_API[YDHW_API["HasShortcutInstalled"] = 10066] = "HasShortcutInstalled";
	        YDHW_API[YDHW_API["ShareAppMessage"] = 10067] = "ShareAppMessage";
	        YDHW_API[YDHW_API["Hook"] = 10068] = "Hook";
	        YDHW_API[YDHW_API["OnFrontend"] = 10069] = "OnFrontend";
	        YDHW_API[YDHW_API["OnBackend"] = 10070] = "OnBackend";
	        YDHW_API[YDHW_API["OnShow"] = 10071] = "OnShow";
	        YDHW_API[YDHW_API["OnHide"] = 10072] = "OnHide";
	        YDHW_API[YDHW_API["OnError"] = 10073] = "OnError";
	        YDHW_API[YDHW_API["GetObject"] = 10074] = "GetObject";
	        YDHW_API[YDHW_API["GetString"] = 10075] = "GetString";
	        YDHW_API[YDHW_API["SetObject"] = 10076] = "SetObject";
	        YDHW_API[YDHW_API["SetString"] = 10077] = "SetString";
	        YDHW_API[YDHW_API["Size"] = 10078] = "Size";
	        YDHW_API[YDHW_API["DeleteObject"] = 10079] = "DeleteObject";
	        YDHW_API[YDHW_API["StoreValue"] = 10080] = "StoreValue";
	        YDHW_API[YDHW_API["Login"] = 10081] = "Login";
	        YDHW_API[YDHW_API["ScoreBoard"] = 10083] = "ScoreBoard";
	        YDHW_API[YDHW_API["IsCanRewardByVideoOrShare"] = 10084] = "IsCanRewardByVideoOrShare";
	        YDHW_API[YDHW_API["StatCustomAd"] = 10085] = "StatCustomAd";
	        YDHW_API[YDHW_API["OnScorecardJump"] = 10086] = "OnScorecardJump";
	        YDHW_API[YDHW_API["GetCapsulePoint"] = 10087] = "GetCapsulePoint";
	        YDHW_API[YDHW_API["GetSysIf"] = 10088] = "GetSysIf";
	    })(YDHW_API = YDHW.YDHW_API || (YDHW.YDHW_API = {}));
	    let EM_TP_AD;
	    (function (EM_TP_AD) {
	        EM_TP_AD[EM_TP_AD["CREATE"] = 0] = "CREATE";
	        EM_TP_AD[EM_TP_AD["LOAD_SUCCESS"] = 1] = "LOAD_SUCCESS";
	        EM_TP_AD[EM_TP_AD["LOAD_FAIL"] = 2] = "LOAD_FAIL";
	        EM_TP_AD[EM_TP_AD["CLICK"] = 3] = "CLICK";
	        EM_TP_AD[EM_TP_AD["SHOW"] = 4] = "SHOW";
	        EM_TP_AD[EM_TP_AD["CLOSE"] = 5] = "CLOSE";
	        EM_TP_AD[EM_TP_AD["EXPOSURE"] = 6] = "EXPOSURE";
	        EM_TP_AD[EM_TP_AD["PLAY_CANCEL"] = 7] = "PLAY_CANCEL";
	        EM_TP_AD[EM_TP_AD["PLAY_FINISH"] = 8] = "PLAY_FINISH";
	    })(EM_TP_AD = YDHW.EM_TP_AD || (YDHW.EM_TP_AD = {}));
	    let EM_SHARE_TYPE;
	    (function (EM_SHARE_TYPE) {
	        EM_SHARE_TYPE[EM_SHARE_TYPE["None"] = 0] = "None";
	        EM_SHARE_TYPE[EM_SHARE_TYPE["Share"] = 1] = "Share";
	        EM_SHARE_TYPE[EM_SHARE_TYPE["Video"] = 2] = "Video";
	    })(EM_SHARE_TYPE = YDHW.EM_SHARE_TYPE || (YDHW.EM_SHARE_TYPE = {}));
	    let EM_POWER_RECOVERY_TYPE;
	    (function (EM_POWER_RECOVERY_TYPE) {
	        EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["None"] = 0] = "None";
	        EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["WatchVideo"] = 1] = "WatchVideo";
	        EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["AutoRecovery"] = 2] = "AutoRecovery";
	        EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["CountDown"] = 3] = "CountDown";
	    })(EM_POWER_RECOVERY_TYPE = YDHW.EM_POWER_RECOVERY_TYPE || (YDHW.EM_POWER_RECOVERY_TYPE = {}));
	    let EM_VIDEO_PLAY_TYPE;
	    (function (EM_VIDEO_PLAY_TYPE) {
	        EM_VIDEO_PLAY_TYPE[EM_VIDEO_PLAY_TYPE["VIDEO_PLAY_FAIL"] = 0] = "VIDEO_PLAY_FAIL";
	        EM_VIDEO_PLAY_TYPE[EM_VIDEO_PLAY_TYPE["VIDEO_PLAY_FINISH"] = 1] = "VIDEO_PLAY_FINISH";
	        EM_VIDEO_PLAY_TYPE[EM_VIDEO_PLAY_TYPE["VIDEO_PLAY_CANCEL"] = 2] = "VIDEO_PLAY_CANCEL";
	    })(EM_VIDEO_PLAY_TYPE = YDHW.EM_VIDEO_PLAY_TYPE || (YDHW.EM_VIDEO_PLAY_TYPE = {}));
	    let WX;
	    (function (WX) {
	        class OnShareTimelineInfo {
	            constructor(imageUrl, title, query) {
	                if (imageUrl)
	                    this.imageUrl = imageUrl;
	                if (title)
	                    this.title = title;
	                if (query)
	                    this.query = query;
	            }
	        }
	        WX.OnShareTimelineInfo = OnShareTimelineInfo;
	        class GameRecorderInfo {
	            constructor(fps, duration, bitrate, gop, hookBgm) {
	                if (fps)
	                    this.fps = fps;
	                if (duration)
	                    this.duration = duration;
	                if (gop)
	                    this.gop = gop;
	                if (hookBgm)
	                    this.hookBgm = hookBgm;
	            }
	        }
	        WX.GameRecorderInfo = GameRecorderInfo;
	        class GameRecorderBtnInfo {
	            constructor(style, share, icon, image, text) {
	                if (style)
	                    this.style = style;
	                if (share)
	                    this.share = share;
	                if (icon)
	                    this.icon = icon;
	                if (image)
	                    this.image = image;
	                if (text)
	                    this.text = text;
	            }
	        }
	        WX.GameRecorderBtnInfo = GameRecorderBtnInfo;
	        class GameRecorderBtnStyle {
	            constructor(left, top, height, iconMarginRight, fontSize, color, paddingLeft, paddingRight) {
	                if (left)
	                    this.left = left;
	                if (top)
	                    this.top = top;
	                if (height)
	                    this.height = height;
	                if (iconMarginRight)
	                    this.iconMarginRight = iconMarginRight;
	                if (fontSize)
	                    this.fontSize = fontSize;
	                if (color)
	                    this.color = color;
	                if (paddingLeft)
	                    this.paddingLeft = paddingLeft;
	                if (paddingRight)
	                    this.paddingRight = paddingRight;
	            }
	        }
	        WX.GameRecorderBtnStyle = GameRecorderBtnStyle;
	        class GameRecorderShareInfo {
	            constructor(query, bgm, title, button, timeRange, volume, atempo, audioMix) {
	                if (query)
	                    this.query = query;
	                if (bgm)
	                    this.bgm = bgm;
	                if (title)
	                    this.title = title;
	                if (button)
	                    this.button = button;
	                if (timeRange)
	                    this.timeRange = timeRange;
	                if (volume)
	                    this.volume = volume;
	                if (atempo)
	                    this.atempo = atempo;
	                if (audioMix)
	                    this.audioMix = audioMix;
	            }
	        }
	        WX.GameRecorderShareInfo = GameRecorderShareInfo;
	        class GameRecorderTitle {
	            constructor(template, data) {
	                if (template)
	                    this.template = template;
	                if (data)
	                    this.data = data;
	            }
	        }
	        WX.GameRecorderTitle = GameRecorderTitle;
	        class GameRecorderBotton {
	            constructor(template) {
	                if (template)
	                    this.template = template;
	            }
	        }
	        WX.GameRecorderBotton = GameRecorderBotton;
	        class GameRecorderTitleData {
	            constructor(score, level, opponent_openid, cost_seconds) {
	                if (score)
	                    this.score = score;
	                if (level)
	                    this.level = level;
	                if (opponent_openid)
	                    this.opponent_openid = opponent_openid;
	                if (cost_seconds)
	                    this.cost_seconds = cost_seconds;
	            }
	        }
	        WX.GameRecorderTitleData = GameRecorderTitleData;
	    })(WX = YDHW.WX || (YDHW.WX = {}));
	    let TT;
	    (function (TT) {
	        class TTSideBoxInfo {
	        }
	        TT.TTSideBoxInfo = TTSideBoxInfo;
	    })(TT = YDHW.TT || (YDHW.TT = {}));
	    let QQ;
	    (function (QQ) {
	        class ShareTempletInfo {
	        }
	        QQ.ShareTempletInfo = ShareTempletInfo;
	        class SHareTemplateData {
	        }
	        QQ.SHareTemplateData = SHareTemplateData;
	        class AppMsgInfo {
	        }
	        QQ.AppMsgInfo = AppMsgInfo;
	        class AddFriendButtonInfo {
	        }
	        QQ.AddFriendButtonInfo = AddFriendButtonInfo;
	        class FbStyle {
	        }
	        QQ.FbStyle = FbStyle;
	        class BlockAdInfo {
	        }
	        QQ.BlockAdInfo = BlockAdInfo;
	        class BlockStyle {
	        }
	        QQ.BlockStyle = BlockStyle;
	        let EM_SHARE_APP_TYPE;
	        (function (EM_SHARE_APP_TYPE) {
	            EM_SHARE_APP_TYPE["QQ"] = "qq";
	            EM_SHARE_APP_TYPE["QQ_FAST_SHARE"] = "qqFastShare";
	            EM_SHARE_APP_TYPE["QQ_FAST_SHARE_LIST"] = "qqFastShareList";
	            EM_SHARE_APP_TYPE["QZONE"] = "qzone";
	            EM_SHARE_APP_TYPE["WECHARTFRIENDS"] = "wechatFriends";
	            EM_SHARE_APP_TYPE["WECHATMOMENT"] = "wechatMoment";
	        })(EM_SHARE_APP_TYPE = QQ.EM_SHARE_APP_TYPE || (QQ.EM_SHARE_APP_TYPE = {}));
	    })(QQ = YDHW.QQ || (YDHW.QQ = {}));
	    class ClickOutRequest {
	    }
	    YDHW.ClickOutRequest = ClickOutRequest;
	    class ShareAppInfo {
	    }
	    YDHW.ShareAppInfo = ShareAppInfo;
	    class StatisticResultInfo {
	    }
	    YDHW.StatisticResultInfo = StatisticResultInfo;
	    class AdStyle {
	    }
	    YDHW.AdStyle = AdStyle;
	    class SdkInfo {
	    }
	    YDHW.SdkInfo = SdkInfo;
	    class StatistiicShareInfo {
	        constructor(sharecardId, sType, target, real) {
	            this.sharecardId = sharecardId || -1;
	            if (sType != null)
	                this.sType = sType;
	            if (target != null)
	                this.target = target;
	            this.real = real || 0;
	        }
	    }
	    YDHW.StatistiicShareInfo = StatistiicShareInfo;
	    class StoreValueRequest {
	        constructor(name, cmd, args) {
	            if (name)
	                this.name = name;
	            if (cmd)
	                this.cmd = cmd;
	            if (args)
	                this.args = args;
	        }
	    }
	    YDHW.StoreValueRequest = StoreValueRequest;
	    class EditRequest {
	    }
	    YDHW.EditRequest = EditRequest;
	})(YDHW || (YDHW = {}));

	class RecordDataMgr {
	    constructor() {
	        this._myRecordIds = [];
	        this._recordDiggs = [];
	        this._recordLasts = [];
	        this.diggRefreshed = false;
	        this.lastRefreshed = false;
	        this._userInfo = null;
	    }
	    static get ins() {
	        if (!this._ins) {
	            this._ins = new RecordDataMgr();
	            this._ins.init();
	        }
	        return this._ins;
	    }
	    init() {
	        let self = this;
	        this.getRecordIds().then(() => {
	            return self.getRecordDigg();
	        }).then(() => {
	            return self.getRecordLast();
	        }).then(() => {
	            console.log("YLSDK ---RecordDataMgr init complete");
	        });
	    }
	    getRecordDigg() {
	        let self = this;
	        let selectInfo = {
	            "#sortField": "digg_count",
	            "#sortType": "-1",
	            "#limit": "100"
	        };
	        return new Promise(resolve => {
	            let args = "" + JSON.stringify(selectInfo);
	            self.StoreValue("tableRecordDigg", "select", args)
	                .then((result) => {
	                if (result && result.value && result.value.length > 0) {
	                    self._recordDiggs = result.value;
	                    EventMgr.event(EventConst.E_REFRESH_RECORD_RANK);
	                }
	                resolve();
	            });
	        });
	    }
	    getRecordLast() {
	        let self = this;
	        let selectInfo = {
	            "#sortField": "share_time",
	            "#sortType": "-1",
	            "#limit": "100"
	        };
	        return new Promise(resolve => {
	            let args = "" + JSON.stringify(selectInfo);
	            self.StoreValue("tableRecordLast", "select", args)
	                .then((result) => {
	                if (result && result.value && result.value.length > 0) {
	                    self._recordLasts = result.value;
	                    EventMgr.event(EventConst.E_REFRESH_RECORD_RANK);
	                }
	                resolve();
	            });
	        });
	    }
	    getRecordIds() {
	        let self = this;
	        let selectInfo = {
	            "#key": "user_id_" + ydhw.AccountId,
	            "#sortField": "#key",
	            "#sortType": "-1",
	            "#limit": "100"
	        };
	        return new Promise(resolve => {
	            let args = "" + JSON.stringify(selectInfo);
	            self.StoreValue("tableRecordId", "select", args)
	                .then((result) => {
	                if (result && result.value && result.value.length > 0) {
	                    let values = result.value[0];
	                    if (values.record_id && values.record_id.length > 0) {
	                        self._myRecordIds = values.record_id;
	                    }
	                }
	                resolve();
	            });
	        });
	    }
	    updateRecordLast(recordId, shareTime) {
	        let self = this;
	        self.diggRefreshed = false;
	        self.lastRefreshed = false;
	        self.getVideoInfo([recordId]).then((res) => {
	            if (res && res.data && res.data.data && res.data.data.length > 0) {
	                let result = res.data.data[0];
	                let user_id = "user_id_" + window.ydhw.AccountId;
	                let lastInfo = JSON.stringify(self.getLastInfo(result, shareTime));
	                let args = user_id + "," + lastInfo;
	                return self.StoreValue("tableRecordLast", "update", args);
	            }
	        }).then((res) => {
	            return self.getRecordLast();
	        }).then(() => {
	            self.addRecordIds(recordId);
	        });
	    }
	    getLastInfo(result, shareTime) {
	        let userInfo = this._userInfo;
	        let lastInfo = {
	            "nickName": (userInfo ? userInfo.nickName : "未知用户"),
	            "avatarUrl": (userInfo ? userInfo.avatarUrl : ""),
	            "gender": (userInfo ? userInfo.gender : 0),
	            "city": (userInfo ? userInfo.city : ""),
	            "province": (userInfo ? userInfo.province : ""),
	            "country": (userInfo ? userInfo.country : "中国"),
	            "language": (userInfo ? userInfo.language : ""),
	            "alias_id": result.alias_id || "",
	            "video_info": result.video_info,
	            "share_time": shareTime
	        };
	        return lastInfo;
	    }
	    getDiggInfo(recordInfo) {
	        let userInfo = this._userInfo;
	        let diggInfo = {
	            "nickName": (userInfo ? userInfo.nickName : "未知用户"),
	            "avatarUrl": (userInfo ? userInfo.avatarUrl : ""),
	            "gender": (userInfo ? userInfo.gender : 0),
	            "city": (userInfo ? userInfo.city : ""),
	            "province": (userInfo ? userInfo.province : ""),
	            "country": (userInfo ? userInfo.country : "中国"),
	            "language": (userInfo ? userInfo.language : ""),
	            "alias_id": recordInfo.alias_id || "",
	            "video_info": recordInfo.video_info,
	        };
	        return diggInfo;
	    }
	    addRecordIds(recordId) {
	        let self = this;
	        this._myRecordIds.push(recordId);
	        let recorderIds = { "record_id": this._myRecordIds };
	        let args = "user_id_" + ydhw.AccountId + "," + JSON.stringify(recorderIds);
	        self.StoreValue("tableRecordId", "update", args)
	            .then((res) => {
	            return self.getRecordIds();
	        }).then(() => {
	            self.updateRecordDigg();
	        });
	    }
	    updateRecordDigg() {
	        let self = this;
	        if (self._myRecordIds && self._myRecordIds.length > 0) {
	            self.getVideoInfo(self._myRecordIds).then((res) => {
	                if (res && res.data
	                    && res.data.data
	                    && res.data.data.length > 0) {
	                    let list = res.data.data;
	                    list.sort(self.compare2);
	                    let recordInfo = list[0];
	                    let user_id = "user_id_" + window.ydhw.AccountId;
	                    let diggInfo = self.getDiggInfo(recordInfo);
	                    let args = user_id + "," + JSON.stringify(diggInfo);
	                    self.StoreValue("tableRecordDigg", "update", args)
	                        .then((result) => {
	                        self.getRecordDigg();
	                    });
	                }
	            });
	        }
	    }
	    compare2(a, b) {
	        let a_diggCount = 0;
	        let b_diggCount = 0;
	        if (a.video_info && a.video_info != "") {
	            a_diggCount = a.video_info.digg_count;
	        }
	        if (b.video_info && b.video_info != "") {
	            b_diggCount = b.video_info.digg_count;
	        }
	        if (a_diggCount > b_diggCount) {
	            return 1;
	        }
	        else if (a_diggCount < b_diggCount) {
	            return -1;
	        }
	        else {
	            return 0;
	        }
	    }
	    getDiggRankInfo() {
	        let self = this;
	        return new Promise(resolve => {
	            if (self.diggRefreshed) {
	                resolve(self._recordDiggs);
	            }
	            else {
	                let _diggsIds = [];
	                self._recordDiggs.forEach(element => {
	                    if (element && element.alias_id) {
	                        _diggsIds.push(element.alias_id);
	                    }
	                });
	                if (_diggsIds.length > 0) {
	                    self.getVideoInfo(_diggsIds).then((res) => {
	                        if (res && res.data
	                            && res.data.data
	                            && res.data.data.length > 0) {
	                            let list = res.data.data;
	                            let rData = list.concat();
	                            self._recordDiggs.forEach(element => {
	                                for (let i = 0; i < rData.length; i++) {
	                                    let item = rData[i];
	                                    if (element.alias_id == item.alias_id &&
	                                        (item.video_info instanceof Object)) {
	                                        element.video_info = item.video_info;
	                                        rData.splice(i, 1);
	                                        break;
	                                    }
	                                }
	                            });
	                            self.diggRefreshed = true;
	                            resolve(self._recordDiggs);
	                        }
	                    });
	                }
	                else {
	                    resolve([]);
	                }
	            }
	        });
	    }
	    getLastRankInfo() {
	        let self = this;
	        return new Promise(resolve => {
	            if (self.lastRefreshed) {
	                resolve(self._recordLasts);
	            }
	            else {
	                let _lastsIds = [];
	                this._recordLasts.forEach(element => {
	                    if (element && element.alias_id) {
	                        _lastsIds.push(element.alias_id);
	                    }
	                });
	                if (_lastsIds.length > 0) {
	                    self.getVideoInfo(_lastsIds).then((res) => {
	                        if (res
	                            && res.data
	                            && res.data.data
	                            && res.data.data.length > 0) {
	                            let list = res.data.data;
	                            let rData = list.concat();
	                            self._recordLasts.forEach(element => {
	                                for (let i = 0; i < rData.length; i++) {
	                                    let item = rData[i];
	                                    if (element.alias_id == item.alias_id &&
	                                        (item.video_info instanceof Object)) {
	                                        element.video_info = item.video_info;
	                                        rData.splice(i, 1);
	                                        break;
	                                    }
	                                }
	                            });
	                            self.lastRefreshed = true;
	                            resolve(self._recordLasts);
	                        }
	                        else {
	                            resolve([]);
	                        }
	                    });
	                }
	                else {
	                    resolve([]);
	                }
	            }
	        });
	    }
	    getVideoInfo(_videoIds) {
	        return new Promise(resolve => {
	            if (window.ydhw_tt) {
	                window['tt'].request({
	                    url: "https://gate.snssdk.com/developer/api/get_video_info",
	                    method: "POST",
	                    data: {
	                        alias_ids: _videoIds,
	                    },
	                    success: (res) => {
	                        resolve(res);
	                    },
	                    fail(res) {
	                        resolve(null);
	                        console.log("get_video_info request fail", JSON.stringify(res));
	                    }
	                });
	            }
	            else {
	                resolve(null);
	            }
	        });
	    }
	    StoreValue(name, cmd, args, callBack) {
	        let storeInfo = new YDHW.StoreValueRequest(name, cmd, args);
	        return new Promise(resolve => {
	            ydhw.Invoke(YDHW.YDHW_API.StoreValue, storeInfo, this, (result) => {
	                console.log("StoreValue-name:", name + ",cmd:", cmd + ",result:", JSON.stringify(result));
	                if (callBack)
	                    callBack(result);
	                resolve(result);
	            });
	        });
	    }
	    set userInfo(user_info) {
	        this._userInfo = user_info;
	    }
	}

	var EM_RECORD_TYPE;
	(function (EM_RECORD_TYPE) {
	    EM_RECORD_TYPE[EM_RECORD_TYPE["None"] = 0] = "None";
	    EM_RECORD_TYPE[EM_RECORD_TYPE["Recording"] = 1] = "Recording";
	    EM_RECORD_TYPE[EM_RECORD_TYPE["Pasuse"] = 2] = "Pasuse";
	    EM_RECORD_TYPE[EM_RECORD_TYPE["Stoped"] = 3] = "Stoped";
	})(EM_RECORD_TYPE || (EM_RECORD_TYPE = {}));
	class SDKMgr {
	    constructor() {
	        this._userInfos = null;
	        this._userInfo = null;
	        this._status = 0;
	        this._recordVideoPath = null;
	        this._recordDurationEnough = false;
	        this._recordStartTime = new Date().getTime();
	        this._recordMinTime = 3000;
	        this.shareListData = [];
	    }
	    static get ins() {
	        if (!this._ins) {
	            this._ins = new SDKMgr();
	        }
	        return this._ins;
	    }
	    RecordStart() {
	        let duration = 30;
	        let self = this;
	        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderStart, duration, (result) => {
	            self._status = EM_RECORD_TYPE.Recording;
	            EventMgr.event(EventConst.E_REFRESH_RECORD_STATUS);
	            self._recordStartTime = new Date().getTime();
	            self._recordDurationEnough = false;
	        }, (videoPath) => {
	            let nowTime = new Date().getTime();
	            let timeSpace = nowTime - self._recordStartTime;
	            self._recordStartTime = nowTime;
	            if (timeSpace > self._recordMinTime) {
	                self._recordVideoPath = videoPath;
	                self._recordDurationEnough = true;
	            }
	            else {
	                self._recordDurationEnough = false;
	            }
	            self._status = EM_RECORD_TYPE.Stoped;
	            let recordInfo = {
	                video_path: videoPath,
	                time_space: timeSpace
	            };
	            EventMgr.event(EventConst.E_REFRESH_RECORD_STATUS, JSON.stringify(recordInfo));
	        }, (result) => {
	            self._status = EM_RECORD_TYPE.Recording;
	            EventMgr.event(EventConst.E_REFRESH_RECORD_STATUS);
	        }, (result) => {
	            self._status = EM_RECORD_TYPE.Pasuse;
	            EventMgr.event(EventConst.E_REFRESH_RECORD_STATUS);
	        }, (result) => {
	            console.warn("Recorder-onError:", JSON.stringify(result));
	        });
	    }
	    GetRecordPath() {
	        return this._recordVideoPath;
	    }
	    RecordStop() {
	        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderStop);
	    }
	    RecordResume() {
	        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderResume);
	    }
	    RecordPasue() {
	        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderPause);
	    }
	    GetRecordStatus() {
	        return this._status;
	    }
	    ShareRecord(callback) {
	        let shareInfo = null;
	        let title = "分享标题";
	        let description = "分享文案！";
	        let self = this;
	        if (!self._recordDurationEnough) {
	            if (callback)
	                callback(2);
	            return;
	        }
	        if (this.shareListData.length > 0) {
	            let index = Math.floor(Math.random() * this.shareListData.length);
	            shareInfo = this.shareListData[index];
	            description = shareInfo.title;
	        }
	        ydhw.Invoke(YDHW.YDHW_TT_API.ShareVideo, title, description, '', (isOk) => {
	            let result = isOk ? 1 : 0;
	            if (callback)
	                callback(result);
	            if (isOk && isOk.videoId) {
	                let videoId = isOk.videoId;
	                let nowTime = new Date().getTime();
	                RecordDataMgr.ins.updateRecordLast(videoId, nowTime);
	            }
	        });
	    }
	    JumpToVideoView(_videoId, callback) {
	        console.log("YLSDK ---JumpToVideoView-_videoId:", _videoId);
	        if (window.ydhw_tt) {
	            window['tt'].navigateToVideoView({
	                videoId: _videoId,
	                success: (res) => {
	                    console.warn("success:", JSON.stringify(res));
	                    if (callback)
	                        callback(res);
	                },
	                fail: (err) => {
	                    if (err.errCode === 1006) {
	                        console.warn("something wrong with your network");
	                    }
	                    if (callback)
	                        callback(err);
	                },
	            });
	        }
	    }
	    GetUserInfo(callback) {
	        let self = this;
	        if (self._userInfo) {
	            if (callback)
	                callback(self._userInfo);
	            return;
	        }
	        ydhw.Invoke(YDHW.YDHW_API.GetUserInfo, this, (result) => {
	            console.log("GetUserInfo-onSuccess:" + JSON.stringify(result));
	            if (result) {
	                self.userInfo = result;
	            }
	            if (callback)
	                callback(result);
	        }, (error) => {
	            console.warn("GetUserInfo-onError:", error);
	            if (callback)
	                callback(false);
	        });
	    }
	    GetUserInfoByStore(callback) {
	        let self = this;
	        let selectInfo = {
	            "#sortField": "gender",
	            "#sortType": "-1",
	            "#limit": "100"
	        };
	        this.StoreValue("tableUser", "select", "" + JSON.stringify(selectInfo), (result) => {
	            if (result && result.value && result.value.length > 0) {
	                self._userInfos = result.value;
	                SDKMgr.ins.SetUserInfoByStore(result, () => {
	                });
	            }
	            if (callback)
	                callback();
	        });
	    }
	    SetUserInfoByStore(result, callback) {
	        let self = this;
	        let args = "user_id_" + ydhw.AccountId + "," + JSON.stringify(result);
	        this.StoreValue("tableUser", "update", args, (result) => {
	            if (result && result.value && result.value.length > 0) {
	                self._userInfos = result.value;
	            }
	            if (callback)
	                callback();
	        });
	    }
	    StoreValue(name, cmd, args, callBack) {
	        let storeInfo = new YDHW.StoreValueRequest(name, cmd, args);
	        ydhw.Invoke(YDHW.YDHW_API.StoreValue, storeInfo, this, (result) => {
	            console.log("StoreValue-name:", name + ",cmd:", cmd + ",result:", JSON.stringify(result));
	            if (callBack)
	                callBack(result);
	        });
	    }
	    get userInfo() {
	        return this._userInfo;
	    }
	    set userInfo(user_info) {
	        this._userInfo = user_info;
	    }
	}
	SDKMgr._ins = null;

	let UIConst = {
	    DouyinRankView: "test/DouyinRankingView.scene",
	};

	var Handler = Laya.Handler;
	class DouyinRankView extends ui.test.DouyinRankingViewUI {
	    constructor() {
	        super(...arguments);
	        this.switchHot = true;
	    }
	    onAwake() {
	        let self = this;
	        this.btn_back.on(Laya.Event.CLICK, this, this.onClose);
	        this.btn_share.on(Laya.Event.CLICK, this, this.onShare);
	        this.lb_hot.on(Laya.Event.CLICK, this, this.clickHot);
	        this.lb_new.on(Laya.Event.CLICK, this, this.clickNew);
	        EventMgr.on(EventConst.E_REFRESH_RECORD_RANK, this, this.refreshRank);
	        self.list.vScrollBarSkin = "";
	        self.list.selectEnable = true;
	        self.list.selectHandler = new Handler(this, self.onSelect);
	        self.list.renderHandler = new Handler(this, self.updateItem);
	        this.img_hot_bg.visible = true;
	        this.img_new_bg.visible = false;
	        this.img_hot_bg.skin = "douyin/img_t.png";
	        this.img_new_bg.skin = "douyin/img_t.png";
	        this.btn_back.visible = true;
	        self.clickHot();
	    }
	    onClose() {
	        EventMgr.off(EventConst.E_REFRESH_RECORD_RANK, this, this.refreshRank);
	        Laya.Dialog.close(UIConst.DouyinRankView);
	    }
	    onShare() {
	        let self = this;
	        if (SDKMgr.ins.GetRecordPath()) {
	            SDKMgr.ins.ShareRecord((res) => {
	                if (res == 1) {
	                    console.warn("分享成功，获得奖励！");
	                }
	                else if (res == 2) {
	                    console.warn("时长未满3秒，无法分享！");
	                }
	                else {
	                    console.warn("分享失败！");
	                }
	            });
	        }
	        else {
	            console.warn("录屏地址不存在！");
	        }
	    }
	    clickHot() {
	        let self = this;
	        let type = 1;
	        self.list.array = [];
	        this.img_hot_bg.visible = type == 1;
	        this.img_new_bg.visible = type != 1;
	        this.switchHot = true;
	        RecordDataMgr.ins.getDiggRankInfo().then((data) => {
	            let list = data.concat();
	            self.list.array = list;
	        });
	    }
	    clickNew() {
	        let self = this;
	        let type = 2;
	        self.list.array = [];
	        this.img_hot_bg.visible = type == 1;
	        this.img_new_bg.visible = type != 1;
	        this.switchHot = false;
	        RecordDataMgr.ins.getLastRankInfo().then((data) => {
	            let list = data.concat();
	            self.list.array = list;
	        });
	    }
	    refreshRank() {
	        if (this.img_hot_bg.visible) {
	            this.clickHot();
	        }
	        else {
	            this.clickNew();
	        }
	    }
	    onSelect(index) {
	        if (index < 0)
	            return;
	        SDKMgr.ins.JumpToVideoView(this.list.selectedItem.alias_id, () => {
	        });
	        this.list.selectedIndex = -1;
	    }
	    updateItem(cell, index) {
	        cell.setData(cell.dataSource, index, this.switchHot);
	    }
	}

	class DouyinRankItem extends ui.test.DouyinRankItemUI {
	    onAwake() {
	    }
	    setData(data, index, switchHot) {
	        let _coverUrl = "";
	        let _diggCount = 0;
	        let _rankText = "";
	        let _shareTiem = "";
	        let _nickName = "未知用户";
	        let _avatarUrl = data.avatarUrl;
	        if (data.video_info) {
	            _diggCount = data.video_info.digg_count || 0;
	            _coverUrl = data.video_info.cover_url || "";
	        }
	        if (data.nickName)
	            _nickName = data.nickName;
	        try {
	            if (!switchHot) {
	                _shareTiem = data.share_time ? this.formatDate(new Date(data.share_time), "YY-MM-dd") : "";
	            }
	        }
	        catch (e) { }
	        _rankText = switchHot ? "" + _diggCount : _shareTiem;
	        this.img_icon.skin = _coverUrl;
	        var self = this, imgIcon = self.img_head;
	        var mask = imgIcon.mask = new Laya.Sprite;
	        this.drawRoundRect(mask.graphics, imgIcon.width, imgIcon.height, 20);
	        _avatarUrl = (_avatarUrl && _avatarUrl != "") ? _avatarUrl : "wxlocal/share1.jpg";
	        ;
	        this.img_head.skin = _avatarUrl;
	        this.lb_name.text = _nickName;
	        this.lb_rank.text = _rankText;
	        let top = index + 1;
	        this.lb_top_num.text = "" + top;
	        let skinStr = "douyin/img_ns.png";
	        switch (top) {
	            case 1:
	                skinStr = "douyin/img_hs.png";
	                break;
	            case 2:
	                skinStr = "douyin/img_ls.png";
	                break;
	            case 3:
	                skinStr = "douyin/img_ss.png";
	                break;
	        }
	        this.img_top_img.skin = skinStr;
	    }
	    formatDate(date, fmt) {
	        let o = {
	            "Y+": date.getFullYear(),
	            "M+": date.getMonth() + 1,
	            "d+": date.getDate(),
	            "h+": date.getHours(),
	            "m+": date.getMinutes(),
	            "s+": date.getSeconds(),
	            "S": date.getMilliseconds()
	        };
	        if (/(y+)/.test(fmt))
	            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
	        for (let k in o)
	            if (new RegExp("(" + k + ")").test(fmt))
	                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
	        return fmt;
	    }
	    drawRoundRect(graphics, width, height, round) {
	        var paths = [
	            ['moveTo', round, 0],
	            ['lineTo', width - round, 0],
	            ['arcTo', width, 0, width, round, round],
	            ['lineTo', width, height - round],
	            ['arcTo', width, height, width - round, height, round],
	            ['lineTo', round, height],
	            ['arcTo', 0, height, 0, height - round, round],
	            ['lineTo', 0, round],
	            ['arcTo', 0, 0, round, 0, round]
	        ];
	        graphics.drawPath(0, 0, paths, { fillStyle: "#0" });
	    }
	}

	class GameUI extends ui.test.TestRecordRankUI {
	    constructor() {
	        super();
	        GameUI.instance = this;
	        Laya.MouseManager.multiTouchEnabled = false;
	    }
	    onEnable() {
	        console.log("YDHW ---YDHW_CONFIG:" + window['YDHW_CONFIG'].platformCode);
	        this.initUI();
	        this.init();
	    }
	    init() {
	        ydhw.Invoke(YDHW.YDHW_API.Login, this, (isOk) => {
	            if (isOk) {
	                console.log("PlatformPublic -Login-isOk: " + isOk);
	                RecordDataMgr.ins;
	                this.TT_GetPlatformUserInfo(null);
	            }
	        });
	    }
	    initUI() {
	        this.initTTplatformUI();
	        this.txt_platfrom_name.text = "头条平台";
	        this.lb_tt_node.visible = true;
	    }
	    initTTplatformUI() {
	        this.lb_tt_node.visible = true;
	        this.txt_record_type.text = "";
	        this.btnTTrecordStart.on(Laya.Event.CLICK, this, this.TT_RecordStart);
	        this.btnTTrecordStop.on(Laya.Event.CLICK, this, this.TT_RecordStop);
	        this.btnTTrecordPasueOrResume.on(Laya.Event.CLICK, this, this.TT_RecordPasueOrResume);
	        this.btnTTshareVideo.on(Laya.Event.CLICK, this, this.TT_ShareVideo);
	        this.btnTTshareToken.on(Laya.Event.CLICK, this, this.TT_ShareToken);
	        this.btnTTgetUserInfo.on(Laya.Event.CLICK, this, this.TT_GetPlatformUserInfo);
	        this.TT_RefreshRecordStatus(null);
	        EventMgr.on(EventConst.E_REFRESH_RECORD_STATUS, this, this.TT_RefreshRecordStatus);
	    }
	    TT_RecordStart(e) {
	        SDKMgr.ins.RecordStart();
	    }
	    TT_RecordStop(e) {
	        console.log("YDHW 停止录屏");
	        SDKMgr.ins.RecordStop();
	    }
	    TT_RecordPasueOrResume(e) {
	        let status = SDKMgr.ins.GetRecordStatus();
	        if (status === 2) {
	            SDKMgr.ins.RecordResume();
	        }
	        else {
	            SDKMgr.ins.RecordPasue();
	        }
	    }
	    TT_RefreshRecordStatus(info) {
	        let status = SDKMgr.ins.GetRecordStatus();
	        let statusS = "录屏-无";
	        let sPoR = "继续录屏";
	        switch (status) {
	            case 0:
	                statusS = "录屏-无";
	                sPoR = "继续录屏";
	                break;
	            case 1:
	                statusS = "录屏中";
	                sPoR = "暂停录屏";
	                break;
	            case 2:
	                statusS = "录屏-暂停";
	                sPoR = "继续录屏";
	                break;
	            case 3:
	                statusS = "录屏-停止:" + info;
	                sPoR = "继续录屏";
	                break;
	        }
	        this.btnTTrecordPasueOrResume.label = sPoR;
	        this.txt_record_type.text = statusS;
	    }
	    TT_ShareVideo(e) {
	        SDKMgr.ins.ShareRecord(() => {
	        });
	    }
	    TT_ShareToken(e) {
	        Laya.Dialog.open(UIConst.DouyinRankView);
	    }
	    TT_GetPlatformUserInfo(e) {
	        SDKMgr.ins.GetUserInfo(() => { });
	    }
	}

	class GameConfig {
	    constructor() {
	    }
	    static init() {
	        var reg = Laya.ClassUtils.regClass;
	        reg("script/douyin/DouyinRankView.ts", DouyinRankView);
	        reg("script/douyin/DouyinRankItem.ts", DouyinRankItem);
	        reg("script/RecordRank.ts", GameUI);
	    }
	}
	GameConfig.width = 750;
	GameConfig.height = 1334;
	GameConfig.scaleMode = "fixedwidth";
	GameConfig.screenMode = "none";
	GameConfig.alignV = "top";
	GameConfig.alignH = "left";
	GameConfig.startScene = "test/DouyinRankItem.scene";
	GameConfig.sceneRoot = "";
	GameConfig.debug = false;
	GameConfig.stat = false;
	GameConfig.physicsDebug = false;
	GameConfig.exportSceneToJson = true;
	GameConfig.init();

	class Main {
	    constructor() {
	        if (window["Laya3D"])
	            Laya3D.init(GameConfig.width, GameConfig.height);
	        else
	            Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
	        Laya["Physics"] && Laya["Physics"].enable();
	        Laya["DebugPanel"] && Laya["DebugPanel"].enable();
	        Laya.stage.scaleMode = GameConfig.scaleMode;
	        Laya.stage.screenMode = GameConfig.screenMode;
	        Laya.stage.alignV = GameConfig.alignV;
	        Laya.stage.alignH = GameConfig.alignH;
	        Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
	        if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
	            Laya.enableDebugPanel();
	        if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
	            Laya["PhysicsDebugDraw"].enable();
	        if (GameConfig.stat)
	            Laya.Stat.show();
	        Laya.alertGlobalError = true;
	        Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
	    }
	    onVersionLoaded() {
	        Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
	    }
	    onConfigLoaded() {
	        Laya.Scene.open('test/TestRecordRank.scene');
	    }
	}
	new Main();

}());
