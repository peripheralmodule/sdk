 //------------------------字节跳动------------------------------------- 
  var YDHW_CONFIG = {
    appid: "tt711307aeee39fe2d",
    version: '1.0.0',
    banner_ad_unit_id_list: ['15926jnded313m55ms'],//Banner广告
    interstitial_ad_unit_id_list: ['b5a2vkpi85b7f203df'],//插屏广告
    spread_ad_unit_id_list: [],//开屏广告
    native_ad_unit_id_list: [],//原生广告
    video_ad_unit_id_list: ['6sn234aoqf3737fmu9'],//视频广告
    grid_ad_unit_id_list: ['adunit-8b60da8bb5f96948'],//格子广告
    tt_template_id_list: ['of6b158hdol2r5berq'],//TT 分享素材模板ID列表
    interstitialAd_first_show_wait_time: 10,//插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
    interstitialAd_show_time_interval: 10,//插屏广告-两次展示之间时间间隔（秒）
    side_box_count: 20,//侧边栏列表item最小保留数(基于曝光策略)
    pkg_name: "",  //包名OPPO、VOVP、魅族平台需要

    //--------------以下配置CP无需理会------------
    project: '',
	platform: 'tt',
    env: 'dev',//online or dev
    debug: true,
    platformCode:4,
    inspector: false,
    engine: 'laya',
    res_version: '20200310',
    appkey: "30248858",
    resource_url: 'http://127.0.0.1:3100',
    scene_white_list: [1005, 1006, 1037, 1035],
};
  
window.YDHW_CONFIG = YDHW_CONFIG;
loadLib("tt/ydhw.tt.sdk.js");
// loadLib("tt/ydhw.tt.sdk.min.js");


/**
 * 设置LayaNative屏幕方向，可设置以下值
 * landscape           横屏
 * portrait            竖屏
 * sensor_landscape    横屏(双方向)
 * sensor_portrait     竖屏(双方向)
 */
window.screenOrientation = "portrait";

//-----libs-begin-----
loadLib("libs/laya.core.js")
loadLib("libs/laya.ui.js")
loadLib("libs/laya.physics.js")
//-----libs-end-------
loadLib("js/bundle.js");
