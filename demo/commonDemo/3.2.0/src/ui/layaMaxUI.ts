/**This class is automatically generated by LayaAirIDE, please do not make any modifications. */
import View=Laya.View;
import Dialog=Laya.Dialog;
import Scene=Laya.Scene;
var REG: Function = Laya.ClassUtils.regClass;
export module ui.test {
    export class TestSceneUI extends Laya.Scene {
		public scoreLbl:Laya.Label;
		public txt_platfrom_name:Laya.Label;
		public btnCreateBanner:Laya.Button;
		public btnShowBanner:Laya.Button;
		public btnBannerAdChangeSize:Laya.Button;
		public btnHideBanner:Laya.Button;
		public btnShowInterstitial:Laya.Button;
		public btnCreateInterstitialAd:Laya.Button;
		public btnShowVideoAd:Laya.Button;
		public btnShowShareOrVideo:Laya.Button;
		public btnOver:Laya.Button;
		public btnDeepTouch:Laya.Button;
		public btnPowerAdd:Laya.Button;
		public btnShare:Laya.Button;
		public btnPowerReduce:Laya.Button;
		public btnUnlockCustomByVideo:Laya.Button;
		public btnSwicthView:Laya.Button;
		public btnVibrateShort:Laya.Button;
		public btnVibrateLong:Laya.Button;
		public btnExit:Laya.Button;
		public btnNatigateToMiniProgram:Laya.Button;
		public btnInstallShortcut:Laya.Button;
		public btnHasShortcutInstalled:Laya.Button;
		public btnGetUserInfo:Laya.Button;
		public lb_tt_node:Laya.Label;
		public btnTTrecordStart:Laya.Button;
		public btnTTrecordStop:Laya.Button;
		public btnTTrecordPasueOrResume:Laya.Button;
		public btnTTshareVideo:Laya.Button;
		public btnTTshareToken:Laya.Button;
		public btnTTshareTemplate:Laya.Button;
		public btnTTshowMoreGame:Laya.Button;
		public btnTTgetLeftTopBtnPosition:Laya.Button;
		public lb_wx_node:Laya.Label;
		public btnWXcreateGridAd:Laya.Button;
		public btnWXshowGridAd:Laya.Button;
		public btnWXhideGridAd:Laya.Button;
		public btnWXsubscribeSysMsg:Laya.Button;
		public btnWXgetSetting:Laya.Button;
		public btnWXshowUserInfoButton:Laya.Button;
		public btnWXHideUserInfoButton:Laya.Button;
		public btnGameRecorderStartOrStop:Laya.Button;
		public btnGameRecorderPauseOrResume:Laya.Button;
		public btnGameRecorderAbort:Laya.Button;
		public btnCreateGameRecorderShareButton:Laya.Button;
		public btnGameRecorderShareButtonOnTap:Laya.Button;
		public btnHideCustomAd:Laya.Button;
		public btnShowCustomAd:Laya.Button;
		public lb_qq_node:Laya.Label;
		public btnShowAppBoxAd:Laya.Button;
		public btnShowBlockAd:Laya.Button;
		public btnHideBlockAd:Laya.Button;
		public btnShareTemplate:Laya.Button;
		public btnColorSign:Laya.Button;
		public btnSubscribeAppMsg:Laya.Button;
		public btnHideAddFriend:Laya.Button;
		public btnAddFriend:Laya.Button;
		public lb_oppo_node:Laya.Label;
		public btnCreateNativeAd:Laya.Button;
		public btnShowNativeAd:Laya.Button;
		public btnClickNativeAd:Laya.Button;
		public btn_creatBoxBanner:Laya.Button;
		public btn_creatBox9:Laya.Button;
		public btn_ShowBanner:Laya.Button;
		public lb_meizu_node:Laya.Label;
		public btnOnNetworkStatusChange:Laya.Button;
		public btnGetSystemInfoSync:Laya.Button;
		public btnGetNetworkType:Laya.Button;
		public lb_vivo_node:Laya.Label;
		public btnCreateNativeAd_VIVO:Laya.Button;
		public btnShowNativeAd_VIVO:Laya.Button;
		public btnClickNativeAd_VIVO:Laya.Button;
        constructor(){ super()}
        createChildren():void {
            super.createChildren();
            this.loadScene("test/TestScene");
        }
    }
    REG("ui.test.TestSceneUI",TestSceneUI);
}