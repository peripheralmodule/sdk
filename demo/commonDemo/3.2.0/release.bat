﻿REM 声明采用UTF-8编码
chcp 65001
@echo off

set releaseDir=%cd%\release\
set sdkDir=%cd%\sdk

:star
cls
echo 发布平台:
echo    1.微信(wx)
echo    2.QQ(qq)
echo    3.Oppo(op)
echo    4.Vivo(vv)
echo    5.头条(tt)
echo    6.百度(bd)
echo    7.小米(xm)
echo 0.退出

set /p a=请选择:
if %a%==1 goto wx
if %a%==2 goto qq
if %a%==3 goto op
if %a%==4 goto vv
if %a%==5 goto tt
if %a%==6 goto bd
if %a%==7 goto xm
if %a%==0 goto end

REM 微信--------------------------------------------------------------------------------
:wx
echo 复制SDK文件
set dir=%cd%\release\wxgame
xcopy %sdkDir%\wx\* %dir%\* /e /y /exclude:%sdkDir%\wx\uncopy.txt
echo 游戏代码混淆
cmd /c gulp default
goto end

REM QQ--------------------------------------------------------------------------------
:qq
set dir=%cd%\release\qq_mini
xcopy %sdkDir%\qq\* %dir%\* /e /y
del /f /s /q /a %dir%\qq_platform-ad52b13e8b.js
del /f /s /q /a %dir%\qq-adapter-1e13299df0.js
echo 游戏代码混淆
cmd /c gulp qqmini
goto common

REM OPPO--------------------------------------------------------------------------------
:op
set dir=%cd%\release\oppogame\quickgame
xcopy %sdkDir%\common\* %dir%\* /e /y
xcopy %sdkDir%\op\* %dir%\* /e /y
echo 游戏代码混淆
cmd /c gulp oppo
del /f /s /q /a %dir%\merge.tfp



echo 打包 release 签名的 rpk
cd %dir%
quickgame pack release & adb push .\dist\ydhw.sqpd.nearme.gamecenter.signed.rpk sdcard/games & @pause


REM 头条--------------------------------------------------------------------------------
:tt
set dir3=%cd%\release

echo 删除tt_mini目录
if exist %dir3%\tt_mini (
rd /s /q %dir3%\tt_mini
)

pause

md %cd%\release\tt_mini
set dir2=%cd%\release
set dir3=%cd%\release\tt_mini
xcopy %cd%\release\wxgame\* %dir3%\ /e /y

set dir=%cd%\release\tt_mini
del /f /s /q /a %dir%\libs\laya.wxmini.js
del /f /s /q /a %dir%\js\bundle.js.map
del /f /s /q /a %dir%\js\bundle.js.map

echo 游戏代码混淆
cmd /c gulp tt
rd /s /q %dir%\libs
xcopy %sdkDir%\tt\* %dir%\ /e /y
xcopy %sdkDir%\common\* %dir%\ /e /y


echo 删除nativescene目录
if exist %dir%\nativescene (
rd /s /q %dir%\nativescene
)

pause
goto common
echo 头条版本完成
