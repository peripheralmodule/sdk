/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./sdk_vivo/Main.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./fw/Assist/Http.ts":
/*!***************************!*\
  !*** ./fw/Assist/Http.ts ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Http; });
/* harmony import */ var _sdk_Assist_Log__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../sdk/Assist/Log */ "./sdk/Assist/Log.ts");
/* harmony import */ var _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../sdk/SDK/Declare */ "./sdk/SDK/Declare.ts");


class Http {
    static Request(platform, url, retryCount = 1, isDebug = false) {
        let xhr = new XMLHttpRequest();
        // if (isDebug) {
        //     isDebug && Log.Debug("http url:" + url);
        // }
        let urlString = url.Value();
        //Browser.onVVMiniGame || Browser.onQGMiniGame || Browser.onQQMiniGame || Browser.onAlipayMiniGame || Browser.onBLMiniGame || Browser.onHWMiniGame || Browser.onTTMiniGame
        if (platform.IsVivo || platform.IsQGMiniGame || platform.IsQQMiniGame || platform.IsAlipay || platform.IsToutiao /**B站，华为 */) {
            urlString = url.EncodeURI();
        }
        if (platform.IsOnMobile) {
            xhr.open(url.Method(), urlString);
        }
        else {
            xhr.open(url.Method(), urlString, url.IsAsync());
        }
        let header = url.Header();
        for (const key in header) {
            xhr.setRequestHeader(key, header[key]);
        }
        xhr.onerror = function (e) {
            if (retryCount > 1) {
                retryCount--;
                Http.Request(platform, url, retryCount, isDebug);
            }
            else {
                isDebug && _sdk_Assist_Log__WEBPACK_IMPORTED_MODULE_0__["Log"].Error("Http OnError:" + e);
                url.InvokeError(e + "");
            }
        };
        xhr.onabort = function (e) {
            url.InvokeException(e);
        };
        xhr.onprogress = function (e) {
        };
        xhr.onreadystatechange = function () {
            if (this.status === 200) {
            }
            else {
                isDebug && _sdk_Assist_Log__WEBPACK_IMPORTED_MODULE_0__["Log"].Debug("http request status:" + this.status);
            }
        };
        xhr.onload = function () {
            try {
                url.Print();
                let response = JSON.parse(this.responseText);
                url.InvokeReceive(response);
            }
            catch (error) {
                console.error("xhr.onload error:" + error);
            }
        };
        try {
            let content = JSON.stringify(url.Data());
            if (platform.IsOnMobile) {
                xhr.send(content);
            }
            else {
                // xhr.send(url.Data());
                xhr.send(content);
            }
            let urlS = url.CheckValue();
            if (urlS.indexOf("statistics/duration") == -1) { //时长统计不打印，太频繁了
                isDebug && _sdk_Assist_Log__WEBPACK_IMPORTED_MODULE_0__["Log"].Log("Request=" + urlS, content);
            }
        }
        catch (e) {
            url.InvokeException(e);
        }
    }
    static RequestNormal(platform, url, retryCount = 1, isDebug = false) {
        let xhr = new XMLHttpRequest();
        // if (isDebug) {
        //     isDebug && Log.Debug("http url:" + url);
        // }
        let urlString = url.CustomUrl();
        if (platform.IsOnMobile) {
            xhr.open(url.Method(), urlString);
        }
        else {
            xhr.open(url.Method(), urlString, url.IsAsync());
        }
        let header = url.Header();
        for (const key in header) {
            xhr.setRequestHeader(key, header[key]);
        }
        xhr.onerror = function (e) {
            if (retryCount > 1) {
                retryCount--;
                Http.RequestNormal(platform, url, retryCount, isDebug);
            }
            else {
                isDebug && _sdk_Assist_Log__WEBPACK_IMPORTED_MODULE_0__["Log"].Error("Http OnError:" + e);
                url.InvokeError(e + "");
            }
        };
        xhr.onabort = function (e) {
            url.InvokeException(e);
        };
        xhr.onprogress = function (e) {
        };
        xhr.onreadystatechange = function () {
            if (this.status === 200) {
            }
            else {
                isDebug && _sdk_Assist_Log__WEBPACK_IMPORTED_MODULE_0__["Log"].Debug("http request status:" + this.status);
            }
        };
        xhr.onload = function () {
            try {
                url.Print();
                _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].W("YDHW Response-" + url + "-data:", this.responseText);
                let response = this.responseText;
                url.InvokeReceive(response);
            }
            catch (error) {
                console.error("xhr.onload error:" + error);
            }
        };
        try {
            xhr.send();
        }
        catch (e) {
            url.InvokeException(e);
        }
    }
}


/***/ }),

/***/ "./fw/Assist/StorageApt.ts":
/*!*********************************!*\
  !*** ./fw/Assist/StorageApt.ts ***!
  \*********************************/
/*! exports provided: StorageApt */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageApt", function() { return StorageApt; });
class StorageApt {
}
StorageApt.SDK_CONFIG = "YDHW_CONFIG";
StorageApt.WriteString = null;
StorageApt.WriteObject = null;
StorageApt.ReadString = null;
StorageApt.ReadObject = null;
StorageApt.DeleteObject = null;


/***/ }),

/***/ "./fw/Core.ts":
/*!********************!*\
  !*** ./fw/Core.ts ***!
  \********************/
/*! exports provided: Method, Wrapper, Variable, VariableContainer, MethodContainer, Core, BaseManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Method", function() { return Method; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Wrapper", function() { return Wrapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Variable", function() { return Variable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VariableContainer", function() { return VariableContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MethodContainer", function() { return MethodContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Core", function() { return Core; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseManager", function() { return BaseManager; });
/* harmony import */ var _Assist_StorageApt__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Assist/StorageApt */ "./fw/Assist/StorageApt.ts");
/* harmony import */ var _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Log/FrameworkLog */ "./fw/Log/FrameworkLog.ts");


class Method {
    constructor(c, m, owner) {
        this.c = c;
        this.m = m;
        this._Owner = owner;
    }
    Log() {
        let tmpString = "";
        tmpString += "/ncaller:" + this.c.name;
        tmpString += "/nmethod:" + this.m.name;
        if (this.args) {
            for (let i = 0; i < this.args.length; i++) {
                let arg = this.args[i];
                tmpString += `/r/n arg[${i}]=${arg.name}`;
            }
        }
        if (this._Owner) {
            tmpString += "/n Owner:" + this._Owner;
        }
        _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FWLog"].D(tmpString);
    }
    Destroy() {
        this.c = null;
        this.m = null;
    }
    // Invoke(...args: any[]): void {
    // }
    Bind(c, binding, ...args) {
        this.c = c;
        this.m = binding;
        this.args = args;
        return this;
    }
}
//Wrapper----------------------------------------------------------------------------
class Wrapper extends Array {
    constructor(capacity = 64) {
        super();
        this._Size = 0;
        this.length = capacity;
    }
    RemoveAt(index) {
        const e = this[index]; // make copy of element to remove so it can be returned
        this[index] = this[--this._Size]; // overwrite item to remove with last element
        this[this._Size] = null; // null last element, so gc can do its work
        return e;
    }
    Remove(e) {
        let i;
        let e2;
        const size = this._Size;
        for (i = 0; i < size; i++) {
            e2 = this[i];
            if (e == e2) {
                this[i] = this[--this._Size]; // overwrite item to remove with last element
                this[this._Size] = null; // null last element, so gc can do its work
                return true;
            }
        }
        return false;
    }
    RemoveLast() {
        if (this._Size > 0) {
            const e = this[--this._Size];
            this[this._Size] = null;
            return e;
        }
        return null;
    }
    Contains(e) {
        let i;
        let size;
        for (i = 0, size = this._Size; size > i; i++) {
            if (e === this[i]) {
                return true;
            }
        }
        return false;
    }
    RemoveAll(bag) {
        let modified = false;
        let i;
        let j;
        let l;
        let e1;
        let e2;
        for (i = 0, l = bag.Size; i < l; i++) {
            e1 = bag.Get(i);
            for (j = 0; j < this._Size; j++) {
                e2 = this[j];
                if (e1 === e2) {
                    this.RemoveAt(j);
                    j--;
                    modified = true;
                    break;
                }
            }
        }
        return modified;
    }
    Get(index) {
        if (index >= this.length) {
            throw new Error('ArrayIndexOutOfBoundsException');
        }
        return this[index];
    }
    SafeGet(index) {
        if (index >= this.length) {
            this.Grow((index * 7) / 4 + 1);
        }
        return this[index];
    }
    get Size() {
        return this._Size;
    }
    GetCapacity() {
        return this.length;
    }
    IsIndexWithinBounds(index) {
        return index < this.GetCapacity();
    }
    IsEmpty() {
        return this._Size == 0;
    }
    Add(e) {
        // is size greater than capacity increase capacity
        if (this._Size === this.length) {
            this.Grow();
        }
        this[this._Size++] = e;
    }
    Set(index, e) {
        if (index >= this.length) {
            this.Grow(index * 2);
        }
        this._Size = index + 1;
        this[index] = e;
    }
    Grow(newCapacity = ~~((this.length * 3) / 2) + 1) {
        this.length = ~~newCapacity;
    }
    EnsureCapacity(index) {
        if (index >= this.length) {
            this.Grow(index * 2);
        }
    }
    Clear() {
        let i;
        let size;
        // null all elements so gc can clean up
        for (i = 0, size = this._Size; i < size; i++) {
            this[i] = null;
        }
        this._Size = 0;
    }
    AddAll(items) {
        let i;
        for (i = 0; items.Size > i; i++) {
            this.Add(items.Get(i));
        }
    }
}
//Variable----------------------------------------------------------------------------
class Variable {
    constructor(varId, owner, value) {
        // c: any;
        // args: any[];
        this._id = 0;
        this._IsStore = false;
        this._ListCaller = {};
        this._LBEvent = {}; //_ListBindingEvent
        this._ListArgs = {}; //Wrapper<any> = new Wrapper<any>();
        this._Owner = owner;
        this._Id = varId;
        this._Value = value;
        this._Type = typeof (value);
        // this._ListCaller = new Wrapper<any>();
        // this._ListBindingEvent = new Wrapper<IBindingEvent>();
        // this._ListArgs = new Wrapper<any>();
    }
    Log() {
        let tmpString = "";
        if (this._Owner) {
            tmpString += "Owner: " + this._Owner;
        }
        tmpString += "\nType: " + this._Type;
        tmpString += "\nVariableId: " + this._Id;
        tmpString += "\nValue: " + this._Value;
        tmpString += "\n";
        _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FWLog"].D(tmpString);
    }
    SetOwner(owner) {
        this._Owner = owner;
        return this;
    }
    Owner() {
        return this._Owner;
    }
    Store() {
        this._IsStore = true;
        return this;
    }
    IsStore() {
        return this._IsStore;
    }
    Data() {
        let data = {
            manager: this.Owner,
        };
    }
    Revert(value) {
        this._Value = value;
        this._Type = typeof (value);
    }
    Id() {
        return this._Id;
    }
    SetValue(value) {
        let oldValue = this._Value;
        this._Value = value;
        this._Type = typeof (value);
        this._EmitValueChanged(oldValue);
        /*todo:optimize
        if (this._Value !== value) {
            let oldValue = this._Value;
            this._Value = value;
            this._Type = typeof (value);
            this._EmitValueChanged(this.c, oldValue);
        }
        */
        return this;
    }
    Value() {
        return this._Value;
    }
    Type() {
        return this._Type;
    }
    Bind(c, bindingEvent, ...args) {
        // this._ListCaller.Remove(c);
        this._id++;
        this._ListCaller[this._id + ""] = c;
        // this._ListBindingEvent.Remove(bindingEvent);
        this._LBEvent[this._id + ""] = bindingEvent;
        this._ListArgs[this._id + ""] = args;
        // if (args && args.length > 0) {
        //     this._ListArgs.Remove(args);
        // this._ListArgs.Add(args);
        // }
    }
    Assign() {
        this._EmitValueChanged(this._Value);
    }
    Unbind(c, bindingEvent) {
        // this._ListCaller.Remove(c);
        for (let key in this._ListCaller) {
            // this._ListCaller[key] = null;
            delete this._ListCaller[key];
        }
        for (let key in this._LBEvent) {
            // this._ListCaller[key] = null;
            delete this._LBEvent[key];
        }
        // this._ListBindingEvent.Remove(bindingEvent);
    }
    _EmitValueChanged(oldValue) {
        for (let key in this._LBEvent) {
            let c = this._ListCaller[key];
            let event = this._LBEvent[key];
            let args = this._ListArgs[key];
            let tmpList = [];
            tmpList.push(this._Value);
            for (let index in args) {
                tmpList.push(args[index]);
            }
            event.apply(c, tmpList);
        }
        // for (let i = 0, count = Object.keys(this._ListBindingEvent).length; i < count; i++) {
        //     let c: any = this._ListCaller[i];
        //     let event: IBindingEvent = this._ListBindingEvent[i];
        //     let args: any[] = this._ListArgs[i];
        //     let tmpList = [];
        //     tmpList.push(this._Value);
        //     for (let index in args) {
        //         tmpList.push(args[index]);
        //     }
        //     event.apply(c, tmpList);
        //     // event.call(c, this._Value, args);
        // }
    }
}
//VariableContainer----------------------------------------------------------------------------
class VariableContainer {
    constructor() {
        this._DictVariable = {}; //用对象属性来作为Dictionary的使用
        /**
         * 默认为还未恢复过数据，如果触发过恢复数据后则为true，后面就不再二次恢复。
         */
        this._IsRestore = false;
    }
    CheckAllVariables() {
        _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FWLog"].D("Check all variables");
        let tmpDict = this._DictVariable;
        for (let key in tmpDict) {
            let variable = tmpDict[key];
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FWLog"].D("Variable:" + key);
            variable.Log();
        }
    }
    _GV(varId, owner) {
        let tmpDict = this._DictVariable;
        let ppt = tmpDict[varId];
        if (ppt == undefined || ppt == null) {
            // if (ppt == null) {
            //     //todo:暂时待定
            //     //console.warn("1 不存在属性变量:" + varId);
            // } else {
            //     tmpDict[varId] = ppt;
            // }
            return null;
        }
        return ppt;
    }
    //GetVariable
    GV(varId, owner) {
        let tmpDict = this._DictVariable;
        let ppt = tmpDict[varId];
        if (ppt == null) {
            let variable = new Variable(varId, owner, null);
            ppt = this.RevertVariable(variable);
            if (ppt == null) {
                //todo:暂时待定
                // console.warn("2 不存在属性变量:" + varId);
            }
            else {
                tmpDict[varId] = ppt;
            }
        }
        return ppt;
    }
    //ModifyVariable
    MV(variable, value) {
        if (variable != null) {
            variable.SetValue(value);
            if (variable.IsStore()) {
                this.SaveV(variable.Id());
            }
        }
        else {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FWLog"].E("修改的变量不存在");
        }
    }
    DV(varId, owner, value) {
        if (varId == null || varId.toString().length == 0) {
            //todo:error
            return null;
        }
        else {
            let ppt = this._GV(varId);
            if (ppt != null) {
                ppt.SetValue(value);
            }
            else {
                ppt = new Variable(varId, owner, value);
                this._DictVariable[varId] = ppt;
            }
            return ppt;
        }
    }
    //RemoveVariable
    RemoveV(varId) {
        let tmpDict = this._DictVariable;
        let ppt = tmpDict[varId];
        if (ppt != null) {
            delete tmpDict[varId]; //删除对象属性
            _Assist_StorageApt__WEBPACK_IMPORTED_MODULE_0__["StorageApt"].DeleteObject(varId + "");
            // Laya.LocalStorage.removeItem(varId);
        }
        else {
            //todo:warn
        }
    }
    _GVV(varId) {
        let ppt = this.GV(varId);
        return ppt == null ? null : ppt.Value();
    }
    GVT(varId, defaultValue) {
        let value = this._GVV(varId);
        if (value == null) {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FWLog"].W(`找不到对应的变量名:${varId}`);
            if (typeof defaultValue == "undefined") {
                return null;
            }
            return defaultValue;
        }
        else {
            return value;
        }
    }
    SVT(varId, value) {
        let ppt = this._GV(varId);
        if (ppt != null) {
            this.MV(ppt, value);
        }
        else {
            console.error("修改的变量不存在：" + varId);
        }
    }
    AVT(varId, value) {
        let ppt = this._GV(varId);
        if (ppt != null) {
            if (typeof value == "number") {
                let tmp = ppt.Value() + Number(value);
                this.MV(ppt, tmp);
                return ppt.Value();
            }
            else if (typeof value == "string") {
                let tmp = ppt.Value() + String(value);
                this.MV(ppt, tmp);
                return ppt.Value();
            }
            else {
                console.error(`当前数据类型暂不支持"+"运算符操作`);
            }
        }
        else {
            console.error(`"+" 变量不存在：${varId}`);
        }
        return null;
    }
    SubVT(varId, value) {
        let ppt = this._GV(varId);
        if (ppt != null) {
            if (typeof value == "number") {
                let tmp = ppt.Value() + Number(value);
                this.MV(ppt, tmp);
                return ppt.Value();
            }
            else if (typeof value == "string") {
                let tmp = ppt.Value() + String(value);
                this.MV(ppt, tmp);
                return ppt.Value();
            }
            else {
                console.error(`当前数据类型暂不支持"+"运算符操作`);
            }
        }
        else {
            console.error(`"-" 变量不存在：${varId}`);
        }
        return null;
    }
    Bind(varId, c, callback, ...args) {
        let ppt = this._GV(varId);
        if (ppt == null) {
            //todo:error
            return false;
        }
        else {
            ppt.Bind(c, callback, args);
            return true;
        }
    }
    Unbind(varId, c, callback) {
        let ppt = this._GV(varId);
        if (ppt == null) {
            //todo:error
            return false;
        }
        else {
            ppt.Unbind(c, callback);
            return true;
        }
    }
    RevertVariable(variable) {
        let varId = variable.Id();
        let data = _Assist_StorageApt__WEBPACK_IMPORTED_MODULE_0__["StorageApt"].ReadObject("varId_" + varId);
        if (!data) {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FWLog"].W("本地数据不存在字段:varId_" + varId);
            return null;
        }
        // console.log("typeof(data)=" + typeof (data));
        let tip = "YDHW-Framework: ";
        let tip2 = " 数据类型不存在!";
        if (typeof (data) == "object") {
            let name = data.name;
            let type = data.type;
            if (typeof (type) == "undefined") {
                console.warn(tip + "1" + tip2);
                return null;
            }
            let value = data.value;
            if (typeof (value) == "undefined") {
                console.warn(tip + "2" + tip2);
                return null;
            }
            let isStore = data.is_store;
            if (typeof (isStore) == "undefined") {
                console.warn(tip + "3" + tip2);
                return null;
            }
            let owner = data.owner;
            if (typeof (owner) == "undefined") {
                console.warn(tip + "4" + tip2);
                return null;
            }
            if (type == "string") {
                variable.Revert(String(value));
            }
            else if (type == "number") {
                variable.Revert(Number(value));
            }
            else if (type == "boolean") {
                variable.Revert(Boolean(value));
            }
            else if (type == "object") {
                variable.Revert(Object(value));
            }
            else {
                console.warn(tip + "" + tip2);
                return null;
            }
            variable.Store();
            variable.SetOwner(owner);
        }
        return variable;
    }
    RevertData() {
        // for (let i = 0, l = vars.length; i < l; i++) {
        //     let data = vars[i];
        //     let name = data.name as string;
        //     let type = data.type as string;
        //     let value = data.value as any;
        //     let isStore = data.is_store as boolean;
        //     let owner = data.owner as string;
        //     if (name in this._DictVariable) {
        //         let ppt = this._DictVariable[name] as IVariable;
        //         if (type == "string") {
        //             ppt.SetValue(String(value));
        //         } else if (type == "number") {
        //             ppt.SetValue(Number(value));
        //         } else if (type == "boolean") {
        //             ppt.SetValue(Boolean(value));
        //         }
        //         ppt.Store();
        //         ppt.SetOwner(owner)
        //     } else {
        //         //TODO:捆版各个变量会各个caller，目前没有需求
        //     }
        // }
        if (this._IsRestore) {
            return;
        }
        let tmpDict = this._DictVariable;
        for (let name in tmpDict) {
            let variable = tmpDict[name];
            if (!variable.IsStore()) {
                continue;
            }
            this.RevertVariable(variable);
            // let data = Adapter.Read(name);
            // if (!data) {
            //     continue;
            // }
            // // console.log("typeof(data)=" + typeof (data));
            // if (typeof (data) == "object") {
            //     let name = data.name as string;
            //     let type = data.type as string;
            //     if (typeof (type) == "undefined") {
            //         console.error("1 数据类型不存在!");
            //         return;
            //     }
            //     let value = data.value as any;
            //     if (typeof (value) == "undefined") {
            //         console.error("2 数据类型不存在!");
            //         return;
            //     }
            //     let isStore = data.is_store as boolean;
            //     if (typeof (isStore) == "undefined") {
            //         console.error("3 数据类型不存在!");
            //         return;
            //     }
            //     let owner = data.owner as string;
            //     if (typeof (owner) == "undefined") {
            //         console.error("4 数据类型不存在!");
            //         return;
            //     }
            //     if (type == "string") {
            //         variable.Revert(String(value));
            //     } else if (type == "number") {
            //         variable.Revert(Number(value));
            //     } else if (type == "boolean") {
            //         variable.Revert(Boolean(value));
            //     } else if (type == "object") {
            //         variable.Revert(Object(value));
            //     } else {
            //         console.error("数据类型不存在!");
            //         return;
            //     }
            //     variable.Store();
            //     variable.SetOwner(owner)
            // }
        }
        this._IsRestore = true;
    }
    SaveData(name) {
        let tmpDict = this._DictVariable;
        let vars = [];
        for (let key in tmpDict) {
            let variable = tmpDict[key];
            // if (variable.Owner() == name && variable.IsStore()) {
            //     vars.push({
            //         name: key,
            //         value: variable.Value(),
            //         type: variable.Type(),
            //         is_store: variable.IsStore(),
            //         owner: variable.Owner(),
            //     })
            // }
            if (variable.Owner() == name && variable.IsStore()) {
                _Assist_StorageApt__WEBPACK_IMPORTED_MODULE_0__["StorageApt"].WriteObject("varId_" + key, {
                    name: key,
                    value: variable.Value(),
                    type: variable.Type(),
                    is_store: variable.IsStore(),
                    owner: variable.Owner(),
                });
                // Laya.LocalStorage.setJSON(key, {
                //     name: key,
                //     value: variable.Value<any>(),
                //     type: variable.Type(),
                //     is_store: variable.IsStore(),
                //     owner: variable.Owner(),
                // });
            }
        }
        return vars;
    }
    SaveAllData() {
        let tmpDict = this._DictVariable;
        let vars = [];
        for (let key in tmpDict) {
            this.SaveV(parseInt(key));
        }
    }
    SaveV(varId) {
        let tmpDict = this._DictVariable;
        let variable = tmpDict[varId];
        if (variable.IsStore()) {
            _Assist_StorageApt__WEBPACK_IMPORTED_MODULE_0__["StorageApt"].WriteObject("varId_" + varId, {
                name: varId,
                value: variable.Value(),
                type: variable.Type(),
                is_store: variable.IsStore(),
                owner: variable.Owner(),
            });
            // Laya.LocalStorage.setJSON(name, {
            //     name: name,
            //     value: variable.Value<any>(),
            //     type: variable.Type(),
            //     is_store: variable.IsStore(),
            //     owner: variable.Owner(),
            // });
        }
    }
}
//MethodContainer----------------------------------------------------------------------------
class MethodContainer {
    constructor() {
        /**DictNotify */
        this._DNfy = {};
        /**DictMethod */
        this._DMtd = {};
    }
    /**CheckAllMethods */
    CheckAllMtd() {
        console.log("Check all notify");
        for (let key in this._DNfy) {
            let notifies = this._DNfy[key];
            console.log(`Notify: ${key}`);
            for (let i = 0; i < notifies.length; i++) {
                let notify = notifies[i];
                notify.Log();
            }
        }
        console.log("Check all m");
        for (let key in this._DMtd) {
            let m = this._DMtd[key];
            console.log(`Mehtod: ${key}`);
            m.Log();
        }
    }
    /**
     * 注册普通事件，可以获取返回值
     * @param mId
     * @param c
     * @param event
     */
    //DeclareMethod
    DM(mId, c, owner, event) {
        let tmpDict = this._DMtd;
        let m = tmpDict[mId];
        if (m == null) {
            tmpDict[mId] = new Method(c, event, owner);
        }
        else {
            console.warn("owner=" + owner + " " + mId + " 已经被注册过了, 二次注册=重载覆写上一个函数的内容");
            tmpDict[mId] = new Method(c, event, owner);
        }
        return m;
    }
    /**
     * 注册广播事件，无返回值
     * @param mId
     * @param c
     * @param event
     */
    DNotify(mId, c, owner, event) {
        let tmpDict = this._DNfy;
        let tmpList = tmpDict[mId];
        if (tmpList == null) {
            tmpDict[mId] = [];
            tmpDict[mId].push(new Method(c, event, owner));
        }
        else {
            tmpDict[mId].push(new Method(c, event, owner));
        }
    }
    // public UnregisterEvent(mId: SINGLETON_METHOD, c: any, m: IMethod): void {
    //     let tmpList: MethodHandler[] = this._DictEvent[mId];
    //     if (tmpList == null) {
    //         //todo:warn
    //     } else if (tmpList.length <= 0) {
    //         delete this._DictEvent[mId];//删除
    //     } else {
    //         tmpList = tmpList.filter(item => item != new MethodHandler(c, m));
    //         this._DictEvent[mId] = tmpList;
    //     }
    // }
    /**
     * 广播通知对应的事件
     * @param mId
     * @param arg
     */
    //InvokeNotify
    InvokeNfy(mId, arg) {
        let tmpDict = this._DNfy;
        let tmpList = tmpDict[mId];
        if (!tmpList || !tmpList.length)
            return;
        for (let i = 0, count = tmpList.length; i < count; i++) {
            let m = tmpList[i];
            m.m.apply(m.c, arg);
        }
    }
    /**
     *
     * @param mId 调用指定事件类型，获取返回参数
     * @param arg 具有返回值
     */
    Invoke(mId, arg) {
        let tmpDict = this._DMtd;
        let m = tmpDict[mId];
        if (m == null) {
            console.error("事件没注册：" + mId);
            return;
        }
        return m.m.apply(m.c, arg);
    }
    // public ClearAllEvent(): void {
    //     for (let key in this._DictEvent) {
    //         let eventList: MethodHandler[] = this._DictEvent[key];
    //         for (let i = 0, count = eventList.length; i < count; i++) {
    //             let m = eventList[i];
    //             m.Destroy();
    //             delete eventList[i];
    //         }
    //     }
    //     delete this._DictEvent;
    // }
    ClearAllMethod() {
        let tmpDict = this._DMtd;
        for (let key in tmpDict) {
            let m = tmpDict[key];
            m.Destroy();
            delete tmpDict[key];
        }
        delete this._DMtd;
    }
}
//Core---------------------------------------------------------------------------------------------
function as(obj, method1) {
    return method1 in obj ? obj : null;
}
class Core {
    constructor() {
        this._VC = new VariableContainer();
        this._MC = new MethodContainer();
        this._ListInit = {};
        this._ListExecute = {};
        this._ListAll = {};
        /**
         * 从游戏开始运行到当前帧率
         * */
        this._CurFrameIndex = 0;
        this._ListInit = {};
        this._ListExecute = {};
        this._ListAll = {};
        this._Manager = new BaseManager();
        this._Manager.SetCore(this);
    }
    static Me() {
        if (Core._Me == null) {
            Core._Me = new Core();
        }
        return Core._Me;
    }
    static SetCore(mgr, core) {
        const m = as(mgr, 'SetCore');
        if (m != null) {
            m.SetCore(core);
        }
    }
    static SetName(mgr, name) {
        const m = as(mgr, 'SetName');
        if (m != null) {
            m.SetName(name);
        }
    }
    VContainer() {
        return this._VC;
    }
    MContainer() {
        return this._MC;
    }
    Add(managerType, manager) {
        _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FWLog"].L("Add Manager: " + managerType);
        if ('function' === typeof manager) {
            const Klass = manager;
            manager = new Klass();
        }
        Core.SetCore(manager, this);
        Core.SetName(manager, managerType);
        const initializeManager = as(manager, 'Init');
        if (initializeManager != null) {
            const _initializeManagers = this._ListInit;
            _initializeManagers[managerType] = initializeManager;
        }
        const executeManager = as(manager, 'Execute');
        if (executeManager != null) {
            const _executeManagers = this._ListExecute;
            _executeManagers[managerType] = executeManager;
        }
        const _allManagers = this._ListAll;
        _allManagers[managerType] = manager;
        return this;
    }
    AsManager() {
        return this._Manager;
    }
    GetManager(mgrType) {
        return this._ListAll[mgrType];
    }
    Initialize() {
        const tmpDict = this._ListInit;
        for (let key in tmpDict) {
            let tmpValue = tmpDict[key];
            tmpValue && tmpValue.Init();
        }
    }
    Execute() {
        this._CurFrameIndex++;
        const tmpDict = this._ListExecute;
        for (let key in tmpDict) {
            let tmpValue = tmpDict[key];
            tmpValue && tmpValue.Execute(this._CurFrameIndex);
        }
    }
    /**
     * 从本地还原数据
     */
    Restore() {
        const tmpList = this._ListAll;
        for (let key in tmpList) {
            let tmpValue = tmpList[key];
            tmpValue.Restore();
        }
    }
    /**
     * 保存数据到本地
     */
    Save() {
        const tmpList = this._ListAll;
        for (let key in tmpList) {
            let tmpValue = tmpList[key];
            tmpValue.Save();
        }
    }
}
Core._Me = null;
//Manager----------------------------------------------------------------------------
class BaseManager {
    constructor() {
        this.Name = "Gauntlet";
        this._Core = null;
        this.VC = null;
        this.MC = null;
    }
    SetName(name) {
        this.Name = name;
    }
    SetCore(core) {
        this._Core = core;
        this.VC = this._Core.VContainer();
        this.MC = this._Core.MContainer();
    }
    /**
     * 根据方法ID注册事件，一个ID侦听一个事件
     * DeclareMethod
     * @param mId
     * @param binding
     */
    DM(mId, c, binding) {
        return this.MC.DM(mId, c, this.Name, binding);
    }
    /**
     * 根据方法ID注册事件：一个ID侦听多个不同事件
     * DeclareNotify
     * @param mId
     * @param binding
     */
    DNfy(mId, c, binding) {
        this.MC.DNotify(mId, c, this.Name, binding);
    }
    /**
     * 根据方法ID执行对应函数：一个ID触发绑定注册的唯一一个事件
     * @param mId
     * @param args
     */
    Invoke(mId, ...args) {
        return this.MC.Invoke(mId, args);
    }
    /**
     * 根据方法ID执行对应函数：一个ID触发绑定注册的一个或多个事件
     * InvokeNotify
     * @param mId
     * @param args
     */
    InvokeNfy(mId, ...args) {
        this.MC.InvokeNfy(mId, args);
    }
    /**
     * DeclareVariable
     * @param varId
     * @param arg
     */
    DV(varId, arg) {
        return this.VC.DV(varId, this.Name, arg);
    }
    /**
     * 获取变量，先在当前内存查找，如果不存在，
     * 则会到本地存储数据里面读取，反序列化后再查找匹对，
     * 如果存在则放入内存种并返回，还不存在则返回null，
     * GetValue
     * @param varId
     */
    GV(varId) {
        return this.VC.GV(varId, this.Name);
    }
    /**
     * GetValueT
     * @param varId
     * @param defaultValue
     */
    GVT(varId, defaultValue) {
        return this.VC.GVT(varId, defaultValue);
    }
    /**
     * SetValueT
     * @param varId
     * @param value
     */
    SVT(varId, value) {
        this.VC.SVT(varId, value);
    }
    /**
     * AddValueT
     * @param varId
     * @param value
     */
    AVT(varId, value) {
        return this.VC.AVT(varId, value);
    }
    /**
     * SubValueT
     * @param varId
     * @param value
     */
    SubVT(varId, value) {
        return this.VC.SubVT(varId, value);
    }
    /**
     * BindVariable
     * @param varId
     * @param c
     * @param callback
     * @param args
     */
    BindV(varId, c, callback, ...args) {
        return this.VC.Bind(varId, c, callback);
    }
    /**
     * UnbindVariable
     * @param varId
     * @param c
     * @param callback
     */
    UnbindV(varId, c, callback) {
        return this.VC.Unbind(varId, c, callback);
    }
    /**
     * RemoveVariable
     * @param varId
     */
    RemoveV(varId) {
        this.VC.RemoveV(varId);
    }
    /**
     * 每个Manager重载这个函数可以在加载本地数据时初始化自定数据格式的数据配置
     */
    Restore() {
        this.VC.RevertData();
    }
    /**
     * 每个Manager重载这个函数保存自定数据格式的数据配置
     */
    Save() {
        this.Name && this.Name.length > 0 ? this.VC.SaveData(this.Name) : this.VC.SaveAllData();
    }
    SaveVariable(name) {
        this.Name && this.Name.length > 0 && this.VC.SaveV(name);
    }
}


/***/ }),

/***/ "./fw/Enums/PLAT.ts":
/*!**************************!*\
  !*** ./fw/Enums/PLAT.ts ***!
  \**************************/
/*! exports provided: PLAT */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PLAT", function() { return PLAT; });
/**EM_PLATFORM_TYPE */
var PLAT;
(function (PLAT) {
    PLAT[PLAT["Common"] = -1] = "Common";
    PLAT[PLAT["Wechat"] = 0] = "Wechat";
    PLAT[PLAT["QQ"] = 1] = "QQ";
    PLAT[PLAT["Oppo"] = 2] = "Oppo";
    PLAT[PLAT["Vivo"] = 3] = "Vivo";
    PLAT[PLAT["Toutiao"] = 4] = "Toutiao";
    PLAT[PLAT["Baidu"] = 5] = "Baidu";
    PLAT[PLAT["P_4399"] = 6] = "P_4399";
    PLAT[PLAT["Qutoutiao"] = 7] = "Qutoutiao";
    PLAT[PLAT["P_360"] = 8] = "P_360";
    PLAT[PLAT["Momo"] = 9] = "Momo";
    PLAT[PLAT["Web"] = 10] = "Web";
    PLAT[PLAT["Xiaomi"] = 11] = "Xiaomi";
    PLAT[PLAT["Meizu"] = 12] = "Meizu";
    PLAT[PLAT["UC"] = 13] = "UC";
    PLAT[PLAT["Alipay"] = 14] = "Alipay";
})(PLAT || (PLAT = {}));


/***/ }),

/***/ "./fw/Log/FrameworkLog.ts":
/*!********************************!*\
  !*** ./fw/Log/FrameworkLog.ts ***!
  \********************************/
/*! exports provided: FWLog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FWLog", function() { return FWLog; });
/* harmony import */ var _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Utility/LogUtility */ "./fw/Utility/LogUtility.ts");

//FrameworkLog
class FWLog {
    static L(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().L(this.a, message, null, args);
    }
    static D(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().D(this.a, message, null, args);
    }
    static I(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().I(this.a, message, null, args);
    }
    static W(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().W(this.a, message, null, args);
    }
    static E(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().E(this.a, message, null, args);
    }
}
FWLog.a = "Framework";


/***/ }),

/***/ "./fw/Log/UrlLog.ts":
/*!**************************!*\
  !*** ./fw/Log/UrlLog.ts ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return UrlLog; });
/* harmony import */ var _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Utility/LogUtility */ "./fw/Utility/LogUtility.ts");

class UrlLog {
    static L(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().L(this.a, message, null, args);
    }
    static D(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().D(this.a, message, null, args);
    }
    static I(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().I(this.a, message, null, args);
    }
    static W(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().W(this.a, message, null, args);
    }
    static E(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().E(this.a, message, null, args);
    }
}
UrlLog.a = "Url";


/***/ }),

/***/ "./fw/Manager/ApiManager.ts":
/*!**********************************!*\
  !*** ./fw/Manager/ApiManager.ts ***!
  \**********************************/
/*! exports provided: ApiManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiManager", function() { return ApiManager; });
/* harmony import */ var _Manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Manager */ "./fw/Manager/Manager.ts");
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Core */ "./fw/Core.ts");


class ApiManager extends _Manager__WEBPACK_IMPORTED_MODULE_0__["Manager"] {
    SetCore(core) {
        this._Core = core;
        this.VC = new _Core__WEBPACK_IMPORTED_MODULE_1__["VariableContainer"]();
        this.MC = new _Core__WEBPACK_IMPORTED_MODULE_1__["MethodContainer"]();
    }
    Init() {
    }
}


/***/ }),

/***/ "./fw/Manager/CommerceManager.ts":
/*!***************************************!*\
  !*** ./fw/Manager/CommerceManager.ts ***!
  \***************************************/
/*! exports provided: CommerceManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommerceManager", function() { return CommerceManager; });
/* harmony import */ var _Manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Manager */ "./fw/Manager/Manager.ts");

class CommerceManager extends _Manager__WEBPACK_IMPORTED_MODULE_0__["Manager"] {
    Init() {
    }
}


/***/ }),

/***/ "./fw/Manager/Manager.ts":
/*!*******************************!*\
  !*** ./fw/Manager/Manager.ts ***!
  \*******************************/
/*! exports provided: Manager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Manager", function() { return Manager; });
/* harmony import */ var _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Utility/LogUtility */ "./fw/Utility/LogUtility.ts");
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Core */ "./fw/Core.ts");


class Manager extends _Core__WEBPACK_IMPORTED_MODULE_1__["BaseManager"] {
    constructor() {
        super(...arguments);
        this.Name = "Gauntlet";
    }
    Log(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().L(this.Name, message, Manager.LogStyle, args);
    }
    Debug(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().D(this.Name, message, Manager.DebugStyle, args);
    }
    Info(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().I(this.Name, message, Manager.InfoStyle, args);
    }
    Warn(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().W(this.Name, message, Manager.WarnStyle, args);
    }
    Error(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().E(this.Name, message, Manager.ErrorStyle, args);
    }
    Table(obj, name) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().T(this.Name, Manager.TableStyle, obj, name);
    }
}
Manager.DEFAULT_FONT_SIZE = 12;
Manager.LogStyle = `color: green; font-size:${Manager.DEFAULT_FONT_SIZE}px`;
Manager.DebugStyle = `color: DodgerBlue; font-size:${Manager.DEFAULT_FONT_SIZE}px`;
Manager.InfoStyle = `color: Turquoise; font-size:${Manager.DEFAULT_FONT_SIZE}px`;
Manager.WarnStyle = `color: Orange; font-size:bold,${Manager.DEFAULT_FONT_SIZE}px`;
Manager.ErrorStyle = `color: Tomato; font-size:${Manager.DEFAULT_FONT_SIZE}px`;
Manager.TableStyle = `color: Black; font-size:${Manager.DEFAULT_FONT_SIZE}px`;


/***/ }),

/***/ "./fw/Manager/NetManager.ts":
/*!**********************************!*\
  !*** ./fw/Manager/NetManager.ts ***!
  \**********************************/
/*! exports provided: NetManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NetManager", function() { return NetManager; });
/* harmony import */ var _Manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Manager */ "./fw/Manager/Manager.ts");
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Core */ "./fw/Core.ts");


class NetManager extends _Manager__WEBPACK_IMPORTED_MODULE_0__["Manager"] {
    SetCore(core) {
        this._Core = core;
        this.VC = new _Core__WEBPACK_IMPORTED_MODULE_1__["VariableContainer"]();
        this.MC = new _Core__WEBPACK_IMPORTED_MODULE_1__["MethodContainer"]();
    }
    Init() {
    }
}


/***/ }),

/***/ "./fw/Utility/Assist.ts":
/*!******************************!*\
  !*** ./fw/Utility/Assist.ts ***!
  \******************************/
/*! exports provided: Assist */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Assist", function() { return Assist; });
class Assist {
    static ToB(value) {
        return value === 1 ? true : false;
    }
    static ToSB(value, name) {
        let result = value === 0 ? false : true;
        if (!result)
            console.warn(name + " is off");
        return result;
    }
}


/***/ }),

/***/ "./fw/Utility/LogUtility.ts":
/*!**********************************!*\
  !*** ./fw/Utility/LogUtility.ts ***!
  \**********************************/
/*! exports provided: LogUtility */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogUtility", function() { return LogUtility; });
class LogUtility {
    constructor() {
        this._Tag = "";
        this.PARAM_COUNT = 3;
        this._LogStyle = `color:green;font-size:${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        this._DebugStyle = `color:dodgerblue;font-size:${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        this._InfoStyle = `color:turquoise;font-size:${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        this._WarnStyle = `color:orange;font-size:bold,${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        this._ErrorStyle = `color:tomato;font-size:${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        this._TableStyle = `color:magenta;font-size:${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
    }
    static Me() {
        if (this._Me == null) {
            this._Me = new LogUtility();
        }
        return this._Me;
    }
    ;
    L(tag, message, style, args) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof (style) != "undefined") ? style : this._LogStyle;
        let options = [];
        for (let i = this.PARAM_COUNT; i < arguments.length; i++) {
            options[i - this.PARAM_COUNT] = arguments[i];
        }
        if (this.P && (this.P.IsOppo || this.P.IsVivo || this.P.IsMeizu || (!this.P.IsSimulator))) {
            let msg = "";
            if (args && (typeof args != "undefined") && args.length > 0)
                msg = JSON.stringify(options[0]);
            console.log("[Log][" + tag + "] " + message + " " + msg);
            return;
        }
        if (this.P && (this.P.IsOppo || this.P.IsWechat)) {
            args && (typeof args != "undefined") && args.length > 0
                ? console.log.apply(console, ["[Log][" + tag + "] " + message].concat(options))
                : console.log.apply(console, ["[Log][" + tag + "] " + message]);
            return;
        }
        args && (typeof args != "undefined") && args.length > 0
            ? console.groupCollapsed.apply(console, ["%c[Log][" + tag + "] " + message, style].concat(options))
            : console.groupCollapsed.apply(console, ["%c[Log][" + tag + "] " + message, style]);
        console.trace();
        console.groupEnd();
    }
    D(tag, message, style, args) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof style != "undefined") ? style : this._DebugStyle;
        let options = [];
        for (let i = this.PARAM_COUNT; i < arguments.length; i++) {
            options[i - this.PARAM_COUNT] = arguments[i];
        }
        if (this.P && (this.P.IsOppo || this.P.IsVivo || this.P.IsMeizu || (!this.P.IsSimulator))) {
            let msg = "";
            if (args && (typeof args != "undefined") && args.length > 0)
                msg = JSON.stringify(options[0]);
            console.debug("[Debug][" + tag + "] " + message + " " + msg);
            return;
        }
        if (this.P && (this.P.IsOppo || this.P.IsWechat)) {
            args && (typeof args != "undefined") && args.length > 0
                ? console.debug.apply(console, ["[Debug][" + tag + "] " + message].concat(options))
                : console.debug.apply(console, ["[Debug][" + tag + "] " + message]);
            return;
        }
        args && (typeof args != "undefined") && args.length > 0
            ? console.groupCollapsed.apply(console, ["%c[Debug][" + tag + "] " + message, style].concat(options))
            : console.groupCollapsed.apply(console, ["%c[Debug][" + tag + "] " + message, style]);
        console.trace();
        console.groupEnd();
    }
    I(tag, message, style, args) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof style != "undefined") ? style : this._InfoStyle;
        let options = [];
        for (let i = this.PARAM_COUNT; i < arguments.length; i++) {
            options[i - this.PARAM_COUNT] = arguments[i];
        }
        if (this.P && (this.P.IsOppo || this.P.IsVivo || this.P.IsMeizu || (!this.P.IsSimulator))) {
            let msg = "";
            if (args && (typeof args != "undefined") && args.length > 0)
                msg = JSON.stringify(options[0]);
            console.log("[Info][" + tag + "] " + message + " " + msg);
            return;
        }
        if (this.P && (this.P.IsOppo || this.P.IsWechat)) {
            args && (typeof args != "undefined") && args.length > 0
                ? console.info.apply(console, ["[Info][" + tag + "] " + message].concat(options))
                : console.info.apply(console, ["[Info][" + tag + "] " + message]);
            return;
        }
        args && (typeof args != "undefined") && args.length > 0
            ? console.groupCollapsed.apply(console, ["%c[Info][" + tag + "] " + message, style].concat(options))
            : console.groupCollapsed.apply(console, ["%c[Info][" + tag + "] " + message, style]);
        console.trace();
        console.groupEnd();
    }
    W(tag, message, style, args) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof style != "undefined") ? style : this._WarnStyle;
        let options = [];
        for (let i = this.PARAM_COUNT; i < arguments.length; i++) {
            options[i - this.PARAM_COUNT] = arguments[i];
        }
        if (this.P && (this.P.IsOppo || this.P.IsVivo || this.P.IsMeizu || (!this.P.IsSimulator))) {
            let msg = "";
            if (args && (typeof args != "undefined") && args.length > 0)
                msg = JSON.stringify(options[0]);
            console.warn("[Warn][" + tag + "] " + message + " " + msg);
            return;
        }
        if (this.P && (this.P.IsOppo || this.P.IsWechat)) {
            args && (typeof args != "undefined") && args.length > 0
                ? console.warn.apply(console, ["[Warn][" + tag + "] " + message].concat(options))
                : console.warn.apply(console, ["[Warn][" + tag + "] " + message]);
            return;
        }
        args && (typeof args != "undefined") && args.length > 0
            ? console.groupCollapsed.apply(console, ["%c[Warn][" + tag + "] " + message, style].concat(options))
            : console.groupCollapsed.apply(console, ["%c[Warn][" + tag + "] " + message, style]);
        console.trace();
        console.groupEnd();
    }
    E(tag, message, style, args) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof style != "undefined") ? style : this._ErrorStyle;
        let options = [];
        for (let i = this.PARAM_COUNT; i < arguments.length; i++) {
            options[i - this.PARAM_COUNT] = arguments[i];
        }
        if (this.P && (this.P.IsOppo || this.P.IsVivo || this.P.IsMeizu || (!this.P.IsSimulator))) {
            let msg = "";
            if (args && (typeof args != "undefined") && args.length > 0)
                msg = JSON.stringify(options[0]);
            console.error("[Error][" + tag + "] " + message + " " + msg);
            return;
        }
        if (this.P && (this.P.IsOppo || this.P.IsWechat)) {
            args && (typeof args != "undefined") && args.length > 0
                ? console.error.apply(console, ["[Error][" + tag + "] " + message].concat(options))
                : console.error.apply(console, ["[Error][" + tag + "] " + message]);
            return;
        }
        args && (typeof args != "undefined") && args.length > 0
            ? console.groupCollapsed.apply(console, ["%c[Error][" + tag + "] " + message, style].concat(options))
            : console.groupCollapsed.apply(console, ["%c[Error][" + tag + "] " + message, style]);
        console.trace();
        console.groupEnd();
    }
    T(tab, obj, name, style) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof style != "undefined") ? style : this._TableStyle;
        if (this.P && (this.P.IsOppo || this.P.IsWechat)) {
            return;
        }
        console.groupCollapsed.apply(console, ["%c[Table][" + tab + "] ", style]);
        // name && console.log("%ctable: " + name, style);
        console.table(obj);
        console.groupEnd();
    }
}
LogUtility._Me = null;
LogUtility.IS_ENABLE_DEBUG = true;
LogUtility.DEFAULT_FONT_SIZE = 12;
LogUtility.BACK_GROUND_COLOR = "background:#FFFF99";
new LogUtility();


/***/ }),

/***/ "./fw/Utility/RandomUtility.ts":
/*!*************************************!*\
  !*** ./fw/Utility/RandomUtility.ts ***!
  \*************************************/
/*! exports provided: RandomUtility */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RandomUtility", function() { return RandomUtility; });
class RandomUtility {
    static Probability(pro) {
        if (pro == 100)
            return true;
        if (pro == 0)
            return false;
        let probability = Math.floor(Math.random() * 100);
        return probability <= pro;
    }
}


/***/ }),

/***/ "./fw/Utility/TimerUtility.ts":
/*!************************************!*\
  !*** ./fw/Utility/TimerUtility.ts ***!
  \************************************/
/*! exports provided: Timer, DelayTimer, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Timer", function() { return Timer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DelayTimer", function() { return DelayTimer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TimerUtility; });
class Timer {
    constructor() {
        this.IntervalID = null;
    }
    Init(name, c, m, interval, args) {
        this.Name = name;
        this._Caller = c;
        this._Method = m;
        this.Args = args;
        this._StartTimestamp = Date.now();
        this.Interval = interval;
    }
    Execute(...args) {
        // let bIsResult = false;
        // if (this.Args && this.Args.length > 0) {
        //     bIsResult = this._Method.call(this._Caller, this.Args);
        // } else {
        //     bIsResult = this._Method.call(this._Caller);
        // }
        let bIsResult = this._Method.call(this._Caller, args);
        if (bIsResult) {
            TimerUtility.RemoveTimer(this.Name);
        }
    }
    Invoke(...args) {
        this._Method.call(this._Caller, args);
    }
}
class DelayTimer extends Timer {
    Init(name, c, m, interval, args) {
        this.Name = name;
        this._Caller = c;
        this._InnerMethod = m;
        this.Args = args;
        this._StartTimestamp = Date.now();
        this.Interval = interval;
    }
    Execute(...args) {
        this._InnerMethod.apply(this._Caller, args);
    }
    Invoke(...args) {
        this._InnerMethod.apply(this._Caller, args);
    }
}
class TimerUtility {
    // private static _DictInterval: { [key: string]: any } = {};
    /**
     * 数字转字符串时 至少保持两位数，如：1=>01,2=>02,3=>03
     * @param num
     */
    static DecimalFormat(num) {
        return (num > 9 ? '' : '0') + num;
    }
    static TimeFormat_1(seconds, sufix) {
        return this.SecondsToDay(seconds) +
            sufix + this.SecondsToHour(seconds) +
            sufix + this.SecondsToMinute(seconds % 60);
    }
    static MilliSecondsToSeconds(milliseconds) {
        return Math.floor(milliseconds / 1000);
    }
    static SecondsToDay(seconds) {
        return seconds / 3600 | 0;
    }
    static SecondsToHour(seconds) {
        return (seconds / 60 | 0) % 60;
    }
    static SecondsToMinute(seconds) {
        return seconds / 60;
    }
    static MilliSecondsToMinute(milliSeconds) {
        return this.SecondsToMinute(this.MilliSecondsToSeconds(milliSeconds));
    }
    static Delay(interval, c, m, ...args) {
        let timer = new DelayTimer();
        timer.Init("delay_event", c, m, interval, args);
        timer.IntervalID = setTimeout(function () {
            timer.Execute(timer.Args);
        }, timer.Interval);
        // Laya.timer.once(interval, timer, timer.Execute, args);
        let key = timer.IntervalID + "";
        this._DelayTimer[key] = timer;
        return key;
    }
    static RemoveDelayTimer(id) {
        let timer = this._DelayTimer[id];
        if (timer && timer instanceof DelayTimer) {
            clearTimeout(timer.IntervalID);
        }
    }
    static DelayBtnEnabled(c, m, ...args) {
        this.Delay(2500, c, m, ...args);
    }
    static DeactivateTimer(name) {
        let timer = this._DictTimer[name];
        if (timer && timer instanceof Timer) {
            clearInterval(timer.IntervalID);
            // Laya.timer.clear(timer, timer.Execute);
        }
        else {
            console.warn("休眠 " + this.noTimer + `${name}`);
        }
    }
    static ActivateTimer(name) {
        let timer = this._DictTimer[name];
        if (timer && timer instanceof Timer) {
            clearInterval(timer.IntervalID);
            timer.IntervalID = setInterval(function () {
                timer.Execute(timer.Args);
            }, timer.Interval);
            // Laya.timer.loop(timer.Interval, timer, timer.Execute, timer.Args);
        }
        else {
            console.warn("激活" + this.noTimer + `${name}`);
        }
    }
    static AddTimer(name, interval, c, init, m, args) {
        let timer = this._DictTimer[name];
        if (timer && timer instanceof Timer) {
            console.warn(`添加 定时器已存在：${name}`);
        }
        else {
            c && init && init.call(c);
            timer = new Timer();
            timer.Init(name, c, m, interval, args);
            timer.IntervalID = setInterval(function () {
                timer.Execute(timer.Args);
            }, interval, args);
            this._DictTimer[name] = timer;
            // Laya.timer.loop(interval, timer, timer.Execute, args);
        }
    }
    static GetTimer(name) {
        let timer = this._DictTimer[name];
        if (timer && timer instanceof Timer) {
            return timer;
        }
        else {
            console.warn("获取" + this.noTimer + `${name}`);
            return null;
        }
    }
    static RemoveTimer(name) {
        let timer = this._DictTimer[name];
        if (timer && timer instanceof Timer) {
            timer = this._DictTimer[name];
            clearInterval(timer.IntervalID);
            // Laya.timer.clear(timer, timer.Execute);
            this._DictTimer[name] = null;
        }
        else {
            console.warn("移除" + this.noTimer + `${name}`);
        }
    }
}
TimerUtility._DictTimer = {};
TimerUtility._DelayTimer = {};
TimerUtility.noTimer = "定时器不存在：";


/***/ }),

/***/ "./fw/Utility/Url.ts":
/*!***************************!*\
  !*** ./fw/Utility/Url.ts ***!
  \***************************/
/*! exports provided: URL_SLICE_TYPE, Url */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL_SLICE_TYPE", function() { return URL_SLICE_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Url", function() { return Url; });
/* harmony import */ var _Assist_Http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Assist/Http */ "./fw/Assist/Http.ts");
/* harmony import */ var _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Log/UrlLog */ "./fw/Log/UrlLog.ts");
/* harmony import */ var _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../sdk/SDK/Declare */ "./sdk/SDK/Declare.ts");



const NS = "need set ";
const NOS = "not set ";
var URL_SLICE_TYPE;
(function (URL_SLICE_TYPE) {
    URL_SLICE_TYPE["None"] = "Null";
    URL_SLICE_TYPE["Method"] = "Method";
    URL_SLICE_TYPE["Domain"] = "Domain";
    URL_SLICE_TYPE["Manager"] = "Manager";
    URL_SLICE_TYPE["Version"] = "Version";
    URL_SLICE_TYPE["Category"] = "Category";
    URL_SLICE_TYPE["Router"] = "Router";
    URL_SLICE_TYPE["Append"] = "Append";
    URL_SLICE_TYPE["Description"] = "Description";
    URL_SLICE_TYPE["End"] = "End";
})(URL_SLICE_TYPE || (URL_SLICE_TYPE = {}));
class Url {
    constructor() {
        this._Slices = [];
        this._Header = {};
        this._AccountPass = "";
        this._MethodType = "";
        this._Http = "";
        this._DomainIndex = 0;
        this._Domains = [];
        this._Version = "";
        this._Category = "";
        this._Manager = "";
        this._Router = "";
        this._AppendList = [];
        this._Params = {};
        this._CAIndex = 0; //_CurrentAppendIndex
        this._Description = "";
        this._Url = "";
        this._CSType = URL_SLICE_TYPE.None; //_CurrentSliceType
        this._Async = true;
        this._IsEnd = false;
        this._Data = "";
        this._IsDebug = true;
        this._ReceiveCaller = null;
        this._ReceiveMethod = null;
        this._ErrorCaller = null;
        this._ErrorMethod = null;
        this._ExceptionCaller = null;
        this.HTTPs = {
            1: "Content-Type",
            2: "AccountPass",
            3: "EngineType",
            4: "EngineVer",
        };
        this.HEADs = {
            1: "application/json;charset=UTF-8",
            2: "application/x-www-form-urlencoded",
        };
        Url.AllUrls.push(this);
    }
    Print() {
        let url = this._Category + "-" + this._Router;
        if (this._IsDebug)
            console.log("" + url);
    }
    SetIsDebugMode(isDebug) {
        this._IsDebug = isDebug;
    }
    static SetAllDebugMode(debug) {
        for (let i = 0; i < Url.AllUrls.length; i++) {
            let url = Url.AllUrls[i];
            url._IsDebug = debug;
        }
    }
    AccountPass(value) {
        this._AccountPass = value;
        return this;
    }
    static SetAccountPass(key) {
        for (let i = 0; i < Url.AllUrls.length; i++) {
            let url = Url.AllUrls[i];
            url._AccountPass = key;
        }
    }
    Head(key, value) {
        if (typeof (value) == "string") {
            this._Header[this.HTTPs[key]] = value;
        }
        else {
            this._Header[this.HTTPs[key]] = this.HEADs[value];
        }
        return this;
    }
    Header() {
        return this._Header;
    }
    AsPost() {
        this._MethodType = "post";
        return this;
    }
    AsGet() {
        this._MethodType = "get";
        return this;
    }
    Method() {
        return this._MethodType;
    }
    Des(value) {
        this._Description = value;
        this._CSType = URL_SLICE_TYPE.Description;
        if (this._Slices.indexOf(this._CSType) < 0) {
            this._Slices.push(this._CSType);
        }
        return this;
    }
    Switch(index) {
        this._DomainIndex = index;
        return this;
    }
    Domain(value) {
        this._CSType = URL_SLICE_TYPE.Domain;
        if (this._Slices.indexOf(this._CSType) < 0) {
            this._Slices.push(this._CSType);
        }
        this._Domains = value;
        return this;
    }
    Manager(value) {
        if (this._Slices.indexOf(URL_SLICE_TYPE.Domain) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NS + "domain");
            return this;
        }
        this._CSType = URL_SLICE_TYPE.Manager;
        if (this._Slices.indexOf(this._CSType) < 0) {
            this._Slices.push(this._CSType);
        }
        this._Manager = value;
        return this;
    }
    Version(value) {
        if (this._Slices.indexOf(URL_SLICE_TYPE.Manager) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NS + "manager");
            return this;
        }
        this._CSType = URL_SLICE_TYPE.Version;
        if (this._Slices.indexOf(this._CSType) < 0) {
            this._Slices.push(this._CSType);
        }
        this._Version = value;
        return this;
    }
    C(value) {
        if (this._Slices.indexOf(URL_SLICE_TYPE.Version) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NS + "version");
            return this;
        }
        this._CSType = URL_SLICE_TYPE.Category;
        if (this._Slices.indexOf(this._CSType) < 0) {
            this._Slices.push(this._CSType);
        }
        this._Category = value;
        return this;
    }
    R(value) {
        if (this._Slices.indexOf(URL_SLICE_TYPE.Category) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NS + "category");
            return this;
        }
        this._CSType = URL_SLICE_TYPE.Router;
        if (this._Slices.indexOf(this._CSType) < 0) {
            this._Slices.push(this._CSType);
        }
        this._Router = value;
        if (this._Router.length >= 0) {
            this._AppendList = [];
        }
        return this;
    }
    A(value) {
        if (this._Slices.indexOf(URL_SLICE_TYPE.Router) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NS + "router");
            return this;
        }
        this._CSType = URL_SLICE_TYPE.Append;
        if (this._Slices.indexOf(this._CSType) < 0) {
            this._Slices.push(this._CSType);
        }
        this._CAIndex = this._AppendList.push(value);
        return this;
    }
    End() {
        // if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Description) < 0) {
        //     UrlLog.Error("not set description");
        //     return this;
        // }
        if (this._Slices.indexOf(URL_SLICE_TYPE.Domain) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NOS + "domain");
            return this;
        }
        if (this._Slices.indexOf(URL_SLICE_TYPE.Manager) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NOS + "manager");
            return this;
        }
        if (this._Slices.indexOf(URL_SLICE_TYPE.Version) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NOS + "version");
            return this;
        }
        if (this._Slices.indexOf(URL_SLICE_TYPE.Category) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NOS + "category");
            return this;
        }
        if (this._Slices.indexOf(URL_SLICE_TYPE.Router) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NOS + "router");
            return this;
        }
        this._CSType = URL_SLICE_TYPE.End;
        this._IsEnd = true;
        if (this._DomainIndex > this._Domains.length - 1) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E("domain error");
            return null;
        }
        if (this._DomainIndex < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E("domain switch error");
            return null;
        }
        if (!this._Domains[this._DomainIndex]) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E("domain null");
            return null;
        }
        this._Url = this._Http + this._Domains[this._DomainIndex] + "/" + this._Manager + "/" + this._Version + "/" + this._Category + "/" + this._Router;
        for (const key in this._AppendList) {
            this._Url = this._Url + "/" + this._AppendList[key];
        }
        if (Object.keys(this._Params).length > 0) { //todo:js编译版本需要注意
            let first = true;
            for (const key in this._Params) {
                let value = this._Params[key];
                first ? (first = false, this._Url += "?") : (this._Url += "&");
                this._Url += (key + '=' + value);
            }
        }
        return this;
    }
    Value() {
        if (!(this._IsEnd === true)) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NOS + "end yet");
            return null;
        }
        if (this._MethodType.length <= 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E(NOS + "m type");
            return null;
        }
        switch (this._CSType) {
            case URL_SLICE_TYPE.None:
                return "Error Url";
            case URL_SLICE_TYPE.Method:
                return this._MethodType;
            case URL_SLICE_TYPE.Domain:
                return this._Domains[this._DomainIndex];
            case URL_SLICE_TYPE.Version:
                return this._Version;
            case URL_SLICE_TYPE.Category:
                return this._Category;
            case URL_SLICE_TYPE.Router:
                return this._Router;
            case URL_SLICE_TYPE.Append:
                if (this._AppendList.length > 0) {
                    return this._AppendList[this._CAIndex];
                }
                else {
                    return "Error Append Param";
                }
            case URL_SLICE_TYPE.End:
                return this._Url;
        }
        return null;
    }
    SetCustomUrl(url) {
        this._Url = url;
        return this;
    }
    CustomUrl() {
        return this._Url;
    }
    CheckValue() {
        return this._Url;
    }
    Data() {
        return this._Data;
    }
    Post(data) {
        if (this._MethodType != "post") {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E("current m is not post");
            return null;
        }
        this._Data = data || "";
        return this;
    }
    IsAsync() {
        return this._Async;
    }
    EncodeURI() {
        if (this._IsEnd === false) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].E("not end");
            return "";
        }
        return encodeURI(this._Url);
    }
    Param(key, value) {
        // if (this._ReceiveCaller == null) {
        //     UrlLog.Error("not bind event");
        // }
        this._Params[key] = value;
        return this;
    }
    static Post(data) {
        let url = new Url();
        url._MethodType = "post";
        url._Data = data;
        return url;
    }
    static Get() {
        let url = new Url();
        url._MethodType = "get";
        return url;
    }
    Send(clientInfo) {
        // if (this._IsDebug) {
        //     console.log("url=" + this._Url);
        // }
        if (this._AccountPass && this._AccountPass.length > 0) {
            this.Head(2, this._AccountPass);
        }
        if (clientInfo && typeof clientInfo == "object") {
            // let ClientInfo = {
            //     "brand": sys.brand || "",
            //     "model": sys.model || "",
            //     "platform": sys.platform || "",
            //     "version": s_d._SDKversion,
            //     "resolution": sys.resolution || _resolution
            // };
            // http.setRequestHeader("ClientInfo", clientInfo);
        }
        _Assist_Http__WEBPACK_IMPORTED_MODULE_0__["default"].Request(_sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].P(), this, 3, this._IsDebug);
    }
    SendCustomUrl() {
        _Assist_Http__WEBPACK_IMPORTED_MODULE_0__["default"].RequestNormal(_sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].P(), this, 3, this._IsDebug);
    }
    Clone() {
        let url = new Url();
        url._Slices = [];
        for (const item of this._Slices) {
            url._Slices.push(item);
        }
        url._Header = {};
        for (const key in this._Header) {
            url._Header[key] = this._Header[key];
        }
        url._AccountPass = this._AccountPass;
        url._MethodType = this._MethodType;
        url._Http = this._Http;
        url._DomainIndex = this._DomainIndex;
        url._Domains = [];
        for (const domain of this._Domains) {
            url._Domains.push(domain);
        }
        url._Version = this._Version;
        url._Category = this._Category;
        url._Manager = this._Manager;
        url._Router = this._Router;
        url._AppendList = [];
        for (const item of this._AppendList) {
            url._AppendList.push(item);
        }
        url._CAIndex = this._CAIndex;
        url._Url = this._Url;
        url._CSType = this._CSType;
        url._Async = this._Async;
        url._IsEnd = this._IsEnd;
        url._Data = JSON.parse(JSON.stringify(this._Data));
        url._ReceiveCaller = null;
        url._ReceiveMethod = null;
        url._ErrorCaller = null;
        url._ErrorMethod = null;
        url._ExceptionCaller = null;
        url._ExceptionMethod = null;
        return url;
    }
    OnReceive(c, m) {
        this._ReceiveCaller = c;
        this._ReceiveMethod = m;
        return this;
    }
    InvokeReceive(data) {
        let isBind = this._ReceiveCaller && this._ReceiveMethod;
        if (!isBind) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].W("not bind receive callback");
            return;
        }
        this._ReceiveMethod.call(this._ReceiveCaller, data);
    }
    OnError(c, m) {
        this._ErrorCaller = c;
        this._ErrorMethod = m;
        return this;
    }
    InvokeError(data) {
        let isBind = this._ErrorCaller && this._ErrorMethod;
        if (!isBind) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].W("not bind error callback");
            return;
        }
        this._ErrorMethod.call(this._ErrorCaller, data);
    }
    OnException(c, m) {
        this._ExceptionCaller = c;
        this._ExceptionMethod = m;
        return this;
    }
    InvokeException(data) {
        let isBind = this._ExceptionCaller && this._ExceptionMethod;
        if (!isBind) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["default"].W("not bind exception callback");
            return;
        }
        this._ExceptionMethod.call(this._ExceptionCaller, data);
    }
}
Url.AllUrls = [];


/***/ }),

/***/ "./sdk/Assist/Log.ts":
/*!***************************!*\
  !*** ./sdk/Assist/Log.ts ***!
  \***************************/
/*! exports provided: Log */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Log", function() { return Log; });
/* harmony import */ var _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../fw/Utility/LogUtility */ "./fw/Utility/LogUtility.ts");

class Log {
    static Log(message, ...args) {
        _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().L("YDHW_SDK", message, null, args);
    }
    static Debug(message, ...args) {
        _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().D("YDHW_SDK", message, null, args);
    }
    static Info(message, ...args) {
        _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().I("YDHW_SDK", message, null, args);
    }
    static Warn(message, ...args) {
        _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().W("YDHW_SDK", message, null, args);
    }
    static Error(message, ...args) {
        _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Me().E("YDHW_SDK", message, null, args);
    }
}


/***/ }),

/***/ "./sdk/Base/SDKP.ts":
/*!**************************!*\
  !*** ./sdk/Base/SDKP.ts ***!
  \**************************/
/*! exports provided: SDKErrorInfo, SDKP */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDKErrorInfo", function() { return SDKErrorInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDKP", function() { return SDKP; });
/* harmony import */ var _fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../fw/Assist/StorageApt */ "./fw/Assist/StorageApt.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../fw/Utility/LogUtility */ "./fw/Utility/LogUtility.ts");
/* harmony import */ var _Extensions_ClientLogSystem__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Extensions/ClientLogSystem */ "./sdk/Extensions/ClientLogSystem.ts");
/* harmony import */ var _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../fw/Enums/PLAT */ "./fw/Enums/PLAT.ts");





const YS = "YDHW-SDK:";
class SDKErrorInfo {
}
class SDKP {
    constructor() {
        // static readonly ERROR_DESCRIPTION: { [key: number]: SDKErrorInfo } = {
        //     1000: { ErrCode: 1000, Description: "appid无效", SolutionHint: "1.请检查appid是否和游点好玩后台及平台申请的一致"}, 
        //     1001: { ErrCode: 1001, Description: "version无效", SolutionHint: "请检查版本号是否和游点好玩后台配置的一致"}, 
        //     1002: { ErrCode: 1002, Description: "code与AccountPass必传其一", SolutionHint: "请联系游点好玩SDK技术"}, 
        //     1003: { ErrCode: 1003, Description: "QQ code无效", SolutionHint: "请检查appid是否和游点好玩后台及工程配置的一致"}, 
        //     1004: { ErrCode: 1004, Description: "OPPO token无效", SolutionHint: "请检查appid和包名是否和游点好玩后台及平台申请的一致"}, 
        //     1005: { ErrCode: 1005, Description: "VIVO token无效", SolutionHint: "请检查appid和包名是否和游点好玩后台及平台申请的一致"}, 
        //     1006: { ErrCode: 1006, Description: "头条 code无效", SolutionHint: "1.请检查appid是否和游点好玩后台及工程配置的一致;2.发布工程的appid是否和配置文件及后台配置的一致"}, 
        //     1007: { ErrCode: 1007, Description: "百度 code无效", SolutionHint: "1.请检查appid是否和游点好玩后台及工程配置的一致;2.发布工程的appid是否和配置文件及后台配置的一致"}, 
        //     1008: { ErrCode: 1008, Description: "魅族 token无效", SolutionHint: "1.请检查包名是否和游点好玩后台及平台申请的一致;2.发布工程的包名是否和配置文件及后台配置的一致"}, 
        //     1009: { ErrCode: 1009, Description: "UC code无效", SolutionHint: "1.请检查clientid是和游点好玩后台及工程配置的一致;2.发布工程的clientid是否和配置文件及后台配置的一致"}, 
        //     1010: { ErrCode: 1010, Description: "微信 code无效", SolutionHint: "1.请检查appid是否和游点好玩后台及工程配置的一致;2.发布工程的appid是否和配置文件及后台配置的一致"}, 
        //     1011: { ErrCode: 1011, Description: "分享图场景值为空", SolutionHint: "请核对传参是否有误"}, 
        //     1012: { ErrCode: 1012, Description: "积分墙模块信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1013: { ErrCode: 1013, Description: "领取积分墙相关信息不全", SolutionHint: "请核对传参是否有误"}, 
        //     1014: { ErrCode: 1014, Description: "自定义空间参数不正确", SolutionHint: "请核对传参是否有误"}, 
        //     1015: { ErrCode: 1015, Description: "自定义空间关联的游戏不存在", SolutionHint: "请到游点好玩后台核对"}, 
        //     1016: { ErrCode: 1016, Description: "自定义空间已达到最大值", SolutionHint: "请到游点好玩后台核对"}, 
        //     1017: { ErrCode: 1017, Description: "自定义空间不存在", SolutionHint: "请到游点好玩后台核对"}, 
        //     1018: { ErrCode: 1018, Description: "自定义空间参数格式不正确", SolutionHint: "请到游点好玩后台核对"}, 
        //     1019: { ErrCode: 1019, Description: "传入事件信息为空", SolutionHint: "请核对传参是否有误"},
        //     1020: { ErrCode: 1020, Description: "传入结果信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1021: { ErrCode: 1021, Description: "传入流失信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1022: { ErrCode: 1022, Description: "传入分享图信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1023: { ErrCode: 1023, Description: "传入视频广告信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1024: { ErrCode: 1024, Description: "传入Banner广告信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1025: { ErrCode: 1025, Description: "传入格子广告信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1026: { ErrCode: 1026, Description: "传入原生模板广告信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1027: { ErrCode: 1027, Description: "传入插屏广告信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1028: { ErrCode: 1028, Description: "传入积木广告信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1029: { ErrCode: 1029, Description: "传入原生广告信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1030: { ErrCode: 1030, Description: "传入盒子广告信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1031: { ErrCode: 1031, Description: "传入时长信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1032: { ErrCode: 1032, Description: "传入SDK统计信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1033: { ErrCode: 1033, Description: "传入SDK统计信息不全", SolutionHint: "请核对传参是否有误"}, 
        //     1034: { ErrCode: 1034, Description: "传入客户端错误信息为空", SolutionHint: "请核对传参是否有误"}, 
        //     1035: { ErrCode: 1035, Description: "传入卖量信息为空", SolutionHint: "请核对传参是否有误"},
        //     1036: { ErrCode: 1036, Description: "游戏配置无效", SolutionHint: "如需用到自定义配置数,请到游点好玩后后台配置,无需使用,可忽略"},
        //     1037: { ErrCode: 1037, Description: "游戏版本无效", SolutionHint: "请到游点好玩后台配置游戏版本号"},
        //     9000: { ErrCode: 9000, Description: "网络地址无效", SolutionHint: "请联系游点好玩SDK技术" },
        //     9001: { ErrCode: 9001, Description: "没有访问凭证", SolutionHint: "请检查是否登录成功" },
        //     9002: { ErrCode: 9002, Description: "访问凭证无效", SolutionHint: "请检查是否登录成功" },
        //     9003: { ErrCode: 9003, Description: "客户端版本过低", SolutionHint: "暂无" },
        //     9004: { ErrCode: 9004, Description: "数据校验不通过", SolutionHint: "暂无" },
        //     9999: { ErrCode: 9999, Description: "系统异常", SolutionHint: "请联系游点好玩SDK技术" },
        // };
        //这些是不暴露对外的
        this.Ctrl = null;
        this.Controller2 = null;
        this.Config = null;
        this.Name = "SDKPlatform";
        this.SysInfo = null;
        //BannerAd
        this.BnAdId = "";
        this.BnAd = null;
        this.BnStyle = null;
        this.COnRsBnAd = null;
        this.IsMTBnAd = false;
        this.IsSBnAd = false;
        //RewardVideoAd
        this.VdAdId = "";
        this.VdAd = null;
        this.IsShowVdAd = false;
        //InterstitialAd
        this.IntttAdId = "";
        this.IntttAd = null;
        //GridAd
        this.GridAdId = "";
        //AppBox
        this.AppBoxAdId = "";
        //BlockAdId
        this.BlockAdId = "";
        //UserInfoButton
        this.UIBtn = null;
        this.IsSimulator = false;
        this.NickName = "";
        this.AvatarUrl = "";
        this.Code = "";
        this.OpenID = "";
        this.AccountPass = "";
        this.NetType = "Get NetworkType failed!";
        // Version:string = "";
        this.Brand = "";
        this.Model = "";
        this.Resolution = "";
        this.AppName = ""; //字节跳动
        this.Env = "";
        this.InviteAccount = "";
        this.IsDebug = false;
        this.IsWechat = false;
        this.IsQQ = false;
        this.IsOppo = false;
        this.IsVivo = false;
        this.IsToutiao = false;
        this.IsBaidu = false;
        this.Is4399 = false;
        this.IsQutoutiao = false;
        this.Is360 = false;
        this.IsMomo = false;
        this.IsXiaomi = false;
        this.IsMeizu = false;
        this.IsUC = false;
        this.IsWeb = false;
        this.IsAlipay = false;
        this.IsQGMiniGame = false;
        this.IsQQMiniGame = false;
        this.IsOnMobile = false;
        this.IsOnIOS = false;
        this.IsOnIPhone = false;
        this.IsOnMac = false;
        this.IsOnIPad = false;
        this.IsOnAndroid = false;
        this.IsOnWP = false;
        this.IsOnQQBrowser = false;
        this.IsOnMQQBrowser = false;
        this.IsOnWeiXin = false;
        this.IsOnSafari = false;
        this.IsOnPC = false;
        this.IsCocosEngine = false;
        this.IsLayaEngine = false;
        this.Name = this.constructor.name;
        this.Config = window[_fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_0__["StorageApt"].SDK_CONFIG];
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Debug = Boolean(this.Config.debug);
        this.IsDebug = Boolean(this.Config.debug);
        this.Env = String(this.Config.env);
        let u = window.navigator.userAgent;
        this.IsOnMobile = window['isConchApp'] ? true : u.indexOf("Mobile") > -1;
        this.IsOnIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
        this.IsOnIPhone = u.indexOf("iPhone") > -1;
        this.IsOnMac = u.indexOf("Mac OS X") > -1;
        this.IsOnIPad = u.indexOf("iPad") > -1;
        this.IsOnAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1;
        this.IsOnWP = u.indexOf("Windows Phone") > -1;
        this.IsOnQQBrowser = u.indexOf("QQBrowser") > -1;
        this.IsOnMQQBrowser = u.indexOf("MQQBrowser") > -1 || (u.indexOf("Mobile") > -1 && u.indexOf("QQ") > -1);
        this.IsOnWeiXin = u.indexOf('MicroMessenger') > -1;
        this.IsOnSafari = u.indexOf("Safari") > -1;
        this.IsOnPC = !this.IsOnMobile;
        if (typeof (wx) != "undefined") {
            if (typeof (qq) != "undefined" && (u.indexOf('MiniGame') > -1 || u.indexOf('qqdevtools') > -1)) {
                this.L("平台:QQ");
                this.Ctrl = window['qq'];
                this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].QQ;
                this.IsQQ = true;
                this.IsQQMiniGame = true;
                this.initPlatform();
            }
            else if (u.indexOf('wechatdevtools') > -1) { //同时wx，wechatdevtools是微信模拟器
                this.L("平台:微信开发者工具");
                this.Ctrl = window['wx'];
                this.IsSimulator = this.Ctrl.getSystemInfoSync().platform == 'devtools';
                this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Wechat;
                this.IsWechat = true;
                this.initPlatform();
            }
            else if (typeof (tt) != "undefined" && (u.indexOf('MiniGame') > -1 || u.indexOf('bytedanceide') > -1)) {
                this.L("平台:今日头条");
                this.Ctrl = window['tt'];
                this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Toutiao;
                this.IsToutiao = true;
                this.initPlatform();
            }
            else if (u.indexOf('MiniGame') > -1) {
                this.L("平台:微信");
                this.Ctrl = window['wx'];
                this.IsSimulator = this.Ctrl.getSystemInfoSync().platform == 'devtools';
                this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Wechat;
                this.IsWechat = true;
                this.initPlatform();
            }
        }
        else if (typeof (qg) != "undefined" && u.indexOf('OPPO') > -1) {
            this.L("平台:Oppo");
            this.Ctrl = window['qg'];
            this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Oppo;
            this.IsOppo = true;
            this.IsQGMiniGame = true;
            this.initPlatform();
        }
        else if (typeof (qg) != "undefined" && u.indexOf('VVGame') > -1) {
            this.L("平台:Vivo");
            this.Ctrl = window['qg'];
            this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Vivo;
            this.IsVivo = true;
            this.IsQGMiniGame = true;
            this.initPlatform();
        }
        else if (typeof (swan) != "undefined" && u.indexOf('SwanGame') > -1) {
            this.L("平台:百度");
            this.Ctrl = window['swan'];
            this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Baidu;
            this.IsBaidu = true;
            this.initPlatform();
        }
        else if ( true && u.indexOf('4399') > -1) {
            this.L("平台:4399");
        }
        else if (typeof (qu_tou_tiao) != "undefined") {
            this.L("平台:趣头条");
        }
        else if ( true && u.indexOf('360') > -1) {
            this.L("平台:360");
        }
        else if (typeof (momo) != "undefined") {
            this.L("平台:陌陌");
        }
        else if (u.indexOf('QuickGame') > -1) {
            this.L("平台:小米");
            this.Ctrl = window['qg'];
            this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Xiaomi;
            this.IsXiaomi = true;
            this.IsQGMiniGame = true;
        }
        else if (typeof (mz) != "undefined" && u.indexOf('MZ-') > -1) {
            this.L("平台:魅族");
            this.Controller2 = window['mz'];
            this.Ctrl = window['qg'];
            this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Meizu;
            this.IsMeizu = true;
        }
        else if (typeof (uc) != "undefined") {
            this.L("平台:UC");
            this.Ctrl = window['uc'];
            this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].UC;
            this.IsUC = true;
            this.initPlatform();
        }
        else if (typeof (my) != "undefined" && u.indexOf('AlipayMiniGame') > -1) { //支付宝小游戏
            this.Ctrl = window['my'];
            this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Alipay;
            this.IsAlipay = true;
        }
        else if (typeof (document) != "undefined" && u.indexOf("Mobile") < 0) { // Web 平台
            this.L("平台:");
            this.Ctrl = window;
            this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Web;
            this.IsWeb = true;
        }
        else if (typeof (document) != "undefined" && u.indexOf("Mobile") > 0) { // Mobile H5 平台
            this.Ctrl = window;
            this.PfType = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Web;
            this.IsWeb = true;
        }
        else {
            console.error("当前平台识别不出来,不知道是什么鬼:" + u);
        }
        if (!window[_fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_0__["StorageApt"].SDK_CONFIG]) {
            console.error("缺少配置:" + _fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_0__["StorageApt"].SDK_CONFIG);
        }
    }
    L(...args) {
        this.Debug && console.log(...args);
    }
    CreateVdAd2(adUnitId, c, onClose, onError) {
        // throw new Error("Method not implemented.");
    }
    ShowVdAd2(c, onLoad, onShow, onException) {
        // throw new Error("Method not implemented.");
    }
    ShareAppMsg(title, imageUrl, query, c, m, target) {
        // throw new Error("Method not implemented.");
    }
    CreateNativeAd(adUnitId, c, onCreate, m) {
        // throw new Error("Method not implemented.");
    }
    ShowNativeAd(nativeId) {
        // throw new Error("Method not implemented.");
    }
    ClickNativeAd(nativeId) {
        // throw new Error("Method not implemented.");
    }
    HasShortcut(c, onSuccess, onFail, onComplete) {
        // throw new Error("Method not implemented.");
    }
    InstallShortcut(c, onSuccess, onFail, onComplete, message) {
        // throw new Error("Method not implemented.");
    }
    ExitGame() {
        // throw new Error("Method not implemented.");
    }
    InitEngine() {
        this.IsCocosEngine = window['cc'] && typeof window['cc'] === 'object' && window['cc']['sys'] === 'object'; //&& window['cc']['sys']['platform'] === window['cc']['sys'].WECHAT_GAME;
        this.IsLayaEngine = window['Laya'] && typeof Laya === 'object' && typeof Laya.Browser === 'function'; //&& typeof Laya.Browser.onWeiXin == 'boolean' && Laya.Browser.onWeiXin;
    }
    initPlatform() {
        try {
            var sys = this.Ctrl.getSystemInfoSync();
            let _platform = null;
            if (this.PfType == _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_4__["PLAT"].Vivo) { // || this.PlatformType == EM_PLATFORM_TYPE.Meizu
                _platform = sys.osType;
            }
            else {
                _platform = sys.platform;
            }
            if (_platform) {
                if (_platform == 'ios') {
                    this.IsOnIOS = true;
                }
                else if (_platform == 'android') {
                    this.IsOnAndroid = true;
                }
                else if (_platform == 'devtools') {
                    this.IsSimulator = true;
                }
            }
        }
        catch (error) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("获取系统信息失败");
        }
    }
    Init() {
        this.GetSysIf(this, (data) => {
            this.SysInfo = data;
            if (data.brand)
                this.Brand = data.brand;
            if (data.model)
                this.Model = data.model;
            if (data.appName)
                this.AppName = data.appName; //字节跳动
            let _resolution = "";
            if (data.screenWidth && data.screenHeight)
                _resolution = data.screenWidth + "x" + data.screenHeight;
            this.Resolution = data.resolution || _resolution;
        });
        return true;
    }
    Login(c, onSuccess, onError) {
        throw new Error("Login Method not implemented.");
    }
    HasAPI(name) {
        let func = this.Ctrl[name];
        if (!func || func === undefined || typeof func !== "function") {
            this.Warn("platform not exists api:" + name);
            return false;
        }
        return true;
    }
    HasAPI2(name) {
        let func = this.Controller2[name];
        if (!func || func === undefined || typeof func !== "function") {
            this.Warn("platform not exists api:" + name);
            return false;
        }
        return true;
    }
    LaunchInfo() {
        if (!this.HasAPI("getLaunchOptionsSync"))
            return;
        let info = this.Ctrl.getLaunchOptionsSync();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("LaunchInfo:" + JSON.stringify(info));
        return info;
    }
    GetSysIf(c, m) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("SDKPlatform GetSystemInfoSync()");
    }
    OnFrontend(c, m) {
    }
    OnBackend(c, m) {
    }
    OnError(c, m) {
        if (!this.HasAPI("onError"))
            return;
        let me = this;
        this.Ctrl.onError((e) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, m, e);
            me.Warn("onError:", e);
            _Extensions_ClientLogSystem__WEBPACK_IMPORTED_MODULE_3__["ClientLogSystem"].Me().setErrorStack(e, {});
        });
    }
    CFBBtn(_btnVect, hide) {
    }
    SFBBtn(visible) {
    }
    ExitMP() {
    }
    TriggerGC() {
    }
    OnShare(_data) {
    }
    NToMP(ownAppId, toAppId, toUrl, c, onSuccess, onFail) {
    }
    CreateBnAd(isSmall, adUnitId, isMisTouch, style, c, onResize, m) {
    }
    RefBnAd() {
    }
    DstyBnAd() {
        if (!this.BnAd) {
            return;
        }
        this.BnAd.destroy();
        this.BnAd = null;
    }
    BnAdV(val) {
    }
    ChangeBnAdStyle(style) {
        if (!this.BnAd || !style) {
            return;
        }
        if (style.left)
            this.BnAd.style.left = style.left;
        if (style.top)
            this.BnAd.style.top = style.top;
        if (style.width)
            this.BnAd.style.width = style.width;
        if (style.height)
            this.BnAd.style.height = style.height;
        this.BnAd._style = style;
        this.BnStyle = style;
    }
    CreateVdAd(adUnitId, c, onLoad, onCLose, onError) {
    }
    ShowVdAd(c, onShow, onException) {
    }
    CreateIntttAd(adUnitId, c, onLoad, onClose, onError) {
    }
    ShowIntttAd(c, m) {
    }
    ClearIntttAd() {
    }
    GetUserInfo(c, onSuccess, onError) {
        if (!this.HasAPI("getUserInfo")) {
            c && onError && onError.call(c);
            return;
        }
        this._Author('scope.userInfo', this, (res) => {
            this.Ctrl.getUserInfo({
                withCredentials: false,
                lang: 'zh_CN',
                success: (res) => {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("YDHW getUserInfo:" + JSON.stringify(res));
                    if (res.userInfo) {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, onSuccess, res.userInfo);
                    }
                },
                fail: (res) => {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, onError, res);
                }
            });
        }, () => {
            c && onError && onError.call(c);
        });
    }
    CreateUIBtn(btnInfo, c, onSuccess, onError) {
    }
    ShowUIBtn() {
        if (this.UIBtn) {
            this.UIBtn.show();
        }
    }
    HideUIBtn() {
        if (this.UIBtn) {
            this.UIBtn.hide();
        }
    }
    DestroyUIBtn() {
        if (this.UIBtn) {
            this.UIBtn.offTap();
            this.HideUIBtn();
            this.UIBtn.destroy();
            this.UIBtn = null;
        }
    }
    VibrateShort() {
    }
    VibrateLong() {
    }
    SetUserCloudStorage(_kvDataList) {
    }
    PostMessage(_data) {
    }
    CheckUpdate() {
    }
    GetNetworkType(c, m) {
    }
    CreateVideo(data) {
        return null;
    }
    LoadSubpackage(name, update) {
        return null;
    }
    ShowLoading() {
    }
    HideLoading() {
    }
    ShowShare(data) {
    }
    ShowModal(modal) {
    }
    _Author(_scope, c, onSuccess, onError) {
        if (!this.HasAPI("getSetting")) {
            c && onError && onError.call(c);
            return;
        }
        if (!this.HasAPI("authorize")) {
            c && onError && onError.call(c);
            return;
        }
        let self = this;
        this.Ctrl.getSetting({
            success(res) {
                if (!res.authSetting[_scope]) {
                    self.Ctrl.authorize({
                        scope: _scope,
                        success(res) {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, onSuccess, res);
                        },
                        fail() {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, onError, 'No authority');
                        }
                    });
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, onSuccess, res);
                }
            }
        });
    }
    Hook(name, c, m) {
        try {
            let tmpFunc = this.Ctrl[name];
            Object.defineProperty(this.Ctrl, name, { writable: true });
            this.Ctrl[name] = function () {
                let len = arguments.length;
                let tmpList = Array(len);
                for (let i = 0; i < len; i++) {
                    tmpList[i] = arguments[i];
                }
                let tmpOut = tmpFunc.apply(this, tmpList);
                if (Object.prototype.toString.call(name).slice(8, -1) == "Function") {
                    return m.call(c, tmpList, tmpOut);
                }
            };
            Object.defineProperty(this.Ctrl, name, { writable: false });
            this.Log("Hook Success name:" + name);
        }
        catch (e) {
            this.Log("Hook Exception:" + e + " name:" + name);
        }
    }
    Log(message, ...args) {
        this.IsDebug && _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_2__["LogUtility"].Me().L(YS + this.Name, message, null, args);
    }
    Debug(message, ...args) {
        this.IsDebug && _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_2__["LogUtility"].Me().D(YS + this.Name, message, null, args);
    }
    Info(message, ...args) {
        this.IsDebug && _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_2__["LogUtility"].Me().I(YS + this.Name, message, null, args);
    }
    Warn(message, ...args) {
        this.IsDebug && _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_2__["LogUtility"].Me().W(YS + this.Name, message, null, args);
    }
    Error(message, ...args) {
        this.IsDebug && _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_2__["LogUtility"].Me().E(YS + this.Name, message, null, args);
    }
}


/***/ }),

/***/ "./sdk/Commerce/VivoC.ts":
/*!*******************************!*\
  !*** ./sdk/Commerce/VivoC.ts ***!
  \*******************************/
/*! exports provided: IsStartupByShortcut, VivoP, VivoC */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IsStartupByShortcut", function() { return IsStartupByShortcut; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VivoP", function() { return VivoP; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VivoC", function() { return VivoC; });
/* harmony import */ var _Mgr_CommerceMgr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Mgr/CommerceMgr */ "./sdk/Mgr/CommerceMgr.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Config/SDKConfig */ "./sdk/Config/SDKConfig.ts");
/* harmony import */ var _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../fw/Utility/TimerUtility */ "./fw/Utility/TimerUtility.ts");
/* harmony import */ var _Base_SDKP__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../Base/SDKP */ "./sdk/Base/SDKP.ts");
/* harmony import */ var _fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../fw/Assist/StorageApt */ "./fw/Assist/StorageApt.ts");







const IsStartupByShortcut = 1;
class VivoP extends _Base_SDKP__WEBPACK_IMPORTED_MODULE_5__["SDKP"] {
    constructor() {
        super();
        /**
         * 唯一标识码
         */
        this.VivoGuidCode = 5555;
        this._NativeAd = null;
        this._BannerAd = null;
        this._VideoAd = null;
        this._InterstitialAd = null;
        this._IsShowBannerAd = false;
        this.Name = this.constructor.name;
    }
    Init() {
        if (!super.Init()) {
            return false;
        }
        _fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_6__["StorageApt"].WriteString = (key, value) => {
            this.Ctrl.setStorageSync({ key: key, value: value });
        };
        _fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_6__["StorageApt"].WriteObject = (key, value) => {
            this.Ctrl.setStorageSync({ key: key, value: JSON.stringify(value) });
        };
        _fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_6__["StorageApt"].ReadString = (key) => {
            return this.Ctrl.getStorageSync({ key: key });
        };
        _fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_6__["StorageApt"].ReadObject = (key) => {
            let value = this.Ctrl.getStorageSync({ key: key });
            if (value.length == 0) {
                return {};
            }
            let retTmp = {};
            try {
                if (typeof (value) == "object") {
                    return value;
                }
                retTmp = JSON.parse(value);
            }
            catch (err) {
                this.Error(err);
            }
            return retTmp;
        };
        _fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_6__["StorageApt"].DeleteObject = (key) => {
            this.Ctrl.deleteStorageSync({ key: key });
        };
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Me.DV(this.VivoGuidCode, "").Store();
        return true;
    }
    Login(c, onSuccess, onFail) {
        console.log("YDHW ---Login-1");
        this.LoginByTourist(c, onSuccess, onFail);
    }
    /**
     * 授权登录
     * @param c
     * @param onSuccess
     * @param onFail
     */
    LoginByAuth(c, onSuccess, onFail) {
        this.Ctrl.login({
            success(result) {
                let data = {
                    code: result.token,
                    token: null,
                };
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onSuccess, result.token, null, data);
            },
            fail(error) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("登录错误:" + JSON.stringify(error));
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onFail, error);
            },
            complete(result) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L('complete:', result);
            }
        });
    }
    /**
     * 游客登录
     * @param c
     * @param onSuccess
     * @param onFail
     */
    LoginByTourist(c, onSuccess, onFail) {
        console.log("YDHW ---LoginByTourist-1");
        this.GetCODE(function (_code) {
            console.log("YDHW ---LoginByTourist-2:"+_code);
            let data = {
                code: _code,
                token: null,
            };
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onSuccess, null, _code, data);
        });
    }
    /**
    * 获取本地CODE
    */
    GetCODE(_callback) {
        let l_code = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().GVT(this.VivoGuidCode, "");
        // let l_code = p_f.getLD(ld_key.LD_KEY_TOURIST_CODE);
        console.log("YDHW ---GetCODE-1",l_code);
        if (l_code && l_code != '') {
            console.log("YDHW ---GetCODE-1-2",l_code);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("YLSDK ---getCODE:", l_code);
            if (_callback)
                _callback(l_code);
        }
        else {
            console.log("YDHW ---GetCODE-1-3",l_code);
            let sys = this.Ctrl.getSystemInfoSync();
            let me = this;
            this.getMyIp(function (ipStr) {
                console.log("YDHW ---GetCODE-1-3-1",ipStr);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("YLSDK ---_sdk.getMyIp--ipStr:", ipStr);
                let product = sys.product;
                let appid = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().AppId;
                let ip = "192.168.1.125";
                if (ipStr && ipStr != "") {
                    ip = ipStr;
                }
                let nowTime = new Date().getTime();
                let random = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
                // product + APPID + IP + 时间戳 + 随机
                let GUID = product + appid + ip + nowTime + random;
                // let l_code = hex_md5(md5); 
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("YLSDK --getCODE-madeCode:", GUID);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SVT(me.VivoGuidCode, GUID);
                if (_callback)
                    _callback(GUID);
            });
        }
    }
    /**
     * 请求本机IP
     * "http://pv.sohu.com/cityjson"
     * @param _callback
     */
    getMyIp(_callback) {
        console.log("YDHW ---getMyIp-1");
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Me.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].GetMyIp, this, (res) => {
            console.log("YDHW ---getMyIp-1-1");
            let ip_str = "192.168.255.255";
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("YLSDK getMyIp-res:" + res);
            console.log("YDHW ---getMyIp-1-2");
            try {
                if (res != "") {
                    let str = JSON.stringify(res);
                    // var str = "var returnCitySN = {\"cip\": \"61.141.254.186\", \"cid\": \"440300\", \"cname\": \"广东省深圳市\"};"
                    var str_1 = str.replace(/var returnCitySN = /, "");
                    var str_2 = str_1.replace(/};/, "}");
                    var result = JSON.parse(str_2);
                    var result_2 = JSON.parse(result);
                    console.log("YDHW ---getMyIp-1-3");
                    if (result_2 && result_2.cip) {
                        ip_str = result_2.cip;
                    }
                    console.log("YDHW ---getMyIp-1-4");
                }
            }
            catch (res) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("YLSDK getMyIp-error:" + res);
            }
            if (_callback)
                _callback(ip_str);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("YLSDK ---result:", ip_str);
        });
    }
    GetSysIf(c, m) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, this.Ctrl.getSystemInfoSync());
    }
    OnFrontend(c, m) {
        if (!this.HasAPI("onShow"))
            return;
        this.Ctrl.onShow((e) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, e);
        });
    }
    OnBackend(c, m) {
        if (!this.HasAPI("onHide"))
            return;
        this.Ctrl.onHide((e) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, e);
        });
    }
    CreateBnAd(isSmall, adUnitId, isMisTouch, style, c, onResize) {
        if (!this.HasAPI("createBannerAd"))
            return;
        this.COnRsBnAd = c;
        this.BnAdId = adUnitId;
        this.OnRsBnAd = onResize;
        this.IsMTBnAd = isMisTouch;
        let bannerInfo = null;
        if (style) {
            this.BnStyle = style;
            bannerInfo = {
                posId: adUnitId,
                style: style
            };
        }
        else {
            bannerInfo = {
                posId: adUnitId,
            };
        }
        this.BnAd = this.Ctrl.createBannerAd(bannerInfo);
        //在外面设置一下Style，这样可以强制刷新Banner，触发onResize监听
        if (style && style.left) {
            this.BnAd.style.left = style.left;
        }
        let me = this;
        this.BnAd.onSize((result) => {
            if (me.OnRsBnAd) {
                this.COnRsBnAd && this.OnRsBnAd.call(this.COnRsBnAd, result);
            }
            else {
                if (style) {
                    me.BnAd.style = style;
                }
            }
            if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchLog) {
                this.Log("CreateBannerAd onSize:" + JSON.stringify(me.BnAd.style));
            }
        });
    }
    RefBnAd() {
        if (!this.BnAd) {
            this.Warn("RefreshBannerAd banner ad is not exists");
            return;
        }
        this.BnAd.show();
    }
    DstyBnAd() {
        if (!this.BnAd) {
            return;
        }
        this.BnAd.destroy();
        this.BnAd = null;
    }
    BnAdV(val) {
        if (!this.BnAd) {
            this.Warn("SetBannerVisible banner ad is not exists");
            return;
        }
        if (val === true) {
            let adShow = this.BnAd.show();
            let me = this;
            adShow && adShow.then(() => {
                me.IsSBnAd = true;
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("YLSDK banner广告展示成功");
            }).catch(err => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("YLSDK banner广告展示失败:" + JSON.stringify(err));
            });
        }
        else {
            let adHide = this.BnAd.hide();
            adHide && adHide.then(() => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("YLSDK banner隐藏展示成功");
            }).catch(err => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("YLSDK banner广告隐藏失败" + JSON.stringify(err));
            });
            this.IsSBnAd = false;
        }
    }
    ChangeBnAdStyle(style) {
        if (!this.BnAd || !style) {
            return;
        }
        if (!this.BnAd.style) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L('VIVO 平台Banner 没有 style 不能调整');
            return;
        }
        if (style.left)
            this.BnAd.style.left = style.left;
        if (style.top)
            this.BnAd.style.top = style.top;
        if (style.width)
            this.BnAd.style.width = style.width;
        if (style.height)
            this.BnAd.style.height = style.height;
        this.BnAd._style = style;
        this.BnStyle = style;
    }
    CreateVdAd2(adUnitId, c, _onCLose, _onError) {
        if (!this.HasAPI("createRewardedVideoAd"))
            return;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("createRewardedVideoAd adUnitId=" + adUnitId);
        let me = this;
        if (!adUnitId) {
            me.Warn("Recorder 激励视频 ID出错");
            me.IsShowVdAd = false;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, _onError, false);
            return;
        }
        let onError = function (error) {
            me.Warn("VideoAd onError:", error);
            me.IsShowVdAd = false;
            me.removeVideoAd();
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, _onError, error);
        };
        let onClose = function (res) {
            me.IsShowVdAd = false;
            me.removeVideoAd();
            _onCLose.call(c, res);
        };
        if (this.VdAd) {
            this.VdAd.offError(onError);
            this.VdAd.offClose(onClose);
            this.VdAd.destroy();
            this.VdAd = null;
        }
        this.VdAd = this.Ctrl.createRewardedVideoAd({ posId: adUnitId });
        this.VdAdId = adUnitId;
        if (this.VdAd) {
            this.VdAd.onError(onError);
            //设置视频关闭回调
            this.VdAd.onClose(onClose);
        }
    }
    removeVideoAd() {
        this.VdAd.destroy();
        this.VdAd = null;
    }
    ShowVdAd2(c, _onLoad, onShow, onException) {
        if (!this.VdAd) {
            this.Error("VideoAd is null");
            return;
        }
        this.IsShowVdAd = true;
        let me = this;
        let onLoad = function () {
            me.IsShowVdAd = true;
            c && _onLoad && _onLoad.call(c, true);
            me.Log("VideoAd onLoad");
            me.VdAd.show().then(() => {
                c && onShow && onShow.call(c);
            }).catch(res => {
                me.IsShowVdAd = false;
                me.Warn("RewardVideoAd.show fail:" + JSON.stringify(res));
                c && onException && onException.call(c, res);
            });
        };
        if (this.VdAd) {
            this.VdAd.offLoad(onLoad);
            this.VdAd.onLoad(onLoad);
        }
        let adLoad = this.VdAd.load();
        adLoad.then(() => {
        }).catch((err) => {
            this.IsShowVdAd = false;
            this.Warn("RewardVideoAd.load fail:" + JSON.stringify(err));
        });
    }
    CreateIntttAd(adUnitId, c, onLoad, onClose, onError) {
        if (!this.HasAPI("createInterstitialAd"))
            return;
        let me = this;
        let _onLoad = () => {
            c && onLoad && onLoad.call(c);
            me.Log("InterstitialAd onLoad");
        };
        let _onError = (error) => {
            me.Warn("InterstitialAd onError:", error);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onError, error);
            me.IntttAd = null;
        };
        let _onClose = (result) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onClose, result);
            me.IntttAd = null;
        };
        if (this.IntttAd) {
            try {
                this.IntttAd.offLoad(_onLoad);
                this.IntttAd.offError(_onError);
                this.IntttAd.offClose(_onClose);
            }
            catch (e) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("InterstitialAd.off error:", e);
            }
            this.IntttAd = null;
        }
        this.IntttAd = this.Ctrl.createInterstitialAd({ adUnitId: adUnitId });
        this.IntttAdId = adUnitId;
        this.IntttAd.onLoad(_onLoad);
        this.IntttAd.onError(_onError);
        this.IntttAd.onClose(_onClose);
    }
    ShowIntttAd(c, m) {
        if (!this.IntttAd) {
            return;
        }
        let adShow = this.IntttAd.show();
        let me = this;
        adShow && adShow.then(() => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m);
            me.Log('YLSDK InterstitialAd.show-then');
        }).catch((err) => {
            me.Warn('YLSDK InterstitialAd.show-fail:', JSON.stringify(err));
            me.IntttAd = null;
        });
    }
    CreateNativeAd(adUnitId, c, onCreate, m) {
        if (!this.HasAPI("createNativeAd"))
            return;
        if (this._NativeAd) {
            this._NativeAd = null;
        }
        this._NativeAd = this.Ctrl.createNativeAd({ adUnitId: adUnitId });
        c && onCreate && onCreate.call(c);
        let me = this;
        //设置广告加载成功回调
        this._NativeAd.onLoad((result) => {
            if (result && result.adList) {
                let list = result.adList;
                me._NativeAdInfoList = list;
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, result.adList);
                me.Log("NativeAd onLoad");
            }
            else {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("返回空广告,请先去执行reportAdShow和reportAdClick使用上一条广告！");
            }
        });
        //保留上一次原生广告信息，因为原生广告刷新可能和上一次的信息一样，导致不会进入onload方法
        this._NativeAdInfoList && _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, this._NativeAdInfoList);
        this._NativeAd.onError((error) => {
            me.Warn("NativeAd onError:", error);
        });
        this._NativeAd.load();
    }
    ShowNativeAd(nativeId) {
        this._NativeAd && this._NativeAd.reportAdShow({ adId: nativeId });
    }
    ClickNativeAd(nativeId) {
        this._NativeAd && this._NativeAd.reportAdClick({ adId: nativeId });
    }
    HasShortcut(c, onSuccess, onFail) {
        if (!this.HasAPI("hasShortcutInstalled"))
            return;
        this.Ctrl.hasShortcutInstalled({
            success: function (status) {
                if (status) {
                    c && onSuccess.call(c, null);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onFail, null);
                }
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("hasShortcutInstalled status:" + status);
            },
            fail: function (error) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("hasShortcutInstalled error:" + error);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onFail, error);
            }
        });
    }
    InstallShortcut(c, onSuccess, onFail, onComplete, message) {
        if (!this.HasAPI("installShortcut"))
            return;
        this.Ctrl.installShortcut({
            message: message,
            success: function () {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("installShortcut success");
                c && onSuccess && onSuccess.call(c);
            },
            fail: function (error) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("InstallShortcut error:" + error);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onFail, error);
            },
            complete: function () {
                c && onComplete && onComplete.call(c);
            }
        });
    }
    IsStartupByShortcut(c, onSuccess, onFail) {
        if (!this.HasAPI("isStartupByShortcut"))
            return;
        this.Ctrl.isStartupByShortcut({
            success: function (status) {
                if (status) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L('通过桌面图标启动应用');
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L('不是通过桌面图标启动应用');
                }
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onSuccess, status);
            },
            fail: function (error) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onFail, error);
            }
        });
    }
    /**
     * 使手机发生较短时间的振动（20 ms）
     */
    VibrateShort() {
        if (!this.HasAPI("vibrateShort"))
            return;
        this.Ctrl.vibrateShort();
    }
    VibrateLong() {
        if (!this.HasAPI("vibrateLong"))
            return;
        this.Ctrl.vibrateLong();
    }
    ExitGame() {
        if (!this.HasAPI("exitApplication"))
            return;
        this.Ctrl.exitApplication();
    }
}
class VivoC extends _Mgr_CommerceMgr__WEBPACK_IMPORTED_MODULE_0__["CommerceMgr"] {
    constructor() {
        super();
        this._LoginInfo = null;
        this.P = null;
        this._LastTimeRefreshBannerAd = 0;
        this.Name = "YDHW-SDK:" + this.constructor.name;
    }
    Init() {
        super.Init();
        this.P.OnFrontend(this, (res) => {
            this.Log("OnFrontend");
            this._ShareAppInfo && this.Strategy && this.Strategy.GetSRlt(this._ShareAppInfo, this, (shareBackInfo) => {
                this.Warn("本次分享");
                this._CallerShareCardBack && this._EvtOnShareCardBack && this._EvtOnShareCardBack.call(this._CallerShareCardBack, shareBackInfo);
                this._EvtOnShareCardBack = null;
            });
            this.Invoke(10069, res);
        });
        this.P.OnBackend(this, (res) => {
            this.Log("OnBackend");
            this.Invoke(10070, res);
        });
        this.DM(10081, this, (c, m) => {
            console.log("YDHW ---10081-登录");
            let self = this;
            this.P.Login(this, (code, token, result) => {
                console.log("YDHW ---10081-1-1",code, token);
                this.Code = code;
                if (!code) {
                    //游客身份登录的情况下，和后台约定好了
                    //将token传过去,同时pkgName设置为anonymousCode
                    this.Code = token;
                    this.PkgName = 'anonymousCode';
                }
                if (this._LoginInfo) {
                    this._LoginInfo.code = this.Code || "";
                    this._LoginInfo.token = this.Code || "";
                    this._LoginInfo.pkgName = this.PkgName || "";
                }
                else {
                    // this._LoginInfo = {
                    //     code: result.code || "",
                    //     token: result.token || "",
                    //     pkgName: this.PkgName || "",
                    // };
                }
                this.Log("LoginInfo:", JSON.stringify(this._LoginInfo));
                this._InnerLogin(c, m);
            }, (error) => {
                this.Error("登录失败Error:" + error);
                c && m.call(c, false);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10081);
        });
        this.DM(10034, this, (isMisTouch, isShow, c, m) => {
            this._CreateBannerAd(false, isMisTouch, isShow, null, c, m, null);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10034);
        });
        this.DM(10035, this, (isMisTouch, isShow, c, m) => {
            this._CreateBannerAd(true, isMisTouch, isShow, null, c, m, null);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10035);
        });
        this.DM(10036, this, (isMisTouch, isShow, style, c, m, onResize) => {
            this._CreateBannerAd(false, isMisTouch, isShow, style, c, m, onResize);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10036);
        });
        this.DM(10037, this, (style) => {
            this.P.ChangeBnAdStyle(style);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10037);
        });
        this.DM(10038, this, () => {
            this.Log("ShowBannerAd");
            let nowTime = new Date().getTime();
            let last = this._LastTimeRefreshBannerAd || nowTime;
            let isSmall = false;
            let IsOn = false;
            let interval = 0;
            let create = false; //是否走创建banner流程
            if (this.BnRefCfg) {
                IsOn = this.BnRefCfg.IsOn || false;
                interval = this.BnRefCfg.Interval || _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_3__["SDKCfg"].DIFRefreshBannerAd;
            }
            if (this.P.BnAd) {
                if (IsOn) {
                    isSmall = this._IsSmallBannerAd || false;
                }
                let isRefresh = ((nowTime - last) >= _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_3__["SDKCfg"].DIFRefreshBannerAd * 1000);
                this._LastTimeRefreshBannerAd = nowTime;
                if (IsOn && isRefresh) {
                    //到时间定时更新banner
                    this.Log("ShowBannerAd CreateBannerAd");
                    create = true;
                    this._CreateBannerAd(isSmall, this.P.IsMTBnAd, true, this.P.BnStyle, this.P.OnRsBnAd, null, null);
                }
                if (!create) {
                    this.P.BnAdV(true);
                    this.Invoke(10002, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].SHOW, this.P.BnAdId);
                    this._needShowBannerAd = false;
                }
            }
            if (!create && IsOn) {
                this.Log(`将在${interval}秒后重新创建banner`);
                _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_4__["default"].RemoveDelayTimer(this._BannerAdDelayTimerId);
                this._BannerAdDelayTimerId = _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_4__["default"].Delay(interval * 1000, this, () => {
                    if (this.P.IsSBnAd)
                        this._CreateBannerAd(isSmall, this.P.IsMTBnAd, true, this.P.BnStyle, this.P.OnRsBnAd, null, null); //延迟刷新banner
                });
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10038);
        });
        this.DM(10039, this, () => {
            this.Log("HideBannerAd");
            this.P.BnAdV(false);
            this._needShowBannerAd = false;
            _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_4__["default"].RemoveDelayTimer(this._BannerAdDelayTimerId);
            this._BannerAdDelayTimerId = "";
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10039);
        });
        this.DM(10016, this, (isMisTouched) => {
            if (!this.P.BnAd) {
                this.Warn("BannerAd is null");
            }
            else if (this.BnRefCfg && this.BnRefCfg.IsForceRefresh) {
                let nowTime = new Date().getTime();
                let lastTime = this._LastTimeRefreshBannerAd || nowTime;
                let interval = this.BnRefCfg.MinimumInterval || _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_3__["SDKCfg"].DIFRefreshBannerAd;
                if (nowTime - lastTime >= interval * 1000) {
                    if (this.P.IsSBnAd && this._IsSmallBannerAd) {
                        this.Invoke(10035, isMisTouched, true, this, () => {
                        });
                    }
                    else if (this.P.IsSBnAd) {
                        this.Invoke(10034, isMisTouched, true, this, (isOk) => {
                        });
                    }
                    else {
                        this._LastTimeRefreshBannerAd = nowTime - (this.BnRefCfg.MinimumInterval * 1000 + 1000);
                    }
                }
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10016);
        });
        this.DM(10041, this, () => {
            if (!this.VideoAdUnitIdList || this.VideoAdUnitIdList.length == 0) {
                this.Error("VideoAdUnitIdList is empty.");
            }
            else if (!this.P.VdAd) {
                let me = this;
                let index = Math.floor(Math.random() * this.VideoAdUnitIdList.length);
                let adId = this.VideoAdUnitIdList[index];
                this.P.CreateVdAd2(adId, this, (result) => {
                    if (result && result.isEnded || result === undefined) {
                        me.Log("视频播放完成");
                        me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["VIDEO_P_T"].FINISH);
                        me._AddUnlockVideoLevelNumber(me._UnlockCustomNumber);
                        this.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].PLAY_FINISH, adId);
                        if (me._IsAddPower) {
                            me._AddPowerByVideo();
                        }
                    }
                    else { //播放中途退出
                        me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["VIDEO_P_T"].CANCEL);
                        me.Log("视频播放中途取消");
                        this.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].PLAY_CANCEL, adId);
                    }
                    me._UnlockCustomNumber = null;
                    me._EvtOnCloseRewardVideoAd = null;
                    me._showVideoAd = false;
                }, (error) => {
                    me.Log("RewardVideoAd onError:", error);
                    this.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].LOAD_FAIL, adId);
                    me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["VIDEO_P_T"].FAIL);
                    me._EvtOnCloseRewardVideoAd = null;
                    me._UnlockCustomNumber = null;
                    me._showVideoAd = false;
                });
                this.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].CREATE, adId);
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10041);
        });
        this.DM(10042, this, (unlockCustomNumber, isAddPower, c, onClose) => {
            let me = this;
            this._IsRewardVideoAdFinish = false;
            this._UnlockCustomNumber = unlockCustomNumber;
            this._CallerRewardVideoAd = c;
            this._EvtOnCloseRewardVideoAd = onClose;
            this._IsAddPower = isAddPower || false;
            if (!this.P.VdAd) {
                this.Invoke(10041);
                if (!this.P.VdAd) {
                    return;
                }
            }
            this.P.ShowVdAd2(this, () => {
                me._IsRewardVideoAdFinish = true;
                me.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].LOAD_SUCCESS, me._P.VdAdId);
            }, () => {
                me.Log("视频播放成功");
                me._showVideoAd = true;
                me._IsRewardVideoAdFinish = true;
                me.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].SHOW, me._P.VdAdId);
            }, () => {
                me.Error("视频播放失败");
                me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["VIDEO_P_T"].FAIL);
                me._EvtOnCloseRewardVideoAd = null;
                me._showVideoAd = false;
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10042);
        });
        this.DM(10052, this, (isShow, c, onLoad, onClose, onError) => {
            let nowTime = new Date().getTime();
            if (this._P.IntttAd && this._intttAdWaitShow) {
                this.Invoke(10053);
                return;
            }
            if (!this.InterstitialAdUnitIdList || this.InterstitialAdUnitIdList.length == 0) {
                this.Log("InterstitialAdUnitIdList is empty");
                return;
            }
            if (!this._CanCreateIntttAd()) {
                c && onError && onError.call(c);
                return;
            }
            let me = this;
            let index = Math.floor(Math.random() * this.InterstitialAdUnitIdList.length);
            let adId = this.InterstitialAdUnitIdList[index];
            if (!this._P.IntttAd) {
            }
            this.Invoke(10004, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].CREATE, adId);
            this._P.CreateIntttAd(adId, this, () => {
                me.Invoke(10004, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].LOAD_SUCCESS, adId);
                c && onLoad && onLoad.call(c);
                me.Log("InterstitialAd OnLoad");
            }, (result) => {
                me.Invoke(10004, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].CLOSE, adId);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onClose, result);
                me._showInterstitialAd = false;
                me._LCIntttAd = new Date().getTime();
            }, (error) => {
                me.Invoke(10004, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].LOAD_FAIL, adId);
                me.Warn("InterstistAd Error:" + error + " adId:" + adId);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onError, error);
                me._showInterstitialAd = false;
                me._LCIntttAd = new Date().getTime();
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10052);
            if (isShow) {
                me.Invoke(10053);
            }
        });
        this.DM(50001, this, (c, onSuccess, onFail) => {
            this.P.IsStartupByShortcut(c, onSuccess, onFail);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(50001);
        });
    }
    _CreateBannerAd(isSmall, isMisTouch, isShow, style, c, m, onResize) {
        if (!this.BannerAdUnitIdList || this.BannerAdUnitIdList.length == 0) {
            this.Error("BannerAdUnitIdList is empty.");
        }
        else {
            if (this.P.BnAd) {
                this.P.DstyBnAd();
                this.P.IsSBnAd = false;
            }
            let me = this;
            this._needShowBannerAd = true;
            let index = Math.floor(Math.random() * this.BannerAdUnitIdList.length);
            let adId = this.BannerAdUnitIdList[index];
            this.P.CreateBnAd(isSmall, adId, isMisTouch, style, this, onResize);
            this.Invoke(10002, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].CREATE, adId);
            //是否定时刷新banner
            let isOn = this.BnRefCfg && this.BnRefCfg.IsOn;
            if (isOn) {
                this._IsSmallBannerAd = isSmall;
            }
            this.P.BnAd.onLoad(function () {
                me.Log("BannerAd onLoad");
                me.Invoke(10002, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].LOAD_SUCCESS, adId);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                if (isShow && me._needShowBannerAd)
                    me.Invoke(10038);
            });
            this.P.BnAd.onError((error) => {
                me.Invoke(10002, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["TP_AD"].LOAD_FAIL, adId);
                me.Warn("BannerAd onError:", error);
                this.P.DstyBnAd();
                this._needShowBannerAd = false;
                this.P.IsSBnAd = false;
                if (isOn) {
                    let delay = me.BnRefCfg.Interval;
                    me.Log(`将在${delay}秒后重新创建banner(调用ylBannerAdHide会取消重试`);
                    _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_4__["default"].RemoveDelayTimer(me._BannerAdDelayTimerId);
                    me._BannerAdDelayTimerId = _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_4__["default"].Delay(delay * 1000, me, () => {
                        me._CreateBannerAd(isSmall, isMisTouch, isShow, style, onResize, c, m); //延迟刷新banner
                    });
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            });
            // this.Platform.BannerAd.onClose && this.Platform.BannerAd.onClose(()=>{
            //     if (me.SwitchLog) { me.Log("广告被关闭") };
            //     this.Platform.DestroyBannerAd();
            // });
        }
    }
}


/***/ }),

/***/ "./sdk/Config/SDKConfig.ts":
/*!*********************************!*\
  !*** ./sdk/Config/SDKConfig.ts ***!
  \*********************************/
/*! exports provided: SDKCfg */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDKCfg", function() { return SDKCfg; });
//SDKConfig
class SDKCfg {
}
/**
 * 场景白名单
 */
SDKCfg.SceneWhiteList = [];
/**
 * 强制刷新时间（秒）
 * 全称:DefaultIntervalForceRefreshBannerAd
 */
SDKCfg.DIFRefreshBannerAd = 10;


/***/ }),

/***/ "./sdk/Extensions/ClientLogSystem.ts":
/*!*******************************************!*\
  !*** ./sdk/Extensions/ClientLogSystem.ts ***!
  \*******************************************/
/*! exports provided: ClientLogSystem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientLogSystem", function() { return ClientLogSystem; });
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../fw/Utility/TimerUtility */ "./fw/Utility/TimerUtility.ts");
/* harmony import */ var Utils_TimeUtils__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! Utils/TimeUtils */ "./src/Utils/TimeUtils.ts");



class StackInfo {
    constructor(errStack, logStr, strDate) {
        this.errStack = errStack;
        this.logStr = logStr;
        this.strDate = strDate;
    }
}
class ClientLogSystem {
    constructor() {
        this._SendErrorStackTimerId = "SendErrorStackTimerId";
        this.stackList = new Array(); //异常上报服务器列表
        this.stackTab = new Array(); //异常标记列表，用于报错过的异常拦截掉
    }
    ;
    static Me() {
        return ClientLogSystem._Me;
    }
    setErrorStack(ErrStack, LogStr) {
        if (this.hasErrorStack(JSON.stringify(ErrStack))) {
            // SDK.W("DYHW ---已经提交过该异常");
            return;
        }
        let StrDate = Utils_TimeUtils__WEBPACK_IMPORTED_MODULE_2__["default"].formatDate(new Date(), "YYYY-MM-dd hh:mm:ss");
        this.stackList.push(new StackInfo(ErrStack, LogStr, StrDate));
        this.stackTab.push(JSON.stringify(ErrStack));
        this.sendErrorStack();
    }
    getErrorStack() {
        return this.stackList.pop();
    }
    hasErrorStack(ErrStack) {
        let index = this.stackTab.indexOf(ErrStack);
        // SDK.W("DYHW ---hasErrorStack:",index);
        return (index >= 0);
    }
    sendErrorStack() {
        if (this.stackList && this.stackList.length > 0) {
            _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_1__["default"].RemoveTimer(this._SendErrorStackTimerId);
            _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_1__["default"].AddTimer(this._SendErrorStackTimerId, 1000, this, null, () => {
                let stack = this.getErrorStack();
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().Invoke(10009, stack.errStack, stack.logStr, stack.strDate);
                let sLength = this.stackList.length;
                return (sLength == 0);
            });
        }
    }
}
ClientLogSystem._Me = new ClientLogSystem();


/***/ }),

/***/ "./sdk/Extensions/CloudAPI.ts":
/*!************************************!*\
  !*** ./sdk/Extensions/CloudAPI.ts ***!
  \************************************/
/*! exports provided: EM_CLOUD_API_TYPE, CloudAPI */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EM_CLOUD_API_TYPE", function() { return EM_CLOUD_API_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CloudAPI", function() { return CloudAPI; });
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _Model_Data__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Model/Data */ "./sdk/Model/Data.ts");



var EM_CLOUD_API_TYPE;
(function (EM_CLOUD_API_TYPE) {
    EM_CLOUD_API_TYPE[EM_CLOUD_API_TYPE["String"] = 0] = "String";
    EM_CLOUD_API_TYPE[EM_CLOUD_API_TYPE["StringArray"] = 1] = "StringArray";
    EM_CLOUD_API_TYPE[EM_CLOUD_API_TYPE["StringSet"] = 2] = "StringSet";
    EM_CLOUD_API_TYPE[EM_CLOUD_API_TYPE["StringMap"] = 3] = "StringMap";
    EM_CLOUD_API_TYPE[EM_CLOUD_API_TYPE["StringRandom"] = 4] = "StringRandom";
})(EM_CLOUD_API_TYPE || (EM_CLOUD_API_TYPE = {}));
class CloudAPI {
    static All() {
    }
    static GetObject(key, c, m) {
        let request = new _Model_Data__WEBPACK_IMPORTED_MODULE_2__["StoreValueReq"]();
        request.name = key;
        request.cmd = "get";
        request.args = "";
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].StoreValue, request, this, (result) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].C(c, m, JSON.parse(result.value));
        });
    }
    static GetString(key, c, m) {
        let request = new _Model_Data__WEBPACK_IMPORTED_MODULE_2__["StoreValueReq"]();
        request.name = key;
        request.cmd = "get";
        request.args = "";
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].StoreValue, request, this, (result) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].C(c, m, result.value);
        });
    }
    static SetObject(key, value, c, m) {
        let request = new _Model_Data__WEBPACK_IMPORTED_MODULE_2__["StoreValueReq"]();
        request.name = key;
        request.cmd = "set";
        request.args = JSON.stringify(value);
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].StoreValue, request, this, (result) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].C(c, m, result.value === true);
        });
    }
    static SetString(key, value, c, m) {
        let request = new _Model_Data__WEBPACK_IMPORTED_MODULE_2__["StoreValueReq"]();
        request.name = key;
        request.cmd = "set";
        request.args = value;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].StoreValue, request, this, (result) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].C(c, m, result.value === true);
        });
    }
    static Size(c, m) {
        let request = new _Model_Data__WEBPACK_IMPORTED_MODULE_2__["StoreValueReq"]();
        request.cmd = "size";
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].StoreValue, request, this, (result) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].C(c, m, result.value === true);
        });
    }
    static DeleteObject(key, c, m) {
        let request = new _Model_Data__WEBPACK_IMPORTED_MODULE_2__["StoreValueReq"]();
        request.name = key;
        request.cmd = "del";
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].StoreValue, request, this, (result) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].C(c, m, result.value === true);
        });
    }
    static StoreValue(storeInfo, c, m) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].StoreValue, storeInfo, this, (result) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].C(c, m, result);
        });
    }
}


/***/ }),

/***/ "./sdk/Extensions/HeartBeatSystem.ts":
/*!*******************************************!*\
  !*** ./sdk/Extensions/HeartBeatSystem.ts ***!
  \*******************************************/
/*! exports provided: HeartBeatSystem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeartBeatSystem", function() { return HeartBeatSystem; });
/* harmony import */ var _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../fw/Utility/TimerUtility */ "./fw/Utility/TimerUtility.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");


class HeartBeatSystem {
    constructor() {
        this._UploadTimeTimerId = "_UploadTimeTimerId";
        this._timeSpace = 3000;
        this._timeChange = false;
    }
    Init() {
    }
    static Me() {
        return HeartBeatSystem._Me;
    }
    /**
     * 同步在线时长定时器-启动
     */
    StartUploadTime() {
        this.StopUploadTimer();
        _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_0__["default"].AddTimer(this._UploadTimeTimerId, this._timeSpace, this, null, () => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().Invoke(10012);
            if (this._timeChange) {
                this._timeChange = false;
                this.StartUploadTime();
            }
            return false;
        });
    }
    /**
     * 同步云控定时时长数据(需要自定义配置中配置ydhw_duration参数，参数值为数字型)
     * @param times 间隔时间(秒)需要>1
     */
    SetTimeSpace(times) {
        if (times && times >= 1) {
            this._timeSpace = times * 1000;
            this._timeChange = true;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("SetTimeSpace-times:", times);
        }
    }
    /**
     * 同步在线时长定时器-停止
     */
    StopUploadTimer() {
        _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_0__["default"].RemoveTimer(this._UploadTimeTimerId);
    }
}
HeartBeatSystem._Me = new HeartBeatSystem();


/***/ }),

/***/ "./sdk/Extensions/PowerSystem.ts":
/*!***************************************!*\
  !*** ./sdk/Extensions/PowerSystem.ts ***!
  \***************************************/
/*! exports provided: PowerInfo, PowerSystem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PowerInfo", function() { return PowerInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PowerSystem", function() { return PowerSystem; });
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../fw/Utility/TimerUtility */ "./fw/Utility/TimerUtility.ts");



class PowerInfo {
}
class PowerSystem {
    constructor() {
        this._AutoRecoveryTimerId = "PowerAutoRecoveryTimerId";
        /**
         * 体力变化监听-上下文
         * _CallerPowerChanged
         */
        this._CPC = null;
        /**
         * 体力变化监听-回调函数
         * _ListenOnPowerChanged
         */
        this._LOPC = null;
        /**
         * 是否正在倒计时
         * _OnCountDown
         */
        this._OCD = false;
        /**
         * 倒计时时间进度
         * _CountDownTime
         */
        this._CDT = 0;
        /**
         * 当前体力值
         * _CurrentPowerNum
         */
        this._CPN = 0;
    }
    Init() {
        if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().PSysCfg || _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().PSysCfg.IsOn === false) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("Power System is off");
            return;
        }
        let nowTime = new Date().getTime();
        this._CPN = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Me.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].TPCounter, -1);
        let lastTime = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Me.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].LPAddTstamp, nowTime);
        this._CDT = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].CDPro, 0);
        this._OCD = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].OnCD, false);
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("total power:" + this._CPN);
        let upperLimit = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().PSysCfg.UpperLimit;
        let recoveryTime = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().PSysCfg.AutoRecoveryTime;
        let totalPower = (this._CPN < 0) ? _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().PSysCfg.DefaultPowerValue : this._CPN;
        if (totalPower < upperLimit) {
            //离线时间恢复体力
            if (nowTime !== lastTime) {
                let timeSpace = Math.floor((nowTime - lastTime) / 1000);
                timeSpace = timeSpace < 0 ? 0 : timeSpace;
                let getPowerNum = Math.floor(timeSpace / recoveryTime);
                totalPower += getPowerNum;
                totalPower = (totalPower > upperLimit) ? upperLimit : totalPower;
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L(`timeSpace=${timeSpace}, getPowerNum=${getPowerNum},totalPower=${totalPower}`);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Me.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].LPAddTstamp, new Date().getTime());
            }
        }
        this.SetPower(totalPower, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["P_R_T"].None);
    }
    SetPower(powerNum, type) {
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().PSysCfg.IsOn === false) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("PowerSystem is off");
            return;
        }
        let total = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Me.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].TPCounter, 0);
        let lastPowerNum = total;
        //体力上限
        let upperLimit = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().PSysCfg.UpperLimit;
        let recoveryTime = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().PSysCfg.AutoRecoveryTime;
        this._CPN = (powerNum > upperLimit) ? upperLimit : ((powerNum < 0) ? 0 : powerNum);
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].TPCounter, this._CPN);
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].LPAddTstamp, new Date().getTime());
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("setPower-powerUpper:" + upperLimit + ",recoveryTime:" + recoveryTime + ",_CurrentPowerNum:" + this._CPN);
        if (this._CPN < upperLimit) {
            this._CDT = this._CDT == 0 ? recoveryTime : this._CDT;
            _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_2__["default"].AddTimer(this._AutoRecoveryTimerId, 1000, this, null, () => {
                return this._AddPowerAuto();
            });
            this._OCD = true;
        }
        else {
            _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_2__["default"].RemoveTimer(this._AutoRecoveryTimerId);
            this._OCD = false;
            this._CDT = 0;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].CDPro, this._CDT);
        }
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].OnCD, this._OCD);
        let IsHasChanged = (lastPowerNum != this._CPN);
        if (this._CPC && this._LOPC && IsHasChanged) {
            let powerInfo = new PowerInfo();
            powerInfo.Type = type;
            powerInfo.PowerNum = this._CPN;
            powerInfo.CountDownPro = this._CDT;
            powerInfo.OnCountDown = this._OCD;
            this._CPC && this._LOPC && this._LOPC.call(this._CPC, powerInfo);
        }
    }
    _AddPowerAuto() {
        this._CDT -= 1;
        let total = this._CPN;
        if (this._CDT <= 0) {
            total += 1;
            let upperLimit = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().PSysCfg.UpperLimit;
            total = (total > upperLimit) ? upperLimit : ((total < 0) ? 0 : total);
            this.SetPower(total, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["P_R_T"].AutoRecovery);
            if (total === upperLimit) {
                return true;
            }
            else {
                return false;
            }
        }
        else {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].CDPro, this._CDT);
            let powerInfo = new PowerInfo();
            powerInfo.Type = _SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["P_R_T"].CountDown;
            powerInfo.PowerNum = total;
            powerInfo.CountDownPro = this._CDT;
            powerInfo.OnCountDown = this._OCD;
            this._CPC && this._LOPC && this._LOPC.call(this._CPC, powerInfo);
        }
    }
    GetPower() {
        let power = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SV"].TPCounter, 0);
        return power;
    }
    LOnPC(c, m) {
        this._CPC = c;
        this._LOPC = m;
    }
}


/***/ }),

/***/ "./sdk/Extensions/StatisticSystem.ts":
/*!*******************************************!*\
  !*** ./sdk/Extensions/StatisticSystem.ts ***!
  \*******************************************/
/*! exports provided: StatSystem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatSystem", function() { return StatSystem; });
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _Model_Default__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Model/Default */ "./sdk/Model/Default.ts");
/* harmony import */ var _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../fw/Utility/TimerUtility */ "./fw/Utility/TimerUtility.ts");




class StatSystem {
    constructor() {
        this._StatTimerId = "_StatTimerId";
        /**
         * 统计信息列表
         */
        this.lStatIf = [];
        /**
         * 是否开启了定时器
         */
        this._tSta = false;
    }
    Initialize() { }
    static Me() {
        return StatSystem._Me;
    }
    RQAd(type, adId, sType, rqType, stName) {
        if (!this.checkStatAdInfo(stName, type, adId))
            return;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].STAC().Invoke(rqType, type, adId, this, (isOk) => {
            if (!isOk) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].L(stName + " fail type:" + type);
                this.lStatIf.unshift({ type: type, adId: adId, sType: sType });
            }
        });
    }
    /**
     * Banner广告
     * @param type 0:创建,1:加载成功,2:展示,3:点击,4:加载失败
     * @param adId 广告id
     */
    StatBanner(type, adId) {
        if (!this.checkStatAdInfo("StatBanner", type, adId))
            return;
        this.lStatIf.push({ type: type, adId: adId, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Banner });
        this.startT();
    }
    /**
     * 激励视频
     * @param type
     * @param adId
     */
    StatVideo(type, adId) {
        if (!this.checkStatAdInfo("StatVideo", type, adId))
            return;
        this.lStatIf.push({ type: type, adId: adId, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Video });
        this.startT();
    }
    /**
     * 插屏广告
     * @param type
     * @param adId
     */
    StatInterstitial(type, adId) {
        if (!this.checkStatAdInfo("StatInterstitial", type, adId))
            return;
        this.lStatIf.push({ type: type, adId: adId, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Interstitial });
        this.startT();
    }
    /**
     * 格子广告
     * @param type
     * @param adId
     */
    StatGrid(type, adId) {
        if (!this.checkStatAdInfo("StatGrid", type, adId))
            return;
        this.lStatIf.push({ type: type, adId: adId, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Grid });
        this.startT();
    }
    /**
     * 盒子广告
     * @param type
     * @param adId
     */
    StatAppBox(type, adId) {
        if (!this.checkStatAdInfo("StatAppBox", type, adId))
            return;
        this.lStatIf.push({ type: type, adId: adId, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].AppBox });
        this.startT();
    }
    /**
     * 积木广告
     * @param type
     * @param adId
     */
    StatBlock(type, adId) {
        if (!this.checkStatAdInfo("StatBlock", type, adId))
            return;
        this.lStatIf.push({ type: type, adId: adId, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Block });
        this.startT();
    }
    /**
     * 原生广告
     * @param type
     * @param adId
     */
    StatNativeAd(type, adId) {
        if (!this.checkStatAdInfo("StatNativeAd", type, adId))
            return;
        this.lStatIf.push({ type: type, adId: adId, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Native });
        this.startT();
    }
    /**
     * 结束统计
     * @param details 对象或数组
     */
    StatResult(details) {
        if (details == null) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().Warn("StatResult details is null");
            return;
        }
        this.lStatIf.push({ details: details, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Result });
        this.startT();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].API().Invoke(10010);
    }
    RQResult(details) {
        if (details == null) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().Warn("StatResult details is null");
            return;
        }
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].STAC().Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].Result, details, this, (isOk) => {
            if (!isOk) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].L("StatResult fail");
                this.lStatIf.unshift({ details: details, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Result });
            }
        });
    }
    /**
     * 事件统计
     * @param event
     * @param scene
     */
    StatEvent(event, scene) {
        if (event == null) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().Warn("StatEvent event is null");
            return;
        }
        let time = new Date().getTime();
        this.lStatIf.push({ event: event, scene: scene, time: time, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Event });
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().SwitchEvent === false && scene !== 'LayerOn') {
            //关卡('LayerOn')进入次数统计事件不受事件开关控制
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].L("StatEvent is stop by SwitchEvent");
            return;
        }
        this.startT();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].API().Invoke(10011);
    }
    RQEvent(event, scene, time) {
        if (event == null || scene == null || time == null) {
            return;
        }
        let request = new _Model_Default__WEBPACK_IMPORTED_MODULE_2__["StatisticEventReq"]();
        request.event = event;
        request.scene = scene;
        request.time = time;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].STAC().Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].Event, [request], this, (isOk) => {
            if (!isOk) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].L("StatEvent fail");
                this.lStatIf.unshift({ event: event, scene: scene, time: time, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Event });
            }
        });
    }
    /**
     * 卖量统计
     * @param request
     */
    StatClickOut(request) {
        let str = "StatClickOut request";
        if (!request) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].L(str + " is null");
            return;
        }
        if (!request.iconId) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].L(str + ".iconId is null");
            return;
        }
        if (!request.action) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].L(str + ".action is null");
            return;
        }
        this.lStatIf.push({ request: request, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].ClickOut });
        this.startT();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].API().Invoke(10013);
    }
    RQClickOut(request) {
        if (request == null) {
            return;
        }
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].STAC().Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].ClickOut, request, this, (isOk) => {
            if (!isOk) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].L("StatClickOut fail");
                this.lStatIf.unshift({ request: request, sType: _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].ClickOut });
            }
        });
    }
    /**
     * 检测参数是否存在
     * @param type
     * @param adId
     */
    checkStatAdInfo(name, type, adId) {
        if (type == null) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().Warn(name + " type is null");
            return false;
        }
        if (!adId || adId == "") {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().Warn(name + " adId is null");
            return false;
        }
        return true;
    }
    /**
     * 启动定时器
     */
    startT() {
        if (!this._tSta && !this.noData()) {
            _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_3__["default"].RemoveTimer(this._StatTimerId);
            let me = this;
            _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_3__["default"].AddTimer(this._StatTimerId, 500, this, null, () => {
                if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().OnLine) {
                    if (me.lStatIf && me.lStatIf.length > 0) {
                        let requestInfo = me.lStatIf.shift();
                        if (requestInfo && requestInfo.sType) {
                            me.RQStat(requestInfo);
                        }
                    }
                }
                return this.noData();
            });
            this._tSta = true;
        }
    }
    RQStat(requestInfo) {
        switch (requestInfo.sType) {
            case _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Banner:
                this.RQAd(requestInfo.type, requestInfo.adId, requestInfo.sType, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].Banner, "StatBanner");
                break;
            case _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Video:
                this.RQAd(requestInfo.type, requestInfo.adId, requestInfo.sType, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].Video, "StatVideo");
                break;
            case _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Interstitial:
                this.RQAd(requestInfo.type, requestInfo.adId, requestInfo.sType, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].InterstitialAd, "StatInterstitial");
                break;
            case _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Grid:
                this.RQAd(requestInfo.type, requestInfo.adId, requestInfo.sType, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].GridAd, "StatGrid");
                break;
            case _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Block:
                this.RQAd(requestInfo.type, requestInfo.adId, requestInfo.sType, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].BlockAd, "StatBlock");
                break;
            case _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].AppBox:
                this.RQAd(requestInfo.type, requestInfo.adId, requestInfo.sType, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].AppBoxAd, "StatAppBox");
                break;
            case _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Native:
                this.RQAd(requestInfo.type, requestInfo.adId, requestInfo.sType, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["NET_API"].NativeAd, "StatNativeAd");
                break;
            case _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Event:
                this.RQEvent(requestInfo.event, requestInfo.scene, requestInfo.time);
                break;
            case _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].Result:
                this.RQResult(requestInfo.details);
                break;
            case _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["ST_S_TYPE"].ClickOut:
                this.RQClickOut(requestInfo.request);
                break;
        }
    }
    noData() {
        if (this.lStatIf && this.lStatIf.length != 0)
            return false;
        this._tSta = false;
        return true;
    }
}
StatSystem._Me = new StatSystem();


/***/ }),

/***/ "./sdk/Extensions/Strategy.ts":
/*!************************************!*\
  !*** ./sdk/Extensions/Strategy.ts ***!
  \************************************/
/*! exports provided: Strategy */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Strategy", function() { return Strategy; });
/* harmony import */ var _Model_User__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Model/User */ "./sdk/Model/User.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _Model_Default__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Model/Default */ "./sdk/Model/Default.ts");
/* harmony import */ var _fw_Utility_RandomUtility__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../fw/Utility/RandomUtility */ "./fw/Utility/RandomUtility.ts");





class Strategy {
    constructor(moduleList, moduleFalseList) {
        this._ShareSceneConfig = {};
        this._ShareSceneNames = [];
        this.ModuleFalseList = [];
        this.ModuleList = [];
        this.CurrentShareVideoTypes = [];
        if (moduleList.length > 0) {
            this.ModuleList = [];
            moduleList.forEach((element) => {
                let newModule = new _Model_User__WEBPACK_IMPORTED_MODULE_0__["ShareVideoModule"]();
                newModule._id = element._id;
                newModule.channel = element.channel;
                newModule.version = element.version;
                newModule.module = element.module;
                newModule.type = element.type;
                newModule.videoNum = element.videoNum;
                newModule.shareNum = element.shareNum;
                newModule.remarks = element.remarks;
                newModule.strategy = element.strategy;
                newModule.loopIndex = 0;
                newModule.logic = element.logicJson && element.logicJson.length > 0 ? JSON.parse(element.logicJson) : null;
                this.ModuleList.push(newModule);
            });
        }
        if (moduleFalseList.length > 0) {
            this.ModuleFalseList = [];
            moduleFalseList.forEach((element) => {
                let newModule = new _Model_User__WEBPACK_IMPORTED_MODULE_0__["ShareVideoModuleFalse"]();
                newModule._id = element._id;
                newModule.strategyId = 0;
                newModule.timeId = 0;
                newModule.count = 0;
                newModule.strategies = element.strategyjson.length > 0 ? JSON.parse(element.strategyjson) : null;
                newModule.tips = element.trickJson.length > 0 ? JSON.parse(element.trickJson) : null;
                this.ModuleFalseList.push(newModule);
            });
        }
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Me.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SV"].SConfig, this._ShareSceneConfig);
    }
    SSVd(channel, module, c, m) {
        //先查看是否有缓存记录
        let unuseStrategy = this.GUUS(channel, module, false);
        if (unuseStrategy) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, m, unuseStrategy.type);
            return;
        }
        if (this.ModuleList && this.ModuleList.length == 0) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, m, 0);
            return;
        }
        let bIsHasModule = false; //默认无策略
        this.ModuleList.every((element) => {
            if (element.channel === channel && element.module === module) {
                if (element.logic) {
                    let unUse = new _Model_User__WEBPACK_IMPORTED_MODULE_0__["UnUseShareVideoModule"]();
                    unUse.channel = element.channel;
                    unUse.module = element.module;
                    bIsHasModule = true;
                    if (element.logic.pe && element.logic.pe.length > 0) {
                        let type = element.logic.pe.shift();
                        unUse.type = type;
                        this.CurrentShareVideoTypes.push(unUse);
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, m, type);
                        return false;
                    }
                    else if (element.logic.loop && element.logic.loop.length > 0 && element.logic.time && element.logic.time > 0) {
                        let type = element.logic.loop[element.loopIndex];
                        element.loopIndex += 1;
                        if (element.loopIndex >= element.logic.loop.length) {
                            element.loopIndex = 0;
                            element.logic.time -= 1;
                        }
                        unUse.type = type;
                        this.CurrentShareVideoTypes.push(unUse);
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, m, type);
                        return false;
                    }
                }
            }
            return true;
        });
        if (bIsHasModule === false) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, m, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SHARE_T"].None);
        }
    }
    //获取未使用的策略
    /**
     *
     * @param channel
     * @param module
     * @param use
     * getUnUseStrategy
     */
    GUUS(channel, module, use) {
        //判断是否存在策略未试用
        if (this.CurrentShareVideoTypes.length > 0) {
            for (let i = 0; i < this.CurrentShareVideoTypes.length; i++) {
                let item = this.CurrentShareVideoTypes[i];
                if (item.channel == channel && item.module == module) {
                    let result = item;
                    if (use) {
                        result = this.CurrentShareVideoTypes.splice(i, 1)[0];
                    }
                    return item;
                }
            }
        }
        return null;
    }
    /**
     * 使用视频分享策略
     * @param channel
     * @param module
     * @param c
     * @param m
     */
    USVSty(channel, module, c, m) {
        let unuseStrategy = this.GUUS(channel, module, true);
        if (unuseStrategy) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, m, unuseStrategy.type);
            return;
        }
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, m, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SHARE_T"].None);
    }
    /**
     *
     * @param scene
     * @param configs
     */
    ISConf(scene, configs) {
        let me = this;
        if (scene !== "") {
            this._ShareSceneConfig[scene] = configs;
            return;
        }
        configs.forEach(element => {
            if (me._ShareSceneConfig[element.scene]) {
                let configList = me._ShareSceneConfig[element.scene];
                let isHasItem = false;
                for (let i = 0, l = configList.length; i < l; i++) {
                    let tmpConfig = configList[i];
                    if (tmpConfig.id == element.id) {
                        isHasItem = true;
                        break;
                    }
                }
                if (!isHasItem) {
                    configList.push(element);
                }
                me._ShareSceneConfig[element.scene] = configList;
            }
            else {
                let tmpList = [];
                tmpList.push(element);
                me._ShareSceneConfig[element.scene] = tmpList;
            }
            if (me._ShareSceneNames.indexOf(element.scene) == -1)
                me._ShareSceneNames.push(element.scene);
        });
    }
    /**
     * 分享列表中是否存在该场景
     * @param scene
     */
    HasSScene(scene) {
        let tmpList = this._ShareSceneConfig[scene];
        if (tmpList) {
            return true;
        }
        else {
            return false;
        }
    }
    GetSSConf(scene) {
        return this._ShareSceneConfig[scene];
    }
    /**
     * 随机获取一个分享信息
     */
    GetRSConf() {
        if (this._ShareSceneNames && this._ShareSceneNames.length > 0) {
            var sceneIndex = Math.floor(Math.random() * this._ShareSceneNames.length);
            let scene = this._ShareSceneNames[sceneIndex];
            if (this._ShareSceneConfig[scene]) {
                let shareConfig = this._ShareSceneConfig[scene];
                if (shareConfig && shareConfig.length > 0) {
                    let index = Math.floor(Math.random() * shareConfig.length);
                    let item = shareConfig[index];
                    // SDK.L("YDHW ---GetRandomShareConfig:"+JSON.stringify(item));
                    return item;
                }
            }
        }
        return null;
    }
    // GetStrategy(channel:string, module:string,)
    GetShareStrategy(channel, module) {
        let shareStrategy = null;
        if (this.ModuleFalseList && this.ModuleFalseList.length > 0) {
            if (channel && module && this.ModuleList && this.ModuleList.length > 0) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("getSharingStrategy-channel,module:", channel, module);
                this.ModuleList.every((element) => {
                    if (element.type && element.channel && element.module && element.channel == channel && element.module == module) {
                        this.ModuleFalseList.forEach((mElement) => {
                            if (mElement._id == Number(element.type)) {
                                shareStrategy = mElement;
                                return false;
                            }
                        });
                    }
                    return true;
                });
            }
        }
        return shareStrategy;
    }
    GetSRlt(shareAppInfo, c, m) {
        let shareSuccess = false;
        let shareBackInfo = new _Model_Default__WEBPACK_IMPORTED_MODULE_3__["ShareBackInfo"]();
        var curTime = new Date().getTime();
        let time_space = curTime - shareAppInfo.showTime;
        let sharingStrategy = this.GetShareStrategy(shareAppInfo.channel, shareAppInfo.module); //获取策略
        if (sharingStrategy) {
            let strategyIndex = sharingStrategy.strategyId;
            let strategy = sharingStrategy.strategies[strategyIndex];
            let timeId = sharingStrategy.timeId;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("当前策略:", JSON.stringify(strategy));
            if (strategy.time && timeId < strategy.time.length) {
                let time_item = strategy.time[timeId];
                let item_prob = strategy.prob[timeId];
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("time:" + JSON.stringify(time_item) + " , prob:" + JSON.stringify(item_prob) + ",time_space:", time_space);
                if (time_item && time_item.length > 1) {
                    for (let i = 1; i < time_item.length; i++) {
                        if (time_space >= time_item[i - 1] && time_space <= time_item[i]) {
                            let prob = item_prob[i - 1];
                            shareSuccess = _fw_Utility_RandomUtility__WEBPACK_IMPORTED_MODULE_4__["RandomUtility"].Probability(prob);
                        }
                        else if (i == time_item.length - 1 && time_space > time_item[i]) {
                            let prob = item_prob[i];
                            shareSuccess = _fw_Utility_RandomUtility__WEBPACK_IMPORTED_MODULE_4__["RandomUtility"].Probability(prob);
                        }
                    }
                }
            }
            timeId += 1;
            //
            if (shareSuccess) {
                sharingStrategy.timeId = 0;
                sharingStrategy.count += 1;
                shareBackInfo.IsSuccess = true;
            }
            else {
                if (timeId >= strategy.time.length) {
                    sharingStrategy.timeId = 0;
                    sharingStrategy.count += 1;
                }
                else {
                    sharingStrategy.timeId = timeId;
                }
                shareBackInfo.IsSuccess = false;
                shareBackInfo.IsHasStrategy = true;
                shareBackInfo.Tips = sharingStrategy.tips;
            }
            if (sharingStrategy.count >= strategy.num) {
                //寻找下一组
                sharingStrategy.count = 0;
                sharingStrategy.strategyId = this._GetNextStrategy(sharingStrategy, strategyIndex);
            }
            this._SetSharingStategy(sharingStrategy);
        }
        else {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("真分享(time_space > 3000)?:" + time_space);
            if (time_space > 3000) {
                shareSuccess = true;
                shareBackInfo.IsSuccess = true;
            }
            else {
                shareBackInfo.IsSuccess = false;
                shareBackInfo.IsHasStrategy = false;
            }
        }
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].C(c, m, shareBackInfo);
    }
    _GetNextStrategy(mFalseList, strategyId) {
        let nextStrategyId = strategyId + 1;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("nextStrategyId:" + nextStrategyId);
        if (nextStrategyId >= mFalseList.strategies.length) {
            nextStrategyId = 0;
        }
        let strategy = mFalseList.strategies[nextStrategyId];
        if (strategy.num == 0) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("num:" + strategy.num);
            nextStrategyId = this._GetNextStrategy(mFalseList, nextStrategyId);
        }
        return nextStrategyId;
    }
    //改变分享策略数据
    _SetSharingStategy(sStrategy) {
        for (let i = 0; i < this.ModuleFalseList.length; i++) {
            let element = this.ModuleFalseList[i];
            if (element._id && sStrategy._id) {
                this.ModuleFalseList[i] = sStrategy;
                break;
            }
        }
    }
    GetDMT(customNumber) {
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().SwitchTouch === false)
            return false;
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().DevAcntCfg.IsOn && _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().DevAcntCfg.Accounts.length > 0) {
            //白名单开关打开，判断account是否在白名单中
            let accountList = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().DevAcntCfg.Accounts;
            for (let i = 0; i < accountList.length; i++) {
                if (accountList[i] == _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().AccountId + "") {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L(`In whiteList:${accountList[i]}`);
                    return true;
                }
            }
        }
        ;
        if (this._IsHasMistouchTimer()) {
            return this._IsDepthShield(customNumber);
        }
        return false;
    }
    _IsHasMistouchTimer() {
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().MisTSwCfg) {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().MisTSwCfg.IsOn)
                return true;
            let mistouchTimer = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().MisTSwCfg;
            let startTimeHour = mistouchTimer.StartTimeHour || 0;
            let startTimeMinute = mistouchTimer.StartTimeMinute || 0;
            let endTimeHour = mistouchTimer.EndTimeHour || 23;
            let endTimeMinute = mistouchTimer.EndTimeMinute || 59;
            //当前时间
            let nowDate = new Date();
            let misNThours = nowDate.getHours();
            let misNTminutes = nowDate.getMinutes();
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L(`checkMistouchTimer- ${startTimeHour}:${startTimeMinute} > ${misNThours}:${misNTminutes} < ${endTimeHour}:${endTimeMinute}`);
            let inTime = !((misNThours < startTimeHour || (misNThours == startTimeHour && misNTminutes <= startTimeMinute)) || (misNThours > endTimeHour || (misNThours == endTimeHour && misNTminutes >= endTimeMinute)));
            return !inTime;
        }
        return false;
    }
    _IsDepthShield(customNumber) {
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().DeepSRCfg) {
            let description = '_IsDepthShield-';
            let depthShield = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().DeepSRCfg;
            let totalCustoms = depthShield.CustomNumberCounter || 0;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L(description + "depthShield.Type:" + depthShield.Type);
            //0:关闭,1:按对局数,2:按关卡数
            if (depthShield.Type == 0) {
                return !this._IsGoodUser(depthShield);
            }
            else if (depthShield.Type == 1) {
                let TotalCustomNumberCounter = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Me.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SV"].TCNCounter, 0);
                let TodayCustomNumberCounter = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Me.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SV"].TdCNCounter, 0);
                //按对局数
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L(`TotalCustomNumberCounter > totalCustoms: ${TotalCustomNumberCounter} > ${depthShield.CustomNumberCounter}`);
                if (!depthShield.CustomNumberCounter || TotalCustomNumberCounter > depthShield.CustomNumberCounter) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L(`toalByTodayNum > dayCustoms: ${TodayCustomNumberCounter} > ${depthShield.DayCustomNumber[0]}`);
                    if (!depthShield.CustomNumberCounter || TodayCustomNumberCounter > depthShield.DayCustomNumber[0]) {
                        return !this._IsGoodUser(depthShield);
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            else if (depthShield.Type == 2) {
                //按关卡数
                //玩家传入的关卡数 customNum == totalCustoms 或者  totalCustoms 开始每间隔 depthShield.dayCustoms[0]关
                if (customNumber || customNumber === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L(`customNum >= totalCustoms: ${customNumber} >= ${totalCustoms}`);
                    if (customNumber >= totalCustoms) {
                        if (!depthShield.DayCustomNumber || depthShield.DayCustomNumber.length == 0) {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("depthShield.dayCustoms is null or empty");
                            return false;
                        }
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L(`${customNumber} == ${totalCustoms} || ((${customNumber} > ${totalCustoms}) && ((${customNumber} - ${totalCustoms}) % (${depthShield.DayCustomNumber[0]} + 1)) == 0)`);
                        if ((customNumber == totalCustoms) || ((customNumber > totalCustoms) && ((customNumber - totalCustoms) % (depthShield.DayCustomNumber[0] + 1)) == 0)) {
                            return !this._IsGoodUser(depthShield);
                        }
                    }
                }
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("customNum is " + customNumber);
                return false;
            }
        }
    }
    //检测优质用户
    _IsGoodUser(depthShield) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("_IsGoodUser:" + depthShield.ExecellentUserSwitch);
        let isGoods = true;
        if (depthShield.ExecellentUserSwitch) {
            if (depthShield.WatchVideoCounter || depthShield.WatchVideoCounter === 0) {
                let TodayWatchVideoCounter = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Me.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SV"].TdWVCounter);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L(`isGoodUser--${TodayWatchVideoCounter} > ${depthShield.WatchVideoCounter}`);
                isGoods = (TodayWatchVideoCounter > depthShield.WatchVideoCounter);
            }
        }
        return isGoods;
    }
    IsUVd(index) {
        let unlock = false;
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().VdULCfg) {
            let customNumber = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().VdULCfg.CustomNumber || 0;
            let intervalCount = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().VdULCfg.IntervalCount || 0;
            if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].CME().VdULCfg.IsOn) {
                if ((index == customNumber) || ((index > customNumber) && ((index - customNumber) % (intervalCount + 1)) == 0)) {
                    //判断是否有看视频解锁过该关卡
                    let unlockLevelNumberList = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Me.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SV"].LUlVLNumber, []);
                    if (unlockLevelNumberList && unlockLevelNumberList.length > 0) {
                        unlockLevelNumberList.forEach(element => {
                            if (index == element) {
                                unlock = true;
                            }
                        });
                    }
                }
                else {
                    unlock = true;
                }
            }
            else {
                unlock = true;
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L("视频解锁关卡开关-关闭,开启所有关卡");
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].L(`IsUnlockVideo-index:${index} customNumber:${customNumber} intervalCount=${intervalCount} unlock=${unlock}`);
        }
        return unlock;
    }
}


/***/ }),

/***/ "./sdk/Extensions/YDHWCache.ts":
/*!*************************************!*\
  !*** ./sdk/Extensions/YDHWCache.ts ***!
  \*************************************/
/*! exports provided: YDHWCache */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YDHWCache", function() { return YDHWCache; });
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _Model_Default__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Model/Default */ "./sdk/Model/Default.ts");



class YDHWCache {
    constructor() {
        this.JumpOutInfo = null;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Me.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["SV"].JOInfo, new _Model_Default__WEBPACK_IMPORTED_MODULE_2__["JumpOutInfo"]());
    }
    RemoveItemFrom(appid) {
        let jumpOutInfo = _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Me.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["SV"].JOInfo, null);
        //点击并确认跳转某个icon后，当日（可调整）隐藏对应appid的所有icon（开启内部标识的icon不受此功能影响）
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().ListBoxConfig && _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().ListBoxConfig.length > 0 && _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().SideBoxCount < _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().ListBoxConfig.length) {
            for (let i = 0; i < _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().ListBoxConfig.length; i++) {
                let item = _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().ListBoxConfig[i];
                if (item.toAppid === appid && item.innerStatus == 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].L("隐藏对应appid的所有icon--id:" + item._id);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].CME().ListBoxConfig.splice(i, 1);
                    let hasItem = false;
                    for (let j = 0; j < jumpOutInfo.List.length; j++) {
                        if (jumpOutInfo.List[j].appid == appid) {
                            hasItem = true;
                        }
                    }
                    if (!hasItem) {
                        jumpOutInfo.List.push(new _Model_Default__WEBPACK_IMPORTED_MODULE_2__["AppInfo"](appid));
                    }
                    jumpOutInfo.Date = new Date().getTime();
                }
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Me.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["SV"].JOInfo, jumpOutInfo);
        }
    }
}


/***/ }),

/***/ "./sdk/Mgr/CommerceMgr.ts":
/*!********************************!*\
  !*** ./sdk/Mgr/CommerceMgr.ts ***!
  \********************************/
/*! exports provided: EditRequest, ApiMgr, StatMgr, AgentMgr, DataMgr, CommerceMgr */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EditRequest", function() { return EditRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiMgr", function() { return ApiMgr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatMgr", function() { return StatMgr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgentMgr", function() { return AgentMgr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataMgr", function() { return DataMgr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommerceMgr", function() { return CommerceMgr; });
/* harmony import */ var _fw_Manager_CommerceManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../fw/Manager/CommerceManager */ "./fw/Manager/CommerceManager.ts");
/* harmony import */ var _fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../fw/Assist/StorageApt */ "./fw/Assist/StorageApt.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _Model_User__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Model/User */ "./sdk/Model/User.ts");
/* harmony import */ var _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../fw/Utility/Assist */ "./fw/Utility/Assist.ts");
/* harmony import */ var _fw_Utility_Url__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../fw/Utility/Url */ "./fw/Utility/Url.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _Model_Data__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Model/Data */ "./sdk/Model/Data.ts");
/* harmony import */ var _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Config/SDKConfig */ "./sdk/Config/SDKConfig.ts");
/* harmony import */ var _Model_Default__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../Model/Default */ "./sdk/Model/Default.ts");
/* harmony import */ var _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../fw/Utility/TimerUtility */ "./fw/Utility/TimerUtility.ts");
/* harmony import */ var _Extensions_Strategy__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../Extensions/Strategy */ "./sdk/Extensions/Strategy.ts");
/* harmony import */ var _Model_Statistic__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../Model/Statistic */ "./sdk/Model/Statistic.ts");
/* harmony import */ var _Extensions_YDHWCache__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../Extensions/YDHWCache */ "./sdk/Extensions/YDHWCache.ts");
/* harmony import */ var _Extensions_PowerSystem__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../Extensions/PowerSystem */ "./sdk/Extensions/PowerSystem.ts");
/* harmony import */ var _Extensions_CloudAPI__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ../Extensions/CloudAPI */ "./sdk/Extensions/CloudAPI.ts");
/* harmony import */ var _Extensions_HeartBeatSystem__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ../Extensions/HeartBeatSystem */ "./sdk/Extensions/HeartBeatSystem.ts");
/* harmony import */ var _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ../Extensions/StatisticSystem */ "./sdk/Extensions/StatisticSystem.ts");
/* harmony import */ var _Assist_Log__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ../Assist/Log */ "./sdk/Assist/Log.ts");
/* harmony import */ var _fw_Manager_ApiManager__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ../../fw/Manager/ApiManager */ "./fw/Manager/ApiManager.ts");
/* harmony import */ var _fw_Manager_NetManager__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ../../fw/Manager/NetManager */ "./fw/Manager/NetManager.ts");
/* harmony import */ var _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ../../fw/Enums/PLAT */ "./fw/Enums/PLAT.ts");
























const ER = "Error=";
const EX = "Exception=";
const SS = "server say:";
const CD = "code:";
const SP = "Response=";
class EditRequest {
}
/**
 * API调用统计管理器
 */
class ApiMgr extends _fw_Manager_ApiManager__WEBPACK_IMPORTED_MODULE_19__["ApiManager"] {
    constructor() {
        super(...arguments);
        this._ApiStatisticUploadTimeId = "_ApiStatisticUploadTimeId";
        this.pendingMsgList = [];
        this._CurrentCallIndex = 1;
        this._CounterList = {};
        this.categoryStatistics = "statistics";
    }
    Init() {
        super.Init();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].PACList, {}).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].BatchID, 0).Store();
        this._UrApiFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("接口调用统计-强制")
            .C(this.categoryStatistics)
            .R("sdk")
            .A("final")
            .End();
        this._UrlApi = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("接口调用统计")
            .C(this.categoryStatistics)
            .R("sdk")
            .End();
        this.DNfy(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SE"].OnFrontend, this, () => {
            this.getLocalApiCountList();
        });
        this.DNfy(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SE"].OnBackend, this, () => {
            if (this.pendingMsgList.length == 0) {
                return;
            }
            this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].PACList, this.pendingMsgList);
            this.pendingMsgList = [];
        });
        // SDK.STAC().DM(NET_API.Statistic_Api, this, (apiType: number, platformType: EM_PLATFORM_TYPE): void => {
        //     // SDK.L("Stat_Api()-info:", JSON.stringify(sdksInfo))
        // });
        _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].AddTimer(this._ApiStatisticUploadTimeId, 5000, this, null, () => {
            return this.UploadStatisticSdk(); //return [false:多次触发,true:单次触发定时器]
        });
        //YDHW_API
        for (let i = 10001; i <= 10085; i++) {
            this.DeclareMethod(i);
        }
        //YDHW_WECHAT_API
        for (let i = 20001; i <= 20025; i++) {
            this.DeclareMethod(i);
        }
        //YDHW_QQ_API
        for (let i = 30001; i <= 30013; i++) {
            this.DeclareMethod(i);
        }
        //YDHW_TT_API
        for (let i = 40001; i <= 40010; i++) {
            this.DeclareMethod(i);
        }
        //YDHW_VIVO_API
        for (let i = 50001; i <= 50001; i++) {
            this.DeclareMethod(i);
        }
        //YDHW_MZ_API       
        for (let i = 140001; i <= 140002; i++) {
            this.DeclareMethod(i);
        }
    }
    DeclareMethod(temp) {
        this.DM(temp, this, (_platform) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchSdk)
                return;
            let batchId = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].BatchID);
            let time = new Date().getTime();
            // SDK.L("YDHW ---ApiMgr-monthd:" + tmpEnum + ",platform:" + _platform+",_CurrentCallIndex:"+this._CurrentCallIndex);
            if (this._CounterList[temp]) {
                this._CounterList[temp]++;
            }
            else {
                this._CounterList[temp] = 1;
            }
            let apiInfo = new _Model_Statistic__WEBPACK_IMPORTED_MODULE_12__["SdkInfo"]();
            apiInfo.index = this._CurrentCallIndex;
            this._CurrentCallIndex++;
            apiInfo.count = this._CounterList[temp];
            apiInfo.name = String(temp);
            apiInfo.platform = this.getPlatformName(_platform);
            apiInfo.time = time;
            apiInfo.wholesaleId = batchId;
            this.pendingMsgList.push(apiInfo);
        });
    }
    /**
     * 上传统计数据
     */
    UploadStatisticSdk() {
        if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().OnLine) {
            return false;
        }
        if (this.pendingMsgList.length == 0) {
            return false;
        }
        this._UrApiFinal.AsPost()
            .Post({
            appid: _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().AppId,
            accountId: _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().AccountId,
            infos: this.pendingMsgList
        }).OnReceive(this, (response) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Sdk " + SP, response);
            if (response.code === 0) {
                this.pendingMsgList = [];
            }
            else {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Debug && (_Assist_Log__WEBPACK_IMPORTED_MODULE_18__["Log"].Error(SS + response.info), _Assist_Log__WEBPACK_IMPORTED_MODULE_18__["Log"].Error(CD + response.code));
            }
        }).OnError(this, (error) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
        }).OnException(this, (data) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
        }).Send();
        return false;
    }
    /**
     * 获取平台类型对应的名称
     *
     * @param type 平台类型
     */
    getPlatformName(type) {
        let platformName = "";
        for (var key in _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_21__["PLAT"]) {
            let value = _fw_Enums_PLAT__WEBPACK_IMPORTED_MODULE_21__["PLAT"][key];
            if (type == value) {
                platformName = "" + key;
            }
            // SDK.L("YDHW ------getPlatformName-key:"+key+",value:"+value);
        }
        return platformName;
    }
    /**
     * 读取缓存数据
     */
    Restore() {
        super.Restore();
        this.getLocalApiCountList();
        this.AVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].BatchID, 1);
        this._CurrentCallIndex = 1;
    }
    /**
     * 获取Api统计本地缓存
     */
    getLocalApiCountList() {
        let tmpList = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].PACList, []);
        if (tmpList.length > 0) {
            this.pendingMsgList = tmpList;
        }
    }
}
/**
 * 统计上报管理器
 */
class StatMgr extends _fw_Manager_NetManager__WEBPACK_IMPORTED_MODULE_20__["NetManager"] {
    constructor() {
        super(...arguments);
        this.categoryStat = "statistics";
    }
    Init() {
        this._UrlBanner = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("Banner统计")
            .C(this.categoryStat)
            .R("banner")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Banner, this, (type, adId, c, m) => {
            this._UrlBanner.AsGet()
                .Param("type", type)
                .Param("adId", adId)
                .End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Banner " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlClickCount = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("卖量统计")
            .C(this.categoryStat)
            .R("clickcount")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].ClickOut, this, (request, c, m) => {
            this._UrlClickCount.AsPost()
                .Post(request).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E("ClickOut", response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlClientLog = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("客户端错误日志")
            .C(this.categoryStat)
            .R("clientlog")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].ClientLog, this, (request, c, m) => {
            try {
                this._UrlClientLog.AsPost()
                    .Post(request).OnReceive(this, (response) => {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("ClientLog " + SP, response);
                    if (response.code === 0) {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                    }
                    else {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                    }
                }).OnError(this, (error) => {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
                }).OnException(this, (data) => {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
                }).Send();
            }
            catch (e) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W(e);
            }
        });
        this._UrlDuration = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("时长统计")
            .C(this.categoryStat)
            .R("duration")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Duration, this, (type, duration, c, m) => {
            this._UrlDuration.AsGet()
                .Param("type", type)
                .Param("duration", duration)
                .End()
                .OnReceive(this, (response) => {
                // SDK.L("Duration "+SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlEvent = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("事件统计")
            .C(this.categoryStat)
            .R("event")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Event, this, (events, c, m) => {
            this._UrlEvent.AsPost()
                .Post({ events: events }).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Event " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlInterstitialAd = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("插屏广告统计")
            .C(this.categoryStat)
            .R("interstitial")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].InterstitialAd, this, (type, adId, c, m) => {
            this._UrlInterstitialAd.AsGet()
                .Param("type", type)
                .Param("adId", adId)
                .End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Event " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlGridAd = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("格子广告")
            .C(this.categoryStat)
            .R("grid")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].GridAd, this, (type, adId, c, m) => {
            this._UrlGridAd.AsGet()
                .Param("type", type)
                .Param("adId", adId)
                .End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Event " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlLayer = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("流失统计")
            .C(this.categoryStat)
            .R("layer")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Stat_Layer, this, (layer, c, m) => {
            this._UrlLayer.AsPost()
                .Post({ layer: layer }).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Event " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlResult = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("结果统计")
            .C(this.categoryStat)
            .R("result")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Result, this, (_detail, c, m) => {
            this._UrlResult.AsPost()
                .Post({ detail: _detail }).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Result " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlShareCard = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("分享图统计")
            .C(this.categoryStat)
            .R("sharecard")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Stat_ShareCard, this, (info, c, m) => {
            this._UrlShareCard.AsPost()
                .Post(info).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("ShareCard " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlVideo = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("视频统计")
            .C(this.categoryStat)
            .R("video")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Video, this, (type, adId, c, m) => {
            this._UrlVideo.AsPost()
                .Post({ type: type, adId: adId }).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Video " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlAppBoxAd = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("盒子广告统计")
            .C(this.categoryStat)
            .R("box")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].AppBoxAd, this, (type, adId, c, m) => {
            this._UrlAppBoxAd.AsGet()
                .Param("type", type)
                .Param("adId", adId)
                .End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("AppBox " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlBlockAd = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("积木广告统计")
            .C(this.categoryStat)
            .R("block")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].BlockAd, this, (type, adId, c, m) => {
            this._UrlBlockAd.AsGet()
                .Param("type", type)
                .Param("adId", adId)
                .End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Block " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlNativeAd = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("原生广告统计")
            .C(this.categoryStat)
            .R("native")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].NativeAd, this, (type, adId, c, m) => {
            this._UrlNativeAd.AsGet()
                .Param("type", type)
                .Param("adId", adId)
                .End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Native " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
    }
}
class AgentMgr extends _fw_Manager_NetManager__WEBPACK_IMPORTED_MODULE_20__["NetManager"] {
    Init() {
        super.Init();
        this._UrlEdit = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("用户信息更新")
            .C("user")
            .R("info")
            .A("edit")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Edit, this, (request, c, m) => {
            this._UrlEdit.AsPost().Post(request).OnReceive(this, (response) => {
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlMyInfo = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("服务端信息")
            .C("user")
            .R("my")
            .A("info")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].MyInfo, this, (c, m) => {
            this._UrlMyInfo.AsGet().OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("MyInfo " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Debug && (_Assist_Log__WEBPACK_IMPORTED_MODULE_18__["Log"].Error(SS + response.info), _Assist_Log__WEBPACK_IMPORTED_MODULE_18__["Log"].Error(CD + response.code));
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this._UrlLogin = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("用户登录")
            .C("user")
            .R("login")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Me.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Login, this, (engineType, engineVer, request, c, m, onFailed) => {
            this._UrlLogin.Head(3, engineType).Head(4, engineVer);
            this._UrlLogin.AsPost().Post(request).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Login " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Debug && (_Assist_Log__WEBPACK_IMPORTED_MODULE_18__["Log"].Error(SS + response.info), _Assist_Log__WEBPACK_IMPORTED_MODULE_18__["Log"].Error(CD + response.code));
                    c && onFailed && onFailed.call(c);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this.Debug("Edit Url=" + this._UrlEdit.CheckValue());
        this.Debug("MyInfo Url=" + this._UrlMyInfo.CheckValue());
        this.Debug("Login Url=" + this._UrlLogin.CheckValue());
    }
}
class DataMgr extends _fw_Manager_NetManager__WEBPACK_IMPORTED_MODULE_20__["NetManager"] {
    Init() {
        this.UrlConfig = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("获取游戏配置列表")
            .C("gamebase")
            .R("config")
            .End();
        this.UrlConfigFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("强制获取游戏配置列表")
            .C("gamebase")
            .R("config")
            .A("final")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Config, this, (c, m) => {
            this.UrlConfig.AsGet().End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Config " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    this.UrlConfigFinal.AsGet()
                        .Param("appid", _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().AppId)
                        .End()
                        .OnReceive(this, (response) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("ConfigForce " + SP, response);
                        if (response.code === 0) {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                        }
                        else {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, null);
                        }
                    }).OnError(this, (error) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
                    }).OnException(this, (data) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
                    }).Send();
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this.UrlCustomConfig = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("获取自定义配置列表")
            .C("gamebase")
            .R("customconfig")
            .End();
        this.UrlCustomConfigFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("强制获取自定义配置列表")
            .C("gamebase")
            .R("customconfig")
            .A("final")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].CustomConfig, this, (c, m) => {
            this.UrlCustomConfig.AsGet().End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("CustomConfig " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    this.UrlCustomConfigFinal.AsGet()
                        .Param("appid", _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().AppId)
                        .Param("version", _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().Version)
                        .End()
                        .OnReceive(this, (response) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("CustomConfigForce " + SP, response);
                        if (response.code === 0) {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                        }
                        else {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, null);
                        }
                    }).OnError(this, (error) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
                    }).OnException(this, (data) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
                    }).Send();
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this.UrlDataDecode = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("获取平台加密数据")
            .C("gamebase")
            .R("data")
            .A("decode")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].DataDecode, this, (request, c, m) => {
            this.UrlDataDecode.AsPost().Post(request)
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("DataDecode " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, null);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this.UrlGetBoardAward = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("领取积分墙奖励")
            .C("gamebase")
            .R("getboardaward")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].GetBoardAward, this, (module, awardId, c, m) => {
            this.UrlGetBoardAward.AsPost().Post({ module: module, awardId: awardId })
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("GetBoardAward " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, null);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this.UrlLayer = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("获取游戏流失路径列表")
            .C("gamebase")
            .R("layer")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Data_Layer, this, (c, m) => {
            this.UrlLayer.AsGet().End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("Layer " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, null);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this.UrlScoreBoard = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            .AsPost()
            // .Des("获取积分墙列表")
            .C("gamebase")
            .R("scoreboard")
            .End();
        this.UrlScoreBoardFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("强制获取积分墙列表")
            .C("gamebase")
            .R("scoreboard")
            .A("final")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].ScoreBoard, this, (module, c, m) => {
            this.UrlScoreBoard.AsPost().Post({ module: module })
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("ScoreBoard " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    this.UrlScoreBoardFinal.AsGet()
                        .Param("appid", _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().AppId)
                        .Param("version", _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().Version)
                        .Param("module", module)
                        .End()
                        .OnReceive(this, (response) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("ScoreBoard " + SP, response);
                        if (response.code === 0) {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                        }
                        else {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, null);
                        }
                    }).OnError(this, (error) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
                    }).OnException(this, (data) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
                    }).Send();
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this.UrlSideBox = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("获取侧边栏列表")
            .C("gamebase")
            .R("sidebox")
            .End();
        this.UrlSideBoxFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("强制获取侧边栏列表")
            .C("gamebase")
            .R("sidebox")
            .A("final")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].SideBox, this, (c, m) => {
            this.UrlSideBox.AsGet().End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("SideBox " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    this.UrlSideBoxFinal.AsGet()
                        .Param("appi", _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().AppId)
                        .Param("version", _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().Version).End()
                        .OnReceive(this, (response) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("SideBoxForce " + SP, response);
                        if (response.code === 0) {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                        }
                        else {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, null);
                        }
                    }).OnError(this, (error) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
                    }).OnException(this, (data) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
                    }).Send();
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this.UrlShareCard = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("获取分享图列表")
            .C("gamebase")
            .R("sharecard")
            .End();
        this.UrlShareCardFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 2)
            // .Des("强制获取分享图列表")
            .C("gamebase")
            .R("sharecard")
            .A("final")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Data_ShareCard, this, (scene, c, m) => {
            this.UrlShareCard.AsPost().Post({ scene: scene })
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("ShareCard " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    this.UrlShareCardFinal.AsGet()
                        .Param("appid", _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().AppId)
                        .Param("scene", scene)
                        .End()
                        .OnReceive(this, (response) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("ShareCardFinal " + SP, response);
                        if (response.code === 0) {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                        }
                        else {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, null);
                        }
                    }).OnError(this, (error) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
                    }).OnException(this, (data) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
                    }).Send();
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this.UrlStoreValue = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Domain().Clone()
            .Head(1, 1)
            // .Des("获取自定义空间值")
            .C("gamebase")
            .R("store")
            .A("value")
            .End();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].StoreValue, this, (request, c, m) => {
            this.UrlStoreValue.AsPost().Post(request)
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("StoreValue " + SP, response);
                if (response.code === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E1(response);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, null);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).Send();
        });
        this.UrlGetMyIp = new _fw_Utility_Url__WEBPACK_IMPORTED_MODULE_5__["Url"]();
        // "http://pv.sohu.com/cityjson"
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Me.DM(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].GetMyIp, this, (c, m) => {
            console.log("YDHW --N-GetMyIp--1");
            this.UrlGetMyIp.AsGet().SetCustomUrl("http://pv.sohu.com/cityjson")
                .Head(1, 1)
                .OnReceive(this, (response) => {
                    console.log("YDHW --N-GetMyIp--OnReceive");
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("UrlGetMyIp " + SP, response);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, response);
            }).OnError(this, (error) => {
                console.log("YDHW --N-GetMyIp--OnError");
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(ER, error);
            }).OnException(this, (data) => {
                console.log("YDHW --N-GetMyIp--OnException");
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].E(EX, data);
            }).SendCustomUrl();
        });
    }
}
class CommerceMgr extends _fw_Manager_CommerceManager__WEBPACK_IMPORTED_MODULE_0__["CommerceManager"] {
    constructor() {
        //内部不爆露的
        // protected _IsMisTouchBannerAd: boolean = false;
        super(...arguments);
        this.Config = null;
        this._P = null;
        this.Strategy = null;
        this.Cache = null;
        this.PowerSystem = null;
        this.SDK_VERSION = "3.1.0";
        this.AppId = "";
        this.AppKey = "";
        this.PkgName = "";
        this.Version = "";
        this.SceneId = 0;
        this.SwitchTouch = false;
        this.SwitchPush = false;
        this.SwitchLog = false;
        this.SwitchLogin = true;
        this.SwitchJump = true;
        this.SwitchShare = true;
        this.SwitchVideo = true;
        this.SwitchEvent = true;
        this.SwitchLayer = true;
        this.SwitchResult = false;
        this.IsLowVersion = false;
        this.IsRealVersion = false;
        this.SwitchClient = true;
        this.SwitchDuration = true;
        this.SwitchBanner = true;
        this.SwitchBlock = true;
        this.SwitchBox = true;
        this.SwitchCustom = true;
        this.SwitchGrid = true;
        this.SwitchInterstitial = true;
        this.SwitchNative = true;
        this.SwitchSdk = true;
        this.OnLine = false; //是否在线状态
        this.VideoFalseLimit = 0;
        this.ShareFalseLimit = 0;
        this.LastLoginTime = 0;
        // ModuleFalseList: ShareVideoModuleFalse[] = [];
        // ModuleList: ShareVideoModule[] = [];
        this.ListCustomConfig = null;
        this.ListBoxConfig = null;
        this.ListLayer = null;
        this.BannerAdUnitIdList = null; //侧边栏广告
        this.InterstitialAdUnitIdList = null; //插屏广告
        this.SpreadAdUnitIdList = null; //开屏广告
        this.NativeAdUnitIdList = null; //原生广告
        this.VideoAdUnitIdList = null; //视频广告
        /**
         * Banner定时刷新配置信息
         * BannerRefreshCfg
         */
        this.BnRefCfg = null;
        /**
         *体力系统信息
         * PowerSystemCfg
         */
        this.PSysCfg = null;
        /**
         * 视频解锁关卡信息
         * VideoUnlockLevelCfg
         */
        this.VdULCfg = null;
        /**
         * 深度误触屏蔽规则信息
         * DeepSheildRuleCfg
         */
        this.DeepSRCfg = null;
        /**
         * 定时开关误触信息
         * MisTouchSwitchCfg
         */
        this.MisTSwCfg = null;
        /**
         * 开发人员白名单信息
         * DeveloperAccountCfg
         */
        this.DevAcntCfg = null;
        //BannerAd
        // _BannerAd: any = null;
        this._LastTimeRefreshBannerAd = 0;
        this._BannerAdDelayTimerId = "";
        this._IsSmallBannerAd = false;
        //RewarVideoAd
        this._UnlockCustomNumber = 0;
        this._CallerRewardVideoAd = null;
        this._EvtOnCloseRewardVideoAd = null;
        this._IsAddPower = false;
        this._IsRewardVideoAdFinish = false;
        //InterstitialAd
        /**
         * 插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
         */
        this.IntttAdFirstShowWT = 0; //InterstitialAdFirstShowWaitTime
        /**
         * 插屏广告-两次展示之间时间间隔（秒）
         */
        this.IntttAdShowTimeI = 0; //InterstitialAdShowTimeInterval
        /**
         * 最后一次创建插屏广告的时间戳
         */
        this._LCIntttAd = 0; //_LastTimeCreateInterstitialAd
        //ShareCard
        this._CurrentShareScene = "";
        this._CallerShareCardBack = null;
        this._EvtOnShareCardBack = null;
        this._ShareAppInfo = null;
        this.SideBoxCount = 20;
        this._LastPlayTime = new Date().getTime();
        this._showShare = false;
        this._showVideoAd = false;
        this._showInterstitialAd = false;
        this._intttAdWaitShow = false;
        this._needShowBannerAd = false; //需要展示Banner 
        this._isShowBannerAd = false; //已经展示Banner
        this._showMoreGameView = false;
        this._localLayerList = null;
        //屏幕适配(魅族)
        this._ResolutionWidth = 0;
        this._resolutionHeight = 0;
        this._ScaleType = 0;
        this._hasHide = false; //是否回到后台
        this._onShow = null;
        this._onHide = null;
        this._onError = null;
    }
    _InnerLogin(c, m) {
        this._P.InitEngine();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("CommerceMgr InnerLogin()");
        if (!this.SwitchLogin) {
            console.error("当前游戏已经被禁止登录，请联系SDK官方 !");
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
            return;
        }
        this._LCIntttAd = new Date().getTime();
        // 获取网络状态
        this._P.GetNetworkType(this, (netType) => {
            this._P.NetType = netType;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("GetNetworkType = ", netType);
        });
        let request = new _Model_User__WEBPACK_IMPORTED_MODULE_3__["LoginRequest"]();
        request.platform = this._P.PfType + "";
        request.appid = this.AppId;
        request.version = this.Version;
        request.code = this.Code;
        request.pkgName = this.PkgName;
        let shareInfo = this.Invoke(10001);
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("shareInfo=", shareInfo);
        // SDK.W("YDHW -----shareInfo:" + JSON.stringify(shareInfo));
        request.shareInfo = shareInfo;
        let clientInfo = new _Model_User__WEBPACK_IMPORTED_MODULE_3__["ClientInfo"]();
        clientInfo.uuid = "";
        clientInfo.platform = this._P.PfType + "";
        clientInfo.brand = this._P.Brand;
        clientInfo.version = this.SDK_VERSION;
        clientInfo.model = this._P.Model;
        clientInfo.appName = this._P.AppName;
        clientInfo.resolution = this._P.Resolution;
        let engineType = "";
        let engineVer = "";
        try {
            if (this._P.IsLayaEngine) {
                engineType = "laya";
                engineVer = Laya.version;
            }
            else if (this._P.IsCocosEngine) {
                engineType = "cocos";
                if (window['cc'] && window['cc'].ENGINE_VERSION) {
                    engineVer = window['cc'].ENGINE_VERSION; //window['cc'].ENGINE_VERSION.split(".")[0];
                }
            }
        }
        catch (e) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("get platform info error:" + e);
        }
        request.clientInfo = clientInfo;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Me.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Login, engineType, engineVer, request, this, (result) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("login result:" + JSON.stringify(result));
            this.OnLine = true;
            this.AccountId = result.accountId;
            this.NickName = result.nickName || "";
            this.AvatarUrl = result.avatarUrl || "";
            this.IsNewPlayer = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(result.newPlayer);
            this.OpenID = result.openid || "";
            this.AccountPass = result.accountPass || "";
            this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].APYDHW, { account_pass: this.AccountPass, save_time: new Date().getTime() });
            _fw_Utility_Url__WEBPACK_IMPORTED_MODULE_5__["Url"].SetAccountPass(this.AccountPass);
            if (this.IsNewPlayer === true) {
                //todo:something
            }
            this.SwitchTouch = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(result.switchTouch) || false;
            this.SwitchPush = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(result.switchPush) || false;
            this.SwitchLog = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(result.switchLog) || false;
            this.SwitchLogin = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchLogin, "switchLogin") || false;
            this.SwitchJump = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchJump, "switchJump") || false;
            this.SwitchShare = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchShare, "switchShare") || false;
            this.SwitchVideo = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchVideo, "switchVideo") || false;
            this.SwitchEvent = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchEvent, "switchEvent") || false;
            this.SwitchLayer = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchLayer, "switchLayer") || false;
            this.SwitchResult = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchResult, "switchResult") || false;
            this.IsLowVersion = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(result.isLowVersion) || false;
            this.IsRealVersion = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(result.isRealVersion) || false;
            this.SwitchClient = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchClient, "switchClient") || false;
            this.SwitchDuration = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchDuration, "switchDuration") || false;
            this.SwitchBanner = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchBanner, "switchBanner") || false;
            this.SwitchBlock = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchBlock, "switchBlock") || false;
            this.SwitchBox = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchBox, "switchBox") || false;
            this.SwitchCustom = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchCustom, "switchCustom") || false;
            this.SwitchGrid = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchGrid, "switchGrid") || false;
            this.SwitchInterstitial = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchInterstitial, "switchInterstitial") || false;
            this.SwitchNative = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchNative, "switchNative") || false;
            this.SwitchSdk = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToSB(result.switchSdk, "switchSdk") || false;
            this.VideoFalseLimit = result.videoFalseLimit || 0;
            this.ShareFalseLimit = result.shareFalseLimit || 0;
            let moduleFalseList = result.moduleFalseList || [];
            let moduleList = result.moduleList || [];
            this.Strategy = new _Extensions_Strategy__WEBPACK_IMPORTED_MODULE_11__["Strategy"](moduleList, moduleFalseList);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("SDK version:" + this.SDK_VERSION + ",Game version:" + this.Version);
            if (this.IsLowVersion)
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("SDK is low please update !");
            if (!this.IsRealVersion)
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].W("not found Game version by Server !");
            if (!this.SwitchLogin)
                console.error("当前游戏已经被禁止登录，请联系SDK官方 !");
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Debug = this.SwitchLog;
            this._P.IsDebug = this.SwitchLog;
            _fw_Utility_Url__WEBPACK_IMPORTED_MODULE_5__["Url"].SetAllDebugMode(this.SwitchLog);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Config, this, (result) => {
                result.forEach((element) => {
                    let tmpList = JSON.parse(element.value);
                    if (element.code == "b_t_s") { //banner定时刷新配置
                        this.BnRefCfg = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["BannerRefreshCfg"]();
                        this.BnRefCfg.IsOn = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(tmpList[0]);
                        this.BnRefCfg.Interval = tmpList[1] || 10;
                        this.BnRefCfg.IsForceRefresh = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(tmpList[2]);
                        this.BnRefCfg.MinimumInterval = tmpList[3] || 3;
                    }
                    else if (element.code == "power") { //体力系统
                        this.PSysCfg = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["PowerSystemCfg"]();
                        this.PSysCfg.DefaultPowerValue = tmpList[0] || 5;
                        this.PSysCfg.UpperLimit = tmpList[1] || 5;
                        this.PSysCfg.AutoRecoveryTime = tmpList[2] || 300;
                        this.PSysCfg.VideoPowerWeight = tmpList[3] || 1;
                        this.PSysCfg.IsOn = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(tmpList[4]);
                    }
                    else if (element.code == "v_u_c") { //视频解锁关卡
                        this.VdULCfg = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["VideoUnlockLevelCfg"]();
                        this.VdULCfg.IsOn = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(tmpList[0]);
                        this.VdULCfg.CustomNumber = tmpList[1] || 2;
                        this.VdULCfg.IntervalCount = tmpList[2] || 0;
                    }
                    else if (element.code == "dp_s") { //深度屏蔽开关
                        this.DeepSRCfg = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["DeepSheildRuleCfg"]();
                        this.DeepSRCfg.Type = tmpList[0] || 0;
                        this.DeepSRCfg.CustomNumberCounter = tmpList[1] || 5;
                        this.DeepSRCfg.ExecellentUserSwitch = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(tmpList[2]);
                        this.DeepSRCfg.WatchVideoCounter = tmpList[3] || 2;
                        this.DeepSRCfg.DayCustomNumber = tmpList[4] || 0;
                    }
                    else if (element.code == "m_t") { //定时器开关误触
                        this.MisTSwCfg = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["MisTouchSwitchCfg"]();
                        this.MisTSwCfg.IsOn = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(tmpList[0]);
                        this.MisTSwCfg.StartTimeHour = tmpList[1] || 0;
                        this.MisTSwCfg.StartTimeMinute = tmpList[2] || 0;
                        this.MisTSwCfg.EndTimeHour = tmpList[3] || 23;
                        this.MisTSwCfg.EndTimeMinute = tmpList[4] || 0;
                    }
                    else if (element.code == "d_a") { //开发者账号（白名单
                        this.DevAcntCfg = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["DeveloperAccountCfg"]();
                        this.DevAcntCfg.IsOn = _fw_Utility_Assist__WEBPACK_IMPORTED_MODULE_4__["Assist"].ToB(tmpList[0]);
                        for (let i = 1, l = tmpList.length; i < l; i++) {
                            this.DevAcntCfg.Accounts.push(tmpList[i]);
                        }
                    }
                });
                this.PowerSystem.Init();
                c && m.call(c, true);
            });
            this.Invoke(10018, this, (result) => { }); //获取自定义配置
            this.Invoke(10012);
            _Extensions_HeartBeatSystem__WEBPACK_IMPORTED_MODULE_16__["HeartBeatSystem"].Me().StartUploadTime();
        }, () => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
        });
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("SDKPlatform InnerLogin() end");
    }
    Init() {
        super.Init();
        this.RefreshLastPlayTime();
        this.Name = "YDHW-SDK:" + this.Name;
        this.Cache = new _Extensions_YDHWCache__WEBPACK_IMPORTED_MODULE_13__["YDHWCache"]();
        this.PowerSystem = new _Extensions_PowerSystem__WEBPACK_IMPORTED_MODULE_14__["PowerSystem"]();
        this.Config = window[_fw_Assist_StorageApt__WEBPACK_IMPORTED_MODULE_1__["StorageApt"].SDK_CONFIG];
        this.AppId = String(this.Config.appid);
        this.AppKey = String(this.Config.appkey);
        this.PkgName = String(this.Config.pkg_name);
        this.Version = String(this.Config.version);
        this.SwitchLog = this._P.IsDebug;
        this.BannerAdUnitIdList = this.Config.banner_ad_unit_id_list;
        this.IntttAdFirstShowWT = Number(this.Config.interstitialAd_first_show_wait_time) || 0;
        this.IntttAdShowTimeI = Number(this.Config.interstitialAd_show_time_interval) || 0;
        this.SideBoxCount = Number(this.Config.side_box_count) || 0;
        if (!this.BannerAdUnitIdList || typeof this.BannerAdUnitIdList == "undefined") {
            this._P.IsDebug && console.error("YDHW_CONFIG.banner_ad_unit_id_list not exists");
        }
        this.InterstitialAdUnitIdList = this.Config.interstitial_ad_unit_id_list;
        if (!this.InterstitialAdUnitIdList || typeof this.InterstitialAdUnitIdList == "undefined") {
            this._P.IsDebug && console.error("YDHW_CONFIG.interstitial_ad_unit_id_list not exists");
        }
        this.SpreadAdUnitIdList = this.Config.spread_ad_unit_id_list;
        if (!this.SpreadAdUnitIdList || typeof this.SpreadAdUnitIdList == "undefined") {
            this._P.IsDebug && console.error("YDHW_CONFIG.spread_ad_unit_id_list not exists");
        }
        this.NativeAdUnitIdList = this.Config.native_ad_unit_id_list;
        if (!this.NativeAdUnitIdList || typeof this.NativeAdUnitIdList == "undefined") {
            this._P.IsDebug && console.error("YDHW_CONFIG.native_ad_unit_id_list not exists");
        }
        this.VideoAdUnitIdList = this.Config.video_ad_unit_id_list;
        if (!this.VideoAdUnitIdList || typeof this.VideoAdUnitIdList == "undefined") {
            this._P.IsDebug && console.error("YDHW_CONFIG.video_ad_unit_id_list not exists");
        }
        this._ResolutionWidth = Number(this.Config.resolution_width) || 0;
        this._resolutionHeight = Number(this.Config.resolution_height) || 0;
        this._ScaleType = Number(this.Config.scale_type) || 0;
        // SDK.L("YDHW ---屏幕适配-_ResolutionWidth:"+this._ResolutionWidth);
        // SDK.L("YDHW ---屏幕适配-_resolutionHeight:"+this._resolutionHeight);
        // SDK.L("YDHW ---屏幕适配-_ScaleType:"+this._ScaleType);
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].APYDHW, null).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].TPCounter, -1).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LPAddTstamp, new Date().getTime()).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].TdWVCounter, 0).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LWhVTstamp, new Date().getTime()).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LUlVLNumber, []).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].MCNumber, 0).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LCNumber, 0).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].TdCNCounter, 0).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].TCNCounter, 0).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LPTstamp, new Date().getTime());
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LUlVLNumber, []).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].VFLimit, 0).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].SFLimit, 0).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].PfUInfo, null);
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LStttLayer, null).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].CDPro, 0).Store();
        this.DV(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].OnCD, false).Store();
        // this._Platform.OnError(this, (res:any) => { 
        //     if(this._onError){
        //         this._onError.c && this._onError.m && this._onError.m.call(this._onError.c,res);
        //     }
        //  });
        // this.DM(10001, this, (): string => {
        //     this.Invoke(API_LoginAddress);
        //     return "";
        // });
        // this.DM(10002, this, (): IYDHW.IAdStyle => {
        //     this.Invoke(API_GetLeftTopBtnPosition);
        //     return null;
        // });
        /**OnFrontend */
        this.DM(10069, this, (res) => {
            if (this._hasHide) {
                this.RefreshLastPlayTime();
                this._hasHide = false;
            }
            _Extensions_HeartBeatSystem__WEBPACK_IMPORTED_MODULE_16__["HeartBeatSystem"].Me().StartUploadTime();
            this._ShareAppInfo = null;
            this.InvokeNfy(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SE"].OnFrontend);
            if (this._onShow) {
                this._onShow.c && this._onShow.m && this._onShow.m.call(this._onShow.c, res);
            }
        });
        /**OnBackend */
        this.DM(10070, this, (res) => {
            if (this._P.IsSBnAd && !this._showInterstitialAd && !this._showVideoAd && !this._showShare) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("YDHW has click Banner AdId:" + this._P.BnAdId);
                this.Invoke(10002, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].CLICK, this._P.BnAdId);
            }
            _Extensions_HeartBeatSystem__WEBPACK_IMPORTED_MODULE_16__["HeartBeatSystem"].Me().StopUploadTimer();
            this._hasHide = true;
            this.Invoke(10012);
            this.InvokeNfy(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SE"].OnBackend);
            if (this._onHide) {
                this._onHide.c && this._onHide.m && this._onHide.m.call(this._onHide.c, res);
            }
        });
        //OnShow
        this.DM(10071, this, (c, m) => {
            this._onShow = { c, m };
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10071);
        });
        //OnHide
        this.DM(10072, this, (c, m) => {
            this._onHide = { c, m };
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10072);
        });
        //OnError
        this.DM(10073, this, (c, m) => {
            this._onError = { c, m };
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10073);
        });
        // /**ShareInfo */
        // this.DM(10001, this, (): IYDHW.User.IShareInfo => {
        //     // this.Invoke(API_ShareInfo);
        //     return null
        // });
        //StatisticBanner
        this.DM(10002, this, (type, adId) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchBanner)
                return;
            _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__["StatSystem"].Me().StatBanner(type, adId);
        });
        //StatisticVideo
        this.DM(10003, this, (type, adId) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchVideo)
                return;
            _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__["StatSystem"].Me().StatVideo(type, adId);
        });
        //StatisticInterstitial
        this.DM(10004, this, (type, adId) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchInterstitial)
                return;
            _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__["StatSystem"].Me().StatInterstitial(type, adId);
        });
        //StatisticGrid
        this.DM(10005, this, (type, adId) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchGrid)
                return;
            _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__["StatSystem"].Me().StatGrid(type, adId);
        });
        //StatisticAppBox
        this.DM(10006, this, (type, adId) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchBox)
                return;
            _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__["StatSystem"].Me().StatAppBox(type, adId);
        });
        //StatisticBlock
        this.DM(10007, this, (type, adId) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchBlock)
                return;
            _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__["StatSystem"].Me().StatBlock(type, adId);
        });
        //StatisticNativeAd
        this.DM(10008, this, (type, adId) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchNative)
                return;
            _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__["StatSystem"].Me().StatNativeAd(type, adId);
        });
        //StatisticErrorStack
        this.DM(10009, this, (ErrStack, LogStr, strDate) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchClient)
                return;
            try {
                let systemInfo = this._P.SysInfo;
                let request = new _Model_Statistic__WEBPACK_IMPORTED_MODULE_12__["ClientLogReq"]();
                request.ErrStack = ErrStack;
                request.LogStr = LogStr;
                if (systemInfo) {
                    request.appid = this.AppId;
                    request.version = this.Version;
                    request.language = systemInfo.language;
                    request.system = systemInfo.system;
                    request.model = systemInfo.model;
                    request.brand = systemInfo.brand;
                    request.platform = systemInfo.platform;
                    request.SDKVersion = this.SDK_VERSION;
                    request.resolution = systemInfo.screenWidth + "x" + systemInfo.screenHeight;
                    request.window = systemInfo.windowWidth + "x" + systemInfo.windowHeight;
                }
                request.addDate = strDate;
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].ClientLog, request, this, (isOk) => {
                    if (isOk === true) {
                        this.Log("StatErrorStack isOk:" + isOk);
                    }
                });
            }
            catch (err) {
                this.Warn("StatErrorStack-err:", err);
            }
        });
        //StatisticResult
        this.DM(10010, this, (details) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchResult)
                return;
            _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__["StatSystem"].Me().StatResult(details);
        });
        //StatisticEvent
        this.DM(10011, this, (event, scene) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchEvent)
                return;
            _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__["StatSystem"].Me().StatEvent(event, scene);
        });
        //StatisticDuration
        this.DM(10012, this, () => {
            if (!this.OnLine) {
                this.Warn("is not onLine");
                return;
            }
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchDuration)
                return;
            let nowTime = new Date().getTime();
            let duration = nowTime - this._LastPlayTime;
            let me = this;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Duration, 0, duration, this, (isOk) => {
                if (isOk === true) {
                    me._LastPlayTime = nowTime;
                    // this.Log("StatDuration success");
                }
            });
        });
        //StatisticClickOut
        this.DM(10013, this, (request) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchJump)
                return;
            _Extensions_StatisticSystem__WEBPACK_IMPORTED_MODULE_17__["StatSystem"].Me().StatClickOut(request);
        });
        //ShowShareOrVideo
        this.DM(10014, this, (channel, module, c, m) => {
            if (!channel) {
                this.Error("channel is null");
                return;
            }
            if (!module) {
                this.Error("module is null");
                return;
            }
            this.Strategy && this.Strategy.SSVd(channel, module, c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10014);
        });
        //UseShareOrVideoStrategy
        this.DM(10015, this, (channel, module, c, m) => {
            if (!channel) {
                this.Error("channel is null");
                return;
            }
            if (!module) {
                this.Error("module is null");
                return;
            }
            this.Strategy && this.Strategy.USVSty(channel, module, c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10015);
        });
        //SwitchView
        this.DM(10016, this, (isMisTouched) => {
            if (!this._P.BnAd && this.SwitchLog) {
                this.Warn("BannerAd is null");
            }
            else if (this.BnRefCfg && this.BnRefCfg.IsForceRefresh) {
                let nowTime = new Date().getTime();
                let lastTime = this._LastTimeRefreshBannerAd || nowTime;
                let interval = this.BnRefCfg.MinimumInterval || _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_8__["SDKCfg"].DIFRefreshBannerAd;
                if (nowTime - lastTime >= interval * 1000) {
                    // if (this._Platform.IsShowBannerAd && this._IsSmallBannerAd) {
                    //     this._CreateBannerAd(this._IsSmallBannerAd, this._Platform.IsMisTouchBannerAd, true, this._Platform.BannerStyle, this._Platform.OnResizeBannerAd, null, null);
                    //     this.CreateSmallBannerAd(isMisTouched, true, this, (): void => {
                    //     });
                    // } else if (this._Platform.IsShowBannerAd) {
                    //     this.CreateBannerAd(isMisTouched, true, this, (isOk: boolean): void => {
                    //     });
                    // } else {
                    //     this._LastTimeRefreshBannerAd = nowTime - (this.BannerRefreshConfig.MinimumInterval * 1000 + 1000);
                    // }
                    if (this._P.IsSBnAd) {
                        this._CreateBannerAd(this._IsSmallBannerAd, this._P.IsMTBnAd, true, this._P.BnStyle, this._P.OnRsBnAd, null, null);
                    }
                    else {
                        this._LastTimeRefreshBannerAd = nowTime - (this.BnRefCfg.MinimumInterval * 1000 + 1000);
                    }
                }
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10016);
        });
        //GetDeepTouchInfo
        this.DM(10017, this, (customNumber) => {
            let strategy = this.Strategy && this.Strategy.GetDMT(customNumber);
            this.Log("GetDeepTouchInfo:" + strategy + ",customNumber:" + customNumber);
            let deepTouchInfo = new _Model_Default__WEBPACK_IMPORTED_MODULE_9__["DeepTouchInfo"]();
            deepTouchInfo.deepTouch = strategy ? true : false;
            if (this.ListCustomConfig && this.ListCustomConfig.length > 0) {
                this.ListCustomConfig.forEach(element => {
                    if (!strategy) {
                        if (parseInt(element.type) == 1) {
                            element.value = "";
                        }
                        else if (parseInt(element.type) == 2) {
                            element.value = "0";
                        }
                    }
                    deepTouchInfo.ListCustomInfo.push(element);
                });
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10017);
            return deepTouchInfo;
        });
        //GetCustomConfig
        this.DM(10018, this, (c, m) => {
            if (this.ListCustomConfig) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, this.ListCustomConfig);
            }
            else {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].CustomConfig, this, (result) => {
                    this.ListCustomConfig = result;
                    //误触开关关闭且item的switchTouchUse == 1的情况下将type==1的值设为空串，type==2的值设为false
                    this.ListCustomConfig.forEach(element => {
                        if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchTouch) {
                            if (parseInt(element.switchTouchUse) == 1) {
                                if (parseInt(element.type) == 1) {
                                    element.value = "";
                                }
                                else if (parseInt(element.type) == 2) {
                                    element.value = "0";
                                }
                            }
                        }
                        //
                        //定时同步在线时长的定时器云控
                        if (element.name == 'ydhw_duration' && element.type == "1" && element.value) {
                            let time = 0;
                            try {
                                time = Number(element.value);
                            }
                            catch (e) {
                                this.Warn("GetCustomConfig-parse ydhw_duration-error", e);
                            }
                            _Extensions_HeartBeatSystem__WEBPACK_IMPORTED_MODULE_16__["HeartBeatSystem"].Me().SetTimeSpace(time);
                        }
                    });
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, this.ListCustomConfig);
                });
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10018);
        });
        //GameOver
        this.DM(10019, this, (index) => {
            this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LCNumber, index);
            let maxCustomNumber = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].MCNumber, 0);
            if (index > maxCustomNumber) {
                this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].MCNumber, index);
            }
            this.AVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].TdCNCounter, 1);
            this.AVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].TCNCounter, 1);
            this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LPTstamp, new Date().getTime());
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10019);
        });
        //IsUnlockVideo
        this.DM(10020, this, (index) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10020);
            return this.Strategy && this.Strategy.IsUVd(index);
        });
        //GetPowerInfo
        this.DM(10021, this, () => {
            if (!this.PSysCfg) {
                this.Error("GetPowerInfo error: PowerInfo is null");
                return null;
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10021);
            return this.PSysCfg;
        });
        //ListenOnPowerChange
        this.DM(10022, this, (c, m) => {
            this.PowerSystem.LOnPC(c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10022);
        });
        //SetPower
        this.DM(10023, this, (power, type) => {
            if (!this.PSysCfg || this.PSysCfg.IsOn === false) {
                this.Warn("Power System is Off");
                return;
            }
            this.PowerSystem && this.PowerSystem.SetPower(power, type);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10023);
        });
        //GetPower
        this.DM(10024, this, () => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10024);
            return this.PowerSystem && this.PowerSystem.GetPower();
        });
        //GetSideBox
        this.DM(10025, this, (c, m) => {
            if (this.ListBoxConfig) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, this.ListBoxConfig);
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].SideBox, this, (result) => {
                this.ListBoxConfig = result;
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, this.ListBoxConfig);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10025);
        });
        //GetScoreBoardList
        this.DM(10026, this, (c, m) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].ScoreBoard, "scoreboard", c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10026);
        });
        //GetScoreBoardAward
        this.DM(10027, this, (id, c, m) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].GetBoardAward, "scoreboard", id, c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10027);
        });
        //ShareCard
        this.DM(10028, this, (scene, c, m) => {
            this._CurrentShareScene = (scene !== "") ? scene : "default";
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Data_ShareCard, scene, this, (result) => {
                this.Strategy.ISConf(scene, result);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, result);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10028);
        });
        //GetTodayBoutCount
        this.DM(10029, this, () => {
            let counter = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].TdCNCounter, 0);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10029);
            return counter;
        });
        //GetTotalBoutCount
        this.DM(10030, this, () => {
            let counter = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].TCNCounter, 0);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10030);
            return counter;
        });
        //GetLastBountNumber
        this.DM(10031, this, () => {
            let index = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LCNumber, 0);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10031);
            return index;
        });
        //GetMaxBountNumber
        this.DM(10032, this, () => {
            let index = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].MCNumber, 0);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10032);
            return index;
        });
        //GetTodayWatchVideoCounter
        this.DM(10033, this, () => {
            let counter = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].TdWVCounter, 0);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10033);
            return counter;
        });
        //CreateBannerAd
        this.DM(10034, this, (isMisTouch, isShow, c, m) => {
            this._P.BnStyle = null;
            this._CreateBannerAd(false, isMisTouch, isShow, null, c, m, null);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10034);
        });
        //CreateSmallBannerAd
        this.DM(10035, this, (isMisTouch, isShow, c, m) => {
            this._P.BnStyle = null;
            this._CreateBannerAd(true, isMisTouch, isShow, null, c, m, null);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10035);
        });
        //CreateCustomBannerAd
        this.DM(10036, this, (isMisTouch, isShow, style, c, m, onResize) => {
            this._P.BnStyle = style;
            this._CreateBannerAd(false, isMisTouch, isShow, style, c, m, onResize);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10036);
        });
        //BannerAdChangeSize
        this.DM(10037, this, (style) => {
            this._P.ChangeBnAdStyle(style);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10037);
        });
        //ShowBannerAd
        this.DM(10038, this, () => {
            this.Log("ShowBannerAd");
            let nowTime = new Date().getTime();
            let last = this._LastTimeRefreshBannerAd || nowTime;
            let isSmall = false;
            let IsOn = false;
            let interval = 0;
            let create = false; //是否走创建banner流程
            if (this.BnRefCfg) {
                IsOn = this.BnRefCfg.IsOn || false;
                interval = this.BnRefCfg.Interval || _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_8__["SDKCfg"].DIFRefreshBannerAd;
            }
            if (this._P.BnAd) {
                if (IsOn) {
                    isSmall = this._IsSmallBannerAd || false;
                }
                let isRefresh = ((nowTime - last) >= _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_8__["SDKCfg"].DIFRefreshBannerAd * 1000);
                if (IsOn && isRefresh) {
                    //到时间定时更新banner
                    this.Log("ShowBannerAd CreateBannerAd");
                    create = true;
                    this._CreateBannerAd(isSmall, this._P.IsMTBnAd, true, this._P.BnStyle, this._P.OnRsBnAd, null, null);
                }
                if (!create) {
                    this._P.BnAdV(true);
                    this.Invoke(10002, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].SHOW, this._P.BnAdId);
                    this._needShowBannerAd = false;
                }
                this._LastTimeRefreshBannerAd = nowTime;
            }
            if (!create && IsOn) {
                this.Log(`将在${interval}秒后重新创建banner`);
                _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].RemoveDelayTimer(this._BannerAdDelayTimerId);
                this._BannerAdDelayTimerId = _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].Delay(interval * 1000, this, () => {
                    if (this._P.IsSBnAd)
                        this._CreateBannerAd(isSmall, this._P.IsMTBnAd, true, this._P.BnStyle, this._P.OnRsBnAd, null, null); //延迟刷新banner
                });
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10038);
        });
        //HideBannerAd
        this.DM(10039, this, () => {
            this.Log("HideBannerAd");
            this._P.BnAdV(false);
            this._needShowBannerAd = false;
            _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].RemoveDelayTimer(this._BannerAdDelayTimerId);
            this._BannerAdDelayTimerId = "";
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10039);
        });
        //ChangeBannerStyle
        this.DM(10040, this, (style) => {
            this._P.ChangeBnAdStyle(style);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10040);
        });
        //CreateRewardVideoAd
        this.DM(10041, this, () => {
            if (!this.VideoAdUnitIdList || this.VideoAdUnitIdList.length == 0) {
                this.Error("VideoAdUnitIdList is empty.");
            }
            else if (!this._P.VdAd) {
                let me = this;
                let index = Math.floor(Math.random() * this.VideoAdUnitIdList.length);
                let adId = this.VideoAdUnitIdList[index];
                this._P.CreateVdAd(adId, this, () => {
                    me._IsRewardVideoAdFinish = true;
                }, (result) => {
                    me._LCIntttAd = new Date().getTime();
                    if (result && result.isEnded || result === undefined) { //点击关闭按钮
                        me.Log("视频播放完成");
                        me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["VIDEO_P_T"].FINISH);
                        me._AddUnlockVideoLevelNumber(me._UnlockCustomNumber);
                        me.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].PLAY_FINISH, adId);
                        if (me._IsAddPower) {
                            me._AddPowerByVideo();
                        }
                    }
                    else { //播放中途退出
                        me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["VIDEO_P_T"].CANCEL);
                        me.Log("视频播放中途取消");
                        this.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].PLAY_CANCEL, adId);
                    }
                    me._UnlockCustomNumber = null;
                    me._EvtOnCloseRewardVideoAd = null;
                    me._showVideoAd = false;
                }, (error) => {
                    me.Log("RewardVideoAd onError:", error);
                    this.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].LOAD_FAIL, adId);
                    me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["VIDEO_P_T"].FAIL);
                    me._EvtOnCloseRewardVideoAd = null;
                    me._UnlockCustomNumber = null;
                    me._showVideoAd = false;
                    me._LCIntttAd = new Date().getTime();
                });
                this.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].CREATE, adId);
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10041);
        });
        //ShowRewardVideoAd
        this.DM(10042, this, (unlockCustomNumber, isAddPower, c, onClose) => {
            let me = this;
            this._IsRewardVideoAdFinish = false;
            this._UnlockCustomNumber = unlockCustomNumber;
            this._CallerRewardVideoAd = c;
            this._EvtOnCloseRewardVideoAd = onClose;
            this._IsAddPower = isAddPower || false;
            if (!this._P.VdAd) {
                this.Invoke(10041);
                if (!this._P.VdAd) {
                    return;
                }
            }
            this._P.ShowVdAd(this, () => {
                me.Log("视频播放成功");
                me._showVideoAd = true;
                me._IsRewardVideoAdFinish = true;
                me.Invoke(10003, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].SHOW, this._P.VdAdId);
            }, () => {
                me.Error("视频播放失败");
                me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["VIDEO_P_T"].FAIL);
                me._EvtOnCloseRewardVideoAd = null;
                me._showVideoAd = false;
                me._LCIntttAd = new Date().getTime();
            });
            console.log("YDHW ----ShowRewardVideoAd-3");
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10042);
        });
        //StatisticShareCard
        this.DM(10043, this, (info) => {
            this.Invoke(10044, info);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10043);
        });
        //StatisticShareCardInner
        this.DM(10044, this, (info) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchShare)
                return;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Stat_ShareCard, info, this, (isOk) => {
            });
        });
        //GetLayerList
        this.DM(10045, this, (onReturn) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Data_Layer, this, (layerList) => {
                this.ListLayer = layerList;
                //读取本地缓存的今日已统计过的流失路径列表                          
                let localData = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LStttLayer, null);
                if (localData) {
                    this._localLayerList = localData;
                }
                onReturn && onReturn(layerList);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10045);
        });
        //NavigateToMiniProgram
        this.DM(10046, this, (id, toAppId, toUrl, source, c, m) => {
            let layer = source ? source : "default";
            let request = new _Model_Statistic__WEBPACK_IMPORTED_MODULE_12__["ClickOutReq"]();
            request.iconId = id;
            request.target = toAppId;
            request.souce = layer;
            // SDK.L("YDHW ---NavigateToMiniProgram-iconId:" + id + ",toAppId:" + toAppId + ",toUrl:" + toUrl + ",source:" + source);
            let str = "小游戏跳转-跳转";
            this._P.NToMP(this.AppId, toAppId, toUrl, this, () => {
                this.Log(str + "成功！");
                request.action = "enable";
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].ClickOut, request, null);
                this.Cache.RemoveItemFrom(toAppId);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
            }, () => {
                this.Log(str + '失败！');
                request.action = "cancel";
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].ClickOut, request, null);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10046);
        });
        //GetUserInfo
        this.DM(10047, this, (c, onSuccess, onError) => {
            this._P.GetUserInfo(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onSuccess, data);
                let userInfo = new EditRequest();
                if (data.nickName)
                    userInfo.nickName = data.nickName;
                if (data.avatarUrl)
                    userInfo.headimgurl = data.avatarUrl;
                if (data.gender)
                    userInfo.gender = data.gender;
                if (data.province)
                    userInfo.province = data.province;
                if (data.city)
                    userInfo.city = data.city;
                if (data.country)
                    userInfo.country = data.country;
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Edit, userInfo, c, () => { });
                if (data.nickName)
                    this.NickName = data.nickName;
                if (data.avatarUrl)
                    this.AvatarUrl = data.avatarUrl;
            }, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onError, error);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10047);
        });
        //CreateUserInfoButton
        this.DM(10048, this, (btnInfo, c, onSuccess, onError) => {
            let str = "ShowUserInfoButton";
            this.Log(str);
            this._P.CreateUIBtn(btnInfo, this, (data) => {
                this.Log(str);
                this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].PfUInfo, data);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onSuccess, data);
                let userInfo = new EditRequest();
                if (data.nickName)
                    userInfo.nickName = data.nickName;
                if (data.avatarUrl)
                    userInfo.headimgurl = data.avatarUrl;
                if (data.gender)
                    userInfo.gender = data.gender;
                if (data.province)
                    userInfo.province = data.province;
                if (data.city)
                    userInfo.city = data.city;
                if (data.country)
                    userInfo.country = data.country;
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Edit, userInfo, c, () => { });
                if (data.nickName)
                    this.NickName = data.nickName;
                if (data.avatarUrl)
                    this.AvatarUrl = data.avatarUrl;
            }, (error) => {
                this.Warn(str + " error: " + error);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onError, null);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10048);
        });
        //ShowUserInfoButton
        this.DM(10049, this, () => {
            this._P.ShowUIBtn();
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10049);
        });
        //HideUserInfoButton
        this.DM(10050, this, () => {
            this._P.HideUIBtn();
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10050);
        });
        //DestroyUserInfoButton
        this.DM(10051, this, () => {
            this._P.DestroyUIBtn();
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10051);
        });
        //CreateInterstitialAd
        this.DM(10052, this, (isShow, c, onLoad, onClose, onError) => {
            let nowTime = new Date().getTime();
            if (this._P.IntttAd && this._intttAdWaitShow) {
                this.Invoke(10053);
                return;
            }
            if (!this.InterstitialAdUnitIdList || this.InterstitialAdUnitIdList.length == 0) {
                this.Log("InterstitialAdUnitIdList is empty");
                return;
            }
            if (!this._CanCreateIntttAd()) {
                c && onError && onError.call(c);
                return;
            }
            let me = this;
            let index = Math.floor(Math.random() * this.InterstitialAdUnitIdList.length);
            let adId = this.InterstitialAdUnitIdList[index];
            if (this._P.IntttAd) {
                this._P.ClearIntttAd();
            }
            this.Invoke(10004, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].CREATE, adId);
            this._P.CreateIntttAd(adId, this, () => {
                me.Invoke(10004, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].LOAD_SUCCESS, adId);
                if (isShow) {
                    me.Invoke(10053);
                }
                else {
                    c && onLoad && onLoad.call(c);
                }
                me.Log("InterstitialAd OnLoad");
            }, (result) => {
                me.Invoke(10004, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].CLOSE, adId);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onClose, result);
                me._showInterstitialAd = false;
                me._LCIntttAd = new Date().getTime();
            }, (error) => {
                me.Invoke(10004, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].LOAD_FAIL, adId);
                me.Warn("InterstistAd Error:" + error + " adId:" + adId);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onError, error);
                me._showInterstitialAd = false;
                me._LCIntttAd = new Date().getTime();
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10052);
        });
        //ShowInterstitialAd
        this.DM(10053, this, () => {
            if (!this._P.IntttAd) {
                this._intttAdWaitShow = false;
                return;
            }
            this._intttAdWaitShow = true;
            this._P.ShowIntttAd(this, () => {
                this._showInterstitialAd = true;
                this._intttAdWaitShow = false;
                this.Invoke(10004, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].SHOW, this._P.IntttAdId);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10053);
        });
        //GetSharingResults
        this.DM(10054, this, (shareInfo, c, m) => {
            shareInfo && this.Strategy && this.Strategy.GetSRlt(shareInfo, this, (shareBackInfo) => {
                this.Warn("Sharing Results");
                c && m.call(c, shareBackInfo);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10054);
        });
        //StatisticLayer 
        this.DM(10055, this, (layerPath) => {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().SwitchLayer)
                return;
            if (!layerPath || "" == layerPath) {
                this.Warn("layerPath is null or empty");
                return;
            }
            let nowTime = new Date().getTime();
            if (this._localLayerList) {
                //清理昨天的本地缓存数据
                let getTime = this._localLayerList.get_time;
                if (getTime) {
                    //判断本地缓存最后一次流失打点时间是否是今天(只保存今天的)
                    if (!this.isToday(getTime)) {
                        this._localLayerList = null;
                    }
                }
                else {
                    //清除本地缓存
                    this._localLayerList = null;
                }
            }
            //先看本地缓存已经打点的流失路径列表中是否有该路径
            let localInfo = this._localLayerList;
            let hasStatic = false; //是否已经打过点
            if (localInfo && localInfo.s_layers) {
                let index = localInfo.s_layers.indexOf(layerPath);
                hasStatic = (index > 0);
            }
            if (!(this.ListLayer && this.ListLayer.length > 0)) {
                this.Warn("流失列表为空,请确认是否调用获取流失列表接口,或后台配置了流失列表");
                return;
            }
            let pathList = this.ListLayer;
            let hasLayer = false;
            let p_index = -1;
            //查看后台配置的流失列表中是否有该路径
            for (let i = 0; i < pathList.length; i++) {
                let item = pathList[i];
                if (item.layerPath == layerPath) {
                    hasLayer = true;
                    p_index = i;
                    break;
                }
            }
            if (hasLayer && p_index != -1) {
                if (!hasStatic) {
                    if (!this._localLayerList) {
                        this._localLayerList = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["LocalLayerInfo"]();
                        this._localLayerList.s_layers = [];
                    }
                    for (let i = 0; i <= p_index; i++) {
                        let lPath = pathList[i].layerPath;
                        let index = this._localLayerList.s_layers.indexOf(lPath);
                        if (index < 0) {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].STAC().Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].Stat_Layer, lPath, this, (isOk) => { });
                            this._localLayerList.s_layers.push(lPath);
                        }
                    }
                    this._localLayerList.get_time = nowTime;
                    this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LStttLayer, this._localLayerList);
                }
                //这是一个特殊事件打点，用于统计关卡进入人数
                //和后台商议scene值为'LayerOn'
                this.Invoke(10011, layerPath, 'LayerOn');
            }
            else {
                this.Warn("statLayer-layerPath not found", 'warn');
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10055);
        });
        //GetShareRewardLimit
        this.DM(10056, this, () => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10056);
            return this.ShareFalseLimit;
        });
        //GetVideoRewardLimit
        this.DM(10057, this, () => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10057);
            return this.VideoFalseLimit;
        });
        /**GetServerInfo */
        this.DM(10058, this, (c, m) => {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.Invoke(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["NET_API"].MyInfo, c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10058);
        });
        /**ExitGame */
        this.DM(10059, this, () => {
            this._P.ExitGame();
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10059);
        });
        /**VibrateShort */
        this.DM(10060, this, () => {
            this._P.VibrateShort();
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10060);
        });
        /**VibrateLong */
        this.DM(10061, this, () => {
            this._P.VibrateLong();
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10061);
        });
        /**InstallShortcut */
        this.DM(10062, this, (c, onSuccess, onFail, onComplete, message) => {
            this._P.InstallShortcut(this, () => {
                c && onSuccess && onSuccess.call(c);
            }, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onFail, error);
            }, () => {
                c && onComplete && onComplete.call(c);
            }, message);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10062);
        });
        /**CreateNativeAd */
        this.DM(10063, this, (c, m) => {
            let index = Math.floor(Math.random() * this.NativeAdUnitIdList.length);
            this.nativeAdId = this.NativeAdUnitIdList[index];
            this._P.CreateNativeAd(this.nativeAdId, this, () => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().Invoke(10008, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].CREATE, this.nativeAdId);
            }, (_args) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, _args);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10063);
        });
        /**ShowNativeAd */
        this.DM(10064, this, (nativeId) => {
            this._P.ShowNativeAd(nativeId);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().Invoke(10008, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].EXPOSURE, this.nativeAdId);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10064);
        });
        /**ClickNativeAd */
        this.DM(10065, this, (nativeId) => {
            this._P.ClickNativeAd(nativeId);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].CME().Invoke(10008, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].CLICK, this.nativeAdId);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10065);
        });
        /**HasShortcutInstalled */
        this.DM(10066, this, (c, onSuccess, onFail, onComplete) => {
            this._P.HasShortcut(this, (result) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onSuccess, result);
            }, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, onFail, error);
            }, () => {
                c && onComplete && onComplete.call(c);
            });
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10066);
        });
        /**Hook */
        this.DM(10068, this, (name, c, m, ...args) => {
            this._P.Hook(name, c, m);
        });
        //GetObject
        this.DM(10074, this, (key, c, m) => {
            _Extensions_CloudAPI__WEBPACK_IMPORTED_MODULE_15__["CloudAPI"].GetObject(key, c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10074);
        });
        //GetString
        this.DM(10075, this, (key, c, m) => {
            _Extensions_CloudAPI__WEBPACK_IMPORTED_MODULE_15__["CloudAPI"].GetString(key, c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10075);
        });
        //SetObject
        this.DM(10076, this, (key, value, c, m) => {
            _Extensions_CloudAPI__WEBPACK_IMPORTED_MODULE_15__["CloudAPI"].SetObject(key, value, c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10076);
        });
        //SetString
        this.DM(10077, this, (key, value, c, m) => {
            _Extensions_CloudAPI__WEBPACK_IMPORTED_MODULE_15__["CloudAPI"].SetString(key, value, c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10077);
        });
        //Size
        this.DM(10078, this, (c, m) => {
            _Extensions_CloudAPI__WEBPACK_IMPORTED_MODULE_15__["CloudAPI"].Size(c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10078);
        });
        //DeleteObject
        this.DM(10079, this, (key, c, m) => {
            _Extensions_CloudAPI__WEBPACK_IMPORTED_MODULE_15__["CloudAPI"].DeleteObject(key, c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10079);
        });
        //StoreValue
        this.DM(10080, this, (storeInfo, c, m) => {
            _Extensions_CloudAPI__WEBPACK_IMPORTED_MODULE_15__["CloudAPI"].StoreValue(storeInfo, c, m);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10080);
        });
        this.DM(10084, this, (type) => {
            let IsCanReward = false;
            let videoFalseLimit = this.VideoFalseLimit;
            let shareFalseLimit = this.ShareFalseLimit;
            if (type == _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SHARE_T"].Video) {
                if (videoFalseLimit && videoFalseLimit > 0) {
                    IsCanReward = true;
                    videoFalseLimit -= 1;
                    videoFalseLimit = videoFalseLimit < 0 ? 0 : videoFalseLimit;
                    this.VideoFalseLimit = videoFalseLimit;
                }
            }
            else if (type == _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SHARE_T"].Share) {
                if (shareFalseLimit && shareFalseLimit > 0) {
                    IsCanReward = true;
                    shareFalseLimit -= 1;
                    shareFalseLimit = shareFalseLimit < 0 ? 0 : shareFalseLimit;
                    this.ShareFalseLimit = shareFalseLimit;
                }
            }
            if (IsCanReward) {
                this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].VFLimit, this.VideoFalseLimit);
                this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].SFLimit, this.ShareFalseLimit);
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10084);
            return IsCanReward;
        });
    }
    _AddUnlockVideoLevelNumber(index) {
        if (!index) {
            return;
        }
        this.AVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].TdWVCounter, 1);
        this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LWhVTstamp, new Date().getTime());
        let list = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LUlVLNumber, []);
        if (list.indexOf(index) == -1) {
            this.Log("AddUnlockLevelNumber index=" + index);
            list.push(index);
            this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].LUlVLNumber, list);
        }
    }
    RefreshLastPlayTime() {
        this._LastPlayTime = new Date().getTime();
    }
    //_IsCanCreateInterstitialAd
    /**
     * 是否可以创建插屏广告
     */
    _CanCreateIntttAd() {
        let nowTime = new Date().getTime();
        let interval = (nowTime - this._LCIntttAd) / 1000;
        let canCreate = false;
        this.Log("CreateIntertitialAd interval:", interval);
        if (this._P.IntttAd) {
            //如果存在InterstitialAd对象，标识已经创建过插屏广告
            let ifs = this.IntttAdFirstShowWT || 0;
            canCreate = interval >= ifs;
        }
        else {
            let is = this.IntttAdShowTimeI || 0;
            canCreate = interval >= is;
        }
        if (!canCreate) {
            this.Warn("创建插屏广告太频繁，请稍后再试");
        }
        return canCreate;
    }
    //判断传入的时间戳是否是今天
    //completeTime 需要比较的时间戳 
    isToday(completeTime) {
        let nowTime = new Date();
        let nowYear = nowTime.getFullYear();
        let nowMonth = nowTime.getMonth() + 1;
        let nowDay = nowTime.getDate();
        let comT = new Date(completeTime);
        let comTYear = comT.getFullYear();
        let comTMonth = comT.getMonth() + 1;
        let comTDay = comT.getDate();
        return (nowYear == comTYear && nowMonth == comTMonth && nowDay == comTDay);
    }
    _AddPowerByVideo() {
        if (!this.PSysCfg || this.PSysCfg.IsOn === false) {
            this.Log("Power System is off");
            return;
        }
        let powerWeight = this.PSysCfg.VideoPowerWeight;
        let powerNum = this.PowerSystem.GetPower();
        this.PowerSystem.SetPower((powerNum + powerWeight), _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["P_R_T"].WatchVideo);
    }
    IsCanRewardByVideoOrShare(type) {
        let IsCanReward = false;
        let videoFalseLimit = this.VideoFalseLimit;
        let shareFalseLimit = this.ShareFalseLimit;
        if (type == _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SHARE_T"].Video) {
            if (videoFalseLimit && videoFalseLimit > 0) {
                IsCanReward = true;
                videoFalseLimit -= 1;
                videoFalseLimit = videoFalseLimit < 0 ? 0 : videoFalseLimit;
                this.VideoFalseLimit = videoFalseLimit;
            }
        }
        else if (type == _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SHARE_T"].Share) {
            if (shareFalseLimit && shareFalseLimit > 0) {
                IsCanReward = true;
                shareFalseLimit -= 1;
                shareFalseLimit = shareFalseLimit < 0 ? 0 : shareFalseLimit;
                this.ShareFalseLimit = shareFalseLimit;
            }
        }
        if (IsCanReward) {
            this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].VFLimit, this.VideoFalseLimit);
            this.SVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].SFLimit, this.ShareFalseLimit);
        }
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].API().Invoke(10084);
        return IsCanReward;
    }
    _CreateBannerAd(isSmall, isMisTouch, isShow, style, c, m, onResize) {
        if (!this.BannerAdUnitIdList || this.BannerAdUnitIdList.length == 0) {
            this.Error("BannerAdUnitIdList is empty.");
        }
        else {
            if (this._P.BnAd) {
                this._P.DstyBnAd();
                this._P.IsSBnAd = false;
            }
            let me = this;
            this._needShowBannerAd = true;
            let index = Math.floor(Math.random() * this.BannerAdUnitIdList.length);
            let adId = this.BannerAdUnitIdList[index];
            this._P.CreateBnAd(isSmall, adId, isMisTouch, style, this, onResize);
            this.Invoke(10002, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].CREATE, adId);
            //是否定时刷新banner
            let isOn = this.BnRefCfg && this.BnRefCfg.IsOn;
            if (isOn) {
                this._IsSmallBannerAd = isSmall;
            }
            this._P.BnAd.onLoad(function () {
                me.Log("BannerAd onLoad");
                me.Invoke(10002, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].LOAD_SUCCESS, adId);
                if (isShow && me._needShowBannerAd)
                    me.Invoke(10038);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, true);
            });
            this._P.BnAd.onError((error) => {
                me.Invoke(10002, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["TP_AD"].LOAD_FAIL, adId);
                me.Warn("BannerAd onError:", error);
                this._P.DstyBnAd();
                this._needShowBannerAd = false;
                this._P.IsSBnAd = false;
                if (isOn) {
                    let delay = me.BnRefCfg.Interval;
                    me.Log(`将在${delay}秒后重新创建banner(调用ylBannerAdHide会取消重试`);
                    _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].RemoveDelayTimer(me._BannerAdDelayTimerId);
                    me._BannerAdDelayTimerId = _fw_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].Delay(delay * 1000, me, () => {
                        me._CreateBannerAd(isSmall, isMisTouch, isShow, style, onResize, c, m); //延迟刷新banner
                    });
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].C(c, m, false);
                }
            });
        }
    }
    /**
     * 是否直接通过AccountPass登录
     * true
     * 条件：
     *  1.本地存在AccountPass
     *  2.本地AccountPass的没有超过半天
     */
    LoginByAccountPass() {
        let accountPassInfo = this.GVT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SV"].APYDHW, null);
        if (accountPassInfo) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].L("YDHW AccountPass:" + JSON.stringify(accountPassInfo));
            let isHalfDay = true;
            if (accountPassInfo.save_time) {
                let nowTime = new Date().getTime();
                let saveTime = accountPassInfo.save_time;
                let halfDay = 1000 * 60 * 60 * 12; //超过半天就重新调用平台登录刷新accountPass
                isHalfDay = (nowTime - saveTime) >= halfDay;
            }
            if (!isHalfDay) {
                this.AccountPass = accountPassInfo.account_pass;
                _fw_Utility_Url__WEBPACK_IMPORTED_MODULE_5__["Url"].SetAccountPass(this.AccountPass);
                return true;
            }
        }
        return false;
    }
    Log(message, ...args) {
        this.SwitchLog && super.Log(message, ...args);
    }
    Debug(message, ...args) {
        this.SwitchLog && super.Debug(message, ...args);
    }
    Info(message, ...args) {
        this.SwitchLog && super.Info(message, ...args);
    }
    Warn(message, ...args) {
        this.SwitchLog && super.Warn(message, ...args);
    }
    Error(message, ...args) {
        this.SwitchLog && super.Error(message, ...args);
    }
}


/***/ }),

/***/ "./sdk/Model/Data.ts":
/*!***************************!*\
  !*** ./sdk/Model/Data.ts ***!
  \***************************/
/*! exports provided: StoreValueReq, BannerRefreshCfg, PowerSystemCfg, VideoUnlockLevelCfg, DeepSheildRuleCfg, MisTouchSwitchCfg, DeveloperAccountCfg, LocalLayerInfo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoreValueReq", function() { return StoreValueReq; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BannerRefreshCfg", function() { return BannerRefreshCfg; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PowerSystemCfg", function() { return PowerSystemCfg; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoUnlockLevelCfg", function() { return VideoUnlockLevelCfg; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeepSheildRuleCfg", function() { return DeepSheildRuleCfg; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MisTouchSwitchCfg", function() { return MisTouchSwitchCfg; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeveloperAccountCfg", function() { return DeveloperAccountCfg; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalLayerInfo", function() { return LocalLayerInfo; });
class StoreValueReq {
    constructor() {
        this.name = "";
        this.cmd = "";
        this.args = "";
    }
}
class BannerRefreshCfg {
}
class PowerSystemCfg {
    constructor() {
        this.DefaultPowerValue = 0;
        this.UpperLimit = 0;
        this.AutoRecoveryTime = 0;
        this.VideoPowerWeight = 0;
        this.IsOn = false;
    }
}
class VideoUnlockLevelCfg {
    constructor() {
        this.IsOn = false;
        this.CustomNumber = 0;
        this.IntervalCount = 0;
    }
}
class DeepSheildRuleCfg {
    constructor() {
        this.Type = 0;
        this.CustomNumberCounter = 0;
        this.ExecellentUserSwitch = false;
        this.WatchVideoCounter = 0;
        this.DayCustomNumber = [];
    }
}
class MisTouchSwitchCfg {
    constructor() {
        this.IsOn = false;
        this.StartTimeHour = 0;
        this.StartTimeMinute = 0;
        this.EndTimeHour = 0;
        this.EndTimeMinute = 0;
    }
}
class DeveloperAccountCfg {
    constructor() {
        this.IsOn = false;
        this.Accounts = [];
    }
}
class LocalLayerInfo {
    constructor() {
        this.s_layers = null;
        this.get_time = null;
    }
}


/***/ }),

/***/ "./sdk/Model/Default.ts":
/*!******************************!*\
  !*** ./sdk/Model/Default.ts ***!
  \******************************/
/*! exports provided: BannerAdStyle, ShareAppInfo, ShareBackInfo, CustomInfo, DeepTouchInfo, AppInfo, JumpOutInfo, StatisticEventReq */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BannerAdStyle", function() { return BannerAdStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareAppInfo", function() { return ShareAppInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareBackInfo", function() { return ShareBackInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomInfo", function() { return CustomInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeepTouchInfo", function() { return DeepTouchInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppInfo", function() { return AppInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JumpOutInfo", function() { return JumpOutInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatisticEventReq", function() { return StatisticEventReq; });
class BannerAdStyle {
}
class ShareAppInfo {
    constructor() {
        this.channel = "";
        this.module = "";
        this.showTime = 0;
        this.shareId = 0;
    }
}
class ShareBackInfo {
    constructor() {
        this.IsSuccess = false;
        this.IsHasStrategy = false;
        this.Tips = null;
    }
}
class CustomInfo {
    constructor() {
        this._id = 0;
        this.name = "";
        this.version = "";
        this.type = "";
        this.value = "";
        this.switchRegionUse = "";
        this.switchTouchUse = "";
        this.desc = "";
    }
}
class DeepTouchInfo {
    constructor() {
        this.deepTouch = false;
        this.ListCustomInfo = [];
    }
}
class AppInfo {
    constructor(appId) {
        this.appid = "";
        this.appid = appId;
    }
}
class JumpOutInfo {
    constructor() {
        this.Date = 0;
        this.List = [];
    }
}
class StatisticEventReq {
}


/***/ }),

/***/ "./sdk/Model/Statistic.ts":
/*!********************************!*\
  !*** ./sdk/Model/Statistic.ts ***!
  \********************************/
/*! exports provided: ClientLogReq, ClickOutReq, SdkInfo, StatistiicShareInfo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientLogReq", function() { return ClientLogReq; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClickOutReq", function() { return ClickOutReq; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SdkInfo", function() { return SdkInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatistiicShareInfo", function() { return StatistiicShareInfo; });
class ClientLogReq {
}
class ClickOutReq {
}
class SdkInfo {
}
class StatistiicShareInfo {
    constructor(shareTime, sharecardId, sType, target, real) {
        this.sharecardId = sharecardId || -1;
        this.shareTime = shareTime;
        if (sType != null)
            this.sType = sType || 0;
        if (target)
            this.target = target;
        this.real = real || 0;
    }
}


/***/ }),

/***/ "./sdk/Model/User.ts":
/*!***************************!*\
  !*** ./sdk/Model/User.ts ***!
  \***************************/
/*! exports provided: ShareInfo, ClientInfo, LoginRequest, LoginRsp, StrategyData, Tips, ShareVideoModuleFalse, LoopCtrl, ShareVideoModule, UnUseShareVideoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareInfo", function() { return ShareInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientInfo", function() { return ClientInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginRequest", function() { return LoginRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginRsp", function() { return LoginRsp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StrategyData", function() { return StrategyData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tips", function() { return Tips; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareVideoModuleFalse", function() { return ShareVideoModuleFalse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoopCtrl", function() { return LoopCtrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareVideoModule", function() { return ShareVideoModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UnUseShareVideoModule", function() { return UnUseShareVideoModule; });
class ShareInfo {
}
class ClientInfo {
}
class LoginRequest {
}
class LoginRsp {
}
class StrategyData {
}
class Tips {
}
class ShareVideoModuleFalse {
    constructor() {
        /**
         * 策略id
         */
        this.strategyId = 0;
        /**
         * time数组ID
         */
        this.timeId = 0;
        /**
         * 执行遍数
         */
        this.count = 0;
    }
}
class LoopCtrl {
}
class ShareVideoModule {
}
class UnUseShareVideoModule {
}


/***/ }),

/***/ "./sdk/SDK/Declare.ts":
/*!****************************!*\
  !*** ./sdk/SDK/Declare.ts ***!
  \****************************/
/*! exports provided: SDK */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDK", function() { return SDK; });
/* harmony import */ var _fw_Core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../fw/Core */ "./fw/Core.ts");
/* harmony import */ var _Enum__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Enum */ "./sdk/SDK/Enum.ts");
/* harmony import */ var _fw_Utility_Url__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../fw/Utility/Url */ "./fw/Utility/Url.ts");
/* harmony import */ var _Base_SDKP__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Base/SDKP */ "./sdk/Base/SDKP.ts");
/* harmony import */ var _Assist_Log__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../Assist/Log */ "./sdk/Assist/Log.ts");





class SDK {
    static Domain() {
        return this._Url;
    }
    static P() {
        if (this._Me == null) {
            this._Me = new _Base_SDKP__WEBPACK_IMPORTED_MODULE_3__["SDKP"]();
        }
        return this._Me;
    }
    static Core() {
        return _fw_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Me();
    }
    static get Me() {
        return _fw_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Me().AsManager();
    }
    /**AgentMgr extends NetManager */
    static get Agent() {
        return _fw_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Me().GetManager(_Enum__WEBPACK_IMPORTED_MODULE_1__["MGR"].Agent);
    }
    /**CommerceManager extends BaseManager */
    static CME() {
        return _fw_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Me().GetManager(_Enum__WEBPACK_IMPORTED_MODULE_1__["MGR"].Commerce);
    }
    /** StatisticManager extends NetManager*/
    static STAC() {
        return _fw_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Me().GetManager(_Enum__WEBPACK_IMPORTED_MODULE_1__["MGR"].Statistic);
    }
    /** ApiManager extends ApiManager*/
    static API() {
        return _fw_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Me().GetManager(_Enum__WEBPACK_IMPORTED_MODULE_1__["MGR"].Api);
    }
    /** DataMgr extends NetManager */
    static get Data() {
        return _fw_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Me().GetManager(_Enum__WEBPACK_IMPORTED_MODULE_1__["MGR"].Data);
    }
    static E(name, rsp) {
        SDK.Debug && _Assist_Log__WEBPACK_IMPORTED_MODULE_4__["Log"].Log(name + " Response=", rsp);
    }
    static L(...args) {
        SDK.Debug && console.log(...args);
    }
    static W(...args) {
        SDK.Debug && console.warn(...args);
    }
    static E1(rsp) {
        SDK.Debug && _Assist_Log__WEBPACK_IMPORTED_MODULE_4__["Log"].Error("server say:" + rsp.info + " code:" + rsp.code);
    }
    static C(c, m, ...args) {
        c && m && m.call(c, ...args);
    }
}
SDK.Debug = false;
// static get Instance(): Core {
//     return Core.Instance;
// }
SDK._Url = new _fw_Utility_Url__WEBPACK_IMPORTED_MODULE_2__["Url"]();
SDK._Me = null;


/***/ }),

/***/ "./sdk/SDK/Enum.ts":
/*!*************************!*\
  !*** ./sdk/SDK/Enum.ts ***!
  \*************************/
/*! exports provided: MGR */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MGR", function() { return MGR; });
/**
 * 管理器类型枚举
 */
var MGR;
(function (MGR) {
    MGR["Agent"] = "AgentMgr";
    MGR["Event"] = "EventMgr";
    MGR["Commerce"] = "CommerceMgr";
    MGR["Resource"] = "ResourceMgr";
    MGR["State"] = "StateMgr";
    MGR["Panel"] = "PanelMgr";
    MGR["Statistic"] = "StatisticMgr";
    MGR["Data"] = "DataMgr";
    MGR["Sound"] = "SoundMgr";
    MGR["Platform"] = "PlatformMgr";
    MGR["Logic"] = "Logic";
    MGR["Log"] = "LogMgr";
    MGR["Power"] = "Power";
    MGR["Api"] = "ApiMgr";
})(MGR || (MGR = {}));
// /**
//  * 方法ID枚举
//  */
// export enum METHOD {
//     InitView = "InitView",
//     OnClear = "OnClear",
//     OnCancel = "OnCancel",
//     OnRemoved = "OnRemoved",
//     OnSelect = "OnSelect",
//     OnRefresh = "OnRefresh",
//     OnChangedItem = "OnChangedItem",
//     //----------------------------------------------------------------------------------------
//     //ResourceMgr
//     OnPreloadStateGame = "PreloadStateGame",
//     OnPreloadStateHome = "OnPreloadStateHome",
//     OnPreloadStateLoading = "OnPreloadStateLoading",
//     OnPreloadStateResult = "OnPreloadStateResult",
//     OnPreloadConfig = "OnPreloadConfig",
//     OnLoginSuccess = "OnLoginSuccess",
//     //----------------------------------------------------------------------------------------
//     //Config
//     //----------------------------------------------------------------------------------------
//     //UserDataMgr
//     /**
//      * 进入下一关
//      */
//     OnIncreaseLevel = "OnIncreaseLevel",
//     //商业化
//     /**
//      * todo:关闭原生Banner
//      */
//     OnCloseNativeBanner = "OnCloseNativeBanner",
//     /**
//      * todo:刷新原生Banner
//      */
//     OnRefreshNativeBanner = "OnRefreshNativeBanner",
//     //----------------------------------------------------------------------------------------
//     //StateMgr
//     /**
//      * 加载进度完成时触发
//      */
//     OnFinishLoading = "OnFinishLoading",
//     /**
//      * 刷新进度条
//      */
//     OnLoadingProgress = "OnLoadingProgress",
//     /**
//      * 加载进度报错
//      */
//     OnLoadingError = "OnLoadingError",
//     //----------------------------------------------------------------------------------------
//     //PanelMgr
//     OnMouseDownBtnSkinVisible = "OnMouseDownBtnSkinVisible",
//     //----------------------------------------------------------------------------------------
//     //SoundManager
//     OnPlayEffect = "OnPlayEffect",
//     OnVibrate = "OnVibrate",
//     //----------------------------------------------------------------------------------------
//     //Logic
//     // OnLogicPreload = "OnLogicPreload",
//     //----------------------------------------------------------------------------------------
//     //Skin
//     /**
//      * 解锁皮肤
//      */
//     /**
//      * 随机出一个皮肤ID
//      */
//     OnRandomUnlockSkin = "OnRandomUnlockSkin",
//     /**
//      * 开宝箱解锁皮肤
//      */
//     OnUnlockSkinByTreasureBox = "OnUnlockSkinByTreasureBox",
//     /**
//      * 计算金币解锁皮肤需要多少个金币数量
//      */
//     OnHowManyCoinCost = "OnHowManyCoinCost",
//     /**
//      * 花费金币购买皮肤
//      */
//     OnUnlockSkinByGold = "OnUnlockSkinByGold",
//     /**
//      * 如果当前解锁皮肤的方式是看视频，则需要保存视频次数
//      */
//     OnUnlockSkinByVideoAd = "OnUnlockSkinByVideoAd",
//     //---------------------------------------------------------------------------------------------
//     //Commmerce 商业化
//     /**
//      * 重新初始化侧边栏
//      */
//     OnReinitSideBox = "重新初始化侧边栏",
//     //----------------------------------------------------------------------------------------
//     //LogMgr
//     OnCheckAllVariables = "OnCheckAllVariables",
//     OnCheckAllMethods = "OnCheckAllMethods",
// }
// /**
//  * 变量ID枚举
//  */
// export enum VARS {
//     Power = "Power",
//     BallBounceSound = "BallBounceSound",
//     FloorBrokeSound = "FloorBrokeSound",
//     MusicSuffixName = "MusicSuffixName",
//     BackgroundMusic = "BackgroundMusic",
//     IsOpenBackgrouMusic = "IsOpenBackgrouMusic",
//     IsOpenEffectSound = "IsOpenEffectSound",
//     IsOpenVibrate = "IsOpenVibrate",
//     IsFirstTimeEnterGame = "IsFirstGameStart",
//     /**
//      * 设置背景音乐音量
//      */
//     MusicVolume = "MusicVolume",
//     /**
//      * 设置音效音乐音量
//      */
//     EffectVolume = "EffectVolume",
//     /**
//      * 当前进行中的关卡
//      */
//     CurrentLevelIndex = "CurrentLevelIndex",
//     /**
//      * 当前金币数量
//      */
//     CurrentCoinCount = "CurrentCoinCount",
//     /**
//      * 当前抽奖券数量
//      */
//     CurrentTicketCount = "CurrentTicketCount",
//     /**
//      * 当前剩余看视频获取抽奖券有效机会
//      */
//     CurrentTicketsForWatchVideoAd = "CurrentTicketUsedForWatchVideoAd",
//     /**
//      * 当前累计总分
//      */
//     CurrentTotalScore = "CurrentTotalScore",
//     /**
//      * 历史最高分
//      */
//     HistoryHighScore = "HistoryHighScore",
//     /**
//      * 当前累计看视频次数
//      */
//     CurrentVideoAdCount = "CurrentVideoAdCount",
//     /**
//      * 当前正在看视频解锁的皮肤
//      */
//     CurrentUnlockingSkinId = "CurrentUnlockingSkinId",
//     CurrentUsingSkinId = "CurrentUsingSkinId",
//     /**
//      * 时间戳
//      */
//     LastTimestamp = "LastTimestamp",
//     /**
//      * 已经解锁的皮肤的全局皮肤Id列表
//      */
//     UnlockSkinList = "UnlockSkinList",
//     /**
//      * 已经抽奖被抽取过的转盘选项Id列表
//      */
//     UsedRewardList = "UsedRewardList",
//     GMGameFinish = "GMGameFinish",
//     /**
//      * 当前需要获取的卡片下标
//      */
//     CurrentShareCardIndex = "ShareCardIndex",
//     CurrentScoreVisible = "CurrentScoreVisible",
//     HighestScoreVisible = "HistoryHighestScoreVisible",
//     IsBtnSkinVisible = "IsBtnSkinVisible",
//     /**
//      * 商业化
//      */
//     // ComerceSceneId = "ComerceSceneId",
//     // ComerceChannel = "ComerceChannel",
//     // ComerceShareCardId = "ComerceShareCardId",
//     // ComerceExtraData = "ComerceExtraData",
// }


/***/ }),

/***/ "./sdk/SDK/EventDef.ts":
/*!*****************************!*\
  !*** ./sdk/SDK/EventDef.ts ***!
  \*****************************/
/*! exports provided: NET_API, ST_S_TYPE, SV, SE, VIDEO_P_T, TP_AD, SHARE_T, P_R_T, POWER_EVT, TIMER_NAME, EM_APP_BOX_TYPE, BLOCK_T, SHARE_A_T */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NET_API", function() { return NET_API; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ST_S_TYPE", function() { return ST_S_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SV", function() { return SV; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SE", function() { return SE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VIDEO_P_T", function() { return VIDEO_P_T; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TP_AD", function() { return TP_AD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SHARE_T", function() { return SHARE_T; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "P_R_T", function() { return P_R_T; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "POWER_EVT", function() { return POWER_EVT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TIMER_NAME", function() { return TIMER_NAME; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EM_APP_BOX_TYPE", function() { return EM_APP_BOX_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BLOCK_T", function() { return BLOCK_T; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SHARE_A_T", function() { return SHARE_A_T; });
/**
 * NET_API
 */
// export enum NET_API {
//     //Agent
//     Edit = "Edit",
//     Login = "Login",
//     MyInfo = "MyInfo",
//     //Statistic
//     //广告
//     Banner = "Banner",
//     Video = "Video",
//     InterstitialAd = "InterstitialAd",
//     GridAd = "GridAd",
//     AppBoxAd = "AppBoxAd",
//     BlockAd = "BlockAd",
//     NativeAd = "NativeAd",
//     /**
//      * 卖量统计1
//      */
//     ClickOut = "ClickOut",
//     ClientLog = "ClientLog",
//     Duration = "Duration",
//     Event = "Event",
//     Statistic_Api() = "Statistic_Api()",
//     Statistic_Layer = "Statistic_Layer",
//     Result = "Result",
//     Statistic_ShareCard = "Statistic_ShareCard",
//     //Data
//     Config = "Config",
//     CustomConfig = "CustomConfig",
//     DataDecode = "DataDecode",
//     GetBoardAward = "GetBoardAward",
//     Data_Layer = "Data_Layer",
//     // ScoreBoard = "ScoreBoard",
//     SideBox = "SideBox",
//     Data_ShareCard = "Data_ShareCard",
//     StoreValue = "StoreValue",
//     GetMyIp = "GetMyIp",
// }
var NET_API;
(function (NET_API) {
    //Agent
    NET_API[NET_API["Edit"] = 201] = "Edit";
    NET_API[NET_API["Login"] = 202] = "Login";
    NET_API[NET_API["MyInfo"] = 203] = "MyInfo";
    //Statistic
    //广告
    NET_API[NET_API["Banner"] = 204] = "Banner";
    NET_API[NET_API["Video"] = 205] = "Video";
    NET_API[NET_API["InterstitialAd"] = 206] = "InterstitialAd";
    NET_API[NET_API["GridAd"] = 207] = "GridAd";
    NET_API[NET_API["AppBoxAd"] = 208] = "AppBoxAd";
    NET_API[NET_API["BlockAd"] = 209] = "BlockAd";
    NET_API[NET_API["NativeAd"] = 210] = "NativeAd";
    /**
     * 卖量统计1
     */
    NET_API[NET_API["ClickOut"] = 211] = "ClickOut";
    NET_API[NET_API["ClientLog"] = 212] = "ClientLog";
    NET_API[NET_API["Duration"] = 213] = "Duration";
    NET_API[NET_API["Event"] = 214] = "Event";
    NET_API[NET_API["Stat_Api"] = 215] = "Stat_Api";
    NET_API[NET_API["Stat_Layer"] = 216] = "Stat_Layer";
    NET_API[NET_API["Result"] = 217] = "Result";
    NET_API[NET_API["Stat_ShareCard"] = 218] = "Stat_ShareCard";
    //Data
    NET_API[NET_API["Config"] = 219] = "Config";
    NET_API[NET_API["CustomConfig"] = 220] = "CustomConfig";
    NET_API[NET_API["DataDecode"] = 221] = "DataDecode";
    NET_API[NET_API["GetBoardAward"] = 222] = "GetBoardAward";
    NET_API[NET_API["Data_Layer"] = 223] = "Data_Layer";
    NET_API[NET_API["ScoreBoard"] = 224] = "ScoreBoard";
    NET_API[NET_API["SideBox"] = 225] = "SideBox";
    NET_API[NET_API["Data_ShareCard"] = 226] = "Data_ShareCard";
    NET_API[NET_API["StoreValue"] = 227] = "StoreValue";
    NET_API[NET_API["GetMyIp"] = 228] = "GetMyIp";
})(NET_API || (NET_API = {}));
//统计系统的统计事件类型
var ST_S_TYPE;
(function (ST_S_TYPE) {
    ST_S_TYPE[ST_S_TYPE["Banner"] = 1] = "Banner";
    ST_S_TYPE[ST_S_TYPE["Video"] = 2] = "Video";
    ST_S_TYPE[ST_S_TYPE["Interstitial"] = 3] = "Interstitial";
    ST_S_TYPE[ST_S_TYPE["Grid"] = 4] = "Grid";
    ST_S_TYPE[ST_S_TYPE["AppBox"] = 5] = "AppBox";
    ST_S_TYPE[ST_S_TYPE["Block"] = 6] = "Block";
    ST_S_TYPE[ST_S_TYPE["Native"] = 7] = "Native";
    ST_S_TYPE[ST_S_TYPE["Result"] = 8] = "Result";
    ST_S_TYPE[ST_S_TYPE["ClickOut"] = 9] = "ClickOut";
    ST_S_TYPE[ST_S_TYPE["Event"] = 10] = "Event";
})(ST_S_TYPE || (ST_S_TYPE = {}));
/**
 * COMMERCE_API
 */
// export enum COMMERCE_API {
//     LoginAddress = "API_LoginAddress",
//     Login_Api = "API_Login_Api",
//     GetLeftTopBtnPosition = "API_GetLeftTopBtnPosition",
//     ShareInfo = "API_ShareInfo",
//     StatisticBanner = "API_StatisticBanner",
//     StatisticVideo = "API_StatisticVideo",
//     StatisticInterstitial = "API_StatisticInterstitial",
//     StatisticGrid = "API_StatisticGrid",
//     StatisticAppBox = "API_StatisticAppBox",
//     StatisticBlock = "API_StatisticBlock",
//     StatisticNativeAd = "API_StatisticNativeAd",
//     StatisticErrorStack = "API_StatisticErrorStack",
//     StatisticResult = "API_StatisticResult",
//     StatisticEvent = "API_StatisticEvent",
//     StatisticDuration = "API_StatisticDuration",
//     StatisticClickOut = "API_StatisticClickOut",
//     ShowShareOrVideo = "API_ShowShareOrVideo",
//     UseShareOrVideoStrategy = "API_UseShareOrVideoStrategy",
//     SwitchView = "API_SwitchView",
//     GetDeepTouchInfo = "API_GetDeepTouchInfo",
//     GetCustomConfig = "API_GetCustomConfig",
//     GameOver = "API_GameOver",
//     IsUnlockVideo = "API_IsUnlockVideo",
//     GetPowerInfo = "API_GetPowerInfo",
//     ListenOnPowerChanged = "API_ListenOnPowerChanged",
//     SetPower = "API_SetPower",
//     GetPower = "API_GetPower",
//     GetSideBox = "API_GetSideBox",
//     GetScoreBoardList = "API_GetScoreBoardList",
//     GetScoreBoardAward = "API_GetScoreBoardAward",
//     ShareCard = "API_ShareCard",
//     GetTodayBoutCount = "API_GetTodayBoutCount",
//     GetTotalBoutCount = "API_GetTotalBoutCount",
//     GetLastBountNumber = "API_GetLastBountNumber",
//     GetMaxBountNumber = "API_GetMaxBountNumber",
//     GetTodayWatchVideoCounter = "API_GetTodayWatchVideoCounter",
//     CreateBannerAd = "API_CreateBannerAd",
//     CreateSmallBannerAd = "API_CreateSmallBannerAd",
//     CreateCustomBannerAd = "API_CreateCustomBannerAd",
//     BannerAdChangeSize = "API_BannerAdChangeSize",
//     ShowBannerAd = "API_ShowBannerAd",
//     HideBannerAd = "API_HideBannerAd",
//     ChangeBannerStyle = "API_ChangeBannerStyle",
//     CreateRewardVideoAd = "API_CreateRewardVideoAd",
//     ShowRewardVideoAd = "API_ShowRewardVideoAd",
//     StatisticShareCard = "API_StatisticShareCard",
//     GetLayerList = "API_GetLayerList",
//     NavigateToMiniProgram = "API_NavigateToMiniProgram",
//     CreateUserInfoButton = "API_CreateUserInfoButton",
//     ShowUserInfoButton = "API_ShowUserInfoButton",
//     HideUserInfoButton = "API_HideUserInfoButton",
//     DestroyUserInfoButton = "API_DestroyUserInfoButton",
//     CreateInterstitialAd = "API_CreateInterstitialAd",
//     ShowInterstitialAd = "API_ShowInterstitialAd",
//     GetSharingResults = "API_GetSharingResults",
//     StatisticLayer = "API_StatisticLayer",
//     IsCanRewardByVideoOrShare = "API_IsCanRewardByVideoOrShare",
//     GetShareRewardLimit = "API_GetShareRewardLimit",
//     GetVideoRewardLimit = "API_GetVideoRewardLimit",
//     GetServerInfo = "API_GetServerInfo",
//     ExitGame = "API_ExitGame",
//     VibrateShort = "API_VibrateShort",
//     VibrateLong = "API_VibrateLong",
//     InstallShortcut = "API_InstallShortcut",
//     CreateNativeAd = "API_CreateNativeAd",
//     ShowNativeAd = "API_ShowNativeAd",
//     ClickNativeAd = "API_ClickNativeAd",
//     HasShortcutInstalled = "API_HasShortcutInstalled",
//     ShareAppMessage = "API_ShareAppMessage",
//     Hook = "API_Hook",
//     OnFrontend = "API_OnFrontend",
//     OnBackend = "API_OnBackend",
//     OnShow = "API_OnShow",
//     OnHide = "API_OnHide",
//     OnError = "API_OnError",
//     GetObject = "API_GetObject",
//     GetString = "API_GetString",
//     SetObject = "API_SetObject",
//     SetString = "API_SetString",
//     Size = "API_Size",
//     DeleteObject = "API_DeleteObject",
//     StoreValue = "API_StoreValue",
//     //----------------------------------------------------------------
//     //TT
//     ShareImage = "API_ShareImage",
//     ShareVideo = "API_ShareVideo",
//     ShareTemplate = "API_ShareTemplate",
//     ShareToken = "API_ShareToken",
//     ShowMoreGamesModal = "API_ShowMoreGamesModal",
//     CreateMoreGamesButton = "API_CreateMoreGamesButton",
//     RecorderStart = "API_RecorderStart",
//     RecorderStop = "API_RecorderStop",
//     RecorderPause = "API_RecorderPause",
//     RecorderResume = "API_RecorderResume",
//     StatisticMoreGameOut = "API_StatisticMoreGameOut",
//     //----------------------------------------------------------------
//     //WeChat
//     CreateGridAd = "API_CreateGridAd",
//     ShowGridAd = "API_ShowGridAd",
//     HideGridAd = "API_HideGridAd",
//     SubscribeSysMsg = "API_SubscribeSysMsg",
//     GetSetting = "API_GetSetting",
//     WXRecorderOff = "API_WXRecorderOff",
//     WXRecorderOn = "API_WXRecorderOn",
//     WXRecorderStart = "API_WXRecorderStart",
//     WXRecorderStop = "API_WXRecorderStop",
//     WXRecorderPause = "API_WXRecorderPause",
//     WXRecorderResume = "API_WXRecorderResume",
//     WXRecorderAbort = "API_WXRecorderAbort",
//     IsAtempoSupported = "API_IsAtempoSupported",
//     IsFrameSupported = "API_IsFrameSupported",
//     IsSoundSupported = "API_IsSoundSupported",
//     IsVolumeSupported = "API_IsVolumeSupported",
//     CreateGameRecorderShareButton = "API_CreateGameRecorderShareButton",
//     GameRecorderShareButtonHide = "API_GameRecorderShareButtonHide",
//     GameRecorderShareButtonShow = "API_GameRecorderShareButtonShow",
//     GameRecorderShareButtonOnTap = "API_GameRecorderShareButtonOnTap",
//     GameRecorderShareButtonOffTap = "API_GameRecorderShareButtonOffTap",
//     SetMessageToFriendQuery = "API_SetMessageToFriendQuery",
//     OnShareMessageToFriend = "API_OnShareMessageToFriend",
//     OnShareTimeline = "API_OnShareTimeline",
//     OffShareTimeline = "API_OffShareTimeline",
//     //----------------------------------------------------------------
//     //QQ
//     ShareByTemplateId = "API_ShareByTemplateId",
//     AddColorSign = "API_AddColorSign",
//     // SaveAppToDesktop = "API_SaveAppToDesktop",
//     SubscribeAppMsg = "API_SubscribeAppMsg",
//     CreateAddFriendButton = "API_CreateAddFriendButton",
//     ShowAddFriendButton = "API_ShowAddFriendButton",
//     HideAddFriendButton = "API_HideAddFriendButton",
//     DestroyAddFriendButton = "API_DestroyAddFriendButton",
//     CreateAppBox = "API_CreateAppBox",
//     ShowAppBox = "API_ShowAppBox",
//     CreateBlockAd = "API_CreateBlockAd",
//     ShowBlockAd = "API_ShowBlockAd",
//     HideBlockAd = "API_HideBlockAd",
//     DestroyBlockAd = "API_DestroyBlockAd",
//     //----------------------------------------------------------------
//     //MeiZu
//     GetNetworkType = "API_GetNetworkType",
//     OnNetworkStatusChange = "API_OnNetworkStatusChange",
//     //----------------------------------------------------------------
//     //Vivo
//     IsStartupByShortcut = "API_IsStartupByShortcut",
// }
// export enum COMMERCE_API {
//     LoginAddress,
//     Login_Api,
//     GetLeftTopBtnPosition,
//     ShareInfo,
//     StatisticBanner,
//     StatisticVideo,
//     StatisticInterstitial,
//     StatisticGrid,
//     StatisticAppBox,
//     StatisticBlock,
//     StatisticNativeAd,
//     StatisticErrorStack,
//     StatisticResult,
//     StatisticEvent,
//     StatisticDuration,
//     StatisticClickOut,
//     ShowShareOrVideo,
//     UseShareOrVideoStrategy,
//     SwitchView,
//     GetDeepTouchInfo,
//     GetCustomConfig,
//     GameOver,
//     IsUnlockVideo,
//     GetPowerInfo,
//     ListenOnPowerChanged,
//     SetPower,
//     GetPower,
//     GetSideBox,
//     GetScoreBoardList,
//     GetScoreBoardAward,
//     ShareCard,
//     GetTodayBoutCount,
//     GetTotalBoutCount,
//     GetLastBountNumber,
//     GetMaxBountNumber,
//     GetTodayWatchVideoCounter,
//     CreateBannerAd,
//     CreateSmallBannerAd,
//     CreateCustomBannerAd,
//     BannerAdChangeSize,
//     ShowBannerAd,
//     HideBannerAd,
//     ChangeBannerStyle,
//     CreateRewardVideoAd,
//     ShowRewardVideoAd,
//     StatisticShareCard,
//     GetLayerList,
//     NavigateToMiniProgram,
//     CreateUserInfoButton,
//     ShowUserInfoButton,
//     HideUserInfoButton,
//     DestroyUserInfoButton,
//     CreateInterstitialAd,
//     ShowInterstitialAd,
//     GetSharingResults,
//     StatisticLayer,
//     IsCanRewardByVideoOrShare,
//     GetShareRewardLimit,
//     GetVideoRewardLimit,
//     GetServerInfo,
//     ExitGame,
//     VibrateShort,
//     VibrateLong,
//     InstallShortcut,
//     CreateNativeAd,
//     ShowNativeAd,
//     ClickNativeAd,
//     HasShortcutInstalled,
//     ShareAppMessage,
//     Hook,
//     OnFrontend,
//     OnBackend,
//     OnShow,
//     OnHide,
//     OnError,
//     GetObject,
//     GetString,
//     SetObject,
//     SetString,
//     Size,
//     DeleteObject,
//     StoreValue,
//     //----------------------------------------------------------------
//     //TT
//     ShareImage,
//     ShareVideo,
//     ShareTemplate,
//     ShareToken,
//     ShowMoreGamesModal,
//     CreateMoreGamesButton,
//     RecorderStart,
//     RecorderStop,
//     RecorderPause,
//     RecorderResume,
//     StatisticMoreGameOut,
//     //----------------------------------------------------------------
//     //WeChat
//     CreateGridAd,
//     ShowGridAd,
//     HideGridAd,
//     SubscribeSysMsg,
//     GetSetting,
//     WXRecorderOff,
//     WXRecorderOn,
//     WXRecorderStart,
//     WXRecorderStop,
//     WXRecorderPause,
//     WXRecorderResume,
//     WXRecorderAbort,
//     IsAtempoSupported,
//     IsFrameSupported,
//     IsSoundSupported,
//     IsVolumeSupported,
//     CreateGameRecorderShareButton,
//     GameRecorderShareButtonHide,
//     GameRecorderShareButtonShow,
//     GameRecorderShareButtonOnTap,
//     GameRecorderShareButtonOffTap,
//     SetMessageToFriendQuery,
//     OnShareMessageToFriend,
//     OnShareTimeline,
//     OffShareTimeline,
//     //----------------------------------------------------------------
//     //QQ
//     ShareByTemplateId,
//     AddColorSign,
//     SubscribeAppMsg,
//     CreateAddFriendButton,
//     ShowAddFriendButton,
//     HideAddFriendButton,
//     DestroyAddFriendButton,
//     CreateAppBox,
//     ShowAppBox,
//     CreateBlockAd,
//     ShowBlockAd,
//     HideBlockAd,
//     DestroyBlockAd,
//     //----------------------------------------------------------------
//     //MeiZu
//     GetNetworkType,
//     OnNetworkStatusChange,
//     //----------------------------------------------------------------
//     //Vivo
//     IsStartupByShortcut,
// }
// /**
//  * SDK_VARS
//  */
// export enum SV {
//     /**
//      * 登录凭证(有效期暂时为1天)
//      */
//     AccountPassYDHW = "AccountPassYDHW",
//     /**
//      * 当前体力总点数
//      */
//     TotalPowerCounter = "PowerCounter",
//     /**
//      * 最后一次增加体力时的时间戳
//      */
//     LastPowerAddTimestamp = "LastPowerAddTimestamp",
//     /**
//      * 倒计时进度
//      */
//     CountDownPro = "CountDownPro",
//     /**
//      * 是否正在倒计时
//      */
//     OnCountDown = "OnCountDown",
//     /**
//      * 今日看视频数量
//      */
//     TodayWatchVideoCounter = "TodayWatchVideoCounter",
//     /**
//      * 最后一次看视频的时间戳
//      */
//     LastWatchVideoTimestamp = "LastWatchVideoTimestamp",
//     /**
//      * 已解锁关卡数 列表
//      */
//     ListUnlockVideoLevelNumber = "ListUnlockLevelNumber",
//     /**
//      * 本地缓存已打点流失路径
//      */
//     ListStatisticsLayer = "ListStatisticsLayer",
//     ShareConfig = "ShareConfig",
//     /**
//      * 最大关卡编号
//      */
//     MaxCustomNumber = "MaxCustomNumber",
//     /**
//      * 最后一次玩的关卡编号
//      */
//     LastCustomNumber = "LastCustomNumber",
//     /**
//      * 今日晚的总关卡数量
//      */
//     TodayCustomNumberCounter = "TodayCustomNumberCounter",
//     /**
//      * 迄今为止总共关卡数量
//      */
//     TotalCustomNumberCounter = "TotalCustomNumberCounter",
//     /**
//      * 最后一次玩的时间
//      */
//     LastPlayTimestamp = "LastPlayTimestamp",
//     JumpOutInfo = "JumpOutInfo",
//     /**
//      * 视频上限
//      */
//     VideoFalseLimit = "VideoFalseLimit",
//     /**
//      * 分享上限
//      */
//     ShareFalseLimit = "ShareFalseLimit",
//     /**
//      * 平台用户信息
//      */
//     PlatformUserInfo = "PlatformUserInfo",
//     /**
//      * API统计批次ID
//      */
//     BatchID = "BatchID",
//     /**
//      * API统计信息列表
//      */
//     PendingApiCounterList = "PendingApiCounterList",
// }
/**
 * SDK_VARS
 */
var SV;
(function (SV) {
    /**
     * 登录凭证(有效期暂时为1天)
     * 全名：AccountPassYDHW
     */
    SV[SV["APYDHW"] = 30001] = "APYDHW";
    /**
     * 当前体力总点数
     * 全名：TotalPowerCounter
     */
    SV[SV["TPCounter"] = 30002] = "TPCounter";
    /**
     * 最后一次增加体力时的时间戳
     * 全名：LastPowerAddTimestamp
     */
    SV[SV["LPAddTstamp"] = 30003] = "LPAddTstamp";
    /**
     * 倒计时进度
     * 全名：CountDownPro
     */
    SV[SV["CDPro"] = 30004] = "CDPro";
    /**
     * 是否正在倒计时
     * 全名：OnCountDown
     */
    SV[SV["OnCD"] = 30005] = "OnCD";
    /**
     * 今日看视频数量
     * 全名：TodayWatchVideoCounter
     */
    SV[SV["TdWVCounter"] = 30006] = "TdWVCounter";
    /**
     * 最后一次看视频的时间戳
     * 全名：LastWatchVideoTimestamp
     */
    SV[SV["LWhVTstamp"] = 30007] = "LWhVTstamp";
    /**
     * 已解锁关卡数 列表
     * 全名：ListUnlockVideoLevelNumber
     */
    SV[SV["LUlVLNumber"] = 30008] = "LUlVLNumber";
    /**
     * 本地缓存已打点流失路径
     * 全名：ListStatisticsLayer
     */
    SV[SV["LStttLayer"] = 30009] = "LStttLayer";
    /**
     * 分享信息
     * 全名：ShareConfig
     */
    SV[SV["SConfig"] = 30010] = "SConfig";
    /**
     * 最大关卡编号
     * 全名：MaxCustomNumber
     */
    SV[SV["MCNumber"] = 30011] = "MCNumber";
    /**
     * 最后一次玩的关卡编号
     * 全名：LastCustomNumber
     */
    SV[SV["LCNumber"] = 30012] = "LCNumber";
    /**
     * 今日晚的总关卡数量
     * 全名：TodayCustomNumberCounter
     */
    SV[SV["TdCNCounter"] = 30013] = "TdCNCounter";
    /**
     * 迄今为止总共关卡数量
     * 全名：TotalCustomNumberCounter
     */
    SV[SV["TCNCounter"] = 30014] = "TCNCounter";
    /**
     * 最后一次玩的时间
     * 全名：LastPlayTimestamp
     */
    SV[SV["LPTstamp"] = 30015] = "LPTstamp";
    /**
     * 跳转信息
     * 全名：JumpOutInfo
     */
    SV[SV["JOInfo"] = 30016] = "JOInfo";
    /**
     * 视频上限
     * 全名：VideoFalseLimit
     */
    SV[SV["VFLimit"] = 30017] = "VFLimit";
    /**
     * 分享上限
     * 全名：ShareFalseLimit
     */
    SV[SV["SFLimit"] = 30018] = "SFLimit";
    /**
     * 平台用户信息
     * 全名：PlatformUserInfo
     */
    SV[SV["PfUInfo"] = 30019] = "PfUInfo";
    /**
     * API统计批次ID
     * 全名：BatchID
     */
    SV[SV["BatchID"] = 30020] = "BatchID";
    /**
     * API统计信息列表
     * 全名：PendingApiCounterList
     */
    SV[SV["PACList"] = 30021] = "PACList";
})(SV || (SV = {}));
/**
 * SDK_EVT
 */
// export enum SE {
//     /**
//      * 显示banner广告
//      */
//     ShowBanner = "显示banner广告",
//     /**
//      * 播放完banner广告后的回调
//      */
//     ShowBannerCallback = "播放完banner广告后的回调",
//     /**
//      * 检查桌面是否有图标
//      */
//     CheckIsHasDesktopIcon = "检查桌面是否有图标",
//     /**
//      * 看视频广告获取奖励
//      */
//     WatchVideoAdForReward = "看视频广告获取奖励",
//     /**
//      * 看视频获取金币
//      */
//     WatchVideoForCoin = "看视频获取金币",
//     /**
//      * 看视频获取3倍金币奖励
//      */
//     WatchVideoForThriceCoin = "看视频获取3倍金币奖励",
//     /**
//      * 看视频获得皮肤
//      */
//     WatchVideoForSkin = "看视频获得皮肤",
//     /**
//      * 看视频得抽奖券
//      */
//     WatchVideoForTicket = "WatchVideoForTicket",
//     /**
//      * 看视频获得离线奖励
//      */
//     WatchVideoForOfflineReward = "WatchVideoForOfflineReward",
//     /**
//      * 看视频复活重生恢复
//      */
//     WatchVideoForRevive = "WatchVideoForRevive",
//     /**
//      * 每三关看视频获取惊喜大礼包
//      */
//     WatchVideoForGiftPer3Level = "WatchVideoForGiftPer3Level",
//     /**
//      * 看视频开宝箱
//      */
//     WatchVideoAdForOpenTreasureBox = "WatchVideoAdForOpenTreasureBox",
//     OnFrontend = "OnFrontend",
//     OnBackend = "OnBackendNotification",
// }
var SE;
(function (SE) {
    //     /**
    //      * 显示banner广告
    //      */
    //     // ShowBanner,
    //     /**
    //      * 播放完banner广告后的回调
    //      */
    //     ShowBannerCallback = 20001,
    //     /**
    //      * 检查桌面是否有图标
    //      */
    //     CheckIsHasDesktopIcon = 20002,
    //     /**
    //      * 看视频广告获取奖励
    //      */
    //     WatchVideoAdForReward = 20003,
    //     /**
    //      * 看视频获取金币
    //      */
    //     WatchVideoForCoin = 20004,
    //     /**
    //      * 看视频获取3倍金币奖励
    //      */
    //     WatchVideoForThriceCoin = 20005,
    //     /**
    //      * 看视频获得皮肤
    //      */
    //     WatchVideoForSkin = 20006,
    //     /**
    //      * 看视频得抽奖券
    //      */
    //     WatchVideoForTicket = 20007,
    //     /**
    //      * 看视频获得离线奖励
    //      */
    //     WatchVideoForOfflineReward = 20008,
    //     /**
    //      * 看视频复活重生恢复
    //      */
    //     WatchVideoForRevive = 20009,
    //     /**
    //      * 每三关看视频获取惊喜大礼包
    //      */
    //     WatchVideoForGiftPer3Level = 20010,
    //     /**
    //      * 看视频开宝箱
    //      */
    //     WatchVideoAdForOpenTreasureBox = 20011,
    /**
     * onShow
     */
    SE[SE["OnFrontend"] = 20012] = "OnFrontend";
    /**
     * onHide
     */
    SE[SE["OnBackend"] = 20013] = "OnBackend";
})(SE || (SE = {}));
//EM_VIDEO_PLAY_TYPE
var VIDEO_P_T;
(function (VIDEO_P_T) {
    /**
     * 视频广告-播放失败
     */
    VIDEO_P_T[VIDEO_P_T["FAIL"] = 0] = "FAIL";
    /**
     * 视频广告-播放完成
     */
    VIDEO_P_T[VIDEO_P_T["FINISH"] = 1] = "FINISH";
    /**
     * 视频广告-播放取消
     */
    VIDEO_P_T[VIDEO_P_T["CANCEL"] = 2] = "CANCEL";
})(VIDEO_P_T || (VIDEO_P_T = {}));
//广告状态
var TP_AD;
(function (TP_AD) {
    /**
     * 创建
     */
    TP_AD[TP_AD["CREATE"] = 0] = "CREATE";
    /**
     * 加载成功
     */
    TP_AD[TP_AD["LOAD_SUCCESS"] = 1] = "LOAD_SUCCESS";
    /**
     * 加载失败
     */
    TP_AD[TP_AD["LOAD_FAIL"] = 2] = "LOAD_FAIL";
    /**
     * 点击(上报点击)
     */
    TP_AD[TP_AD["CLICK"] = 3] = "CLICK";
    /**
     * 展示
     */
    TP_AD[TP_AD["SHOW"] = 4] = "SHOW";
    /**
     * 关闭
     */
    TP_AD[TP_AD["CLOSE"] = 5] = "CLOSE";
    /**
     * 上报曝光
     */
    TP_AD[TP_AD["EXPOSURE"] = 6] = "EXPOSURE";
    /**
     * 播放取消
     */
    TP_AD[TP_AD["PLAY_CANCEL"] = 7] = "PLAY_CANCEL";
    /**
     * 播放完成
     */
    TP_AD[TP_AD["PLAY_FINISH"] = 8] = "PLAY_FINISH";
})(TP_AD || (TP_AD = {}));
//EM_SHARE_TYPE
var SHARE_T;
(function (SHARE_T) {
    /**
     * 无策略
     */
    SHARE_T[SHARE_T["None"] = 0] = "None";
    /**
     * 分享
     */
    SHARE_T[SHARE_T["Share"] = 1] = "Share";
    /**
     * 视频
     */
    SHARE_T[SHARE_T["Video"] = 2] = "Video";
})(SHARE_T || (SHARE_T = {}));
//EM_POWER_RECOVERY_TYPE
var P_R_T;
(function (P_R_T) {
    P_R_T[P_R_T["None"] = 0] = "None";
    /**
     * 看视频恢复体力
     */
    P_R_T[P_R_T["WatchVideo"] = 1] = "WatchVideo";
    /**
     * 定时恢复体力
     */
    P_R_T[P_R_T["AutoRecovery"] = 2] = "AutoRecovery";
    /**
     * 倒计时
     */
    P_R_T[P_R_T["CountDown"] = 3] = "CountDown";
})(P_R_T || (P_R_T = {}));
// export enum POWER_EVT {
//     AddPower = "AddPower",
// }
var POWER_EVT;
(function (POWER_EVT) {
    POWER_EVT[POWER_EVT["AddPower"] = 0] = "AddPower";
})(POWER_EVT || (POWER_EVT = {}));
// export enum TIMER_NAME {
//     /**
//      * 旋转装盘的定时器时间名
//      */
//     RotateTurnplate = "RotateTurnplate",
//     SaveLastTimestamp = "SaveLastTimestamp",
// }
var TIMER_NAME;
(function (TIMER_NAME) {
    /**
     * 旋转装盘的定时器时间名
     */
    TIMER_NAME["RotateTurnplate"] = "RotateTurnplate";
    TIMER_NAME["SaveLastTimestamp"] = "SaveLastTimestamp";
})(TIMER_NAME || (TIMER_NAME = {}));
var EM_APP_BOX_TYPE;
(function (EM_APP_BOX_TYPE) {
    /**
     * 盒子广告-创建成功
     */
    EM_APP_BOX_TYPE[EM_APP_BOX_TYPE["C_SUC"] = 0] = "C_SUC";
    /**
     * 盒子广告-创建失败
     */
    EM_APP_BOX_TYPE[EM_APP_BOX_TYPE["C_FAIL"] = 1] = "C_FAIL";
    /**
     * 盒子广告-关闭
     */
    EM_APP_BOX_TYPE[EM_APP_BOX_TYPE["CLOSE"] = 2] = "CLOSE";
})(EM_APP_BOX_TYPE || (EM_APP_BOX_TYPE = {}));
//EM_BLOCK_TYPE
var BLOCK_T;
(function (BLOCK_T) {
    /**
     * 盒子广告-创建成功
     */
    BLOCK_T[BLOCK_T["C_SUC"] = 0] = "C_SUC";
    /**
     * 盒子广告-创建失败
     */
    BLOCK_T[BLOCK_T["C_FAIL"] = 1] = "C_FAIL";
    /**
     * 盒子广告-监听积木广告尺寸大小事件
     */
    BLOCK_T[BLOCK_T["ONRESIZE"] = 2] = "ONRESIZE";
})(BLOCK_T || (BLOCK_T = {}));
//EM_SHARE_APP_TYPE
var SHARE_A_T;
(function (SHARE_A_T) {
    SHARE_A_T["QQ"] = "qq";
    SHARE_A_T["QQ_FAST_SHARE"] = "qqFastShare";
    SHARE_A_T["QQ_FAST_SHARE_LIST"] = "qqFastShareList";
    SHARE_A_T["QZONE"] = "qzone";
    SHARE_A_T["WECHARTFRIENDS"] = "wechatFriends";
    SHARE_A_T["WECHATMOMENT"] = "wechatMoment";
})(SHARE_A_T || (SHARE_A_T = {}));


/***/ }),

/***/ "./sdk_vivo/Main.ts":
/*!**************************!*\
  !*** ./sdk_vivo/Main.ts ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../sdk/SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../sdk/SDK/Enum */ "./sdk/SDK/Enum.ts");
/* harmony import */ var _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../fw/Utility/LogUtility */ "./fw/Utility/LogUtility.ts");
/* harmony import */ var _sdk_Commerce_VivoC__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sdk/Commerce/VivoC */ "./sdk/Commerce/VivoC.ts");
/* harmony import */ var _sdk_Mgr_CommerceMgr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../sdk/Mgr/CommerceMgr */ "./sdk/Mgr/CommerceMgr.ts");





class Main {
    constructor() {
        this.SDK_NAME = "ydhw_vivo";
        this.ALL_SDK_NAME = "ydhw";
        _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_2__["LogUtility"].IS_ENABLE_DEBUG = true;
        _fw_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_2__["LogUtility"].Me().P = _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].P();
        if (!_sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].P().Init()) {
            console.error("Init SDK failed!");
            return;
        }
        let platform = new _sdk_Commerce_VivoC__WEBPACK_IMPORTED_MODULE_3__["VivoP"]();
        platform.Init();
        let commerce = new _sdk_Commerce_VivoC__WEBPACK_IMPORTED_MODULE_3__["VivoC"]();
        commerce._P = platform;
        commerce.P = platform;
        if (window[this.SDK_NAME] != null) {
            _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Debug && console.error("已存在: " + this.SDK_NAME);
        }
        else if (commerce != null) {
            _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].L("Main constructor platform.Init():", _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].P().PfType);
            window[this.SDK_NAME] = commerce;
            window[this.ALL_SDK_NAME] = commerce;
        }
        else {
            _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Debug && console.error("不存在平台对应的SDK");
        }
        _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Domain().Domain(["http://test-api.ylxyx.cn", "https://api.ylxyx.cn"]).Manager("api/collect").Version("v1").Switch(1);
        _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Core()
            .Add(_sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__["MGR"].Commerce, commerce)
            .Add(_sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__["MGR"].Agent, new _sdk_Mgr_CommerceMgr__WEBPACK_IMPORTED_MODULE_4__["AgentMgr"]())
            .Add(_sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__["MGR"].Statistic, new _sdk_Mgr_CommerceMgr__WEBPACK_IMPORTED_MODULE_4__["StatMgr"]())
            .Add(_sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__["MGR"].Data, new _sdk_Mgr_CommerceMgr__WEBPACK_IMPORTED_MODULE_4__["DataMgr"]())
            .Add(_sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__["MGR"].Api, new _sdk_Mgr_CommerceMgr__WEBPACK_IMPORTED_MODULE_4__["ApiMgr"]())
            .Initialize();
        _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Core().Restore();
        // SDK.Commerce().Login(this, (isOk: boolean) => { });
    }
}
//激活启动类
new Main();


/***/ }),

/***/ "./src/Utils/TimeUtils.ts":
/*!********************************!*\
  !*** ./src/Utils/TimeUtils.ts ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TimeUtils; });
// 缓存变量
var outCode = 0, valCode = 0, timeout = {}, interval = {};
/**
  * 清除
  * @param data 缓存数据
  * @param key 标志
  */
var clear = function (data, key) {
    var info = data[key];
    if (info) {
        delete data[key];
        Laya.timer.clear(null, info);
    }
};
/**
 * 时间工具类
 * @author Yin
 */
class TimeUtils {
    constructor() {
    }
    /**
     * 数字保持两位数
     */
    static formatTen(num) {
        return (num > 9 ? '' : '0') + num;
    }
    /**
     * 小时-分-秒
     */
    static formatHour(second, sufix = ':') {
        var ten = TimeUtils.formatTen;
        var hour = second / 3600 | 0;
        var min = (second / 60 | 0) % 60;
        var sec = second % 60;
        return ten(hour) + sufix + ten(min) + sufix + ten(sec);
    }
    /**
     * 获取经过的天数
     * @param time 时间，单位毫秒
     */
    static getDay(time) {
        return (time / 3600000 + 8) / 24 | 0; // 时间零点是从早上8点开始的，因此加上8
    }
    /**
     * 检测两个毫秒数是否同一天
     */
    static isSameDay(time0, time1) {
        var get = TimeUtils.getDay;
        return get(time0) == get(time1);
    }
    /**
     * 模仿setTimeout
     * @param call
     * @param thisObj
     * @param delay
     */
    static setTimeout(call, thisObj, delay, ...param) {
        var curc = ++outCode;
        var func = timeout[curc] = function () {
            call.apply(thisObj, param);
            delete timeout[curc];
        };
        Laya.timer.once(delay, null, func);
        return curc;
    }
    /**
      * 清除延迟回调
      * @param key 标志
      */
    static clearTimeout(key) {
        clear(timeout, key);
    }
    ;
    /**
      * 设置间隔回调
      * @param call 回调函数
      * @param thisObj 回调所属对象
      * @param delay 回调间隔
      */
    static setInterval(call, thisObj, delay, ...param) {
        var curc = ++valCode;
        var func = interval[curc] = function () {
            call.apply(thisObj, param);
        };
        Laya.timer.loop(delay, null, func);
        return curc;
    }
    /**
      * 清除间隔回调
      * @param key
      */
    static clearInterval(key) {
        clear(interval, key);
    }
    ;
    static log(...params) {
        let str = TimeUtils.formatDate(new Date(), "[hh:mm:ss.SSS]");
        params.forEach(element => {
            str += " " + JSON.stringify(element);
        });
        console.log(str);
    }
    static formatDate(date, fmt) {
        let o = {
            "Y+": date.getFullYear(),
            "M+": date.getMonth() + 1,
            "d+": date.getDate(),
            "h+": date.getHours(),
            "m+": date.getMinutes(),
            "s+": date.getSeconds(),
            "S+": date.getMilliseconds() //毫秒   
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (let k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt;
    }
    static formatFullDate(date, fmt) {
        let o = {
            "Y+": date.getFullYear(),
            "M+": date.getMonth() + 1,
            "d+": date.getDate(),
            "h+": date.getHours(),
            "m+": date.getMinutes(),
            "s+": date.getSeconds(),
            "S+": date.getMilliseconds() //毫秒   
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (let k in o)
            if (new RegExp("(" + k + ")").test(fmt))
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
        return fmt = date.getFullYear() + fmt.substr(2, fmt.length);
    }
}


/***/ })

/******/ });
//# sourceMappingURL=ydhw.vivo.sdk.js.map