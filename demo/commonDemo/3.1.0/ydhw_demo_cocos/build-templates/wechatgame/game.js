// var YDHW_CONFIG = {
//     project: 'HelixJump',
//     platform: 'web',
//     env: 'dev',//online or dev
//     appid: "tt711307aeee39fe2d",
//     appkey: "30248858",
//     debug: true,
//     inspector: false,
//     engine: 'laya',
//     res_version: '20200310',
//     version: '1.0.0',
//     pkg_name: "",
//     libs: {},
//     resource_url: 'http://127.0.0.1:3100',
//     login_address: 'https://ydhwslb.szvi-bo.com',
//     banner_ad_unit_id_list: ['15926jnded313m55ms'],//侧边栏广告
//     interstitial_ad_unit_id_list: ['b5a2vkpi85b7f203df'],//插屏广告
//     spread_ad_unit_id_list: ['164281'],//开屏广告
//     native_ad_unit_id_list: ['164282'],//原生广告
//     video_ad_unit_id_list: ['6sn234aoqf3737fmu9'],//视频广告
//     grid_ad_unit_id_list: ['fsdfsdf'],//格子广告
//     tt_template_id_list: ["of6b158hdol2r5berq"],//TT 分享素材模板ID列表
//     scene_white_list: [1005, 1006, 1037, 1035],
//     interstitialAd_first_show_wait_time: 0,//插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
//     interstitialAd_show_time_interval: 0,//插屏广告-两次展示之间时间间隔（秒）
//     side_box_count: 20,//侧边栏列表item最小保留数(基于曝光策略)
// };
// window.YDHW_CONFIG = YDHW_CONFIG;

// require("./tt/ydhw.tt.sdk.js");

var YDHW_CONFIG = {
    project: 'HelixJump',
    platform:"wechat",
    env: 'dev',//online or dev
    appid: "wxd42b44dbd686d382",//
    appkey:"",
    debug: true,
    inspector: false,
    engine: 'laya',
    res_version: '20200310',
    version: '1.0.0',
    pkg_name:"wxd42b44dbd686d382",
    libs: {},
    resource_url: 'http://127.0.0.1:3100',
    login_address: 'https://ydhwslb.szvi-bo.com',
    banner_ad_unit_id_list: ['adunit-723102dee1e8ddc3'],//Banner广告
    interstitial_ad_unit_id_list: ['adunit-fe07eedeb0218f5c'],//插屏广告
    spread_ad_unit_id_list: [],//开屏广告
    native_ad_unit_id_list: [],//原生广告
    video_ad_unit_id_list: ['adunit-546eabc2b53c133e'],//视频广告
    grid_ad_unit_id_list: ['adunit-8b60da8bb5f96948'],//格子广告
    tt_template_id_list: [],//TT 分享素材模板ID列表
    scene_white_list: [1005, 1006, 1037, 1035],
    interstitialAd_first_show_wait_time: 10,//插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
    interstitialAd_show_time_interval: 10,//插屏广告-两次展示之间时间间隔（秒）
    side_box_count: 20,//侧边栏列表item最小保留数(基于曝光策略)
};

window.YDHW_CONFIG = YDHW_CONFIG;

require("./wx/ydhw.wx.sdk.js");


require('libs/weapp-adapter/index');
var Parser = require('libs/xmldom/dom-parser');
window.DOMParser = Parser.DOMParser;
require('libs/wx-downloader.js');
require('src/settings');
var settings = window._CCSettings;
require('main');
require(settings.debug ? 'cocos2d-js.js' : 'cocos2d-js-min.js');
require('./libs/engine/index.js');

wxDownloader.REMOTE_SERVER_ROOT = "";
wxDownloader.SUBCONTEXT_ROOT = "";
var pipeBeforeDownloader = cc.loader.md5Pipe || cc.loader.assetLoader;
cc.loader.insertPipeAfter(pipeBeforeDownloader, wxDownloader);

if (cc.sys.browserType === cc.sys.BROWSER_TYPE_WECHAT_GAME_SUB) {
    require('./libs/sub-context-adapter');
}
else {
    // Release Image objects after uploaded gl texture
    cc.macro.CLEANUP_IMAGE_CACHE = true;
}

window.boot();