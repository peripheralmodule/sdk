/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./sdk_tt/Main.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./framework/Assist/Http.ts":
/*!**********************************!*\
  !*** ./framework/Assist/Http.ts ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return Http; });
/* harmony import */ var _sdk_Assist_Log__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../sdk/Assist/Log */ "./sdk/Assist/Log.ts");

class Http {
    static Request(platform, url, retryCount = 1, isDebug = false) {
        let xhr = new XMLHttpRequest();
        if (isDebug) {
            isDebug && _sdk_Assist_Log__WEBPACK_IMPORTED_MODULE_0__["Log"].Debug("http url:" + url);
        }
        let urlString = url.Value();
        if (platform.IsVivo || platform.IsQGMiniGame || platform.IsQQMiniGame) {
            urlString = url.EncodeURI();
        }
        if (platform.IsOnMobile) {
            xhr.open(url.Method(), urlString);
        }
        else {
            xhr.open(url.Method(), urlString, url.IsAsync());
        }
        let header = url.Header();
        for (const key in header) {
            xhr.setRequestHeader(key, header[key]);
        }
        xhr.onerror = function (e) {
            if (retryCount > 1) {
                retryCount--;
                Http.Request(platform, url, retryCount, isDebug);
            }
            else {
                isDebug && _sdk_Assist_Log__WEBPACK_IMPORTED_MODULE_0__["Log"].Error("Http OnError:" + e);
                url.InvokeError(e + "");
            }
        };
        xhr.onabort = function (e) {
            url.InvokeException(e);
        };
        xhr.onprogress = function (e) {
        };
        xhr.onreadystatechange = function () {
            if (this.status === 200) {
            }
            else {
                isDebug && _sdk_Assist_Log__WEBPACK_IMPORTED_MODULE_0__["Log"].Debug("http request status:" + this.status);
            }
        };
        xhr.onload = function () {
            try {
                // console.warn("YDHW Response-"+url+"-data:",this.responseText);
                let response = JSON.parse(this.responseText);
                url.InvokeReceive(response);
            }
            catch (error) {
                console.error("xhr.onload error:" + error);
            }
        };
        try {
            let content = JSON.stringify(url.Data());
            // console.warn("YDHW Request-"+url+"-data:",content);
            if (platform.IsOnMobile) {
                xhr.send(content);
            }
            else {
                // xhr.send(url.Data());
                xhr.send(content);
            }
        }
        catch (e) {
            url.InvokeException(e);
        }
    }
}


/***/ }),

/***/ "./framework/Assist/StorageAdapter.ts":
/*!********************************************!*\
  !*** ./framework/Assist/StorageAdapter.ts ***!
  \********************************************/
/*! exports provided: StorageAdapter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageAdapter", function() { return StorageAdapter; });
/* harmony import */ var _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Log/FrameworkLog */ "./framework/Log/FrameworkLog.ts");

class StorageAdapter {
    static set WriteString(method) {
        this._StringWriter = method;
    }
    static get WriteString() {
        if (this._StringWriter == null) {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_0__["FrameworkLog"].Error("Adapter.WriteString is not set");
            return (key, value) => { };
        }
        return this._StringWriter;
    }
    static set WriteObject(method) {
        this._ObjectWriter = method;
    }
    static get WriteObject() {
        if (this._ObjectWriter == null) {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_0__["FrameworkLog"].Error("Adapter.WriteObject is not set");
            return (key, data) => { };
        }
        return this._ObjectWriter;
    }
    static set ReadString(method) {
        this._StringReader = method;
    }
    static get ReadString() {
        if (this._StringReader == null) {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_0__["FrameworkLog"].Error("Adapter.ReadObject is not set");
            return (key) => { return ""; };
        }
        return this._StringReader;
    }
    static set ReadObject(method) {
        this._ObjectReader = method;
    }
    static get ReadObject() {
        if (this._ObjectReader == null) {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_0__["FrameworkLog"].Error("Adapter.ReadObject is not set");
            return (key) => { return {}; };
        }
        return this._ObjectReader;
    }
    static set DeleteObject(method) {
        this._ObjectDeleter = method;
    }
    ;
    static get DeleteObject() {
        if (this._ObjectDeleter == null) {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_0__["FrameworkLog"].Error("Adapter.DeleteObject is not set");
            return (key) => { };
        }
        return this._ObjectDeleter;
    }
}
StorageAdapter.SDK_CONFIG = "YDHW_CONFIG";
StorageAdapter._StringWriter = null;
StorageAdapter._ObjectWriter = null;
StorageAdapter._StringReader = null;
StorageAdapter._ObjectReader = null;
StorageAdapter._ObjectDeleter = null;


/***/ }),

/***/ "./framework/Core.ts":
/*!***************************!*\
  !*** ./framework/Core.ts ***!
  \***************************/
/*! exports provided: Method, Wrapper, Variable, VariableContainer, MethodContainer, Core, BaseManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Method", function() { return Method; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Wrapper", function() { return Wrapper; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Variable", function() { return Variable; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VariableContainer", function() { return VariableContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MethodContainer", function() { return MethodContainer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Core", function() { return Core; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BaseManager", function() { return BaseManager; });
/* harmony import */ var _Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Assist/StorageAdapter */ "./framework/Assist/StorageAdapter.ts");
/* harmony import */ var _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Log/FrameworkLog */ "./framework/Log/FrameworkLog.ts");


class Method {
    constructor(caller, method, owner) {
        this.caller = caller;
        this.method = method;
        this._Owner = owner;
    }
    Log() {
        let tmpString = "";
        tmpString += "/ncaller:" + this.caller.name;
        tmpString += "/nmethod:" + this.method.name;
        if (this.args) {
            for (let i = 0; i < this.args.length; i++) {
                let arg = this.args[i];
                tmpString += `/r/n arg[${i}]=${arg.name}`;
            }
        }
        if (this._Owner) {
            tmpString += "/n Owner:" + this._Owner;
        }
        _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FrameworkLog"].Debug(tmpString);
    }
    Destroy() {
        this.caller = null;
        this.method = null;
    }
    // Invoke(...args: any[]): void {
    // }
    Bind(caller, binding, ...args) {
        this.caller = caller;
        this.method = binding;
        this.args = args;
        return this;
    }
}
//Wrapper----------------------------------------------------------------------------
class Wrapper extends Array {
    constructor(capacity = 64) {
        super();
        this._Size = 0;
        this.length = capacity;
    }
    RemoveAt(index) {
        const e = this[index]; // make copy of element to remove so it can be returned
        this[index] = this[--this._Size]; // overwrite item to remove with last element
        this[this._Size] = null; // null last element, so gc can do its work
        return e;
    }
    Remove(e) {
        let i;
        let e2;
        const size = this._Size;
        for (i = 0; i < size; i++) {
            e2 = this[i];
            if (e == e2) {
                this[i] = this[--this._Size]; // overwrite item to remove with last element
                this[this._Size] = null; // null last element, so gc can do its work
                return true;
            }
        }
        return false;
    }
    RemoveLast() {
        if (this._Size > 0) {
            const e = this[--this._Size];
            this[this._Size] = null;
            return e;
        }
        return null;
    }
    Contains(e) {
        let i;
        let size;
        for (i = 0, size = this._Size; size > i; i++) {
            if (e === this[i]) {
                return true;
            }
        }
        return false;
    }
    RemoveAll(bag) {
        let modified = false;
        let i;
        let j;
        let l;
        let e1;
        let e2;
        for (i = 0, l = bag.Size(); i < l; i++) {
            e1 = bag.Get(i);
            for (j = 0; j < this._Size; j++) {
                e2 = this[j];
                if (e1 === e2) {
                    this.RemoveAt(j);
                    j--;
                    modified = true;
                    break;
                }
            }
        }
        return modified;
    }
    Get(index) {
        if (index >= this.length) {
            throw new Error('ArrayIndexOutOfBoundsException');
        }
        return this[index];
    }
    SafeGet(index) {
        if (index >= this.length) {
            this.Grow((index * 7) / 4 + 1);
        }
        return this[index];
    }
    Size() {
        return this._Size;
    }
    GetCapacity() {
        return this.length;
    }
    IsIndexWithinBounds(index) {
        return index < this.GetCapacity();
    }
    IsEmpty() {
        return this._Size == 0;
    }
    Add(e) {
        // is size greater than capacity increase capacity
        if (this._Size === this.length) {
            this.Grow();
        }
        this[this._Size++] = e;
    }
    Set(index, e) {
        if (index >= this.length) {
            this.Grow(index * 2);
        }
        this._Size = index + 1;
        this[index] = e;
    }
    Grow(newCapacity = ~~((this.length * 3) / 2) + 1) {
        this.length = ~~newCapacity;
    }
    EnsureCapacity(index) {
        if (index >= this.length) {
            this.Grow(index * 2);
        }
    }
    Clear() {
        let i;
        let size;
        // null all elements so gc can clean up
        for (i = 0, size = this._Size; i < size; i++) {
            this[i] = null;
        }
        this._Size = 0;
    }
    AddAll(items) {
        let i;
        for (i = 0; items.Size() > i; i++) {
            this.Add(items.Get(i));
        }
    }
}
//Variable----------------------------------------------------------------------------
class Variable {
    constructor(varId, owner, value) {
        this._IsStore = false;
        this._ListCaller = new Wrapper();
        this._ListBindingEvent = new Wrapper();
        this._ListArgs = new Wrapper();
        this._Owner = owner;
        this._Name = varId;
        this._Value = value;
        this._Type = typeof (value);
        this._ListCaller = new Wrapper();
        this._ListBindingEvent = new Wrapper();
        this._ListArgs = new Wrapper();
    }
    Log() {
        let tmpString = "";
        if (this._Owner) {
            tmpString += "Owner: " + this._Owner;
        }
        tmpString += "\nType: " + this._Type;
        tmpString += "\nVariableId: " + this._Name;
        tmpString += "\nValue: " + this._Value;
        tmpString += "\n";
        _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FrameworkLog"].Debug(tmpString);
    }
    SetOwner(owner) {
        this._Owner = owner;
        return this;
    }
    Owner() {
        return this._Owner;
    }
    Store() {
        this._IsStore = true;
        return this;
    }
    IsStore() {
        return this._IsStore;
    }
    Data() {
        let data = {
            manager: this.Owner,
        };
    }
    Revert(value) {
        this._Value = value;
        this._Type = typeof (value);
    }
    Name() {
        return this._Name;
    }
    SetValue(value) {
        let oldValue = this._Value;
        this._Value = value;
        this._Type = typeof (value);
        this._EmitValueChanged(oldValue);
        /*todo:optimize
        if (this._Value !== value) {
            let oldValue = this._Value;
            this._Value = value;
            this._Type = typeof (value);
            this._EmitValueChanged(this.caller, oldValue);
        }
        */
        return this;
    }
    Value() {
        return this._Value;
    }
    Type() {
        return this._Type;
    }
    Bind(caller, bindingEvent, ...args) {
        // this._ListCaller.Remove(caller);
        this._ListCaller.Add(caller);
        // this._ListBindingEvent.Remove(bindingEvent);
        this._ListBindingEvent.Add(bindingEvent);
        this._ListArgs.Add(args);
        // if (args && args.length > 0) {
        //     this._ListArgs.Remove(args);
        //     this._ListArgs.Add(args);
        // }
    }
    Assign() {
        this._EmitValueChanged(this._Value);
    }
    Unbind(caller, bindingEvent) {
        this._ListCaller.Remove(caller);
        this._ListBindingEvent.Remove(bindingEvent);
    }
    _EmitValueChanged(oldValue) {
        for (let i = 0, count = this._ListBindingEvent.Size(); i < count; i++) {
            let caller = this._ListCaller[i];
            let event = this._ListBindingEvent[i];
            let args = this._ListArgs[i];
            let tmpList = [];
            tmpList.push(this._Value);
            for (let index in args) {
                tmpList.push(args[index]);
            }
            event.apply(caller, tmpList);
            // event.call(caller, this._Value, args);
        }
    }
}
//VariableContainer----------------------------------------------------------------------------
class VariableContainer {
    constructor() {
        this._DictVariable = {}; //用对象属性来作为Dictionary的使用
        /**
         * 默认为还未恢复过数据，如果触发过恢复数据后则为true，后面就不再二次恢复。
         */
        this._IsRestore = false;
    }
    CheckAllVariables() {
        _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FrameworkLog"].Debug("Check all variables");
        _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FrameworkLog"].Debug("---------------------------------------------");
        let tmpDict = this._DictVariable;
        for (let key in tmpDict) {
            let variable = tmpDict[key];
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FrameworkLog"].Debug("Variable:" + key);
            variable.Log();
        }
        _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FrameworkLog"].Debug("---------------------------------------------");
    }
    _GetVariable(varId, owner) {
        let tmpDict = this._DictVariable;
        let ppt = tmpDict[varId];
        if (ppt == undefined || ppt == null) {
            // if (ppt == null) {
            //     //todo:暂时待定
            //     //console.warn("1 不存在属性变量:" + varId);
            // } else {
            //     tmpDict[varId] = ppt;
            // }
            return null;
        }
        return ppt;
    }
    GetVariable(varId, owner) {
        let tmpDict = this._DictVariable;
        let ppt = tmpDict[varId];
        if (ppt == null) {
            let variable = new Variable(varId, owner, null);
            ppt = this.RevertVariable(variable);
            if (ppt == null) {
                //todo:暂时待定
                // console.warn("2 不存在属性变量:" + varId);
            }
            else {
                tmpDict[varId] = ppt;
            }
        }
        return ppt;
    }
    ModifyVariable(variable, value) {
        if (variable != null) {
            variable.SetValue(value);
            if (variable.IsStore()) {
                this.SaveVariable(variable.Name());
            }
        }
        else {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FrameworkLog"].Error("修改的变量不存在");
        }
    }
    DeclareVariable(varId, owner, value) {
        if (varId == null || varId.toString().length == 0) {
            //todo:error
            return null;
        }
        else {
            let ppt = this._GetVariable(varId);
            if (ppt != null) {
                ppt.SetValue(value);
            }
            else {
                ppt = new Variable(varId, owner, value);
                this._DictVariable[varId] = ppt;
            }
            return ppt;
        }
    }
    RemoveVariable(varId) {
        let tmpDict = this._DictVariable;
        let ppt = tmpDict[varId];
        if (ppt != null) {
            delete tmpDict[varId]; //删除对象属性
            _Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_0__["StorageAdapter"].DeleteObject(varId);
            // Laya.LocalStorage.removeItem(varId);
        }
        else {
            //todo:warn
        }
    }
    _GetVariableValue(varId) {
        let ppt = this.GetVariable(varId);
        return ppt == null ? null : ppt.Value();
    }
    GetValueT(varId, defaultValue) {
        let value = this._GetVariableValue(varId);
        if (value == null) {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FrameworkLog"].Error(`找不到对应的变量名:${varId}`);
            if (typeof defaultValue == "undefined") {
                return null;
            }
            return defaultValue;
        }
        else {
            return value;
        }
    }
    SetValueT(varId, value) {
        let ppt = this._GetVariable(varId);
        if (ppt != null) {
            this.ModifyVariable(ppt, value);
        }
        else {
            console.error("修改的变量不存在：" + varId);
        }
    }
    AddValueT(varId, value) {
        let ppt = this._GetVariable(varId);
        if (ppt != null) {
            if (typeof value == "number") {
                let tmp = ppt.Value() + Number(value);
                this.ModifyVariable(ppt, tmp);
                return ppt.Value();
            }
            else if (typeof value == "string") {
                let tmp = ppt.Value() + String(value);
                this.ModifyVariable(ppt, tmp);
                return ppt.Value();
            }
            else {
                console.error(`当前数据类型暂不支持"+"运算符操作`);
            }
        }
        else {
            console.error(`"+" 变量不存在：${varId}`);
        }
        return null;
    }
    SubValueT(varId, value) {
        let ppt = this._GetVariable(varId);
        if (ppt != null) {
            if (typeof value == "number") {
                let tmp = ppt.Value() + Number(value);
                this.ModifyVariable(ppt, tmp);
                return ppt.Value();
            }
            else if (typeof value == "string") {
                let tmp = ppt.Value() + String(value);
                this.ModifyVariable(ppt, tmp);
                return ppt.Value();
            }
            else {
                console.error(`当前数据类型暂不支持"+"运算符操作`);
            }
        }
        else {
            console.error(`"-" 变量不存在：${varId}`);
        }
        return null;
    }
    Bind(varId, caller, callback, ...args) {
        let ppt = this._GetVariable(varId);
        if (ppt == null) {
            //todo:error
            return false;
        }
        else {
            ppt.Bind(caller, callback, args);
            return true;
        }
    }
    Unbind(varId, caller, callback) {
        let ppt = this._GetVariable(varId);
        if (ppt == null) {
            //todo:error
            return false;
        }
        else {
            ppt.Unbind(caller, callback);
            return true;
        }
    }
    RevertVariable(variable) {
        let varId = variable.Name();
        let data = _Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_0__["StorageAdapter"].ReadObject(varId);
        if (!data) {
            _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FrameworkLog"].Warn("本地数据不存在字段:" + varId);
            return null;
        }
        // console.log("typeof(data)=" + typeof (data));
        if (typeof (data) == "object") {
            let name = data.name;
            let type = data.type;
            if (typeof (type) == "undefined") {
                console.error("1 数据类型不存在!");
                return null;
            }
            let value = data.value;
            if (typeof (value) == "undefined") {
                console.error("2 数据类型不存在!");
                return null;
            }
            let isStore = data.is_store;
            if (typeof (isStore) == "undefined") {
                console.error("3 数据类型不存在!");
                return null;
            }
            let owner = data.owner;
            if (typeof (owner) == "undefined") {
                console.error("4 数据类型不存在!");
                return null;
            }
            if (type == "string") {
                variable.Revert(String(value));
            }
            else if (type == "number") {
                variable.Revert(Number(value));
            }
            else if (type == "boolean") {
                variable.Revert(Boolean(value));
            }
            else if (type == "object") {
                variable.Revert(Object(value));
            }
            else {
                console.error("数据类型不存在!");
                return null;
            }
            variable.Store();
            variable.SetOwner(owner);
        }
        return variable;
    }
    RevertData() {
        // for (let i = 0, l = vars.length; i < l; i++) {
        //     let data = vars[i];
        //     let name = data.name as string;
        //     let type = data.type as string;
        //     let value = data.value as any;
        //     let isStore = data.is_store as boolean;
        //     let owner = data.owner as string;
        //     if (name in this._DictVariable) {
        //         let ppt = this._DictVariable[name] as IVariable;
        //         if (type == "string") {
        //             ppt.SetValue(String(value));
        //         } else if (type == "number") {
        //             ppt.SetValue(Number(value));
        //         } else if (type == "boolean") {
        //             ppt.SetValue(Boolean(value));
        //         }
        //         ppt.Store();
        //         ppt.SetOwner(owner)
        //     } else {
        //         //TODO:捆版各个变量会各个caller，目前没有需求
        //     }
        // }
        if (this._IsRestore) {
            return;
        }
        let tmpDict = this._DictVariable;
        for (let name in tmpDict) {
            let variable = tmpDict[name];
            if (!variable.IsStore()) {
                continue;
            }
            this.RevertVariable(variable);
            // let data = Adapter.Read(name);
            // if (!data) {
            //     continue;
            // }
            // // console.log("typeof(data)=" + typeof (data));
            // if (typeof (data) == "object") {
            //     let name = data.name as string;
            //     let type = data.type as string;
            //     if (typeof (type) == "undefined") {
            //         console.error("1 数据类型不存在!");
            //         return;
            //     }
            //     let value = data.value as any;
            //     if (typeof (value) == "undefined") {
            //         console.error("2 数据类型不存在!");
            //         return;
            //     }
            //     let isStore = data.is_store as boolean;
            //     if (typeof (isStore) == "undefined") {
            //         console.error("3 数据类型不存在!");
            //         return;
            //     }
            //     let owner = data.owner as string;
            //     if (typeof (owner) == "undefined") {
            //         console.error("4 数据类型不存在!");
            //         return;
            //     }
            //     if (type == "string") {
            //         variable.Revert(String(value));
            //     } else if (type == "number") {
            //         variable.Revert(Number(value));
            //     } else if (type == "boolean") {
            //         variable.Revert(Boolean(value));
            //     } else if (type == "object") {
            //         variable.Revert(Object(value));
            //     } else {
            //         console.error("数据类型不存在!");
            //         return;
            //     }
            //     variable.Store();
            //     variable.SetOwner(owner)
            // }
        }
        this._IsRestore = true;
    }
    SaveData(name) {
        let tmpDict = this._DictVariable;
        let vars = [];
        for (let key in tmpDict) {
            let variable = tmpDict[key];
            // if (variable.Owner() == name && variable.IsStore()) {
            //     vars.push({
            //         name: key,
            //         value: variable.Value(),
            //         type: variable.Type(),
            //         is_store: variable.IsStore(),
            //         owner: variable.Owner(),
            //     })
            // }
            if (variable.Owner() == name && variable.IsStore()) {
                _Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_0__["StorageAdapter"].WriteObject(key, {
                    name: key,
                    value: variable.Value(),
                    type: variable.Type(),
                    is_store: variable.IsStore(),
                    owner: variable.Owner(),
                });
                // Laya.LocalStorage.setJSON(key, {
                //     name: key,
                //     value: variable.Value<any>(),
                //     type: variable.Type(),
                //     is_store: variable.IsStore(),
                //     owner: variable.Owner(),
                // });
            }
        }
        return vars;
    }
    SaveAllData() {
        let tmpDict = this._DictVariable;
        let vars = [];
        for (let key in tmpDict) {
            this.SaveVariable(key);
        }
    }
    SaveVariable(name) {
        let tmpDict = this._DictVariable;
        let variable = tmpDict[name];
        if (variable.IsStore()) {
            _Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_0__["StorageAdapter"].WriteObject(name, {
                name: name,
                value: variable.Value(),
                type: variable.Type(),
                is_store: variable.IsStore(),
                owner: variable.Owner(),
            });
            // Laya.LocalStorage.setJSON(name, {
            //     name: name,
            //     value: variable.Value<any>(),
            //     type: variable.Type(),
            //     is_store: variable.IsStore(),
            //     owner: variable.Owner(),
            // });
        }
    }
}
//MethodContainer----------------------------------------------------------------------------
class MethodContainer {
    constructor() {
        this._DictNotify = {};
        this._DictMethod = {};
    }
    CheckAllMethods() {
        console.log("Check all notify");
        console.log("---------------------------------------------");
        for (let key in this._DictNotify) {
            let notifies = this._DictNotify[key];
            console.log(`Notify: ${key}`);
            for (let i = 0; i < notifies.length; i++) {
                let notify = notifies[i];
                notify.Log();
            }
        }
        console.log("Check all method");
        console.log("---------------------------------------------");
        for (let key in this._DictMethod) {
            let method = this._DictMethod[key];
            console.log(`Mehtod: ${key}`);
            method.Log();
        }
        console.log("---------------------------------------------");
    }
    /**
     * 注册普通事件，可以获取返回值
     * @param methodId
     * @param caller
     * @param event
     */
    DeclareMethod(methodId, caller, owner, event) {
        let tmpDict = this._DictMethod;
        let method = tmpDict[methodId];
        if (method == null) {
            tmpDict[methodId] = new Method(caller, event, owner);
        }
        else {
            console.error(methodId + " 已经被注册过了");
        }
        return method;
    }
    /**
     * 注册广播事件，无返回值
     * @param methodId
     * @param caller
     * @param event
     */
    DeclareNotify(methodId, caller, owner, event) {
        let tmpDict = this._DictNotify;
        let tmpList = tmpDict[methodId];
        if (tmpList == null) {
            tmpDict[methodId] = [];
            tmpDict[methodId].push(new Method(caller, event, owner));
        }
        else {
            tmpDict[methodId].push(new Method(caller, event, owner));
        }
    }
    // public UnregisterEvent(methodId: SINGLETON_METHOD, caller: any, method: IMethod): void {
    //     let tmpList: MethodHandler[] = this._DictEvent[methodId];
    //     if (tmpList == null) {
    //         //todo:warn
    //     } else if (tmpList.length <= 0) {
    //         delete this._DictEvent[methodId];//删除
    //     } else {
    //         tmpList = tmpList.filter(item => item != new MethodHandler(caller, method));
    //         this._DictEvent[methodId] = tmpList;
    //     }
    // }
    /**
     * 广播通知对应的事件
     * @param methodId
     * @param arg
     */
    InvokeNotify(methodId, arg) {
        let tmpDict = this._DictNotify;
        let tmpList = tmpDict[methodId];
        if (!tmpList || !tmpList.length)
            return;
        for (let i = 0, count = tmpList.length; i < count; i++) {
            let method = tmpList[i];
            method.method.apply(method.caller, arg);
        }
    }
    /**
     *
     * @param methodId 调用指定事件类型，获取返回参数
     * @param arg 具有返回值
     */
    InvokeMethod(methodId, arg) {
        let tmpDict = this._DictMethod;
        let method = tmpDict[methodId];
        if (method == null) {
            console.error("事件没注册：" + methodId);
            return;
        }
        return method.method.apply(method.caller, arg);
    }
    // public ClearAllEvent(): void {
    //     for (let key in this._DictEvent) {
    //         let eventList: MethodHandler[] = this._DictEvent[key];
    //         for (let i = 0, count = eventList.length; i < count; i++) {
    //             let method = eventList[i];
    //             method.Destroy();
    //             delete eventList[i];
    //         }
    //     }
    //     delete this._DictEvent;
    // }
    ClearAllMethod() {
        let tmpDict = this._DictMethod;
        for (let key in tmpDict) {
            let method = tmpDict[key];
            method.Destroy();
            delete tmpDict[key];
        }
        delete this._DictMethod;
    }
}
//Core---------------------------------------------------------------------------------------------
function as(obj, method1) {
    return method1 in obj ? obj : null;
}
class Core {
    constructor() {
        this.VariableContainer = new VariableContainer();
        this.MethodContainer = new MethodContainer();
        this._ListInitialize = {};
        this._ListExecute = {};
        this._ListAll = {};
        /**
         * 从游戏开始运行到当前帧率
         * */
        this._CurFrameIndex = 0;
        this._ListInitialize = {};
        this._ListExecute = {};
        this._ListAll = {};
        this._Manager = new BaseManager();
        this._Manager.SetCore(this);
    }
    static get Instance() {
        if (Core._Instance == null) {
            Core._Instance = new Core();
        }
        return Core._Instance;
    }
    static SetCore(mgr, core) {
        const method = as(mgr, 'SetCore');
        if (method != null) {
            method.SetCore(core);
        }
    }
    static SetName(mgr, name) {
        const method = as(mgr, 'SetName');
        if (method != null) {
            method.SetName(name);
        }
    }
    GetVariableContainer() {
        return this.VariableContainer;
    }
    GetMethodContainer() {
        return this.MethodContainer;
    }
    AddManager(managerType, manager) {
        _Log_FrameworkLog__WEBPACK_IMPORTED_MODULE_1__["FrameworkLog"].Log("Add Manager: " + managerType);
        if ('function' === typeof manager) {
            const Klass = manager;
            manager = new Klass();
        }
        Core.SetCore(manager, this);
        Core.SetName(manager, managerType);
        const initializeManager = as(manager, 'Initialize');
        if (initializeManager != null) {
            const _initializeManagers = this._ListInitialize;
            _initializeManagers[managerType] = initializeManager;
        }
        const executeManager = as(manager, 'Execute');
        if (executeManager != null) {
            const _executeManagers = this._ListExecute;
            _executeManagers[managerType] = executeManager;
        }
        const _allManagers = this._ListAll;
        _allManagers[managerType] = manager;
        return this;
    }
    AsManager() {
        return this._Manager;
    }
    GetManager(mgrType) {
        return this._ListAll[mgrType];
    }
    Initialize() {
        const tmpDict = this._ListInitialize;
        for (let key in tmpDict) {
            let tmpValue = tmpDict[key];
            tmpValue && tmpValue.Initialize();
        }
    }
    Execute() {
        this._CurFrameIndex++;
        const tmpDict = this._ListExecute;
        for (let key in tmpDict) {
            let tmpValue = tmpDict[key];
            tmpValue && tmpValue.Execute(this._CurFrameIndex);
        }
    }
    /**
     * 从本地还原数据
     */
    Restore() {
        const tmpList = this._ListAll;
        for (let key in tmpList) {
            let tmpValue = tmpList[key];
            tmpValue.Restore();
        }
    }
    /**
     * 保存数据到本地
     */
    Save() {
        const tmpList = this._ListAll;
        for (let key in tmpList) {
            let tmpValue = tmpList[key];
            tmpValue.Save();
        }
    }
}
Core._Instance = null;
//Manager----------------------------------------------------------------------------
class BaseManager {
    constructor() {
        this.Name = "Gauntlet";
        this._Core = null;
        this.VariableContainer = null;
        this.MethodContainer = null;
    }
    SetName(name) {
        this.Name = name;
    }
    SetCore(core) {
        this._Core = core;
        this.VariableContainer = this._Core.GetVariableContainer();
        this.MethodContainer = this._Core.GetMethodContainer();
    }
    /**
     * 根据方法ID注册事件，一个ID侦听一个事件
     * @param methodId
     * @param binding
     */
    DeclareMethod(methodId, caller, binding) {
        return this.MethodContainer.DeclareMethod(methodId, caller, this.Name, binding);
    }
    /**
     * 根据方法ID注册事件：一个ID侦听多个不同事件
     * @param methodId
     * @param binding
     */
    DeclareNotify(methodId, caller, binding) {
        this.MethodContainer.DeclareNotify(methodId, caller, this.Name, binding);
    }
    /**
     * 根据方法ID执行对应函数：一个ID触发绑定注册的唯一一个事件
     * @param methodId
     * @param args
     */
    InvokeMethod(methodId, ...args) {
        return this.MethodContainer.InvokeMethod(methodId, args);
    }
    /**
     * 根据方法ID执行对应函数：一个ID触发绑定注册的一个或多个事件
     * @param methodId
     * @param args
     */
    InvokeNotify(methodId, ...args) {
        this.MethodContainer.InvokeNotify(methodId, args);
    }
    DeclareVariable(varId, arg) {
        return this.VariableContainer.DeclareVariable(varId, this.Name, arg);
    }
    /**
     * 获取变量，先在当前内存查找，如果不存在，
     * 则会到本地存储数据里面读取，反序列化后再查找匹对，
     * 如果存在则放入内存种并返回，还不存在则返回null，
     * @param varId
     */
    GetVariable(varId) {
        return this.VariableContainer.GetVariable(varId, this.Name);
    }
    GetValueT(varId, defaultValue) {
        return this.VariableContainer.GetValueT(varId, defaultValue);
    }
    SetValueT(varId, value) {
        this.VariableContainer.SetValueT(varId, value);
    }
    AddValueT(varId, value) {
        return this.VariableContainer.AddValueT(varId, value);
    }
    SubValueT(varId, value) {
        return this.VariableContainer.SubValueT(varId, value);
    }
    BindVariable(varId, caller, callback, ...args) {
        return this.VariableContainer.Bind(varId, caller, callback);
    }
    UnbindVariable(varId, caller, callback) {
        return this.VariableContainer.Unbind(varId, caller, callback);
    }
    RemoveVariable(varId) {
        this.VariableContainer.RemoveVariable(varId);
    }
    /**
     * 每个Manager重载这个函数可以在加载本地数据时初始化自定数据格式的数据配置
     */
    Restore() {
        this.VariableContainer.RevertData();
    }
    /**
     * 每个Manager重载这个函数保存自定数据格式的数据配置
     */
    Save() {
        this.Name && this.Name.length > 0 ? this.VariableContainer.SaveData(this.Name) : this.VariableContainer.SaveAllData();
    }
    SaveVariable(name) {
        this.Name && this.Name.length > 0 && this.VariableContainer.SaveVariable(name);
    }
}


/***/ }),

/***/ "./framework/Define.ts":
/*!*****************************!*\
  !*** ./framework/Define.ts ***!
  \*****************************/
/*! exports provided: EM_PLATFORM_TYPE, GAME_STATE, SYS_VAR, PANEL_TYPE, PERIOD */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EM_PLATFORM_TYPE", function() { return EM_PLATFORM_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GAME_STATE", function() { return GAME_STATE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SYS_VAR", function() { return SYS_VAR; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PANEL_TYPE", function() { return PANEL_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PERIOD", function() { return PERIOD; });
var EM_PLATFORM_TYPE;
(function (EM_PLATFORM_TYPE) {
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Wechat"] = 0] = "Wechat";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["QQ"] = 1] = "QQ";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Oppo"] = 2] = "Oppo";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Vivo"] = 3] = "Vivo";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Toutiao"] = 4] = "Toutiao";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Baidu"] = 5] = "Baidu";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["P_4399"] = 6] = "P_4399";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Qutoutiao"] = 7] = "Qutoutiao";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["P_360"] = 8] = "P_360";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Momo"] = 9] = "Momo";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Web"] = 10] = "Web";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Xiaomi"] = 11] = "Xiaomi";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Meizu"] = 12] = "Meizu";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["UC"] = 13] = "UC";
    EM_PLATFORM_TYPE[EM_PLATFORM_TYPE["Alipay"] = 14] = "Alipay";
})(EM_PLATFORM_TYPE || (EM_PLATFORM_TYPE = {}));
/**
 * 游戏状态枚举
 */
var GAME_STATE;
(function (GAME_STATE) {
    GAME_STATE["INVALID"] = "INVALID";
    GAME_STATE["HOME"] = "HOME";
    GAME_STATE["LOADING"] = "LOADING";
    GAME_STATE["GAME"] = "GAME";
    GAME_STATE["PAUSE"] = "PAUSE";
    GAME_STATE["RESULT"] = "RESULT";
})(GAME_STATE || (GAME_STATE = {}));
var SYS_VAR;
(function (SYS_VAR) {
    SYS_VAR["ResourceManager"] = "ResourceManager";
})(SYS_VAR || (SYS_VAR = {}));
var PANEL_TYPE;
(function (PANEL_TYPE) {
    PANEL_TYPE["APP"] = "APP";
    PANEL_TYPE["POP"] = "POP";
    PANEL_TYPE["SYS"] = "SYS";
})(PANEL_TYPE || (PANEL_TYPE = {}));
var PERIOD;
(function (PERIOD) {
    PERIOD["Invalid"] = "Invalid";
    /**
     * 加载完毕时
     */
    PERIOD["OnLoading"] = "OnLoading";
    /**
     * 主界面时
     */
    PERIOD["OnInit"] = "OnInit";
    /**
     * 游戏预加载
     */
    PERIOD["OnPreload"] = "OnPreload";
    /**
     * 游戏开始
     */
    PERIOD["OnStart"] = "OnStart";
    /**
     * 游戏进行时
     */
    PERIOD["OnRun"] = "OnRun";
    /**
     * 游戏恢复时
     */
    PERIOD["OnResume"] = "OnResume";
    /**
     * 游戏暂停时
     */
    PERIOD["OnPause"] = "OnPause";
    /**
     * 游戏结算时
     */
    PERIOD["OnResult"] = "OnResult";
    /**
     * 角色重生时
     */
    PERIOD["OnRevive"] = "OnRevive";
})(PERIOD || (PERIOD = {}));


/***/ }),

/***/ "./framework/Log/FrameworkLog.ts":
/*!***************************************!*\
  !*** ./framework/Log/FrameworkLog.ts ***!
  \***************************************/
/*! exports provided: FrameworkLog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FrameworkLog", function() { return FrameworkLog; });
/* harmony import */ var _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Utility/LogUtility */ "./framework/Utility/LogUtility.ts");

class FrameworkLog {
    static Log(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Log("Framework", message, null, args);
    }
    static Debug(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Debug("Framework", message, null, args);
    }
    static Info(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Info("Framework", message, null, args);
    }
    static Warn(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Warn("Framework", message, null, args);
    }
    static Error(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Error("Framework", message, null, args);
    }
}


/***/ }),

/***/ "./framework/Log/UrlLog.ts":
/*!*********************************!*\
  !*** ./framework/Log/UrlLog.ts ***!
  \*********************************/
/*! exports provided: UrlLog */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UrlLog", function() { return UrlLog; });
/* harmony import */ var _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Utility/LogUtility */ "./framework/Utility/LogUtility.ts");

class UrlLog {
    static Log(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Log("Url", message, null, args);
    }
    static Debug(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Debug("Url", message, null, args);
    }
    static Info(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Info("Url", message, null, args);
    }
    static Warn(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Warn("Url", message, null, args);
    }
    static Error(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Error("Url", message, null, args);
    }
}


/***/ }),

/***/ "./framework/Manager/AgentManager.ts":
/*!*******************************************!*\
  !*** ./framework/Manager/AgentManager.ts ***!
  \*******************************************/
/*! exports provided: AgentManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgentManager", function() { return AgentManager; });
/* harmony import */ var _Manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Manager */ "./framework/Manager/Manager.ts");

class AgentManager extends _Manager__WEBPACK_IMPORTED_MODULE_0__["Manager"] {
    Initialize() {
    }
    Post(url, data, callback, retryCount = 1, isAsync, isDebug) {
        if (!data) {
            data = {};
        }
        isAsync = isAsync ? isAsync == true : false;
        isDebug = isDebug ? isDebug == true : false;
        let http = new XMLHttpRequest();
        http.timeout = 5000;
        if (isDebug) {
            console.log("http post url:" + url);
        }
        http.open("post", url, isAsync);
        http.setRequestHeader("Content-Type", "application/json");
        let self = this;
        http.onerror = function (e) {
            console.error(e);
            if (retryCount > 1) {
                retryCount--;
                self.Post(url, data, callback, retryCount, isAsync, isDebug);
            }
            else if (callback && typeof callback === "function") {
                callback(e);
            }
            else {
                console.log("callback is no correct");
            }
        };
        http.onabort = function (e) {
            console.error(e);
        };
        http.onprogress = function (e) {
        };
        http.onreadystatechange = function () {
            if (this.status === 200) {
            }
            else {
                console.warn("http request status:" + this.status);
            }
        };
        http.onload = function () {
            let response = JSON.parse(this.responseText);
            if (response && callback && typeof callback === "function") {
                callback(response);
            }
            else {
                console.error("json parse failed!");
            }
        };
        let json = JSON.stringify(data);
        if (json && json.length > 0 && isDebug) {
            console.log("http post " + " url:" + url + " json:" + json);
        }
        try {
            http.send(json);
        }
        catch (e) {
            console.exception(e);
        }
    }
    Get(url, data, callback, retryCount = 1, isAsync, isDebug) {
        if (!data) {
            data = {};
        }
        isAsync = isAsync ? isAsync == true : false;
        isDebug = isDebug ? isDebug == true : false;
        let send_url = '?';
        let first = true;
        for (let key in data) {
            first ? (first = false, send_url += "?") : (send_url += "&");
            send_url += (key + '=' + data[key]);
        }
        let http = new XMLHttpRequest();
        http.timeout = 5000;
        if (isDebug) {
            console.log("http get url:" + url);
        }
        http.open("get", url, isAsync);
        http.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        let self = this;
        http.onerror = function (e) {
            console.error(e);
            if (retryCount > 1) {
                retryCount--;
                self.Get(url, data, callback, retryCount, isAsync, isDebug);
            }
            else if (callback && typeof callback === "function") {
                callback(e);
            }
            else {
                console.log("callback is no correct");
            }
        };
        http.onabort = function (e) {
            console.error(e);
        };
        http.onprogress = function (e) {
        };
        http.onreadystatechange = function () {
            if (this.status === 200) {
            }
            else {
                console.warn("http request status:" + this.status);
            }
        };
        http.onload = function () {
            let response = JSON.parse(this.responseText);
            if (response && callback && typeof callback === "function") {
                callback(response);
            }
            else {
                console.error("json parse failed!");
            }
        };
        try {
            http.send();
        }
        catch (e) {
            console.exception(e);
        }
    }
}


/***/ }),

/***/ "./framework/Manager/CommerceManager.ts":
/*!**********************************************!*\
  !*** ./framework/Manager/CommerceManager.ts ***!
  \**********************************************/
/*! exports provided: CommerceManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommerceManager", function() { return CommerceManager; });
/* harmony import */ var _Manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Manager */ "./framework/Manager/Manager.ts");

class CommerceManager extends _Manager__WEBPACK_IMPORTED_MODULE_0__["Manager"] {
    Initialize() {
    }
}


/***/ }),

/***/ "./framework/Manager/DataManager.ts":
/*!******************************************!*\
  !*** ./framework/Manager/DataManager.ts ***!
  \******************************************/
/*! exports provided: DataManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataManager", function() { return DataManager; });
/* harmony import */ var _Manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Manager */ "./framework/Manager/Manager.ts");

class DataManager extends _Manager__WEBPACK_IMPORTED_MODULE_0__["Manager"] {
    Initialize() {
    }
}


/***/ }),

/***/ "./framework/Manager/Manager.ts":
/*!**************************************!*\
  !*** ./framework/Manager/Manager.ts ***!
  \**************************************/
/*! exports provided: Manager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Manager", function() { return Manager; });
/* harmony import */ var _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Utility/LogUtility */ "./framework/Utility/LogUtility.ts");
/* harmony import */ var _Core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Core */ "./framework/Core.ts");


class Manager extends _Core__WEBPACK_IMPORTED_MODULE_1__["BaseManager"] {
    constructor() {
        super(...arguments);
        this.Name = "Gauntlet";
    }
    Log(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Log(this.Name, message, Manager.LogStyle, args);
    }
    Debug(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Debug(this.Name, message, Manager.DebugStyle, args);
    }
    Info(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Info(this.Name, message, Manager.InfoStyle, args);
    }
    Warn(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Warn(this.Name, message, Manager.WarnStyle, args);
    }
    Error(message, ...args) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Error(this.Name, message, Manager.ErrorStyle, args);
    }
    Table(obj, name) {
        _Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Table(this.Name, Manager.TableStyle, obj, name);
    }
}
Manager.DEFAULT_FONT_SIZE = 12;
Manager.LogStyle = `color: green; font-size:${Manager.DEFAULT_FONT_SIZE}px`;
Manager.DebugStyle = `color: DodgerBlue; font-size:${Manager.DEFAULT_FONT_SIZE}px`;
Manager.InfoStyle = `color: Turquoise; font-size:${Manager.DEFAULT_FONT_SIZE}px`;
Manager.WarnStyle = `color: Orange; font-size:bold,${Manager.DEFAULT_FONT_SIZE}px`;
Manager.ErrorStyle = `color: Tomato; font-size:${Manager.DEFAULT_FONT_SIZE}px`;
Manager.TableStyle = `color: Black; font-size:${Manager.DEFAULT_FONT_SIZE}px`;


/***/ }),

/***/ "./framework/Utility/LogUtility.ts":
/*!*****************************************!*\
  !*** ./framework/Utility/LogUtility.ts ***!
  \*****************************************/
/*! exports provided: LogUtility */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LogUtility", function() { return LogUtility; });
class LogUtility {
    constructor() {
        this._Tag = "";
        this.PARAM_COUNT = 3;
        this._LogStyle = `color:green;font-size:${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        this._DebugStyle = `color:dodgerblue;font-size:${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        this._InfoStyle = `color:turquoise;font-size:${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        this._WarnStyle = `color:orange;font-size:bold,${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        this._ErrorStyle = `color:tomato;font-size:${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        this._TableStyle = `color:magenta;font-size:${LogUtility.DEFAULT_FONT_SIZE}px;font-weight:bold;`;
        // LogUtility._Platform = window[StorageAdapter.SDK_PLATFORM_NAME] as YDHW.IPlatform;
    }
    static get Instance() {
        if (this._Instance == null) {
            this._Instance = new LogUtility();
        }
        return this._Instance;
    }
    ;
    Log(tag, message, style, args) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof (style) != "undefined") ? style : this._LogStyle;
        let options = [];
        for (let i = this.PARAM_COUNT; i < arguments.length; i++) {
            options[i - this.PARAM_COUNT] = arguments[i];
        }
        if (this._Platform && this._Platform.IsOppo) {
            args && (typeof args != "undefined") && args.length > 0
                ? console.log.apply(console, ["[Log][" + tag + "] " + message].concat(options))
                : console.log.apply(console, ["[Log][" + tag + "] " + message]);
            return;
        }
        args && (typeof args != "undefined") && args.length > 0
            ? console.groupCollapsed.apply(console, ["%c[Log][" + tag + "] " + message, style].concat(options))
            : console.groupCollapsed.apply(console, ["%c[Log][" + tag + "] " + message, style]);
        console.trace();
        console.groupEnd();
    }
    Debug(tag, message, style, args) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof style != "undefined") ? style : this._DebugStyle;
        let options = [];
        for (let i = this.PARAM_COUNT; i < arguments.length; i++) {
            options[i - this.PARAM_COUNT] = arguments[i];
        }
        if (this._Platform && this._Platform.IsOppo) {
            args && (typeof args != "undefined") && args.length > 0
                ? console.debug.apply(console, ["[Debug][" + tag + "] " + message].concat(options))
                : console.debug.apply(console, ["[Debug][" + tag + "] " + message]);
            return;
        }
        args && (typeof args != "undefined") && args.length > 0
            ? console.groupCollapsed.apply(console, ["%c[Debug][" + tag + "] " + message, style].concat(options))
            : console.groupCollapsed.apply(console, ["%c[Debug][" + tag + "] " + message, style]);
        console.trace();
        console.groupEnd();
    }
    Info(tag, message, style, args) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof style != "undefined") ? style : this._InfoStyle;
        let options = [];
        for (let i = this.PARAM_COUNT; i < arguments.length; i++) {
            options[i - this.PARAM_COUNT] = arguments[i];
        }
        if (this._Platform && this._Platform.IsOppo) {
            args && (typeof args != "undefined") && args.length > 0
                ? console.info.apply(console, ["[Info][" + tag + "] " + message].concat(options))
                : console.info.apply(console, ["[Info][" + tag + "] " + message]);
            return;
        }
        args && (typeof args != "undefined") && args.length > 0
            ? console.groupCollapsed.apply(console, ["%c[Info][" + tag + "] " + message, style].concat(options))
            : console.groupCollapsed.apply(console, ["%c[Info][" + tag + "] " + message, style]);
        console.trace();
        console.groupEnd();
    }
    Warn(tag, message, style, args) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof style != "undefined") ? style : this._WarnStyle;
        let options = [];
        for (let i = this.PARAM_COUNT; i < arguments.length; i++) {
            options[i - this.PARAM_COUNT] = arguments[i];
        }
        if (this._Platform && this._Platform.IsOppo) {
            args && (typeof args != "undefined") && args.length > 0
                ? console.warn.apply(console, ["[Warn][" + tag + "] " + message].concat(options))
                : console.warn.apply(console, ["[Warn][" + tag + "] " + message]);
            return;
        }
        args && (typeof args != "undefined") && args.length > 0
            ? console.groupCollapsed.apply(console, ["%c[Warn][" + tag + "] " + message, style].concat(options))
            : console.groupCollapsed.apply(console, ["%c[Warn][" + tag + "] " + message, style]);
        console.trace();
        console.groupEnd();
    }
    Error(tag, message, style, args) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof style != "undefined") ? style : this._ErrorStyle;
        let options = [];
        for (let i = this.PARAM_COUNT; i < arguments.length; i++) {
            options[i - this.PARAM_COUNT] = arguments[i];
        }
        if (this._Platform && this._Platform.IsOppo) {
            args && (typeof args != "undefined") && args.length > 0
                ? console.error.apply(console, ["[Error][" + tag + "] " + message].concat(options))
                : console.error.apply(console, ["[Error][" + tag + "] " + message]);
            return;
        }
        args && (typeof args != "undefined") && args.length > 0
            ? console.groupCollapsed.apply(console, ["%c[Error][" + tag + "] " + message, style].concat(options))
            : console.groupCollapsed.apply(console, ["%c[Error][" + tag + "] " + message, style]);
        console.trace();
        console.groupEnd();
    }
    Table(tab, obj, name, style) {
        if (!LogUtility.IS_ENABLE_DEBUG) {
            return;
        }
        style = style && (typeof style != "undefined") ? style : this._TableStyle;
        if (this._Platform && this._Platform.IsOppo) {
            return;
        }
        console.groupCollapsed.apply(console, ["%c[Table][" + tab + "] ", style]);
        // name && console.log("%ctable: " + name, style);
        console.table(obj);
        console.groupEnd();
    }
}
LogUtility._Instance = null;
LogUtility.IS_ENABLE_DEBUG = true;
LogUtility.DEFAULT_FONT_SIZE = 12;
LogUtility.BACK_GROUND_COLOR = "background:#FFFF99";
new LogUtility();


/***/ }),

/***/ "./framework/Utility/RandomUtility.ts":
/*!********************************************!*\
  !*** ./framework/Utility/RandomUtility.ts ***!
  \********************************************/
/*! exports provided: RandomUtility */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RandomUtility", function() { return RandomUtility; });
class RandomUtility {
    static Probability(pro) {
        if (pro == 100)
            return true;
        if (pro == 0)
            return false;
        let probability = Math.floor(Math.random() * 100);
        return probability <= pro;
    }
}


/***/ }),

/***/ "./framework/Utility/TimerUtility.ts":
/*!*******************************************!*\
  !*** ./framework/Utility/TimerUtility.ts ***!
  \*******************************************/
/*! exports provided: Timer, DelayTimer, default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Timer", function() { return Timer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DelayTimer", function() { return DelayTimer; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "default", function() { return TimerUtility; });
class Timer {
    constructor() {
        this.IntervalID = null;
    }
    Initialize(name, caller, method, interval, args) {
        this.Name = name;
        this._Caller = caller;
        this._Method = method;
        this.Args = args;
        this._StartTimestamp = Date.now();
        this.Interval = interval;
    }
    Execute(...args) {
        // let bIsResult = false;
        // if (this.Args && this.Args.length > 0) {
        //     bIsResult = this._Method.call(this._Caller, this.Args);
        // } else {
        //     bIsResult = this._Method.call(this._Caller);
        // }
        let bIsResult = this._Method.call(this._Caller, args);
        if (bIsResult) {
            TimerUtility.RemoveTimer(this.Name);
        }
    }
    Invoke(...args) {
        this._Method.call(this._Caller, args);
    }
}
class DelayTimer extends Timer {
    Initialize(name, caller, method, interval, args) {
        this.Name = name;
        this._Caller = caller;
        this._InnerMethod = method;
        this.Args = args;
        this._StartTimestamp = Date.now();
        this.Interval = interval;
    }
    Execute(...args) {
        this._InnerMethod.apply(this._Caller, args);
    }
    Invoke(...args) {
        this._InnerMethod.apply(this._Caller, args);
    }
}
class TimerUtility {
    // private static _DictInterval: { [key: string]: any } = {};
    /**
     * 数字转字符串时 至少保持两位数，如：1=>01,2=>02,3=>03
     * @param num
     */
    static DecimalFormat(num) {
        return (num > 9 ? '' : '0') + num;
    }
    static TimeFormat_1(seconds, sufix) {
        return this.SecondsToDay(seconds) +
            sufix + this.SecondsToHour(seconds) +
            sufix + this.SecondsToMinute(seconds % 60);
    }
    static MilliSecondsToSeconds(milliseconds) {
        return Math.floor(milliseconds / 1000);
    }
    static SecondsToDay(seconds) {
        return seconds / 3600 | 0;
    }
    static SecondsToHour(seconds) {
        return (seconds / 60 | 0) % 60;
    }
    static SecondsToMinute(seconds) {
        return seconds / 60;
    }
    static MilliSecondsToMinute(milliSeconds) {
        return this.SecondsToMinute(this.MilliSecondsToSeconds(milliSeconds));
    }
    static Delay(interval, caller, method, ...args) {
        let timer = new DelayTimer();
        timer.Initialize("delay_event", caller, method, interval, args);
        timer.IntervalID = setTimeout(function () {
            timer.Execute(timer.Args);
        }, timer.Interval);
        // Laya.timer.once(interval, timer, timer.Execute, args);
        let key = timer.IntervalID + "";
        this._DelayTimer[key] = timer;
        return key;
    }
    static RemoveDelayTimer(id) {
        let timer = this._DictTimer[id];
        if (timer && timer instanceof DelayTimer) {
            clearTimeout(timer.IntervalID);
        }
    }
    static DelayBtnEnabled(caller, method, ...args) {
        this.Delay(2500, caller, method, ...args);
    }
    static DeactivateTimer(name) {
        let timer = this._DictTimer[name];
        if (timer && timer instanceof Timer) {
            clearInterval(timer.IntervalID);
            // Laya.timer.clear(timer, timer.Execute);
        }
        else {
            console.warn(`休眠 定时器不存在：${name}`);
        }
    }
    static ActivateTimer(name) {
        let timer = this._DictTimer[name];
        if (timer && timer instanceof Timer) {
            clearInterval(timer.IntervalID);
            timer.IntervalID = setInterval(function () {
                timer.Execute(timer.Args);
            }, timer.Interval);
            // Laya.timer.loop(timer.Interval, timer, timer.Execute, timer.Args);
        }
        else {
            console.warn(`激活 定时器不存在：${name}`);
        }
    }
    static AddTimer(name, interval, caller, init, method, args) {
        let timer = this._DictTimer[name];
        if (timer && timer instanceof Timer) {
            // console.error(`添加 定时器已存在：${name}`);
        }
        else {
            caller && init && init.call(caller);
            timer = new Timer();
            timer.Initialize(name, caller, method, interval, args);
            timer.IntervalID = setInterval(function () {
                timer.Execute(timer.Args);
            }, interval, args);
            this._DictTimer[name] = timer;
            // Laya.timer.loop(interval, timer, timer.Execute, args);
        }
    }
    static GetTimer(name) {
        let timer = this._DictTimer[name];
        if (timer && timer instanceof Timer) {
            return timer;
        }
        else {
            console.error(`获取 定时器不存在:${name}`);
            return null;
        }
    }
    static RemoveTimer(name) {
        let timer = this._DictTimer[name];
        if (timer && timer instanceof Timer) {
            timer = this._DictTimer[name];
            clearInterval(timer.IntervalID);
            // Laya.timer.clear(timer, timer.Execute);
            this._DictTimer[name] = null;
        }
        else {
            console.warn(`移除 定时器不存在:${name}`);
        }
    }
}
TimerUtility._DictTimer = {};
TimerUtility._DelayTimer = {};


/***/ }),

/***/ "./framework/Utility/Url.ts":
/*!**********************************!*\
  !*** ./framework/Utility/Url.ts ***!
  \**********************************/
/*! exports provided: URL_SLICE_TYPE, Url */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "URL_SLICE_TYPE", function() { return URL_SLICE_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Url", function() { return Url; });
/* harmony import */ var _Assist_Http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Assist/Http */ "./framework/Assist/Http.ts");
/* harmony import */ var _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../Log/UrlLog */ "./framework/Log/UrlLog.ts");
/* harmony import */ var _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../sdk/SDK/Declare */ "./sdk/SDK/Declare.ts");



var URL_SLICE_TYPE;
(function (URL_SLICE_TYPE) {
    URL_SLICE_TYPE["None"] = "Null";
    URL_SLICE_TYPE["Method"] = "Method";
    URL_SLICE_TYPE["Domain"] = "Domain";
    URL_SLICE_TYPE["Manager"] = "Manager";
    URL_SLICE_TYPE["Version"] = "Version";
    URL_SLICE_TYPE["Category"] = "Category";
    URL_SLICE_TYPE["Router"] = "Router";
    URL_SLICE_TYPE["Append"] = "Append";
    URL_SLICE_TYPE["Description"] = "Description";
    URL_SLICE_TYPE["End"] = "End";
})(URL_SLICE_TYPE || (URL_SLICE_TYPE = {}));
class Url {
    constructor() {
        this._ListSliceType = [];
        this._Header = {};
        this._AccountPass = "";
        this._MethodType = "";
        this._Http = "";
        this._DomainIndex = 0;
        this._Domains = [];
        this._Version = "";
        this._Category = "";
        this._Manager = "";
        this._Router = "";
        this._AppendList = [];
        this._Params = {};
        this._CurrentAppendIndex = 0;
        this._Description = "";
        this._Url = "";
        this._CurrentSliceType = URL_SLICE_TYPE.None;
        this._Async = true;
        this._IsEnd = false;
        this._Data = "";
        this._IsDebug = false;
        this._ReceiveCaller = null;
        this._ReceiveMethod = null;
        this._ErrorCaller = null;
        this._ErrorMethod = null;
        this._ExceptionCaller = null;
        Url.AllUrls.push(this);
    }
    SetIsDebugMode(isDebug) {
        this._IsDebug = isDebug;
    }
    AccountPass(value) {
        this._AccountPass = value;
        return this;
    }
    static SetAllUrlAccountPass(key) {
        for (let i = 0; i < Url.AllUrls.length; i++) {
            let url = Url.AllUrls[i];
            url._AccountPass = key;
        }
    }
    RequestHeader(key, value) {
        this._Header[key] = value;
        return this;
    }
    Header() {
        return this._Header;
    }
    AsPost() {
        this._MethodType = "post";
        return this;
    }
    AsGet() {
        this._MethodType = "get";
        return this;
    }
    Method() {
        return this._MethodType;
    }
    Description(value) {
        this._Description = value;
        this._CurrentSliceType = URL_SLICE_TYPE.Description;
        if (this._ListSliceType.indexOf(this._CurrentSliceType) < 0) {
            this._ListSliceType.push(this._CurrentSliceType);
        }
        return this;
    }
    Switch(index) {
        this._DomainIndex = index;
        return this;
    }
    Domain(value) {
        this._CurrentSliceType = URL_SLICE_TYPE.Domain;
        if (this._ListSliceType.indexOf(this._CurrentSliceType) < 0) {
            this._ListSliceType.push(this._CurrentSliceType);
        }
        this._Domains = value;
        return this;
    }
    Manager(value) {
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Domain) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("need set domain");
            return this;
        }
        this._CurrentSliceType = URL_SLICE_TYPE.Manager;
        if (this._ListSliceType.indexOf(this._CurrentSliceType) < 0) {
            this._ListSliceType.push(this._CurrentSliceType);
        }
        this._Manager = value;
        return this;
    }
    Version(value) {
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Manager) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("need set manager");
            return this;
        }
        this._CurrentSliceType = URL_SLICE_TYPE.Version;
        if (this._ListSliceType.indexOf(this._CurrentSliceType) < 0) {
            this._ListSliceType.push(this._CurrentSliceType);
        }
        this._Version = value;
        return this;
    }
    Category(value) {
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Version) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("need set version");
            return this;
        }
        this._CurrentSliceType = URL_SLICE_TYPE.Category;
        if (this._ListSliceType.indexOf(this._CurrentSliceType) < 0) {
            this._ListSliceType.push(this._CurrentSliceType);
        }
        this._Category = value;
        return this;
    }
    Router(value) {
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Category) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("need set category");
            return this;
        }
        this._CurrentSliceType = URL_SLICE_TYPE.Router;
        if (this._ListSliceType.indexOf(this._CurrentSliceType) < 0) {
            this._ListSliceType.push(this._CurrentSliceType);
        }
        this._Router = value;
        if (this._Router.length >= 0) {
            this._AppendList = [];
        }
        return this;
    }
    Append(value) {
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Router) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("need set router");
            return this;
        }
        this._CurrentSliceType = URL_SLICE_TYPE.Append;
        if (this._ListSliceType.indexOf(this._CurrentSliceType) < 0) {
            this._ListSliceType.push(this._CurrentSliceType);
        }
        this._CurrentAppendIndex = this._AppendList.push(value);
        return this;
    }
    End() {
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Description) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("not set description");
            return this;
        }
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Domain) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("not set domain");
            return this;
        }
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Manager) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("not set manager");
            return this;
        }
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Version) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("not set version");
            return this;
        }
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Category) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("not set category");
            return this;
        }
        if (this._ListSliceType.indexOf(URL_SLICE_TYPE.Router) < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("not set router");
            return this;
        }
        this._CurrentSliceType = URL_SLICE_TYPE.End;
        this._IsEnd = true;
        if (this._DomainIndex > this._Domains.length - 1) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("domain error");
            return null;
        }
        if (this._DomainIndex < 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("domain switch error");
            return null;
        }
        if (!this._Domains[this._DomainIndex]) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("domain null");
            return null;
        }
        this._Url = this._Http + this._Domains[this._DomainIndex] + "/" + this._Manager + "/" + this._Version + "/" + this._Category + "/" + this._Router;
        for (const key in this._AppendList) {
            this._Url = this._Url + "/" + this._AppendList[key];
        }
        if (Object.keys(this._Params).length > 0) { //todo:js编译版本需要注意
            let first = true;
            for (const key in this._Params) {
                let value = this._Params[key];
                first ? (first = false, this._Url += "?") : (this._Url += "&");
                this._Url += (key + '=' + value);
            }
        }
        return this;
    }
    Value() {
        if (!(this._IsEnd === true)) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("not set end yet");
            return null;
        }
        if (this._MethodType.length <= 0) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("not set method type");
            return null;
        }
        switch (this._CurrentSliceType) {
            case URL_SLICE_TYPE.None:
                return "Error Url";
            case URL_SLICE_TYPE.Method:
                return this._MethodType;
            case URL_SLICE_TYPE.Domain:
                return this._Domains[this._DomainIndex];
            case URL_SLICE_TYPE.Version:
                return this._Version;
            case URL_SLICE_TYPE.Category:
                return this._Category;
            case URL_SLICE_TYPE.Router:
                return this._Router;
            case URL_SLICE_TYPE.Append:
                if (this._AppendList.length > 0) {
                    return this._AppendList[this._CurrentAppendIndex];
                }
                else {
                    return "Error Append Param";
                }
            case URL_SLICE_TYPE.End:
                return this._Url;
        }
        return null;
    }
    CheckValue() {
        return this._Url;
    }
    Data() {
        return this._Data;
    }
    Post(data) {
        if (this._MethodType != "post") {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("current method is not post");
            return null;
        }
        this._Data = data || "";
        return this;
    }
    IsAsync() {
        return this._Async;
    }
    EncodeURI() {
        if (this._IsEnd === false) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Error("not end");
            return "";
        }
        return encodeURI(this._Url);
    }
    Param(key, value) {
        // if (this._ReceiveCaller == null) {
        //     UrlLog.Error("not bind event");
        // }
        this._Params[key] = value;
        return this;
    }
    static Post(data) {
        let url = new Url();
        url._MethodType = "post";
        url._Data = data;
        return url;
    }
    static Get() {
        let url = new Url();
        url._MethodType = "get";
        return url;
    }
    Send(clientInfo) {
        if (this._IsDebug) {
            console.log("url=" + this._Url);
        }
        if (this._AccountPass && this._AccountPass.length > 0) {
            this.RequestHeader("AccountPass", this._AccountPass);
        }
        if (clientInfo && typeof clientInfo == "object") {
            // let ClientInfo = {
            //     "brand": sys.brand || "",
            //     "model": sys.model || "",
            //     "platform": sys.platform || "",
            //     "version": s_d._SDKversion,
            //     "resolution": sys.resolution || _resolution
            // };
            // http.setRequestHeader("ClientInfo", clientInfo);
        }
        _Assist_Http__WEBPACK_IMPORTED_MODULE_0__["default"].Request(_sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Platform, this, 3, this._IsDebug);
    }
    Clone() {
        let url = new Url();
        url._ListSliceType = [];
        for (const item of this._ListSliceType) {
            url._ListSliceType.push(item);
        }
        url._Header = {};
        for (const key in this._Header) {
            url._Header[key] = this._Header[key];
        }
        url._AccountPass = this._AccountPass;
        url._MethodType = this._MethodType;
        url._Http = this._Http;
        url._DomainIndex = this._DomainIndex;
        url._Domains = [];
        for (const domain of this._Domains) {
            url._Domains.push(domain);
        }
        url._Version = this._Version;
        url._Category = this._Category;
        url._Manager = this._Manager;
        url._Router = this._Router;
        url._AppendList = [];
        for (const item of this._AppendList) {
            url._AppendList.push(item);
        }
        url._CurrentAppendIndex = this._CurrentAppendIndex;
        url._Url = this._Url;
        url._CurrentSliceType = this._CurrentSliceType;
        url._Async = this._Async;
        url._IsEnd = this._IsEnd;
        url._Data = JSON.parse(JSON.stringify(this._Data));
        url._ReceiveCaller = null;
        url._ReceiveMethod = null;
        url._ErrorCaller = null;
        url._ErrorMethod = null;
        url._ExceptionCaller = null;
        url._ExceptionMethod = null;
        return url;
    }
    OnReceive(caller, method) {
        this._ReceiveCaller = caller;
        this._ReceiveMethod = method;
        return this;
    }
    InvokeReceive(data) {
        let isBind = this._ReceiveCaller && this._ReceiveMethod;
        if (!isBind) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Warn("not bind receive callback");
            return;
        }
        this._ReceiveMethod.call(this._ReceiveCaller, data);
    }
    OnError(caller, method) {
        this._ErrorCaller = caller;
        this._ErrorMethod = method;
        return this;
    }
    InvokeError(data) {
        let isBind = this._ErrorCaller && this._ErrorMethod;
        if (!isBind) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Warn("not bind error callback");
            return;
        }
        this._ErrorMethod.call(this._ErrorCaller, data);
    }
    OnException(caller, method) {
        this._ExceptionCaller = caller;
        this._ExceptionMethod = method;
        return this;
    }
    InvokeException(data) {
        let isBind = this._ExceptionCaller && this._ExceptionMethod;
        if (!isBind) {
            _Log_UrlLog__WEBPACK_IMPORTED_MODULE_1__["UrlLog"].Warn("not bind exception callback");
            return;
        }
        this._ExceptionMethod.call(this._ExceptionCaller, data);
    }
}
Url.AllUrls = [];


/***/ }),

/***/ "./framework/Utility/VerifyUtility.ts":
/*!********************************************!*\
  !*** ./framework/Utility/VerifyUtility.ts ***!
  \********************************************/
/*! exports provided: VerifyUtility */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VerifyUtility", function() { return VerifyUtility; });
class VerifyUtility {
    static ToBoolean(value) {
        return value === 1 ? true : false;
    }
}


/***/ }),

/***/ "./sdk/Assist/Log.ts":
/*!***************************!*\
  !*** ./sdk/Assist/Log.ts ***!
  \***************************/
/*! exports provided: Log */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Log", function() { return Log; });
/* harmony import */ var _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../framework/Utility/LogUtility */ "./framework/Utility/LogUtility.ts");

class Log {
    static Log(message, ...args) {
        _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Log("YDHW_SDK", message, null, args);
    }
    static Debug(message, ...args) {
        _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Debug("YDHW_SDK", message, null, args);
    }
    static Info(message, ...args) {
        _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Info("YDHW_SDK", message, null, args);
    }
    static Warn(message, ...args) {
        _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Warn("YDHW_SDK", message, null, args);
    }
    static Error(message, ...args) {
        _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_0__["LogUtility"].Instance.Error("YDHW_SDK", message, null, args);
    }
}


/***/ }),

/***/ "./sdk/Base/SDKPlatform.ts":
/*!*********************************!*\
  !*** ./sdk/Base/SDKPlatform.ts ***!
  \*********************************/
/*! exports provided: SDKErrorInfo, SDKPlatform */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDKErrorInfo", function() { return SDKErrorInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDKPlatform", function() { return SDKPlatform; });
/* harmony import */ var _framework_Define__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../framework/Define */ "./framework/Define.ts");
/* harmony import */ var _framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../framework/Assist/StorageAdapter */ "./framework/Assist/StorageAdapter.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../framework/Utility/LogUtility */ "./framework/Utility/LogUtility.ts");




class SDKErrorInfo {
}
class SDKPlatform {
    constructor() {
        //这些是不暴露对外的
        this.Controller = null;
        this.Controller2 = null;
        this.Config = null;
        this.Name = "SDKPlatform";
        this.SystemInfo = null;
        //BannerAd
        this.BannerAdId = "";
        this.BannerAd = null;
        this.BannerStyle = null;
        this.CallerOnResizeBannerAd = null;
        this.IsMisTouchBannerAd = false;
        this.IsShowBannerAd = false;
        //RewardVideoAd
        this.RewardVideoAdId = "";
        this.RewardVideoAd = null;
        this.IsShowRewardVideoAd = false;
        //InterstitialAd
        this.InterstitialAdId = "";
        this.InterstitialAd = null;
        //UserInfoButton
        this.UserInfoButton = null;
        this.IsSimulator = false;
        this.NickName = "";
        this.AvatarUrl = "";
        this.Code = "";
        this.OpenID = "";
        this.AccountPass = "";
        this.NetType = "Get NetworkType failed!";
        // Version:string = "";
        this.Brand = "";
        this.Model = "";
        this.Resolution = "";
        this.Env = "";
        this.InviteAccount = "";
        this.IsDebug = false;
        this.IsWechat = false;
        this.IsQQ = false;
        this.IsOppo = false;
        this.IsVivo = false;
        this.IsToutiao = false;
        this.IsBaidu = false;
        this.Is4399 = false;
        this.IsQutoutiao = false;
        this.Is360 = false;
        this.IsMomo = false;
        this.IsXiaomi = false;
        this.IsMeizu = false;
        this.IsUC = false;
        this.IsWeb = false;
        this.IsAlipay = false;
        this.IsQGMiniGame = false;
        this.IsQQMiniGame = false;
        this.IsOnMobile = false;
        this.IsOnIOS = false;
        this.IsOnIPhone = false;
        this.IsOnMac = false;
        this.IsOnIPad = false;
        this.IsOnAndroid = false;
        this.IsOnWP = false;
        this.IsOnQQBrowser = false;
        this.IsOnMQQBrowser = false;
        this.IsOnWeiXin = false;
        this.IsOnSafari = false;
        this.IsOnPC = false;
        this.IsCocosEngine = false;
        this.IsLayaEngine = false;
        this.Name = this.constructor.name;
        this.Config = window[_framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__["StorageAdapter"].SDK_CONFIG];
        this.IsDebug = Boolean(this.Config.debug);
        this.Env = String(this.Config.env);
        this.IsDebug = Boolean(this.Config.debug);
        let u = window.navigator.userAgent;
        if (window.hasOwnProperty('wx')) {
            if (window.hasOwnProperty('qq') && u.indexOf('MiniGame') > -1) {
                this.IsDebug && console.log("平台:QQ");
                this.Controller = window['qq'];
                this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].QQ;
                this.IsQQ = true;
                this.IsQQMiniGame = true;
            }
            else if (u.indexOf('wechatdevtools') > -1) { //同时wx，wechatdevtools是微信模拟器
                this.IsDebug && console.log("平台:微信开发者工具");
                this.Controller = window['wx'];
                this.IsSimulator = this.Controller.getSystemInfoSync().platform == 'devtools';
                this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Wechat;
                this.IsWechat = true;
            }
            else if (window.hasOwnProperty('tt') && (u.indexOf('MiniGame') > -1 ||u.indexOf('bytedanceide') > -1)) {
                this.IsDebug && console.log("平台:今日头条");
                this.Controller = window['tt'];
                this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Toutiao;
                this.IsToutiao = true;
            }
            else if (u.indexOf('MiniGame') > -1) {
                this.IsDebug && console.log("平台:微信");
                this.Controller = window['wx'];
                this.IsSimulator = this.Controller.getSystemInfoSync().platform == 'devtools';
                this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Wechat;
                this.IsWechat = true;
            }
        }
        else if (window.hasOwnProperty('qg') && u.indexOf('OPPO') > -1) {
            this.IsDebug && console.log("平台:Oppo");
            this.Controller = window['qg'];
            this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Oppo;
            this.IsOppo = true;
            this.IsQGMiniGame = true;
        }
        else if (window.hasOwnProperty("qg") && u.indexOf('VVGame') > -1) {
            this.IsDebug && console.log("平台:Vivo");
            this.Controller = window['qg'];
            this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Vivo;
            this.IsVivo = true;
            this.IsQGMiniGame = true;
        }
        else if (window.hasOwnProperty('swan') && u.indexOf('SwanGame') > -1) {
            this.IsDebug && console.log("平台:百度");
            this.Controller = window['swan'];
            this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Baidu;
            this.IsBaidu = true;
        }
        else if (window.hasOwnProperty('4399')) {
            this.IsDebug && console.log("平台:4399");
        }
        else if (window.hasOwnProperty('qu_tou_tiao')) {
            this.IsDebug && console.log("平台:趣头条");
        }
        else if (window.hasOwnProperty('360')) {
            this.IsDebug && console.log("平台:360");
        }
        else if (window.hasOwnProperty('momo')) {
            this.IsDebug && console.log("平台:陌陌");
        }
        else if (u.indexOf('QuickGame') > -1) {
            this.IsDebug && console.log("平台:小米");
            this.Controller = window['qg'];
            this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Xiaomi;
            this.IsXiaomi = true;
            this.IsQGMiniGame = true;
        }
        else if (window.hasOwnProperty("mz") && u.indexOf('MZ-') > -1) {
            this.IsDebug && console.log("平台:魅族");
            this.Controller = window['mz'];
            this.Controller2 = window['qg'];
            this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Meizu;
            this.IsMeizu = true;
        }
        else if (window.hasOwnProperty("uc")) {
            this.IsDebug && console.log("平台:UC");
            this.Controller = window['uc'];
            this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].UC;
            this.IsUC = true;
        }
        else if (window.hasOwnProperty("my") && u.indexOf('AlipayMiniGame') > -1) { //支付宝小游戏
            this.Controller = window['my'];
            this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Alipay;
            this.IsAlipay = true;
        }
        else if (window.hasOwnProperty('document') && u.indexOf("Mobile") < 0) { // Web 平台
            this.IsDebug && console.log("平台:");
            this.Controller = window;
            this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Web;
            this.IsWeb = true;
        }
        else if (window.hasOwnProperty('document') && u.indexOf("Mobile") > 0) { // Mobile H5 平台
            this.Controller = window;
            this.PlatformType = _framework_Define__WEBPACK_IMPORTED_MODULE_0__["EM_PLATFORM_TYPE"].Web;
            this.IsWeb = true;
        }
        else {
            console.error("当前平台识别不出来,不知道是什么鬼:" + u);
        }
        if (!window[_framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__["StorageAdapter"].SDK_CONFIG]) {
            console.error("缺少配置:" + _framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__["StorageAdapter"].SDK_CONFIG);
        }
        this.IsOnMobile = window['isConchApp'] ? true : u.indexOf("Mobile") > -1;
        this.IsOnIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
        this.IsOnIPhone = u.indexOf("iPhone") > -1;
        this.IsOnMac = u.indexOf("Mac OS X") > -1;
        this.IsOnIPad = u.indexOf("iPad") > -1;
        this.IsOnAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1;
        this.IsOnWP = u.indexOf("Windows Phone") > -1;
        this.IsOnQQBrowser = u.indexOf("QQBrowser") > -1;
        this.IsOnMQQBrowser = u.indexOf("MQQBrowser") > -1 || (u.indexOf("Mobile") > -1 && u.indexOf("QQ") > -1);
        this.IsOnWeiXin = u.indexOf('MicroMessenger') > -1;
        this.IsOnSafari = u.indexOf("Safari") > -1;
        this.IsOnPC = !this.IsOnMobile;
        this.IsCocosEngine = window['cc'] && typeof window['cc'] === 'object' && window['cc']['sys'] === 'object' && window['cc']['sys']['platform'] === window['cc']['sys'].WECHAT_GAME;
        this.IsLayaEngine = window['Laya'] && typeof Laya === 'object' && typeof Laya.Browser === 'function' && typeof Laya.Browser.onWeiXin == 'boolean' && Laya.Browser.onWeiXin;
    }
    Initialize() {
        this.GetSystemInfoSync(this, (data) => {
            this.SystemInfo = data;
        });
        return true;
    }
    Login(caller, onSuccess, onError) {
        throw new Error("Login Method not implemented.");
    }
    IsHasAPI(name) {
        let func = this.Controller[name];
        if (!func || func === undefined || typeof func !== "function") {
            this.Warn("platform not exists api:" + name);
            return false;
        }
        return true;
    }
    LaunchInfo() {
    }
    GetSystemInfoSync(caller, method) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Platform.IsDebug && console.log("SDKPlatform GetSystemInfoSync()");
    }
    OnFrontend(caller, method) {
    }
    OnBackend(caller, method) {
    }
    OnError(caller, method) {
    }
    CreateFeedbackButton(_btnVect, hide) {
    }
    ShowFeedbackButton(visible) {
    }
    ExitMiniProgram() {
    }
    TriggerGC() {
    }
    OnShare(_data) {
    }
    NavigateToMiniProgram(ownAppId, toAppId, toUrl, caller, onSuccess, onFail) {
    }
    CreateBannerAd(adUnitId, isMisTouch, style, caller, onResize, method) {
    }
    RefreshBannerAd() {
    }
    CloseBannerAd() {
        if (!this.BannerAd) {
            return;
        }
        this.BannerAd.destroy();
        this.BannerAd = null;
    }
    SetBannerVisible(val) {
    }
    ChangeBannerStyle(style) {
        this.BannerAd && (this.BannerStyle = style,
            this.BannerAd.style.top = style.top,
            this.BannerAd.style.left = style.left,
            this.BannerAd.width = style.width,
            this.BannerAd.height = style.height);
    }
    CreateRewardVideoAd(adUnitId, caller, onLoad, onCLose, onError) {
    }
    ShowRewardVideoAd(caller, onShow, onException) {
    }
    CreateInterstitialAd(adUnitId, caller, onLoad, onClose, onError) {
    }
    ShowInterstitialAd(caller, method) {
    }
    ClearInterstitialAd() {
    }
    ShowUserInfoButton(info, caller, onSuccess, onError) {
    }
    HideUserInfoButton() {
    }
    VibrateShort() {
    }
    VibrateLong() {
    }
    SetUserCloudStorage(_kvDataList) {
    }
    PostMessage(_data) {
    }
    CheckUpdate() {
    }
    GetNetworkType(caller, method) {
    }
    CreateVideo(data) {
        return null;
    }
    LoadSubpackage(name, update) {
        return null;
    }
    ShowLoading() {
    }
    HideLoading() {
    }
    ShowShare(data) {
    }
    ShowModal(modal) {
    }
    // CreateNativeAd(adUnitId: string, caller: any, method: (...args: any[]) => void): void {
    //     
    // }
    // NativeAdShow(nativeId: string): void {
    //     
    // }
    // NativeAdClick(nativeId: string): void {
    //     
    // }
    Hook(name, caller, method) {
        try {
            let tmpFunc = this.Controller[name];
            Object.defineProperty(this.Controller, name, { writable: true });
            this.Controller[name] = function () {
                let len = arguments.length;
                let tmpList = Array(len);
                for (let i = 0; i < len; i++) {
                    tmpList[i] = arguments[i];
                }
                let tmpOut = tmpFunc.apply(this, tmpList);
                if (Object.prototype.toString.call(name).slice(8, -1) == "Function") {
                    return method.call(caller, tmpList, tmpOut);
                }
            };
            Object.defineProperty(this.Controller, name, { writable: false });
            this.Log("Hook Success name:" + name);
        }
        catch (e) {
            this.Log("Hook Exception:" + e + " name:" + name);
        }
    }
    Log(message, ...args) {
        _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_3__["LogUtility"].Instance.Log("YDHW-SDK:" + this.Name, message, null, args);
    }
    Debug(message, ...args) {
        _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_3__["LogUtility"].Instance.Debug("YDHW-SDK:" + this.Name, message, null, args);
    }
    Info(message, ...args) {
        _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_3__["LogUtility"].Instance.Info("YDHW-SDK:" + this.Name, message, null, args);
    }
    Warn(message, ...args) {
        _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_3__["LogUtility"].Instance.Warn("YDHW-SDK:" + this.Name, message, null, args);
    }
    Error(message, ...args) {
        _framework_Utility_LogUtility__WEBPACK_IMPORTED_MODULE_3__["LogUtility"].Instance.Error("YDHW-SDK:" + this.Name, message, null, args);
    }
}
SDKPlatform.ERROR_DESCRIPTION = {
    9000: { ErrCode: 9000, Description: "网络地址无效", SolutionHint: "暂无" },
    9001: { ErrCode: 9001, Description: "没有访问凭证", SolutionHint: "暂无" },
    9002: { ErrCode: 9002, Description: "访问凭证无效", SolutionHint: "暂无" },
    9003: { ErrCode: 9003, Description: "客户端版本过低", SolutionHint: "暂无" },
    9004: { ErrCode: 9004, Description: "数据校验不通过", SolutionHint: "暂无" },
    9999: { ErrCode: 9999, Description: "系统异常", SolutionHint: "暂无" },
};


/***/ }),

/***/ "./sdk/Commerce/ToutiaoCommerce.ts":
/*!*****************************************!*\
  !*** ./sdk/Commerce/ToutiaoCommerce.ts ***!
  \*****************************************/
/*! exports provided: ToutiaoCommerce */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToutiaoCommerce", function() { return ToutiaoCommerce; });
/* harmony import */ var _Manager_CommerceMgr__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Manager/CommerceMgr */ "./sdk/Manager/CommerceMgr.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _Model_Toutiao__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Model/Toutiao */ "./sdk/Model/Toutiao.ts");




class ToutiaoCommerce extends _Manager_CommerceMgr__WEBPACK_IMPORTED_MODULE_0__["CommerceMgr"] {
    constructor(platform) {
        super(platform);
        this.Platform = null;
        this.TemplateIdList = null;
        this.IsSharingStatus = false;
        this.Platform = platform;
        this.Name = "YDHW-SDK:" + this.constructor.name;
        this.TemplateIdList = this.Config.tt_template_id_list;
        if (!this.TemplateIdList || typeof this.TemplateIdList == "undefined") {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && console.error("YDHW_CONFIG.tt_template_id_list not exists");
        }
        this.Platform.OnFrontend(this, () => {
            this.SwitchLog && this.Log("OnFrontend");
            this.RefreshLastPlayTime();
        });
        this.Platform.OnBackend(this, () => {
            this.SwitchLog && this.Log("OnBackend");
            console.log("YDHW -----OnFrontend:", this._showBannerAd, this._showInterstitialAd, this._showVideoAd, this._showShare);
            if (this._showBannerAd && !this._showInterstitialAd && !this._showVideoAd && !this._showShare) {
                console.log("YDHW has click Banner ?");
                this.StatisticBanner(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["EM_STATISTIC_TYPE"].CREATE, this._bannerId);
            }
            this.StatisticDuration();
        });
    }
    StatisticBanner(type, adId) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SDK_NET_API"].Banner, type, adId, this, (isOk) => {
            if (isOk === true) {
                this.Log("StatisticBanner success");
            }
        });
    }
    Initialize() {
        super.Initialize();
    }
    Login(caller, method) {
        this.Platform.Login(this, (code, token, result) => {
            this.Code = code;
            if (!code) {
                //游客身份登录的情况下，和后台约定好了
                //将anonymousCode通过code传过去,同时pkgName设置为anonymousCode
                this.Code = result.anonymousCode;
                this.PkgName = 'anonymousCode';
            }
            this._InnerLogin(caller, method);
        }, (error) => {
            this.Error("登录失败 Error:" + JSON.stringify(error));
            caller && method.call(caller, false);
        });
    }
    _ShareAppMessage(shareCardId, channel, templateId, desc, title, imageUrl, query, extra, caller, onSuccess, onError) {
        this.Platform.ShareAppMessage(channel, templateId, desc, title, imageUrl, query, extra, this, () => {
            //分享图统计（打点统计）
            shareCardId && this.StatisticShareCard(shareCardId);
            caller && onSuccess && onSuccess.call(caller, true);
            this._showShare = true;
        }, (error) => {
            this.Error("ShareAppMessage Error:" + error);
            caller && onSuccess && onSuccess.call(caller, false);
            caller && onError && onError.call(caller, error);
            this._showShare = false;
        });
    }
    ShareImage(scene, onSuccess, channel, description) {
        this._CurrentShareScene = scene;
        let templateId = "";
        let card = null;
        let query = null;
        let shareList = this.Strategy.GetShareSceneConfig(scene);
        if (shareList && shareList.length > 0) {
            let index = Math.floor(Math.random() * shareList.length);
            card = shareList[index];
            query = 'account_id=' + this.AccountId + '&sharecard_id=' + card.id + '&from=' + 'stage_invite';
        }
        if (this.TemplateIdList && this.TemplateIdList.length > 0) {
            let index = Math.floor(Math.random() * this.TemplateIdList.length);
            templateId = this.TemplateIdList[index];
        }
        if (card) {
            this._ShareAppMessage(card.id, channel, templateId, description, card.title, card.img, query, null, this, (isOk) => {
                onSuccess && onSuccess(isOk);
            }, (error) => {
                this.Error("ShareImage Error:" + error);
                onSuccess && onSuccess(false);
            });
        }
        else {
            this._ShareAppMessage(card.id, channel, templateId, description, "", "", query, null, this, (isOk) => {
                onSuccess && onSuccess(isOk);
            }, (error) => {
                this.Error("ShareImage Error:" + error);
                onSuccess && onSuccess(false);
            });
        }
    }
    ShareVideo(title, description, query, onSuccess) {
        if (!this.Platform.VideoPath) {
            this.Error("video is not exists");
            onSuccess && onSuccess(false);
        }
        else if (this.TemplateIdList && this.TemplateIdList.length > 0) {
            let index = Math.floor(Math.random() * this.TemplateIdList.length);
            let templateId = this.TemplateIdList[index];
            let extra = new _Model_Toutiao__WEBPACK_IMPORTED_MODULE_3__["ShareAppMessageExtra"]();
            extra.videoTopics = [""];
            extra.videoPath = this.Platform.VideoPath;
            // _ShareAppMessage(shareCardId: number, channel: string, templateId: string,
            //     desc: string, title: string, imageUrl: string, query: string,
            //     extra: YDHW.TT.IShareAppMessageExtra, caller: any, onSuccess: (isOk: boolean) => void, onError?: (error: any) => void): void {
            this._ShareAppMessage(null, "video", templateId, description, title, "", "", extra, this, (isOk) => {
                onSuccess && onSuccess(isOk);
            });
        }
    }
    ShareTemplate(onSuccess) {
        if (this.TemplateIdList && this.TemplateIdList.length > 0) {
            let index = Math.floor(Math.random() * this.TemplateIdList.length);
            let templateId = this.TemplateIdList[index];
            this._ShareAppMessage(null, "", templateId, "", "", "", "", null, this, (isOk) => {
                onSuccess && onSuccess(isOk);
            });
        }
    }
    ShareToken(onSuccess) {
        if (this.TemplateIdList && this.TemplateIdList.length > 0) {
            let index = Math.floor(Math.random() * this.TemplateIdList.length);
            let templateId = this.TemplateIdList[index];
            this._ShareAppMessage(null, "token", templateId, "", "", "", "", null, this, (isOk) => {
                onSuccess && onSuccess(isOk);
            });
        }
    }
    ShowMoreGamesModal(onEvtOpen) {
        this.Platform.ShowMoreGamesModal(this, () => {
            let optionList = [];
            if (this.ListBoxConfig && this.ListBoxConfig.length > 0) {
                this.ListBoxConfig.forEach(element => {
                    if (element.type == '0') {
                        optionList.push({
                            appId: element.toAppid,
                            query: element.toUrl,
                            extraData: { appid: this.AppId }
                        });
                    }
                });
            }
            return optionList;
        }, (isOk) => {
            onEvtOpen && onEvtOpen(isOk);
        }, (error) => {
            this.Error("ShowMoreGamesModal Error" + error);
        });
    }
    CreateMoreGamesButton(type, imageUrl, style, caller, onMoreGame, onTip) {
        this.Platform.CreateMoreGamesButton(type, imageUrl, style, this, () => {
            let optionList = [];
            if (this.ListBoxConfig && this.ListBoxConfig.length > 0) {
                this.ListBoxConfig.forEach(element => {
                    if (element.type == '0') {
                        optionList.push({
                            appId: element.toAppid,
                            query: element.toUrl,
                            extraData: { appid: this.AppId }
                        });
                    }
                });
            }
            return optionList;
        }, (isOk) => {
            caller && onMoreGame && onMoreGame.call(caller, isOk);
        }, () => {
            caller && onTip && onTip.call(caller);
        });
    }
    RecorderStart(duration, onStart, onError, InterruptionBegin, InterruptionEnd) {
        this.Platform.RecorderStart(duration, this, onStart, onError, InterruptionBegin, InterruptionEnd);
    }
    RecorderStop(onStop) {
        this.Platform.RecorderStop(this, onStop);
    }
    RecorderPause(onPause) {
        this.Platform.RecorderPause(this, (result) => {
            onPause && onPause(result);
        });
    }
    RecorderResume(onResume) {
        this.Platform.RecorderResume(this, (result) => {
            onResume && onResume(result);
        });
    }
    GetPlatformUserInfo(caller, method) {
        this.Platform.GetPlatformUserInfo(this, (userInfo) => {
            caller && method.call(caller, userInfo);
        }, (error) => {
            this.Error("GetPlatformUserInfo Error:" + error);
            caller && method.call(caller, null);
        });
    }
}


/***/ }),

/***/ "./sdk/Config/SDKConfig.ts":
/*!*********************************!*\
  !*** ./sdk/Config/SDKConfig.ts ***!
  \*********************************/
/*! exports provided: SDKConfig */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDKConfig", function() { return SDKConfig; });
class SDKConfig {
}
/**
 * 场景白名单
 */
SDKConfig.SceneWhiteList = [];
/**
 * 强制刷新时间（秒）
 */
SDKConfig.DefaultIntervalForceRefreshBannerAd = 10;


/***/ }),

/***/ "./sdk/Extensions/PowerSystem.ts":
/*!***************************************!*\
  !*** ./sdk/Extensions/PowerSystem.ts ***!
  \***************************************/
/*! exports provided: PowerSystem */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PowerSystem", function() { return PowerSystem; });
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../framework/Utility/TimerUtility */ "./framework/Utility/TimerUtility.ts");



class PowerSystem {
    constructor() {
        this._AutoRecoveryTimerId = "PowerAutoRecoveryTimerId";
        this._CallerPowerChanged = null;
        this._ListenOnPowerChanged = null;
    }
    Initialize() {
        if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.PowerSystemConfig || !_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.PowerSystemConfig.IsOn === false) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("Power System is off");
            return;
        }
        let total = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SDK_VARS"].TotalPowerCounter, 0);
        let lastTime = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SDK_VARS"].LastPowerAddTimestamp, new Date().getTime());
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("total power:" + total);
        let upperLimit = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.PowerSystemConfig.UpperLimit;
        let recoveryTime = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.PowerSystemConfig.AutoRecoveryTime;
        let totalPower = total || _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.PowerSystemConfig.DefaultPowerValue;
        if (totalPower < upperLimit) {
            //离线时间恢复体力
            let nowTime = new Date().getTime();
            let timeSpace = Math.floor((nowTime - lastTime) / 1000);
            timeSpace = timeSpace < 0 ? 0 : timeSpace;
            let getPowerNum = Math.floor(timeSpace / recoveryTime);
            totalPower += getPowerNum;
            totalPower = (totalPower > upperLimit) ? upperLimit : totalPower;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log(`timeSpace=${timeSpace}, getPowerNum=${getPowerNum},totalPower=${totalPower}`);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SDK_VARS"].LastPowerAddTimestamp, new Date().getTime());
        }
        this.SetPower(totalPower, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["EM_POWER_RECOVERY_TYPE"].None);
    }
    SetPower(powerNum, type) {
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.PowerSystemConfig.IsOn === false) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("PowerSystem is off");
            return;
        }
        let total = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SDK_VARS"].TotalPowerCounter, 0);
        let lastPowerNum = total;
        //体力上限
        let upperLimit = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.PowerSystemConfig.UpperLimit;
        let recoveryTime = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.PowerSystemConfig.AutoRecoveryTime;
        let newPowerCount = (powerNum > upperLimit) ? upperLimit : ((powerNum < 0) ? 0 : powerNum);
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SDK_VARS"].TotalPowerCounter, newPowerCount);
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SDK_VARS"].LastPowerAddTimestamp, new Date().getTime());
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("setPower-powerUpper:" + upperLimit + ",recoveryTime:" + recoveryTime + ",userPowerNum:" + newPowerCount);
        if (newPowerCount < upperLimit) {
            _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_2__["default"].AddTimer(this._AutoRecoveryTimerId, recoveryTime * 1000, this, null, () => {
                return this._AddPowerAuto(newPowerCount);
            });
        }
        else {
            _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_2__["default"].RemoveTimer(this._AutoRecoveryTimerId);
        }
        let IsHasChanged = (lastPowerNum != newPowerCount);
        if (this._CallerPowerChanged && this._ListenOnPowerChanged && IsHasChanged && type !== 0) {
            this._CallerPowerChanged && this._ListenOnPowerChanged && this._ListenOnPowerChanged.call(this._CallerPowerChanged, type);
        }
    }
    _AddPowerAuto(total) {
        total += 1;
        let upperLimit = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.PowerSystemConfig.UpperLimit;
        total = (total > upperLimit) ? upperLimit : ((total < 0) ? 0 : total);
        if (total === upperLimit) {
            return true;
        }
        else {
            this.SetPower(total, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["EM_POWER_RECOVERY_TYPE"].AutoRecovery);
            return false;
        }
    }
    GetPower() {
        let power = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_0__["SDK_VARS"].TotalPowerCounter, 0);
        return power;
    }
    ListenOnPowerChange(caller, method) {
        this._CallerPowerChanged = caller;
        this._ListenOnPowerChanged = method;
    }
}


/***/ }),

/***/ "./sdk/Extensions/Strategy.ts":
/*!************************************!*\
  !*** ./sdk/Extensions/Strategy.ts ***!
  \************************************/
/*! exports provided: Strategy */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Strategy", function() { return Strategy; });
/* harmony import */ var _Model_User__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Model/User */ "./sdk/Model/User.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _Model_Default__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Model/Default */ "./sdk/Model/Default.ts");
/* harmony import */ var _framework_Utility_RandomUtility__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../framework/Utility/RandomUtility */ "./framework/Utility/RandomUtility.ts");





class Strategy {
    constructor(moduleList, moduleFalseList) {
        this._ShareSceneConfig = {};
        this.ModuleFalseList = [];
        this.ModuleList = [];
        if (moduleList.length > 0) {
            this.ModuleList = [];
            moduleList.forEach((element) => {
                let newModule = new _Model_User__WEBPACK_IMPORTED_MODULE_0__["ShareVideoModule"]();
                newModule._id = element._id;
                newModule.channel = element.channel;
                newModule.version = element.version;
                newModule.module = element.module;
                newModule.type = element.type;
                newModule.videoNum = element.videoNum;
                newModule.shareNum = element.shareNum;
                newModule.remarks = element.remarks;
                newModule.strategy = element.strategy;
                newModule.loopIndex = 0;
                newModule.logic = element.logicJson && element.logicJson.length > 0 ? JSON.parse(element.logicJson) : null;
                this.ModuleList.push(newModule);
            });
        }
        if (moduleFalseList.length > 0) {
            this.ModuleFalseList = [];
            moduleFalseList.forEach((element) => {
                let newModule = new _Model_User__WEBPACK_IMPORTED_MODULE_0__["ShareVideoModuleFalse"]();
                newModule._id = element._id;
                newModule.strategyId = 0;
                newModule.timeId = 0;
                newModule.count = 0;
                newModule.strategies = element.strategyjson.length > 0 ? JSON.parse(element.strategyjson) : null;
                newModule.tips = element.trickJson.length > 0 ? JSON.parse(element.trickJson) : null;
                this.ModuleFalseList.push(newModule);
            });
        }
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SDK_VARS"].ShareConfig, this._ShareSceneConfig);
    }
    ShowShareVideo(channel, module, caller, method) {
        if (this.ModuleList && this.ModuleList.length == 0) {
            caller && method && method.call(caller, 0);
            return;
        }
        let bIsHasModule = false; //默认无策略
        this.ModuleList.every((element) => {
            if (element.channel === channel && element.module === module) {
                if (element.logic) {
                    bIsHasModule = true;
                    if (element.logic.pe && element.logic.pe.length > 0) {
                        let type = element.logic.pe.shift();
                        caller && method && method.call(caller, type);
                        return false;
                    }
                    else if (element.logic.loop && element.logic.loop.length > 0 && element.logic.time && element.logic.time > 0) {
                        let type = element.logic.loop[element.loopIndex];
                        element.loopIndex += 1;
                        if (element.loopIndex >= element.logic.loop.length) {
                            element.loopIndex = 0;
                            element.logic.time -= 1;
                        }
                        caller && method && method.call(caller, type);
                        return false;
                    }
                }
            }
            return true;
        });
        if (bIsHasModule === false) {
            caller && method && method.call(caller, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["EM_SHARE_TYPE"].None);
        }
    }
    InitShareConfig(scene, configs) {
        let me = this;
        if (scene !== "") {
            this._ShareSceneConfig[scene] = configs;
            return;
        }
        configs.forEach(element => {
            if (me._ShareSceneConfig[element.scene]) {
                let configList = me._ShareSceneConfig[element.scene];
                let isHasItem = false;
                for (let i = 0, l = configList.length; i < l; i++) {
                    let tmpConfig = configList[i];
                    if (tmpConfig.id == element.id) {
                        isHasItem = true;
                        break;
                    }
                }
                if (!isHasItem) {
                    configList.push(element);
                }
                me._ShareSceneConfig[element.scene] = configList;
            }
            else {
                let tmpList = [];
                tmpList.push(element);
                me._ShareSceneConfig[element.scene] = tmpList;
            }
        });
    }
    IsHasShareScene(scene) {
        let tmpList = this._ShareSceneConfig[scene];
        if (tmpList) {
            return true;
        }
        else {
            return false;
        }
    }
    GetShareSceneConfig(scene) {
        return this._ShareSceneConfig[scene];
    }
    // GetStrategy(channel:string, module:string,)
    GetShareStrategy(channel, module) {
        let shareStrategy = null;
        if (this.ModuleFalseList && this.ModuleFalseList.length > 0) {
            if (channel && module && this.ModuleList && this.ModuleList.length > 0) {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("getSharingStrategy-channel,module:", channel, module);
                this.ModuleList.every((element) => {
                    if (element.type && element.channel && element.module && element.channel == channel && element.module == module) {
                        this.ModuleFalseList.forEach((mElement) => {
                            if (mElement._id == Number(element.type)) {
                                shareStrategy = mElement;
                                return false;
                            }
                        });
                    }
                    return true;
                });
            }
        }
        return shareStrategy;
    }
    GetShareResult(shareAppInfo, caller, method) {
        let shareSuccess = false;
        let shareBackInfo = new _Model_Default__WEBPACK_IMPORTED_MODULE_3__["ShareBackInfo"]();
        var curTime = new Date().getTime();
        let time_space = curTime - shareAppInfo.showTime;
        let sharingStrategy = this.GetShareStrategy(shareAppInfo.channel, shareAppInfo.module); //获取策略
        if (sharingStrategy) {
            let strategyIndex = sharingStrategy.strategyId;
            let strategy = sharingStrategy.strategies[strategyIndex];
            let timeId = sharingStrategy.timeId;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("假分享策略:[" + strategyIndex + " - " + timeId + "],time_space:" + time_space, JSON.stringify(strategy));
            if (strategy.time && timeId < strategy.time.length) {
                let time_item = strategy.time[timeId];
                let item_prob = strategy.prob[timeId];
                if (time_item && time_item.length > 1) {
                    for (let i = 1; i < time_item.length; i++) {
                        if (time_space >= time_item[i - 1] && time_space <= time_item[i]) {
                            let prob = item_prob[i - 1];
                            shareSuccess = _framework_Utility_RandomUtility__WEBPACK_IMPORTED_MODULE_4__["RandomUtility"].Probability(prob);
                        }
                        else if (i == time_item.length - 1 && time_space > time_item[i]) {
                            let prob = item_prob[i];
                            shareSuccess = _framework_Utility_RandomUtility__WEBPACK_IMPORTED_MODULE_4__["RandomUtility"].Probability(prob);
                        }
                    }
                }
            }
            timeId += 1;
            //
            if (shareSuccess) {
                sharingStrategy.timeId = 0;
                sharingStrategy.count += 1;
            }
            else {
                if (timeId >= strategy.time.length) {
                    sharingStrategy.timeId = 0;
                    sharingStrategy.count += 1;
                }
                else {
                    sharingStrategy.timeId = timeId;
                }
                shareBackInfo.IsSuccess = false;
                shareBackInfo.IsHasStrategy = true;
                shareBackInfo.Tips = sharingStrategy.tips;
            }
            if (sharingStrategy.count >= strategy.num) {
                //寻找下一组
                sharingStrategy.count = 0;
                sharingStrategy.strategyId = this._GetNextStrategy(sharingStrategy, strategyIndex);
            }
            this._SetSharingStategy(sharingStrategy);
        }
        else {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("真分享(time_space > 3000)?:" + time_space);
            if (time_space > 3000) {
                shareSuccess = true;
            }
            else {
                shareBackInfo.IsSuccess = false;
                shareBackInfo.IsHasStrategy = false;
            }
        }
        caller && method && method.call(caller, shareBackInfo);
    }
    _GetNextStrategy(mFalseList, strategyId) {
        let nextStrategyId = strategyId + 1;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("nextStrategyId:" + nextStrategyId);
        if (nextStrategyId >= mFalseList.strategies.length) {
            nextStrategyId = 0;
        }
        let strategy = mFalseList.strategies[nextStrategyId];
        if (strategy.num == 0) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("num:" + strategy.num);
            nextStrategyId = this._GetNextStrategy(mFalseList, nextStrategyId);
        }
        return nextStrategyId;
    }
    //改变分享策略数据
    _SetSharingStategy(sStrategy) {
        for (let i = 0; i < this.ModuleFalseList.length; i++) {
            let element = this.ModuleFalseList[i];
            if (element._id && sStrategy._id) {
                this.ModuleFalseList[i] = sStrategy;
                break;
            }
        }
    }
    GetDeepMisTouch(customNumber) {
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.SwitchTouch === false)
            return false;
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.DeveloperAccountConfig.IsOn && _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.DeveloperAccountConfig.Accounts.length > 0) {
            //白名单开关打开，判断account是否在白名单中
            let accountList = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.DeveloperAccountConfig.Accounts;
            for (let i = 0; i < accountList.length; i++) {
                if (accountList[i] == _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.AccountId + "") {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log(`In whiteList:${accountList[i]}`);
                    return true;
                }
            }
        }
        ;
        if (this._IsHasMistouchTimer()) {
            return this._IsDepthShield(customNumber);
        }
        return false;
    }
    _IsHasMistouchTimer() {
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.TimingMisTouchSwitchConfig) {
            if (!_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.TimingMisTouchSwitchConfig.IsOn)
                return true;
            let mistouchTimer = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.TimingMisTouchSwitchConfig;
            let startTimeHour = mistouchTimer.StartTimeHour || 0;
            let startTimeMinute = mistouchTimer.StartTimeMinute || 0;
            let endTimeHour = mistouchTimer.EndTimeHour || 23;
            let endTimeMinute = mistouchTimer.EndTimeMinute || 59;
            //当前时间
            let nowDate = new Date();
            let misNThours = nowDate.getHours();
            let misNTminutes = nowDate.getMinutes();
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log(`checkMistouchTimer- ${startTimeHour}:${startTimeMinute} > ${misNThours}:${misNTminutes} < ${endTimeHour}:${endTimeMinute}`);
            let inTime = !((misNThours < startTimeHour || (misNThours == startTimeHour && misNTminutes <= startTimeMinute)) || (misNThours > endTimeHour || (misNThours == endTimeHour && misNTminutes >= endTimeMinute)));
            return !inTime;
        }
        return false;
    }
    _IsDepthShield(customNumber) {
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.DeepSheildRuleConfig) {
            let description = '_IsDepthShield-';
            let depthShield = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.DeepSheildRuleConfig;
            let totalCustoms = depthShield.CustomNumberCounter || 0;
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log(description + "depthShield.Type:" + depthShield.Type);
            //0:关闭,1:按对局数,2:按关卡数
            if (depthShield.Type == 0) {
                return !this._IsGoodUser(depthShield);
            }
            else if (depthShield.Type == 1) {
                let TotalCustomNumberCounter = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SDK_VARS"].TotalCustomNumberCounter, 0);
                let TodayCustomNumberCounter = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SDK_VARS"].TodayCustomNumberCounter, 0);
                //按对局数
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log(`TotalCustomNumberCounter > totalCustoms: ${TotalCustomNumberCounter} > ${depthShield.CustomNumberCounter}`);
                if (!depthShield.CustomNumberCounter || TotalCustomNumberCounter > depthShield.CustomNumberCounter) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log(`toalByTodayNum > dayCustoms: ${TodayCustomNumberCounter} > ${depthShield.DayCustomNumber[0]}`);
                    if (!depthShield.CustomNumberCounter || TodayCustomNumberCounter > depthShield.DayCustomNumber[0]) {
                        return !this._IsGoodUser(depthShield);
                    }
                    else {
                        return false;
                    }
                }
                else {
                    return false;
                }
            }
            else if (depthShield.Type == 2) {
                //按关卡数
                //玩家传入的关卡数 customNum == totalCustoms 或者  totalCustoms 开始每间隔 depthShield.dayCustoms[0]关
                if (customNumber || customNumber === 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log(`customNum >= totalCustoms: ${customNumber} >= ${totalCustoms}`);
                    if (customNumber >= totalCustoms) {
                        if (!depthShield.DayCustomNumber || depthShield.DayCustomNumber.length == 0) {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("depthShield.dayCustoms is null or empty");
                            return false;
                        }
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log(`${customNumber} == ${totalCustoms} || ((${customNumber} > ${totalCustoms}) && ((${customNumber} - ${totalCustoms}) % (${depthShield.DayCustomNumber[0]} + 1)) == 0)`);
                        if ((customNumber == totalCustoms) || ((customNumber > totalCustoms) && ((customNumber - totalCustoms) % (depthShield.DayCustomNumber[0] + 1)) == 0)) {
                            return !this._IsGoodUser(depthShield);
                        }
                    }
                }
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("customNum is " + customNumber);
                return false;
            }
        }
    }
    //检测优质用户
    _IsGoodUser(depthShield) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log("_IsGoodUser:" + depthShield.ExecellentUserSwitch);
        let isGoods = true;
        if (depthShield.ExecellentUserSwitch) {
            if (depthShield.WatchVideoCounter || depthShield.WatchVideoCounter === 0) {
                let TodayWatchVideoCounter = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SDK_VARS"].TodayWatchVideoCounter);
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log(`isGoodUser--${TodayWatchVideoCounter} > ${depthShield.WatchVideoCounter}`);
                isGoods = (TodayWatchVideoCounter > depthShield.WatchVideoCounter);
            }
        }
        return isGoods;
    }
    IsUnlockVideo(index) {
        let unlock = false;
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.VideoUnlockLevelConfig) {
            let customNumber = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.VideoUnlockLevelConfig.CustomNumber || 0;
            let intervalCount = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.VideoUnlockLevelConfig.IntervalCount || 0;
            if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.VideoUnlockLevelConfig.IsOn) {
                if (index == customNumber) {
                    unlock = true;
                }
                else if ((index > customNumber) && ((index - customNumber) % (intervalCount + 1)) == 0) {
                    unlock = true;
                }
                else {
                    //判断是否有看视频解锁过该关卡
                    let unlockLevelNumberList = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["SDK_VARS"].ListUnlockVideoLevelNumber, []);
                    if (unlockLevelNumberList && unlockLevelNumberList.length > 0) {
                        unlockLevelNumberList.forEach(element => {
                            if (index == element) {
                                unlock = true;
                            }
                        });
                    }
                }
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Log(`IsUnlockVideo-index:${index} customNumber:${customNumber} intervalCount=${intervalCount} unlock=${unlock}`);
        }
        return unlock;
    }
}


/***/ }),

/***/ "./sdk/Extensions/YDHWCache.ts":
/*!*************************************!*\
  !*** ./sdk/Extensions/YDHWCache.ts ***!
  \*************************************/
/*! exports provided: YDHWCache */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YDHWCache", function() { return YDHWCache; });
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _Model_Default__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Model/Default */ "./sdk/Model/Default.ts");



class YDHWCache {
    constructor() {
        this.JumpOutInfo = null;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Instance.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["SDK_VARS"].JumpOutInfo, new _Model_Default__WEBPACK_IMPORTED_MODULE_2__["JumpOutInfo"]());
    }
    RemoveItemFrom(appid) {
        let jumpOutInfo = _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Instance.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["SDK_VARS"].JumpOutInfo, null);
        //点击并确认跳转某个icon后，当日（可调整）隐藏对应appid的所有icon（开启内部标识的icon不受此功能影响）
        if (_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Commerce.ListBoxConfig && _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Commerce.ListBoxConfig.length > 0 && _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Commerce.SideBoxCount < _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Commerce.ListBoxConfig.length) {
            for (let i = 0; i < _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Commerce.ListBoxConfig.length; i++) {
                let item = _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Commerce.ListBoxConfig[i];
                if (item.toAppid === appid && item.innerStatus == 0) {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Commerce.Log("隐藏对应appid的所有icon--id:" + item._id);
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Commerce.ListBoxConfig.splice(i, 1);
                    let hasItem = false;
                    for (let j = 0; j < jumpOutInfo.List.length; j++) {
                        if (jumpOutInfo.List[j].appid == appid) {
                            hasItem = true;
                        }
                    }
                    if (!hasItem) {
                        jumpOutInfo.List.push(new _Model_Default__WEBPACK_IMPORTED_MODULE_2__["AppInfo"](appid));
                    }
                    jumpOutInfo.Date = new Date().getTime();
                }
            }
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Instance.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_1__["SDK_VARS"].JumpOutInfo, jumpOutInfo);
        }
    }
}


/***/ }),

/***/ "./sdk/Manager/AgentMgr.ts":
/*!*********************************!*\
  !*** ./sdk/Manager/AgentMgr.ts ***!
  \*********************************/
/*! exports provided: AgentMgr */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AgentMgr", function() { return AgentMgr; });
/* harmony import */ var _framework_Manager_AgentManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../framework/Manager/AgentManager */ "./framework/Manager/AgentManager.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Base/SDKPlatform */ "./sdk/Base/SDKPlatform.ts");
/* harmony import */ var _Assist_Log__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Assist/Log */ "./sdk/Assist/Log.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");





class AgentMgr extends _framework_Manager_AgentManager__WEBPACK_IMPORTED_MODULE_0__["AgentManager"] {
    Initialize() {
        super.Initialize();
        this._UrlEdit = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("用户信息更新")
            .Category("user")
            .Router("info")
            .Append("edit")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Edit, this, (request, caller, method) => {
            this._UrlEdit.AsPost().Post(request).OnReceive(this, (response) => {
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("MyInfo Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlMyInfo = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("服务端信息")
            .Category("user")
            .Router("my")
            .Append("info")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].MyInfo, this, (caller, method) => {
            this._UrlMyInfo.AsGet().OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("MyInfo Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlLogin = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("用户登录")
            .Category("user")
            .Router("login")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Login, this, (request, caller, method, onFailed) => {
            this._UrlLogin.AsPost().Post(request).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && console.log("Login 错误:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && onFailed && onFailed.call(caller);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this.Debug("Edit Url=" + this._UrlEdit.CheckValue());
        this.Debug("MyInfo Url=" + this._UrlMyInfo.CheckValue());
        this.Debug("Login Url=" + this._UrlLogin.CheckValue());
    }
}


/***/ }),

/***/ "./sdk/Manager/CommerceMgr.ts":
/*!************************************!*\
  !*** ./sdk/Manager/CommerceMgr.ts ***!
  \************************************/
/*! exports provided: CommerceMgr */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CommerceMgr", function() { return CommerceMgr; });
/* harmony import */ var _framework_Manager_CommerceManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../framework/Manager/CommerceManager */ "./framework/Manager/CommerceManager.ts");
/* harmony import */ var _framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../framework/Assist/StorageAdapter */ "./framework/Assist/StorageAdapter.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _Model_User__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Model/User */ "./sdk/Model/User.ts");
/* harmony import */ var _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../framework/Utility/VerifyUtility */ "./framework/Utility/VerifyUtility.ts");
/* harmony import */ var _framework_Utility_Url__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../framework/Utility/Url */ "./framework/Utility/Url.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _Model_Data__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../Model/Data */ "./sdk/Model/Data.ts");
/* harmony import */ var _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../Config/SDKConfig */ "./sdk/Config/SDKConfig.ts");
/* harmony import */ var _Model_Default__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../Model/Default */ "./sdk/Model/Default.ts");
/* harmony import */ var _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../../framework/Utility/TimerUtility */ "./framework/Utility/TimerUtility.ts");
/* harmony import */ var _Extensions_Strategy__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../Extensions/Strategy */ "./sdk/Extensions/Strategy.ts");
/* harmony import */ var _Model_Statistic__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../Model/Statistic */ "./sdk/Model/Statistic.ts");
/* harmony import */ var _Extensions_YDHWCache__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ../Extensions/YDHWCache */ "./sdk/Extensions/YDHWCache.ts");
/* harmony import */ var _Extensions_PowerSystem__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ../Extensions/PowerSystem */ "./sdk/Extensions/PowerSystem.ts");















class CommerceMgr extends _framework_Manager_CommerceManager__WEBPACK_IMPORTED_MODULE_0__["CommerceManager"] {
    constructor(platform) {
        super();
        //内部不爆露的
        // protected _IsMisTouchBannerAd: boolean = false;
        this.Config = null;
        this._Platform = null;
        this.Strategy = null;
        this.Cache = null;
        this.PowerSystem = null;
        this.AppId = "";
        this.AppKey = "";
        this.PkgName = "";
        this.Version = "";
        this.SceneId = 0;
        this.SwitchTouch = false;
        this.SwitchPush = false;
        this.SwitchLog = false;
        this.SwitchLogin = false;
        this.SwitchJump = false;
        this.SwitchShare = false;
        this.SwitchVideo = false;
        this.SwitchEvent = false;
        this.SwitchLayer = false;
        this.SwitchResult = false;
        this.IsLowVersion = false;
        this.IsRealVersion = false;
        this.VideoFalseLimit = 0;
        this.ShareFalseLimit = 0;
        this.LastLoginTime = 0;
        // ModuleFalseList: ShareVideoModuleFalse[] = [];
        // ModuleList: ShareVideoModule[] = [];
        this.ListCustomConfig = null;
        this.ListBoxConfig = null;
        this.ListLayer = null;
        this.BannerAdUnitIdList = null; //侧边栏广告
        this.InterstitialAdUnitIdList = null; //插屏广告
        this.SpreadAdUnitIdList = null; //开屏广告
        this.NativeAdUnitIdList = null; //原生广告
        this.VideoAdUnitIdList = null; //视频广告
        this.BannerRefreshConfig = null;
        this.PowerSystemConfig = null;
        this.VideoUnlockLevelConfig = null;
        this.DeepSheildRuleConfig = null;
        this.TimingMisTouchSwitchConfig = null;
        this.DeveloperAccountConfig = null;
        //BannerAd
        // _BannerAd: any = null;
        this._LastTimeRefreshBannerAd = 0;
        this._BannerAdDelayTimerId = "";
        this._IsSmallBannerAd = false;
        //RewarVideoAd
        this._UnlockCustomNumber = 0;
        this._CallerRewardVideoAd = null;
        this._EvtOnCloseRewardVideoAd = null;
        this._IsAddPower = false;
        this._IsRewardVideoAdFinish = false;
        this._RewardVideoDelayTimerId = "";
        //InterstitialAd
        /**
         * 插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
         */
        this.InterstitialAdFirstShowWaitTime = 0;
        /**
         * 插屏广告-两次展示之间时间间隔（秒）
         */
        this.InterstitialAdShowTimeInterval = 0;
        this._LastTimeCreateInterstitialAd = 0;
        //ShareCard
        this._CurrentShareScene = "";
        this._CallerShareCardBack = null;
        this._EvtOnShareCardBack = null;
        this._ShareAppInfo = null;
        this.SideBoxCount = 20;
        this._LastPlayTime = new Date().getTime();
        this._showShare = false;
        this._showVideoAd = false;
        this._showInterstitialAd = false;
        this._showBannerAd = false;
        this._showMoreGameView = false;
        this._bannerId = "";
        this._localLayerList = null;
        platform.Initialize();
        this._Platform = platform;
        this.Name = "YDHW-SDK:" + this.Name;
        this.Cache = new _Extensions_YDHWCache__WEBPACK_IMPORTED_MODULE_13__["YDHWCache"]();
        this.PowerSystem = new _Extensions_PowerSystem__WEBPACK_IMPORTED_MODULE_14__["PowerSystem"]();
        this.Config = window[_framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__["StorageAdapter"].SDK_CONFIG];
        this.AppId = String(this.Config.appid);
        this.AppKey = String(this.Config.appkey);
        this.PkgName = String(this.Config.pkg_name);
        this.Version = String(this.Config.version);
        this.SwitchLog = platform.IsDebug;
        this.BannerAdUnitIdList = this.Config.banner_ad_unit_id_list;
        this.InterstitialAdFirstShowWaitTime = Number(this.Config.interstitialAd_first_show_wait_time) || 0;
        this.InterstitialAdShowTimeInterval = Number(this.Config.interstitialAd_show_time_interval) || 0;
        this.SideBoxCount = Number(this.Config.side_box_count) || 0;
        if (!this.BannerAdUnitIdList || typeof this.BannerAdUnitIdList == "undefined") {
            this._Platform.IsDebug && console.error("YDHW_CONFIG.banner_ad_unit_id_list not exists");
        }
        this.InterstitialAdUnitIdList = this.Config.interstitial_ad_unit_id_list;
        if (!this.InterstitialAdUnitIdList || typeof this.InterstitialAdUnitIdList == "undefined") {
            this._Platform.IsDebug && console.error("YDHW_CONFIG.interstitial_ad_unit_id_list not exists");
        }
        this.SpreadAdUnitIdList = this.Config.spread_ad_unit_id_list;
        if (!this.SpreadAdUnitIdList || typeof this.SpreadAdUnitIdList == "undefined") {
            this._Platform.IsDebug && console.error("YDHW_CONFIG.spread_ad_unit_id_list not exists");
        }
        this.NativeAdUnitIdList = this.Config.native_ad_unit_id_list;
        if (!this.NativeAdUnitIdList || typeof this.NativeAdUnitIdList == "undefined") {
            this._Platform.IsDebug && console.error("YDHW_CONFIG.native_ad_unit_id_list not exists");
        }
        this.VideoAdUnitIdList = this.Config.video_ad_unit_id_list;
        if (!this.VideoAdUnitIdList || typeof this.VideoAdUnitIdList == "undefined") {
            this._Platform.IsDebug && console.error("YDHW_CONFIG.video_ad_unit_id_list not exists");
        }
    }
    _InnerLogin(caller, method) {
        this._Platform.IsDebug && console.log("CommerceMgr InnerLogin()");
        // 获取网络状态
        this._Platform.GetNetworkType(this, (netType) => {
            this._Platform.NetType = netType;
            this._Platform.IsDebug && console.log("GetNetworkType = ", netType);
        });
        let request = new _Model_User__WEBPACK_IMPORTED_MODULE_3__["LoginRequest"]();
        request.platform = this._Platform.PlatformType + "";
        request.appid = this.AppId;
        request.version = this.Version;
        request.code = this.Code;
        request.pkgName = this.PkgName;
        let shareInfo = this.ShareInfo();
        this._Platform.IsDebug && console.log("shareInfo=", shareInfo);
        request.shareInfo = shareInfo;
        let clientInfo = new _Model_User__WEBPACK_IMPORTED_MODULE_3__["ClientInfo"]();
        clientInfo.uuid = "";
        clientInfo.platform = this._Platform.PlatformType + "";
        clientInfo.brand = this._Platform.Brand;
        clientInfo.version = this.Version;
        clientInfo.model = this._Platform.Model;
        clientInfo.appName = "";
        clientInfo.resolution = this._Platform.Resolution;
        request.clientInfo = clientInfo;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Instance.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Login, request, this, (result) => {
            this._Platform.IsDebug && console.log("login result:", result);
            this.AccountId = result.accountId;
            this.NickName = result.nickName || "";
            this.AvatarUrl = result.avatarUrl || "";
            this.IsNewPlayer = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.newPlayer);
            this.OpenID = result.openid || "";
            this.AccountPass = result.accountPass || "";
            _framework_Utility_Url__WEBPACK_IMPORTED_MODULE_5__["Url"].SetAllUrlAccountPass(this.AccountPass);
            if (this.IsNewPlayer === true) {
                //todo:something
            }
            this.SwitchTouch = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.switchTouch) || false;
            this.SwitchPush = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.switchPush) || false;
            this.SwitchLog = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.switchLog) || false;
            this.SwitchLogin = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.switchLogin) || false;
            this.SwitchJump = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.switchJump) || false;
            this.SwitchShare = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.switchShare) || false;
            this.SwitchVideo = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.switchVideo) || false;
            this.SwitchEvent = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.switchEvent) || false;
            this.SwitchLayer = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.switchLayer) || false;
            this.SwitchResult = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.switchResult) || false;
            this.IsLowVersion = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.isLowVersion) || false;
            this.IsRealVersion = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(result.isRealVersion) || false;
            this.VideoFalseLimit = result.videoFalseLimit || 0;
            this.ShareFalseLimit = result.shareFalseLimit || 0;
            let moduleFalseList = result.moduleFalseList || [];
            let moduleList = result.moduleList || [];
            this.Strategy = new _Extensions_Strategy__WEBPACK_IMPORTED_MODULE_11__["Strategy"](moduleList, moduleFalseList);
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Instance.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Config, this, (result) => {
                result.forEach((element) => {
                    let tmpList = JSON.parse(element.value);
                    if (element.code == "b_t_s") { //banner定时刷新配置
                        this.BannerRefreshConfig = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["BannerRefreshConfig"]();
                        this.BannerRefreshConfig.IsOn = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(tmpList[0]);
                        this.BannerRefreshConfig.Interval = tmpList[1] || 10;
                        this.BannerRefreshConfig.IsForceRefresh = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(tmpList[2]);
                        this.BannerRefreshConfig.MinimumInterval = tmpList[3] || 3;
                    }
                    else if (element.code == "power") { //体力系统
                        this.PowerSystemConfig = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["PowerSystemConfig"]();
                        this.PowerSystemConfig.DefaultPowerValue = tmpList[0] || 5;
                        this.PowerSystemConfig.UpperLimit = tmpList[1] || 5;
                        this.PowerSystemConfig.AutoRecoveryTime = tmpList[2] || 300;
                        this.PowerSystemConfig.VideoPowerWeight = tmpList[3] || 1;
                        this.PowerSystemConfig.IsOn = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(tmpList[4]);
                    }
                    else if (element.code == "v_u_c") { //视频解锁关卡
                        this.VideoUnlockLevelConfig = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["VideoUnlockLevelConfig"]();
                        this.VideoUnlockLevelConfig.IsOn = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(tmpList[0]);
                        this.VideoUnlockLevelConfig.CustomNumber = tmpList[1] || 2;
                        this.VideoUnlockLevelConfig.IntervalCount = tmpList[2] || 0;
                    }
                    else if (element.code == "dp_s") { //深度屏蔽开关
                        this.DeepSheildRuleConfig = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["DeepSheildRuleConfig"]();
                        this.DeepSheildRuleConfig.Type = tmpList[0] || 0;
                        this.DeepSheildRuleConfig.CustomNumberCounter = tmpList[1] || 5;
                        this.DeepSheildRuleConfig.ExecellentUserSwitch = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(tmpList[2]);
                        this.DeepSheildRuleConfig.WatchVideoCounter = tmpList[3] || 2;
                        this.DeepSheildRuleConfig.DayCustomNumber = tmpList[4] || 0;
                    }
                    else if (element.code == "m_t") { //定时器开关误触
                        this.TimingMisTouchSwitchConfig = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["TimingMisTouchSwitchConfig"]();
                        this.TimingMisTouchSwitchConfig.IsOn = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(tmpList[0]);
                        this.TimingMisTouchSwitchConfig.StartTimeHour = tmpList[1] || 0;
                        this.TimingMisTouchSwitchConfig.StartTimeMinute = tmpList[2] || 0;
                        this.TimingMisTouchSwitchConfig.EndTimeHour = tmpList[3] || 23;
                        this.TimingMisTouchSwitchConfig.EndTimeMinute = tmpList[4] || 0;
                    }
                    else if (element.code == "d_a") { //开发者账号（白名单
                        this.DeveloperAccountConfig = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["DeveloperAccountConfig"]();
                        this.DeveloperAccountConfig.IsOn = _framework_Utility_VerifyUtility__WEBPACK_IMPORTED_MODULE_4__["VerifyUtility"].ToBoolean(tmpList[0]);
                        for (let i = 1, l = tmpList.length; i < l; i++) {
                            this.DeveloperAccountConfig.Accounts.push(tmpList[i]);
                        }
                    }
                });
                this.PowerSystem.Initialize();
                caller && method.call(caller, true);
            });
            // SDK.DataMgr.CustomConfig(this, (result: YDHW.GameBase.ICustomConfigResult): void => {
            //     this.CustomConfig = result;
            //     caller && method.call(caller, true);
            // });
        }, () => {
            caller && method && method.call(caller, false);
        });
        this._Platform.IsDebug && console.log("SDKPlatform InnerLogin() end");
    }
    Initialize() {
        super.Initialize();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TotalPowerCounter, 0).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].LastPowerAddTimestamp, new Date().getTime());
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TodayWatchVideoCounter, 0).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].LastWatchVideoTimestamp, new Date().getTime()).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].ListUnlockVideoLevelNumber, []).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].MaxCustomNumber, 0).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].LastCustomNumber, 0).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TodayCustomNumberCounter, 0).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TotalCustomNumberCounter, 0).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].LastPlayTimestamp, new Date().getTime());
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].ListUnlockVideoLevelNumber, []).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].VideoFalseLimit, 0).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].ShareFalseLimit, 0).Store();
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].PlatformUserInfo, null);
        this.DeclareVariable(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].ListStatisticsLayer, null).Store();
    }
    _AddUnlockVideoLevelNumber(index) {
        if (!index) {
            return;
        }
        this.AddValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TodayWatchVideoCounter, 1);
        this.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].LastWatchVideoTimestamp, new Date().getTime());
        let list = this.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].ListUnlockVideoLevelNumber, []);
        if (list.indexOf(index) !== -1) {
            this.SwitchLog && this.Log("AddUnlockLevelNumber index=" + index);
            list.push(index);
            this.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].ListUnlockVideoLevelNumber, list);
        }
    }
    LoginAddress() {
        return "";
    }
    Login(caller, method) {
    }
    GetLeftTopBtnPosition() {
        return null;
    }
    ShareInfo() {
        return null;
    }
    /**
     *
     * @param type 0:创建,1:加载成功,2:展示,3:点击,4:加载失败
     * @param adId 广告id
     */
    StatisticBanner(type, adId) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Banner, type, adId, this, (isOk) => {
            if (isOk === true) {
                this.Log("StatisticBanner success");
            }
        });
    }
    StatisticVideo(type, adId, scene) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Video, type, adId, scene, this, (isOk) => {
            if (isOk === true) {
                this.Log("StatisticVideo success");
            }
        });
    }
    StatisticInterstitial(type, adId) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].InterstitialAd, type, adId, this, (isOk) => {
            if (isOk === true) {
                this.Log("StatisticInterstitial success");
            }
        });
    }
    /**
     *
     * @param details 对象或数组
     */
    StatisticResult(details) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Result, details, this, (isOk) => {
            if (isOk === true) {
                this.Log("StatisticResult success");
            }
        });
    }
    StatisticEvent(event, scene) {
        if (this.SwitchEvent === false) {
            this.SwitchLog && this.Log("StatisticEvent is stop by SwitchEvent");
            return;
        }
        let request = new _Model_Default__WEBPACK_IMPORTED_MODULE_9__["StatisticEventRequest"]();
        request.event = event;
        request.scene = scene;
        request.time = new Date().getTime();
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Event, [request], this, (isOk) => {
            if (isOk === true) {
                this.Log("StatisticEvent success");
            }
        });
    }
    StatisticInterstitialAd(type, adId) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].InterstitialAd, type, adId, this, (isOk) => {
            if (isOk === true) {
                this.Log("StatisticInterstitialAd success");
            }
        });
    }
    StatisticDuration() {
        let nowTime = new Date().getTime();
        let duration = nowTime - this._LastPlayTime;
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Duration, 0, duration, this, (isOk) => {
            if (isOk === true) {
                this.Log("StatisticDuration success");
            }
        });
    }
    StatisticClickOut(request) {
        if (!request) {
            this.Log("StatisticClickOut request is null");
            return;
        }
        if (!request.iconId) {
            this.Log("StatisticClickOut request.iconId is null");
            return;
        }
        if (!request.action) {
            this.Log("StatisticClickOut request.action is null");
            return;
        }
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].ClickOut, request, this, (isOk) => {
            if (isOk === true) {
                this.Log("StatisticClickOut success");
            }
        });
    }
    RefreshLastPlayTime() {
        this._LastPlayTime = new Date().getTime();
    }
    ShowShareOrVideo(channel, module, caller, method) {
        if (!channel) {
            this.Error("channel is null");
            return;
        }
        if (!module) {
            this.Error("module is null");
            return;
        }
        this.Strategy && this.Strategy.ShowShareVideo(channel, module, caller, method);
    }
    SwitchView(isMisTouched) {
        if (!this._Platform.BannerAd && this.SwitchLog) {
            this.Warn("BannerAd is null");
        }
        else if (this.BannerRefreshConfig && this.BannerRefreshConfig.IsOn) {
            let nowTime = new Date().getTime();
            let lastTime = this._LastTimeRefreshBannerAd || nowTime;
            let interval = this.BannerRefreshConfig.Interval || _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_8__["SDKConfig"].DefaultIntervalForceRefreshBannerAd;
            if (nowTime - lastTime >= interval * 1000) {
                if (this._Platform.IsShowBannerAd && this._IsSmallBannerAd) {
                    this.CreateSmallBannerAd(isMisTouched, true, this, () => {
                    });
                }
                else if (this._Platform.IsShowBannerAd) {
                    this.CreateBannerAd(isMisTouched, true, this, (isOk) => {
                    });
                }
                else {
                    this._LastTimeRefreshBannerAd = nowTime - (this.BannerRefreshConfig.Interval * 1000 + 1000);
                }
            }
        }
    }
    GetDeepTouchInfo(customNumber) {
        let strategy = this.Strategy && this.Strategy.GetDeepMisTouch(customNumber);
        if (this.SwitchLog)
            this.Log("GetDeepTouchInfo:", strategy, customNumber);
        let deepTouchInfo = new _Model_Default__WEBPACK_IMPORTED_MODULE_9__["DeepTouchInfo"]();
        deepTouchInfo.deepTouch = strategy ? true : false;
        if (this.ListCustomConfig && this.ListCustomConfig.length > 0) {
            this.ListCustomConfig.forEach(element => {
                //如果是开关的话就返回
                if (parseInt(element.type) == 2) {
                    if (!strategy) {
                        element.value = "0";
                    }
                    deepTouchInfo.ListCustomInfo.push(element);
                }
            });
        }
        return deepTouchInfo;
    }
    GetCustomConfig(caller, method) {
        if (this.ListCustomConfig) {
            caller && method && method.call(caller, this.ListCustomConfig);
        }
        else {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].CustomConfig, this, (result) => {
                this.ListCustomConfig = result;
                caller && method && method.call(caller, this.ListCustomConfig);
            });
        }
    }
    GameOver(index) {
        this.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].LastCustomNumber, index);
        let maxCustomNumber = this.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].MaxCustomNumber, 0);
        if (index > maxCustomNumber) {
            this.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].MaxCustomNumber, index);
        }
        this.AddValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TodayCustomNumberCounter, 1);
        this.AddValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TotalCustomNumberCounter, 1);
        this.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].LastPlayTimestamp, new Date().getTime());
    }
    /**
     * 视频解锁
     * @param index
     */
    IsUnlockVideo(index) {
        return this.Strategy && this.Strategy.IsUnlockVideo(index);
    }
    /**
     * 获取体力信息
     */
    GetPowerInfo() {
        // SDK.Data.InvokeMethod(SDK_NET_API.Config, this, (result:YDHW.GameBase.IConfig[]):void => {
        // });
        if (!this.PowerSystemConfig) {
            this.Error("GetPowerInfo error: PowerInfo is null");
            return null;
        }
        return this.PowerSystemConfig;
    }
    ListenOnPowerChanged(caller, method) {
        this.PowerSystem.ListenOnPowerChange(caller, method);
    }
    AddPower(type) {
        if (!this.PowerSystemConfig || this.PowerSystemConfig.IsOn === false) {
            this.SwitchLog && this.Warn("Power System is Off");
            return;
        }
        if (this.PowerSystemConfig) {
            let total = _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Commerce.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TotalPowerCounter, 0);
            let power = this.PowerSystemConfig.VideoPowerWeight;
            if (type == _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_POWER_RECOVERY_TYPE"].WatchVideo) {
                total += power;
            }
            else if (type == _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_POWER_RECOVERY_TYPE"].AutoRecovery) {
                total += 1;
            }
            this.PowerSystem && this.PowerSystem.SetPower(total, type);
        }
    }
    SetPower(power, type) {
        if (!this.PowerSystemConfig || this.PowerSystemConfig.IsOn === false) {
            this.SwitchLog && this.Warn("Power System is Off");
            return;
        }
        this.PowerSystem && this.PowerSystem.SetPower(power, type);
    }
    GetPower() {
        return this.PowerSystem && this.PowerSystem.GetPower();
    }
    GetSideBox(caller, method) {
        if (this.ListBoxConfig) {
            caller && method && method.call(caller, this.ListBoxConfig);
        }
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].SideBox, this, (result) => {
            this.ListBoxConfig = result;
            caller && method && method.call(caller, this.ListBoxConfig);
        });
    }
    GetScoreBoardList(caller, method) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].ScoreBoard, "scoreboard", caller, method);
    }
    GetScoreBoardAward(id, caller, method) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].GetBoardAward, "scoreboard", id, caller, method);
    }
    ShareCard(scene, caller, method) {
        this._CurrentShareScene = (scene !== "") ? scene : "default";
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Instance.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Data_ShareCard, scene, this, (result) => {
            this.Strategy.InitShareConfig(scene, result);
            caller && method && method.call(caller, result);
        });
    }
    GetTodayBoutCount() {
        let counter = this.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TodayCustomNumberCounter, 0);
        return counter;
    }
    GetTotalBoutCount() {
        let counter = this.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TotalCustomNumberCounter, 0);
        return counter;
    }
    GetLastBountNumber() {
        let index = this.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].LastCustomNumber, 0);
        return index;
    }
    GetMaxBountNumber() {
        let index = this.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].MaxCustomNumber, 0);
        return index;
    }
    GetTodayWatchVideoCounter() {
        let counter = this.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TodayWatchVideoCounter, 0);
        return counter;
    }
    _CreateBannerAd(isSmall, isMisTouch, isShow, style, caller, method, onResize) {
        if (!this.BannerAdUnitIdList || this.BannerAdUnitIdList.length == 0) {
            this.Error("BannerAdUnitIdList is empty.");
        }
        else {
            if (this._Platform.BannerAd) {
                this._Platform.CloseBannerAd();
                this._showBannerAd = false;
            }
            let me = this;
            let index = Math.floor(Math.random() * this.BannerAdUnitIdList.length);
            let adId = this.BannerAdUnitIdList[index];
            let ww = isSmall ? 300 : this._Platform.SystemInfo.windowWidth;
            let left = (this._Platform.SystemInfo.windowWidth - ww) / 2;
            let defaultStyle = new _Model_Default__WEBPACK_IMPORTED_MODULE_9__["BannerAdStyle"]();
            defaultStyle.left = left;
            defaultStyle.top = this._Platform.SystemInfo.windowHeight - this._Platform.SystemInfo.windowWidth * 0.28695;
            defaultStyle.width = ww;
            this._Platform.CreateBannerAd(adId, isMisTouch, defaultStyle, this, onResize);
            this.StatisticBanner(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_STATISTIC_TYPE"].CREATE, adId);
            this._bannerId = adId;
            //是否定时刷新banner
            let isOn = this.BannerRefreshConfig && this.BannerRefreshConfig.IsOn;
            if (isOn) {
                this._IsSmallBannerAd = isSmall;
            }
            this._Platform.BannerAd.onLoad(() => {
                if (me.SwitchLog) {
                    me.Log("广告加载成功");
                }
                ;
                isShow && me.ShowBannerAd();
                caller && method && method.call(caller, true);
            });
            this._Platform.BannerAd.onError((error) => {
                if (me.SwitchLog) {
                    me.Error("oNeRROR事件里:" + error);
                }
                this._Platform.CloseBannerAd();
                this._showBannerAd = false;
                if (isOn) {
                    let delay = me.BannerRefreshConfig.Interval;
                    if (me.SwitchLog) {
                        me.Log(`将在${delay}秒后重新创建banner(调用ylBannerAdHide会取消重试`);
                    }
                    _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].RemoveDelayTimer(me._BannerAdDelayTimerId);
                    me._BannerAdDelayTimerId = _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].Delay(delay * 1000, me, () => {
                        me._CreateBannerAd(isSmall, isMisTouch, isShow, style, onResize, caller, method); //延迟刷新banner
                    });
                    caller && method && method.call(caller, false);
                }
            });
        }
    }
    CreateBannerAd(isMisTouch, isShow, caller, method) {
        this._CreateBannerAd(false, isMisTouch, isShow, null, caller, method, null);
    }
    CreateSmallBannerAd(isMisTouch, isShow, caller, method) {
        this._CreateBannerAd(true, isMisTouch, isShow, null, caller, method, null);
    }
    CreateCustomBannerAd(isMisTouch, isShow, style, caller, method, onResize) {
        this._CreateBannerAd(false, isMisTouch, isShow, style, caller, method, onResize);
    }
    BannerAdChangeSize(style) {
        this._Platform.ChangeBannerStyle(style);
    }
    ShowBannerAd() {
        if (this.SwitchLog) {
            this.Log("ShowBannerAd");
        }
        let nowTime = new Date().getTime();
        let last = this._LastTimeRefreshBannerAd || nowTime;
        let isSmall = false;
        let IsOn = false;
        let interval = 0;
        let create = false; //是否走创建banner流程
        if (this.BannerRefreshConfig) {
            IsOn = this.BannerRefreshConfig.IsOn || false;
            interval = this.BannerRefreshConfig.Interval || _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_8__["SDKConfig"].DefaultIntervalForceRefreshBannerAd;
        }
        if (this._Platform.BannerAd) {
            if (IsOn) {
                isSmall = this._IsSmallBannerAd || false;
            }
            let isRefresh = ((nowTime - last) >= _Config_SDKConfig__WEBPACK_IMPORTED_MODULE_8__["SDKConfig"].DefaultIntervalForceRefreshBannerAd * 1000);
            if (IsOn && isRefresh) {
                //到时间定时更新banner
                if (this.SwitchLog) {
                    this.Log("ShowBannerAd CreateBannerAd");
                }
                create = true;
                this._CreateBannerAd(isSmall, this._Platform.IsMisTouchBannerAd, true, this._Platform.BannerStyle, this._Platform.OnResizeBannerAd, null, null);
            }
            if (!create) {
                this._Platform.SetBannerVisible(true);
                this._showBannerAd = true;
            }
            this._LastTimeRefreshBannerAd = nowTime;
        }
        if (!create && IsOn) {
            if (this.SwitchLog) {
                this.Log(`将在${interval}秒后重新创建banner`);
            }
            _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].RemoveDelayTimer(this._BannerAdDelayTimerId);
            this._BannerAdDelayTimerId = _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].Delay(interval * 1000, this, () => {
                this._CreateBannerAd(isSmall, this._Platform.IsMisTouchBannerAd, true, this._Platform.BannerStyle, this._Platform.OnResizeBannerAd, null, null); //延迟刷新banner
            });
        }
    }
    HideBannerAd() {
        if (this.SwitchLog) {
            this.Log("HideBannerAd");
        }
        this._Platform.SetBannerVisible(false);
        _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].RemoveDelayTimer(this._BannerAdDelayTimerId);
        this._BannerAdDelayTimerId = "";
    }
    ChangeBannerStyle(style) {
        this._Platform.ChangeBannerStyle(style);
    }
    CreateRewardVideoAd() {
        if (!this.VideoAdUnitIdList || this.VideoAdUnitIdList.length == 0) {
            this.Error("VideoAdUnitIdList is empty.");
        }
        else if (!this._Platform.RewardVideoAd) {
            let me = this;
            let index = Math.floor(Math.random() * this.VideoAdUnitIdList.length);
            let adId = this.VideoAdUnitIdList[index];
            this._Platform.CreateRewardVideoAd(adId, this, () => {
                me._IsRewardVideoAdFinish = true;
                _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].RemoveDelayTimer(me._RewardVideoDelayTimerId);
                me._RewardVideoDelayTimerId = "";
            }, (result) => {
                if (result && result.isEnded || result === undefined) {
                    if (me.SwitchLog) {
                        me.Log("视频播放完成");
                    }
                    me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_VIDEO_PLAY_TYPE"].VIDEO_PLAY_FINISH);
                    me._AddUnlockVideoLevelNumber(me._UnlockCustomNumber);
                    if (me._IsAddPower) {
                        me._AddPowerByVideo();
                    }
                }
                else { //播放中途退出
                    me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_VIDEO_PLAY_TYPE"].VIDEO_PLAY_CANCEL);
                    me.SwitchLog && me.Log("视频播放中途取消");
                }
                me._UnlockCustomNumber = null;
                me._EvtOnCloseRewardVideoAd = null;
                me._showVideoAd = false;
            }, (error) => {
                me.SwitchLog && me.Log("RewardVideoAd onError:", error);
                me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_VIDEO_PLAY_TYPE"].VIDEO_PLAY_FAIL);
                me._EvtOnCloseRewardVideoAd = null;
                me._UnlockCustomNumber = null;
                me._showVideoAd = false;
            });
        }
    }
    ShowRewardVideoAd(unlockCustomNumber, isAddPower, caller, onClose) {
        let me = this;
        this._IsRewardVideoAdFinish = false;
        this._UnlockCustomNumber = unlockCustomNumber;
        this._CallerRewardVideoAd = caller;
        this._EvtOnCloseRewardVideoAd = onClose;
        this._IsAddPower = isAddPower || false;
        if (!this._Platform.RewardVideoAd) {
            this.CreateRewardVideoAd();
            if (!this._Platform.RewardVideoAd) {
                return;
            }
        }
        this._Platform.ShowRewardVideoAd(this, () => {
            me.SwitchLog && me.Log("视频播放成功");
            me._showVideoAd = true;
        }, () => {
            me.SwitchLog && me.Error("视频播放失败");
            me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_VIDEO_PLAY_TYPE"].VIDEO_PLAY_FAIL);
            me._EvtOnCloseRewardVideoAd = null;
            me._showVideoAd = false;
        });
        if (this._RewardVideoDelayTimerId.length == 0) {
            this._RewardVideoDelayTimerId = _framework_Utility_TimerUtility__WEBPACK_IMPORTED_MODULE_10__["default"].Delay(4 * 1000, this, () => {
                if (this._IsRewardVideoAdFinish === false && this._EvtOnCloseRewardVideoAd) {
                    me.SwitchLog && me.Log("4秒内回调检测判断无广告返回，则触发回调传回播放失败");
                    me._CallerRewardVideoAd && me._EvtOnCloseRewardVideoAd && me._EvtOnCloseRewardVideoAd.call(me._CallerRewardVideoAd, _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_VIDEO_PLAY_TYPE"].VIDEO_PLAY_FAIL);
                    me._EvtOnCloseRewardVideoAd = null;
                }
            });
        }
    }
    StatisticShareCard(id) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Statistic_ShareCard, id, this, (isOk) => {
        });
    }
    GetLayerList(onReturn) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Instance.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Data_Layer, this, (layerList) => {
            this.ListLayer = layerList;
            //读取本地缓存的今日已统计过的流失路径列表                          
            let localData = this.GetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].ListStatisticsLayer, null);
            if (localData) {
                this._localLayerList = localData;
            }
            onReturn && onReturn(layerList);
        });
    }
    NavigateToMiniProgram(id, toAppId, toUrl, source, caller, method) {
        let layer = source ? source : "default";
        let request = new _Model_Statistic__WEBPACK_IMPORTED_MODULE_12__["ClickOutRequest"]();
        request.iconId = id;
        request.target = toAppId;
        request.souce = layer;
        this._Platform.NavigateToMiniProgram(this.AppId, toAppId, toUrl, this, () => {
            if (this.SwitchLog)
                this.Log("小游戏跳转-跳转成功！");
            request.action = "enable";
            this.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].ClickOut, request, null);
            this.Cache.RemoveItemFrom(toAppId);
            caller && method && method.call(caller, true);
        }, () => {
            if (this.SwitchLog)
                this.Log('小游戏跳转-跳转失败！');
            request.action = "cancel";
            this.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].ClickOut, request, null);
            caller && method && method.call(caller, false);
        });
    }
    ShowUserInfoButton(info, caller, method) {
        this.SwitchLog && this.Log("ShowUserInfoButton");
        this._Platform.ShowUserInfoButton(info, this, (data) => {
            this.Log("ShowUserInfoButton");
            this.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].PlatformUserInfo, data);
            caller && method && method.call(caller, data);
        }, (error) => {
            this.Warn("ShowUserInfoButton error: " + error);
            caller && method && method.call(caller, null);
        });
    }
    HideUserInfoButton() {
        this._Platform.HideUserInfoButton();
    }
    _IsCanCreateInterstitialAd() {
        let nowTime = new Date().getTime();
        let interval = (nowTime - this._LastTimeCreateInterstitialAd) / 1000;
        let canCreate = false;
        if (this.SwitchLog)
            this.Log("CreateIntertitialAd interval:", interval);
        if (this._Platform.InterstitialAd) {
            //如果存在InterstitialAd对象，标识已经创建过插屏广告
            let ifs = this.InterstitialAdFirstShowWaitTime || 0;
            canCreate = interval >= ifs;
        }
        else {
            let is = this.InterstitialAdShowTimeInterval || 0;
            canCreate = interval >= is;
        }
        if (!canCreate) {
            if (this.SwitchLog)
                this.Warn("创建插屏广告太频繁，请稍后再试");
        }
        return canCreate;
    }
    CreateInterstitialAd(isShow, caller, onLoad, onClose, onError) {
        if (!this.InterstitialAdUnitIdList || this.InterstitialAdUnitIdList.length == 0) {
            this.SwitchLog && this.Log("InterstitialAdUnitIdList is empty");
            return;
        }
        if (!this._IsCanCreateInterstitialAd()) {
            caller && onError && onError.call(caller);
            return;
        }
        let me = this;
        let index = Math.floor(Math.random() * this.InterstitialAdUnitIdList.length);
        let adId = this.InterstitialAdUnitIdList[index];
        if (this._Platform.InterstitialAd) {
            this._Platform.ClearInterstitialAd();
        }
        this.StatisticInterstitialAd(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_STATISTIC_TYPE"].CREATE, adId);
        this._Platform.CreateInterstitialAd(adId, this, () => {
            me.StatisticInterstitialAd(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_STATISTIC_TYPE"].LOAD_SUCCESS, adId);
            if (isShow) {
                me.ShowInterstitialAd();
            }
            else {
                caller && onLoad && onLoad.call(caller);
            }
            me.SwitchLog && me.Log("InterstitialAd OnLoad");
        }, (result) => {
            me.StatisticInterstitialAd(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_STATISTIC_TYPE"].CLOSE, adId);
            caller && onClose && onClose.call(caller, result);
            me._showInterstitialAd = false;
        }, (error) => {
            me.StatisticInterstitialAd(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_STATISTIC_TYPE"].LOAD_FAIL, adId);
            me.SwitchLog && me.Warn("InterstistAd Error:" + error + " adId:" + adId);
            caller && onError && onError.call(caller, error);
            me._showInterstitialAd = false;
        });
    }
    ShowInterstitialAd() {
        if (!this._Platform.InterstitialAd) {
            return;
        }
        this._Platform.ShowInterstitialAd(this, () => {
            this._showInterstitialAd = true;
            this.StatisticInterstitialAd(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_STATISTIC_TYPE"].SHOW, this._Platform.InterstitialAdId);
        });
    }
    GetSharingResults(shareInfo, caller, method) {
        shareInfo && this.Strategy && this.Strategy.GetShareResult(shareInfo, this, (shareBackInfo) => {
            this.Warn("Sharing Results");
            caller && this._EvtOnShareCardBack && method.call(caller, shareBackInfo);
            this._EvtOnShareCardBack = null;
        });
    }
    StatisticLayer(layerPath) {
        let nowTime = new Date().getTime();
        if (this._localLayerList) {
            //清理昨天的本地缓存数据
            let getTime = this._localLayerList.get_time;
            if (getTime) {
                //判断本地缓存最后一次流失打点时间是否是今天(只保存今天的)
                if (!this.isToday(getTime)) {
                    this._localLayerList = null;
                }
            }
            else {
                //清除本地缓存
                this._localLayerList = null;
            }
        }
        //先看本地缓存已经打点的流失路径列表中是否有该路径
        let localInfo = this._localLayerList;
        let hasStatic = false; //是否已经打过点
        if (localInfo && localInfo.s_layers) {
            let index = localInfo.s_layers.indexOf(layerPath);
            hasStatic = (index > 0);
        }
        let pathList = this.ListLayer;
        let hasLayer = false;
        let p_index = -1;
        for (let i = 0; i < pathList.length; i++) {
            let item = pathList[i];
            if (item.layerPath == layerPath) {
                hasLayer = true;
                p_index = i;
                break;
            }
        }
        if (hasLayer && p_index != -1) {
            if (!hasStatic) {
                if (!(localInfo && localInfo.get_time && localInfo.s_layers)) {
                    //如果今日没有打点，则从最开始连续打点
                    for (let i = 0; i <= p_index; i++) {
                        let lPath = pathList[i].layerPath;
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Statistic_Layer, lPath, this, (isOk) => { });
                        if (!this._localLayerList) {
                            this._localLayerList = new _Model_Data__WEBPACK_IMPORTED_MODULE_7__["LocalLayerInfo"]();
                            this._localLayerList.s_layers = [];
                        }
                        this._localLayerList.s_layers.push(lPath);
                        this._localLayerList.get_time = nowTime;
                    }
                }
                else {
                    //接着往后打点
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Statistic_Layer, layerPath, this, (isOk) => { });
                    this._localLayerList.get_time = nowTime;
                    this._localLayerList.s_layers.push(layerPath);
                }
                this.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].ListStatisticsLayer, this._localLayerList);
            }
            else {
                //今日已有打点,则只打点，不存储
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Statistic.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].Statistic_Layer, layerPath, this, (isOk) => { });
                this._localLayerList.get_time = nowTime;
            }
        }
        else {
            console.log("statisticLayer-layerPath not found", 'warn');
        }
        // SDK.Statistic.InvokeMethod(SDK_NET_API.Statistic_Layer, layerPath, this, (isOk: boolean) => {
        // });
    }
    //判断传入的时间戳是否是今天
    //completeTime 需要比较的时间戳 
    isToday(completeTime) {
        let nowTime = new Date();
        let nowYear = nowTime.getFullYear();
        let nowMonth = nowTime.getMonth() + 1;
        let nowDay = nowTime.getDate();
        let comT = new Date(completeTime);
        let comTYear = comT.getFullYear();
        let comTMonth = comT.getMonth() + 1;
        let comTDay = comT.getDate();
        return (nowYear == comTYear && nowMonth == comTMonth && nowDay == comTDay);
    }
    IsCanRewardByVideoOrShare(type) {
        let IsCanReward = false;
        let videoFalseLimit = this.VideoFalseLimit;
        let shareFalseLimit = this.ShareFalseLimit;
        if (type == _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_SHARE_TYPE"].Video) {
            if (videoFalseLimit && videoFalseLimit > 0) {
                IsCanReward = true;
                videoFalseLimit -= 1;
                videoFalseLimit = videoFalseLimit < 0 ? 0 : videoFalseLimit;
                this.VideoFalseLimit = videoFalseLimit;
            }
        }
        else if (type == _SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["EM_SHARE_TYPE"].Share) {
            if (shareFalseLimit && shareFalseLimit > 0) {
                IsCanReward = true;
                shareFalseLimit -= 1;
                shareFalseLimit = shareFalseLimit < 0 ? 0 : shareFalseLimit;
                this.ShareFalseLimit = shareFalseLimit;
            }
        }
        if (IsCanReward) {
            this.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].VideoFalseLimit, this.VideoFalseLimit);
            this.SetValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].ShareFalseLimit, this.ShareFalseLimit);
        }
        return IsCanReward;
    }
    GetShareRewardLimit() {
        return this.ShareFalseLimit;
    }
    GetVideoRewardLimit() {
        return this.VideoFalseLimit;
    }
    GetServerInfo(caller, method) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_2__["SDK"].Data.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_NET_API"].MyInfo, caller, method);
    }
    Hook(name, caller, method, ...args) {
        this._Platform.Hook(name, caller, method);
    }
    _AddPowerByVideo() {
        if (!this.PowerSystemConfig || this.PowerSystemConfig.IsOn === false) {
            this.SwitchLog && this.Log("Power System is off");
            return;
        }
        let power = this.PowerSystemConfig.VideoPowerWeight;
        this.AddValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TotalPowerCounter, power);
    }
    _AddPowerAuto() {
        if (!this.PowerSystemConfig || this.PowerSystemConfig.IsOn === false) {
            this.SwitchLog && this.Log("Power System is off");
            return;
        }
        let power = this.PowerSystemConfig.VideoPowerWeight;
        this.AddValueT(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_6__["SDK_VARS"].TotalPowerCounter, 1); //每次自动恢复一点
    }
    Log(message, ...args) {
        this.SwitchLog && super.Log(message, ...args);
    }
    Debug(message, ...args) {
        this.SwitchLog && super.Debug(message, ...args);
    }
    Info(message, ...args) {
        this.SwitchLog && super.Info(message, ...args);
    }
    Warn(message, ...args) {
        this.SwitchLog && super.Warn(message, ...args);
    }
    Error(message, ...args) {
        this.SwitchLog && super.Error(message, ...args);
    }
}


/***/ }),

/***/ "./sdk/Manager/DataMgr.ts":
/*!********************************!*\
  !*** ./sdk/Manager/DataMgr.ts ***!
  \********************************/
/*! exports provided: DataMgr */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataMgr", function() { return DataMgr; });
/* harmony import */ var _framework_Manager_DataManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../framework/Manager/DataManager */ "./framework/Manager/DataManager.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Base/SDKPlatform */ "./sdk/Base/SDKPlatform.ts");
/* harmony import */ var _Assist_Log__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Assist/Log */ "./sdk/Assist/Log.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");





class DataMgr extends _framework_Manager_DataManager__WEBPACK_IMPORTED_MODULE_0__["DataManager"] {
    Initialize() {
        this.UrlConfig = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("获取游戏配置列表")
            .Category("gamebase")
            .Router("config")
            .End();
        this.UrlConfigFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("强制获取游戏配置列表")
            .Category("gamebase")
            .Router("config")
            .Append("final")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Config, this, (caller, method) => {
            this.UrlConfig.AsGet().End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("Config response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Config Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    this.UrlConfigFinal.AsGet()
                        .Param("appid", _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.AppId)
                        .End()
                        .OnReceive(this, (response) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("ConfigForce response=", response);
                        if (response.code === 0) {
                            caller && method && method.call(caller, response.result);
                        }
                        else {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                            let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("ConfigForce Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                            caller && method && method.call(caller, null);
                        }
                    }).OnError(this, (error) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
                    }).OnException(this, (data) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
                    }).Send();
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this.UrlCustomConfig = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("获取自定义配置列表")
            .Category("gamebase")
            .Router("customconfig")
            .End();
        this.UrlCustomConfigFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("强制获取自定义配置列表")
            .Category("gamebase")
            .Router("customconfig")
            .Append("final")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].CustomConfig, this, (caller, method) => {
            this.UrlCustomConfig.AsGet().End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("CustomConfig response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("CustomConfig Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    this.UrlCustomConfigFinal.AsGet()
                        .Param("appid", _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.AppId)
                        .Param("version", _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Version)
                        .End()
                        .OnReceive(this, (response) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("CustomConfigForce response=", response);
                        if (response.code === 0) {
                            caller && method && method.call(caller, response.result);
                        }
                        else {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                            let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("CustomConfigFinal Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                            caller && method && method.call(caller, null);
                        }
                    }).OnError(this, (error) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
                    }).OnException(this, (data) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
                    }).Send();
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this.UrlDataDecode = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("获取平台加密数据")
            .Category("gamebase")
            .Router("data")
            .Append("decode")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].DataDecode, this, (request, caller, method) => {
            this.UrlDataDecode.AsPost().Post(request)
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("DataDecode Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("CustomConfig Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, null);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this.UrlGetBoardAward = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("领取积分墙奖励")
            .Category("gamebase")
            .Router("getboardaward")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].GetBoardAward, this, (module, awardId, caller, method) => {
            this.UrlGetBoardAward.AsPost().Post({ module: module, awardId: awardId })
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("GetBoardAward Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("GetBoardAward Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, null);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this.UrlLayer = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("获取游戏流失路径列表")
            .Category("gamebase")
            .Router("layer")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Data_Layer, this, (caller, method) => {
            this.UrlLayer.AsGet().End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("Layer response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Layer Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, null);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this.UrlScoreBoard = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .AsPost()
            .Description("获取积分墙列表")
            .Category("gamebase")
            .Router("scoreboard")
            .End();
        this.UrlScoreBoardFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("强制获取积分墙列表")
            .Category("gamebase")
            .Router("scoreboard")
            .Append("final")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].ScoreBoard, this, (module, caller, method) => {
            this.UrlScoreBoard.AsPost().Post({ module: module })
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("ScoreBoard Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("ScoreBoard Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    this.UrlScoreBoardFinal.AsGet()
                        .Param("appid", _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.AppId)
                        .Param("version", _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Version)
                        .Param("module", module)
                        .End()
                        .OnReceive(this, (response) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("ScoreBoard Response=", response);
                        if (response.code === 0) {
                            caller && method && method.call(caller, response.result);
                        }
                        else {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                            let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("ScoreBoard Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                            caller && method && method.call(caller, null);
                        }
                    }).OnError(this, (error) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
                    }).OnException(this, (data) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
                    }).Send();
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this.UrlSideBox = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("获取侧边栏列表")
            .Category("gamebase")
            .Router("sidebox")
            .End();
        this.UrlSideBoxFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("强制获取侧边栏列表")
            .Category("gamebase")
            .Router("sidebox")
            .Append("final")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].SideBox, this, (caller, method) => {
            this.UrlSideBox.AsGet().End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("SideBox response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("SideBox Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    this.UrlSideBoxFinal.AsGet()
                        .Param("appi", _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.AppId)
                        .Param("version", _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.Version).End()
                        .OnReceive(this, (response) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("SideBoxForce response=", response);
                        if (response.code === 0) {
                            caller && method && method.call(caller, response.result);
                        }
                        else {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                            let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("SideBoxFinal Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                            caller && method && method.call(caller, null);
                        }
                    }).OnError(this, (error) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
                    }).OnException(this, (data) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
                    }).Send();
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this.UrlShareCard = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("获取分享图列表")
            .Category("gamebase")
            .Router("sharecard")
            .End();
        this.UrlShareCardFinal = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("强制获取分享图列表")
            .Category("gamebase")
            .Router("sharecard")
            .Append("final")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Data_ShareCard, this, (scene, caller, method) => {
            this.UrlShareCard.AsPost().Post({ scene: scene })
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("ShareCard response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("ShareCard Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    this.UrlShareCardFinal.AsGet()
                        .Param("appid", _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Commerce.AppId)
                        .Param("scene", scene)
                        .End()
                        .OnReceive(this, (response) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("ShareCardFinal response=", response);
                        if (response.code === 0) {
                            caller && method && method.call(caller, response.result);
                        }
                        else {
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                            let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                            _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("ShareCardFinal Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                            caller && method && method.call(caller, null);
                        }
                    }).OnError(this, (error) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
                    }).OnException(this, (data) => {
                        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
                    }).Send();
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
        this.UrlStoreValue = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("获取自定义空间值")
            .Category("gamebase")
            .Router("store")
            .Append("value")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].StoreValue, this, (request, caller, method) => {
            this.UrlStoreValue.AsPost().Post(request)
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Log("StoreValue response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_2__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("StoreValue Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, null);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_3__["Log"].Error("Exception=", data);
            }).Send();
        });
    }
}


/***/ }),

/***/ "./sdk/Manager/PowerMgr.ts":
/*!*********************************!*\
  !*** ./sdk/Manager/PowerMgr.ts ***!
  \*********************************/
/*! exports provided: PowerMgr, YDHWVarExporter */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PowerMgr", function() { return PowerMgr; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "YDHWVarExporter", function() { return YDHWVarExporter; });
/* harmony import */ var _framework_Manager_Manager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../framework/Manager/Manager */ "./framework/Manager/Manager.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");
/* harmony import */ var _SDK_Enum__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../SDK/Enum */ "./sdk/SDK/Enum.ts");




class PowerMgr extends _framework_Manager_Manager__WEBPACK_IMPORTED_MODULE_0__["Manager"] {
    Initialize() {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["POWER_EVT"].AddPower, this, (num) => {
        });
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.DeclareVariable(_SDK_Enum__WEBPACK_IMPORTED_MODULE_3__["VARS"].Power, 0).Store().Bind(this, (num, num2) => {
            //todo:update data to server
        });
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.SetValueT(_SDK_Enum__WEBPACK_IMPORTED_MODULE_3__["VARS"].Power, 5);
        let tmpVar = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.GetVariable(_SDK_Enum__WEBPACK_IMPORTED_MODULE_3__["VARS"].Power);
        tmpVar.Bind(this, (num) => {
        });
        let power = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.GetValueT(_SDK_Enum__WEBPACK_IMPORTED_MODULE_3__["VARS"].Power);
        YDHWVarExporter.AddPower = 5;
    }
}
class YDHWVarExporter {
    static get Power() {
        return 0;
    }
    static set Power(value) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.SetValueT(_SDK_Enum__WEBPACK_IMPORTED_MODULE_3__["VARS"].Power, value);
    }
    static set AddPower(value) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["POWER_EVT"].AddPower, value);
    }
    static AddPower2(value, value2) {
        _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Instance.InvokeMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_2__["POWER_EVT"].AddPower, value);
    }
}


/***/ }),

/***/ "./sdk/Manager/StatisticMgr.ts":
/*!*************************************!*\
  !*** ./sdk/Manager/StatisticMgr.ts ***!
  \*************************************/
/*! exports provided: StatisticMgr */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatisticMgr", function() { return StatisticMgr; });
/* harmony import */ var _framework_Manager_DataManager__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../framework/Manager/DataManager */ "./framework/Manager/DataManager.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _Assist_Log__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Assist/Log */ "./sdk/Assist/Log.ts");
/* harmony import */ var _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Base/SDKPlatform */ "./sdk/Base/SDKPlatform.ts");
/* harmony import */ var _SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../SDK/EventDef */ "./sdk/SDK/EventDef.ts");





class StatisticMgr extends _framework_Manager_DataManager__WEBPACK_IMPORTED_MODULE_0__["DataManager"] {
    Initialize() {
        this._UrlBanner = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("Banner统计")
            .Category("statistics")
            .Router("banner")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Banner, this, (type, adId, caller, method) => {
            this._UrlBanner.AsGet()
                .Param("type", type)
                .Param("adId", adId)
                .End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Log("Banner response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Banner Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlClickCount = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("卖量统计")
            .Category("statistics")
            .Router("clickcount")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].ClickOut, this, (request, caller, method) => {
            this._UrlClickCount.AsPost()
                .Post(request).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Log("ClickOut Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("ClickOut Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlClientLog = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("客户端错误日志")
            .Category("statistics")
            .Router("clientlog")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].ClientLog, this, (request, caller, method) => {
            this._UrlClientLog.AsPost()
                .Post(request).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Log("ClientLog Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("ClientLog Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlDuration = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("时长统计")
            .Category("statistics")
            .Router("duration")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Duration, this, (type, duration, caller, method) => {
            this._UrlDuration.AsGet()
                .Param("type", type)
                .Param("duration", duration)
                .End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Log("Duration Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Duration Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlEvent = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("事件统计")
            .Category("statistics")
            .Router("event")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Event, this, (events, caller, method) => {
            this._UrlEvent.AsPost()
                .Post({ events: events }).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Log("Event Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Event Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlInterstitialAd = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/x-www-form-urlencoded")
            .Description("插屏广告统计")
            .Category("statistics")
            .Router("interstital")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].InterstitialAd, this, (type, adId, caller, method) => {
            this._UrlEvent.AsGet()
                .Param("type", type)
                .Param("adId", adId)
                .End()
                .OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Log("Event Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("InterstitialAd Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlLayer = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("流失统计")
            .Category("statistics")
            .Router("layer")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Statistic_Layer, this, (layer, caller, method) => {
            this._UrlLayer.AsPost()
                .Post({ layer: layer }).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Log("Event Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Layer Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlResult = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("结果统计")
            .Category("statistics")
            .Router("result")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Result, this, (detail, caller, method) => {
            this._UrlResult.AsPost()
                .Post({}).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Log("Result Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, response.result);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Result Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlShareCard = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("分享图统计")
            .Category("statistics")
            .Router("sharecard")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Statistic_ShareCard, this, (sharecardId, caller, method) => {
            this._UrlShareCard.AsPost()
                .Post({ sharecardId: sharecardId }).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Log("ShareCard Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("ShareCard Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Exception=", data);
            }).Send();
        });
        this._UrlVideo = _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Domain.Clone()
            .RequestHeader("Content-Type", "application/json;charset=UTF-8")
            .Description("视频统计")
            .Category("statistics")
            .Router("video")
            .End();
        this.DeclareMethod(_SDK_EventDef__WEBPACK_IMPORTED_MODULE_4__["SDK_NET_API"].Video, this, (type, adId, scene, caller, method) => {
            this._UrlVideo.AsPost()
                .Post({ type: type, adId: adId, scene: scene }).OnReceive(this, (response) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Log("Video Response=", response);
                if (response.code === 0) {
                    caller && method && method.call(caller, true);
                }
                else {
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("server say:" + response.info);
                    let descriptor = _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"].ERROR_DESCRIPTION[response.code];
                    _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && descriptor && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Video Error:" + descriptor.Description + "\n提示:" + descriptor.SolutionHint);
                    caller && method && method.call(caller, false);
                }
            }).OnError(this, (error) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Error=", error);
            }).OnException(this, (data) => {
                _SDK_Declare__WEBPACK_IMPORTED_MODULE_1__["SDK"].Platform.IsDebug && _Assist_Log__WEBPACK_IMPORTED_MODULE_2__["Log"].Error("Exception=", data);
            }).Send();
        });
    }
}


/***/ }),

/***/ "./sdk/Model/Data.ts":
/*!***************************!*\
  !*** ./sdk/Model/Data.ts ***!
  \***************************/
/*! exports provided: StoreValueRequest, BannerRefreshConfig, PowerSystemConfig, VideoUnlockLevelConfig, DeepSheildRuleConfig, TimingMisTouchSwitchConfig, DeveloperAccountConfig, LocalLayerInfo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StoreValueRequest", function() { return StoreValueRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BannerRefreshConfig", function() { return BannerRefreshConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PowerSystemConfig", function() { return PowerSystemConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VideoUnlockLevelConfig", function() { return VideoUnlockLevelConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeepSheildRuleConfig", function() { return DeepSheildRuleConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TimingMisTouchSwitchConfig", function() { return TimingMisTouchSwitchConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeveloperAccountConfig", function() { return DeveloperAccountConfig; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LocalLayerInfo", function() { return LocalLayerInfo; });
class StoreValueRequest {
    constructor() {
        this.name = "";
        this.cmd = "";
        this.args = "";
    }
}
class BannerRefreshConfig {
}
class PowerSystemConfig {
    constructor() {
        this.DefaultPowerValue = 0;
        this.UpperLimit = 0;
        this.AutoRecoveryTime = 0;
        this.VideoPowerWeight = 0;
        this.IsOn = false;
    }
}
class VideoUnlockLevelConfig {
    constructor() {
        this.IsOn = false;
        this.CustomNumber = 0;
        this.IntervalCount = 0;
    }
}
class DeepSheildRuleConfig {
    constructor() {
        this.Type = 0;
        this.CustomNumberCounter = 0;
        this.ExecellentUserSwitch = false;
        this.WatchVideoCounter = 0;
        this.DayCustomNumber = [];
    }
}
class TimingMisTouchSwitchConfig {
    constructor() {
        this.IsOn = false;
        this.StartTimeHour = 0;
        this.StartTimeMinute = 0;
        this.EndTimeHour = 0;
        this.EndTimeMinute = 0;
    }
}
class DeveloperAccountConfig {
    constructor() {
        this.IsOn = false;
        this.Accounts = [];
    }
}
class LocalLayerInfo {
    constructor() {
        this.s_layers = null;
        this.get_time = null;
    }
}


/***/ }),

/***/ "./sdk/Model/Default.ts":
/*!******************************!*\
  !*** ./sdk/Model/Default.ts ***!
  \******************************/
/*! exports provided: BannerAdStyle, ShareAppInfo, ShareBackInfo, CustomInfo, DeepTouchInfo, AppInfo, JumpOutInfo, StatisticEventRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BannerAdStyle", function() { return BannerAdStyle; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareAppInfo", function() { return ShareAppInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareBackInfo", function() { return ShareBackInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CustomInfo", function() { return CustomInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DeepTouchInfo", function() { return DeepTouchInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppInfo", function() { return AppInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JumpOutInfo", function() { return JumpOutInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StatisticEventRequest", function() { return StatisticEventRequest; });
class BannerAdStyle {
}
class ShareAppInfo {
    constructor() {
        this.channel = "";
        this.module = "";
        this.showTime = 0;
        this.shareId = 0;
    }
}
class ShareBackInfo {
    constructor() {
        this.IsSuccess = false;
        this.IsHasStrategy = false;
        this.Tips = null;
    }
}
class CustomInfo {
    constructor() {
        this._id = 0;
        this.name = "";
        this.version = "";
        this.type = "";
        this.value = "";
        this.switchRegionUse = "";
        this.switchTouchUse = "";
        this.desc = "";
    }
}
class DeepTouchInfo {
    constructor() {
        this.deepTouch = false;
        this.ListCustomInfo = [];
    }
}
class AppInfo {
    constructor(appId) {
        this.appid = "";
        this.appid = appId;
    }
}
class JumpOutInfo {
    constructor() {
        this.Date = 0;
        this.List = [];
    }
}
class StatisticEventRequest {
}


/***/ }),

/***/ "./sdk/Model/Statistic.ts":
/*!********************************!*\
  !*** ./sdk/Model/Statistic.ts ***!
  \********************************/
/*! exports provided: ClientLogRequest, ClickOutRequest */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientLogRequest", function() { return ClientLogRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClickOutRequest", function() { return ClickOutRequest; });
class ClientLogRequest {
}
class ClickOutRequest {
}


/***/ }),

/***/ "./sdk/Model/Toutiao.ts":
/*!******************************!*\
  !*** ./sdk/Model/Toutiao.ts ***!
  \******************************/
/*! exports provided: EM_RECORD_TYPE, ShareAppMessageExtra */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EM_RECORD_TYPE", function() { return EM_RECORD_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareAppMessageExtra", function() { return ShareAppMessageExtra; });
var EM_RECORD_TYPE;
(function (EM_RECORD_TYPE) {
    EM_RECORD_TYPE[EM_RECORD_TYPE["None"] = 0] = "None";
    EM_RECORD_TYPE[EM_RECORD_TYPE["Recording"] = 1] = "Recording";
    EM_RECORD_TYPE[EM_RECORD_TYPE["Pasuse"] = 2] = "Pasuse";
})(EM_RECORD_TYPE || (EM_RECORD_TYPE = {}));
class ShareAppMessageExtra {
}


/***/ }),

/***/ "./sdk/Model/User.ts":
/*!***************************!*\
  !*** ./sdk/Model/User.ts ***!
  \***************************/
/*! exports provided: ShareInfo, ClientInfo, LoginRequest, LoginResponse, StrategyData, Tips, ShareVideoModuleFalse, LoopCtrl, ShareVideoModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareInfo", function() { return ShareInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientInfo", function() { return ClientInfo; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginRequest", function() { return LoginRequest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginResponse", function() { return LoginResponse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StrategyData", function() { return StrategyData; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Tips", function() { return Tips; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareVideoModuleFalse", function() { return ShareVideoModuleFalse; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoopCtrl", function() { return LoopCtrl; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ShareVideoModule", function() { return ShareVideoModule; });
class ShareInfo {
}
class ClientInfo {
}
class LoginRequest {
}
class LoginResponse {
}
class StrategyData {
}
class Tips {
}
class ShareVideoModuleFalse {
    constructor() {
        /**
         * 策略id
         */
        this.strategyId = 0;
        /**
         * time数组ID
         */
        this.timeId = 0;
        /**
         * 执行遍数
         */
        this.count = 0;
    }
}
class LoopCtrl {
}
class ShareVideoModule {
}


/***/ }),

/***/ "./sdk/Platform/ToutiaoPlatform.ts":
/*!*****************************************!*\
  !*** ./sdk/Platform/ToutiaoPlatform.ts ***!
  \*****************************************/
/*! exports provided: ToutiaoPlatform */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToutiaoPlatform", function() { return ToutiaoPlatform; });
/* harmony import */ var _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../Base/SDKPlatform */ "./sdk/Base/SDKPlatform.ts");
/* harmony import */ var _framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../../framework/Assist/StorageAdapter */ "./framework/Assist/StorageAdapter.ts");
/* harmony import */ var _Model_Toutiao__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../Model/Toutiao */ "./sdk/Model/Toutiao.ts");
/* harmony import */ var _SDK_Declare__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../SDK/Declare */ "./sdk/SDK/Declare.ts");




class ToutiaoPlatform extends _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_0__["SDKPlatform"] {
    constructor() {
        super();
        this.RecorderStatus = _Model_Toutiao__WEBPACK_IMPORTED_MODULE_2__["EM_RECORD_TYPE"].None;
        this.Recorder = null;
        this.VideoPath = null;
        this._ListenOnRecoredStop = null;
        this.Name = this.constructor.name;
    }
    Initialize() {
        if (!super.Initialize()) {
            return false;
        }
        _framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__["StorageAdapter"].WriteString = this.Controller.setStorageSync;
        _framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__["StorageAdapter"].WriteObject = (key, value) => {
            this.Controller.setStorageSync(key, value);
        };
        _framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__["StorageAdapter"].ReadString = this.Controller.getStorageSync;
        _framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__["StorageAdapter"].ReadObject = (key) => {
            let content = this.Controller.getStorageSync(key);
            if (!content || typeof content === "undefined") {
                return null;
            }
            else if (typeof content === "object") {
                return content;
            }
            return JSON.parse(content);
        };
        _framework_Assist_StorageAdapter__WEBPACK_IMPORTED_MODULE_1__["StorageAdapter"].DeleteObject = this.Controller.removeStorageSync;
        let platform = this.SystemInfo.platform;
        if (platform === "Android") {
            this.IsOnAndroid = true;
        }
        else if (platform === "iOS") {
            this.IsOnIOS = true;
        }
        return true;
    }
    Login(caller, onSuccess, onFail) {
        this.Controller.login({
            force: false,
            success(result) {
                caller && onSuccess && onSuccess.call(caller, result.code, result.token, result);
            },
            fail(error) {
                caller && onFail && onFail.call(caller, error);
            },
            complete() {
            }
        });
    }
    GetSystemInfoSync(caller, method) {
        caller && method && method.call(caller, this.Controller.getSystemInfoSync());
    }
    OnFrontend(caller, method) {
        this.Controller.onShow((e) => {
            caller && method && method.call(caller);
        });
    }
    OnBackend(caller, method) {
        this.Controller.onHide((e) => {
            console.log("YDHW -----onHide:", e);
            caller && method && method.call(caller);
        });
    }
    NavigateToMiniProgram(ownAppId, toAppId, toUrl, caller, onSuccess, onFail) {
        this.Controller.navigateToMiniProgram({
            appId: toAppId,
            path: toUrl || "",
            extraData: {
                appid: ownAppId //
            },
            success() {
                caller && onSuccess && onSuccess.call(caller);
            },
            fail() {
                caller && onFail && onFail.call(caller);
            }
        });
    }
    CreateBannerAd(adUnitId, isMisTouch, style, caller, onResize) {
        this.CallerOnResizeBannerAd = caller;
        this.OnResizeBannerAd = onResize;
        this.BannerStyle = style;
        this.IsMisTouchBannerAd = isMisTouch;
        let bannerInfo = {
            adUnitId: adUnitId,
            style: style,
            //todo:adIntervals这个参数旧sdk里面旧有，也不知道怎么得来的，得问问
            adIntervals: 31,
        };
        this.BannerAd = this.Controller.createBannerAd(bannerInfo);
        let me = this;
        this.BannerAd.onResize((result) => {
            if (me.OnResizeBannerAd) {
                this.CallerOnResizeBannerAd && this.OnResizeBannerAd.call(this.CallerOnResizeBannerAd, result);
            }
            else {
                if (style) {
                    me.BannerAd.style = style;
                }
                else {
                    me.BannerStyle.top = me.SystemInfo.windowHeight - result.height;
                    me.BannerStyle.left = (me.SystemInfo.windowWidth - result.width) * 0.5;
                }
            }
        });
    }
    CloseBannerAd() {
        if (!this.BannerAd) {
            return;
        }
        this.BannerAd.destroy();
        this.BannerAd = null;
    }
    SetBannerVisible(val) {
        if (!this.BannerAd) {
            _SDK_Declare__WEBPACK_IMPORTED_MODULE_3__["SDK"].Platform.IsDebug && console.error("SetBannerVisible banner ad is not exists");
            return;
        }
        // this._IsShowBannerAd = val;
        val ? this.BannerAd.show() : this.BannerAd.hide();
    }
    CreateRewardVideoAd(adUnitId, caller, onLoad, onCLose, onError) {
        let me = this;
        if (!this.RewardVideoAd) {
            this.RewardVideoAdId = adUnitId;
            this.RewardVideoAd = this.Controller.createRewardedVideoAd({ adUnitId: adUnitId });
            let closeEvent = (result) => {
                me.IsShowRewardVideoAd = false;
                caller && onCLose && onCLose.call(caller, result);
            };
            let errorEvent = (error) => {
                caller && onError && onError.call(caller, error);
                me.IsShowRewardVideoAd = false;
            };
            let loadEvent = () => {
                caller && onLoad && onLoad.call(caller);
            };
            this.RewardVideoAd.offClose(closeEvent);
            this.RewardVideoAd.onClose(closeEvent);
            this.RewardVideoAd.offError(errorEvent);
            this.RewardVideoAd.onError(errorEvent);
            this.RewardVideoAd.offLoad(loadEvent);
            this.RewardVideoAd.onLoad(loadEvent);
        }
        else if (this.RewardVideoAdId !== adUnitId) {
            this.RewardVideoAd = adUnitId;
        }
    }
    ShowRewardVideoAd(caller, onShow, onException) {
        if (!this.RewardVideoAd) {
            this.Error("VideoAd is null");
            return;
        }
        this.IsShowRewardVideoAd = true;
        this.RewardVideoAd.load().then(() => {
            this.RewardVideoAd.show().then(() => {
                caller && onShow && onShow.call(caller);
            }).catch(res => {
                this.IsShowRewardVideoAd = false;
                caller && onException && onException.call(caller, res);
            });
        }).catch(() => {
            this.IsShowRewardVideoAd = false;
        });
    }
    CreateInterstitialAd(adUnitId, caller, onLoad, onClose, onError) {
        this.InterstitialAdId = adUnitId;
        this.InterstitialAd = this.Controller.createInterstitialAd({ adUnitId: adUnitId });
        if (!this.InterstitialAd) {
            return;
        }
        let loadEvent = () => {
            caller && onLoad && onLoad.call(caller);
        };
        let closeEvent = (result) => {
            caller && onClose && onClose.call(caller, result);
        };
        let errorEvent = (error) => {
            caller && onError && onError.call(caller, error);
        };
        // this.InterstitialAd.offLoad(loadEvent);
        this.InterstitialAd.onLoad(loadEvent);
        // this.InterstitialAd.offClose(closeEvent);
        this.InterstitialAd.onClose(closeEvent);
        // this.InterstitialAd.offError(errorEvent);
        this.InterstitialAd.onError(errorEvent);
        this.InterstitialAd.load();
    }
    ShowInterstitialAd(caller, method) {
        if (!this.InterstitialAd) {
            return;
        }
        this.InterstitialAd.show();
        caller && method && method.call(caller);
    }
    ClearInterstitialAd() {
        if (!this.InterstitialAd) {
            this.Log("InterstitialAd is null");
            return;
        }
        this.InterstitialAd.destroy();
        this.InterstitialAd = null;
    }
    ShareAppMessage(channel, templateId, desc, title, imageUrl, query, extra, caller, onSuccess, onFail) {
        let tempObject = {
            channel: channel,
            templateId: templateId,
            desc: desc,
            title: title,
            imageUrl: imageUrl,
            query: query,
            extra: extra,
            success() {
                caller && onSuccess && onSuccess.call(caller);
            },
            fail(error) {
                caller && onFail && onFail.call(caller, error);
            }
        };
        extra && (tempObject.extra = extra);
        console.log("YDHW -----ShareAppMessage:", JSON.stringify(tempObject));
        this.Controller.shareAppMessage(tempObject);
    }
    RecorderStart(duration, caller, onStart, onError, onInterruptionBegin, onInterruptionEnd) {
        if (this.Recorder) {
            this.Log("already recording...");
            return;
        }
        this.Log("录屏-RecorderStart-duration:" + duration + "  duration  Recorder:" + this.Recorder);
        if (duration <= 3) {
            this.Warn("录屏-录屏时长必须 多于 秒");
            duration = 15;
        }
        if (duration > 300) {
            this.Warn("录屏-录屏时长必须 小于等于300 秒");
            duration = 300;
        }
        if (!this.IsHasAPI("getGameRecorderManager")) {
            this.Warn("getGameRecorderManager is not supported");
            return;
        }
        this.Recorder = this.Controller.getGameRecorderManager();
        this.Recorder.onStart((result) => {
            this.RecorderStatus = _Model_Toutiao__WEBPACK_IMPORTED_MODULE_2__["EM_RECORD_TYPE"].Recording;
            caller && onStart && onStart.call(caller, result);
        });
        this.Recorder.onInterruptionBegin(() => {
            caller && onInterruptionBegin && onInterruptionBegin.call(caller);
        });
        this.Recorder.onInterruptionEnd(() => {
            caller && onInterruptionEnd && onInterruptionEnd.call(caller);
        });
        this.Recorder.onError((error) => {
            caller && onError && onError.call(caller, error);
        });
        this.Recorder.start({ duration: duration });
    }
    RecorderStop(caller, onStop) {
        if (!this.Recorder) {
            this.Error("Recorder is null");
            return;
        }
        if (this._ListenOnRecoredStop) {
            return;
        }
        let me = this;
        this._ListenOnRecoredStop = (result) => {
            this.RecorderStatus = _Model_Toutiao__WEBPACK_IMPORTED_MODULE_2__["EM_RECORD_TYPE"].None;
            this.VideoPath = result.videoPath;
            console.log("YDHW ---RecorderStop-VideoPath:", this.VideoPath);
            caller && onStop && onStop.call(caller, this.VideoPath);
            me._ListenOnRecoredStop = null;
            me.Recorder = null;
        };
        this.Recorder.onStop(this._ListenOnRecoredStop);
        this.Recorder.stop();
    }
    RecorderPause(caller, onPause) {
        this.Recorder && this.Recorder.onPause((result) => {
            this.RecorderStatus = _Model_Toutiao__WEBPACK_IMPORTED_MODULE_2__["EM_RECORD_TYPE"].Pasuse;
            caller && onPause && onPause.call(caller, result);
        });
        this.Recorder && this.Recorder.pause();
    }
    RecorderResume(caller, onResume) {
        this.Recorder && this.Recorder.onResume((result) => {
            this.RecorderStatus = _Model_Toutiao__WEBPACK_IMPORTED_MODULE_2__["EM_RECORD_TYPE"].Recording;
            caller && onResume && onResume.call(caller, result);
        });
        this.Recorder && this.Recorder.resume();
    }
    ShowMoreGamesModal(caller, onGetOptions, onSuccess, onError) {
        // 监听弹窗关闭
        this.Controller.onMoreGamesModalClose((result) => { });
        this.Controller.onNavigateToMiniProgram((res) => { });
        let _options = onGetOptions();
        if (!_options) {
            this.Error("ShowMoreGamesModal onGetOptions is null");
            caller && onSuccess && onSuccess.call(caller, false);
            return;
        }
        // iOS 不支持，建议先检测再使用
        if (!this.IsOnIOS) {
            // 打开互跳弹窗
            this.Controller.showMoreGamesModal({
                appLaunchOptions: _options,
                success(res) {
                    caller && onSuccess && onSuccess.call(caller, true);
                },
                fail(res) {
                    caller && onError && onError.call(caller, res);
                }
            });
        }
        else {
            this.Warn("iOS is not supported");
            caller && onSuccess && onSuccess.call(caller, false);
        }
    }
    CreateMoreGamesButton(type, image, style, caller, onGetOptions, onMoreGame, onTip) {
        let _options = onGetOptions();
        if (!_options) {
            this.Error("ShowMoreGamesModal onGetOptions is null");
            caller && onMoreGame && onMoreGame.call(caller, false);
            return;
        }
        let btn = this.Controller.createMoreGamesButton({
            type: type,
            image: image,
            style: style,
            appLaunchOptions: _options,
            onNavigateToMiniGame(res) {
                caller && onMoreGame && onMoreGame.call(caller, true);
            }
        });
        btn.onTap(() => {
            caller && onTip && onTip();
        });
    }
    /**
     *                   if (pf_data.switchLog) console.log("YLSDK tt.getUserInfo--success:", JSON.stringify(res));
  if (res.userInfo) {
    let user_tt_info = res.userInfo;
    _sdk.setUserPlatFormInfo(user_tt_info);
  }
  if (cb) cb(_sdk.getUserPlatFormInfo());
     */
    GetPlatformUserInfo(caller, onSuccess, onFail) {
        if (!this.IsHasAPI("getUserInfo")) {
            this.Warn("getUserInfo is not supported");
            return;
        }
        else {
            this.Controller.getUserInfo({
                withCredentials: false,
                lang: 'zh_CN',
                success: (result) => {
                    caller && onSuccess && onSuccess.call(caller, result.userInfo);
                },
                fail: (error) => {
                    caller && onFail && onFail.call(caller, error);
                },
            });
        }
    }
}


/***/ }),

/***/ "./sdk/SDK/Declare.ts":
/*!****************************!*\
  !*** ./sdk/SDK/Declare.ts ***!
  \****************************/
/*! exports provided: SDK */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDK", function() { return SDK; });
/* harmony import */ var _framework_Core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../framework/Core */ "./framework/Core.ts");
/* harmony import */ var _Enum__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Enum */ "./sdk/SDK/Enum.ts");
/* harmony import */ var _framework_Utility_Url__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../framework/Utility/Url */ "./framework/Utility/Url.ts");
/* harmony import */ var _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../Base/SDKPlatform */ "./sdk/Base/SDKPlatform.ts");




class SDK {
    static get Domain() {
        return this._Url;
    }
    static get Platform() {
        if (this._Instance == null) {
            this._Instance = new _Base_SDKPlatform__WEBPACK_IMPORTED_MODULE_3__["SDKPlatform"]();
        }
        return this._Instance;
    }
    static get Core() {
        return _framework_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Instance;
    }
    static get Instance() {
        return _framework_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Instance.AsManager();
    }
    static get Agent() {
        return _framework_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Instance.GetManager(_Enum__WEBPACK_IMPORTED_MODULE_1__["ENUM_MANAGER"].Agent);
    }
    static get Commerce() {
        return _framework_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Instance.GetManager(_Enum__WEBPACK_IMPORTED_MODULE_1__["ENUM_MANAGER"].Commerce);
    }
    static get Statistic() {
        return _framework_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Instance.GetManager(_Enum__WEBPACK_IMPORTED_MODULE_1__["ENUM_MANAGER"].Statistic);
    }
    static get Data() {
        return _framework_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Instance.GetManager(_Enum__WEBPACK_IMPORTED_MODULE_1__["ENUM_MANAGER"].Data);
    }
    static get PowerMgr() {
        return _framework_Core__WEBPACK_IMPORTED_MODULE_0__["Core"].Instance.GetManager(_Enum__WEBPACK_IMPORTED_MODULE_1__["ENUM_MANAGER"].Power);
    }
}
// static get Instance(): Core {
//     return Core.Instance;
// }
SDK._Url = new _framework_Utility_Url__WEBPACK_IMPORTED_MODULE_2__["Url"]();
SDK._Instance = null;


/***/ }),

/***/ "./sdk/SDK/Enum.ts":
/*!*************************!*\
  !*** ./sdk/SDK/Enum.ts ***!
  \*************************/
/*! exports provided: ENUM_MANAGER, METHOD, VARS */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ENUM_MANAGER", function() { return ENUM_MANAGER; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "METHOD", function() { return METHOD; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VARS", function() { return VARS; });
/**
 * 管理器类型枚举
 */
var ENUM_MANAGER;
(function (ENUM_MANAGER) {
    ENUM_MANAGER["Agent"] = "AgentMgr";
    ENUM_MANAGER["Event"] = "EventMgr";
    ENUM_MANAGER["Commerce"] = "CommerceMgr";
    ENUM_MANAGER["Resource"] = "ResourceMgr";
    ENUM_MANAGER["State"] = "StateMgr";
    ENUM_MANAGER["Panel"] = "PanelMgr";
    ENUM_MANAGER["Statistic"] = "StatisticMgr";
    ENUM_MANAGER["Data"] = "DataMgr";
    ENUM_MANAGER["Sound"] = "SoundMgr";
    ENUM_MANAGER["Platform"] = "PlatformMgr";
    ENUM_MANAGER["Logic"] = "Logic";
    ENUM_MANAGER["Log"] = "LogMgr";
    ENUM_MANAGER["Power"] = "Power";
})(ENUM_MANAGER || (ENUM_MANAGER = {}));
/**
 * 方法ID枚举
 */
var METHOD;
(function (METHOD) {
    METHOD["InitView"] = "InitView";
    METHOD["OnClear"] = "OnClear";
    METHOD["OnCancel"] = "OnCancel";
    METHOD["OnRemoved"] = "OnRemoved";
    METHOD["OnSelect"] = "OnSelect";
    METHOD["OnRefresh"] = "OnRefresh";
    METHOD["OnChangedItem"] = "OnChangedItem";
    //----------------------------------------------------------------------------------------
    //ResourceMgr
    METHOD["OnPreloadStateGame"] = "PreloadStateGame";
    METHOD["OnPreloadStateHome"] = "OnPreloadStateHome";
    METHOD["OnPreloadStateLoading"] = "OnPreloadStateLoading";
    METHOD["OnPreloadStateResult"] = "OnPreloadStateResult";
    METHOD["OnPreloadConfig"] = "OnPreloadConfig";
    METHOD["OnLoginSuccess"] = "OnLoginSuccess";
    //----------------------------------------------------------------------------------------
    //Config
    //----------------------------------------------------------------------------------------
    //UserDataMgr
    /**
     * 进入下一关
     */
    METHOD["OnIncreaseLevel"] = "OnIncreaseLevel";
    //商业化
    /**
     * todo:关闭原生Banner
     */
    METHOD["OnCloseNativeBanner"] = "OnCloseNativeBanner";
    /**
     * todo:刷新原生Banner
     */
    METHOD["OnRefreshNativeBanner"] = "OnRefreshNativeBanner";
    //----------------------------------------------------------------------------------------
    //StateMgr
    /**
     * 加载进度完成时触发
     */
    METHOD["OnFinishLoading"] = "OnFinishLoading";
    /**
     * 刷新进度条
     */
    METHOD["OnLoadingProgress"] = "OnLoadingProgress";
    /**
     * 加载进度报错
     */
    METHOD["OnLoadingError"] = "OnLoadingError";
    //----------------------------------------------------------------------------------------
    //PanelMgr
    METHOD["OnMouseDownBtnSkinVisible"] = "OnMouseDownBtnSkinVisible";
    //----------------------------------------------------------------------------------------
    //SoundManager
    METHOD["OnPlayEffect"] = "OnPlayEffect";
    METHOD["OnVibrate"] = "OnVibrate";
    //----------------------------------------------------------------------------------------
    //Logic
    // OnLogicPreload = "OnLogicPreload",
    //----------------------------------------------------------------------------------------
    //Skin
    /**
     * 解锁皮肤
     */
    /**
     * 随机出一个皮肤ID
     */
    METHOD["OnRandomUnlockSkin"] = "OnRandomUnlockSkin";
    /**
     * 开宝箱解锁皮肤
     */
    METHOD["OnUnlockSkinByTreasureBox"] = "OnUnlockSkinByTreasureBox";
    /**
     * 计算金币解锁皮肤需要多少个金币数量
     */
    METHOD["OnHowManyCoinCost"] = "OnHowManyCoinCost";
    /**
     * 花费金币购买皮肤
     */
    METHOD["OnUnlockSkinByGold"] = "OnUnlockSkinByGold";
    /**
     * 如果当前解锁皮肤的方式是看视频，则需要保存视频次数
     */
    METHOD["OnUnlockSkinByVideoAd"] = "OnUnlockSkinByVideoAd";
    //---------------------------------------------------------------------------------------------
    //Commmerce 商业化
    /**
     * 重新初始化侧边栏
     */
    METHOD["OnReinitSideBox"] = "\u91CD\u65B0\u521D\u59CB\u5316\u4FA7\u8FB9\u680F";
    //----------------------------------------------------------------------------------------
    //LogMgr
    METHOD["OnCheckAllVariables"] = "OnCheckAllVariables";
    METHOD["OnCheckAllMethods"] = "OnCheckAllMethods";
})(METHOD || (METHOD = {}));
/**
 * 变量ID枚举
 */
var VARS;
(function (VARS) {
    VARS["Power"] = "Power";
    VARS["BallBounceSound"] = "BallBounceSound";
    VARS["FloorBrokeSound"] = "FloorBrokeSound";
    VARS["MusicSuffixName"] = "MusicSuffixName";
    VARS["BackgroundMusic"] = "BackgroundMusic";
    VARS["IsOpenBackgrouMusic"] = "IsOpenBackgrouMusic";
    VARS["IsOpenEffectSound"] = "IsOpenEffectSound";
    VARS["IsOpenVibrate"] = "IsOpenVibrate";
    VARS["IsFirstTimeEnterGame"] = "IsFirstGameStart";
    /**
     * 设置背景音乐音量
     */
    VARS["MusicVolume"] = "MusicVolume";
    /**
     * 设置音效音乐音量
     */
    VARS["EffectVolume"] = "EffectVolume";
    /**
     * 当前进行中的关卡
     */
    VARS["CurrentLevelIndex"] = "CurrentLevelIndex";
    /**
     * 当前金币数量
     */
    VARS["CurrentCoinCount"] = "CurrentCoinCount";
    /**
     * 当前抽奖券数量
     */
    VARS["CurrentTicketCount"] = "CurrentTicketCount";
    /**
     * 当前剩余看视频获取抽奖券有效机会
     */
    VARS["CurrentTicketsForWatchVideoAd"] = "CurrentTicketUsedForWatchVideoAd";
    /**
     * 当前累计总分
     */
    VARS["CurrentTotalScore"] = "CurrentTotalScore";
    /**
     * 历史最高分
     */
    VARS["HistoryHighScore"] = "HistoryHighScore";
    /**
     * 当前累计看视频次数
     */
    VARS["CurrentVideoAdCount"] = "CurrentVideoAdCount";
    /**
     * 当前正在看视频解锁的皮肤
     */
    VARS["CurrentUnlockingSkinId"] = "CurrentUnlockingSkinId";
    VARS["CurrentUsingSkinId"] = "CurrentUsingSkinId";
    /**
     * 时间戳
     */
    VARS["LastTimestamp"] = "LastTimestamp";
    /**
     * 已经解锁的皮肤的全局皮肤Id列表
     */
    VARS["UnlockSkinList"] = "UnlockSkinList";
    /**
     * 已经抽奖被抽取过的转盘选项Id列表
     */
    VARS["UsedRewardList"] = "UsedRewardList";
    VARS["GMGameFinish"] = "GMGameFinish";
    /**
     * 当前需要获取的卡片下标
     */
    VARS["CurrentShareCardIndex"] = "ShareCardIndex";
    VARS["CurrentScoreVisible"] = "CurrentScoreVisible";
    VARS["HighestScoreVisible"] = "HistoryHighestScoreVisible";
    VARS["IsBtnSkinVisible"] = "IsBtnSkinVisible";
    /**
     * 商业化
     */
    // ComerceSceneId = "ComerceSceneId",
    // ComerceChannel = "ComerceChannel",
    // ComerceShareCardId = "ComerceShareCardId",
    // ComerceExtraData = "ComerceExtraData",
})(VARS || (VARS = {}));


/***/ }),

/***/ "./sdk/SDK/EventDef.ts":
/*!*****************************!*\
  !*** ./sdk/SDK/EventDef.ts ***!
  \*****************************/
/*! exports provided: SDK_NET_API, SDK_VARS, SDK_EVT, EM_VIDEO_PLAY_TYPE, EM_STATISTIC_TYPE, EM_SHARE_TYPE, EM_POWER_RECOVERY_TYPE, POWER_EVT, TIMER_NAME */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDK_NET_API", function() { return SDK_NET_API; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDK_VARS", function() { return SDK_VARS; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SDK_EVT", function() { return SDK_EVT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EM_VIDEO_PLAY_TYPE", function() { return EM_VIDEO_PLAY_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EM_STATISTIC_TYPE", function() { return EM_STATISTIC_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EM_SHARE_TYPE", function() { return EM_SHARE_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EM_POWER_RECOVERY_TYPE", function() { return EM_POWER_RECOVERY_TYPE; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "POWER_EVT", function() { return POWER_EVT; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TIMER_NAME", function() { return TIMER_NAME; });
var SDK_NET_API;
(function (SDK_NET_API) {
    //Agent
    SDK_NET_API["Edit"] = "Edit";
    SDK_NET_API["Login"] = "Login";
    SDK_NET_API["MyInfo"] = "MyInfo";
    //Statistic
    SDK_NET_API["Banner"] = "Banner";
    /**
     * 卖量统计
     */
    SDK_NET_API["ClickOut"] = "ClickOut";
    SDK_NET_API["ClientLog"] = "ClientLog";
    SDK_NET_API["Duration"] = "Duration";
    SDK_NET_API["Event"] = "Event";
    SDK_NET_API["InterstitialAd"] = "InterstitialAd";
    SDK_NET_API["Statistic_Layer"] = "Statistic_Layer";
    SDK_NET_API["Result"] = "Result";
    SDK_NET_API["Statistic_ShareCard"] = "Statistic_ShareCard";
    SDK_NET_API["Video"] = "Video";
    //Data
    SDK_NET_API["Config"] = "Config";
    SDK_NET_API["CustomConfig"] = "CustomConfig";
    SDK_NET_API["DataDecode"] = "DataDecode";
    SDK_NET_API["GetBoardAward"] = "GetBoardAward";
    SDK_NET_API["Data_Layer"] = "Data_Layer";
    SDK_NET_API["ScoreBoard"] = "ScoreBoard";
    SDK_NET_API["SideBox"] = "SideBox";
    SDK_NET_API["Data_ShareCard"] = "Data_ShareCard";
    SDK_NET_API["StoreValue"] = "StoreValue";
})(SDK_NET_API || (SDK_NET_API = {}));
var SDK_VARS;
(function (SDK_VARS) {
    /**
     * 当前体力总点数
     */
    SDK_VARS["TotalPowerCounter"] = "PowerCounter";
    /**
     * 最后一次增加体力时的时间戳
     */
    SDK_VARS["LastPowerAddTimestamp"] = "LastPowerAddTimestamp";
    /**
     * 今日看视频数量
     */
    SDK_VARS["TodayWatchVideoCounter"] = "TodayWatchVideoCounter";
    /**
     * 最后一次看视频的时间戳
     */
    SDK_VARS["LastWatchVideoTimestamp"] = "LastWatchVideoTimestamp";
    /**
     * 已解锁关卡数 列表
     */
    SDK_VARS["ListUnlockVideoLevelNumber"] = "ListUnlockLevelNumber";
    /**
     * 本地缓存已打点流失路径
     */
    SDK_VARS["ListStatisticsLayer"] = "ListStatisticsLayer";
    SDK_VARS["ShareConfig"] = "ShareConfig";
    /**
     * 最大关卡编号
     */
    SDK_VARS["MaxCustomNumber"] = "MaxCustomNumber";
    /**
     * 最后一次玩的关卡编号
     */
    SDK_VARS["LastCustomNumber"] = "LastCustomNumber";
    /**
     * 今日晚的总关卡数量
     */
    SDK_VARS["TodayCustomNumberCounter"] = "TodayCustomNumberCounter";
    /**
     * 迄今为止总共关卡数量
     */
    SDK_VARS["TotalCustomNumberCounter"] = "TotalCustomNumberCounter";
    /**
     * 最后一次玩的时间
     */
    SDK_VARS["LastPlayTimestamp"] = "LastPlayTimestamp";
    SDK_VARS["JumpOutInfo"] = "JumpOutInfo";
    /**
     * 视频上限
     */
    SDK_VARS["VideoFalseLimit"] = "VideoFalseLimit";
    /**
     * 分享上限
     */
    SDK_VARS["ShareFalseLimit"] = "ShareFalseLimit";
    /**
     * 平台用户信息
     */
    SDK_VARS["PlatformUserInfo"] = "PlatformUserInfo";
})(SDK_VARS || (SDK_VARS = {}));
var SDK_EVT;
(function (SDK_EVT) {
    /**
     * 显示banner广告
     */
    SDK_EVT["ShowBanner"] = "\u663E\u793Abanner\u5E7F\u544A";
    /**
     * 播放完banner广告后的回调
     */
    SDK_EVT["ShowBannerCallback"] = "\u64AD\u653E\u5B8Cbanner\u5E7F\u544A\u540E\u7684\u56DE\u8C03";
    /**
     * 检查桌面是否有图标
     */
    SDK_EVT["CheckIsHasDesktopIcon"] = "\u68C0\u67E5\u684C\u9762\u662F\u5426\u6709\u56FE\u6807";
    /**
     * 看视频广告获取奖励
     */
    SDK_EVT["WatchVideoAdForReward"] = "\u770B\u89C6\u9891\u5E7F\u544A\u83B7\u53D6\u5956\u52B1";
    /**
     * 看视频获取金币
     */
    SDK_EVT["WatchVideoForCoin"] = "\u770B\u89C6\u9891\u83B7\u53D6\u91D1\u5E01";
    /**
     * 看视频获取3倍金币奖励
     */
    SDK_EVT["WatchVideoForThriceCoin"] = "\u770B\u89C6\u9891\u83B7\u53D63\u500D\u91D1\u5E01\u5956\u52B1";
    /**
     * 看视频获得皮肤
     */
    SDK_EVT["WatchVideoForSkin"] = "\u770B\u89C6\u9891\u83B7\u5F97\u76AE\u80A4";
    /**
     * 看视频得抽奖券
     */
    SDK_EVT["WatchVideoForTicket"] = "WatchVideoForTicket";
    /**
     * 看视频获得离线奖励
     */
    SDK_EVT["WatchVideoForOfflineReward"] = "WatchVideoForOfflineReward";
    /**
     * 看视频复活重生恢复
     */
    SDK_EVT["WatchVideoForRevive"] = "WatchVideoForRevive";
    /**
     * 每三关看视频获取惊喜大礼包
     */
    SDK_EVT["WatchVideoForGiftPer3Level"] = "WatchVideoForGiftPer3Level";
    /**
     * 看视频开宝箱
     */
    SDK_EVT["WatchVideoAdForOpenTreasureBox"] = "WatchVideoAdForOpenTreasureBox";
})(SDK_EVT || (SDK_EVT = {}));
var EM_VIDEO_PLAY_TYPE;
(function (EM_VIDEO_PLAY_TYPE) {
    EM_VIDEO_PLAY_TYPE[EM_VIDEO_PLAY_TYPE["VIDEO_PLAY_FAIL"] = 0] = "VIDEO_PLAY_FAIL";
    EM_VIDEO_PLAY_TYPE[EM_VIDEO_PLAY_TYPE["VIDEO_PLAY_FINISH"] = 1] = "VIDEO_PLAY_FINISH";
    EM_VIDEO_PLAY_TYPE[EM_VIDEO_PLAY_TYPE["VIDEO_PLAY_CANCEL"] = 2] = "VIDEO_PLAY_CANCEL";
})(EM_VIDEO_PLAY_TYPE || (EM_VIDEO_PLAY_TYPE = {}));
var EM_STATISTIC_TYPE;
(function (EM_STATISTIC_TYPE) {
    /**
     * 创建
     */
    EM_STATISTIC_TYPE[EM_STATISTIC_TYPE["CREATE"] = 0] = "CREATE";
    /**
     * 加载成功
     */
    EM_STATISTIC_TYPE[EM_STATISTIC_TYPE["LOAD_SUCCESS"] = 1] = "LOAD_SUCCESS";
    /**
     * 加载失败
     */
    EM_STATISTIC_TYPE[EM_STATISTIC_TYPE["LOAD_FAIL"] = 2] = "LOAD_FAIL";
    /**
     * 点击(上报点击)
     */
    EM_STATISTIC_TYPE[EM_STATISTIC_TYPE["CLICK"] = 3] = "CLICK";
    /**
     * 展示
     */
    EM_STATISTIC_TYPE[EM_STATISTIC_TYPE["SHOW"] = 4] = "SHOW";
    /**
     * 关闭
     */
    EM_STATISTIC_TYPE[EM_STATISTIC_TYPE["CLOSE"] = 5] = "CLOSE";
    /**
     * 上报曝光
     */
    EM_STATISTIC_TYPE[EM_STATISTIC_TYPE["EXPOSURE"] = 6] = "EXPOSURE";
    /**
     * 播放取消
     */
    EM_STATISTIC_TYPE[EM_STATISTIC_TYPE["PLAY_CANCEL"] = 7] = "PLAY_CANCEL";
    /**
     * 播放完成
     */
    EM_STATISTIC_TYPE[EM_STATISTIC_TYPE["PLAY_FINISH"] = 8] = "PLAY_FINISH";
})(EM_STATISTIC_TYPE || (EM_STATISTIC_TYPE = {}));
var EM_SHARE_TYPE;
(function (EM_SHARE_TYPE) {
    EM_SHARE_TYPE[EM_SHARE_TYPE["None"] = 0] = "None";
    EM_SHARE_TYPE[EM_SHARE_TYPE["Share"] = 1] = "Share";
    EM_SHARE_TYPE[EM_SHARE_TYPE["Video"] = 2] = "Video";
})(EM_SHARE_TYPE || (EM_SHARE_TYPE = {}));
var EM_POWER_RECOVERY_TYPE;
(function (EM_POWER_RECOVERY_TYPE) {
    EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["None"] = 0] = "None";
    /**
     * 看视频恢复体力
     */
    EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["WatchVideo"] = 1] = "WatchVideo";
    /**
     * 定时恢复体力
     */
    EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["AutoRecovery"] = 2] = "AutoRecovery";
})(EM_POWER_RECOVERY_TYPE || (EM_POWER_RECOVERY_TYPE = {}));
var POWER_EVT;
(function (POWER_EVT) {
    POWER_EVT["AddPower"] = "AddPower";
})(POWER_EVT || (POWER_EVT = {}));
var TIMER_NAME;
(function (TIMER_NAME) {
    /**
     * 旋转装盘的定时器时间名
     */
    TIMER_NAME["RotateTurnplate"] = "RotateTurnplate";
    TIMER_NAME["SaveLastTimestamp"] = "SaveLastTimestamp";
})(TIMER_NAME || (TIMER_NAME = {}));


/***/ }),

/***/ "./sdk_tt/Main.ts":
/*!************************!*\
  !*** ./sdk_tt/Main.ts ***!
  \************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../sdk/SDK/Declare */ "./sdk/SDK/Declare.ts");
/* harmony import */ var _sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../sdk/SDK/Enum */ "./sdk/SDK/Enum.ts");
/* harmony import */ var _sdk_Manager_AgentMgr__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../sdk/Manager/AgentMgr */ "./sdk/Manager/AgentMgr.ts");
/* harmony import */ var _sdk_Manager_StatisticMgr__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sdk/Manager/StatisticMgr */ "./sdk/Manager/StatisticMgr.ts");
/* harmony import */ var _sdk_Manager_DataMgr__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../sdk/Manager/DataMgr */ "./sdk/Manager/DataMgr.ts");
/* harmony import */ var _sdk_Manager_PowerMgr__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../sdk/Manager/PowerMgr */ "./sdk/Manager/PowerMgr.ts");
/* harmony import */ var _sdk_Commerce_ToutiaoCommerce__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../sdk/Commerce/ToutiaoCommerce */ "./sdk/Commerce/ToutiaoCommerce.ts");
/* harmony import */ var _sdk_Platform_ToutiaoPlatform__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../sdk/Platform/ToutiaoPlatform */ "./sdk/Platform/ToutiaoPlatform.ts");








class Main {
    constructor() {
        this.SDK_NAME = "ydhw_tt";
        if (!_sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Platform.Initialize()) {
            console.error("Initialize SDK failed!");
            return;
        }
        let commerce = new _sdk_Commerce_ToutiaoCommerce__WEBPACK_IMPORTED_MODULE_6__["ToutiaoCommerce"](new _sdk_Platform_ToutiaoPlatform__WEBPACK_IMPORTED_MODULE_7__["ToutiaoPlatform"]());
        if (window[this.SDK_NAME] != null) {
            _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Platform.IsDebug && console.error("已存在: " + this.SDK_NAME);
        }
        else if (commerce != null) {
            _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Platform.IsDebug && console.log("Main constructor platform.Initialize():", _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Platform.PlatformType);
            window[this.SDK_NAME] = commerce;
        }
        else {
            _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Platform.IsDebug && console.error("不存在平台对应的SDK");
        }
        _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Domain.Domain(["http://test-api.ylxyx.cn", "https://api.ylxyx.cn"]).Manager("api/collect").Version("v1"); //.Switch(1)
        _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Core
            .AddManager(_sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__["ENUM_MANAGER"].Commerce, commerce)
            .AddManager(_sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__["ENUM_MANAGER"].Agent, new _sdk_Manager_AgentMgr__WEBPACK_IMPORTED_MODULE_2__["AgentMgr"]())
            .AddManager(_sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__["ENUM_MANAGER"].Statistic, new _sdk_Manager_StatisticMgr__WEBPACK_IMPORTED_MODULE_3__["StatisticMgr"]())
            .AddManager(_sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__["ENUM_MANAGER"].Data, new _sdk_Manager_DataMgr__WEBPACK_IMPORTED_MODULE_4__["DataMgr"]())
            .AddManager(_sdk_SDK_Enum__WEBPACK_IMPORTED_MODULE_1__["ENUM_MANAGER"].Power, new _sdk_Manager_PowerMgr__WEBPACK_IMPORTED_MODULE_5__["PowerMgr"]())
            .Initialize();
        _sdk_SDK_Declare__WEBPACK_IMPORTED_MODULE_0__["SDK"].Core.Restore();
        // SDK.Commerce.Login(this, (isOk: boolean) => { });
    }
}
//激活启动类
new Main();


/***/ })

/******/ });
//# sourceMappingURL=ydhw.tt.sdk.js.map