//请将以下配置拷贝到index.js文件中
//下面这段配置拷贝放到index.js最前面
var YDHW_CONFIG = {
    appid: "100001687",
    version: '1.0.0',
    banner_ad_unit_id_list: ['87b40276059048e7b86629c6a49b4187'],//Banner广告
    interstitial_ad_unit_id_list: ['4bd36ddbc30e41ca9f9b23335643f8e9'],//插屏广告
    spread_ad_unit_id_list: [],//开屏广告
    native_ad_unit_id_list: ['9303d25cb65c4cdd858b43c6bb1a5345'],//原生广告[OPPO,VIVO]
    video_ad_unit_id_list: ['065c483cddd148e5b89bbaad68fded5c'],//视频广告
    grid_ad_unit_id_list: [],//格子广告
    appbox_ad_unit_id_list: [],//appBox广告[QQ]
    block_ad_unit_id_list:[],//积木广告[QQ]
    tt_template_id_list: [],//TT 分享素材模板ID列表
    interstitialAd_first_show_wait_time: 10,//插屏广告-首次进入游戏展示时间(进入游戏后x秒后才能展示)（秒）
    interstitialAd_show_time_interval: 10,//插屏广告-两次展示之间时间间隔（秒）
    side_box_count: 20,//侧边栏列表item最小保留数(基于曝光策略)
    pkg_name: "com.szydhw.fkcc.vivominigame",  //包名OPPO、VOVP、魅族平台需要
    

    //--------------以下配置CP无需理会------------
    project: '',
	platform: 'vivo',
    platformCode:3,
    env: 'dev',//online or dev
    debug: true,
    inspector: false,
    engine: 'laya',
    res_version: '20200310',
    appkey: "30248858",
    resource_url: 'http://127.0.0.1:3100',
    scene_white_list: [1005, 1006, 1037, 1035],
};

window.YDHW_CONFIG = YDHW_CONFIG;
//这个引入需要根据自己放置的SDK位置修改
// require("./ydhw.vivo.sdk.min.js"); 