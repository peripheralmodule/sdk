
cc.Class({
    extends: cc.Component,
    properties: {
        pbShake:cc.ProgressBar,
        btnSure:cc.Node,
        btnShake:cc.Node,
        nodePot:cc.Node,
        nodeProBar:cc.Node,
        nodeTree:cc.Node,
        nodeFinger:cc.Node,
        lbCountDown:cc.Label,
        lbGolds:cc.Label,
        lbBoxs:cc.Label,
        sShake:{
            type:cc.AudioClip,
            default:null,
        },
        sAward:{
            type:cc.AudioClip,
            default:null,
        },
        sFail:{
            type:cc.AudioClip,
            default:null,
        },
        clouds:[cc.Node],
        nodeGolds:[cc.Node],

    },
    showView(){
        //奖励数据自己定义,此处只供参考
        this.awards = {id:1,award:[{type:"golds",value:200}],title:"金币"};
        //广告ID记得用自己游戏申请的
        tt.ylBannerAdCreate(false);
        this.node.active = true;
        
        this.initData();
        this.startCountDown();
        this.goldsY = new Array();
        this.nodeGolds.forEach(v=>{
            this.goldsY.push(v.y);
        });
    },
    initData(){
        this.count = 800; //倒计时
        this.changeCountDown();
        this.isStart = false;
        this.triggerAD = false;//触发广告倒计时
        this.pbShake.progress = 0;
        this.btnSure.active = false;
        this.nodeFinger.active = true;
        this.lbCountDown.active = true;
        this.btnShake.active = true;
        this.pb_step = 0.025;
        this.awrd_golds = 0;
        this.refershAwards();
        this._awards = this.awards.award[0].value;
        this.award_step = this._awards/(1/this.pb_step);
        tt.ylBannerAdHide();
        //弹出广告的倒计时间范围
        this.count_show_banner = Math.floor(Math.random()*4)+1; 
        //连续点击总数
        this.series_all = Math.floor(Math.random()*2)+2;
        this.animShake = this.nodeTree.getComponent(cc.Animation);
        this.animFinger = this.nodeFinger.getComponent(cc.Animation);
        this.animFinger.play();
    },
    startCountDown(){
        if(!this.isStart){
            this.isStart = true;
            this.countDownTime();
        }
    },
    countDownTime(){
        let that = this;
        let countDownFun = function() {
            that.count -= 1;
            that.count = that.count < 0 ? 0 :that.count;
            that.changeCountDown();
            let pro = that.pbShake.progress - 0.001;
            that.pbShake.progress = pro < 0 ? 0 : pro;
            that.refershPotX();
            if(that.count == 0){
                that.btnSure.active = true;
                that.lbCountDown.active = false;
                that.btnShake.active = false;
            }

            if(that.count <= 400){
                if(this.triggerAD) return;
                this.triggerAD = true;
                let random_time = Math.random()*3;
                // console.log("倒计时到4秒:"+random_time);
                that.scheduleOnce(function(){
                    this.unschedule(countDownFun);
                    that.btnSure.active = true;
                    that.lbCountDown.active = false;
                    that.btnShake.active = false;
                    tt.ylBannerAdShow();
                },random_time);
            }
         };
        this.schedule(countDownFun, 0.01, (that.count-1), 0.01);
    },
    changeCountDown(){
        let t_1 = Math.floor(this.count/100);
        let t_2 = this.count%100;
        this.lbCountDown.string = "0"+t_1+":"+(t_2<10 ? "0"+t_2 : t_2);
    },
    refershAwards(){
        this.lbGolds.string = this.awrd_golds;
    },
    refershPotX(){
        let _x = this.nodeProBar.x + this.nodeProBar.width
        this.nodePot.x = _x-10;
    },
    update(dt){
        if(!this.node.active) return;
        this.clouds[0].x -= 1;
        this.clouds[0].x = this.clouds[0].x <= -809 ? 808 : this.clouds[0].x;
        this.clouds[1].x += 1;
        this.clouds[1].x = this.clouds[1].x >= 681 ? -687 : this.clouds[1].x;
    },
    onShake(){
        if(this.nodeFinger.active){
            this.animFinger.stop();
            this.nodeFinger.active = false;
        }
        Global.playSound(this.sShake);
        this.animShake.play();
        this.dropGold();
        this.pbShake.progress = (this.pbShake.progress +this.pb_step) >= 1 ? 
                                    1 : this.pbShake.progress + this.pb_step;
        this.refershPotX();
        this.awrd_golds = (this.awrd_golds +this.award_step) >= this._awards ? 
                                    this._awards : this.awrd_golds + this.award_step; 
        this.refershAwards();
    },
    dropGold(){
        if(!this.goldsY || !this.nodeGolds) return;
        let goldsId = Math.floor(Math.random()*this.goldsY.length);
        this.nodeGolds[goldsId].y = this.goldsY[goldsId];
        this.nodeGolds[goldsId].runAction(cc.sequence(
            cc.moveTo(0.3,this.nodeGolds[goldsId].x,-250),
            cc.callFunc(function(){
                this.nodeGolds[goldsId].scale = 0.1;
                this.nodeGolds[goldsId].y = this.goldsY[goldsId];
                this.nodeGolds[goldsId].runAction(cc.scaleTo(0.1,1,1));
            },this,0)));
    },
    onFailAgain(){
        this.startCountDown();
    },
    onFailCancel(){
        this.onClose();
    },
    onClose(){
        tt.ylBannerAdHide();
        this.node.active = false;
    },
});
