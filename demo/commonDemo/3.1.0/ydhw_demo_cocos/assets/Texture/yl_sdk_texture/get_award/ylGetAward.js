
cc.Class({
    extends: cc.Component,
    properties: {
        lbAwardNum:cc.Label,
        spAwardIcon:cc.Sprite,
        sfAwardIcon:[cc.SpriteFrame],
    },

    showView(obj) {
        this._obj = obj;
        this.node.active = true;
        if(this._obj){
            //此处根据自己的需求做相应修改，仅供参考
            if(this._obj.award && this._obj.award.length >0){
                this.lbAwardNum.string = this._obj.award[0].value;
                let size = this.spAwardIcon.node.getContentSize();
                this.spAwardIcon.spriteFrame = this._obj.award[0].type == '1' ? this.sfAwardIcon[0] : this.sfAwardIcon[1];
                this.spAwardIcon.node.setContentSize(size);
            }
        }
    },
    onClose(){
        this.node.active = false;
    },
});
