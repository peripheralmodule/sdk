import { YDHW } from "./../../libs/ydhw/ydhw.sdk";

export class PlatformOPPO{

    constructor() {
    }
    private nativeAdId:string = null;
    /**
     * 创建-原生广告
     * @param e 
     */
    CreateNativeAd():void{
        ydhw.Invoke(YDHW.YDHW_API.CreateNativeAd,this,
            (args: any)=>{
                console.log("PlatformOPPO ---CreateNativeAd-back:",JSON.stringify(args));
                this.nativeAdId = args[0].adId;
                console.log("PlatformOPPO ---nativeAdId:",this.nativeAdId);
            });
    }
    /**
     * 显示上报-原生广告
     * @param e 
     */
    ShowNativeAd():void{
        console.log("PlatformOPPO ---ShowNativeAd:",this.nativeAdId);
        if(this.nativeAdId) ydhw.Invoke(YDHW.YDHW_API.ShowNativeAd,this.nativeAdId);
    }

    /**
     * 点击上报-原生广告
     * @param e 
     */
    ClickNativeAd():void{
        console.log("PlatformOPPO ---ClickNativeAd:",this.nativeAdId);
        if(this.nativeAdId) ydhw.Invoke(YDHW.YDHW_API.ClickNativeAd,this.nativeAdId);
    }

    /**
     * 小游戏跳转
     */
    NavigateToMiniProgram(id: number, toAppId: string, toUrl: string, source: string): void {
        ydhw.Invoke(YDHW.YDHW_API.NavigateToMiniProgram,id,toAppId,toUrl,source,this,(isOk:boolean)=>{
            console.log("PlatformOPPO -NavigateToMiniProgram-isOk:",isOk);
        });
    }

}