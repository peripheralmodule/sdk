import { ui } from "./../ui/layaMaxUI";

import { PlatformTT } from "./PlatformTT";
import { PlatformWX,EM_RECORD_STATUS } from "./PlatformWX";
import { PlatformQQ } from "./PlatformQQ";
import { PlatformOPPO } from "./PlatformOPPO";
import { PlatformMZ } from "./PlatformMZ";
import { PlatformVIVO } from "./PlatformVIVO";
import { PlatformPublic } from "./PlatformPublic";

//平台[ 0:微信,1:QQ,2:OPPO,3:VIVO,4:字节跳动,5:百度,12:魅族 ]//暂未开放:,6:4399,7:趣头条,8:360,9:陌陌,10:影流小游戏,11:小米
export enum EM_PLATFORM {
    WX = 0,
    QQ = 1,
    OPPO = 2,
    VIVO = 3,
    TT = 4,
    BAI_DU = 5,
    MEI_ZU = 12,
}

export default class GameUI extends ui.test.TestSceneUI {

    private PlatForm = null;
    private PlatFormPUB = null;
    static instance: GameUI;
    private platformId = window['YDHW_CONFIG'].platformCode || 0;//平台ID(index.js中配置)

    constructor() {
        super();
        GameUI.instance = this;
        Laya.MouseManager.multiTouchEnabled = false;
    }

    onEnable(): void {
        console.log("YDHW ---YDHW_CONFIG:"+window['YDHW_CONFIG'].platformCode);
        this.initPlatform();
        this.initUI();
        this.PlatFormPUB.init();
    }

    /**
     * 初始化平台信息
     */
    initPlatform(){
        this.PlatFormPUB = new PlatformPublic();
        let platfromName = "平台";
        switch (this.platformId){
            case EM_PLATFORM.WX:
                this.PlatForm = new PlatformWX();
                platfromName = "微信平台";
                break;
            case EM_PLATFORM.TT:
                this.PlatForm = new PlatformTT();
                platfromName = "头条平台";
                break;
            case EM_PLATFORM.QQ:
                this.PlatForm = new PlatformQQ();
                platfromName = "QQ平台";
                break;
            case EM_PLATFORM.OPPO:
                this.PlatForm = new PlatformOPPO();
                platfromName = "OPPO平台";
                break;
            case EM_PLATFORM.VIVO:
                this.PlatForm = new PlatformVIVO();
                platfromName = "VIVO平台";
                break;
            case EM_PLATFORM.MEI_ZU:
                this.PlatForm = new PlatformMZ();
                platfromName = "魅族平台";
                break;
        }
        this.txt_platfrom_name.text = platfromName;
    }

    /**
     * 初始化UI
     */
    initUI():void{
        this.hideAllPlatformUI();
        this.initPlatformView();
        this.initPublicBtn();
    }

    /**
     * 初始化平台独有的UI
     */
    initPlatformView():void{
        console.warn("YDHW ---当前平台ID："+this.platformId);
        switch (this.platformId){
            case EM_PLATFORM.WX:
                this.initWXplatformUI();
                break;
            case EM_PLATFORM.TT:
                this.initTTplatformUI();
                break;
            case EM_PLATFORM.QQ:
               this.initQQplatformUI();
                break;
            case EM_PLATFORM.OPPO:
                this.initOPPOplatformUI();
                break;
            case EM_PLATFORM.VIVO:
                this.initVIVOUplatformUI();
                break;
            case EM_PLATFORM.MEI_ZU:
                this.initMEIZUplatformUI();
                break;
                
        }
    }

    /**
     * 隐藏所有平台独有的UI控件
     */
    hideAllPlatformUI(){
        this.lb_wx_node.visible = false;
        this.lb_tt_node.visible = false;
        this.lb_qq_node.visible = false;
        this.lb_oppo_node.visible = false;
        this.lb_vivo_node.visible = false;
        this.lb_meizu_node.visible = false;
    }

    /**
     * 初始化公用按钮监听
     */
    initPublicBtn(){
        this.btnCreateBanner.on(Laya.Event.CLICK,this,this.onCreateBanner);
        this.btnShowShareOrVideo.on(Laya.Event.CLICK,this,this.onShowShareOrVideo);
        this.btnShowBanner.on(Laya.Event.CLICK,this,this.onShowBanner);
        this.btnHideBanner.on(Laya.Event.CLICK,this,this.onHideBanner);
        this.btnBannerAdChangeSize.on(Laya.Event.CLICK,this,this.onBannerAdChangeSize);
        this.btnShowInterstitial.on(Laya.Event.CLICK,this,this.onShowInterstitial);
        this.btnCreateInterstitialAd.on(Laya.Event.CLICK,this,this.onCreateInterstitialAd);
        this.btnShowVideoAd.on(Laya.Event.CLICK,this,this.onShowVideo);
        this.btnOver.on(Laya.Event.CLICK,this,this.onOver);
        this.btnDeepTouch.on(Laya.Event.CLICK,this,this.onDeepTouch);
        this.btnPowerAdd.on(Laya.Event.CLICK,this,this.onPowerAdd);
        this.btnPowerReduce.on(Laya.Event.CLICK,this,this.onPowerReduce);
        this.btnShare.on(Laya.Event.CLICK,this,this.onShare);
        this.btnSwicthView.on(Laya.Event.CLICK,this,this.onSwitchView);
        this.btnUnlockCustomByVideo.on(Laya.Event.CLICK,this,this.onUnlockCustomByVideo);
        this.btnExit.on(Laya.Event.CLICK,this,this.Exit);
		this.btnVibrateShort.on(Laya.Event.CLICK,this,this.VibrateShort);
        this.btnVibrateLong.on(Laya.Event.CLICK,this,this.VibrateLong);
        this.btnHasShortcutInstalled.on(Laya.Event.CLICK,this,this.HasShortcutInstalled);
        this.btnInstallShortcut.on(Laya.Event.CLICK,this,this.InstallShortcut);
        this.btnNatigateToMiniProgram.on(Laya.Event.CLICK,this,this.NavigateToMiniProgram);
        this.btnGetUserInfo.on(Laya.Event.CLICK,this,this.GetUserInfo);

        if(this.platformId == EM_PLATFORM.OPPO){
            this.btnShowInterstitial.visible = false;
            this.btnCreateInterstitialAd.visible = false;
        }
        this.btnNatigateToMiniProgram.visible = (this.platformId == EM_PLATFORM.OPPO || this.platformId == EM_PLATFORM.WX);
        if(!(this.platformId == EM_PLATFORM.OPPO || this.platformId == EM_PLATFORM.VIVO)){
            this.btnHasShortcutInstalled.visible = false;
            this.btnInstallShortcut.visible = (this.platformId == EM_PLATFORM.QQ);
        }

        this.btnShare.visible = (this.platformId == EM_PLATFORM.WX || this.platformId == EM_PLATFORM.QQ || this.platformId == EM_PLATFORM.TT);
        this.btnGetUserInfo.visible = (this.platformId == EM_PLATFORM.WX || this.platformId == EM_PLATFORM.QQ || this.platformId == EM_PLATFORM.TT);

        // if(!(this.platformId == EM_PLATFORM.OPPO || this.platformId == EM_PLATFORM.VIVO || this.platformId == EM_PLATFORM.MEI_ZU)){
        //     this.btnExit.visible = false;
        //     this.btnVibrateShort.visible = false;
        //     this.btnVibrateLong.visible = false;
        // }
    }

    /**
     * 初始化平台独有UI-微信
     */
    initWXplatformUI():void{
        this.lb_wx_node.visible = true;
        this.btnWXcreateGridAd.on(Laya.Event.CLICK,this,this.WX_CreateGridAd);
        this.btnWXshowGridAd.on(Laya.Event.CLICK,this,this.WX_ShowGridAd);
        this.btnWXhideGridAd.on(Laya.Event.CLICK,this,this.WX_HideGridAd);
        this.btnWXsubscribeSysMsg.on(Laya.Event.CLICK,this,this.WX_SubscribeSysMsg);
        this.btnWXgetSetting.on(Laya.Event.CLICK,this,this.WX_GetSetting);
        this.btnWXshowUserInfoButton.on(Laya.Event.CLICK,this,this.WX_ShowUserInfoButton);
        this.btnWXHideUserInfoButton.on(Laya.Event.CLICK,this,this.WX_HideUserInfoButton);
        // this.btnWXgetLeftTopBtnPosition.on(Laya.Event.CLICK,this,this.WX_GetLeftTopBtnPosition);

        this.btnGameRecorderStartOrStop.on(Laya.Event.CLICK,this,this.WX_GameRecorderStartOrStop);
		this.btnGameRecorderPauseOrResume.on(Laya.Event.CLICK,this,this.WX_GameRecorderPauseOrResume);
		this.btnGameRecorderAbort.on(Laya.Event.CLICK,this,this.WX_GameRecorderAbort);
		this.btnCreateGameRecorderShareButton.on(Laya.Event.CLICK,this,this.WX_CreateGameRecorderShareButton);
        this.btnGameRecorderShareButtonOnTap.on(Laya.Event.CLICK,this,this.WX_GameRecorderShareButtonOnTap);
        this.btnHideCustomAd.on(Laya.Event.CLICK,this,this.WX_HideCustomAd);
        this.btnShowCustomAd.on(Laya.Event.CLICK,this,this.WX_ShowCustomAd);
        this.btnGameRecorderPauseOrResume.visible = false;
        this.WX_SetRecordChangeCB();
    }

    /**
     * 初始化平台独有UI-字节跳动
     */
    initTTplatformUI():void{
        this.lb_tt_node.visible = true;
        this.btnTTrecordStart.on(Laya.Event.CLICK,this,this.TT_RecordStart);
        this.btnTTrecordStop.on(Laya.Event.CLICK,this,this.TT_RecordStop);
        this.btnTTrecordPasueOrResume.on(Laya.Event.CLICK,this,this.TT_RecordPasueOrResume);
        this.btnTTshareVideo.on(Laya.Event.CLICK,this,this.TT_ShareVideo);
        this.btnTTshareToken.on(Laya.Event.CLICK,this,this.TT_ShareToken);
        this.btnTTshareTemplate.on(Laya.Event.CLICK,this,this.TT_ShareTemplate);
        this.btnTTshowMoreGame.on(Laya.Event.CLICK,this,this.TT_ShowMoreGame);
        this.btnTTgetLeftTopBtnPosition.on(Laya.Event.CLICK,this,this.TT_GetLeftTopBtnPosition);
    }
    
    /**
     * 初始化平台独有UI-QQ
     */
    initQQplatformUI():void{
        this.lb_qq_node.visible = true;

        this.btnShowAppBoxAd.on(Laya.Event.CLICK,this,this.QQ_ShowAppBoxAd);
        this.btnShowBlockAd.on(Laya.Event.CLICK,this,this.QQ_ShowBlockAd);
        this.btnHideBlockAd.on(Laya.Event.CLICK,this,this.QQ_HideBlockAd);
        this.btnShareTemplate.on(Laya.Event.CLICK,this,this.QQ_ShareTemplate);
        this.btnColorSign.on(Laya.Event.CLICK,this,this.QQ_addColorSign);
        this.btnSubscribeAppMsg.on(Laya.Event.CLICK,this,this.QQ_SubscribeAppMsg);
        this.btnAddFriend.on(Laya.Event.CLICK,this,this.QQ_AddFriend);
        this.btnHideAddFriend.on(Laya.Event.CLICK,this,this.QQ_HideFriend);
        this.btnHideAddFriend.on(Laya.Event.CLICK,this,this.QQ_HideFriend);
    }

    /**
     * 初始化平台独有UI-OPPO
     */
    initOPPOplatformUI():void{
        console.warn("YDHW ---初始化平台独有UI-OPPO：");
        this.lb_oppo_node.visible = true;
        this.btnCreateNativeAd.on(Laya.Event.CLICK,this,this.OPPO_CreateNativeAd);
        this.btnShowNativeAd.on(Laya.Event.CLICK,this,this.OPPO_ShowNativeAd);
        this.btnClickNativeAd.on(Laya.Event.CLICK,this,this.OPPO_ClickNativeAd);
    }
    /**
     * 初始化平台独有UI-MEIZU
     */
    initMEIZUplatformUI():void {
        this.lb_meizu_node.visible = true;		
		// this.btnGetToken.on(Laya.Event.CLICK,this,this.MZ_GetToken);
		// this.btnGetIMEI.on(Laya.Event.CLICK,this,this.MZ_GetIMEI);
		this.btnGetNetworkType.on(Laya.Event.CLICK,this,this.MZ_GetNetworkType);
		this.btnOnNetworkStatusChange.on(Laya.Event.CLICK,this,this.MZ_OnNetworkStatusChange);
    }

    /**
     * 初始化平台独有UI-VIVO
     */
    initVIVOUplatformUI():void {
        this.lb_vivo_node.visible = true;	
        this.btnCreateNativeAd_VIVO.on(Laya.Event.CLICK,this,this.VIVO_CreateNativeAd);
		this.btnShowNativeAd_VIVO.on(Laya.Event.CLICK,this,this.VIVO_ShowNativeAd);
		this.btnClickNativeAd_VIVO.on(Laya.Event.CLICK,this,this.VIVO_ClickNativeAd);
    }
    
    
    /**
     * 视频分享策略
     */
    onShowShareOrVideo(e:Laya.Event):void{
        this.PlatFormPUB.onShowShareOrVideo();
    }

    /**
     * 创建Banner
     * @param e 
     */
    onCreateBanner(e:Laya.Event):void{
        this.PlatFormPUB.onCreateBanner();
    }

    /**
     * 显示Banner
     */
    onShowBanner(e:Laya.Event):void{
        this.PlatFormPUB.onShowBanner();
    }

    /**
     * 隐藏Banner
     */
    onHideBanner(e:Laya.Event):void{
        this.PlatFormPUB.onHideBanner();
    }

    /**
     * 改变BannerSize
     */
    onBannerAdChangeSize(e:Laya.Event):void{
        this.PlatFormPUB.onBannerAdChangeSize();
    }

    /**
     * 显示插屏广告
     */
    onShowInterstitial(e:Laya.Event):void{
        this.PlatFormPUB.onShowInterstitial();
    }
    
    /**
     * 显示激励广告
     */
    onShowVideo(e:Laya.Event):void{
        this.PlatFormPUB.onShowVideo();
    }

    /**
     * 游戏结束
     */
    onOver(e:Laya.Event):void{
        this.PlatFormPUB.onOver();
    }

    /**
     * 深度误触屏蔽
     */
    onDeepTouch(e:Laya.Event):void{
        this.PlatFormPUB.onDeepTouch();
    }

    /**
     * 增加体力
     */
    onPowerAdd(e:Laya.Event):void{
        this.PlatFormPUB.onPowerAdd();
    }

    /**
     * 消耗体力
     */
    onPowerReduce(e:Laya.Event):void{
        this.PlatFormPUB.onPowerReduce();
    }

    /**
     * 切换界面(强刷Banner)
     * @param e 
     */
    onSwitchView(e:Laya.Event):void{
        this.PlatFormPUB.onSwitchView();
    }

    /**
     * 创建插屏广告
     * @param e 
     */
    onCreateInterstitialAd(e:Laya.Event):void{
        this.PlatFormPUB.onCreateInterstitialAd();
    }

    /**
     * 视频解锁关卡
     * @param e 
     */
    onUnlockCustomByVideo(e:Laya.Event):void{
        this.PlatFormPUB.onUnlockCustomByVideo();
    }


    /**
     * 退出游戏
     * 
     * @param e 
     */
    Exit(e:Laya.Event):void{
        this.PlatFormPUB.Exit();
    }

    /**
     * 震动-短时
     * 
     * @param e 
     */
    VibrateShort(e:Laya.Event):void{
        this.PlatFormPUB.VibrateShort();
    }

    /**
     * 震动-长时
     * 
     * @param e 
     */
    VibrateLong(e:Laya.Event):void{
        this.PlatFormPUB.VibrateLong();
    }

     /**
     * 是否已经创建桌面图标
     * @param e 
     */
    HasShortcutInstalled(e:Laya.Event):void{
        this.PlatFormPUB.HasShortcutInstalled();
}

    /**
     * 创建桌面图标
     * @param e 
     */
    InstallShortcut(e:Laya.Event):void{
        this.PlatFormPUB.InstallShortcut();
    }

    /**
     * 获取平台用户信息
     * @param e 
     */
    GetUserInfo(e:Laya.Event):void{
        this.PlatFormPUB.GetUserInfo();
    }

    /**
     * 卖量跳转
     * @param e 
     */
    NavigateToMiniProgram(e:Laya.Event):void{
        let sideBoxList = this.PlatFormPUB.getSideBoxList();
        if(!sideBoxList || sideBoxList.length == 0){
            console.log("GameUI -NavigateToMiniProgram-侧边栏数据为空");
            return;
        }
        let id = sideBoxList[0]._id;
        let toAppid = sideBoxList[0].toAppid;
        let toUrl = sideBoxList[0].toUrl;
        console.log("GameUI -NavigateToMiniProgram:",id,toAppid,toUrl);
        this.PlatForm.NavigateToMiniProgram(id,toAppid,toUrl,"MainScene_SideBox");
    }

    /**
     * 分享
     * @param e 
     */
    onShare(e:Laya.Event):void{
        this.PlatForm.onShareAppMessage();
    }


    /********************************************************************/
    /****                     微信平台独有接口                      ******/
    /********************************************************************/

    /**
     * 创建格子广告
     * @param e 
     */
    WX_CreateGridAd(e:Laya.Event):void{
        this.PlatForm.CreateGridAd();
    }

    /**
     * 显示格子广告
     * @param e 
     */
    WX_ShowGridAd(e:Laya.Event):void{
        this.PlatForm.ShowGridAd();
    }

    /**
     * 隐藏格子广告
     * @param e 
     */
    WX_HideGridAd(e:Laya.Event):void{
        this.PlatForm.HideGridAd();
    }

    /**
     * 系统订阅
     * @param e 
     */
    WX_SubscribeSysMsg(e:Laya.Event):void{
        this.PlatForm.SubscribeSysMsg();
    }

    /**
     * 获取用户当前设置
     * @param e 
     */
    WX_GetSetting(e:Laya.Event):void{
        this.PlatForm.GetSetting();
    }

    /**
     * 显示用户信息
     * @param e 
     */
    WX_ShowUserInfoButton(e:Laya.Event):void{
        this.PlatForm.ShowUserInfoButton();
    }

    /**
     * 隐藏用户信息按钮
     * @param e 
     */
    WX_HideUserInfoButton(e:Laya.Event):void{
        this.PlatForm.HideUserInfoButton();
    }

    /**
     * 获取胶囊按钮左侧位置
     * @param e 
     */
    WX_GetLeftTopBtnPosition(e:Laya.Event):void{
        this.PlatForm.GetLeftTopBtnPosition();
    }

    /**
     * 开始或停止录制游戏
     * @param e 
     */
    WX_GameRecorderStartOrStop(e:Laya.Event):void{
        this.PlatForm.GameRecorderStartOrStop();
    }

    /**
     * 开始或停止录制游戏
     * @param e 
     */
    WX_GameRecorderPauseOrResume(e:Laya.Event):void{
        this.PlatForm.GameRecorderPauseOrResume();
    }
    
    /**
     * 放弃录制游戏画面
     * @param e 
     */
    WX_GameRecorderAbort(e:Laya.Event):void{
        this.PlatForm.GameRecorderAbort();
    }

    /**
     * 创建游戏对局回放分享按钮
     * @param e 
     */
    WX_CreateGameRecorderShareButton(e:Laya.Event):void{
        this.PlatForm.CreateGameRecorderShareButton();
    }

    /**
     * 监听游戏对局回放分享按钮的点击事件
     * @param e 
     */
    WX_GameRecorderShareButtonOnTap(e:Laya.Event):void{
        this.PlatForm.GameRecorderShareButtonOnTap();
    }
    /**
     * 录屏状态变化监听设置
     */
    WX_SetRecordChangeCB(){
        this.PlatForm.SetRecordChangeCB((res: any)=>{
            let status = this.PlatForm.GetRecordStatus();
            let startOrStop = "开始录屏";
            let pauseOrResume = "暂停录屏";
            switch(status){
                case EM_RECORD_STATUS.E_NONE:
                    startOrStop = "开始录屏";
                    pauseOrResume = "暂停录屏";
                    this.btnGameRecorderPauseOrResume.visible = false;
                    break;
                case EM_RECORD_STATUS.E_PAUSE:
                    startOrStop = "停止录屏";
                    pauseOrResume = "继续录屏";
                    break;
                case EM_RECORD_STATUS.E_RECORDING:
                    startOrStop = "停止录屏";
                    pauseOrResume = "暂停录屏";
                    this.btnGameRecorderPauseOrResume.visible = true;
                    break;
            }
            this.btnGameRecorderStartOrStop.label = startOrStop;
            this.btnGameRecorderPauseOrResume.label = pauseOrResume;
        });
    }
    
    /**
     * 原生模板广告-隐藏
     * @param e 
     */
    WX_ShowCustomAd(e:Laya.Event):void{
        this.PlatForm.ShowCustomAd();
    }

    /**
     * 原生模板广告-隐藏
     * @param e 
     */
    WX_HideCustomAd(e:Laya.Event):void{
        this.PlatForm.HideCustomAd();
    }

    /********************************************************************/
    /****                   字节跳动平台独有接口                     ******/
    /********************************************************************/

    /**
     * 开始或停止录屏
     * @param e 
     */
    TT_RecordStart(e:Laya.Event):void{
        this.PlatForm.onRecordStart();
    }

    /**
     * 停止录屏
     * @param e 
     */
    TT_RecordStop(e:Laya.Event):void{
        console.log("YDHW 停止录屏");
        this.PlatForm.onRecordStop();
    }

    /**
     * 继续录屏/暂停录屏
     * @param e 
     */
    TT_RecordPasueOrResume(e:Laya.Event):void{
        // {
        //     None = 0,
        //     Recording = 1,
        //     Pasuse = 2,
        // }
        let status = this.PlatForm.getRecordStatus();
        if(status === 2){
            this.PlatForm.onRecordResume();
        }else{
            this.PlatForm.onRecordPasue();
        }
    }

    /**
     * 视频分享(含模板)
     * @param e 
     */
    TT_ShareVideo(e:Laya.Event):void{
        this.PlatForm.onShareVideo();
    }

    /**
     * Token分享(含模板)
     * @param e 
     */
    TT_ShareToken(e:Laya.Event):void{
        this.PlatForm.onShareToken();
    }

    /**
     * 模板分享
     * @param e 
     */
    TT_ShareTemplate(e:Laya.Event):void{
        this.PlatForm.onShareTemplate();
    }

    /**
     * 显示更多游戏
     * @param e 
     */
    TT_ShowMoreGame(e:Laya.Event):void{
        this.PlatForm.showMoreGame();
    }

    /**
     * 获取胶囊按钮左侧位置
     * @param e 
     */
    TT_GetLeftTopBtnPosition(e:Laya.Event):void{
        this.PlatForm.GetLeftTopBtnPosition();
    }

    /**
     * 获取平台用户信息
     * @param e 
     */
    TT_GetPlatformUserInfo(e:Laya.Event):void{
        this.PlatForm.GetPlatformUserInfo();
    }




    /********************************************************************/
    /****                     QQ平台独有接口                        ******/
    /********************************************************************/

    /**
     *显示盒子广告 
     */
    QQ_ShowAppBoxAd(e:Laya.Event):void{
        this.PlatForm.CreateAppBox();
    }

    /**
     * 显示积木广告
     */
    QQ_ShowBlockAd(e:Laya.Event):void{
        this.PlatForm.CreateBlockAd();
    }

    /**
     * 隐藏积木广告
     */
    QQ_HideBlockAd(e:Laya.Event):void{
        this.PlatForm.HideBlockAd();
    }

    /**
     * 分享(邀请类模板)
     */
    QQ_ShareTemplate(e:Laya.Event):void{
        this.PlatForm.ShareByTemplateId();
    }

    /**
     * 添加彩签
     */
    QQ_addColorSign(e:Laya.Event):void{
        this.PlatForm.AddColorSign();
    }

    /**
     * 主动订阅
     */
    QQ_SubscribeAppMsg(e:Laya.Event):void{
        this.PlatForm.SubscribeAppMsg();
    }

    /**
     * 创建-打开添加好友页面的按钮
     */
    QQ_AddFriend(e:Laya.Event):void{
        this.PlatForm.CreateAddFriendButton();
    }
    /**
     * 隐藏-打开添加好友页面的按钮
     */
    QQ_HideFriend(e:Laya.Event):void{
        this.PlatForm.HideAddFriendButton();
    }


    /********************************************************************/
    /****                     OPPO平台独有接口                      ******/
    /********************************************************************/

    /**
     * 创建-原生广告
     * @param e 
     */
    OPPO_CreateNativeAd(e:Laya.Event):void{
        this.PlatForm.CreateNativeAd();
    }

    /**
     * 显示-原生广告
     * @param e 
     */
    OPPO_ShowNativeAd(e:Laya.Event):void{
        this.PlatForm.ShowNativeAd();
    }

    /**
     * 点击-原生广告
     * @param e 
     */
    OPPO_ClickNativeAd(e:Laya.Event):void{
        this.PlatForm.ClickNativeAd();
    }

    /********************************************************************/
    /****                     魅族平台独有接口                      ******/
    /********************************************************************/

    /**
     * 获取网络类型
     * 
     * @param e 
     */
    MZ_GetNetworkType(e:Laya.Event):void{
        this.PlatForm.GetNetworkType();
    }

    /**
     * 监听网络状态变化事件
     * 
     * @param e 
     */
    MZ_OnNetworkStatusChange(e:Laya.Event):void{
        this.PlatForm.OnNetworkStatusChange();
    }

    /********************************************************************/
    /****                     VIVO平台独有接口                      ******/
    /********************************************************************/

    /**
     * 原生广告-创建
     * @param e 
     */
    VIVO_CreateNativeAd(e:Laya.Event):void{
        this.PlatForm.CreateNativeAd();
    }
    
    /**
     * 原生广告-上报广告曝光
     * @param e 
     */
    VIVO_ShowNativeAd(e:Laya.Event):void{
        this.PlatForm.ShowNativeAd();
    }

    /**
     * 原生广告-上报广告点击
     * @param e 
     */
    VIVO_ClickNativeAd(e:Laya.Event):void{
        this.PlatForm.ClickNativeAd();
    }

    /**
     * 判断用户是否通过桌面图标来启动应用
     * @param e 
     */
    VIVO_IsStartupByShortcut(e:Laya.Event):void{
        this.PlatForm.IsStartupByShortcut();
    }
}