import { YDHW } from "../../libs/ydhw/ydhw.sdk";

/**
 * VIVO平台独有接口
 */
export class PlatformVIVO {
    private nativeAdId:string = null;

    constructor() {
    }
    
    /**
     * 创建-原生广告
     */
    CreateNativeAd():void{
        ydhw.Invoke(YDHW.YDHW_API.CreateNativeAd,this,
            (args: any)=>{
                //这个返回的数组请一定要以返回的数据为准，官方文档说是只有一层，本人实测套了三层
                console.log("PlatformVIVO ---CreateNativeAd-back:",JSON.stringify(args));
                this.nativeAdId = args[0].adId;
                console.log("PlatformVIVO ---nativeAdId:",this.nativeAdId);
            });
    }
    /**
     * 显示上报-原生广告
     */
    ShowNativeAd():void{
        console.log("PlatformVIVO ---ShowNativeAd:",this.nativeAdId);
        if(this.nativeAdId != null) ydhw.Invoke(YDHW.YDHW_API.ShowNativeAd,this.nativeAdId);
    }

    /**
     * 点击上报-原生广告
     */
    ClickNativeAd():void{
        console.log("PlatformVIVO ---ClickNativeAd:",this.nativeAdId);
        if(this.nativeAdId != null) ydhw.Invoke(YDHW.YDHW_API.ClickNativeAd,this.nativeAdId);
    }

    /**
     * 判断用户是否通过桌面图标来启动应用
     */
    IsStartupByShortcut():void{
        ydhw.Invoke(YDHW.YDHW_VIVO_API.IsStartupByShortcut,this,
            ()=>{
                console.log("PlatformVIVO ---IsStartupByShortcut-onSuccess");
            },
            (error: any)=>{
                console.log("PlatformVIVO ---IsStartupByShortcut-error:"+JSON.stringify(error));
            });
    }
}