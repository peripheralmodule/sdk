(function () {
	'use strict';

	var REG = Laya.ClassUtils.regClass;
	var ui;
	(function (ui) {
	    var test;
	    (function (test) {
	        class TestSceneUI extends Laya.Scene {
	            constructor() { super(); }
	            createChildren() {
	                super.createChildren();
	                this.loadScene("test/TestScene");
	            }
	        }
	        test.TestSceneUI = TestSceneUI;
	        REG("ui.test.TestSceneUI", TestSceneUI);
	    })(test = ui.test || (ui.test = {}));
	})(ui || (ui = {}));

	var YDHW;
	(function (YDHW) {
	    let YDHW_WECHAT_API;
	    (function (YDHW_WECHAT_API) {
	        YDHW_WECHAT_API[YDHW_WECHAT_API["CreateGridAd"] = 20001] = "CreateGridAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["ShowGridAd"] = 20002] = "ShowGridAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["HideGridAd"] = 20003] = "HideGridAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["SubscribeSysMsg"] = 20004] = "SubscribeSysMsg";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["GetSetting"] = 20005] = "GetSetting";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderOff"] = 20006] = "WXRecorderOff";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderOn"] = 20007] = "WXRecorderOn";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderStart"] = 20008] = "WXRecorderStart";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderStop"] = 20009] = "WXRecorderStop";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderPause"] = 20010] = "WXRecorderPause";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderResume"] = 20011] = "WXRecorderResume";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["WXRecorderAbort"] = 20012] = "WXRecorderAbort";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["IsAtempoSupported"] = 20013] = "IsAtempoSupported";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["IsFrameSupported"] = 20014] = "IsFrameSupported";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["IsSoundSupported"] = 20015] = "IsSoundSupported";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["IsVolumeSupported"] = 20016] = "IsVolumeSupported";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["CreateRecorderShareButton"] = 20017] = "CreateRecorderShareButton";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["RecorderShareButtonHide"] = 20018] = "RecorderShareButtonHide";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["RecorderShareButtonShow"] = 20019] = "RecorderShareButtonShow";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["RecorderShareButtonOnTap"] = 20020] = "RecorderShareButtonOnTap";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["RecorderShareButtonOffTap"] = 20021] = "RecorderShareButtonOffTap";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["SetMessageToFriendQuery"] = 20022] = "SetMessageToFriendQuery";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["OnShareMessageToFriend"] = 20023] = "OnShareMessageToFriend";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["OnShareTimeline"] = 20024] = "OnShareTimeline";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["OffShareTimeline"] = 20025] = "OffShareTimeline";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["CreateCustomAd"] = 20026] = "CreateCustomAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["ShowCustomAd"] = 20027] = "ShowCustomAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["HideCustomAd"] = 20028] = "HideCustomAd";
	        YDHW_WECHAT_API[YDHW_WECHAT_API["IsShowCustomAd"] = 20029] = "IsShowCustomAd";
	    })(YDHW_WECHAT_API = YDHW.YDHW_WECHAT_API || (YDHW.YDHW_WECHAT_API = {}));
	    let YDHW_QQ_API;
	    (function (YDHW_QQ_API) {
	        YDHW_QQ_API[YDHW_QQ_API["ShareByTemplateId"] = 30001] = "ShareByTemplateId";
	        YDHW_QQ_API[YDHW_QQ_API["AddColorSign"] = 30002] = "AddColorSign";
	        YDHW_QQ_API[YDHW_QQ_API["SubscribeAppMsg"] = 30003] = "SubscribeAppMsg";
	        YDHW_QQ_API[YDHW_QQ_API["CreateAddFriendButton"] = 30004] = "CreateAddFriendButton";
	        YDHW_QQ_API[YDHW_QQ_API["ShowAddFriendButton"] = 30005] = "ShowAddFriendButton";
	        YDHW_QQ_API[YDHW_QQ_API["HideAddFriendButton"] = 30006] = "HideAddFriendButton";
	        YDHW_QQ_API[YDHW_QQ_API["DestroyAddFriendButton"] = 30007] = "DestroyAddFriendButton";
	        YDHW_QQ_API[YDHW_QQ_API["CreateAppBox"] = 30008] = "CreateAppBox";
	        YDHW_QQ_API[YDHW_QQ_API["ShowAppBox"] = 30009] = "ShowAppBox";
	        YDHW_QQ_API[YDHW_QQ_API["CreateBlockAd"] = 30010] = "CreateBlockAd";
	        YDHW_QQ_API[YDHW_QQ_API["ShowBlockAd"] = 30011] = "ShowBlockAd";
	        YDHW_QQ_API[YDHW_QQ_API["HideBlockAd"] = 30012] = "HideBlockAd";
	        YDHW_QQ_API[YDHW_QQ_API["DestroyBlockAd"] = 30013] = "DestroyBlockAd";
	    })(YDHW_QQ_API = YDHW.YDHW_QQ_API || (YDHW.YDHW_QQ_API = {}));
	    let YDHW_TT_API;
	    (function (YDHW_TT_API) {
	        YDHW_TT_API[YDHW_TT_API["ShareImage"] = 40001] = "ShareImage";
	        YDHW_TT_API[YDHW_TT_API["ShareVideo"] = 40002] = "ShareVideo";
	        YDHW_TT_API[YDHW_TT_API["ShareTemplate"] = 40003] = "ShareTemplate";
	        YDHW_TT_API[YDHW_TT_API["ShareToken"] = 40004] = "ShareToken";
	        YDHW_TT_API[YDHW_TT_API["ShowMoreGamesModal"] = 40005] = "ShowMoreGamesModal";
	        YDHW_TT_API[YDHW_TT_API["CreateMoreGamesButton"] = 40006] = "CreateMoreGamesButton";
	        YDHW_TT_API[YDHW_TT_API["RecorderStart"] = 40007] = "RecorderStart";
	        YDHW_TT_API[YDHW_TT_API["RecorderStop"] = 40008] = "RecorderStop";
	        YDHW_TT_API[YDHW_TT_API["RecorderPause"] = 40009] = "RecorderPause";
	        YDHW_TT_API[YDHW_TT_API["RecorderResume"] = 40010] = "RecorderResume";
	    })(YDHW_TT_API = YDHW.YDHW_TT_API || (YDHW.YDHW_TT_API = {}));
	    let YDHW_OPPO_API;
	    (function (YDHW_OPPO_API) {
	    })(YDHW_OPPO_API = YDHW.YDHW_OPPO_API || (YDHW.YDHW_OPPO_API = {}));
	    let YDHW_VIVO_API;
	    (function (YDHW_VIVO_API) {
	        YDHW_VIVO_API[YDHW_VIVO_API["IsStartupByShortcut"] = 50001] = "IsStartupByShortcut";
	    })(YDHW_VIVO_API = YDHW.YDHW_VIVO_API || (YDHW.YDHW_VIVO_API = {}));
	    let YDHW_MZ_API;
	    (function (YDHW_MZ_API) {
	        YDHW_MZ_API[YDHW_MZ_API["GetNetworkType"] = 140001] = "GetNetworkType";
	        YDHW_MZ_API[YDHW_MZ_API["OnNetworkStatusChange"] = 140002] = "OnNetworkStatusChange";
	    })(YDHW_MZ_API = YDHW.YDHW_MZ_API || (YDHW.YDHW_MZ_API = {}));
	    let YDHW_API;
	    (function (YDHW_API) {
	        YDHW_API[YDHW_API["ShareInfo"] = 10001] = "ShareInfo";
	        YDHW_API[YDHW_API["StatBanner"] = 10002] = "StatBanner";
	        YDHW_API[YDHW_API["StatVideo"] = 10003] = "StatVideo";
	        YDHW_API[YDHW_API["StatInterstitial"] = 10004] = "StatInterstitial";
	        YDHW_API[YDHW_API["StatGrid"] = 10005] = "StatGrid";
	        YDHW_API[YDHW_API["StatAppBox"] = 10006] = "StatAppBox";
	        YDHW_API[YDHW_API["StatBlock"] = 10007] = "StatBlock";
	        YDHW_API[YDHW_API["StatNativeAd"] = 10008] = "StatNativeAd";
	        YDHW_API[YDHW_API["StatErrorStack"] = 10009] = "StatErrorStack";
	        YDHW_API[YDHW_API["StatResult"] = 10010] = "StatResult";
	        YDHW_API[YDHW_API["StatEvent"] = 10011] = "StatEvent";
	        YDHW_API[YDHW_API["StatDuration"] = 10012] = "StatDuration";
	        YDHW_API[YDHW_API["StatClickOut"] = 10013] = "StatClickOut";
	        YDHW_API[YDHW_API["ShowShareVideo"] = 10014] = "ShowShareVideo";
	        YDHW_API[YDHW_API["UseShareVideoStrategy"] = 10015] = "UseShareVideoStrategy";
	        YDHW_API[YDHW_API["SwitchView"] = 10016] = "SwitchView";
	        YDHW_API[YDHW_API["GetDeepTouchInfo"] = 10017] = "GetDeepTouchInfo";
	        YDHW_API[YDHW_API["GetCustomConfig"] = 10018] = "GetCustomConfig";
	        YDHW_API[YDHW_API["GameOver"] = 10019] = "GameOver";
	        YDHW_API[YDHW_API["IsUnlockVideo"] = 10020] = "IsUnlockVideo";
	        YDHW_API[YDHW_API["GetPowerInfo"] = 10021] = "GetPowerInfo";
	        YDHW_API[YDHW_API["ListenOnPowerChange"] = 10022] = "ListenOnPowerChange";
	        YDHW_API[YDHW_API["SetPower"] = 10023] = "SetPower";
	        YDHW_API[YDHW_API["GetPower"] = 10024] = "GetPower";
	        YDHW_API[YDHW_API["GetSideBox"] = 10025] = "GetSideBox";
	        YDHW_API[YDHW_API["GetScoreBoardList"] = 10026] = "GetScoreBoardList";
	        YDHW_API[YDHW_API["GetScoreBoardAward"] = 10027] = "GetScoreBoardAward";
	        YDHW_API[YDHW_API["ShareCard"] = 10028] = "ShareCard";
	        YDHW_API[YDHW_API["GetTodayBoutCount"] = 10029] = "GetTodayBoutCount";
	        YDHW_API[YDHW_API["GetTotalBoutCount"] = 10030] = "GetTotalBoutCount";
	        YDHW_API[YDHW_API["GetLastBountNumber"] = 10031] = "GetLastBountNumber";
	        YDHW_API[YDHW_API["GetMaxBountNumber"] = 10032] = "GetMaxBountNumber";
	        YDHW_API[YDHW_API["GetTodayWatchVideoCounter"] = 10033] = "GetTodayWatchVideoCounter";
	        YDHW_API[YDHW_API["CreateBannerAd"] = 10034] = "CreateBannerAd";
	        YDHW_API[YDHW_API["CreateSmallBannerAd"] = 10035] = "CreateSmallBannerAd";
	        YDHW_API[YDHW_API["CreateCustomBannerAd"] = 10036] = "CreateCustomBannerAd";
	        YDHW_API[YDHW_API["BannerAdChangeSize"] = 10037] = "BannerAdChangeSize";
	        YDHW_API[YDHW_API["ShowBannerAd"] = 10038] = "ShowBannerAd";
	        YDHW_API[YDHW_API["HideBannerAd"] = 10039] = "HideBannerAd";
	        YDHW_API[YDHW_API["ChangeBannerStyle"] = 10040] = "ChangeBannerStyle";
	        YDHW_API[YDHW_API["CreateRewardVideoAd"] = 10041] = "CreateRewardVideoAd";
	        YDHW_API[YDHW_API["ShowRewardVideoAd"] = 10042] = "ShowRewardVideoAd";
	        YDHW_API[YDHW_API["StatShareCard"] = 10043] = "StatShareCard";
	        YDHW_API[YDHW_API["StatShareCardInner"] = 10044] = "StatShareCardInner";
	        YDHW_API[YDHW_API["GetLayerList"] = 10045] = "GetLayerList";
	        YDHW_API[YDHW_API["NavigateToMiniProgram"] = 10046] = "NavigateToMiniProgram";
	        YDHW_API[YDHW_API["GetUserInfo"] = 10047] = "GetUserInfo";
	        YDHW_API[YDHW_API["CreateUserInfoButton"] = 10048] = "CreateUserInfoButton";
	        YDHW_API[YDHW_API["ShowUserInfoButton"] = 10049] = "ShowUserInfoButton";
	        YDHW_API[YDHW_API["HideUserInfoButton"] = 10050] = "HideUserInfoButton";
	        YDHW_API[YDHW_API["DestroyUserInfoButton"] = 10051] = "DestroyUserInfoButton";
	        YDHW_API[YDHW_API["CreateInterstitialAd"] = 10052] = "CreateInterstitialAd";
	        YDHW_API[YDHW_API["ShowInterstitialAd"] = 10053] = "ShowInterstitialAd";
	        YDHW_API[YDHW_API["GetSharingResults"] = 10054] = "GetSharingResults";
	        YDHW_API[YDHW_API["StatLayer"] = 10055] = "StatLayer";
	        YDHW_API[YDHW_API["GetShareRewardLimit"] = 10056] = "GetShareRewardLimit";
	        YDHW_API[YDHW_API["GetVideoRewardLimit"] = 10057] = "GetVideoRewardLimit";
	        YDHW_API[YDHW_API["GetServerInfo"] = 10058] = "GetServerInfo";
	        YDHW_API[YDHW_API["ExitGame"] = 10059] = "ExitGame";
	        YDHW_API[YDHW_API["VibrateShort"] = 10060] = "VibrateShort";
	        YDHW_API[YDHW_API["VibrateLong"] = 10061] = "VibrateLong";
	        YDHW_API[YDHW_API["InstallShortcut"] = 10062] = "InstallShortcut";
	        YDHW_API[YDHW_API["CreateNativeAd"] = 10063] = "CreateNativeAd";
	        YDHW_API[YDHW_API["ShowNativeAd"] = 10064] = "ShowNativeAd";
	        YDHW_API[YDHW_API["ClickNativeAd"] = 10065] = "ClickNativeAd";
	        YDHW_API[YDHW_API["HasShortcutInstalled"] = 10066] = "HasShortcutInstalled";
	        YDHW_API[YDHW_API["ShareAppMessage"] = 10067] = "ShareAppMessage";
	        YDHW_API[YDHW_API["Hook"] = 10068] = "Hook";
	        YDHW_API[YDHW_API["OnFrontend"] = 10069] = "OnFrontend";
	        YDHW_API[YDHW_API["OnBackend"] = 10070] = "OnBackend";
	        YDHW_API[YDHW_API["OnShow"] = 10071] = "OnShow";
	        YDHW_API[YDHW_API["OnHide"] = 10072] = "OnHide";
	        YDHW_API[YDHW_API["OnError"] = 10073] = "OnError";
	        YDHW_API[YDHW_API["GetObject"] = 10074] = "GetObject";
	        YDHW_API[YDHW_API["GetString"] = 10075] = "GetString";
	        YDHW_API[YDHW_API["SetObject"] = 10076] = "SetObject";
	        YDHW_API[YDHW_API["SetString"] = 10077] = "SetString";
	        YDHW_API[YDHW_API["Size"] = 10078] = "Size";
	        YDHW_API[YDHW_API["DeleteObject"] = 10079] = "DeleteObject";
	        YDHW_API[YDHW_API["StoreValue"] = 10080] = "StoreValue";
	        YDHW_API[YDHW_API["Login"] = 10081] = "Login";
	        YDHW_API[YDHW_API["ScoreBoard"] = 10083] = "ScoreBoard";
	        YDHW_API[YDHW_API["IsCanRewardByVideoOrShare"] = 10084] = "IsCanRewardByVideoOrShare";
	        YDHW_API[YDHW_API["StatCustomAd"] = 10085] = "StatCustomAd";
	    })(YDHW_API = YDHW.YDHW_API || (YDHW.YDHW_API = {}));
	    let EM_TP_AD;
	    (function (EM_TP_AD) {
	        EM_TP_AD[EM_TP_AD["CREATE"] = 0] = "CREATE";
	        EM_TP_AD[EM_TP_AD["LOAD_SUCCESS"] = 1] = "LOAD_SUCCESS";
	        EM_TP_AD[EM_TP_AD["LOAD_FAIL"] = 2] = "LOAD_FAIL";
	        EM_TP_AD[EM_TP_AD["CLICK"] = 3] = "CLICK";
	        EM_TP_AD[EM_TP_AD["SHOW"] = 4] = "SHOW";
	        EM_TP_AD[EM_TP_AD["CLOSE"] = 5] = "CLOSE";
	        EM_TP_AD[EM_TP_AD["EXPOSURE"] = 6] = "EXPOSURE";
	        EM_TP_AD[EM_TP_AD["PLAY_CANCEL"] = 7] = "PLAY_CANCEL";
	        EM_TP_AD[EM_TP_AD["PLAY_FINISH"] = 8] = "PLAY_FINISH";
	    })(EM_TP_AD = YDHW.EM_TP_AD || (YDHW.EM_TP_AD = {}));
	    let EM_SHARE_TYPE;
	    (function (EM_SHARE_TYPE) {
	        EM_SHARE_TYPE[EM_SHARE_TYPE["None"] = 0] = "None";
	        EM_SHARE_TYPE[EM_SHARE_TYPE["Share"] = 1] = "Share";
	        EM_SHARE_TYPE[EM_SHARE_TYPE["Video"] = 2] = "Video";
	    })(EM_SHARE_TYPE = YDHW.EM_SHARE_TYPE || (YDHW.EM_SHARE_TYPE = {}));
	    let EM_POWER_RECOVERY_TYPE;
	    (function (EM_POWER_RECOVERY_TYPE) {
	        EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["None"] = 0] = "None";
	        EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["WatchVideo"] = 1] = "WatchVideo";
	        EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["AutoRecovery"] = 2] = "AutoRecovery";
	        EM_POWER_RECOVERY_TYPE[EM_POWER_RECOVERY_TYPE["CountDown"] = 3] = "CountDown";
	    })(EM_POWER_RECOVERY_TYPE = YDHW.EM_POWER_RECOVERY_TYPE || (YDHW.EM_POWER_RECOVERY_TYPE = {}));
	    let EM_VIDEO_PLAY_TYPE;
	    (function (EM_VIDEO_PLAY_TYPE) {
	        EM_VIDEO_PLAY_TYPE[EM_VIDEO_PLAY_TYPE["VIDEO_PLAY_FAIL"] = 0] = "VIDEO_PLAY_FAIL";
	        EM_VIDEO_PLAY_TYPE[EM_VIDEO_PLAY_TYPE["VIDEO_PLAY_FINISH"] = 1] = "VIDEO_PLAY_FINISH";
	        EM_VIDEO_PLAY_TYPE[EM_VIDEO_PLAY_TYPE["VIDEO_PLAY_CANCEL"] = 2] = "VIDEO_PLAY_CANCEL";
	    })(EM_VIDEO_PLAY_TYPE = YDHW.EM_VIDEO_PLAY_TYPE || (YDHW.EM_VIDEO_PLAY_TYPE = {}));
	    let WX;
	    (function (WX) {
	        class OnShareTimelineInfo {
	            constructor(imageUrl, title, query) {
	                if (imageUrl)
	                    this.imageUrl = imageUrl;
	                if (title)
	                    this.title = title;
	                if (query)
	                    this.query = query;
	            }
	        }
	        WX.OnShareTimelineInfo = OnShareTimelineInfo;
	        class GameRecorderInfo {
	            constructor(fps, duration, bitrate, gop, hookBgm) {
	                if (fps)
	                    this.fps = fps;
	                if (duration)
	                    this.duration = duration;
	                if (gop)
	                    this.gop = gop;
	                if (hookBgm)
	                    this.hookBgm = hookBgm;
	            }
	        }
	        WX.GameRecorderInfo = GameRecorderInfo;
	        class GameRecorderBtnInfo {
	            constructor(style, share, icon, image, text) {
	                if (style)
	                    this.style = style;
	                if (share)
	                    this.share = share;
	                if (icon)
	                    this.icon = icon;
	                if (image)
	                    this.image = image;
	                if (text)
	                    this.text = text;
	            }
	        }
	        WX.GameRecorderBtnInfo = GameRecorderBtnInfo;
	        class GameRecorderBtnStyle {
	            constructor(left, top, height, iconMarginRight, fontSize, color, paddingLeft, paddingRight) {
	                if (left)
	                    this.left = left;
	                if (top)
	                    this.top = top;
	                if (height)
	                    this.height = height;
	                if (iconMarginRight)
	                    this.iconMarginRight = iconMarginRight;
	                if (fontSize)
	                    this.fontSize = fontSize;
	                if (color)
	                    this.color = color;
	                if (paddingLeft)
	                    this.paddingLeft = paddingLeft;
	                if (paddingRight)
	                    this.paddingRight = paddingRight;
	            }
	        }
	        WX.GameRecorderBtnStyle = GameRecorderBtnStyle;
	        class GameRecorderShareInfo {
	            constructor(query, bgm, title, button, timeRange, volume, atempo, audioMix) {
	                if (query)
	                    this.query = query;
	                if (bgm)
	                    this.bgm = bgm;
	                if (title)
	                    this.title = title;
	                if (button)
	                    this.button = button;
	                if (timeRange)
	                    this.timeRange = timeRange;
	                if (volume)
	                    this.volume = volume;
	                if (atempo)
	                    this.atempo = atempo;
	                if (audioMix)
	                    this.audioMix = audioMix;
	            }
	        }
	        WX.GameRecorderShareInfo = GameRecorderShareInfo;
	        class GameRecorderTitle {
	            constructor(template, data) {
	                if (template)
	                    this.template = template;
	                if (data)
	                    this.data = data;
	            }
	        }
	        WX.GameRecorderTitle = GameRecorderTitle;
	        class GameRecorderBotton {
	            constructor(template) {
	                if (template)
	                    this.template = template;
	            }
	        }
	        WX.GameRecorderBotton = GameRecorderBotton;
	        class GameRecorderTitleData {
	            constructor(score, level, opponent_openid, cost_seconds) {
	                if (score)
	                    this.score = score;
	                if (level)
	                    this.level = level;
	                if (opponent_openid)
	                    this.opponent_openid = opponent_openid;
	                if (cost_seconds)
	                    this.cost_seconds = cost_seconds;
	            }
	        }
	        WX.GameRecorderTitleData = GameRecorderTitleData;
	    })(WX = YDHW.WX || (YDHW.WX = {}));
	    let TT;
	    (function (TT) {
	        class TTSideBoxInfo {
	        }
	        TT.TTSideBoxInfo = TTSideBoxInfo;
	    })(TT = YDHW.TT || (YDHW.TT = {}));
	    let QQ;
	    (function (QQ) {
	        class ShareTempletInfo {
	        }
	        QQ.ShareTempletInfo = ShareTempletInfo;
	        class SHareTemplateData {
	        }
	        QQ.SHareTemplateData = SHareTemplateData;
	        class AppMsgInfo {
	        }
	        QQ.AppMsgInfo = AppMsgInfo;
	        class AddFriendButtonInfo {
	        }
	        QQ.AddFriendButtonInfo = AddFriendButtonInfo;
	        class FbStyle {
	        }
	        QQ.FbStyle = FbStyle;
	        class BlockAdInfo {
	        }
	        QQ.BlockAdInfo = BlockAdInfo;
	        class BlockStyle {
	        }
	        QQ.BlockStyle = BlockStyle;
	        let EM_SHARE_APP_TYPE;
	        (function (EM_SHARE_APP_TYPE) {
	            EM_SHARE_APP_TYPE["QQ"] = "qq";
	            EM_SHARE_APP_TYPE["QQ_FAST_SHARE"] = "qqFastShare";
	            EM_SHARE_APP_TYPE["QQ_FAST_SHARE_LIST"] = "qqFastShareList";
	            EM_SHARE_APP_TYPE["QZONE"] = "qzone";
	            EM_SHARE_APP_TYPE["WECHARTFRIENDS"] = "wechatFriends";
	            EM_SHARE_APP_TYPE["WECHATMOMENT"] = "wechatMoment";
	        })(EM_SHARE_APP_TYPE = QQ.EM_SHARE_APP_TYPE || (QQ.EM_SHARE_APP_TYPE = {}));
	    })(QQ = YDHW.QQ || (YDHW.QQ = {}));
	    class ClickOutRequest {
	    }
	    YDHW.ClickOutRequest = ClickOutRequest;
	    class ShareAppInfo {
	    }
	    YDHW.ShareAppInfo = ShareAppInfo;
	    class StatisticResultInfo {
	    }
	    YDHW.StatisticResultInfo = StatisticResultInfo;
	    class AdStyle {
	    }
	    YDHW.AdStyle = AdStyle;
	    class SdkInfo {
	    }
	    YDHW.SdkInfo = SdkInfo;
	    class StatistiicShareInfo {
	        constructor(sharecardId, sType, target, real) {
	            this.sharecardId = sharecardId || -1;
	            if (sType != null)
	                this.sType = sType;
	            if (target != null)
	                this.target = target;
	            this.real = real || 0;
	        }
	    }
	    YDHW.StatistiicShareInfo = StatistiicShareInfo;
	    class StoreValueRequest {
	        constructor(name, cmd, args) {
	            if (name)
	                this.name = name;
	            if (cmd)
	                this.cmd = cmd;
	            if (args)
	                this.args = args;
	        }
	    }
	    YDHW.StoreValueRequest = StoreValueRequest;
	    class EditRequest {
	    }
	    YDHW.EditRequest = EditRequest;
	})(YDHW || (YDHW = {}));

	var EM_RECORD_TYPE;
	(function (EM_RECORD_TYPE) {
	    EM_RECORD_TYPE[EM_RECORD_TYPE["None"] = 0] = "None";
	    EM_RECORD_TYPE[EM_RECORD_TYPE["Recording"] = 1] = "Recording";
	    EM_RECORD_TYPE[EM_RECORD_TYPE["Pasuse"] = 2] = "Pasuse";
	})(EM_RECORD_TYPE || (EM_RECORD_TYPE = {}));
	class PlatformTT {
	    constructor() {
	        this._status = 0;
	        this._status = 0;
	    }
	    onRecordStart() {
	        console.log("-------------onRecordStart--------this._status:", this._status);
	        let duration = 200;
	        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderStart, 10, (result) => {
	            this._status = EM_RECORD_TYPE.Recording;
	            console.log("PlatformTT -RecorderStart-onStart:", JSON.stringify(result), this._status);
	        }, (videoPath) => {
	            console.log("PlatformTT -RecorderStart-onStop:", videoPath);
	        }, (result) => {
	            this._status = EM_RECORD_TYPE.Recording;
	            console.log("PlatformTT -RecorderResume:", result, this._status);
	        }, (result) => {
	            this._status = EM_RECORD_TYPE.Pasuse;
	            console.log("PlatformTT -RecorderPause:", result, this._status);
	        }, (result) => {
	            console.log("PlatformTT -RecorderStart-onError:", JSON.stringify(result));
	        });
	    }
	    onRecordStop() {
	        console.log("PlatformTT -TT- 停止录屏");
	        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderStop);
	    }
	    onRecordResume() {
	        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderResume);
	    }
	    onRecordPasue() {
	        ydhw.Invoke(YDHW.YDHW_TT_API.RecorderPause);
	    }
	    getRecordStatus() {
	        return this._status;
	    }
	    onShareAppMessage() {
	        ydhw.Invoke(YDHW.YDHW_TT_API.ShareImage, 'MainScene', () => {
	        }, "测试文案");
	    }
	    onShareVideo() {
	        ydhw.Invoke(YDHW.YDHW_TT_API.ShareVideo, '视频分享', '描述测试', '', (isOk) => {
	            console.log("PlatformTT -ShareVideo-isOk:", JSON.stringify(isOk));
	        });
	    }
	    onShareToken() {
	        ydhw.Invoke(YDHW.YDHW_TT_API.ShareToken, (isOk) => {
	            console.log("PlatformTT -ShareToken-isOk:", JSON.stringify(isOk));
	        });
	    }
	    onShareTemplate() {
	        ydhw.Invoke(YDHW.YDHW_TT_API.ShareTemplate, (isOk) => {
	            console.log("PlatformTT -ShareTemplate-isOk:", JSON.stringify(isOk));
	        });
	    }
	    showMoreGame() {
	        ydhw.Invoke(YDHW.YDHW_TT_API.ShowMoreGamesModal, (isOk) => {
	            console.log("PlatformTT -ShowMoreGamesModal-isOk:", JSON.stringify(isOk));
	        }, {
	            iconId: 1607290,
	            souce: "MainScene",
	            target: "ttfe8b883661a11109"
	        });
	    }
	    GetLeftTopBtnPosition() {
	    }
	    GetPlatformUserInfo() {
	    }
	}

	var EM_RECORD_STATUS;
	(function (EM_RECORD_STATUS) {
	    EM_RECORD_STATUS[EM_RECORD_STATUS["E_NONE"] = 1] = "E_NONE";
	    EM_RECORD_STATUS[EM_RECORD_STATUS["E_RECORDING"] = 2] = "E_RECORDING";
	    EM_RECORD_STATUS[EM_RECORD_STATUS["E_PAUSE"] = 3] = "E_PAUSE";
	})(EM_RECORD_STATUS || (EM_RECORD_STATUS = {}));
	var EM_RECORD_EVENT;
	(function (EM_RECORD_EVENT) {
	    EM_RECORD_EVENT["E_START"] = "start";
	    EM_RECORD_EVENT["E_STOP"] = "stop";
	    EM_RECORD_EVENT["E_PAUSE"] = "pause";
	    EM_RECORD_EVENT["E_RESUME"] = "resume";
	    EM_RECORD_EVENT["E_ABORT"] = "abort";
	    EM_RECORD_EVENT["E_TIME_UPDATE"] = "timeUpdate";
	    EM_RECORD_EVENT["E_ERROR"] = "error";
	})(EM_RECORD_EVENT || (EM_RECORD_EVENT = {}));
	class PlatformWX {
	    constructor() {
	        this.recorderStatus = 1;
	        this.showRecorderBtn = false;
	        this.RecordChangeCB = null;
	    }
	    CreateGridAd() {
	        let adStyle = new YDHW.AdStyle();
	        adStyle = {
	            top: 200,
	            left: 100,
	            width: 200,
	            height: 100
	        };
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.CreateGridAd, false, 'white', 8, adStyle);
	    }
	    ShowGridAd() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.ShowGridAd);
	    }
	    HideGridAd() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.HideGridAd);
	    }
	    SubscribeSysMsg() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.SubscribeSysMsg, ['SYS_MSG_TYPE_INTERACTIVE'], (result) => {
	            console.log("PlatformWX -SubscribeSysMsg-success:", result);
	        }, (error) => {
	            console.log("PlatformWX -SubscribeSysMsg-error:", error);
	        });
	    }
	    GetSetting() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.GetSetting, true, (result) => {
	            console.log("PlatformWX -GetSetting-success:", result);
	        }, (error) => {
	            console.log("PlatformWX -GetSetting-error:", error);
	        });
	    }
	    ShowUserInfoButton() {
	        ydhw.Invoke(YDHW.YDHW_API.CreateUserInfoButton, {
	            type: 'image',
	            image: './images/sp_btn_start.png',
	            style: {
	                left: 100,
	                top: 66,
	                width: 150,
	                height: 40,
	                lineHeight: 40,
	                borderRadius: 4
	            }
	        }, this, (result) => {
	            console.log("PlatformWX -ShowUserInfoButton-onSuccess:", result);
	            ydhw.Invoke(YDHW.YDHW_API.ShowUserInfoButton);
	        }, (error) => {
	            console.log("PlatformWX -ShowUserInfoButton-onError:", error);
	        });
	    }
	    HideUserInfoButton() {
	        ydhw.Invoke(YDHW.YDHW_API.HideUserInfoButton);
	    }
	    onShareAppMessage() {
	        ydhw.Invoke(YDHW.YDHW_API.ShareAppMessage, "MainScene", "aa", "bb", "award_id", this, (result) => {
	            console.log("PlatformWX --ShareAppMessage-back", JSON.stringify(result));
	            this.getShareResult();
	        });
	    }
	    getShareResult() {
	        let shareResultInfo = new YDHW.ShareAppInfo();
	        shareResultInfo = {
	            channel: "aa",
	            module: "bb",
	            showTime: new Date().getTime(),
	            shareId: 1
	        };
	        ydhw.Invoke(YDHW.YDHW_API.GetSharingResults, shareResultInfo, this, (shareBackInfo) => {
	            console.log("PlatformWX -GetSharingResults:", JSON.stringify(shareBackInfo));
	        });
	    }
	    NavigateToMiniProgram(id, toAppId, toUrl, source) {
	        ydhw.Invoke(YDHW.YDHW_API.NavigateToMiniProgram, id, toAppId, toUrl, source, this, (isOk) => {
	            console.log("PlatformWX -NavigateToMiniProgram-isOk:", isOk);
	        });
	    }
	    GetLeftTopBtnPosition() {
	    }
	    SetRecordChangeCB(callBack) {
	        this.RecordChangeCB = callBack;
	    }
	    GetRecordStatus() {
	        return this.recorderStatus;
	    }
	    GameRecorderStartOrStop() {
	        let isVolumeSupported = this.IsVolumeSupported();
	        let isFrameSupported = this.IsFrameSupported();
	        let isSoundSupported = this.IsSoundSupported();
	        console.log("PlatformWX -isVolumeSupported: " + isVolumeSupported + " ,isFrameSupported: " + isFrameSupported + " ,isSoundSupported：" + isSoundSupported);
	        if (EM_RECORD_STATUS.E_NONE == this.recorderStatus) {
	            let recordInfo = new YDHW.WX.GameRecorderInfo();
	            recordInfo.fps = 26;
	            recordInfo.duration = 20;
	            recordInfo.gop = 15;
	            recordInfo.hookBgm = true;
	            ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderStart, recordInfo, this, (res) => {
	                this.recorderStatus = EM_RECORD_STATUS.E_RECORDING;
	                if (this.RecordChangeCB)
	                    this.RecordChangeCB(this.recorderStatus);
	                console.log("PlatformWX -GameRecorderStart-onStart:", JSON.stringify(res));
	            }, (res) => {
	                this.recorderStatus = EM_RECORD_STATUS.E_NONE;
	                if (this.RecordChangeCB)
	                    this.RecordChangeCB(this.recorderStatus);
	                console.log("PlatformWX -GameRecorderStart-onStop:", JSON.stringify(res));
	            }, (error) => {
	                this.recorderStatus = EM_RECORD_STATUS.E_NONE;
	                if (this.RecordChangeCB)
	                    this.RecordChangeCB(this.recorderStatus);
	                console.log("PlatformWX -GameRecorderStart-onError:", JSON.stringify(error));
	            }, (res) => {
	                this.recorderStatus = EM_RECORD_STATUS.E_PAUSE;
	                if (this.RecordChangeCB)
	                    this.RecordChangeCB(this.recorderStatus);
	                console.log("PlatformWX -GameRecorderStart-onPause:", JSON.stringify(res));
	            }, (res) => {
	                this.recorderStatus = EM_RECORD_STATUS.E_RECORDING;
	                if (this.RecordChangeCB)
	                    this.RecordChangeCB(this.recorderStatus);
	                console.log("PlatformWX -GameRecorderStart-onResume:", JSON.stringify(res));
	            }, (res) => {
	                this.recorderStatus = EM_RECORD_STATUS.E_NONE;
	                if (this.RecordChangeCB)
	                    this.RecordChangeCB(this.recorderStatus);
	                console.log("PlatformWX -GameRecorderStart-onAbort:", JSON.stringify(res));
	            }, (res) => {
	                console.log("PlatformWX -GameRecorderStart-onTimeUpdate:", JSON.stringify(res));
	            });
	        }
	        else {
	            ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderStop);
	        }
	    }
	    GameRecorderPauseOrResume() {
	        if (EM_RECORD_STATUS.E_RECORDING == this.recorderStatus) {
	            ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderPause);
	        }
	        else {
	            ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderResume);
	        }
	    }
	    GameRecorderAbort() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderAbort);
	    }
	    CreateGameRecorderShareButton() {
	        let shareStyle = new YDHW.WX.GameRecorderBtnStyle(100, 100, 60, 8, 30, "#ffffff", 10, 10);
	        let shareInfo = new YDHW.WX.GameRecorderShareInfo("a=1&b=2", "");
	        let shareBtnInfo = new YDHW.WX.GameRecorderBtnInfo(shareStyle, shareInfo);
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.CreateRecorderShareButton, shareBtnInfo);
	    }
	    GameRecorderShareButtonOnTap() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.RecorderShareButtonOnTap, this, (res) => {
	            console.log("PlatformWX -GameRecorderShareButtonOnTap:", JSON.stringify(res));
	        });
	    }
	    WXRecorderOff() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderOff, EM_RECORD_EVENT.E_START, (res) => {
	            console.log("PlatformWX -WXRecorderOff-E_START:", JSON.stringify(res));
	        });
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderOff, EM_RECORD_EVENT.E_STOP, (res) => {
	            console.log("PlatformWX -WXRecorderOff-E_STOP:", JSON.stringify(res));
	        });
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderOff, EM_RECORD_EVENT.E_PAUSE, (res) => {
	            console.log("PlatformWX -WXRecorderOff-E_PAUSE:", JSON.stringify(res));
	        });
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderOff, EM_RECORD_EVENT.E_RESUME, (res) => {
	            console.log("PlatformWX -WXRecorderOff-E_RESUME:", JSON.stringify(res));
	        });
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderOff, EM_RECORD_EVENT.E_ABORT, (res) => {
	            console.log("PlatformWX -WXRecorderOff-E_ABORT:", JSON.stringify(res));
	        });
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderOff, EM_RECORD_EVENT.E_ERROR, (res) => {
	            console.log("PlatformWX -WXRecorderOff-E_ERROR:", JSON.stringify(res));
	        });
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.WXRecorderOff, EM_RECORD_EVENT.E_TIME_UPDATE, (res) => {
	            console.log("PlatformWX -WXRecorderOff-E_TIME_UPDATE:", JSON.stringify(res));
	        });
	    }
	    IsAtempoSupported() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.IsAtempoSupported);
	    }
	    IsFrameSupported() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.IsFrameSupported);
	    }
	    IsSoundSupported() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.IsSoundSupported);
	    }
	    IsVolumeSupported() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.IsVolumeSupported);
	    }
	    GameRecorderShareButtonShowOrHide(caller, method) {
	        if (this.showRecorderBtn) {
	            ydhw.Invoke(YDHW.YDHW_WECHAT_API.RecorderShareButtonHide);
	        }
	        else {
	            ydhw.Invoke(YDHW.YDHW_WECHAT_API.RecorderShareButtonShow);
	        }
	        this.showRecorderBtn = !this.showRecorderBtn;
	        caller && method && method.call(caller, this.showRecorderBtn);
	    }
	    SetMessageToFriendQuery() {
	        let shareMessageToFriendScene = 20;
	        let success = ydhw.Invoke(YDHW.YDHW_WECHAT_API.SetMessageToFriendQuery, shareMessageToFriendScene);
	        console.log("PlatformWX -SetMessageToFriendQuery-success:", success);
	    }
	    OnShareMessageToFriend() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.OnShareMessageToFriend, this, (res) => {
	            console.log("PlatformWX -OnShareMessageToFriend：", JSON.stringify(res));
	        });
	    }
	    OnShareTimeline() {
	        let timeLineInfo = new YDHW.WX.OnShareTimelineInfo("", "分享朋友圈测试", "a=1&b=2");
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.OnShareTimeline, timeLineInfo, this, () => {
	            console.log("PlatformWX -OnShareTimeline:", JSON.stringify(timeLineInfo));
	        });
	    }
	    OffShareTimeline() {
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.OffShareTimeline);
	    }
	    ShowCustomAd() {
	        let me = this;
	        let adStyle = new YDHW.AdStyle();
	        adStyle = {
	            top: 200,
	            left: 100,
	            fixed: 100,
	        };
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.CreateCustomAd, false, 31, adStyle, this, () => {
	            console.log("PlatformWX -CreateCustomAd-onLoad");
	            ydhw.Invoke(YDHW.YDHW_WECHAT_API.ShowCustomAd, me, (success) => {
	                console.log("PlatformWX -ShowCustomAd-success:", success);
	            });
	        }, () => {
	            console.log("PlatformWX -CreateCustomAd-onClose");
	        }, (error) => {
	            console.log("PlatformWX -CreateCustomAd-onError:", JSON.stringify(error));
	        });
	    }
	    HideCustomAd() {
	        let isShow = ydhw.Invoke(YDHW.YDHW_WECHAT_API.IsShowCustomAd);
	        console.log("PlatformWX -HideCustomAd-isShow:", isShow);
	        ydhw.Invoke(YDHW.YDHW_WECHAT_API.HideCustomAd);
	    }
	}

	var EM_APP_BOX_TYPE;
	(function (EM_APP_BOX_TYPE) {
	    EM_APP_BOX_TYPE[EM_APP_BOX_TYPE["APPBOX_CREATE_SUCCESS"] = 0] = "APPBOX_CREATE_SUCCESS";
	    EM_APP_BOX_TYPE[EM_APP_BOX_TYPE["APPBOX_CREATE_FAIL"] = 1] = "APPBOX_CREATE_FAIL";
	    EM_APP_BOX_TYPE[EM_APP_BOX_TYPE["APPBOX_CLOSE"] = 2] = "APPBOX_CLOSE";
	})(EM_APP_BOX_TYPE || (EM_APP_BOX_TYPE = {}));
	class PlatformQQ {
	    constructor() {
	    }
	    ShareByTemplateId() {
	        let info = new YDHW.QQ.ShareTempletInfo();
	        info.shareTemplateId = "EE558DDCEFB407FD811CC6C06181D6AF";
	        info.shareTemplateData = { txt1: "老铁就等你了，快上车!", txt2: "应邀前往" };
	        ydhw.Invoke(YDHW.YDHW_QQ_API.ShareByTemplateId, info, this, (result) => {
	            console.log("PlatformQQ -----ShareByTemplateId:", result);
	        });
	    }
	    AddColorSign() {
	        ydhw.Invoke(YDHW.YDHW_QQ_API.AddColorSign, this, (success) => {
	            console.log("PlatformQQ -----AddColorSign:", success);
	        });
	    }
	    SubscribeAppMsg() {
	        let appMsgInfo = new YDHW.QQ.AppMsgInfo();
	        appMsgInfo.subscribe = true;
	        appMsgInfo.tmplIds = ["4dec1376c90128640a36879636ec6320"];
	        ydhw.Invoke(YDHW.YDHW_QQ_API.SubscribeAppMsg, appMsgInfo, this, (success) => {
	            console.log("PlatformQQ -----SubscribeAppMsg:", success);
	        });
	    }
	    CreateAddFriendButton() {
	        let fBtnInfo = new YDHW.QQ.AddFriendButtonInfo();
	        fBtnInfo = {
	            type: 'text',
	            text: '添加好友',
	            openId: "6A43B93B26F2562B65D317398B58C9FE",
	            style: {
	                left: 10,
	                top: 276,
	                width: 200,
	                height: 40,
	                backgroundColor: '#ff0000',
	                borderColor: '#ff0000',
	                borderWidth: 2,
	                borderRadius: 5,
	                color: '#ffffff',
	                textAlign: 'center',
	                fontSize: 16,
	                lineHeight: 40,
	            }
	        };
	        ydhw.Invoke(YDHW.YDHW_QQ_API.CreateAddFriendButton, fBtnInfo, this, (success) => {
	            console.log("PlatformQQ -----CreateAddFriendButton:", success);
	            this.ShowAddFriendButton();
	        });
	    }
	    ShowAddFriendButton() {
	        ydhw.Invoke(YDHW.YDHW_QQ_API.ShowAddFriendButton);
	    }
	    HideAddFriendButton() {
	        ydhw.Invoke(YDHW.YDHW_QQ_API.HideAddFriendButton);
	        this.DestroyAddFriendButton();
	    }
	    DestroyAddFriendButton() {
	        ydhw.Invoke(YDHW.YDHW_QQ_API.DestroyAddFriendButton);
	    }
	    CreateAppBox() {
	        ydhw.Invoke(YDHW.YDHW_QQ_API.CreateAppBox, this, (status) => {
	            console.log("PlatformQQ -----CreateAppBox:", status);
	            if (status == EM_APP_BOX_TYPE.APPBOX_CREATE_SUCCESS) {
	                this.ShowAppBox();
	            }
	        });
	    }
	    ShowAppBox() {
	        ydhw.Invoke(YDHW.YDHW_QQ_API.ShowAppBox, this, () => {
	            console.log("PlatformQQ ----ShowAppBox-onSuccess:");
	        }, () => {
	            console.log("PlatformQQ ----ShowAppBox-onError:");
	        });
	    }
	    CreateBlockAd() {
	        let blockInfo = new YDHW.QQ.BlockAdInfo();
	        blockInfo = {
	            style: {
	                left: 50,
	                top: 50,
	            },
	            size: 5,
	            orientation: 'landscape'
	        };
	        ydhw.Invoke(YDHW.YDHW_QQ_API.CreateBlockAd, blockInfo, this, (status) => {
	            console.log("PlatformQQ ----CreateBlockAd-status:", status);
	            this.ShowBlockAd();
	        }, (data) => {
	            console.log("PlatformQQ ----onResize-data:", JSON.stringify(data));
	        });
	    }
	    ShowBlockAd() {
	        ydhw.Invoke(YDHW.YDHW_QQ_API.ShowBlockAd, this, () => {
	            console.log("PlatformQQ ----ShowBlockAd");
	        });
	    }
	    HideBlockAd() {
	        ydhw.Invoke(YDHW.YDHW_QQ_API.HideBlockAd);
	        this.DestroyBlockAd();
	    }
	    DestroyBlockAd() {
	        ydhw.Invoke(YDHW.YDHW_QQ_API.DestroyBlockAd);
	    }
	    onShareAppMessage() {
	        ydhw.Invoke(YDHW.YDHW_API.ShareAppMessage, "MainScene", "aa", "bb", "award_id", this, (result) => {
	            console.log("PlatformQQ --ShareAppMessage-back", JSON.stringify(result));
	            this.getShareResult();
	        }, YDHW.QQ.EM_SHARE_APP_TYPE.QQ);
	    }
	    getShareResult() {
	        let shareResultInfo = new YDHW.ShareAppInfo();
	        shareResultInfo = {
	            channel: "aa",
	            module: "bb",
	            showTime: new Date().getTime(),
	            shareId: 1
	        };
	        ydhw.Invoke(YDHW.YDHW_API.GetSharingResults, shareResultInfo, this, (shareBackInfo) => {
	            console.log("PlatformQQ -GetSharingResults:", JSON.stringify(shareBackInfo));
	        });
	    }
	}

	class PlatformOPPO {
	    constructor() {
	        this.nativeAdId = null;
	    }
	    CreateNativeAd() {
	        ydhw.Invoke(YDHW.YDHW_API.CreateNativeAd, this, (args) => {
	            console.log("PlatformOPPO ---CreateNativeAd-back:", JSON.stringify(args));
	            this.nativeAdId = args[0].adId;
	            console.log("PlatformOPPO ---nativeAdId:", this.nativeAdId);
	        });
	    }
	    ShowNativeAd() {
	        console.log("PlatformOPPO ---ShowNativeAd:", this.nativeAdId);
	        if (this.nativeAdId)
	            ydhw.Invoke(YDHW.YDHW_API.ShowNativeAd, this.nativeAdId);
	    }
	    ClickNativeAd() {
	        console.log("PlatformOPPO ---ClickNativeAd:", this.nativeAdId);
	        if (this.nativeAdId)
	            ydhw.Invoke(YDHW.YDHW_API.ClickNativeAd, this.nativeAdId);
	    }
	    NavigateToMiniProgram(id, toAppId, toUrl, source) {
	        ydhw.Invoke(YDHW.YDHW_API.NavigateToMiniProgram, id, toAppId, toUrl, source, this, (isOk) => {
	            console.log("PlatformOPPO -NavigateToMiniProgram-isOk:", isOk);
	        });
	    }
	}

	class PlatformMZ {
	    constructor() {
	    }
	    GetNetworkType() {
	        ydhw.Invoke(YDHW.YDHW_MZ_API.GetNetworkType, this, (type) => {
	            console.log("PlatformMZ --GetNetworkType-type:" + JSON.stringify(type));
	        });
	    }
	    OnNetworkStatusChange() {
	        ydhw.Invoke(YDHW.YDHW_MZ_API.OnNetworkStatusChange, this, (type, isConnected) => {
	            console.log("PlatformMZ --OnNetworkStatusChange-type:" + JSON.stringify(type) + ",isConnected:" + isConnected);
	        });
	    }
	}

	class PlatformVIVO {
	    constructor() {
	        this.nativeAdId = null;
	    }
	    CreateNativeAd() {
	        ydhw.Invoke(YDHW.YDHW_API.CreateNativeAd, this, (args) => {
	            console.log("PlatformVIVO ---CreateNativeAd-back:", JSON.stringify(args));
	            this.nativeAdId = args[0].adId;
	            console.log("PlatformVIVO ---nativeAdId:", this.nativeAdId);
	        });
	    }
	    ShowNativeAd() {
	        console.log("PlatformVIVO ---ShowNativeAd:", this.nativeAdId);
	        if (this.nativeAdId != null)
	            ydhw.Invoke(YDHW.YDHW_API.ShowNativeAd, this.nativeAdId);
	    }
	    ClickNativeAd() {
	        console.log("PlatformVIVO ---ClickNativeAd:", this.nativeAdId);
	        if (this.nativeAdId != null)
	            ydhw.Invoke(YDHW.YDHW_API.ClickNativeAd, this.nativeAdId);
	    }
	    IsStartupByShortcut() {
	        ydhw.Invoke(YDHW.YDHW_VIVO_API.IsStartupByShortcut, this, () => {
	            console.log("PlatformVIVO ---IsStartupByShortcut-onSuccess");
	        }, (error) => {
	            console.log("PlatformVIVO ---IsStartupByShortcut-error:" + JSON.stringify(error));
	        });
	    }
	}

	class PlatformPublic {
	    constructor() {
	        this.sLayerIndex = 0;
	        this.custom = 1;
	        this.lastTime = 0;
	    }
	    init() {
	        ydhw.Invoke(YDHW.YDHW_API.OnHide, this, function (res) {
	            console.log("PlatformPublic -OnHide:" + JSON.stringify(res));
	        });
	        ydhw.Invoke(YDHW.YDHW_API.OnShow, this, function (res) {
	            console.log("PlatformPublic -OnShow:" + JSON.stringify(res));
	        });
	        ydhw.Invoke(YDHW.YDHW_API.OnError, this, function (res) {
	            console.log("PlatformPublic -OnError:" + JSON.stringify(res));
	        });
	        this.lastTime = new Date().getTime();
	        ydhw.Invoke(YDHW.YDHW_API.ListenOnPowerChange, this, (powerInfo) => {
	            console.log("PlatformPublic -监听体力变化:", JSON.stringify(powerInfo));
	        });
	        ydhw.Invoke(YDHW.YDHW_API.Login, this, (isOk) => {
	            if (isOk) {
	                console.log("PlatformPublic -Login-isOk: " + isOk);
	                ydhw.Invoke(YDHW.YDHW_API.GetCustomConfig, this, (result) => {
	                    console.log("PlatformPublic -GetCustomConfig:", JSON.stringify(result));
	                });
	                ydhw.Invoke(YDHW.YDHW_API.GetSideBox, this, (result) => {
	                    this.sideBoxList = result;
	                    console.log("PlatformPublic -GetSideBox:", JSON.stringify(result));
	                });
	                ydhw.Invoke(YDHW.YDHW_API.ShareCard, "", this, (result) => {
	                    console.log("PlatformPublic -ShareCard:", JSON.stringify(result));
	                });
	                let me = this;
	                ydhw.Invoke(YDHW.YDHW_API.GetLayerList, (layerList) => {
	                    console.log("PlatformPublic -GetLayerList:", JSON.stringify(layerList));
	                    me.layerList = layerList;
	                    me.startGame();
	                });
	                ydhw.Invoke(YDHW.YDHW_API.GetScoreBoardList, this, (result) => {
	                    console.log("PlatformPublic -GetScoreBoardList:", JSON.stringify(result));
	                    ydhw.Invoke(YDHW.YDHW_API.GetScoreBoardAward, 54803, this, (result) => {
	                        console.log("PlatformPublic -GetScoreBoardAward:", JSON.stringify(result));
	                    });
	                });
	                let address = " ";
	                let shareInfo = ydhw.Invoke(YDHW.YDHW_API.ShareInfo);
	                console.log("PlatformPublic -LoginAddress:", address, ",ShareInfo:", JSON.stringify(shareInfo));
	                ydhw.Invoke(YDHW.YDHW_API.GetServerInfo, this, (result) => {
	                    console.log("PlatformPublic -GetServerInfo-result:", JSON.stringify(result));
	                });
	                this.initGameInfo();
	                this.testStatisticInterface();
	                this.testVideoRewardLimitInfo();
	                this.testCustomOrBout();
	                this.startGame();
	                this.testStoreValue();
	            }
	        });
	    }
	    initGameInfo() {
	        let SwitchTouch = ydhw.SwitchTouch;
	        let SwitchPush = ydhw.SwitchPush;
	        let IsRealVersion = ydhw.IsRealVersion;
	        let AppId = ydhw.AppId;
	        let PkgName = ydhw.PkgName;
	        let Version = ydhw.Version;
	        let AccountId = ydhw.AccountId;
	        let NickName = ydhw.NickName;
	        let AvatarUrl = ydhw.AvatarUrl;
	        let Code = ydhw.Code;
	        let OpenID = ydhw.OpenID;
	        let AccountPass = ydhw.AccountPass;
	        let IsNewPlayer = ydhw.IsNewPlayer;
	        let InviteAccount = ydhw.InviteAccount;
	        let SceneId = ydhw.SceneId;
	        console.log("PlatformPublic -标识后台返回的游戏版本号是否是真实的游戏版本号：", IsRealVersion);
	        console.log("PlatformPublic -游戏AppId：", AppId);
	        console.log("PlatformPublic -游戏包名：", PkgName);
	        console.log("PlatformPublic -游戏版本：", Version);
	        console.log("PlatformPublic -玩家账号：", AccountId);
	        console.log("PlatformPublic -玩家昵称(需要平台授权)：", NickName);
	        console.log("PlatformPublic -玩家头像(需要平台授权)：", AvatarUrl);
	        console.log("PlatformPublic -平台登录的Code：", Code);
	        console.log("PlatformPublic -平台登录OpenID：", OpenID);
	        console.log("PlatformPublic -玩家登录凭证：", AccountPass);
	        console.log("PlatformPublic -是否新用户：", IsNewPlayer);
	        console.log("PlatformPublic -邀请人账号(从分享进入的时候才有)：", InviteAccount);
	        console.log("PlatformPublic -SceneId：", SceneId);
	        console.log("PlatformPublic -误触开关：", SwitchTouch);
	        console.log("PlatformPublic -推送开关：", SwitchPush);
	        let BannerAdUnitIdList = ydhw.BannerAdUnitIdList;
	        console.log("PlatformPublic -BannerAdUnitIdList：" + JSON.stringify(BannerAdUnitIdList));
	    }
	    testStatisticInterface() {
	        ydhw.Invoke(YDHW.YDHW_API.StatBanner, YDHW.EM_TP_AD.SHOW, "15926jnded313m55ms");
	        ydhw.Invoke(YDHW.YDHW_API.StatVideo, YDHW.EM_TP_AD.CREATE, '6sn234aoqf3737fmu9');
	        ydhw.Invoke(YDHW.YDHW_API.StatEvent, "event", "scene");
	        let clickOutInfo = new YDHW.ClickOutRequest();
	        clickOutInfo = {
	            iconId: 1,
	            souce: "导出模块",
	            target: "导出游戏的appid",
	            action: "enable"
	        };
	        ydhw.Invoke(YDHW.YDHW_API.StatClickOut, clickOutInfo);
	        let _shareInfo = new YDHW.StatistiicShareInfo(2, 0);
	        ydhw.Invoke(YDHW.YDHW_API.StatShareCard, _shareInfo);
	    }
	    testVideoRewardLimitInfo() {
	        let isCanRewardByVideoOrShare = ydhw.Invoke(YDHW.YDHW_API.IsCanRewardByVideoOrShare, 1);
	        let ShareRewardLimit = ydhw.Invoke(YDHW.YDHW_API.GetShareRewardLimit);
	        let getVideoRewardLimit = ydhw.Invoke(YDHW.YDHW_API.GetVideoRewardLimit);
	        console.log("PlatformPublic -IsCanRewardByVideoOrShare:", isCanRewardByVideoOrShare);
	        console.log("PlatformPublic -GetShareRewardLimit:", ShareRewardLimit);
	        console.log("PlatformPublic -GetVideoRewardLimit:", getVideoRewardLimit);
	    }
	    testCustomOrBout() {
	        let boutCount = ydhw.Invoke(YDHW.YDHW_API.GetTodayBoutCount);
	        let totalBountCount = ydhw.Invoke(YDHW.YDHW_API.GetTotalBoutCount);
	        let lastBoutCount = ydhw.Invoke(YDHW.YDHW_API.GetLastBountNumber);
	        let maxBoutCount = ydhw.Invoke(YDHW.YDHW_API.GetMaxBountNumber);
	        let todayWatchVideoCount = ydhw.Invoke(YDHW.YDHW_API.GetTodayWatchVideoCounter);
	        console.log("PlatformPublic -GetTodayBoutCount:", boutCount);
	        console.log("PlatformPublic -GetTotalBoutCount:", totalBountCount);
	        console.log("PlatformPublic -GetLastBountNumber:", lastBoutCount);
	        console.log("PlatformPublic -GetMaxBountNumber:", maxBoutCount);
	        console.log("PlatformPublic -GetTodayWatchVideoCounter:", todayWatchVideoCount);
	    }
	    startGame() {
	        if (this.layerList && this.layerList.length > 0) {
	            this.currentLayerPath = this.layerList[this.sLayerIndex].layerPath;
	            console.log("PlatformPublic -StatisticLayer:", this.currentLayerPath, this.sLayerIndex);
	            ydhw.Invoke(YDHW.YDHW_API.StatLayer, this.currentLayerPath);
	        }
	    }
	    onOver() {
	        this.startGame();
	        ydhw.Invoke(YDHW.YDHW_API.GameOver, this.custom);
	        this.custom += 1;
	        this.sLayerIndex = (this.layerList && (this.sLayerIndex + 1) >= this.layerList.length) ? 0 : (this.sLayerIndex + 1);
	        let win = (this.sLayerIndex % 2 == 0);
	        let resultInfo = new YDHW.StatisticResultInfo();
	        resultInfo = {
	            source: 80, layerPath: this.currentLayerPath, hasWin: win,
	            detail: {
	                name: "test_name", type: "GameOver"
	            }
	        };
	        ydhw.Invoke(YDHW.YDHW_API.StatResult, resultInfo);
	    }
	    onDeepTouch() {
	        let deepTouch = ydhw.Invoke(YDHW.YDHW_API.GetDeepTouchInfo, 2);
	        console.log("PlatformPublic -GetDeepTouchInfo:", deepTouch);
	    }
	    onCreateBanner() {
	        ydhw.Invoke(YDHW.YDHW_API.CreateBannerAd, false, true, this, (isOk) => {
	            console.warn("PlatformPublic ------CreateBannerAd-isOk:", isOk);
	        });
	    }
	    onShowBanner() {
	        ydhw.Invoke(YDHW.YDHW_API.ShowBannerAd);
	        console.log("GameUI-onShowBanner---2:");
	    }
	    onSwitchView() {
	        ydhw.Invoke(YDHW.YDHW_API.SwitchView, true);
	    }
	    onHideBanner(e) {
	        console.log("GameUI-onHideBanner");
	        this.hideBanner();
	    }
	    onBannerAdChangeSize() {
	        ydhw.Invoke(YDHW.YDHW_API.BannerAdChangeSize, {
	            top: 100,
	            left: 50,
	            width: 100,
	            height: 30
	        });
	    }
	    onCreateInterstitialAd() {
	        console.log("GameUI-onCreateInterstitialAd");
	        ydhw.Invoke(YDHW.YDHW_API.CreateInterstitialAd, false, this, () => {
	            console.log("PlatformPublic -CreateInterstitialAd-onSuccess");
	        }, () => {
	            console.log("PlatformPublic -CreateInterstitialAd-onClose");
	        }, () => {
	            console.log("PlatformPublic -CreateInterstitialAd-onFail");
	        });
	    }
	    onShowInterstitial(e) {
	        ydhw.Invoke(YDHW.YDHW_API.ShowInterstitialAd);
	    }
	    onPowerAdd(e) {
	        let p = ydhw.Invoke(YDHW.YDHW_API.GetPower);
	        console.log("PlatformPublic -GetPower:", p);
	        p += 1;
	        ydhw.Invoke(YDHW.YDHW_API.SetPower, p, 2);
	    }
	    onPowerReduce(e) {
	        this.getPowerInfo();
	        let p = ydhw.Invoke(YDHW.YDHW_API.GetPower);
	        console.log("PlatformPublic -GetPower:", p);
	        p -= 1;
	        ydhw.Invoke(YDHW.YDHW_API.SetPower, p, 2);
	    }
	    getPowerInfo() {
	        let powerInfo = ydhw.Invoke(YDHW.YDHW_API.GetPowerInfo);
	        console.log("PlatformPublic -获取体力信息:", powerInfo);
	    }
	    onShowShareOrVideo() {
	        ydhw.Invoke(YDHW.YDHW_API.ShowShareVideo, "bb", "aa", this, (type) => {
	            console.log("PlatformPublic -ShowShareOrVideo:", type);
	        });
	        ydhw.Invoke(YDHW.YDHW_API.UseShareVideoStrategy, "bb", "aa", this, (type) => {
	            console.log("PlatformPublic -UseShareOrVideoStrategy:", type);
	        });
	    }
	    onShowVideo() {
	        this.hideBanner();
	        console.log("PlatformPublic ----onShowVideo-");
	        ydhw.Invoke(YDHW.YDHW_API.ShowRewardVideoAd, this.custom, true, this, (type) => {
	            console.log("PlatformPublic ---激励视频:", type);
	        });
	    }
	    hideBanner() {
	        ydhw.Invoke(YDHW.YDHW_API.HideBannerAd);
	    }
	    onUnlockCustomByVideo() {
	        let result = ydhw.Invoke(YDHW.YDHW_API.IsUnlockVideo, this.custom);
	        console.log("PlatformPublic -视频解锁关卡:", this.custom, result);
	    }
	    onChangeView() {
	        ydhw.Invoke(YDHW.YDHW_API.SwitchView, true);
	    }
	    CreateUserInfoButton() {
	        ydhw.Invoke(YDHW.YDHW_API.CreateUserInfoButton, {
	            type: 'image',
	            image: './images/sp_btn_start.png',
	            style: {
	                left: 100,
	                top: 66,
	                width: 150,
	                height: 40,
	                lineHeight: 40,
	                borderRadius: 4
	            }
	        }, this, (result) => {
	            console.log("PlatformPublic -CreateUserInfoButton-onSuccess:", result);
	        }, (error) => {
	            console.log("PlatformPublic -CreateUserInfoButton-onError:", error);
	        });
	    }
	    HideUserInfoButton() {
	        console.log("PlatformPublic-HideUserInfoButton");
	        ydhw.Invoke(YDHW.YDHW_API.HideUserInfoButton);
	    }
	    GetUserInfo() {
	        ydhw.Invoke(YDHW.YDHW_API.GetUserInfo, this, (result) => {
	            console.log("PlatformPublic-GetUserInfo---result:" + JSON.stringify(result));
	        }, (error) => {
	            console.log("PlatformPublic-GetUserInfo---error:" + JSON.stringify(error));
	        });
	    }
	    HasShortcutInstalled() {
	        ydhw.Invoke(YDHW.YDHW_API.HasShortcutInstalled, this, (result) => {
	            console.log("PlatformPublic ---HasShortcutInstalled-onSuccess-result:", result);
	        }, (error) => {
	            console.log("PlatformPublic ---HasShortcutInstalled-error:", error);
	        }, () => {
	            console.log("PlatformPublic ---HasShortcutInstalled-onComplete");
	        });
	    }
	    InstallShortcut() {
	        ydhw.Invoke(YDHW.YDHW_API.InstallShortcut, this, () => {
	            console.log("PlatformPublic ---InstallShortcut-onSuccess");
	        }, (error) => {
	            console.log("PlatformPublic ---InstallShortcut-error:" + JSON.stringify(error));
	        }, () => {
	            console.log("PlatformPublic ---InstallShortcut-onComplete");
	        });
	    }
	    Exit() {
	        ydhw.Invoke(YDHW.YDHW_API.ExitGame);
	    }
	    VibrateShort() {
	        ydhw.Invoke(YDHW.YDHW_API.VibrateShort);
	    }
	    VibrateLong() {
	        ydhw.Invoke(YDHW.YDHW_API.VibrateLong);
	    }
	    getSideBoxList() {
	        return this.sideBoxList;
	    }
	    testStoreValue() {
	        this.test_sv_string();
	        this.test_sv_list();
	        this.test_sv_set();
	        this.test_sv_hash();
	        this.test_sv_radom();
	    }
	    test_sv_string() {
	        ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testString", "set", "测试数据"), this, function (status) {
	            ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testString", "get"), function (status) {
	            });
	        });
	    }
	    test_sv_list() {
	        ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testList", "add", "0"), this, function (status) {
	        });
	        ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testList", "add", "2"), this, function (status) {
	        });
	        ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testList", "set", "0,3"), this, function (status) {
	            ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testList", "all"), this, function (status) {
	            });
	            ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testList", "get", "0"), this, function (status) {
	            });
	            ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testList", "size"), this, function (status) {
	            });
	            ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testList", "poll", "2"), this, function (status) {
	                ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testList", "size"), this, function (status) {
	                });
	                ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testList", "replace", "[\"1\",\"2\",\"3\",\"4\",\"5\"]"), this, function (status) {
	                    ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testList", "all"), this, function (status) {
	                    });
	                });
	            });
	        });
	    }
	    test_sv_set() {
	        ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testSet", "add", "12"), this, function (status) {
	        });
	        ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testSet", "add", "10"), this, function (status) {
	            ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testSet", "exist", "10"), this, function (status) {
	            });
	            ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testSet", "size"), this, function (status) {
	                ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testSet", "del", "10"), this, function (status) {
	                    YDHW.YDHW_API.StoreValue,
	                        ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testSet", "all", "12"), this, function (status) {
	                            ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testSet", "replace", "[\"1\",\"2\",\"3\",\"4\",\"5\"]"), this, function (status) {
	                                ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testSet", "all"), this, function (status) {
	                                });
	                            });
	                        });
	                });
	            });
	        });
	    }
	    test_sv_hash() {
	        ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testHash", "set", "u_name,许"), this, function (status) {
	            ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testHash", "get", "u_name"), this, function (status) {
	                ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testHash", "replace", "{\"u_name\":\"唐\",\"sex\":\"women\"}"), this, function (status) {
	                    ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testHash", "gets", "u_name,sex"), this, function (status) {
	                    });
	                    ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testHash", "size"), this, function (status) { });
	                    ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testHash", "values", "u_name,sex"), this, function (status) { });
	                    ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testHash", "del", "u_name"), this, function (status) {
	                        ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testHash", "all"), this, function (status) {
	                        });
	                    });
	                });
	            });
	        });
	    }
	    test_sv_radom() {
	        ydhw.Invoke(YDHW.YDHW_API.StoreValue, new YDHW.StoreValueRequest("testRandom"), this, function (status) {
	        });
	    }
	}

	var EM_PLATFORM;
	(function (EM_PLATFORM) {
	    EM_PLATFORM[EM_PLATFORM["WX"] = 0] = "WX";
	    EM_PLATFORM[EM_PLATFORM["QQ"] = 1] = "QQ";
	    EM_PLATFORM[EM_PLATFORM["OPPO"] = 2] = "OPPO";
	    EM_PLATFORM[EM_PLATFORM["VIVO"] = 3] = "VIVO";
	    EM_PLATFORM[EM_PLATFORM["TT"] = 4] = "TT";
	    EM_PLATFORM[EM_PLATFORM["BAI_DU"] = 5] = "BAI_DU";
	    EM_PLATFORM[EM_PLATFORM["MEI_ZU"] = 12] = "MEI_ZU";
	})(EM_PLATFORM || (EM_PLATFORM = {}));
	class GameUI extends ui.test.TestSceneUI {
	    constructor() {
	        super();
	        this.PlatForm = null;
	        this.PlatFormPUB = null;
	        this.platformId = window['YDHW_CONFIG'].platformCode || 0;
	        GameUI.instance = this;
	        Laya.MouseManager.multiTouchEnabled = false;
	    }
	    onEnable() {
	        console.log("YDHW ---YDHW_CONFIG:" + window['YDHW_CONFIG'].platformCode);
	        this.initPlatform();
	        this.initUI();
	        this.PlatFormPUB.init();
	    }
	    initPlatform() {
	        this.PlatFormPUB = new PlatformPublic();
	        let platfromName = "平台";
	        switch (this.platformId) {
	            case EM_PLATFORM.WX:
	                this.PlatForm = new PlatformWX();
	                platfromName = "微信平台";
	                break;
	            case EM_PLATFORM.TT:
	                this.PlatForm = new PlatformTT();
	                platfromName = "头条平台";
	                break;
	            case EM_PLATFORM.QQ:
	                this.PlatForm = new PlatformQQ();
	                platfromName = "QQ平台";
	                break;
	            case EM_PLATFORM.OPPO:
	                this.PlatForm = new PlatformOPPO();
	                platfromName = "OPPO平台";
	                break;
	            case EM_PLATFORM.VIVO:
	                this.PlatForm = new PlatformVIVO();
	                platfromName = "VIVO平台";
	                break;
	            case EM_PLATFORM.MEI_ZU:
	                this.PlatForm = new PlatformMZ();
	                platfromName = "魅族平台";
	                break;
	        }
	        this.txt_platfrom_name.text = platfromName;
	    }
	    initUI() {
	        this.hideAllPlatformUI();
	        this.initPlatformView();
	        this.initPublicBtn();
	    }
	    initPlatformView() {
	        console.warn("YDHW ---当前平台ID：" + this.platformId);
	        switch (this.platformId) {
	            case EM_PLATFORM.WX:
	                this.initWXplatformUI();
	                break;
	            case EM_PLATFORM.TT:
	                this.initTTplatformUI();
	                break;
	            case EM_PLATFORM.QQ:
	                this.initQQplatformUI();
	                break;
	            case EM_PLATFORM.OPPO:
	                this.initOPPOplatformUI();
	                break;
	            case EM_PLATFORM.VIVO:
	                this.initVIVOUplatformUI();
	                break;
	            case EM_PLATFORM.MEI_ZU:
	                this.initMEIZUplatformUI();
	                break;
	        }
	    }
	    hideAllPlatformUI() {
	        this.lb_wx_node.visible = false;
	        this.lb_tt_node.visible = false;
	        this.lb_qq_node.visible = false;
	        this.lb_oppo_node.visible = false;
	        this.lb_vivo_node.visible = false;
	        this.lb_meizu_node.visible = false;
	    }
	    initPublicBtn() {
	        this.btnCreateBanner.on(Laya.Event.CLICK, this, this.onCreateBanner);
	        this.btnShowShareOrVideo.on(Laya.Event.CLICK, this, this.onShowShareOrVideo);
	        this.btnShowBanner.on(Laya.Event.CLICK, this, this.onShowBanner);
	        this.btnHideBanner.on(Laya.Event.CLICK, this, this.onHideBanner);
	        this.btnBannerAdChangeSize.on(Laya.Event.CLICK, this, this.onBannerAdChangeSize);
	        this.btnShowInterstitial.on(Laya.Event.CLICK, this, this.onShowInterstitial);
	        this.btnCreateInterstitialAd.on(Laya.Event.CLICK, this, this.onCreateInterstitialAd);
	        this.btnShowVideoAd.on(Laya.Event.CLICK, this, this.onShowVideo);
	        this.btnOver.on(Laya.Event.CLICK, this, this.onOver);
	        this.btnDeepTouch.on(Laya.Event.CLICK, this, this.onDeepTouch);
	        this.btnPowerAdd.on(Laya.Event.CLICK, this, this.onPowerAdd);
	        this.btnPowerReduce.on(Laya.Event.CLICK, this, this.onPowerReduce);
	        this.btnShare.on(Laya.Event.CLICK, this, this.onShare);
	        this.btnSwicthView.on(Laya.Event.CLICK, this, this.onSwitchView);
	        this.btnUnlockCustomByVideo.on(Laya.Event.CLICK, this, this.onUnlockCustomByVideo);
	        this.btnExit.on(Laya.Event.CLICK, this, this.Exit);
	        this.btnVibrateShort.on(Laya.Event.CLICK, this, this.VibrateShort);
	        this.btnVibrateLong.on(Laya.Event.CLICK, this, this.VibrateLong);
	        this.btnHasShortcutInstalled.on(Laya.Event.CLICK, this, this.HasShortcutInstalled);
	        this.btnInstallShortcut.on(Laya.Event.CLICK, this, this.InstallShortcut);
	        this.btnNatigateToMiniProgram.on(Laya.Event.CLICK, this, this.NavigateToMiniProgram);
	        this.btnGetUserInfo.on(Laya.Event.CLICK, this, this.GetUserInfo);
	        if (this.platformId == EM_PLATFORM.OPPO) {
	            this.btnShowInterstitial.visible = false;
	            this.btnCreateInterstitialAd.visible = false;
	        }
	        this.btnNatigateToMiniProgram.visible = (this.platformId == EM_PLATFORM.OPPO || this.platformId == EM_PLATFORM.WX);
	        if (!(this.platformId == EM_PLATFORM.OPPO || this.platformId == EM_PLATFORM.VIVO)) {
	            this.btnHasShortcutInstalled.visible = false;
	            this.btnInstallShortcut.visible = (this.platformId == EM_PLATFORM.QQ);
	        }
	        this.btnShare.visible = (this.platformId == EM_PLATFORM.WX || this.platformId == EM_PLATFORM.QQ || this.platformId == EM_PLATFORM.TT);
	        this.btnGetUserInfo.visible = (this.platformId == EM_PLATFORM.WX || this.platformId == EM_PLATFORM.QQ || this.platformId == EM_PLATFORM.TT);
	    }
	    initWXplatformUI() {
	        this.lb_wx_node.visible = true;
	        this.btnWXcreateGridAd.on(Laya.Event.CLICK, this, this.WX_CreateGridAd);
	        this.btnWXshowGridAd.on(Laya.Event.CLICK, this, this.WX_ShowGridAd);
	        this.btnWXhideGridAd.on(Laya.Event.CLICK, this, this.WX_HideGridAd);
	        this.btnWXsubscribeSysMsg.on(Laya.Event.CLICK, this, this.WX_SubscribeSysMsg);
	        this.btnWXgetSetting.on(Laya.Event.CLICK, this, this.WX_GetSetting);
	        this.btnWXshowUserInfoButton.on(Laya.Event.CLICK, this, this.WX_ShowUserInfoButton);
	        this.btnWXHideUserInfoButton.on(Laya.Event.CLICK, this, this.WX_HideUserInfoButton);
	        this.btnGameRecorderStartOrStop.on(Laya.Event.CLICK, this, this.WX_GameRecorderStartOrStop);
	        this.btnGameRecorderPauseOrResume.on(Laya.Event.CLICK, this, this.WX_GameRecorderPauseOrResume);
	        this.btnGameRecorderAbort.on(Laya.Event.CLICK, this, this.WX_GameRecorderAbort);
	        this.btnCreateGameRecorderShareButton.on(Laya.Event.CLICK, this, this.WX_CreateGameRecorderShareButton);
	        this.btnGameRecorderShareButtonOnTap.on(Laya.Event.CLICK, this, this.WX_GameRecorderShareButtonOnTap);
	        this.btnHideCustomAd.on(Laya.Event.CLICK, this, this.WX_HideCustomAd);
	        this.btnShowCustomAd.on(Laya.Event.CLICK, this, this.WX_ShowCustomAd);
	        this.btnGameRecorderPauseOrResume.visible = false;
	        this.WX_SetRecordChangeCB();
	    }
	    initTTplatformUI() {
	        this.lb_tt_node.visible = true;
	        this.btnTTrecordStart.on(Laya.Event.CLICK, this, this.TT_RecordStart);
	        this.btnTTrecordStop.on(Laya.Event.CLICK, this, this.TT_RecordStop);
	        this.btnTTrecordPasueOrResume.on(Laya.Event.CLICK, this, this.TT_RecordPasueOrResume);
	        this.btnTTshareVideo.on(Laya.Event.CLICK, this, this.TT_ShareVideo);
	        this.btnTTshareToken.on(Laya.Event.CLICK, this, this.TT_ShareToken);
	        this.btnTTshareTemplate.on(Laya.Event.CLICK, this, this.TT_ShareTemplate);
	        this.btnTTshowMoreGame.on(Laya.Event.CLICK, this, this.TT_ShowMoreGame);
	        this.btnTTgetLeftTopBtnPosition.on(Laya.Event.CLICK, this, this.TT_GetLeftTopBtnPosition);
	    }
	    initQQplatformUI() {
	        this.lb_qq_node.visible = true;
	        this.btnShowAppBoxAd.on(Laya.Event.CLICK, this, this.QQ_ShowAppBoxAd);
	        this.btnShowBlockAd.on(Laya.Event.CLICK, this, this.QQ_ShowBlockAd);
	        this.btnHideBlockAd.on(Laya.Event.CLICK, this, this.QQ_HideBlockAd);
	        this.btnShareTemplate.on(Laya.Event.CLICK, this, this.QQ_ShareTemplate);
	        this.btnColorSign.on(Laya.Event.CLICK, this, this.QQ_addColorSign);
	        this.btnSubscribeAppMsg.on(Laya.Event.CLICK, this, this.QQ_SubscribeAppMsg);
	        this.btnAddFriend.on(Laya.Event.CLICK, this, this.QQ_AddFriend);
	        this.btnHideAddFriend.on(Laya.Event.CLICK, this, this.QQ_HideFriend);
	        this.btnHideAddFriend.on(Laya.Event.CLICK, this, this.QQ_HideFriend);
	    }
	    initOPPOplatformUI() {
	        console.warn("YDHW ---初始化平台独有UI-OPPO：");
	        this.lb_oppo_node.visible = true;
	        this.btnCreateNativeAd.on(Laya.Event.CLICK, this, this.OPPO_CreateNativeAd);
	        this.btnShowNativeAd.on(Laya.Event.CLICK, this, this.OPPO_ShowNativeAd);
	        this.btnClickNativeAd.on(Laya.Event.CLICK, this, this.OPPO_ClickNativeAd);
	    }
	    initMEIZUplatformUI() {
	        this.lb_meizu_node.visible = true;
	        this.btnGetNetworkType.on(Laya.Event.CLICK, this, this.MZ_GetNetworkType);
	        this.btnOnNetworkStatusChange.on(Laya.Event.CLICK, this, this.MZ_OnNetworkStatusChange);
	    }
	    initVIVOUplatformUI() {
	        this.lb_vivo_node.visible = true;
	        this.btnCreateNativeAd_VIVO.on(Laya.Event.CLICK, this, this.VIVO_CreateNativeAd);
	        this.btnShowNativeAd_VIVO.on(Laya.Event.CLICK, this, this.VIVO_ShowNativeAd);
	        this.btnClickNativeAd_VIVO.on(Laya.Event.CLICK, this, this.VIVO_ClickNativeAd);
	    }
	    onShowShareOrVideo(e) {
	        this.PlatFormPUB.onShowShareOrVideo();
	    }
	    onCreateBanner(e) {
	        this.PlatFormPUB.onCreateBanner();
	    }
	    onShowBanner(e) {
	        this.PlatFormPUB.onShowBanner();
	    }
	    onHideBanner(e) {
	        this.PlatFormPUB.onHideBanner();
	    }
	    onBannerAdChangeSize(e) {
	        this.PlatFormPUB.onBannerAdChangeSize();
	    }
	    onShowInterstitial(e) {
	        this.PlatFormPUB.onShowInterstitial();
	    }
	    onShowVideo(e) {
	        this.PlatFormPUB.onShowVideo();
	    }
	    onOver(e) {
	        this.PlatFormPUB.onOver();
	    }
	    onDeepTouch(e) {
	        this.PlatFormPUB.onDeepTouch();
	    }
	    onPowerAdd(e) {
	        this.PlatFormPUB.onPowerAdd();
	    }
	    onPowerReduce(e) {
	        this.PlatFormPUB.onPowerReduce();
	    }
	    onSwitchView(e) {
	        this.PlatFormPUB.onSwitchView();
	    }
	    onCreateInterstitialAd(e) {
	        this.PlatFormPUB.onCreateInterstitialAd();
	    }
	    onUnlockCustomByVideo(e) {
	        this.PlatFormPUB.onUnlockCustomByVideo();
	    }
	    Exit(e) {
	        this.PlatFormPUB.Exit();
	    }
	    VibrateShort(e) {
	        this.PlatFormPUB.VibrateShort();
	    }
	    VibrateLong(e) {
	        this.PlatFormPUB.VibrateLong();
	    }
	    HasShortcutInstalled(e) {
	        this.PlatFormPUB.HasShortcutInstalled();
	    }
	    InstallShortcut(e) {
	        this.PlatFormPUB.InstallShortcut();
	    }
	    GetUserInfo(e) {
	        this.PlatFormPUB.GetUserInfo();
	    }
	    NavigateToMiniProgram(e) {
	        let sideBoxList = this.PlatFormPUB.getSideBoxList();
	        if (!sideBoxList || sideBoxList.length == 0) {
	            console.log("GameUI -NavigateToMiniProgram-侧边栏数据为空");
	            return;
	        }
	        let id = sideBoxList[0]._id;
	        let toAppid = sideBoxList[0].toAppid;
	        let toUrl = sideBoxList[0].toUrl;
	        console.log("GameUI -NavigateToMiniProgram:", id, toAppid, toUrl);
	        this.PlatForm.NavigateToMiniProgram(id, toAppid, toUrl, "MainScene_SideBox");
	    }
	    onShare(e) {
	        this.PlatForm.onShareAppMessage();
	    }
	    WX_CreateGridAd(e) {
	        this.PlatForm.CreateGridAd();
	    }
	    WX_ShowGridAd(e) {
	        this.PlatForm.ShowGridAd();
	    }
	    WX_HideGridAd(e) {
	        this.PlatForm.HideGridAd();
	    }
	    WX_SubscribeSysMsg(e) {
	        this.PlatForm.SubscribeSysMsg();
	    }
	    WX_GetSetting(e) {
	        this.PlatForm.GetSetting();
	    }
	    WX_ShowUserInfoButton(e) {
	        this.PlatForm.ShowUserInfoButton();
	    }
	    WX_HideUserInfoButton(e) {
	        this.PlatForm.HideUserInfoButton();
	    }
	    WX_GetLeftTopBtnPosition(e) {
	        this.PlatForm.GetLeftTopBtnPosition();
	    }
	    WX_GameRecorderStartOrStop(e) {
	        this.PlatForm.GameRecorderStartOrStop();
	    }
	    WX_GameRecorderPauseOrResume(e) {
	        this.PlatForm.GameRecorderPauseOrResume();
	    }
	    WX_GameRecorderAbort(e) {
	        this.PlatForm.GameRecorderAbort();
	    }
	    WX_CreateGameRecorderShareButton(e) {
	        this.PlatForm.CreateGameRecorderShareButton();
	    }
	    WX_GameRecorderShareButtonOnTap(e) {
	        this.PlatForm.GameRecorderShareButtonOnTap();
	    }
	    WX_SetRecordChangeCB() {
	        this.PlatForm.SetRecordChangeCB((res) => {
	            let status = this.PlatForm.GetRecordStatus();
	            let startOrStop = "开始录屏";
	            let pauseOrResume = "暂停录屏";
	            switch (status) {
	                case EM_RECORD_STATUS.E_NONE:
	                    startOrStop = "开始录屏";
	                    pauseOrResume = "暂停录屏";
	                    this.btnGameRecorderPauseOrResume.visible = false;
	                    break;
	                case EM_RECORD_STATUS.E_PAUSE:
	                    startOrStop = "停止录屏";
	                    pauseOrResume = "继续录屏";
	                    break;
	                case EM_RECORD_STATUS.E_RECORDING:
	                    startOrStop = "停止录屏";
	                    pauseOrResume = "暂停录屏";
	                    this.btnGameRecorderPauseOrResume.visible = true;
	                    break;
	            }
	            this.btnGameRecorderStartOrStop.label = startOrStop;
	            this.btnGameRecorderPauseOrResume.label = pauseOrResume;
	        });
	    }
	    WX_ShowCustomAd(e) {
	        this.PlatForm.ShowCustomAd();
	    }
	    WX_HideCustomAd(e) {
	        this.PlatForm.HideCustomAd();
	    }
	    TT_RecordStart(e) {
	        this.PlatForm.onRecordStart();
	    }
	    TT_RecordStop(e) {
	        console.log("YDHW 停止录屏");
	        this.PlatForm.onRecordStop();
	    }
	    TT_RecordPasueOrResume(e) {
	        let status = this.PlatForm.getRecordStatus();
	        if (status === 2) {
	            this.PlatForm.onRecordResume();
	        }
	        else {
	            this.PlatForm.onRecordPasue();
	        }
	    }
	    TT_ShareVideo(e) {
	        this.PlatForm.onShareVideo();
	    }
	    TT_ShareToken(e) {
	        this.PlatForm.onShareToken();
	    }
	    TT_ShareTemplate(e) {
	        this.PlatForm.onShareTemplate();
	    }
	    TT_ShowMoreGame(e) {
	        this.PlatForm.showMoreGame();
	    }
	    TT_GetLeftTopBtnPosition(e) {
	        this.PlatForm.GetLeftTopBtnPosition();
	    }
	    TT_GetPlatformUserInfo(e) {
	        this.PlatForm.GetPlatformUserInfo();
	    }
	    QQ_ShowAppBoxAd(e) {
	        this.PlatForm.CreateAppBox();
	    }
	    QQ_ShowBlockAd(e) {
	        this.PlatForm.CreateBlockAd();
	    }
	    QQ_HideBlockAd(e) {
	        this.PlatForm.HideBlockAd();
	    }
	    QQ_ShareTemplate(e) {
	        this.PlatForm.ShareByTemplateId();
	    }
	    QQ_addColorSign(e) {
	        this.PlatForm.AddColorSign();
	    }
	    QQ_SubscribeAppMsg(e) {
	        this.PlatForm.SubscribeAppMsg();
	    }
	    QQ_AddFriend(e) {
	        this.PlatForm.CreateAddFriendButton();
	    }
	    QQ_HideFriend(e) {
	        this.PlatForm.HideAddFriendButton();
	    }
	    OPPO_CreateNativeAd(e) {
	        this.PlatForm.CreateNativeAd();
	    }
	    OPPO_ShowNativeAd(e) {
	        this.PlatForm.ShowNativeAd();
	    }
	    OPPO_ClickNativeAd(e) {
	        this.PlatForm.ClickNativeAd();
	    }
	    MZ_GetNetworkType(e) {
	        this.PlatForm.GetNetworkType();
	    }
	    MZ_OnNetworkStatusChange(e) {
	        this.PlatForm.OnNetworkStatusChange();
	    }
	    VIVO_CreateNativeAd(e) {
	        this.PlatForm.CreateNativeAd();
	    }
	    VIVO_ShowNativeAd(e) {
	        this.PlatForm.ShowNativeAd();
	    }
	    VIVO_ClickNativeAd(e) {
	        this.PlatForm.ClickNativeAd();
	    }
	    VIVO_IsStartupByShortcut(e) {
	        this.PlatForm.IsStartupByShortcut();
	    }
	}

	class GameConfig {
	    constructor() {
	    }
	    static init() {
	        var reg = Laya.ClassUtils.regClass;
	        reg("script/GameUI.ts", GameUI);
	    }
	}
	GameConfig.width = 640;
	GameConfig.height = 1136;
	GameConfig.scaleMode = "fixedwidth";
	GameConfig.screenMode = "none";
	GameConfig.alignV = "top";
	GameConfig.alignH = "left";
	GameConfig.startScene = "test/TestScene.scene";
	GameConfig.sceneRoot = "";
	GameConfig.debug = false;
	GameConfig.stat = false;
	GameConfig.physicsDebug = false;
	GameConfig.exportSceneToJson = true;
	GameConfig.init();

	class Main {
	    constructor() {
	        if (window["Laya3D"])
	            Laya3D.init(GameConfig.width, GameConfig.height);
	        else
	            Laya.init(GameConfig.width, GameConfig.height, Laya["WebGL"]);
	        Laya["Physics"] && Laya["Physics"].enable();
	        Laya["DebugPanel"] && Laya["DebugPanel"].enable();
	        Laya.stage.scaleMode = GameConfig.scaleMode;
	        Laya.stage.screenMode = GameConfig.screenMode;
	        Laya.stage.alignV = GameConfig.alignV;
	        Laya.stage.alignH = GameConfig.alignH;
	        Laya.URL.exportSceneToJson = GameConfig.exportSceneToJson;
	        if (GameConfig.debug || Laya.Utils.getQueryString("debug") == "true")
	            Laya.enableDebugPanel();
	        if (GameConfig.physicsDebug && Laya["PhysicsDebugDraw"])
	            Laya["PhysicsDebugDraw"].enable();
	        if (GameConfig.stat)
	            Laya.Stat.show();
	        Laya.alertGlobalError = true;
	        Laya.ResourceVersion.enable("version.json", Laya.Handler.create(this, this.onVersionLoaded), Laya.ResourceVersion.FILENAME_VERSION);
	    }
	    onVersionLoaded() {
	        Laya.AtlasInfoManager.enable("fileconfig.json", Laya.Handler.create(this, this.onConfigLoaded));
	    }
	    onConfigLoaded() {
	        GameConfig.startScene && Laya.Scene.open(GameConfig.startScene);
	    }
	}
	new Main();

}());
